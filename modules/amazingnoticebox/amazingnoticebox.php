<?php
/**
* 20012-2014 Vietmoonlight
*
* NOTICE OF LICENSE
*
* This is a commercial license
* Do not allow to re-sales, edit without permission from Vietmoonlight.
* International Registered Trademark & Property of Vietmoonlight
*
* @author    Yen Truong <truthblue82@gmail.com>
* @copyright Vietmoonlight.com
* @license   Commercial License. All right reserved
*/

if (!defined('_PS_VERSION_'))
exit;


class AmazingNoticeBox extends Module
{
	private $html;
	private $display;
	private $hooks = array('displayTop','displayHeader');

	public function __construct()
	{
		$this->name = 'amazingnoticebox';
		$this->tab = 'front_office_features';
		$this->version = '3.3.1';
		$this->author = 'PrestaSOO.com';
		$this->need_instance = 0;
		$this->module_key = 'e8136ba95fa52abcd47d90473f350671';
		$this->ps_versions_compliancy = array('min' => '1.5', 'max' => '1.6');
		$this->bootstrap = true;

		parent::__construct();

		$this->displayName = $this->l('Amazing Notice Box and Bar');
		$this->description = $this->l('Support administrator add an box or bar notification and assign page for unlimited.');
		$this->confirmUninstall = $this->l('Are you crazy? It will erase all your Amazing Notice Box and Bar setting!!!');

	}

	public function install()
	{
		if (!parent::install()
			|| !$this->addHooks()
			|| !$this->setConfiguration()
			|| !$this->createTables()
			|| !$this->installModuleTab('AdminAmazingNoticeBox', $this->displayName))
				return false;
		return true;
	}

	public function uninstall()
	{
		$flag = true;
		if (!parent::uninstall()
			|| !$this->dropTables()
			|| !$this->uninstallModuleTab('AdminAmazingNoticeBox'))
			$flag = false;
		$this->removeHooks();

		return $flag;
	}

	private function setConfiguration()
	{
		$configs = array('AMAZING_NOTICE_BOX_ACTIVE' => 0);
		$checked = true;
		foreach ($configs as $k => $v)
		{
			if (!Configuration::updateValue($k, $v))
				$checked = false;
		}
		return $checked;
	}

	private function createAmazingNoticeBoxTable()
	{
		$sql = 'CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'amazing_notice_box (
				`id_amazing_notice_box` int(11) NOT NULL AUTO_INCREMENT,
				`datefrom` DATE NULL,
				`dateto` DATE NULL,
				`expire` int(11) NULL DEFAULT 0,
				`type` VARCHAR(3) NOT NULL DEFAULT "Box",
				`position` VARCHAR(6) NOT NULL DEFAULT "Center",
				`bg_color` varchar(7) NOT NULL DEFAULT "#fff",
				`text_color` varchar(7) NOT NULL DEFAULT "#000",
				`border_color` tinyint(1) NOT NULL DEFAULT 1,
				`pages` VARCHAR(200) NULL DEFAULT "index",
				`active` tinyint(1) NOT NULL DEFAULT 0,
				`deleted` tinyint(1) NOT NULL DEFAULT 0,
				PRIMARY KEY(`id_amazing_notice_box`),
				INDEX index_pages (`pages`),
				INDEX index_datefrom (`datefrom`),
				INDEX index_dateto (`dateto`)
			) ENGINE='._MYSQL_ENGINE_.' default CHARSET=utf8;';
		return Db::getInstance()->execute($sql);
	}

	private function createAmazingNoticeBoxLangTable()
	{
		$sql = 'CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'amazing_notice_box_lang(
				`id_amazing_notice_box` int(11) NOT NULL,
				`id_lang` INT UNSIGNED NOT NULL,
				`content` TEXT NULL,
				`barcontent` TEXT NULL,
				`barbut` TEXT NULL,
				`barlink` TEXT NULL,
				PRIMARY KEY (`id_amazing_notice_box`,`id_lang`)
			) ENGINE='._MYSQL_ENGINE_.' default CHARSET=utf8;';
		return Db::getInstance()->execute($sql);
	}
	private function createAmazingNoticeBoxPageTable()
	{
		$sql = 'CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'amazing_notice_box_page(
				`id_page` int(4) NOT NULL AUTO_INCREMENT,
				`page` varchar(50) NOT NULL,
				PRIMARY KEY (`id_page`)
			)ENGINE='._MYSQL_ENGINE_.' default CHARSET=utf8;';
		return Db::getInstance()->execute($sql);
	}
	private function insertDefaultAmazingNoticeBoxPage()
	{
		$sql = 'INSERT INTO '._DB_PREFIX_.'amazing_notice_box_page (`id_page`,`page`)
			   VALUES(1,"index"),(2,"product"),(3,"new-products"),(4,"category"),(5,"order"),(6,"manufacturer"),
			   (7,"supplier"),(8,"cms"),(9,"search"),(10,"stores"),(11,"pricesdrop"),(12,"bestsales"),(13,"contact"),
			   (14,"sitemap"),(15,"myaccount"),(16,"discount"),(17,"addresses"),(18,"identity"),(19,"payment"),
			   (20,"orderconfirmation"),(21,"history"),(22,"orderslip"),(23,"account"),(24,"authentication");';
		return Db::getInstance()->execute($sql);
	}

	private function createTables()
	{
		return (
			$this->createAmazingNoticeBoxTable() && $this->createAmazingNoticeBoxLangTable() && $this->createAmazingNoticeBoxPageTable() && $this->insertDefaultAmazingNoticeBoxPage());
	}

	private function dropTables()
	{
		$sql = 'DROP TABLE
			`'._DB_PREFIX_.'amazing_notice_box`,
			`'._DB_PREFIX_.'amazing_notice_box_lang`,
			`'._DB_PREFIX_.'amazing_notice_box_page`';

		return Db::getInstance()->execute($sql);
	}

	private function installModuleTab($tab_class, $tab_name)
	{
		$languages = Language::getLanguages();
		$id_tab = Tab::getIdFromClassName('AdminPrestaSOO');
		if ($id_tab == false)
		{
			if (version_compare('1.6', _PS_VERSION_) <= 0)
			copy(_PS_MODULE_DIR_.$this->name.'/views/img/adminprestasoo.png', _PS_ADMIN_DIR_.'/themes/default/img/adminprestasoo.png');
			else
			copy(_PS_MODULE_DIR_.$this->name.'/views/img/AdminPrestaSOO.gif', _PS_IMG_DIR_.'t/AdminPrestaSOO.gif');
			if (version_compare('1.5', _PS_VERSION_) < 0)
				copy(_PS_MODULE_DIR_.$this->name.'/views/css/admin-theme.css', _PS_ADMIN_DIR_.'/themes/default/css/admin-theme.css');
			$parent_tab = new Tab();
			foreach ($languages as $language)
				$parent_tab->name[$language['id_lang']] = 'PrestaSOO';
			$parent_tab->class_name = 'AdminPrestaSOO';
			$parent_tab->id_parent = 0;
			$parent_tab->module = '';
			$parent_tab->add();
		}
		else
			$parent_tab = new Tab($id_tab);
		$tab = new Tab();
		foreach ($languages as $language)
			$tab->name[$language['id_lang']] = $tab_name;
		$tab->class_name = $tab_class;
		$tab->module = $this->name;
		$tab->id_parent = $parent_tab->id;
		$tab->add();
		return true;
	}
	private function uninstallModuleTab($tab_class)
	{
		$id_tab = Tab::getIdFromClassName($tab_class);
		if ($id_tab)
		{
			$tab = new Tab($id_tab);
			$parent_id = $tab->id_parent;
			$tab->delete();
			$number = Tab::getNbTabs($parent_id);
			if ($number == 0)
			{
				$parent_tab = new Tab($parent_id);
				$parent_tab->delete();
			}
			return true;
		}
		return false;
	}
	public function addHooks()
	{
		$hooks = $this->hooks;

		foreach ($hooks as $hook)
			if (!$this->registerHook($hook)) return false;
		return true;
	}

	public function removeHooks()
	{
		$hooks = $this->hooks;
		$mod = Db::getInstance()->getRow('SELECT `id_module` FROM `'._DB_PREFIX_.'module` WHERE `name`="'.pSQL($this->name).'"');
		Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'hook_module` WHERE `id_module`="'.($mod['id_module']).'"');
	}

	public function initToolbar()
	{
		$current_index = AdminController::$currentIndex;
		$token = Tools::getAdminTokenLite('AdminAmazingNoticeBox');

		$back = Tools::safeOutput(Tools::getValue('back', ''));
		if (!isset($back) || empty($back))
			$back = $current_index.'&amp;configure='.$this->name.'&token='.$token;

		switch ($this->_display)
		{
			case 'add':
				$this->toolbar_btn['cancel'] = array(
					'href' => $back,
					'desc' => $this->l('Cancel')
				);
				break;
			case 'edit':
				$this->toolbar_btn['cancel'] = array(
					'href' => $back,
					'desc' => $this->l('Cancel')
				);
				break;
			case 'view':
				// Default cancel button - like old back link
				$back = Tools::safeOutput(Tools::getValue('back', ''));
				if (empty($back))
					$back = self::$currentIndex.'&token='.$token;
				if (!Validate::isCleanHtml($back))
					die(Tools::displayError());
				if (!$this->lite_display)
					$this->toolbar_btn['back'] = array(
						'href' => $back,
						'desc' => $this->l('Back to list')
					);
				break;

			default:
				break;
		}
		$this->toolbar_btn['save'] = array(
			'href' => '#',
			'desc' => $this->l('Save')
		);
		return $this->toolbar_btn;
	}

	private function initForm()
	{
		$helper = new HelperForm();

		$helper->module = $this;
		$helper->name_controller = 'amazingnoticebox';
		$helper->identifier = $this->identifier;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->languages = $this->context->controller->_languages;
		$helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
		$helper->default_form_language = $this->context->controller->default_form_language;
		$helper->allow_employee_form_lang = $this->context->controller->allow_employee_form_lang;
		$helper->toolbar_scroll = true;
		$helper->toolbar_btn = $this->initToolbar();

		return $helper;
	}

	public function hookDisplayHeader()
	{
		$this->context->controller->addCSS($this->_path.'views/css/amazingnoticebox.css', 'all');
		$this->context->controller->addJS($this->_path.'views/js/cookies.js');
		$this->context->controller->addJQueryPlugin('fancybox');
	}
	public function hookDisplayTop()
	{
		if (!$this->active)
			return;
		require_once(_PS_MODULE_DIR_.'amazingnoticebox/AmazingNoticeBoxModule.php');
			if (version_compare(_PS_VERSION_, '1.5.4', '>=') == true)
			{
				$curpage = Tools::getValue('controller');
				if ($curpage == 'newproducts') $curpage = 'new-products';
			}
			else
				$curpage = $this->context->controller->php_self;
		$curdate = date('Y-m-d');
		$nb = new AmazingNoticeBoxModule();
		$notice = $nb->getBox('`pages` LIKE "%'.$curpage.'%" AND `active`=1 AND `deleted`=0 AND `dateto` >= "'.$curdate.'" AND `datefrom` <= "'.$curdate.'"', false);
		$has_notice = true;
		//check notice is exist in curpage or not
		if (count($notice))
		{
			$tpages = explode(',', $notice[0]['pages']);
			if (in_array($curpage, $tpages)) $has_notice = true;
		}
		$this->smarty->assign(array(
			'nbpath' => $this->_path,
			'curpage' => $curpage,
			'notices' => $notice,
			'hasNotice' => $has_notice
		));
		return $this->display(__FILE__, 'display_top.tpl');
	}
}