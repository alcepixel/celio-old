<?php
/**
* 20012-2014 Vietmoonlight
*
* NOTICE OF LICENSE
*
* This is a commercial license
* Do not allow to re-sales, edit without permission from Vietmoonlight.
* International Registered Trademark & Property of Vietmoonlight
*
* @author    Yen Truong <truthblue82@gmail.com>
* @copyright Vietmoonlight.com
* @license   Commercial License. All right reserved
*/

class AmazingNoticeBoxClass extends ObjectModel
{
	public $id;
	public $content;
	public $barcontent;
	public $barbut;
	public $barlink;
	public $datefrom;
	public $dateto;
	public $expire;
	public $type;
	public $position;
	public $bg_color;
	public $text_color;
	public $border_color;
	public $pages;
	public $active;
	public $deleted;
	public $name = 'amazingnoticebox';
	public static $definition = array(
	'table' => 'amazing_notice_box',
	'primary' => 'id_amazing_notice_box',
	'multilang' => true,
	'fields' => array(
			'datefrom' =>                    array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'required' => true),
			'dateto' =>		                 array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'required' => true),
			'content' =>                     array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString'),
			'barcontent' =>                  array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString'),
			'barbut' =>                  	 array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString'),
			'barlink' =>                     array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isUrl'),
			'expire' =>                      array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
			'type' =>                        array('type' => self::TYPE_STRING, 'validate' => 'isString', 'required' => true),
			'position' =>                    array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'bg_color' =>                    array('type' => self::TYPE_STRING, 'validate' => 'isString', 'required' => true),
			'text_color' =>                  array('type' => self::TYPE_STRING, 'validate' => 'isString', 'required' => true),
			'border_color' =>                array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
			'pages' =>                       array('type' => self::TYPE_STRING, 'validate' => 'isString', 'required' => true),
			'active' =>                      array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
			'deleted' =>                     array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false)
		),
		);

	public function __construct($id = null, $id_lang = null)
	{
		parent::__construct($id, $id_lang);
	}

	public function add($autodate = true, $null_values = false)
	{
		$ret = parent::add($autodate, $null_values);
		return $ret;
	}

	public function update($null_values = false)
	{
		return parent::update(true);
	}

	/*
	 * change status to active/inactive
	 */
	public function toggleStatus()
	{
		return Db::getInstance()->update('amazing_notice_box', array('active' => !(int)$this->active), '`id_amazing_notice_box`='.(int)$this->id);
	}

	/*
	 * Delete object
	 */
	public function delete()
	{
		if (!Validate::isTableOrIdentifier($this->identifier) || !Validate::isTableOrIdentifier($this->table))
		die(Tools::displayError());
		$this->clearCache();
		//delete lang table first
		$sql = 'DELETE FROM `'._DB_PREFIX_.$this->table.'_lang` WHERE `'.$this->identifier.'`='.(int)$this->id;
		Db::getInstance()->execute($sql);
		$sql = 'DELETE FROM `'._DB_PREFIX_.$this->table.'` WHERE `'.$this->identifier.'`='.(int)$this->id;
		return Db::getInstance()->execute($sql);
	}

	/*
	 * Delete several objects from database
	 *
	 * return boolean Deletion result
	 */
	public function deleteSelection($selection)
	{
		if (!is_array($selection) || !Validate::isTableOrIdentifier($this->identifier) || !Validate::isTableOrIdentifier($this->table))
			die(Tools::displayError());
		$result = true;
		foreach ($selection as $id)
		{
			$this->id = (int)$id;
			$result &= $this->delete();
		}
		return $result;
	}

	/**
	 * $conditions: array condition
	 * $flag: true if condition is array, false if condition is where clause
	 * for frontend using
	 */
	public function getBox($conditions = array(), $flag = true)
	{
		$id_lang = (int)Context::getContext()->language->id;
		if (empty($id_lang))
			$id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
		if ($flag)
		{
			$where = '';
			foreach ($conditions as $k => $v)
				$where .= '`'.$k.'`='.pSQL($v).' AND ';
			$where = Tools::substr($where, 0, Tools::strlen($where) - 4);
		}
		else $where = $conditions;
		$sql = 'SELECT nb.*,nbl.content as content,nbl.barcontent as barcontent,nbl.barbut as barbut,nbl.barlink as barlink FROM `'._DB_PREFIX_.'amazing_notice_box` nb
				LEFT JOIN `'._DB_PREFIX_.'amazing_notice_box_lang` nbl
				ON (nb.`id_amazing_notice_box`=nbl.`id_amazing_notice_box` AND nbl.`id_lang` = '.(int)$id_lang.')  WHERE '.$where;
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($sql);
	}

}
