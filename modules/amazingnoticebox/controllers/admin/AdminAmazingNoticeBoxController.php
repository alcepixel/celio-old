<?php
/**
* 20012-2014 Vietmoonlight
*
* NOTICE OF LICENSE
*
* This is a commercial license
* Do not allow to re-sales, edit without permission from Vietmoonlight.
* International Registered Trademark & Property of Vietmoonlight
*
* @author    Yen Truong <truthblue82@gmail.com>
* @copyright Vietmoonlight.com
* @license   Commercial License. All right reserved
*/

include_once _PS_MODULE_DIR_.'amazingnoticebox/classes/AmazingNoticeBoxClass.php';

class AdminAmazingNoticeBoxController extends AdminController
{
	protected $delete_mode;

	protected $default_order_by = 'id_amazing_notice_box';
	protected $default_order_way = 'ASC';
	protected $can_add_ambox = true;

	public function __construct()
	{
		$this->table = 'amazing_notice_box';
		$this->className = 'AmazingNoticeBoxClass';
		$this->module = 'amazingnoticebox';
		$this->lang = true;
		$this->deleted = false;
		$this->required_database = true;
		$this->bootstrap = true;
		$this->addRowAction('edit');
		$this->addRowAction('delete');

		$this->bulk_actions = array('delete' => array('text' => $this->l('Delete selected'), 'confirm' => $this->l('Delete selected items?')));

		$this->context = Context::getContext();

		$this->default_form_language = $this->context->language->id;

		$type = array('Box' => $this->l('Box'), 'Bar' => $this->l('Bar'));
		$position = array('Top' => $this->l('Top'), 'Center' => $this->l('Center'), 'Bottom' => $this->l('Bottom'));

		$res = Db::getInstance()->ExecuteS('SELECT * FROM '._DB_PREFIX_.'amazing_notice_box_page ORDER BY id_page ASC');
		$pages = array();
		$pages2 = array();
		foreach ($res as $i => $p)
		{
			if (!in_array($p['page'], $pages))
			{
				$pages[$p['page']] = $this->l($p['page']);
				$pages2[$i]['id'] = $p['page'];
				$pages2[$i]['name'] = $this->l($p['page']);
			}
		}
		//for active
		$act = array();
		$act_icons = array('default' => 'unknown.gif');
		$act[] = $this->l('Hide');
		$act_icons[] = 'disabled.gif';
		$act[] = $this->l('Show');
		$act_icons[] = 'enabled.gif';

		$this->listpages = $pages2;
		$this->defaultbg = '#fff';
		$this->defaulttext = '#000';
		$this->defaultborder = '#10b0e4';

		$this->fields_list = array(
			'id_amazing_notice_box' => array(
				'title' => $this->l('ID'),
				'align' => 'center',
				'filter_key' => 'id_amazing_notice_box',
				'width' => 20
			),
			'datefrom' => array(
				'title' => $this->l('Date From'),
				'align' => 'right',
				'type' => 'date',
				'filter_key' => 'datefrom',
				'width' => 100
			),
			'dateto' => array(
			'title' => $this->l('Date To'),
			'align' => 'right',
				'type' => 'date',
				'filter_key' => 'dateto',
				'width' => 100
			),
			'type' => array(
				'title' => $this->l('Type'),
				'width' => 60,
				'align' => 'center',
				'orderby' => false,
				'type' => 'select',
				'list' => $type,
				'filter_key' => 'type'
			),
			'position' => array(
				'title' => $this->l('Position'),
				'width' => 60,
				'align' => 'center',
				'orderby' => false,
				'type' => 'select',
				'list' => $position,
				'filter_key' => 'position'
			),
			'bg_color' => array(
				'title' => $this->l('Background Color'),
				'width' => 80,
				'align' => 'center',
				'orderby' => false,
				'search' => false
			),
			'text_color' => array(
				'title' => $this->l('Text Color'),
				'width' => 80,
				'align' => 'center',
				'orderby' => false,
				'search' => false
			),
			'border_color' => array(
				'title' => $this->l('Border'),
				'width' => 60,
				'align' => 'center',
				'active' => 'status',
				'type' => 'bool',
				//'list' => $act,
				'filter_key' => 'active',
				'icon' => $act_icons
			),
			'pages' => array(
				'title' => $this->l('Pages'),
				'width' => 150,
				'align' => 'center',
				'type' => 'select',
				'list' => $pages,
				'filter_key' => 'pages'
			),
			'active' => array(
				'title' => $this->l('Active'),
				'width' => 60,
				'align' => 'center',
				'active' => 'status',
				'type' => 'bool',
				//'list' => $act,
				'filter_key' => 'active',
				'icon' => $act_icons
			)
		);

		parent::__construct();

		// Check if we can add an amazingnoticebox
		if (Shop::isFeatureActive() && (Shop::getContext() == Shop::CONTEXT_ALL || Shop::getContext() == Shop::CONTEXT_GROUP))
			$this->can_add_ambox = false;
	}

	public function initToolbar()
	{
		parent::initToolbar();
		if (!$this->can_add_ambox)
			unset($this->toolbar_btn['new']);
	}

	public function postProcess()
	{
		if (!$this->can_add_ambox && $this->display == 'add')
			$this->redirect_after = $this->context->link->getAdminLink('AdminAmazingNoticeBox');

		parent::postProcess();
	}

	public function initContent()
	{
		if ($this->action == 'select_delete')
			$this->context->smarty->assign(array(
				'delete_form' => true,
				'url_delete' => htmlentities($_SERVER['REQUEST_URI']),
				'boxes' => $this->boxes,
			));

		if (!$this->can_add_ambox && !$this->display)
			$this->informations[] = $this->l('You have to select a shop if you want to create an amazing notice box');

		parent::initContent();
	}

	public function processAdd()
	{
		$this->validateRules();
		$datefrom = Tools::getValue('datefrom');
		$dateto = Tools::getValue('dateto');
		$languages = Language::getLanguages(false);
		if (!count($this->errors))
		{
			//check if exist a box from date to date
			$nb = new AmazingNoticeBoxClass();
			$temp = $nb->getBox('((`datefrom` <= "'.$datefrom.'" AND `dateto` >= "'.$datefrom.'") OR (`dateto` >= "'.$dateto.'" AND `datefrom` <= "'.$dateto.'") OR (`datefrom` BETWEEN "'.$datefrom.'" AND "'.$dateto.'") OR (`dateto` BETWEEN "'.$datefrom.'" AND "'.$dateto.'")) AND `deleted`=0 AND `active`=1 AND `type`="'.Tools::getValue('type').'" ', false);
			$pages = explode(',', Tools::getValue('hdn_shownpages'));
			$flag = true;
			foreach ($temp as $t)
			{
				$tpages = explode(',', $t['pages']);
				foreach ($pages as $p)
				{
					if (in_array($p, $tpages))
					{
						$flag = false;
						break 2;
					}
				}
			}
			if (!$flag)
				$this->errors[] = Tools::displayError('An exists '.Tools::getValue('type').' in the same page from: '.$datefrom.' to '.$dateto);
			if (!count($this->errors))
			{
				//here
				$object = new $this->className(true);
				$object->active = Tools::getValue('active');
				$object->datefrom = $datefrom;
				$object->dateto = $dateto;
				$rules = call_user_func(array(get_class($object), 'getValidationRules'), get_class($object));
				$rules['validateLang']['tags'] = 'isCleanHTML';
				if (count($rules['validateLang']))
				{
					foreach ($languages as $language)
					{
						foreach (array_keys($rules['validateLang']) as $field)
						{
							if (Tools::getValue($field.'_'.(int)$language['id_lang']))
								$object->{$field}[(int)$language['id_lang']] = Tools::getValue($field.'_'.(int)$language['id_lang']);
						}
					}
				}
				$object->expire = Tools::getValue('expire');
				$object->type = Tools::getValue('type');
				if ($object->type == 'Box')
					$object->position = 'Center';
				else $object->position = Tools::getValue('position');
				$object->bg_color = Tools::getValue('bg_color');
				$object->text_color = Tools::getValue('text_color');
				$object->border_color = Tools::getValue('border_color');
				$object->pages = Tools::getValue('hdn_shownpages');
				if (!$object->add())
					$this->errors[] = Tools::displayError('An error occurred while creating object.').' <b>'.$this->table.' ('.Db::getInstance()->getMsgError().')</b>';
				elseif ((Tools::getValue($this->identifier) == $object->id ) && !count($this->errors))
					Tools::redirectAdmin(self::$currentIndex.'&conf=3&token='.Tools::getAdminTokenLite('AdminAmazingNoticeBox'));
			}
			if (count($this->errors))
			{
				$this->display = 'edit';
				return false;
			}
			return $object;
		}
	}

	public function processUpdate()
	{
		$this->validateRules();
		if (!count($this->errors))
		{
			$datefrom = Tools::getValue('datefrom');
			$dateto = Tools::getValue('dateto');
			$languages = Language::getLanguages(false);
			$id = (int)Tools::getValue($this->identifier);
			if (isset($id) && !empty($id))
			{
				if ($this->tabAccess['edit'] !== '1')
					$this->_errors[] = Tools::displayError('You do not have permission to edit here.');
				else
				{
					$object = new $this->className($id);
					if (Validate::isLoadedObject($object))
					{
						//check if exist a box from date to date
						$nb = new AmazingNoticeBoxClass();
						$temp = $nb->getBox('((`datefrom` <= "'.$datefrom.'" AND `dateto` >= "'.$datefrom.'") OR (`dateto` >= "'.$dateto.'" AND `datefrom` <= "'.$dateto.'") OR (`datefrom` BETWEEN "'.$datefrom.'" AND "'.$dateto.'") OR (`dateto` BETWEEN "'.$datefrom.'" AND "'.$dateto.'")) AND `deleted`=0 AND `active`=1 AND `type`="'.Tools::getValue('type').'" ', false);
						$pages = explode(',', Tools::getValue('hdn_shownpages'));
						$flag = true;
						foreach ($temp as $t)
						{
							if ($t['id_amazing_notice_box'] != $id)
							{
								$tpages = explode(',', $t['pages']);
								foreach ($pages as $p)
								{
									if (in_array($p, $tpages))
									{
										$flag = false;
										break 2;
									}
								}
							}
						}
						if (!$flag)
							$this->errors[] = Tools::displayError('An exists '.Tools::getValue('type').' in the same pages from: '.$datefrom.' to '.$dateto);
						if (!count($this->errors))
						{
							$object->datefrom = $datefrom;
							$object->dateto = $dateto;
							$object->expire = Tools::getValue('expire');
							$object->active = Tools::getValue('active');
							$object->border_color = Tools::getValue('border_color');
							$rules = call_user_func(array(get_class($object), 'getValidationRules'), get_class($object));
							$rules['validateLang']['tags'] = 'isCleanHTML';
							if (count($rules['validateLang']))
							{
								foreach ($languages as $language)
								{
									foreach (array_keys($rules['validateLang']) as $field)
									{
										if (Tools::getValue($field.'_'.(int)$language['id_lang']))
											$object->{$field}[(int)$language['id_lang']] = Tools::getValue($field.'_'.(int)$language['id_lang']);
									}
								}
							}
							if (Tools::getValue('type')) $object->type = Tools::getValue('type');
							if ($object->type == 'Box')
								$object->position = 'Center';
							else if (Tools::getValue('position')) $object->position = Tools::getValue('position');
							if (Tools::getValue('bg_color')) $object->bg_color = Tools::getValue('bg_color');
							if (Tools::getValue('text_color')) $object->text_color = Tools::getValue('text_color');
							$object->pages = Tools::getValue('hdn_shownpages');
							$object->update();
							Tools::redirectAdmin(self::$currentIndex.'&conf=4&token='.Tools::getAdminTokenLite('AdminAmazingNoticeBox'));
						}
					}
					else
					{
						$this->errors[] = Tools::displayError('An error occurred while loading the object.').'
						<b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
					}
				}
			}
		}
		$this->errors = array_unique($this->errors);
		if (!empty($this->errors))
		{
			$this->display = 'edit';
			return false;
		}
		if (isset($object))
			return $object;
		return;
	}

	/*
	 * For view page
	 */
	public function renderView()
	{
		if (!($amb = $this->loadObject()))
			return;

		$helper = new HelperView($this);
		$helper->module = new AmazingNoticeBoxClass();
		$this->setHelperDisplay($helper);

		$this->context->amb = $amb;
		$this->tpl_view_vars = array(
			'obj' => $amb,
			'id_lang' => $this->default_form_language,
			'show_toolbar' => true
		);
		$helper->tpl_vars = $this->tpl_view_vars;
		if (!is_null($this->base_tpl_view))
			$helper->base_tpl = $this->base_tpl_view;
		$view = $helper->generateView();

		return $view;
	}

	/**
	 * form for add amazingnoticebox
	 */
	public function renderForm()
	{
		if (!($obj = $this->loadObject(true)))
			return;

		$iso = $this->context->language->iso_code;

		$this->fields_form = array(
			'tinymce' => true,
			'iso' => file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en',
			'ad' => dirname($_SERVER['PHP_SELF']),
			'legend' => array(
				'title' => $this->l('Amazing Notice Box Parameters'),
				'image' => '../img/admin/cog.gif'
			),
			'input' => array(
				array(
					'type' => 'radio',
					'label' => $this->l('Active:'),
					'name' => 'active',
					'required' => false,
					'class' => 't',
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'active_on',
							'value' => 1,
							'label' => $this->l('Show')
						),
						array(
							'id' => 'active_off',
							'value' => 0,
							'label' => $this->l('Hide')
						)
					)
				),
				array(
					'type' => 'date',
					'label' => $this->l('Date From:'),
					'name' => 'datefrom',
					'size' => 20,
					'required' => true,
				),
				array(
					'type' => 'date',
					'label' => $this->l('Date To:'),
					'name' => 'dateto',
					'size' => 20,
					'required' => true,
				),
				array(
					'type' => 'select',
					'name' => 'type',
					'label' => $this->l('Type:'),
					'options' => array(
						'query' => array(array('id'=>'Box','name' =>$this->l('Box')),array('id'=>'Bar','name'=>$this->l('Bar'))),
						'id' => 'id',
						'name' => 'name'
					),
					'onchange' => 'typeChange();',
				),
				array(
					'type' => 'textarea',
					'name' => 'content',
					'label' => $this->l('Content:'),
					'lang' => true,
					'autoload_rte' => true,
					'rows' => 10,
					'cols' => 100,
					'hint' => $this->l('Invalid characters:').' <>;=#{}',
				),
				array(
					'type' => 'text',
					'name' => 'barcontent',
					'lang' => true,
					'label' => $this->l('Text Message'),
					'size' => 20,
				),
				array(
					'type' => 'text',
					'name' => 'barbut',
					'lang' => true,
					'label' => $this->l('Text button'),
					'size' => 20,
				),
				array(
					'type' => 'text',
					'name' => 'barlink',
					'lang' => true,
					'label' => $this->l('Button link'),
					'size' => 20,
				),
				array(
					'type' => 'text',
					'name' => 'expire',
					'label' => $this->l('Expired Time:'),
					'size' => 20,
					'desc' => $this->l('Time in seconds'),
				),
				array(
					'type' => 'select',
					'name' => 'position',
					'label' => $this->l('Position:'),
					'options' => array(
						'query' => array(array('id'=>'Top','name' =>$this->l('Top')),array('id'=>'Center','name'=>$this->l('Center')),array('id'=>'Bottom','name'=>$this->l('Bottom'))),
						'id' => 'id',
						'name' => 'name'
					),
				),
				array(
					'type' => 'color',
					'name' => 'bg_color',
					'label' => $this->l('Background Color:'),
					'size' => 20,
				),
				array(
					'type' => 'color',
					'name' => 'text_color',
					'label' => $this->l('Text Color:'),
					'size' => 20,
				),
				array(
					'type' => 'radio',
					'label' => $this->l('Border or not:'),
					'name' => 'border_color',
					'required' => false,
					'class' => 't',
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'border_on',
							'value' => 1,
							'label' => $this->l('Show')
						),
						array(
							'id' => 'border_off',
							'value' => 0,
							'label' => $this->l('Hide')
						)
					)
				),
				array(
					'type' => 'select',
					'name' => 'pages',
					'label' => $this->l('Show At Pages:'),
					'id' => 'shownpages',
					'size' => 10,
					'multiple' => 'multiple',
					'width' => '200px;',
					'options' => array(
						'query' => $this->listpages,
						'id' => 'id',
						'name' => 'name'
					),
				),
				array(
					'type' => 'hidden',
					'name' => 'hdn_shownpages',
					'id' => 'hdn_shownpages',
				),
			),
			'submit' => array(
				'title' => $this->l('Save'),
				'class' => 'button'
			)
		);

		$pages = explode(',', $this->getFieldValue($obj, 'pages'));
		$parr = array();
		foreach ($this->listpages as $p)
		{
			if (empty($pages[0]) && $p['id'] == 'index') $parr['index'] = 'index';
			if (in_array($p['id'], $pages)) $parr[$p['name']] = $p['id'];
		}
		$this->fields_value = array(
			'active' => $this->getFieldValue($obj, 'active') ? $this->getFieldValue($obj, 'active') : 1,
			'datefrom' => $this->getFieldValue($obj, 'datefrom') ? $this->getFieldValue($obj, 'datefrom') : '',
			'dateto' => $this->getFieldValue($obj, 'dateto') ? $this->getFieldValue($obj, 'dateto') : '',
			'type' => $this->getFieldValue($obj, 'type') ? $this->getFieldValue($obj, 'type') : 'Box',
			'expire' => $this->getFieldValue($obj, 'expire') ? $this->getFieldValue($obj, 'expire') : 5,
			'position' => $this->getFieldValue($obj, 'position') ? $this->getFieldValue($obj, 'position') : 'Center',
			'bg_color' => htmlentities($this->getFieldValue($obj, 'bg_color'), ENT_COMPAT, 'UTF-8') == '' ? $this->defaultbg:htmlentities($this->getFieldValue($obj, 'bg_color'), ENT_COMPAT, 'UTF-8'),
			'content' => $this->getFieldValue($obj, 'content'),
			'barcontent' => $this->getFieldValue($obj, 'barcontent'),
			'barbut' => $this->getFieldValue($obj, 'barbut'),
			'barlink' => $this->getFieldValue($obj, 'barlink'),
			'text_color' => htmlentities($this->getFieldValue($obj, 'text_color'), ENT_COMPAT, 'UTF-8') == '' ? $this->defaulttext:htmlentities($this->getFieldValue($obj, 'text_color'), ENT_COMPAT, 'UTF-8'),
			'border_color' => $this->getFieldValue($obj, 'border_color') ? $this->getFieldValue($obj, 'border_color') : 0,
			'pages' => $parr,
			'hdn_shownpages' => empty($pages[0]) ? 'index' : $this->getFieldValue($obj, 'pages')
		);
		return parent::renderForm();
	}

	/**
	 * Add css and js for backend of this module
	 */
	public function setMedia()
	{
		parent::setMedia();
		if (version_compare(_PS_VERSION_, '1.6', '>=') === false)
			$this->addCSS(_MODULE_DIR_.'amazingnoticebox/views/css/amazingnoticebox.css');
		$this->addJS(_MODULE_DIR_.'amazingnoticebox/views/js/admin_amazingnoticebox.js');
	}

	protected function _childValidation()
	{
		$id_lang = (int)Context::getContext()->language->id;
		if (!($obj = $this->loadObject(true)))
			return false;
		$datefrom = $this->getFieldValue($obj, 'datefrom');
		$dateto = $this->getFieldValue($obj, 'dateto');
		//only check validate for current language
		$content = $this->getFieldValue($obj, 'content_'.$id_lang);
		$barcontent = $this->getFieldValue($obj, 'barcontent_'.$id_lang);
		$barbut = $this->getFieldValue($obj, 'barbut_'.$id_lang);
		$barlink = $this->getFieldValue($obj, 'barlink_'.$id_lang);
		$pages = $this->getFieldValue($obj, 'hdn_shownpages');
		$dto = new DateTime($dateto);
		$dfrom = new DateTime($datefrom);
		if (!Validate::isDateFormat($datefrom) && !Validate::isDate($datefrom))
		$this->errors[] = Tools::displayError('Invalid date from');
		elseif (!Validate::isDateFormat($dateto) && !Validate::isDate($dateto))
		$this->errors[] = Tools::displayError('Invalid date to');
		else if ($dto->format('U') <= $dfrom->format('U'))
			$this->errors[] = Tools::displayError('Date to is less than date from');
		else if ((empty($content)) && (empty($barcontent)))
			$this->errors[] = Tools::displayError('Content is empty');
		else if (empty($pages))
			$this->errors[] = Tools::displayError('Please select pages to display message.');
	}
}
