<?php
/**
* 20012-2014 Vietmoonlight
*
* NOTICE OF LICENSE
*
* This is a commercial license
* Do not allow to re-sales, edit without permission from Vietmoonlight.
* International Registered Trademark & Property of Vietmoonlight
*
* @author    thuyetkhach <sales@vietmoonlight.com>
* @copyright Vietmoonlight.com
* @license   Commercial License. All right reserved
*/

header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');

header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Pragma: no-cache');

header('Location: ../');
exit;