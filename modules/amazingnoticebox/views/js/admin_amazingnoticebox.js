/**
* 20012-2014 Vietmoonlight
*
* NOTICE OF LICENSE
*
* This is a commercial license
* Do not allow to re-sales, edit without permission from Vietmoonlight.
* International Registered Trademark & Property of Vietmoonlight
*
* @author    Yen Truong <truthblue82@gmail.com>
* @copyright Vietmoonlight.com
* @license   Commercial License. All right reserved
*/

$(document).ready(function () {
	$('select').live('click', function () {
		var el = $(this);
		var id = el.attr('id');
		var hel = $('#hdn_' + id);
		var str = '';
		$('#' + id + ' :selected').each(function (i, selected) {
			str += $(selected).val() + ','
		});
		str = str.substr(0, str.length - 1);
		hel.val(str);
	});
	typeChange();
});
function typeChange() {
	var type = $('#type').val();
	if (type == 'Box') {
		$("#position option[value=Center]").removeAttr('disabled');
		$('#position').val('Center');
		$('select[id=position]').attr("disabled", true);
		$('select[id=position]').parent().prev().hide();
		$('select[id=position]').parent().hide();
		$('#expire').parent().show();
		$('span.sootemp').hide();
		$('.mce-menubar').show();
		$('.mce-toolbar-grp').show();
		$('#expire').parent().prev().show();
		$(".form-group span.label-tooltip" ).show();
		$('#expire').parent().prev().parent().prev().hide();
		$('#expire').parent().prev().parent().prev().prev().hide();
		$('#expire').parent().prev().parent().prev().prev().prev().hide();
		$('.mce-tinymce').show();
	} else {
		$('select[id=position]').attr("disabled", false);
		$('#position').val('Top');
		$(".form-group span.label-tooltip" ).hide();
		$('.mce-tinymce').hide();
		$('select[id=position]').parent().show();
		$('select[id=position]').parent().prev().show();
		$("#position option[value=Center]").attr('disabled', 'disabled');
		$('#expire').parent().prev().parent().prev().show();
		$('#expire').parent().prev().parent().prev().prev().show();
		$('#expire').parent().prev().parent().prev().prev().prev().show();
		$('#expire').parent().hide();
		$('#expire').parent().prev().hide();
	}
}