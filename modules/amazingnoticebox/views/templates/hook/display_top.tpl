{*
* 20012-2015 PrestaSOO
*
* NOTICE OF LICENSE
*
* This is a commercial license
* Do not allow to re-sales, edit without permission from PrestaSOO.
* International Registered Trademark & Property of PrestaSOO
*
* @author    thuyetkhach <addons@prestasoo.com>
* @copyright PrestaSOO.com
* @license   Commercial License. All right reserved
*}
{if $hasNotice}
{foreach from=$notices item=notice name=boxboxbox}
    {if $notice.type eq "Bar"}
    <div class="{if $notice.position eq "Top"}beng{else}pheng{/if}">
        <div id="amazing_notice_box" class="icon icon-chevron-down amazingclose" alt="{l s='Show' mod='amazingnoticebox'}">
        </div>
    </div>
    {/if}
    {if $notice.type eq "Box"}
        <div id="notice_container">
            <a id="inline" href="#notice-box"></a>
            <div id="notice-box" style="background-color:{$notice.bg_color|escape:html};color:{$notice.text_color|escape:html}">
            {$notice.content|escape:'UTF-8'}
            </div>
        </div>
    {/if}
    <script type="text/javascript">
        {literal}
            $(document).ready(function(){
        {/literal}
                {if $notice.type eq "Box"}
                    var msec  = {$notice.expire|escape:intval}*1000;
                    var path = "{$curpage|escape:'htmlall'}";
                    var pborder = "{$notice.border_color|escape:html}";
        {literal}
                if(Get_Cookie('noticebox_'+path) == null)
                {
                        $('a#inline').fancybox({/literal}{if $notice.border_color eq 0}{literal}{padding:0, margin:0, tpl: {
        closeBtn: '<a class="fancybox-item fancybox-close" href="javascript:;" onclick="Set_Cookie(\'noticebox_{/literal}{$curpage}{literal}\',\'Amazing notice box\',1,\'/\',\'\',\'\')" title="Close"></a>'} }{/literal}{else}{literal}{tpl: { closeBtn: '<a class="fancybox-item fancybox-close" href="javascript:;" onclick="Set_Cookie(\'noticebox_{/literal}{$curpage}{literal}\',\'Amazing notice box by Vietmoonlight\',1,\'/\',\'\',\'\')" title="Close"></a>'} }{/literal}{/if}{literal}).trigger('click');
                        if(msec != 0)
                            setTimeout("$.fancybox.close()", msec);
                        $('.fancybox-skin').css({'background': 'none repeat scroll 0 0'+pborder});
                }
        {/literal}
                {else}
                    {if $notice.position eq "Top"}
                        var pos = 'position: fixed;top:0px;';
                    {else}
                        var pos = 'position: fixed;bottom:0px';
                    {/if}
                    var bgcolor = "{$notice.bg_color|escape:html}";
                    var tcolor = "{$notice.text_color|escape:html}";
        {literal}
                    var container = "";
                    container +=     "<div id='notice-bar' style='background-color:"+bgcolor+";color:"+tcolor+";"+pos+"'>";
        {/literal}
                    container +=     "{$notice.barcontent|escape:'html'} <a class='barbut' href='{$notice.barlink|escape:'html'}'>{$notice.barbut|escape:'html'}</a>";
                    container +=     "<div class='{if $notice.position eq 'Top'}beng{else}pheng{/if}'>";
        {literal}
                    container +=     "<div id='amazing_notice_box' class='icon icon-chevron-up amazingopen'>";
                    container +=     "</div></div>";
                    container +=     "</div>";
                    $("body").append(container);
                    $("body").css("position", "relative");
                    var barheight = $("#notice-bar").outerHeight();
                    $("body").animate({top: barheight}, 800, 'linear');
					$(".amazingclose").css("background-color", bgcolor);
					$(".amazingopen").click(function(){
						$('#notice-bar').slideToggle("slow");
						$('#amazing_notice_box').attr("class", "icon icon-chevron-down amazingclose");
						$("body").animate({top: 0}, 320, 'linear');
						})
					$(".amazingclose").click(function(){
						$('#notice-bar').slideToggle("slow");
						$('#amazing_notice_box').attr("class", "icon icon-chevron-up amazingopen");
						$("body").animate({top: barheight}, 525, 'linear');
						})
        {/literal}
                {/if}
        {literal}
            });
        {/literal}
    </script>
{/foreach}
{/if}