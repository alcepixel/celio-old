<?php
/**
 * 2007-2013 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 *         DISCLAIMER   *
 * *************************************** */
 /* Do not edit or add to this file if you wish to upgrade Prestashop to newer
 * versions in the future.
 * ****************************************************
 *
 *  @author     BEST-KIT.COM (contact@best-kit.com)
 *  @copyright  http://best-kit.com
 *  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

include_once(_PS_MODULE_DIR_ . 'bestkit_bootstraptabs/bestkit_bootstraptabs.php');

class AdminBootstrapTabs extends AdminController {

    protected $position_identifier = 'id_bootstrap_tab';

    protected $_js = '';

    protected $_module = NULL;

    public function _l($string)
    {
        if (is_null($this->_module)) {
            $this->_module = new bestkit_bootstraptabs;
        }

        return $this->_module->l($string, __CLASS__);
    }

    public function __construct()
    {
    	$this->bootstrap = true;
        $this->table = 'bestkit_bootstrap_tab';
        $this->identifier = 'id_bootstrap_tab';
        $this->className = 'BestkitBootstrapTabs';
        $this->lang = TRUE;
        $this->addRowAction('edit');
        $this->addRowAction('delete');

        $this->fields_list = array(
            'id_bootstrap_tab' => array(
                'title' => $this->l('ID'),
                'align' => 'center',
                'width' => 30
            ),
            'title' => array(
                'title' => $this->l('Tab Title'),
                'width' => 300
            ),
            'tab_identifier' => array(
                'title' => $this->l('Identifier'),
                'width' => 300
            ),
            'status' => array(
                'title' => $this->l('Status'),
                'width' => 40,
                'active' => 'update',
                'align' => 'center',
                'type' => 'bool',
                'orderby' => FALSE,
            ),
            'date_add' => array(
                'title' => $this->l('Date Created'),
                'width' => 150,
                'type' => 'date',
                'align' => 'right'
            ),
            'date_upd' => array(
                'title' => $this->l('Last Modified'),
                'width' => 150,
                'type' => 'date',
                'align' => 'right'
            )
        );

        if (Tools::getValue('controller') == 'AdminBootstrapTabs') {
        	$this->_where .= ' AND a.`specific_product` < 1 ';
        }

        /*if (Shop::isFeatureActive() && Shop::getContext() != Shop::CONTEXT_ALL) {
            $this->_where .= ' AND a.' . $this->identifier . ' IN (
                SELECT sa.' . $this->identifier . '
                FROM `' . _DB_PREFIX_ . $this->table . '_shop` sa
                WHERE sa.id_shop IN (' . implode(', ', Shop::getContextListShopID()) . ')
            )';
        }*/

        $this->identifiersDnd = array('id_bootstrap_tab' => 'id_sslide_to_move');

        parent::__construct();
    }

	public function getList($id_lang, $order_by = null, $order_way = null, $start = 0, $limit = null, $id_lang_shop = false)
	{
		$list = parent::getList($id_lang, $order_by, $order_way, $start, $limit, $id_lang_shop);
		$this->_listsql = false;
		return $list;
	}

    public function renderForm()
    {
        $this->display = 'edit';
        $this->initToolbar();

        if (!$this->loadObject(TRUE)) {
            return;
        }

        $this->fields_form = array(
            'tinymce' => TRUE,
            'legend' => array(
                'title' => $this->l('Product Tab'),
                'image' => '../img/admin/tab-categories.gif'
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Tab Title:'),
                    'name' => 'title',
                    'id' => 'title',
                    'lang' => TRUE,
                    'required' => TRUE,
                    'size' => 50,
                    'maxlength' => 50,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Identifier:'),
                    'name' => 'tab_identifier',
                    'id' => 'tab_identifier',
                    'required' => TRUE,
                    'hint' => $this->l('Allowed characters:') . ' a-z, A-Z, 0-9, _',
                    'size' => 50,
                    'maxlength' => 50,
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Status:'),
                    'name' => 'status',
                    'required' => FALSE,
                    'class' => 't',
                    'is_bool' => TRUE,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Content:'),
                    'name' => 'content',
                    'required' => TRUE,
                    'autoload_rte' => TRUE,
                    'lang' => TRUE,
                    'rows' => 5,
                    'cols' => 40,
                ),
            ),
            'submit' => array(
                'title' => $this->l('   Save   '),
            )
        );
        
        $from_product = (int)Tools::getValue('specific_product');
        if ($from_product) {
        	$this->show_form_cancel_button = false;
            $this->fields_form['input'][] = array(
                'type' => 'hidden',
                'name' => 'specific_product',
                'value' => $from_product,
            );
        }

        if (Shop::isFeatureActive() && $from_product) {
            $this->fields_form['input'][] = array(
                'type' => 'shop',
                'label' => $this->l('Shop association:'),
                'name' => 'checkBoxShopAsso',
            );
        }

        $this->tpl_form_vars = array(
            'status' => $this->object->status,
        );

        return parent::renderForm() . $this->_js;
    }

    protected function updateAssoShop($id_object)
    {
        if (!Shop::isFeatureActive()) {
            return;
        }

        $assos_data = $this->getSelectedAssoShop($this->table, $id_object);

        $exclude_ids = $assos_data;
        foreach (Db::getInstance()->executeS('SELECT id_shop FROM ' . _DB_PREFIX_ . 'shop') as $row) {
            if (!$this->context->employee->hasAuthOnShop($row['id_shop'])) {
                $exclude_ids[] = $row['id_shop'];
            }
        }

        Db::getInstance()->delete($this->table . '_shop', '`' . $this->identifier . '` = ' . (int) $id_object . ($exclude_ids ? ' AND id_shop NOT IN (' . implode(', ', $exclude_ids) . ')' : ''));

        $insert = array();
        foreach ($assos_data as $id_shop) {
            $insert[] = array(
                $this->identifier => $id_object,
                'id_shop' => (int) $id_shop,
            );
        }

        return Db::getInstance()->insert($this->table . '_shop', $insert, FALSE, TRUE, Db::INSERT_IGNORE);
    }

    protected function getSelectedAssoShop($table)
    {
        if (!Shop::isFeatureActive()) {
            return array();
        }

        $shops = Shop::getShops(TRUE, NULL, TRUE);
        if (count($shops) == 1 && isset($shops[0])) {
            return array($shops[0], 'shop');
        }

        $assos = array();
        if (Tools::isSubmit('checkBoxShopAsso_' . $table)) {
            foreach (Tools::getValue('checkBoxShopAsso_' . $table) as $id_shop => $value) {
            	$value = 1;
                $assos[] = (int) $id_shop;
            }
        } else if (Shop::getTotalShops(FALSE) == 1) {
            // if we do not have the checkBox multishop, we can have an admin with only one shop and being in multishop
            $assos[] = (int) Shop::getContextShopID();
        }

        return $assos;
    }

    public function processSave()
    {
        $id = Tools::getValue('tab_identifier');
        if (Validate::isModuleName($id)) {
            return parent::processSave();
        }

        $this->errors[] = Tools::displayError('The field "tab_identifier" is invalid. Allowed characters:') . ' a-z, A-Z, 0-9, _';
        $this->display = 'edit';
        return FALSE;
    }

    public function processAdd()
    {
        $id = Tools::getValue('tab_identifier');
        $block = BestkitBootstrapTabs::getTabByIdentifier($id);
        if ($block === FALSE) {
            return parent::processAdd();
        }

        $this->errors[] = Tools::displayError('Duplicate field "tab_identifier".');
        $this->display = 'edit';
        return FALSE;
    }

    public function processUpdate()
    {
        $id = Tools::getValue('tab_identifier');
        $block = BestkitBootstrapTabs::getTabByIdentifier($id);
        if ($block === FALSE || $block->id == Tools::getValue($this->identifier)) {
            return parent::processUpdate();
        }

        $this->errors[] = Tools::displayError('Duplicate field "tab_identifier".');
        $this->display = 'edit';
        return FALSE;
    }

	public function postProcess()
	{
		$process = parent::postProcess();
		if ($process && $to_product = (int)Tools::getValue('specific_product')) {
			Tools::redirectAdmin(Context::getContext()->link->getAdminLink('AdminProducts') .
			'&id_product=' . $to_product . '&updateproduct&conf=4&key_tab=ModuleBestkit_bootstraptabs');
		}

		return $process;
	}

	public function setHelperDisplay(Helper $helper)
	{
		parent::setHelperDisplay($helper);
		if (Tools::getValue('controller') == 'AdminProducts') {
			$helper->show_toolbar = false;
			//$helper->simple_header = true;
			$helper->show_filters = false;
			$helper->no_link = true;
			$helper->title = 'Product Tabs';
			$helper->currentIndex = 'index.php?controller=' . __CLASS__ . '&specific_product=' . Tools::getValue('id_product');
			$helper->toolbar_btn = array('new' => array(
				'href' => $helper->currentIndex .'&add'.$this->table.'&token='.$this->token,
				'desc' => $this->l('New')
			));
		}
	}

	public function addSqlParam($key, $value)
	{
		$this->$key .= $value;
	}
}