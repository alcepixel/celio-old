<?php
/**
 * 2007-2013 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 *         DISCLAIMER   *
 * *************************************** */
 /* Do not edit or add to this file if you wish to upgrade Prestashop to newer
 * versions in the future.
 * ****************************************************
 *
 *  @author     BEST-KIT.COM (contact@best-kit.com)
 *  @copyright  http://best-kit.com
 *  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

include_once(_PS_MODULE_DIR_ . 'bestkit_bootstraptabs/classes/BestkitBootstrapTabs.php');
include_once(_PS_MODULE_DIR_ . 'bestkit_bootstraptabs/AdminBootstrapTabs.php');

class bestkit_bootstraptabs extends Module
{
	const PREFIX = 'bestkikBPT_';
	const DISPLAY_TYPE = 'display_type';

	protected $_hooks = array(
		'displayProductButtons',
        'displayAdminProductsExtra',
        'actionProductUpdate',
        'displayFooterProduct'
	);

	protected $_displayTypes = array(
		'simple_tabs',
		'left_tabs',
		'right_tabs',
		'left_vertical_tabs',
		'right_vertical_tabs',
		'accordion',
		'two_cols_accordion'
	);

    public function __construct()
    {
        $this->name = 'bestkit_bootstraptabs';
        $this->tab = 'front_office_features';
        $this->version = '1.0.1';
        $this->author = 'best-kit.com';
        $this->need_instance = 0;
        $this->module_key = '8e9ce39b0e4ec064ec134425f7c64dd9';

        parent::__construct();

        $this->displayName = $this->l('Responsive Product Tabs & Accordion');
        $this->description = $this->l('Responsive Product Tabs & Accordion');
    }

	public static function useConfig($name, $value = null)
	{
		$name = self::PREFIX . $name;
		if (is_null($value)) {
			return Configuration::get($name);
		} else {
			return Configuration::updateValue($name, $value);
		}

		return FALSE;
	}

    public function install()
    {
		$install = parent::install();
		
		foreach ($this->_hooks as $hook) {
			$this->registerHook($hook);
		}

		self::useConfig(self::DISPLAY_TYPE, current($this->_displayTypes));

        $sql = array();

        $sql[] =
            'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'bestkit_bootstrap_tab` (
              `id_bootstrap_tab` int(10) unsigned NOT NULL auto_increment,
              `status` int(10) NOT NULL default "1",
              `tab_identifier` varchar(255) NOT NULL,
              `specific_product` int(1) NOT NULL,
              `date_add` datetime NOT NULL,
              `date_upd` datetime NOT NULL,
              PRIMARY KEY  (`id_bootstrap_tab`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';

        $sql[] =
            'ALTER TABLE `' . _DB_PREFIX_ . 'bestkit_bootstrap_tab`
                ADD UNIQUE  `tab_identifier` (  `tab_identifier` )';

        $sql[] =
            'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'bestkit_bootstrap_tab_product` (
              `id_bootstrap_tab` int(10) unsigned NOT NULL,
              `id_product` int(10) unsigned NOT NULL,
              `id_shop` int(10) unsigned NOT NULL,
              `is_enabled` int(1) unsigned NOT NULL,
              `tab_identifiers` TEXT NOT NULL,
              PRIMARY KEY (`id_product`, `id_shop`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';

        $sql[] =
            'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'bestkit_bootstrap_tab_shop` (
              `id_bootstrap_tab` int(10) unsigned NOT NULL,
              `id_shop` int(10) unsigned NOT NULL,
              PRIMARY KEY (`id_bootstrap_tab`, `id_shop`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';

        $sql[] =
            'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'bestkit_bootstrap_tab_lang` (
              `id_bootstrap_tab` int(10) unsigned NOT NULL,
              `id_lang` int(10) unsigned NOT NULL,
              `title` varchar(255) NOT NULL,
              `content` text,
              PRIMARY KEY (`id_bootstrap_tab`,`id_lang`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';

        foreach ($sql as $_sql) {
            Db::getInstance()->Execute($_sql);
        }

        $new_tab = new Tab();
        $new_tab->class_name = 'AdminBootstrapTabs';
        $new_tab->id_parent = Tab::getCurrentParentId();
        $new_tab->module = $this->name;
        $languages = Language::getLanguages();
        foreach ($languages as $language) {
            $new_tab->name[$language['id_lang']] = 'Bootstrap Product Tabs';
        }

        $new_tab->add();

        return $install;
    }

    public function uninstall()
    {
		foreach ($this->_hooks as $hook) {
			$this->unregisterHook($hook);
		}

        $sql = array();

        $sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'bestkit_bootstrap_tab`';
        $sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'bestkit_bootstrap_tab_product`';
        $sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'bestkit_bootstrap_tab_shop`';
        $sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'bestkit_bootstrap_tab_lang`';

        foreach ($sql as $_sql) {
            Db::getInstance()->Execute($_sql);
        }

        $idTab = Tab::getIdFromClassName('AdminBootstrapTabs');
        if ($idTab) {
            $tab = new Tab($idTab);
            $tab->delete();
        }

        return parent::uninstall();
    }

	public static function getShopId()
	{
        $shop_id = 1;
        if (!is_null(Shop::getContextShopID())) {
            $shop_id = Shop::getContextShopID();
        }

        return $shop_id;
	}

	public static function getProductData($id_product)
	{
		$sql = '
		SELECT * FROM `' . _DB_PREFIX_ . 'bestkit_bootstrap_tab_product`
		WHERE `id_product` = ' . (int)$id_product . '
		AND `id_shop` = ' . (int)self::getShopId();

		return Db::getInstance()->getRow($sql);
	}

	public static function setProductData($id_product, array $data)
	{
		$data['id_product'] = $id_product;
		return Db::getInstance()->autoExecute(_DB_PREFIX_ . 'bestkit_bootstrap_tab_product', $data, 'REPLACE');
	}

	public static function prepareProductForm($html)
	{
		$html = array_values(array_filter(explode(chr(10), $html)));
		foreach ($html as $key => $line) {
			if (stristr($line, '<form ') || stristr($line, '</form>') || stristr($line, 'name="token"')) {
				unset($html[$key]);
			}
		}

		return implode(chr(10), $html);
	}

	public function hookDisplayAdminProductsExtra()
	{
        $id_product = (int)Tools::getValue('id_product');
        if ($id_product) {
            $stores = Shop::getContextListShopID();
            if (count($stores) > 1) {
                return $this->l('Please select a shop!');
            }

	        $helper = new HelperForm();
	        $helper->module = $this;
	        $helper->title = $this->displayName;
	        $helper->first_call = false;
	        $helper->table = 'product';

	        $this->fields_form[0]['form'] = array(
	            //'legend' => array('title' => $this->l('Settings'), 'image' => $this->_path . 'logo.gif'),
	            'input' => array(
	                array(
	                    'type' => 'switch',
	                    'label' => $this->l('Show tabs:'),
	                    'name' => 'bestkit_bootstraptabs[is_enabled]',
	                    'class' => 't',
	                    'is_bool' => TRUE,
	                    'values' => array(array(
			                'id' => 'is_enabled_on',
			                'value' => 1), array(
			                'id' => 'is_enabled_off',
			                'value' => 0)
			            )
	                ),
	                array(
	                    'type' => 'tags',
	                    'label' => $this->l('General tabs identifiers:'),
	                    'name' => 'bestkit_bootstraptabs[tab_identifiers]',
	                    'style' => 'width:100px',
	                    'hint' => $this->l('Comma delimiter. For example: "tab1, tab2, tab3"'),
	                    'id' => 'bestkit_bootstraptabs_tagify',
	                    'desc' => $this->l('After entering the tab ID, you must press key Enter!'),
	                ),
	            ),
	            'submit' => array(
	                'name' => 'submitAddproductAndStay',
	                'title' => $this->l('   Save And Stay  '),
				),
	        );

	        $product = self::getProductData($id_product);
			$helper->fields_value['bestkit_bootstraptabs[is_enabled]'] = $product['is_enabled'];
			$helper->fields_value['bestkit_bootstraptabs[tab_identifiers]'] = $product['tab_identifiers'];

	        $html = $helper->generateForm($this->fields_form);

			$tabs = new AdminBootstrapTabs;
			$tabs->addSqlParam('_where', ' AND a.`specific_product` = ' . (int)$id_product . ' ');
			$html .= $tabs->renderList();

	        return self::prepareProductForm($html) . $this->display(__FILE__, 'adminProductTab.tpl');
		}
	}

	public function hookActionProductUpdate($params)
	{
		if (Tools::getIsset('bestkit_bootstraptabs')) {
			$data = Tools::getValue('bestkit_bootstraptabs');
			$tab_identifiers = array_map('trim', explode(',', $data['tab_identifiers']));
			foreach ($tab_identifiers as $key => $tab_identifier) {
				if (!BestkitBootstrapTabs::getTabByIdentifier($tab_identifier, true)) {
					unset($tab_identifiers[$key]);
				}
			}

			$data['is_enabled'] = (int)$data['is_enabled'];
			$data['tab_identifiers'] = implode(', ', $tab_identifiers);
			
			$data['id_product'] = false;
			if (isset($params['product'])) {
				$data['id_product'] = $params['product']->id;
			} else if (isset($params['id_product'])) {
				$data['id_product'] = $params['id_product'];
			} else {
				$data['id_product'] = Tools::getValue('id_product');
			}
			
			$data['id_shop'] = self::getShopId();
			
			if ($data['id_product']) {
				self::setProductData($data['id_product'], $data);
			}
		}
	}

	public function hookDisplayFooterProduct($params)
	{
		$this->context->controller->addCss($this->_path . 'css/bootstrap.vertical-tabs.min.css', 'all');
		$this->context->controller->addCss($this->_path . 'css/front.css', 'all');
		$this->context->controller->addJs($this->_path . 'js/front.js');

		$this->smarty->assign(self::DISPLAY_TYPE, self::useConfig(self::DISPLAY_TYPE));
		$this->smarty->assign('bootstrap_tabs', BestkitBootstrapTabs::getTabsByProductId($params['product']->id, self::getShopId()));
		return $this->display(__FILE__, 'productFooter.tpl');
	}

	public static function prepareArrayConfig($array)
	{
		$new_array = array();
		foreach ($array as $value) {
			$new_array[] = array(
				'id' => $value,
				'label' => str_replace('_', ' ', Tools::ucfirst($value)),
			);
		}

		return $new_array;
	}

    private function initConfigForm()
    {
        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
        $helper->toolbar_scroll = TRUE;
        $helper->title = $this->displayName;
        $helper->submit_action = $this->name;

        $this->fields_form[0]['form'] = array(
            'tinymce' => TRUE,
            //'legend' => array('title' => $this->displayName, 'image' => $this->_path . 'logo.gif', 'width' => '100'),
            'input' => array(
                array(
                    'type' => 'select',
                    'label' => $this->l('Display Type:'),
                    'name' => self::DISPLAY_TYPE,
                    'style' => 'width:100px',
                    'options' => array(
                        'query' => self::prepareArrayConfig($this->_displayTypes),
                        'id' => 'id',
                        'name' => 'label',
                    )
                ),              
            ),
            'submit' => array(
                'name' => $this->name,
                'title' => $this->l('   Save  '),
			),
        );

        return $helper;
    }

    protected function postProcess()
    {
        if (Tools::getIsset(self::DISPLAY_TYPE)) {
			self::useConfig(self::DISPLAY_TYPE, Tools::getValue(self::DISPLAY_TYPE));
			Tools::redirectAdmin('index.php?tab=AdminModules&conf=4&configure=' . $this->name
			. '&token=' . Tools::getAdminToken('AdminModules'
			. (int)(Tab::getIdFromClassName('AdminModules'))
			. (int)$this->context->employee->id));
        }
    }

    public function getContent()
    {
    	$this->bootstrap = true;
        $this->postProcess();
        $helper = $this->initConfigForm();
        foreach ($this->fields_form[0]['form']['input'] as $input) {
            $helper->fields_value[$input['name']] = self::useConfig($input['name']);
        }

        return $helper->generateForm($this->fields_form);
    }

    private function initToolbar()
    {
        $this->toolbar_btn['save'] = array('href' => '#', 'desc' => $this->l('Save'));
        return $this->toolbar_btn;
    }
}
