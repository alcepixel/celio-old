<?php
/**
 * 2007-2013 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 *         DISCLAIMER   *
 * *************************************** */
 /* Do not edit or add to this file if you wish to upgrade Prestashop to newer
 * versions in the future.
 * ****************************************************
 *
 *  @author     BEST-KIT.COM (contact@best-kit.com)
 *  @copyright  http://best-kit.com
 *  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

class BestkitBootstrapTabs extends ObjectModel {

    public $id;

    /** @var integer tab ID */
    public $id_bootstrap_tab;

    /** @var string Title */
    public $title;

    /** @var string Identifier */
    public $tab_identifier;

    /** @var boolean Status for display */
    public $status = 1;
    
    public $specific_product;

    /** @var string Content */
    public $content;

    /** @var string Object creation date */
    public $date_add;

    /** @var string Object last modification date */
    public $date_upd;

    public static $definition = array(
        'table' => 'bestkit_bootstrap_tab',
        'primary' => 'id_bootstrap_tab',
        'multilang' => TRUE,
        'fields' => array(
            'tab_identifier' =>     array('type' => self::TYPE_STRING, 'validate' => 'isModuleName', 'required' => TRUE, 'size' => 50),
            'status' =>             array('type' => self::TYPE_INT),
            'specific_product' =>             array('type' => self::TYPE_INT),
            'date_add' =>             array('type' => self::TYPE_DATE),
            'date_upd' =>             array('type' => self::TYPE_DATE),

            // Lang fields
            'title' =>                 array('type' => self::TYPE_STRING, 'lang' => TRUE, 'validate' => 'isCatalogName', 'required' => TRUE, 'size' => 128),
            'content' =>             array('type' => self::TYPE_HTML, 'lang' => TRUE, 'validate' => 'isString', 'size' => 3999999999999, 'required' => TRUE),
        ),
    );

    public static function getTabByIdentifier($tab_identifier, $without_products = false)
    {
        $sql = '
        SELECT `id_bootstrap_tab`
        FROM `' . _DB_PREFIX_ . 'bestkit_bootstrap_tab`
        WHERE `tab_identifier` = "' . pSQL($tab_identifier) . '" ' . ($without_products ? ' AND `specific_product` < 1' : '') . ';';

        $tab_id = (int)Db::getInstance()->getValue($sql);
        $_tab = new self((int)$tab_id);

        if ($_tab->id) {
            return $_tab;
        }

        return FALSE;
    }

	public static function getTabsByProductId($id_product, $id_shop = 1)
	{
		$productData = bestkit_bootstraptabs::getProductData($id_product);
		if (!$productData['is_enabled']) {
			return array();
		}

		$tabIds = array_map('trim', explode(',', $productData['tab_identifiers']));

		$tabs = new Collection(__CLASS__, Context::getContext()->language->id);
		$tabs->where('status', '=', 1);
		$tabs->sqlWhere('(`specific_product` = ' . (int)$id_product . ') OR `tab_identifier` IN (\'' . implode('\', \'', $tabIds) . '\')');

		return $tabs;
	}
}