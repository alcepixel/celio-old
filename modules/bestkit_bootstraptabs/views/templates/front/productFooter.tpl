{**
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
*         DISCLAIMER   *
* *************************************** */
/* Do not edit or add to this file if you wish to upgrade Prestashop to newer
* versions in the future.
* ****************************************************
*
* @package    bestkit_bootstraptabs
* @author     BEST-KIT.COM (contact@best-kit.com)
* @copyright  http://best-kit.com
**}

{foreach from=$bootstrap_tabs item=tab}
	<section class="page-product-box">
		<h3>{$tab->title|escape:'html':'UTF-8'}</h3>
		<div>
			{$tab->content|escape:'UTF-8'}
		</div>
	</section>
{/foreach}

<div id="bestkit_bootstraptabs" data-type="{$display_type|escape:'html':'UTF-8'}" class="panel">
{if $display_type eq 'accordion' || $display_type eq 'two_cols_accordion'}
	<div class="bs-example"></div>
{else}
	<div class="row {if $display_type eq 'left_tabs' || $display_type eq 'left_vertical_tabs'} tabgroup_left{/if}{if $display_type eq 'right_tabs' || $display_type eq 'right_vertical_tabs'} tabgroup_left{/if}">
		{if $display_type eq 'right_tabs' || $display_type eq 'right_vertical_tabs'}
			{if $display_type neq 'simple_tabs'}
				<div class="col-xs-{if $display_type eq 'left_vertical_tabs' || $display_type eq 'right_vertical_tabs'}11{else}8{/if}  col-lg-{if $display_type eq 'left_vertical_tabs' || $display_type eq 'right_vertical_tabs'}11{else}10{/if}">
			{/if}
			<div class="tab-content"></div>
			{if $display_type neq 'simple_tabs'}
				</div><div class="col-xs-{if $display_type eq 'left_vertical_tabs' || $display_type eq 'right_vertical_tabs'}1{else}4{/if} col-lg-{if $display_type eq 'left_vertical_tabs' || $display_type eq 'right_vertical_tabs'}1{else}2{/if}">
			{/if}
			<ul class="nav nav-tabs{if $display_type eq 'left_tabs' || $display_type eq 'left_vertical_tabs'} tabs-left{/if}{if $display_type eq 'right_tabs' || $display_type eq 'right_vertical_tabs'} tabs-right{/if}{if $display_type eq 'left_vertical_tabs' || $display_type eq 'right_vertical_tabs'} vertical-text{/if}"></ul>
			{if $display_type neq 'simple_tabs'}</div>{/if}
		{else}
			{if $display_type neq 'simple_tabs'}
				<div class="col-xs-{if $display_type eq 'left_vertical_tabs' || $display_type eq 'right_vertical_tabs'}1{else}4{/if} col-lg-{if $display_type eq 'left_vertical_tabs' || $display_type eq 'right_vertical_tabs'}1{else}2{/if}">
			{/if}
			<ul class="nav nav-tabs{if $display_type eq 'left_tabs' || $display_type eq 'left_vertical_tabs'} tabs-left{/if}{if $display_type eq 'right_tabs' || $display_type eq 'right_vertical_tabs'} tabs-right{/if}{if $display_type eq 'left_vertical_tabs' || $display_type eq 'right_vertical_tabs'} vertical-text{/if}"></ul>
			{if $display_type neq 'simple_tabs'}
				</div><div class="col-xs-{if $display_type eq 'left_vertical_tabs' || $display_type eq 'right_vertical_tabs'}11{else}8{/if} col-lg-{if $display_type eq 'left_vertical_tabs' || $display_type eq 'right_vertical_tabs'}11{else}10{/if}">
			{/if}
			<div class="tab-content"></div>
			{if $display_type neq 'simple_tabs'}</div>{/if}
		{/if}
	</div>
{/if}
