<?php
/**
  * Newsletter Popup for PrestaShop by COTOKO.com
  * Version: 2.0.13 -- Apr 20 2015
  * Author: Sergio Quiñonez
  * copyright 2011-2015 COTOKO.com™
  *
  * More info: http://www.prestacheap.com/items/6/Newsletter-Popup-for-PrestaShop
  *
  **/

include_once('../../../config/config.inc.php');
include_once('../ctk_newsletterpopup.php');

if (isset($_POST) && $_POST) {
	$terms = (isset($_POST['np_terms']) ? $_POST['np_terms'] : false);
	$email = (isset($_POST['np_email']) ? $_POST['np_email'] : false);
	
	if (isset($_POST['np_dismiss']))
		include_once('dismiss.php');
	
	$module = new CTK_NewsletterPopup(true);
	die(Tools::jsonEncode($module->subscribeNewsletter($module->_id_np, $terms, $email)));
}