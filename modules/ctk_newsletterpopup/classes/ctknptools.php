<?php
/**
  * Newsletter Popup for PrestaShop by COTOKO.com
  * Version: 2.0.13 -- Apr 20 2015
  * Author: Sergio Quiñonez
  * copyright 2011-2015 COTOKO.com™
  *
  * More info: http://www.prestacheap.com/items/6/Newsletter-Popup-for-PrestaShop
  *
  **/

class CTKNPTools
{
	public static function insTheme($np_id, $theme = 'default')
	{
		$file = dirname(__FILE__).'/../themes/'.$theme.'/'.$theme.'.php';
		if (!file_exists($file))
			return false;
		
		include_once($file);
		
		if (isset($design) && $design) {
			foreach ($design as $k => $v)
				$prep_values[] = "'$v'";
			$values  = implode(', ', $prep_values);
			$sql = 'REPLACE INTO `'._DB_PREFIX_.'ctk_np_design`
					VALUES ('.$np_id.', '.$values.')';
			if (!Db::getInstance()->execute($sql))
				return false;
		}

		if (isset($lang) && $lang) {
			//Get first iso code of languages in theme
			$first_iso_lang = key($lang);
			//Get all iso codes of languages in theme
			foreach ($lang as $iso => $values) {
				$iso_langs[] = $iso;
			}

			$languages = Language::getLanguages();
			foreach ($languages as $language) {
				//Check if the active language exists in iso codes of languages in theme
				if (in_array($language['iso_code'], $iso_langs))
					$theme_lang = $lang[$language['iso_code']];
				else
					$theme_lang = $lang[$first_iso_lang];
					
				unset($prep_values);
				foreach ($theme_lang as $k => $v)
					$prep_values[] = "'$v'";
				
				$values = implode(', ', $prep_values);
				$sql = 'REPLACE INTO `'._DB_PREFIX_.'ctk_np_lang`
						VALUES ('.$np_id.', '.$language['id_lang'].', '.$values.')';
				if (!Db::getInstance()->execute($sql))
					return false;
			}
		}

/*		if (isset($lang) && $lang) {
			foreach ($lang as $iso => $columns) {
				//$cols = implode(', ', array_keys($columns));
				$lang_id = Language::getIdByIso($iso);
				$prep_values = array_map(function($str) { return "'$str'"; }, $columns);
				$values = implode(', ', $prep_values);
				$sql = 'REPLACE INTO `'._DB_PREFIX_.'ctk_np_lang`
						VALUES ('.$np_id.', '.$lang_id.', '.$values.')';
				if (!Db::getInstance()->execute($sql))
					return false;
			}
		}*/
		
		return true;
	}
	
	public static function setTheme($theme = 'default')
	{
		$file = dirname(__FILE__).'/../themes/'.$theme.'/'.$theme.'.php';
		if (!file_exists($file))
			return false;
		
		include_once($file);
		
		if (isset($design) && $design) {
			foreach ($design as $key => $val)
				$_POST[$key] = $val;
		}

		if (isset($lang) && $lang) {
			//Get first iso code of languages in theme
			$first_iso_lang = key($lang);
			//Get all iso codes of languages in theme
			foreach ($lang as $iso => $values)
				$iso_langs[] = $iso;

			$languages = Language::getLanguages();
			foreach ($languages as $language) {
				//Check if the active language exists in iso codes of languages in theme
				if (in_array($language['iso_code'], $iso_langs))
					$theme_lang = $lang[$language['iso_code']];
				else
					$theme_lang = $lang[$first_iso_lang];

				foreach ($theme_lang as $name => $content)
					$_POST[$name.'_'.$language['id_lang']] = $content;
			}
		}
		
		return $_POST;
	}
}
