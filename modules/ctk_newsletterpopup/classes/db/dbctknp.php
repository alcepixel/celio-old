<?php
/**
  * Newsletter Popup for PrestaShop by COTOKO.com
  * Version: 2.0.13 -- Apr 20 2015
  * Author: Sergio Quiñonez
  * copyright 2011-2015 COTOKO.com™
  *
  * More info: http://www.prestacheap.com/items/6/Newsletter-Popup-for-PrestaShop
  *
  **/

abstract class DBCTKNP
{
	public static function getID()
	{
		if (_PS_VERSION_ >= '1.5') {
			return	Db::getInstance()->getValue('
				 	SELECT `id` FROM `'._DB_PREFIX_.'ctk_np`
				 	WHERE `id_shop` = '.(int)Context::getContext()->shop->id.'
				 	AND `id_shop_group` = '.(int)Context::getContext()->shop->id_shop_group.'');
		}
		else
			return 1;
	}

	public static function newID()
	{
		if (_PS_VERSION_ >= '1.5')
			Db::getInstance()->Execute('
			INSERT INTO `'._DB_PREFIX_.'ctk_np` (`id_shop`, `id_shop_group`)
			VALUES ('.(int)Context::getContext()->shop->id.', '.(int)Context::getContext()->shop->id_shop_group.')');
		else
			Db::getInstance()->Execute('
			INSERT INTO `'._DB_PREFIX_.'ctk_np` (`id_shop`, `id_shop_group`)
			VALUES (1, 1)');

		return Db::getInstance()->Insert_ID();
	}
}