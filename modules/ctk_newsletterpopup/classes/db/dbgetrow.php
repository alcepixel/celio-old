<?php
/**
  * Newsletter Popup for PrestaShop by COTOKO.com
  * Version: 2.0.13 -- Apr 20 2015
  * Author: Sergio Quiñonez
  * copyright 2011-2015 COTOKO.com™
  *
  * More info: http://www.prestacheap.com/items/6/Newsletter-Popup-for-PrestaShop
  *
  **/

class DBGetRow extends DBCTKNP
{
	public static function design($np_id, $where = false)
	{
		if (!$np_id)
			return false;

		if (_PS_VERSION_ >= '1.5')	{
			return Db::getInstance()->getRow('
			SELECT d.*
			FROM `'._DB_PREFIX_.'ctk_np_design` d
			INNER JOIN `'._DB_PREFIX_.'ctk_np` np
			WHERE d.`id_np` = '.$np_id.'
			'.($where ? 'AND '.$where : '').'');
		}
		else {
			return Db::getInstance()->getRow('
			SELECT d.*
			FROM `'._DB_PREFIX_.'ctk_np_design` d
			WHERE d.`id_np` = '.$np_id.'
			'.($where ? 'AND '.$where : '').'');
		}
	}
	
	public static function config($np_id, $where = false)
	{
		if (!$np_id)
			return false;

		if (_PS_VERSION_ >= '1.5')	{
			return Db::getInstance()->getRow('
			SELECT c.*
			FROM `'._DB_PREFIX_.'ctk_np_configuration` c
		    WHERE c.`id_np` = '.$np_id.'
		    '.($where ? 'AND '.$where : '').'');
		}
		else {
			return Db::getInstance()->getRow('
			SELECT c.*
			FROM `'._DB_PREFIX_.'ctk_np_configuration` c
		    WHERE c.`id_np` = '.$np_id.'
		    '.($where ? 'AND '.$where : '').'');
		}
	}

	public static function newsConfig($np_id, $where = false)
	{
		if (!$np_id)
			return false;

		if (_PS_VERSION_ >= '1.5')	{
			return Db::getInstance()->getRow('
			SELECT nc.*
			FROM `'._DB_PREFIX_.'ctk_np_news_configuration` nc
		    WHERE nc.`id_np` = '.$np_id.'
		    '.($where ? 'AND '.$where : '').'');
		}
		else {
			return Db::getInstance()->getRow('
			SELECT nc.*
			FROM `'._DB_PREFIX_.'ctk_np_news_configuration` nc
		    WHERE nc.`id_np` = '.$np_id.'
		    '.($where ? 'AND '.$where : '').'');
		}
	}
	
	public static function lang($np_id, $id_lang, $where = false)
	{
		if (!$np_id || !$id_lang)
			return false;

		global $cookie;

		if (_PS_VERSION_ >= '1.5')	{
			return Db::getInstance()->getRow('
		    SELECT nl.*
		    FROM `'._DB_PREFIX_.'ctk_np_lang` nl
		    WHERE nl.`id_np` = '.$np_id.' AND nl.`id_lang` = '.$id_lang.'
		    '.($where ? 'AND '.$where : '').'');
		}
		else {
			return Db::getInstance()->getRow('
		    SELECT nl.*
		    FROM `'._DB_PREFIX_.'ctk_np_lang` nl
		    WHERE nl.`id_np` = '.$np_id.' AND nl.`id_lang` = '.$id_lang.'
		    '.($where ? 'AND '.$where : '').'');
		}
	}
}
