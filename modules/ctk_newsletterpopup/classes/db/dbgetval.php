<?php
/**
  * Newsletter Popup for PrestaShop by COTOKO.com
  * Version: 2.0.13 -- Apr 20 2015
  * Author: Sergio Quiñonez
  * copyright 2011-2015 COTOKO.com™
  *
  * More info: http://www.prestacheap.com/items/6/Newsletter-Popup-for-PrestaShop
  *
  **/

class DBGetVal extends DBCTKNP
{
	public static function design($np_id, $field, $where = false)
	{
		if (!$np_id || !$field)
			return false;

		return Db::getInstance()->getValue('
			   SELECT `'.$field.'`
			   FROM `'._DB_PREFIX_.'ctk_np_design`
			   WHERE `id_np` = '.$np_id.'
			   '.($where ? 'AND '.$where : '').'');
	}
	
	public static function config($np_id, $field, $where = false)
	{
		if (!$np_id || !$field)
			return false;

		return Db::getInstance()->getValue('
			   SELECT `'.$field.'`
			   FROM `'._DB_PREFIX_.'ctk_np_configuration`
			   WHERE `id_np` = '.$np_id.'
			   '.($where ? 'AND '.$where : '').'');
	}

	public static function newsConfig($np_id, $field, $where = false)
	{
		if (!$np_id || !$field)
			return false;
			
		return Db::getInstance()->getValue('
			   SELECT `'.$field.'`
			   FROM `'._DB_PREFIX_.'ctk_np_news_configuration`
			   WHERE `id_np` = '.$np_id.'
			   '.($where ? 'AND '.$where : '').'');
	}

	public static function lang($np_id, $id_lang, $field, $where = false)
	{
		if (!$np_id || !$id_lang || !$field)
			return false;
			
		return Db::getInstance()->getValue('
			   SELECT `'.$field.'`
			   FROM `'._DB_PREFIX_.'ctk_np_lang`
			   WHERE `id_np` = '.$np_id.' AND `id_lang` = '.$id_lang.'
			   '.($where ? 'AND '.$where : '').'');
	}
}
