<?php
/**
  * Newsletter Popup for PrestaShop by COTOKO.com
  * Version: 2.0.13 -- Apr 20 2015
  * Author: Sergio Quiñonez
  * copyright 2011-2015 COTOKO.com™
  *
  * More info: http://www.prestacheap.com/items/6/Newsletter-Popup-for-PrestaShop
  *
  **/

class DBSave extends DBCTKNP
{
	public static function design($np_id)
	{
		return  Db::getInstance()->Execute('
				REPLACE INTO `'._DB_PREFIX_.'ctk_np_design`
				VALUES (\''.$np_id.'\',
						\''.Tools::getValue('theme').'\',
						\''.Tools::getValue('show_on_devices').'\',
						\''.Tools::getValue('show_content_top').'\',
						\''.Tools::getValue('show_news_form').'\',
						\''.Tools::getValue('show_content_bottom').'\',
						\''.Tools::getValue('effect').'\',
						\''.Tools::getValue('speed').'\',
						\''.Tools::getValue('position').'\',
						\''.Tools::getValue('margin_top').'\',
						\''.Tools::getValue('width').'\',
						\''.Tools::getValue('screen_background').'\',
						\''.Tools::getValue('screen_opacity').'\',
						\''.Tools::getValue('popup_background').'\',
						\''.Tools::getValue('border_size').'\',
						\''.Tools::getValue('border_radius').'\',
						\''.Tools::getValue('border_color').'\',
						'.(($t = Tools::getValue('custom_css')) ? '\''.$t.'\'' : 'NULL').')');
	}
	
	public static function config($np_id)
	{
		return  Db::getInstance()->Execute('
				REPLACE INTO `'._DB_PREFIX_.'ctk_np_configuration`
				VALUES (\''.$np_id.'\',
						\''.Tools::getValue('how_to_show').'\',
						\''.Tools::getValue('how_to_show_times').'\',
						\''.Tools::getValue('how_to_show_mins').'\',
						\''.Tools::getValue('how_to_reset').'\',
						\''.Tools::getValue('show_on').'\',
						'.(($t = Tools::getValue('show_on_pages')) ? '\''.implode(',', $t).'\',' : 'NULL,').'
						\''.Tools::getValue('close_esc').'\',
						\''.Tools::getValue('close_click_out').'\',
						\''.Tools::getValue('hide_close_btn').'\',
						\''.Tools::getValue('delay_to_show').'\',
						\''.Tools::getValue('auto_close').'\',
						'.(($t = Tools::getValue('auto_close_opts')) ? '\''.implode(',', $t).'\',' : 'NULL,').'
						\''.Tools::getValue('auto_close_secs').'\',
						\''.Tools::getValue('url_redirect').'\',
						\''.Tools::getValue('launch_by_link').'\')');
	}

	public static function newsConfig($np_id)
	{
		return  Db::getInstance()->Execute('
				REPLACE INTO `'._DB_PREFIX_.'ctk_np_news_configuration`
				VALUES (\''.$np_id.'\',
						\''.Tools::getValue('detect_subscribed').'\',
						\''.Tools::getValue('send_validation').'\',
						\''.Tools::getValue('send_confirmation').'\',
						\''.Tools::getValue('send_voucher').'\',
						'.(($t = Tools::getValue('voucher_code')) ? '\''.$t.'\',' : 'NULL,').'
						'.(($t = Tools::getValue('voucher_value')) ? '\''.$t.'\',' : 'NULL,').'
						\''.Tools::getValue('voucher_type').'\',
						'.(($t = Tools::getValue('voucher_days')) ? '\''.$t.'\',' : 'NULL,').'
						'.(($t = Tools::getValue('voucher_min_order')) ? '\''.$t.'\',' : '0,').'
						\''.Tools::getValue('show_accept_terms').'\',
						\''.Tools::getValue('id_cms_terms').'\')');
	}
	
	public static function lang($np_id)
	{
		$languages = Language::getLanguages();
		$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));

		foreach ($languages AS $language) {
			$content_top = Tools::getValue('content_top_'.$language['id_lang']) ? Tools::getValue('content_top_'.$language['id_lang']) : Tools::getValue('content_top_'.$defaultLanguage);
			$input_text = Tools::getValue('input_text_'.$language['id_lang']) ? Tools::getValue('input_text_'.$language['id_lang']) : Tools::getValue('input_text_'.$defaultLanguage);
			$button_text = Tools::getValue('button_text_'.$language['id_lang']) ? Tools::getValue('button_text_'.$language['id_lang']) : Tools::getValue('button_text_'.$defaultLanguage);
			$content_bottom = Tools::getValue('content_bottom_'.$language['id_lang']) ? Tools::getValue('content_bottom_'.$language['id_lang']) : Tools::getValue('content_bottom_'.$defaultLanguage);
			Db::getInstance()->Execute('
			REPLACE INTO `'._DB_PREFIX_.'ctk_np_lang`
			VALUES ('.$np_id.', '.$language['id_lang'].', \''.addslashes($content_top).'\', \''.addslashes($input_text).'\', \''.addslashes($button_text).'\', \''.addslashes($content_bottom).'\')');
		}
		
		return;
	}
}
