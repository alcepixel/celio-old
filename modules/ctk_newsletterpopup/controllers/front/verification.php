<?php
/**
  * Newsletter Popup for PrestaShop by COTOKO.com
  * Version: 2.0.13 -- Apr 20 2015
  * Author: Sergio Quiñonez
  * copyright 2011-2015 COTOKO.com™
  *
  * More info: http://www.prestacheap.com/items/6/Newsletter-Popup-for-PrestaShop
  *
  **/

class CTK_NewsletterPopupVerificationModuleFrontController extends ModuleFrontController
{
	private $message = '';

	/**
	 * @see FrontController::postProcess()
	 */
	public function postProcess()
	{
		$this->module = new CTK_NewsletterPopup(true);
		$message = $this->module->confirmEmail($this->module->_id_np, Tools::getValue('subs_newsletter_email'), Tools::getValue('token'));

		if ($message['err'])
			$this->message = $this->module->displayError($message['msg']);
		else
			$this->message = $this->module->displayConfirmation($message['msg']);
	}

	/**
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{
		parent::initContent();

		$this->context->smarty->assign('ctknp_msg', $this->message);
		$this->setTemplate('verification.tpl');
	}
}