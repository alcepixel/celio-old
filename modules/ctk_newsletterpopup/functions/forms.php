<?php
/**
  * Newsletter Popup for PrestaShop by COTOKO.com
  * Version: 2.0.13 -- Apr 20 2015
  * Author: Sergio Quiñonez
  * copyright 2011-2015 COTOKO.com™
  *
  * More info: http://www.prestacheap.com/items/6/Newsletter-Popup-for-PrestaShop
  *
  **/

include_once(dirname(__FILE__).'/../classes/db/dbgetval.php');
include_once(dirname(__FILE__).'/../classes/db/dbgetrow.php');

class CTK_NewsletterPopupForms extends CTK_NewsletterPopup
{
	public function _top()
	{
		global $currentIndex;
		
		return '
		<link rel="stylesheet" type="text/css" href="'.$this->_url.'js/multiselect/css/jquery.multiselect.css" />
		<link rel="stylesheet" type="text/css" href="'.$this->_url.'js/multiselect/css/jquery-ui.min.css" />
		'.(_PS_VERSION_ < '1.5' ? '<script type="text/javascript" src="'.$this->_url.'js/jquery-1.7.2.min.js"></script>' : '').'
		<script type="text/javascript" src="'.$this->_url.'js/multiselect/jquery-ui.min.js"></script>
		<script type="text/javascript" src="'.$this->_url.'js/multiselect/jquery.multiselect.min.js"></script>
		<link rel="stylesheet" type="text/css" href="'.$this->_url.'js/easytabs/easy-responsive-tabs.css" />
		<script type="text/javascript" src="'.$this->_url.'js/easytabs/easy-responsive-tabs.js"></script>
		<script type="text/javascript" src="'.$this->_url.'js/jscolor/jscolor.min.js"></script>
		<link rel="stylesheet" type="text/css" href="'.$this->_url.'js/nouislider/jquery.nouislider.min.css" />
		<script type="text/javascript" src="'.$this->_url.'js/nouislider/jquery.nouislider.min.js"></script>
		<link rel="stylesheet" type="text/css" href="'.$this->_url.'css/bo.css" />
		<script type="text/javascript" src="'.$this->_url.'js/bo.top.min.js"></script>
		<script type="text/javascript">
			var _url = "'.$this->_url.'";
			$(function(){
				$("select#show_on_pages").multiselect({
					minWidth: 250,
					height: 165,
					checkAllText: "'.$this->l('Check all').'",
					uncheckAllText: "'.$this->l('Uncheck all').'",
					noneSelectedText: "'.$this->l('Select pages').'",
					selectedText: "# '.$this->l('selected(s)').'"
				}); 
			});
		</script>

		<h2>'.$this->displayName.'</h2>
		<div class="ctk-toolbar">
			<div class="about">
				<img src="'.$this->_url.'logo.png" style="float:left; margin-right:15px;">
				<b>'.$this->description.'</b><br />
				<b>Version:</b> '.$this->version.'<br />
				<b>'.$this->l('Author:').'</b> <a target="_blank" href="http://www.cotoko.com/">'.$this->author.'</a><br />
			</div>
			<ul class="buttons">
				<li>
					<a title="'.$this->l('Save').'" href="#" class="toolbar_btn" id="btn-save">
						<span class="icon-save"></span>
						<div>'.$this->l('Save').'</div>
					</a>
				</li>
				<li>
					<a title="'.$this->l('Updates').'" href="'.$this->Updates.'" class="toolbar_btn" id="btn-updates" target="_blank">
						<span class="icon-updates"></span>
						<div>'.$this->l('Updates').'</div>
					</a>
				</li>
				<li>
					<a title="'.$this->l('Help').'" href="'.$this->_url.'documentation/" class="toolbar_btn" id="btn-help" target="_blank">
						<span class="icon-help"></span>
						<div>'.$this->l('Help').'</div>
					</a>
				</li>
			</ul>
		</div>';
	}
	
	public function _content($id_np)
	{
		global $cookie;

		$iso = Language::getIsoById((int)($cookie->id_lang));

		$form = '
		<form id="ctk-form" method="post" action="'.$_SERVER['REQUEST_URI'].'">
			<div id="vTab">
				<ul class="resp-tabs-list">
					<li>'.$this->l('Design').'</li>
					<li>'.$this->l('General Settings').'</li>
					<li>'.$this->l('Newsletter Settings').'</li>
				</ul>
				<div class="resp-tabs-container">
					<div>'.$this->tabDesign($id_np).'</div>
					<div>'.$this->tabGeneralSettings($id_np).'</div>
					<div>'.$this->tabNewsletterSettings($id_np).'</div>
				</div>
			</div>
		</form>
		<div class="clear"></div>';

		$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
		$ad = dirname($_SERVER["PHP_SELF"]);
		$form .= '<script type="text/javascript">	
			var iso = \''.$isoTinyMCE.'\';
			var pathCSS = \''._THEME_CSS_DIR_.'\';
			var ad = \''.$ad.'\';
		</script>
		<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce/tiny_mce.js"></script>
		<script type="text/javascript" src="'.__PS_BASE_URI__.'js/'.(version_compare(_PS_VERSION_, '1.6.0.12') >= 0 ? 'admin/' : '').'tinymce.inc.js"></script>';
		if (version_compare(_PS_VERSION_, '1.5.0.0', '>='))
			$form .= '<script type="text/javascript">$(document).ready(function(){tinySetup();});</script>';

		return $form;
	}
	
	public function _footer()
	{
		return '
		<script type="text/javascript">
			$(document).ready(function() {
				'.(Tools::getValue('vTab') ? 'window.location.hash = \''.Tools::getValue('vTab').'\';' : '').'
			});
		</script>
		<script type="text/javascript" src="'.$this->_url.'js/bo.footer.min.js"></script>';
	}
	
	//Tab for Design
	private function tabDesign($id_np)
	{
		global $cookie;
		
		$iso = Language::getIsoById((int)($cookie->id_lang));
		$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
		$languages = Language::getLanguages();
		$divLangName = 'content_top¤input_text¤button_text¤content_bottom';

		$form = '';

		if (!$design = DBGetRow::design($id_np))
			$form .= '<script type="text/javascript">var noThemeYet = true;</script>';

		$form .= '
		<div class="clear"></div>
		<label>'.$this->l('Theme:').'</label>
		<div class="margin-form">
			<select name="theme" data-msg="'.$this->l('Are you sure to continue?, the current design will be lost.').'" >';
				$sTheme = Tools::getValue('theme', $design['theme']);
				$dir = new DirectoryIterator(dirname(__FILE__).'/../themes');
				foreach ($dir as $fileinfo) {
					if ($fileinfo->isDir() && !$fileinfo->isDot()) {
						$theme = $fileinfo->getFilename();
						$form .= '<option value="'.$theme.'" '.($theme == $sTheme ? 'selected="selected"' : '').' >'.$theme.'</option>';
					}
				}
				$form .= '
			</select>
			<p>'.$this->l('Select a predefined theme, the current design will be lost.').'</p>
		</div>

		<div class="clear"></div>
		<label>'.$this->l('Show on mobile devices:').'</label>
		<div class="margin-form">
			<input type="radio" name="show_on_devices" value="1" '.(Tools::getValue('show_on_devices', ($design && isset($design['show_on_devices']) ? $design['show_on_devices'] : 0)) ? 'checked="checked"' : '').' />
			<img src="../img/admin/enabled.gif" alt="'.$this->l('Yes').'" title="'.$this->l('Yes').'" />
			<input type="radio" name="show_on_devices" value="0" '.(!Tools::getValue('show_on_devices', ($design && isset($design['show_on_devices']) ? $design['show_on_devices'] : 0)) ? 'checked="checked"' : '').' />
			<img src="../img/admin/disabled.gif" alt="'.$this->l('No').'" title="'.$this->l('No').'" />
			<p>'.$this->l('Show the popup on mobile devices.').'</p>
		</div>

		<div class="clear"></div>
		<label>'.$this->l('Show content top:').'</label>
		<div class="margin-form">
			<input type="radio" name="show_content_top" value="1" '.(Tools::getValue('show_content_top', ($design && isset($design['show_content_top']) ? $design['show_content_top'] : 0)) ? 'checked="checked"' : '').' />
			<img src="../img/admin/enabled.gif" alt="'.$this->l('Yes').'" title="'.$this->l('Yes').'" />
			<input type="radio" name="show_content_top" value="0" '.(!Tools::getValue('show_content_top', ($design && isset($design['show_content_top']) ? $design['show_content_top'] : 0)) ? 'checked="checked"' : '').' />
			<img src="../img/admin/disabled.gif" alt="'.$this->l('No').'" title="'.$this->l('No').'" />
			<div class="clear"></div>
			<div class="show_content_top_block ex-block" style="display: none">
				<div class="clear"></div>';
				foreach ($languages as $language)
				{
					$content = DBGetVal::lang($id_np, $language['id_lang'], 'content_top');
					$form .= '
					<div id="content_top_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').'; float: left;">
						<textarea class="rte" cols="50" rows="30" name="content_top_'.$language['id_lang'].'" id="content_topInput_'.$language['id_lang'].'">'.$content.'</textarea>
					</div>';
				}
				$form .= $this->displayFlags($languages, $defaultLanguage, $divLangName, 'content_top', true);
				$form .= '
				<div class="clear"></div>
			</div>
			<p>'.$this->l('Content in the top of the popup.').'</p>
		</div>

		<div class="clear"></div>
		<label>'.$this->l('Show newsletter form:').'</label>
		<div class="margin-form">
			<input type="radio" name="show_news_form" value="1" '.(Tools::getValue('show_news_form', ($design && isset($design['show_news_form']) ? $design['show_news_form'] : 0)) ? 'checked="checked"' : '').' />
			<img src="../img/admin/enabled.gif" alt="'.$this->l('Yes').'" title="'.$this->l('Yes').'" />
			<input type="radio" name="show_news_form" value="0" '.(!Tools::getValue('show_news_form', ($design && isset($design['show_news_form']) ? $design['show_news_form'] : 0)) ? 'checked="checked"' : '').' />
			<img src="../img/admin/disabled.gif" alt="'.$this->l('No').'" title="'.$this->l('No').'" />
			<div class="clear"></div>
			<div class="show_news_form_block" style="display: none">
				<div style="float: left;">
					<p>'.$this->l('Text of the input.').'</p>';
					foreach ($languages as $language)
					{
						$content = DBGetVal::lang($id_np, $language['id_lang'], 'input_text');
						$form .= '
						<div id="input_text_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').'; float: left;">
							<input type="text" size="30" name="input_text_'.$language['id_lang'].'" id="input_textInput_'.$language['id_lang'].'" value="'.$content.'" />
						</div>';
					}
					$form .= $this->displayFlags($languages, $defaultLanguage, $divLangName, 'input_text', true);
				$form .= '
				</div>
				<div style="float: left; margin-left: 20px;">
					<p>'.$this->l('Text of the button.').'</p>';
					foreach ($languages as $language)
					{
						$content = DBGetVal::lang($id_np, $language['id_lang'], 'button_text');
						$form .= '
						<div id="button_text_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').'; float: left;">
							<input type="text" size="30" name="button_text_'.$language['id_lang'].'" id="button_textInput_'.$language['id_lang'].'" value="'.$content.'" />
						</div>';
					}
					$form .= $this->displayFlags($languages, $defaultLanguage, $divLangName, 'button_text', true);
				$form .= '
				</div>
				<div class="clear"></div>
			</div>
			<p>'.$this->l('Subscription form to the newsletter.').'</p>
		</div>

		<div class="clear"></div>
		<label>'.$this->l('Show content bottom:').'</label>
		<div class="margin-form">
			<input type="radio" name="show_content_bottom" value="1" '.(Tools::getValue('show_content_bottom', ($design && isset($design['show_content_bottom']) ? $design['show_content_bottom'] : 0)) ? 'checked="checked"' : '').' />
			<img src="../img/admin/enabled.gif" alt="'.$this->l('Yes').'" title="'.$this->l('Yes').'" />
			<input type="radio" name="show_content_bottom" value="0" '.(!Tools::getValue('show_content_bottom', ($design && isset($design['show_content_bottom']) ? $design['show_content_bottom'] : 0)) ? 'checked="checked"' : '').' />
			<img src="../img/admin/disabled.gif" alt="'.$this->l('No').'" title="'.$this->l('No').'" />
			<div class="clear"></div>
			<div class="show_content_bottom_block ex-block" style="display: none">
				<div class="clear"></div>';
				foreach ($languages as $language)
				{
					$content = DBGetVal::lang($id_np, $language['id_lang'], 'content_bottom');
					$form .= '
					<div id="content_bottom_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').'; float: left;">
						<textarea class="rte" cols="50" rows="30" name="content_bottom_'.$language['id_lang'].'" id="content_bottomInput_'.$language['id_lang'].'">'.$content.'</textarea>
					</div>';
				}
				$form .= $this->displayFlags($languages, $defaultLanguage, $divLangName, 'content_bottom', true);
				$form .= '
				<div class="clear"></div>
			</div>
			<p>'.$this->l('Content in the bottom of the popup.').'</p>
		</div>

		<div class="clear"></div>
		<label>'.$this->l('Effect:').'</label>
		<div class="margin-form">
			<select name="effect" >
				<option value="fadeAndPop" '.(Tools::getValue('effect', ($design && isset($design['effect']) ? $design['effect'] : 0)) == 'fadeAndPop' ? 'selected="selected"' : '').' >fadeAndPop</option>
				<option value="fade" '.(Tools::getValue('effect', ($design && isset($design['effect']) ? $design['effect'] : 0)) == 'fade' ? 'selected="selected"' : '').' >fade</option>
			</select>
			<p>'.$this->l('Effect of opening and closing the popup.').'</p>
		</div>

		<div class="clear"></div>
		<label>'.$this->l('Speed effect:').'</label>
		<div class="margin-form">
			<div class="slider" style="width: 200px"></div>
			<input style="float:left" type="text" size="5" name="speed" data-min="0" data-max="1000" data-step="1" data-name="speed" value="'.(Tools::getValue('speed', (($design && (isset($design['speed']) && $design['speed'])) ? $design['speed'] : 0))).'" />
			<div class="clear"></div>
			<p>'.$this->l('Speed of opening and closing the popup (milliseconds).').'</p>
		</div>

		<div class="clear"></div>
		<label>'.$this->l('Position:').'</label>
		<div class="margin-form">
			<select name="position" >
				<option value="fixed" '.(Tools::getValue('position', ($design && isset($design['position']) ? $design['position'] : 0)) == 'fixed' ? 'selected="selected"' : '').' >fixed</option>
				<option value="absolute" '.(Tools::getValue('position', ($design && isset($design['position']) ? $design['position'] : 0)) == 'absolute' ? 'selected="selected"' : '').' >absolute</option>
			</select>
			<p>'.$this->l('Position of the popup. (absolute: the popup is scrolled with the page) (fixed: the popup is static and background can be scrolled)').'</p>
		</div>

		<div class="clear"></div>
		<label>'.$this->l('Margin top:').'</label>
		<div class="margin-form">
			<div class="slider" style="width: 200px"></div>
			<input style="float:left" type="text" size="5" name="margin_top" data-min="0" data-max="1000" data-step="1" data-name="margin_top" value="'.(Tools::getValue('margin_top', (($design && (isset($design['margin_top']) && $design['margin_top'])) ? $design['margin_top'] : 0))).'" />
			<div class="clear"></div>
			<p>'.$this->l('Margin top of the popup.').'</p>
		</div>

		<div class="clear"></div>
		<label>'.$this->l('Width:').'</label>
		<div class="margin-form">
			<select name="width" >
				<option value="tiny" '.(Tools::getValue('width', ($design && isset($design['width']) ? $design['width'] : 0)) == 'tiny' ? 'selected="selected"' : '').' >tiny</option>
				<option value="small" '.(Tools::getValue('width', ($design && isset($design['width']) ? $design['width'] : 0)) == 'small' ? 'selected="selected"' : '').' >small</option>
				<option value="medium" '.(Tools::getValue('width', ($design && isset($design['width']) ? $design['width'] : 0)) == 'medium' ? 'selected="selected"' : '').' >medium</option>
				<option value="large" '.(Tools::getValue('width', ($design && isset($design['width']) ? $design['width'] : 0)) == 'large' ? 'selected="selected"' : '').' >large</option>
				<option value="xlarge" '.(Tools::getValue('width', ($design && isset($design['width']) ? $design['width'] : 0)) == 'xlarge' ? 'selected="selected"' : '').' >xlarge</option>
			</select>
			<p>'.$this->l('Width of the popup.').'</p>
		</div>
		
		<div class="clear"></div>
		<label>'.$this->l('Screen background:').'</label>
		<div class="margin-form">
			<input class="color" type="text" name="screen_background" value="'.(Tools::getValue('screen_background', (($design && (isset($design['screen_background']) && $design['screen_background'])) ? $design['screen_background'] : '000000'))).'" />
			<p>'.$this->l('Background color of the screen.').'</p>
		</div>

		<div class="clear"></div>
		<label>'.$this->l('Screen background opacity:').'</label>
		<div class="margin-form">
			<div class="slider" style="width: 200px"></div>
			<input style="float:left" type="text" size="5" name="screen_opacity" data-min="0" data-max="1" data-step="0.01" data-name="screen_opacity" value="'.(Tools::getValue('screen_opacity', (($design && (isset($design['screen_opacity']) && $design['screen_opacity'])) ? $design['screen_opacity'] : 0.50))).'" />
			<div class="clear"></div>
			<p>'.$this->l('Opacity of the screen background.').'</p>
		</div>

		<div class="clear"></div>
		<label>'.$this->l('Popup background:').'</label>
		<div class="margin-form">
			<input class="color" type="text" name="popup_background" value="'.(Tools::getValue('popup_background', (($design && (isset($design['popup_background']) && $design['popup_background'])) ? $design['popup_background'] : 'FFFFFF'))).'" />
			<p>'.$this->l('Background color of the popup.').'</p>
		</div>

		<div class="clear"></div>
		<label>'.$this->l('Border size:').'</label>
		<div class="margin-form">
			<div class="slider" style="width: 200px"></div>
			<input style="float:left" type="text" size="5" name="border_size" data-min="0" data-max="100" data-step="1" data-name="border_size" value="'.(Tools::getValue('border_size', (($design && (isset($design['border_size']) && $design['border_size'])) ? $design['border_size'] : 0))).'" />
			<div class="clear"></div>
			<p>'.$this->l('Border size of the popup (pixels), put at 0 to disable.').'</p>
		</div>
		
		<div class="clear"></div>
		<label>'.$this->l('Border radius:').'</label>
		<div class="margin-form">
			<div class="slider" style="width: 200px"></div>
			<input style="float:left" type="text" size="5" name="border_radius" data-min="0" data-max="100" data-step="1" data-name="border_radius" value="'.(Tools::getValue('border_radius', (($design && (isset($design['border_radius']) && $design['border_radius'])) ? $design['border_radius'] : 0))).'" />
			<div class="clear"></div>
			<p>'.$this->l('Border radius of the popup (pixels), put at 0 to disable.').'</p>
		</div>

		<div class="clear"></div>
		<label>'.$this->l('Border color:').'</label>
		<div class="margin-form">
			<input class="color" type="text" name="border_color" value="'.(Tools::getValue('border_color', (($design && (isset($design['border_color']) && $design['border_color'])) ? $design['border_color'] : '666666'))).'" />
			<p>'.$this->l('Border color of the popup.').'</p>
		</div>
		
		<div class="clear"></div>
		<label>'.$this->l('Custom CSS:').'</label>
		<div class="margin-form">
			<textarea cols="80" rows="10" name="custom_css">'.(Tools::getValue('custom_css', (($design && (isset($design['custom_css']) && $design['custom_css'])) ? $design['custom_css'] : ''))).'</textarea>
			<p>'.$this->l('Only for advanced users, add your own styles.').'</p>
		</div>';
		
		return $form;
	}

	//Tab for General Settings	
	private function tabGeneralSettings($id_np)
	{
		$config = DBGetRow::config($id_np);

		$form = '
		<div class="clear"></div>
		<label>'.$this->l('How to show:').'</label>
		<div class="margin-form">
			<select name="how_to_show" >
				<option value="0" '.(!Tools::getValue('how_to_show', ($config && isset($config['how_to_show']) ? $config['how_to_show'] : 0)) ? 'selected="selected"' : '').' >'.$this->l('Optional for visitors (Do not show again)').'</option>
				<option value="1" '.(Tools::getValue('how_to_show', ($config && isset($config['how_to_show']) ? $config['how_to_show'] : 0)) == 1 ? 'selected="selected"' : '').' >'.$this->l('First visit').'</option>
				<option value="2" '.(Tools::getValue('how_to_show', ($config && isset($config['how_to_show']) ? $config['how_to_show'] : 0)) == 2 ? 'selected="selected"' : '').' >'.$this->l('All time').'</option>
				<option value="3" '.(Tools::getValue('how_to_show', ($config && isset($config['how_to_show']) ? $config['how_to_show'] : 0)) == 3 ? 'selected="selected"' : '').' >'.$this->l('Custom times...').'</option>
			</select>
			<div class="clear"></div>
			<div class="how_to_show_block_3" style="display: none">
				<div style="float: left; width: 210px; padding-right: 25px;">
					<p>'.$this->l('Number of times to show.').'</p>
					<div class="slider" style="width: 200px"></div>
					<input style="float:left" type="text" size="5" name="how_to_show_times" data-min="0" data-max="100" data-step="1" data-name="how_to_show_times" value="'.(Tools::getValue('how_to_show_times', (($config && (isset($config['how_to_show_times']) && $config['how_to_show_times'])) ? $config['how_to_show_times'] : 0))).'" />
					<div class="clear"></div>
				</div>
				<div style="float: left; width: 210px;">
					<p>'.$this->l('Between minutes.').'</p>
					<div class="slider" style="width: 200px"></div>
					<input style="float:left" type="text" size="5" name="how_to_show_mins" data-min="0" data-max="100" data-step="1" data-name="how_to_show_mins" value="'.(Tools::getValue('how_to_show_mins', (($config && (isset($config['how_to_show_mins']) && $config['how_to_show_mins'])) ? $config['how_to_show_mins'] : 0))).'" />
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</div>
			<p>'.$this->l('Number of times to show the popup.').'</p>
		</div>

		<div class="clear"></div>
		<label>'.$this->l('How long to reset the user preferences:').'</label>
		<div class="margin-form">
			<select name="how_to_reset" >
				<option value="0" '.(!Tools::getValue('how_to_reset', ($config && isset($config['how_to_reset']) ? $config['how_to_reset'] : 0)) ? 'selected="selected"' : '').' >'.$this->l('When the browser is closed').'</option>
				<option value="1" '.(Tools::getValue('how_to_reset', ($config && isset($config['how_to_reset']) ? $config['how_to_reset'] : 0)) == 1 ? 'selected="selected"' : '').' >'.$this->l('1 day').'</option>
				<option value="7" '.(Tools::getValue('how_to_reset', ($config && isset($config['how_to_reset']) ? $config['how_to_reset'] : 0)) == 7 ? 'selected="selected"' : '').' >'.$this->l('7 days').'</option>
				<option value="15" '.(Tools::getValue('how_to_reset', ($config && isset($config['how_to_reset']) ? $config['how_to_reset'] : 0)) == 15 ? 'selected="selected"' : '').' >'.$this->l('15 days').'</option>
				<option value="30" '.(Tools::getValue('how_to_reset', ($config && isset($config['how_to_reset']) ? $config['how_to_reset'] : 0)) == 30 ? 'selected="selected"' : '').' >'.$this->l('30 days').'</option>
				<option value="90" '.(Tools::getValue('how_to_reset', ($config && isset($config['how_to_reset']) ? $config['how_to_reset'] : 0)) == 90 ? 'selected="selected"' : '').' >'.$this->l('90 days').'</option>
			</select>
			<p>'.$this->l('Number of times to show the popup.').'</p>
		</div>

		<div class="clear"></div>
		<label>'.$this->l('Show on:').'</label>
		<div class="margin-form">
			<select name="show_on" >
				<option value="0" '.(!Tools::getValue('show_on', ($config && isset($config['show_on']) ? $config['show_on'] : 0)) ? 'selected="selected"' : '').' >'.$this->l('All pages of the shop').'</option>
				<option value="1" '.(Tools::getValue('show_on', ($config && isset($config['show_on']) ? $config['show_on'] : 0)) ? 'selected="selected"' : '').' >'.$this->l('Select pages...').'</option>
			</select>
			<div class="clear"></div>
			<div class="show_on_block_1 ex-block" style="display: none">
				<div class="clear"></div>';
				$pagesToShow = false;
				if ($t = Tools::getValue('show_on_pages')) $pagesToShow = $t;
				elseif ($t = $config['show_on_pages']) $pagesToShow = explode(',', $t);
				$form .= '
				<select multiple="multiple" id="show_on_pages" name="show_on_pages[]">';
					foreach ($this->loadPages() as $pageKey => $pageName) {
						$select = false;
						if ($pagesToShow) {
							foreach ($pagesToShow as $pageToShow)
								if ($pageKey == trim($pageToShow))
									$select = true;
						}
						$form .= '<option value="'.$pageKey.'" '.($select ? 'selected="selected"' : '').' >'.$pageName.'</option>';
					}
					$form .= '
				</select>
			</div>
			<p>'.$this->l('Pages where the popup will be displayed.').'</p>
		</div>
		
		<div class="clear"></div>
		<label>'.$this->l('Close with the esc key:').'</label>
		<div class="margin-form">
			<input type="radio" name="close_esc" value="1" '.(Tools::getValue('close_esc', ($config && isset($config['close_esc']) ? $config['close_esc'] : 0)) ? 'checked="checked"' : '').' />
			<img src="../img/admin/enabled.gif" alt="'.$this->l('Yes').'" title="'.$this->l('Yes').'" />
			<input type="radio" name="close_esc" value="0" '.(!Tools::getValue('close_esc', ($config && isset($config['close_esc']) ? $config['close_esc'] : 0)) ? 'checked="checked"' : '').' />
			<img src="../img/admin/disabled.gif" alt="'.$this->l('No').'" title="'.$this->l('No').'" />
			<p>'.$this->l('The visitor could close the popup with the esc key.').'</p>
		</div>

		<div class="clear"></div>
		<label>'.$this->l('Close by clicking outside:').'</label>
		<div class="margin-form">
			<input type="radio" name="close_click_out" value="1" '.(Tools::getValue('close_click_out', ($config && isset($config['close_click_out']) ? $config['close_click_out'] : 0)) ? 'checked="checked"' : '').' />
			<img src="../img/admin/enabled.gif" alt="'.$this->l('Yes').'" title="'.$this->l('Yes').'" />
			<input type="radio" name="close_click_out" value="0" '.(!Tools::getValue('close_click_out', ($config && isset($config['close_click_out']) ? $config['close_click_out'] : 0)) ? 'checked="checked"' : '').' />
			<img src="../img/admin/disabled.gif" alt="'.$this->l('No').'" title="'.$this->l('No').'" />
			<p>'.$this->l('The visitor could close the popup by clicking outside it.').'</p>
		</div>
		
		<div class="clear"></div>
		<label>'.$this->l('Hide close button:').'</label>
		<div class="margin-form">
			<input type="radio" name="hide_close_btn" value="1" '.(Tools::getValue('hide_close_btn', ($config && isset($config['hide_close_btn']) ? $config['hide_close_btn'] : 0)) ? 'checked="checked"' : '').' />
			<img src="../img/admin/enabled.gif" alt="'.$this->l('Yes').'" title="'.$this->l('Yes').'" />
			<input type="radio" name="hide_close_btn" value="0" '.(!Tools::getValue('hide_close_btn', ($config && isset($config['hide_close_btn']) ? $config['hide_close_btn'] : 0)) ? 'checked="checked"' : '').' />
			<img src="../img/admin/disabled.gif" alt="'.$this->l('No').'" title="'.$this->l('No').'" />
			<p>'.$this->l('The button to close the popup will be hidden, be careful, you must leave at least one option for close it.').'</p>
		</div>
		
		<div class="clear"></div>
		<label>'.$this->l('Set a delay to show:').'</label>
		<div class="margin-form">
			<div class="slider" style="width: 200px"></div>
			<input style="float:left" type="text" size="5" name="delay_to_show" data-min="0" data-max="3600" data-step="1" data-name="delay_to_show" value="'.(Tools::getValue('delay_to_show', (($config && (isset($config['delay_to_show']) && $config['delay_to_show'])) ? $config['delay_to_show'] : 0))).'" />
			<div class="clear"></div>
			<p>'.$this->l('Delay time to show the popup, doesn\'t affect when is launched by link (seconds).').'</p>
		</div>
		
		<div class="clear"></div>
		<label>'.$this->l('Auto close:').'</label>
		<div class="margin-form">
			<input type="radio" name="auto_close" value="1" '.(Tools::getValue('auto_close', ($config && isset($config['auto_close']) ? $config['auto_close'] : 0)) ? 'checked="checked"' : '').' />
			<img src="../img/admin/enabled.gif" alt="'.$this->l('Yes').'" title="'.$this->l('Yes').'" />
			<input type="radio" name="auto_close" value="0" '.(!Tools::getValue('auto_close', ($config && isset($config['auto_close']) ? $config['auto_close'] : 0)) ? 'checked="checked"' : '').' />
			<img src="../img/admin/disabled.gif" alt="'.$this->l('No').'" title="'.$this->l('No').'" />
			<div class="clear"></div>
			<div class="auto_close_block ex-block" style="display: none">
				<div class="auto_close_opts">';
					$autoCloseOpts = false;
					if ($t = Tools::getValue('auto_close_opts')) $autoCloseOpts = $t;
					elseif ($t = $config['auto_close_opts']) $autoCloseOpts = explode(',', $t);
					$form .= '
					<label><input type="checkbox" name="auto_close_opts[]" value="1" '.($autoCloseOpts && in_array(1, $autoCloseOpts) ? 'checked="checked"' : '').' /> '.$this->l('When the popup is open').'</label>
					<label><input type="checkbox" name="auto_close_opts[]" value="2" '.($autoCloseOpts && in_array(2, $autoCloseOpts) ? 'checked="checked"' : '').' /> '.$this->l('When a subscription is successful').'</label>
					<label><input type="checkbox" name="auto_close_opts[]" value="3" '.($autoCloseOpts && in_array(3, $autoCloseOpts) ? 'checked="checked"' : '').' /> '.$this->l('When throw "Internal error" on the subscription').'</label>
				</div>
				<div class="clear"></div>
				<p>'.$this->l('Seconds for auto close').'</p>
				<div class="slider" style="width: 200px"></div>
				<input style="float:left" type="text" size="5" name="auto_close_secs" data-min="0" data-max="3600" data-step="1" data-name="auto_close_secs" value="'.(Tools::getValue('auto_close_secs', (($config && (isset($config['auto_close_secs']) && $config['auto_close_secs'])) ? $config['auto_close_secs'] : 0))).'" />
			</div>
			<div class="clear"></div>
			<p>'.$this->l('Auto close the popup after X event.').'</p>
		</div>

		<div class="clear"></div>
		<label>'.$this->l('URL redirect:').'</label>
		<div class="margin-form">
			<input type="text" size="50" placeholder="http://www.example.com/" name="url_redirect" value="'.(Tools::getValue('url_redirect', (($config && (isset($config['url_redirect']) && $config['url_redirect'])) ? $config['url_redirect'] : ''))).'" />
			<p>'.$this->l('Redirect to an URL, when the popup is closed or auto close, leave in blank to disable.').'</p>
		</div>

		<div class="clear"></div>
		<label>'.$this->l('Launch by link:').'</label>
		<div class="margin-form">
			<input type="radio" name="launch_by_link" value="1" '.(Tools::getValue('launch_by_link', ($config && isset($config['launch_by_link']) ? $config['launch_by_link'] : 0)) ? 'checked="checked"' : '').' />
			<img src="../img/admin/enabled.gif" alt="'.$this->l('Yes').'" title="'.$this->l('Yes').'" />
			<input type="radio" name="launch_by_link" value="0" '.(!Tools::getValue('launch_by_link', ($config && isset($config['launch_by_link']) ? $config['launch_by_link'] : 0)) ? 'checked="checked"' : '').' />
			<img src="../img/admin/disabled.gif" alt="'.$this->l('No').'" title="'.$this->l('No').'" />
			<p>'.$this->l('Adds the ability to launch the popup through a link, need add the next class to your link: class="ctk_np_launch"').'</p>
		</div>';
		
		return $form;
	}
	
	//Tab for Newsletter Settings
	private function tabNewsletterSettings($id_np)
	{
		global $cookie;
		
		$languages = Language::getLanguages();
		foreach ($languages as $language)
			if (!file_exists($this->_path.'mails/'.$language['iso_code']))
				$missingMails[] = $language['name'];

		$cmss = CMS::listCms((int)($cookie->id_lang));
		$newsConfig = DBGetRow::newsConfig($id_np);
		$form = '';

		if (isset($missingMails)) {
			$form .= '
			<div class="warn" style="width: 70em;">
				'.$this->l('Missing email folders for languages​​:').' '.implode(', ', $missingMails).'<br \>
				'.$this->l('You need to create email templates for these languages, otherwise the customer will receive the email in English for these language.').'<br \>
				'.$this->l('To create email templates in your language just go to "mails" folder of this module and make a copy of one of these folder (eg. can copy folder "en"), now rename this folder to the ISO Code of the language you need (can see the ISO Code of a language in your back office in languages​​) and translate the content of each email template.').'
			</div>';
		}

		$form .= '
		<div class="clear"></div>
		<label>'.$this->l('Detect subscribed visitor/customer:').'</label>
		<div class="margin-form">
			<input type="radio" name="detect_subscribed" value="1" '.(Tools::getValue('detect_subscribed', ($newsConfig && isset($newsConfig['detect_subscribed']) ? $newsConfig['detect_subscribed'] : 0)) ? 'checked="checked"' : '').' />
			<img src="../img/admin/enabled.gif" alt="'.$this->l('Yes').'" title="'.$this->l('Yes').'" />
			<input type="radio" name="detect_subscribed" value="0" '.(!Tools::getValue('detect_subscribed', ($newsConfig && isset($newsConfig['detect_subscribed']) ? $newsConfig['detect_subscribed'] : 0)) ? 'checked="checked"' : '').' />
			<img src="../img/admin/disabled.gif" alt="'.$this->l('No').'" title="'.$this->l('No').'" />
			<p>'.$this->l('Detect visitors or customers who are currently subscribed to avoid show the popup.').'</p>
		</div>
		
		<div class="clear"></div>
		<label>'.$this->l('Send email validation link:').'</label>
		<div class="margin-form">
			<input type="radio" name="send_validation" value="1" '.(Tools::getValue('send_validation', ($newsConfig && isset($newsConfig['send_validation']) ? $newsConfig['send_validation'] : 0)) ? 'checked="checked"' : '').' />
			<img src="../img/admin/enabled.gif" alt="'.$this->l('Yes').'" title="'.$this->l('Yes').'" />
			<input type="radio" name="send_validation" value="0" '.(!Tools::getValue('send_validation', ($newsConfig && isset($newsConfig['send_validation']) ? $newsConfig['send_validation'] : 0)) ? 'checked="checked"' : '').' />
			<img src="../img/admin/disabled.gif" alt="'.$this->l('No').'" title="'.$this->l('No').'" />
			<p>'.$this->l('Send email validation link after subscription, the customer must validate their email address.').'</p>
		</div>
		
		<div class="clear"></div>
		<label>'.$this->l('Send email confirmation:').'</label>
		<div class="margin-form">
			<input type="radio" name="send_confirmation" value="1" '.(Tools::getValue('send_confirmation', ($newsConfig && isset($newsConfig['send_confirmation']) ? $newsConfig['send_confirmation'] : 0)) ? 'checked="checked"' : '').' />
			<img src="../img/admin/enabled.gif" alt="'.$this->l('Yes').'" title="'.$this->l('Yes').'" />
			<input type="radio" name="send_confirmation" value="0" '.(!Tools::getValue('send_confirmation', ($newsConfig && isset($newsConfig['send_confirmation']) ? $newsConfig['send_confirmation'] : 0)) ? 'checked="checked"' : '').' />
			<img src="../img/admin/disabled.gif" alt="'.$this->l('No').'" title="'.$this->l('No').'" />
			<p>'.$this->l('Send email confirmation when subscription is successful, this option is disable when "Send voucher code" option is selected.').'</p>
		</div>
		
		<div class="clear"></div>
		<label>'.$this->l('Send voucher code:').'</label>
		<div class="margin-form">
			<select name="send_voucher" >
				<option value="0" '.(!Tools::getValue('send_voucher', ($newsConfig && isset($newsConfig['send_voucher']) ? $newsConfig['send_voucher'] : 0)) ? 'selected="selected"' : '').' >'.$this->l('None').'</option>
				<option value="1" '.(Tools::getValue('send_voucher', ($newsConfig && isset($newsConfig['send_voucher']) ? $newsConfig['send_voucher'] : 0)) == 1 ? 'selected="selected"' : '').' >'.$this->l('Code previously generated').'</option>
				<option value="2" '.(Tools::getValue('send_voucher', ($newsConfig && isset($newsConfig['send_voucher']) ? $newsConfig['send_voucher'] : 0)) == 2 ? 'selected="selected"' : '').' >'.$this->l('Generate automatic code for each subscriber').'</option>
			</select>
			<div class="clear"></div>
			<div class="send_voucher_block_1 ex-block" style="display: none">
				<p>'.$this->l('Enter the voucher code.').'</p>
				<input type="text" name="voucher_code" value="'.(Tools::getValue('voucher_code', (($newsConfig && (isset($newsConfig['voucher_code']) && $newsConfig['voucher_code'])) ? $newsConfig['voucher_code'] : ''))).'" />
			</div>
			<div class="send_voucher_block_2" style="display: none">
				<div style="float: left;">
					<p>'.$this->l('Discount of').'</p>
					<input type="text" name="voucher_value" value="'.(Tools::getValue('voucher_value', (($newsConfig && (isset($newsConfig['voucher_value']) && $newsConfig['voucher_value'])) ? $newsConfig['voucher_value'] : ''))).'" />
				</div>
				<div style="float: left; padding-left: 10px;">
					<p>&nbsp;</p>
					<select name="voucher_type">
						<option value="0" '.(!Tools::getValue('voucher_type', ($newsConfig && isset($newsConfig['voucher_type']) ? $newsConfig['voucher_type'] : 0)) ? 'selected="selected"' : '').' > -- </option>
						<option value="1" '.(Tools::getValue('voucher_type', ($newsConfig && isset($newsConfig['voucher_type']) ? $newsConfig['voucher_type'] : 0)) == 1 ? 'selected="selected"' : '').' >'.$this->l('Percentage').'</option>
						<option value="2" '.(Tools::getValue('voucher_type', ($newsConfig && isset($newsConfig['voucher_type']) ? $newsConfig['voucher_type'] : 0)) == 2 ? 'selected="selected"' : '').' >'.$this->l('Amount').'</option>
					</select>
				</div>
				<div class="clear"></div>
				<div>
					<p>'.$this->l('Discount days validity').'</p>
					<input type="text" name="voucher_days" value="'.(Tools::getValue('voucher_days', (($newsConfig && (isset($newsConfig['voucher_days']) && $newsConfig['voucher_days'])) ? $newsConfig['voucher_days'] : ''))).'" />
				</div>
				<div class="clear"></div>
				<div>
					<p>'.$this->l('Minimum order amount (Leave to 0 to disable)').'</p>
					<input type="text" name="voucher_min_order" value="'.(Tools::getValue('voucher_min_order', (($newsConfig && (isset($newsConfig['voucher_min_order']) && $newsConfig['voucher_min_order'])) ? $newsConfig['voucher_min_order'] : ''))).'" />
				</div>
				<div class="clear"></div>
			</div>
			<p>'.$this->l('Send email with a voucher code when subscription is successful.').'</p>
		</div>
		
		<div class="clear"></div>
		<label>'.$this->l('Show accept the terms and conditions:').'</label>
		<div class="margin-form">
			<input type="radio" name="show_accept_terms" value="1" '.(Tools::getValue('show_accept_terms', ($newsConfig && isset($newsConfig['show_accept_terms']) ? $newsConfig['show_accept_terms'] : 0)) ? 'checked="checked"' : '').' />
			<img src="../img/admin/enabled.gif" alt="'.$this->l('Yes').'" title="'.$this->l('Yes').'" />
			<input type="radio" name="show_accept_terms" value="0" '.(!Tools::getValue('show_accept_terms', ($newsConfig && isset($newsConfig['show_accept_terms']) ? $newsConfig['show_accept_terms'] : 0)) ? 'checked="checked"' : '').' />
			<img src="../img/admin/disabled.gif" alt="'.$this->l('No').'" title="'.$this->l('No').'" />
			<div class="clear"></div>
			<div class="show_accept_terms_block ex-block" style="display: none">
				<div class="clear"></div>
				<select name="id_cms_terms">
					<option value="0" >('.$this->l('Select a page').')</option>';
					foreach ($cmss as $cms)
						$form .= '<option value="'.$cms['id_cms'].'" '.(Tools::getValue('id_cms_terms', ($newsConfig && isset($newsConfig['id_cms_terms']) ? $newsConfig['id_cms_terms'] : 0)) == $cms['id_cms'] ? 'selected="selected"' : '').' >'.$cms['meta_title'].'</option>';
					$form .= '
				</select>
			</div>
			<p>'.$this->l('The customer must accept the terms and conditions for subscription.').'</p>
		</div>';
		
		return $form;
	}
	
	private function loadPages()
	{
		$pages = array('index' => $this->l('Home page'),
					'authentication' => $this->l('Authentication'),
					'password' => $this->l('Password recovery'),
					'my-account' => $this->l('My account'),
					'history' => $this->l('My account - History'),
					'order-slip' => $this->l('My account - Credit slips'),
					'addresses' => $this->l('My account - Addresses'),
					'identity' => $this->l('My account - Personal information'),
					'discount' => $this->l('My account - Vouchers'),
					'sitemap' => $this->l('Sitemap'),
					'search' => $this->l('Search page'),
					'order' => $this->l('Shopping cart'),
					'new-products' => $this->l('New products'),
					'best-sales' => $this->l('Best sales'),
					'prices-drop' => $this->l('Price drop'),
					'manufacturer' => $this->l('Manufacturers'),
					'supplier' => $this->l('Suppliers'),
					'category' => $this->l('Categories'),
					'product' => $this->l('Products'),
					'products-comparison' => $this->l('Products comparison'),
					'cms' => $this->l('CMS pages'),
					'stores' => $this->l('Our stores'),
					'404' => $this->l('Error 404'),
				);

		if (_PS_VERSION_ >= '1.5')
			$pages['contact'] = $this->l('Contact form');
		else
			$pages['contact-form'] = $this->l('Contact form');

		return $pages;
	}
}