<?php
/**
  * Newsletter Popup for PrestaShop by COTOKO.com
  * Version: 2.0.13 -- Apr 20 2015
  * Author: Sergio Quiñonez
  * copyright 2011-2015 COTOKO.com™
  *
  * More info: http://www.prestacheap.com/items/6/Newsletter-Popup-for-PrestaShop
  *
  **/

include_once(dirname(__FILE__).'/../classes/db/dbsave.php');

class CTK_NewsletterPopupFormsSubmit extends CTK_NewsletterPopup
{
	private static $err = false;

	public function _submit($id_np)
	{
		$form = '';

		if (Tools::isSubmit('submitUpdate')) {
			$form .= $this->_submitValidation();
			if (self::$err)
				return $form;
			else
				 return $this->_submitProcess($id_np);
		}
	}
	
	private function _submitValidation()
	{
		$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
		$errors = array();

		//Design
		if (!Tools::getValue('theme'))
			$errors[] = $this->l('Error in "Theme:"');
			
		if (!Validate::isBool($t = Tools::getValue('show_on_devices')))
			$errors[] = $this->l('Error in "Show on mobile devices:"');

		if (!Validate::isBool($t = Tools::getValue('show_content_top')))
			$errors[] = $this->l('Error in "Show content top:"');
		elseif ($t && !Tools::getValue('content_top_'.$defaultLanguage))
			$errors[] = $this->l('Error in "Show content top:", you must enter the content top, at least on your default language');
		
		if (!Validate::isBool($t = Tools::getValue('show_news_form')))
			$errors[] = $this->l('Error in "Show newsletter form:"');
		if ($t && !Tools::getValue('input_text_'.$defaultLanguage))
			$errors[] = $this->l('Error in "Show newsletter form:", you must enter the text of the input, at least on your default language');
		if ($t && !Tools::getValue('button_text_'.$defaultLanguage))
			$errors[] = $this->l('Error in "Show newsletter form:", you must enter the text of the button, at least on your default language');
			
		if (!Validate::isBool($t = Tools::getValue('show_content_bottom')))
			$errors[] = $this->l('Error in "Show content bottom:"');
		elseif ($t && !Tools::getValue('content_bottom_'.$defaultLanguage))
			$errors[] = $this->l('Error in "Show content bottom:", you must enter the content bottom, at least on your default language');
			
		if (!Tools::getValue('effect'))
			$errors[] = $this->l('Error in "Effect:"');

		if (!Validate::isInt(Tools::getValue('speed')))
			$errors[] = $this->l('Error in "Speed effect:", need to be a integer value');

		if (!Tools::getValue('position'))
			$errors[] = $this->l('Error in "Position:"');
			
		if (!Validate::isInt(Tools::getValue('margin_top')))
			$errors[] = $this->l('Error in "Margin top:", need to be a integer value');
		
		if (!Tools::getValue('width'))
			$errors[] = $this->l('Error in "Width:"');
			
		if (!$this->validateHEX(Tools::getValue('screen_background')))
			$errors[] = $this->l('Error in "Screen background:", need to be a HEX value');

		if (!Validate::isFloat(Tools::getValue('screen_opacity')))
			$errors[] = $this->l('Error in "Screen background opacity:", need to be a float value');
			
		if (!$this->validateHEX(Tools::getValue('popup_background')))
			$errors[] = $this->l('Error in "Popup background:", need to be a HEX value');

		if (!Validate::isInt(Tools::getValue('border_size')))
			$errors[] = $this->l('Error in "Border size:", need to be a integer value');
			
		if (!Validate::isInt(Tools::getValue('border_radius')))
			$errors[] = $this->l('Error in "Border radius:", need to be a integer value');

		if (!$this->validateHEX(Tools::getValue('border_color')))
			$errors[] = $this->l('Error in "Border color:", need to be a HEX value');

		//General Settings
		if (!Validate::isInt($t = Tools::getValue('how_to_show')))
			$errors[] = $this->l('Error in "How to show:"');
		if ($t == 3 && !Validate::isInt(Tools::getValue('how_to_show_times')))
			$errors[] = $this->l('Error in "How to show:", need to be a integer value');
		if ($t == 3 && !Validate::isInt(Tools::getValue('how_to_show_mins')))
			$errors[] = $this->l('Error in "How to show:", need to be a integer value');

		if (!Validate::isInt(Tools::getValue('how_to_reset')))
			$errors[] = $this->l('Error in "How long to reset the user preferences:"');

		if (!Validate::isInt($t = Tools::getValue('show_on')))
			$errors[] = $this->l('Error in "Show on:"');
		elseif ($t == 1 && !Tools::getValue('show_on_pages'))
			$errors[] = $this->l('Error in "Show on:", you must select at least one page to show the popup');
			
		if (!Validate::isBool(Tools::getValue('close_esc')))
			$errors[] = $this->l('Error in "Close with the esc key:"');
			
		if (!Validate::isBool(Tools::getValue('close_click_out')))
			$errors[] = $this->l('Error in "Close by clicking outside:"');
			
		if (!Validate::isBool(Tools::getValue('hide_close_btn')))
			$errors[] = $this->l('Error in "Hide close button:"');
			
		if (!Validate::isInt(Tools::getValue('delay_to_show')))
			$errors[] = $this->l('Error in "Set a delay to show:", need to be a integer value');

		if (!Validate::isBool($t = Tools::getValue('auto_close')))
			$errors[] = $this->l('Error in "Auto close:", need to be a integer value');
		if ($t && !Tools::getValue('auto_close_opts'))
			$errors[] = $this->l('Error in "Auto close:", you must select at least one option to auto close');
		if ($t && !Validate::isInt(Tools::getValue('auto_close_secs')))
			$errors[] = $this->l('Error in "Seconds:", need to be a integer value');

		if (!$this->validateUrl(Tools::getValue('url_redirect')))
			$errors[] = $this->l('Error in "URL redirect:", need to be a valid url');

		if (!Validate::isBool(Tools::getValue('launch_by_link')))
			$errors[] = $this->l('Error in "Launch by link:"');
		
		//Newsletter Settings
		if (!Validate::isBool(Tools::getValue('detect_subscribed')))
			$errors[] = $this->l('Error in "Detect subscribed visitor/customer:"');
			
		if (!Validate::isBool(Tools::getValue('send_validation')))
			$errors[] = $this->l('Error in "Send email validation link:"');
			
		if (!Validate::isBool(Tools::getValue('send_confirmation')))
			$errors[] = $this->l('Error in "Send email confirmation:"');
			
		if (!Validate::isInt($v = Tools::getValue('send_voucher')))
			$errors[] = $this->l('Error in "Send voucher code:", need to be a integer value');
		if ($v == 1 && !Validate::isDiscountName(Tools::getValue('voucher_code')))
			$errors[] = $this->l('Error in "Send voucher code:", you must enter a valid voucher code');
		if (!Validate::isInt($t = Tools::getValue('voucher_type')) || ($v == 2 && $t == 0))
			$errors[] = $this->l('Error in "Send voucher code:", you need to select a discount type');
		if (($v == 2 && $t == 1 && !Validate::isFloat(Tools::getValue('voucher_value'))) ||
			($v == 2 && $t == 1 && Tools::getValue('voucher_value') <= 0) ||
			($v == 2 && $t == 1 && Tools::getValue('voucher_value') >= 100))
			$errors[] = $this->l('Error in "Send voucher code:", discount percent need to be a integer or float value greater than 0 and less than 100');
		if (($v == 2 && $t == 2 && !Validate::isFloat(Tools::getValue('voucher_value'))) ||
			($v == 2 && $t == 2 && Tools::getValue('voucher_value') <= 0))
			$errors[] = $this->l('Error in "Send voucher code:", discount value need to be a integer or float value greater than zero');
		if (($v == 2 && !Validate::isInt(Tools::getValue('voucher_days'))) || ($v == 2 && Tools::getValue('voucher_days') < 1))
			$errors[] = $this->l('Error in "Send voucher code:", discount validity need to be a integer value greater than zero');
		if (($v == 2 && !Validate::isFloat(Tools::getValue('voucher_min_order'))) || ($v == 2 && Tools::getValue('voucher_min_order') < 0))
			$errors[] = $this->l('Error in "Send voucher code:", minimum order amount need to be a integer or float value greater than zero');
			
		if (!Validate::isBool($t = Tools::getValue('show_accept_terms')))
			$errors[] = $this->l('Error in "Show accept the terms and conditions:"');
		elseif ($t == 1 && !Tools::getValue('id_cms_terms'))
			$errors[] = $this->l('Error in "Show accept the terms and conditions:", you must select one page for terms and conditions');

		if (sizeof($errors))
		{
			self::$err = true;
			return $this->displayError(implode('<br />', $errors));
		}
		
		return true;
	}
	
	private function _submitProcess($id_np)
	{
		DBSave::design($id_np);
		DBSave::config($id_np);
		DBSave::newsConfig($id_np);
		DBSave::lang($id_np);
		
		return $this->displayConfirmation($this->l('Settings updated successfully'));
	}
	
	private function validateHEX($string)
	{
		if ((strlen($string) == 6) && (ctype_xdigit($string)))
			return true;	
		else
			return false;
	}
	
	private function validateUrl($url)
	{
		if ($url) {
			$regex = "((https?|ftp)\:\/\/)?"; //Scheme
			$regex .= "([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?"; //User and Pass
			$regex .= "([a-z0-9-.]*)\.([a-z]{2,3})"; //Host or IP
			$regex .= "(\:[0-9]{2,5})?"; //Port
			$regex .= "(\/([a-z0-9+\$_-]\.?)+)*\/?"; //Path
			$regex .= "(\?[a-z+&\$_.-][a-z0-9;:@&%=+\/\$_.-]*)?"; //GET Query
			$regex .= "(#[a-z_.-][a-z0-9+\$_.-]*)?"; //Anchor
			if (!preg_match("/^$regex$/", trim($url)))
				return false;
		}
		
		return true;
	}
}