<?php
/**
  * Newsletter Popup for PrestaShop by COTOKO.com
  * Version: 2.0.13 -- Apr 20 2015
  * Author: Sergio Quiñonez
  * copyright 2011-2015 COTOKO.com™
  *
  * More info: http://www.prestacheap.com/items/6/Newsletter-Popup-for-PrestaShop
  *
  **/

class CTK_NewsletterPopupFunctions extends CTK_NewsletterPopup
{
	public function isNewsletterRegistered($id_np, $email)
	{
		if (_PS_VERSION_ >= '1.5') {
			if (Db::getInstance()->getRow('SELECT `email` FROM `'._DB_PREFIX_.'newsletter` WHERE `email` = \''.$email.'\' AND id_shop = '.$this->context->shop->id.''))
			{
				if (DBGetVal::newsConfig($id_np, 'send_validation'))
				{
					if (Db::getInstance()->getRow('SELECT `email` FROM `'._DB_PREFIX_.'newsletter` WHERE `email` = \''.$email.'\' AND `active` = \'0\' AND id_shop = '.$this->context->shop->id.''))
						return 3;
				}
				return 1;
			}
			if (!$registered = Db::getInstance()->getRow('SELECT `newsletter` FROM `'._DB_PREFIX_.'customer` WHERE `email` = \''.$email.'\' AND id_shop = '.$this->context->shop->id.''))
				return -1;
			if ($registered['newsletter'] == '1')
				return 2;
			return 0;
		}
		else {
			if (Db::getInstance()->getRow('SELECT `email` FROM `'._DB_PREFIX_.'newsletter` WHERE `email` = \''.$email.'\''))
			{
				if (DBGetVal::newsConfig($id_np, 'send_validation'))
				{
					if (Db::getInstance()->getRow('SELECT `email` FROM `'._DB_PREFIX_.'newsletter` WHERE `email` = \''.$email.'\' AND `active` = \'0\''))
						return 3;
				}
				return 1;
			}
			if (!$registered = Db::getInstance()->getRow('SELECT `newsletter` FROM `'._DB_PREFIX_.'customer` WHERE `email` = \''.$email.'\''))
				return -1;
			if ($registered['newsletter'] == '1')
				return 2;
			return 0;
		}
	}
	
	public function registerGuest($email, $active = true)
	{
		if (_PS_VERSION_ >= '1.5') {
			$sql = 'INSERT INTO '._DB_PREFIX_.'newsletter (id_shop, id_shop_group, email, newsletter_date_add, ip_registration_newsletter, http_referer, active)
					VALUES
					(
						'.$this->context->shop->id.',
						'.$this->context->shop->id_shop_group.',
						\''.$email.'\',
						NOW(),
						\''.Tools::getRemoteAddr().'\',
						(
							SELECT c.http_referer
							FROM '._DB_PREFIX_.'connections c
							WHERE c.id_guest = '.(int)$this->context->customer->id.'
							ORDER BY c.date_add DESC LIMIT 1
						),
						'.(int)$active.'
					)';
		}
		else {
			global $cookie;
			
			if (!isset($cookie->id_lang))
				$cookie = new Cookie('ps');
			
			$sql = 'INSERT INTO '._DB_PREFIX_.'newsletter (email, newsletter_date_add, ip_registration_newsletter, http_referer, active)
					VALUES
					(\''.$email.'\',
						NOW(),
						\''.Tools::getRemoteAddr().'\',
						(
							SELECT c.http_referer
							FROM '._DB_PREFIX_.'connections c
							WHERE c.id_guest = '.(int)($cookie->id_guest).'
							ORDER BY c.date_add DESC LIMIT 1
						),
						'.(int)$active.'
					)';
		}

		if (!Db::getInstance()->execute($sql))
			return false;
		
		return true;
	}
	
	public function registerUser($email)
	{
		if (_PS_VERSION_ >= '1.5') {
			$sql = 'UPDATE '._DB_PREFIX_.'customer
					SET `newsletter` = 1, newsletter_date_add = NOW(), `ip_registration_newsletter` = \''.Tools::getRemoteAddr().'\'
					WHERE `email` = \''.$email.'\'
					AND id_shop = '.$this->context->shop->id;
		}
		else {
			$sql = 'UPDATE `'._DB_PREFIX_.'customer`
					SET `newsletter` = 1, newsletter_date_add = NOW(), `ip_registration_newsletter` = \''.Tools::getRemoteAddr().'\'
					WHERE `email` = \''.$email.'\'';
		}

		if (!Db::getInstance()->execute($sql))
			return false;

		return true;
	}
	
	public function activateGuest($email)
	{
		return	Db::getInstance()->execute('
				UPDATE `'._DB_PREFIX_.'newsletter`
				SET `active` = 1
				WHERE `email` = \''.$email.'\'');
	}
	
	public function getGuestEmail($token)
	{
		$sql = 'SELECT `email`
				FROM `'._DB_PREFIX_.'newsletter`
				WHERE MD5(CONCAT( `email` , `newsletter_date_add`, \''._COOKIE_KEY_.'\')) = \''.$token.'\'
				AND `active` = 0';

		return Db::getInstance()->getValue($sql);
	}
	
	public function getToken($email, $registerStatus)
	{
		/* Unsubscribe if the user ins't a customer */
		if ($registerStatus == 1)
		{
			$sql = 'SELECT MD5(CONCAT( `email` , `newsletter_date_add` , \''._COOKIE_KEY_.'\' )) as token
					FROM `'._DB_PREFIX_.'newsletter`
					WHERE `email` = \''.$email.'\'';
		}
		
		/* Unsubscribe if the user is a customer */
		elseif ($registerStatus == 2)
		{
			$sql = 'SELECT MD5(CONCAT( `email` , `date_add` , \''._COOKIE_KEY_.'\' )) as token
					FROM `'._DB_PREFIX_.'customer`
					WHERE `email` = \''.$email.'\'';
		}
		
		/* Subscribe ii the user is a customer or not */
		else
		{
			$sql = 'SELECT MD5(CONCAT( `email` , `newsletter_date_add` , \''._COOKIE_KEY_.'\' )) as token
					FROM `'._DB_PREFIX_.'newsletter`
					WHERE `email` = \''.$email.'\'';
		}		

		return Db::getInstance()->getValue($sql);
	}

	public function createDiscount($value, $type, $dateValidity, $min_order, $description)
	{
		$id_currency = (int)Configuration::get('PS_CURRENCY_DEFAULT');
		
		if (_PS_VERSION_ >= '1.5') {
			$cartRule = new CartRule();
			if ($type == 1)
				$cartRule->reduction_percent = (float)$value;
			else
				$cartRule->reduction_amount = (float)$value;
			$cartRule->date_to = $dateValidity;
			$cartRule->date_from = date('Y-m-d H:i:s');
			$cartRule->quantity = 1;
			$cartRule->quantity_per_user = 1;
			$cartRule->cart_rule_restriction = 1;
			$cartRule->minimum_amount = $min_order;
			$cartRule->minimum_amount_currency = $id_currency;
			$cartRule->reduction_currency = $id_currency;
			
			$languages = Language::getLanguages(true);
			foreach ($languages AS $language)
				$cartRule->name[(int)$language['id_lang']] = $description;

			$code = 'CTKNP-'.strtoupper(Tools::passwdGen(10));
			$cartRule->code = $code;
			$cartRule->active = 1;
			
			if (!$cartRule->add())
				return false;
			
			return $cartRule;
		}
		else {
			$discount = new Discount();
			$discount->id_discount_type = $type;
			$discount->value = (float)($value);
			$discount->id_currency = $id_currency;
			$discount->date_to = $dateValidity;
			$discount->date_from = date('Y-m-d H:i:s');
			$discount->quantity = 1;
			$discount->quantity_per_user = 1;
			$discount->cumulable = 0;
			$discount->cumulable_reduction = 1;
			$discount->minimal = $min_order;
			
			$languages = Language::getLanguages(true);
			foreach ($languages AS $language)
				$discount->description[(int)($language['id_lang'])] = $description;
				
			$name = 'CTKNP-'.strtoupper(Tools::passwdGen(10));
			$discount->name = $name;
			$discount->active = 1;
			$result = $discount->add();
			
			if (!$result)
				return false;
			
			return $discount;
		}
	}

	public function _sendVoucher($email, $template, $templateVars = null)
	{
		if (_PS_VERSION_ >= '1.5') {
			if (!Mail::Send($this->mailLang(), $template, Mail::l('Newsletter voucher', $this->context->language->id), $templateVars, $email, null, null, null, null, null, $this->_path.'mails/', false, $this->context->shop->id))
				return false;
		}
		else {
			global $cookie;
			
			if (!isset($cookie->id_lang))
				$cookie = new Cookie('ps');

			if (!Mail::Send($this->mailLang(), $template, Mail::l('Newsletter voucher', (int)$cookie->id_lang), $templateVars, $email, null, null, null, null, null, $this->_path.'mails/'))
				return false;
		}
		
		return true;
	}
	
	public function mailLang()
	{
		global $cookie;
		
		if (!isset($cookie->id_lang))
			$cookie = new Cookie('ps');

		$id_lang = (int)$cookie->id_lang;
		$iso = Language::getIsoById($id_lang);
		$id_en = Language::getIdByIso('en');
		if (!file_exists($this->_path.'mails/'.$iso))
			return $id_en;
		else
			return $id_lang;
	}
}
