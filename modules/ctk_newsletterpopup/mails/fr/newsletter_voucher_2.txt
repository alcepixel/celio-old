﻿Bonjour,


Inscription à la newsletter

Nous vous remercions de votre abonnement à notre newsletter en {shop_name}
 
En guise de remerciement, nous voulons vous donner un rabais de {value} sur votre prochaine commande! Cette offre est valable {days} jours, ne perdez plus un instant!

Voici le numéro de votre bon de réduction: {voucher_num}

Copiez-collez ce numéro dans le panier de votre prochain achat avant de régler la commande.



{shop_name} [{shop_url}] propulsé par PrestaShop(tm) [http://www.prestashop.com/]