<?php
/**
  * Newsletter Popup for PrestaShop by COTOKO.com
  * Version: 2.0.13 -- Apr 20 2015
  * Author: Sergio Quiñonez
  * copyright 2011-2015 COTOKO.com™
  *
  * More info: http://www.prestacheap.com/items/6/Newsletter-Popup-for-PrestaShop
  *
  **/

include_once('../../../config/config.inc.php');
include_once('../ctk_newsletterpopup.php');

$module = new CTK_NewsletterPopup();

$design = array(
	'theme' => 'black-friday',
	'show_on_devices' => '1',
	'show_content_top' => '1',
	'show_news_form' => '1',
	'show_content_bottom' => '1',
	'effect' => 'fade',
	'speed' => '0',
	'position' => 'fixed',
	'margin_top' => '30',
	'width' => 'medium',
	'screen_background' => '000000',
	'screen_opacity' => '0.65',
	'popup_background' => 'FFFFFF',
	'border_size' => '1',
	'border_radius' => '0',
	'border_color' => 'C81D1F',
	'custom_css' => '.reveal-modal button, .reveal-modal .button {
  background-color: #ed2323;
}
.reveal-modal button:hover, .reveal-modal button:focus, .reveal-modal .button:hover, .reveal-modal .button:focus {
  background-color: #b51919;
  color: white;
}
.reveal-modal button.disabled, .reveal-modal button[disabled], .reveal-modal .button.disabled, .reveal-modal .button[disabled] {
  background-color: #ed5c5c;
}
.reveal-modal input[type="text"][name=np_email] {
  padding-left: 65px !important;
  background: url('.$module->_url.'themes/black-friday/images/email.png) #ffffff no-repeat 10px; !important;
}
.reveal-modal input[type="text"][name=np_email]:focus {
  background: url('.$module->_url.'themes/black-friday/images/email.png) #e5e5e5 no-repeat 10px; !important;
  border-color: #999999;
}'
);

$lang = array(
	'en' => array(
		'content_top' => '<p><img style="float: left; margin-right: 10px; vertical-align: middle;" src="'.$module->_url.'themes/black-friday/images/special-offer-red.png" alt="prestacheap special offer" /></p>
<h1 style="background: none; text-align: left;">Subscribe to our Newsletter</h1>
<p style="background: none; text-align: left;">Subscribe to our newsletter and discover the best offers and news, subscribe today and you will receive a voucher code, which could be used in your next purchase.</p>',
		'input_text' => 'enter your e-mail',
		'button_text' => 'Subscribe',
		'content_bottom' => '<p><a href="http://www.prestacheap.com/items/6/Newsletter-Popup-for-PrestaShop" target="_blank"><img src="'.$module->_url.'themes/black-friday/images/black-friday.jpg" alt="prestacheap special offer" width="100%" height="auto" /></a></p>'),
		
	'fr' => array(
		'content_top' => '<p><img style="float: left; margin-right: 10px; vertical-align: middle;" src="'.$module->_url.'themes/black-friday/images/special-offer-red.png" alt="prestacheap offre spéciale" /></p>
<h1 style="background: none; text-align: left;">Abonnez-vous à notre Newsletter</h1>
<p style="background: none; text-align: left;">Abonnez-vous à notre newsletter et découvrir les meilleures offres et des nouvelles, abonnez-vous dès aujourd hui et vous recevrez un code de réduction, ce qui pourrait être utilisé dans votre prochain achat.</p>',
		'input_text' => 'entrez votre e-mail',
		'button_text' => 'Souscrire',
		'content_bottom' => '<p><a href="http://www.prestacheap.com/items/6/Newsletter-Popup-for-PrestaShop" target="_blank"><img src="'.$module->_url.'themes/black-friday/images/black-friday.jpg" alt="prestacheap offre spéciale" width="100%" height="auto" /></a></p>'),
		
	'es' => array(
		'content_top' => '<p><img style="float: left; margin-right: 10px; vertical-align: middle;" src="'.$module->_url.'themes/black-friday/images/special-offer-red.png" alt="prestacheap oferta especial" /></p>
<h1 style="background: none; text-align: left;">Suscribase a nuestro Boletin</h1>
<p style="background: none; text-align: left;">Suscribase a nuestro boletin y descubra las mejores ofertas y novedades, suscribase hoy y usted recibira un codigo de descuento, que se podra utilizar en su proxima compra.</p>',
		'input_text' => 'escriba su e-mail',
		'button_text' => 'Suscribirse',
		'content_bottom' => '<p><a href="http://www.prestacheap.com/items/6/Newsletter-Popup-for-PrestaShop" target="_blank"><img src="'.$module->_url.'themes/black-friday/images/black-friday.jpg" alt="prestacheap oferta especial" width="100%" height="auto" /></a></p>')
);