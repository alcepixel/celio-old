<?php
/**
  * Newsletter Popup for PrestaShop by COTOKO.com
  * Version: 2.0.13 -- Apr 20 2015
  * Author: Sergio Quiñonez
  * copyright 2011-2015 COTOKO.com™
  *
  * More info: http://www.prestacheap.com/items/6/Newsletter-Popup-for-PrestaShop
  *
  **/

include_once('../../../config/config.inc.php');
include_once('../ctk_newsletterpopup.php');

$module = new CTK_NewsletterPopup();

$design = array(
	'theme' => 'default',
	'show_on_devices' => '1',
	'show_content_top' => '1',
	'show_news_form' => '1',
	'show_content_bottom' => '1',
	'effect' => 'fadeAndPop',
	'speed' => '250',
	'position' => 'absolute',
	'margin_top' => '70',
	'width' => 'medium',
	'screen_background' => '000000',
	'screen_opacity' => '0.45',
	'popup_background' => 'FFFFFF',
	'border_size' => '1',
	'border_radius' => '10',
	'border_color' => '636363',
	'custom_css' => '.reveal-modal button, .reveal-modal .button {
  -webkit-border-radius: 5px !important;
  border-radius: 5px !important;
}
.reveal-modal .alert-box {
  -webkit-border-radius: 5px !important;
  border-radius: 5px !important;
}
.reveal-modal input[type="text"][name=np_email] {
  padding-left: 65px !important;
  background: url('.$module->_url.'themes/default/images/email.png) #efefef no-repeat 10px; !important;
  -webkit-border-radius: 5px !important;
  border-radius: 5px !important;
}
.reveal-modal input[type="text"][name=np_email]:focus {
  background: url('.$module->_url.'themes/default/images/email.png) #f7f7f7 no-repeat 10px; !important;
  border-color: #999999;
}
.reveal-modal input[type="text"][name=np_email][disabled] {
  background-color: #dddddd !important;
}'
);

$lang = array(
	'en' => array(
		'content_top' => '<p><img style="float: left; margin-right: 10px; vertical-align: middle;" src="'.$module->_url.'themes/default/images/special-offer-blue.png" alt="prestacheap special offer" /></p>
<h1 style="background: none; text-align: left;">Subscribe to our Newsletter</h1>
<p style="background: none; text-align: left;">Subscribe to our newsletter and discover the best offers and news, subscribe today and you will receive a voucher code, which could be used in your next purchase.</p>',
		'input_text' => 'enter your e-mail',
		'button_text' => 'Subscribe',
		'content_bottom' => '<p style="padding-bottom: 0;"><iframe style="border: none; overflow: hidden; width: 100%; height: 270px;" src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2FPrestaCheap&amp;width=781&amp;height=290&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;locale=en_US&amp;stream=false&amp;show_border=false" frameborder="0" scrolling="no" width="320" height="240"></iframe></p>'),

	'fr' => array(
		'content_top' => '<p><img style="float: left; margin-right: 10px; vertical-align: middle;" src="'.$module->_url.'themes/default/images/special-offer-blue.png" alt="prestacheap offre spéciale" /></p>
<h1 style="background: none; text-align: left;">Abonnez-vous à notre Newsletter</h1>
<p style="background: none; text-align: left;">Abonnez-vous à notre newsletter et découvrir les meilleures offres et des nouvelles, abonnez-vous dès aujourd hui et vous recevrez un code de réduction, ce qui pourrait être utilisé dans votre prochain achat.</p>',
		'input_text' => 'entrez votre e-mail',
		'button_text' => 'Souscrire',
		'content_bottom' => '<p style="padding-bottom: 0;"><iframe style="border: none; overflow: hidden; width: 100%; height: 270px;" src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2FPrestaCheap&amp;width=781&amp;height=290&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;locale=fr_FR&amp;stream=false&amp;show_border=false" frameborder="0" scrolling="no" width="320" height="240"></iframe></p>'),
		
	'es' => array(
		'content_top' => '<p><img style="float: left; margin-right: 10px; vertical-align: middle;" src="'.$module->_url.'themes/default/images/special-offer-blue.png" alt="prestacheap oferta especial" /></p>
<h1 style="background: none; text-align: left;">Suscribase a nuestro Boletin</h1>
<p style="background: none; text-align: left;">Suscribase a nuestro boletin y descubra las mejores ofertas y novedades, suscribase hoy y usted recibira un codigo de descuento, que se podra utilizar en su proxima compra.</p>',
		'input_text' => 'escriba su e-mail',
		'button_text' => 'Suscribirse',
		'content_bottom' => '<p style="padding-bottom: 0;"><iframe style="border: none; overflow: hidden; width: 100%; height: 270px;" src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2FPrestaCheap&amp;width=781&amp;height=290&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;locale=es_ES&amp;stream=false&amp;show_border=false" frameborder="0" scrolling="no" width="320" height="240"></iframe></p>')
);