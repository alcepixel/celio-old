{**
  * Newsletter Popup for PrestaShop by COTOKO.com
  * Version: 2.0.13 -- Apr 20 2015
  * Author: Sergio Quiñonez
  * copyright 2011-2015 COTOKO.com™
  *
  * More info: http://www.prestacheap.com/items/6/Newsletter-Popup-for-PrestaShop
  *
  **}

<!-- Start Module Newsletter Popup -->
<style>
	.reveal-modal-bg {literal}{{/literal}
		background: #{$ctk_np_design.screen_background};
		background: rgba({$ctk_np_design.screen_background_rgb}, {$ctk_np_design.screen_opacity});
	{literal}}{/literal}
	.reveal-modal {literal}{{/literal}
		background-color: #{$ctk_np_design.popup_background};
		border: solid {$ctk_np_design.border_size}px #{$ctk_np_design.border_color};
		-webkit-border-radius: {$ctk_np_design.border_radius}px;
		border-radius: {$ctk_np_design.border_radius}px;
	{literal}}{/literal}
	@media only screen and (min-width: 40.063em) {literal}{{/literal}
		.reveal-modal {literal}{{/literal}
			position: {$ctk_np_design.position};
			top: {$ctk_np_design.margin_top}px;
		{literal}}{/literal}
	{literal}}{/literal}
{if isset($ctk_np_design.custom_css) && $ctk_np_design.custom_css}{$ctk_np_design.custom_css}{/if}
</style>

<!-- Newsletter Popup -->
<div id="ctk_np_popup" class="reveal-modal {$ctk_np_design.width}" data-reveal data-options="animation: {$ctk_np_design.effect}; animation_speed: {$ctk_np_design.speed}; close_on_background_click: {$ctk_np_config.close_click_out}; close_on_esc: {$ctk_np_config.close_esc}">
    {if isset($ctk_np_config.hide_close_btn) && !$ctk_np_config.hide_close_btn}
    	<a class="close-reveal-modal">&#215;</a>
	{/if}
    
    <!-- Content top -->
    {if isset($ctk_np_design.show_content_top) && $ctk_np_design.show_content_top}
        <div class="row cnt-top">
            <div class="large-12 columns">
                {$ctk_np_lang.content_top}
            </div>
        </div>
    {/if}

     <!-- Content bottom -->
    {if isset($ctk_np_design.show_content_bottom) && $ctk_np_design.show_content_bottom}
        <!--<div class="row"><div class="large-12 columns line-h"></div></div>-->
        <div class="row cnt-btm">
            <div class="large-12 columns">
                {$ctk_np_lang.content_bottom}
            </div>
        </div>
    {/if}

    <!-- Newsletter form -->
    {if isset($ctk_np_design.show_news_form) && $ctk_np_design.show_news_form}
    	<div class="nws-frm">
            <div class="row alr-bx">
                <div class="large-12 columns">
                    <div class="alert-box">
                      <span></span>
                      <a href="#" class="close">&times;</a>
                    </div>
                </div>
            </div>
            <form id="np_form" action="" method="post">
                <div class="row ipt-btn">
                    <div class="large-9 columns">
                        <input type="text" name="np_email" placeholder="{$ctk_np_lang.input_text}" value="" onfocus="javascript:if(this.value=='{$ctk_np_lang.input_text}')this.value='';" onblur="javascript:if(this.value=='')this.value='{$ctk_np_lang.input_text}';" />
                    </div>
                    <div class="large-3 columns">
                        <a id="np_submit" href="#" class="button expand">{$ctk_np_lang.button_text}</a>
                    </div>
                </div>
                {if isset($ctk_np_newsConfig.show_accept_terms) && $ctk_np_newsConfig.show_accept_terms}
                    <div class="row apt-trm">
                        <div class="large-12 columns text-right">
							<div class="checkbox right">
								<a class="left" style="margin-right: 5px !important;" data-reveal-id="ctk_np_popup_terms">{l s='Accept the terms and conditions' mod='ctk_newsletterpopup'}</a>
								<input class="right" type="checkbox" name="np_terms" />
							</div>
                        </div>
                    </div>
                {/if}
            </form>
		</div>
    {/if}
    
    <!-- Footer line -->
    {if (isset($ctk_np_config.how_to_show) && !$ctk_np_config.how_to_show) OR (isset($ctk_np_config.auto_close_secs) && $ctk_np_config.auto_close_secs)}
        <div class="row">
            <div class="large-12 columns">&nbsp;</div>
        </div>
        <div class="row ftr-ln">
            {if isset($ctk_np_config.how_to_show) && !$ctk_np_config.how_to_show}
                <div class="large-{if $ctk_np_config.auto_close_secs}6{else}12{/if} columns text-left">
					<div class="checkbox">
                    	<input id="np_dismiss" type="checkbox" name="np_dismiss" />
						<label for="np_dismiss">{l s='Do not show again' mod='ctk_newsletterpopup'}</label>
					</div>
                </div>
            {/if}
            {if isset($ctk_np_config.auto_close_secs) && $ctk_np_config.auto_close_secs}
                <div class="large-{if !$ctk_np_config.how_to_show}6{else}12{/if} columns text-right">
                    <p class="auto-close"></p>
                </div>
            {/if}
        </div>
    {/if}
</div>

<!-- Terms Popup -->
{if isset($ctk_np_newsConfig.show_accept_terms) && $ctk_np_newsConfig.show_accept_terms}
    {if isset($ctk_np_terms) && $ctk_np_terms}
        <div id="ctk_np_popup_terms" class="reveal-modal {$ctk_np_design.width}" data-reveal data-options="animation: {$ctk_np_design.effect}; animation_speed: {$ctk_np_design.speed}; close_on_background_click: false; close_on_esc: false">
            <a class="close-reveal-modal" data-reveal-id="ctk_np_popup">&lt;</a>
            <div class="row">
                <div class="large-12 columns">
                    <h1>{$ctk_np_terms.meta_title}</h1>
                    <div class="terms-content">
                        {$ctk_np_terms.content}
                    </div>
                </div>
            </div>
		</div>
    {/if}
{/if}

<script type="text/javascript">
	var ctk_np = {literal}{{/literal}
		{if $ctk_np_vars.dontShowAgain}dontShowAgain: true,{/if}
		_url: "{$ctk_np_vars._url}",
		urlTheme: "{$ctk_np_vars.urlTheme}",
		launchByLink: {$ctk_np_vars.launchByLink},
		delayToShow: {$ctk_np_vars.delayToShow},
		autoClose: {$ctk_np_vars.autoClose},
		autoCloseOpts: [{$ctk_np_vars.autoCloseOpts}],
		autoCloseSecs: {$ctk_np_vars.autoCloseSecs},
		autoCloseText: "{$ctk_np_vars.autoCloseText}",
		urlRedirect: "{$ctk_np_vars.urlRedirect}",
		msgTimeOut: "{$ctk_np_vars.msgTimeOut}"
	{literal}}{/literal}
	
	$(document).foundation();
</script>
<!-- End Module Newsletter Popup -->