<?php
/**
  * Newsletter Popup for PrestaShop by COTOKO.com
  * Version: 2.0.13 -- Apr 20 2015
  * Author: Sergio Quiñonez
  * copyright 2011-2015 COTOKO.com™
  *
  * More info: http://www.prestacheap.com/items/6/Newsletter-Popup-for-PrestaShop
  *
  **/

include_once('../../../config/config.inc.php');
include_once('../ctk_newsletterpopup.php');

$module = new CTK_NewsletterPopup();

$design = array(
	'theme' => 'fashion',
	'show_on_devices' => '1',
	'show_content_top' => '1',
	'show_news_form' => '1',
	'show_content_bottom' => '1',
	'effect' => 'fade',
	'speed' => '400',
	'position' => 'fixed',
	'margin_top' => '30',
	'width' => 'large',
	'screen_background' => '000000',
	'screen_opacity' => '0.55',
	'popup_background' => 'FFFFFF',
	'border_size' => '1',
	'border_radius' => '0',
	'border_color' => '3D2821',
	'custom_css' => '.reveal-modal button, .reveal-modal .button {
  background-color: #6c463a;
   border-color: #5f3e33;
}
.reveal-modal button:hover, .reveal-modal button:focus, .reveal-modal .button:hover, .reveal-modal .button:focus {
  background-color: #54372d;
  border-color: #4b3128;
  color: white;
}
.reveal-modal button.disabled, .reveal-modal button[disabled], .reveal-modal .button.disabled, .reveal-modal .button[disabled] {
  background-color: #a86e5a;
  border-color: #9a6553;
}
.reveal-modal input[type="text"][name=np_email] {
  padding-left: 65px !important;
  background: url('.$module->_url.'themes/fashion/images/email.png) #ffffff no-repeat 10px; !important;
  border-color: #6c463a;
  color: #31191b;
}
.reveal-modal input[type="text"][name=np_email]:focus {
  background: url('.$module->_url.'themes/fashion/images/email.png) #fefdee no-repeat 10px; !important;
  border-color: #54372d;
}
.reveal-modal input[type="text"][name=np_email][disabled] {
  background-color: #f1dfc0 !important;
}
.reveal-modal .close-reveal-modal {
  color: #6c463a;
 }
.reveal-modal .close-reveal-modal:hover {
  color: #472e26;
  text-shadow: 0 0 5px #815445;
}
@media only screen and (min-width: 64.063em) {
  .reveal-modal .cnt-btm {
    float: left !important;
    width: 60% !important;
    border-right: 1px solid !important;
    border-color: #ebd4ad !important;
  }
  .reveal-modal .cnt-btm p, .reveal-modal .cnt-btm h1 {
    text-align: right !important;
  }
  .reveal-modal .nws-frm {
    float: right !important;
    width: 40% !important;
  }
  .reveal-modal .ipt-btn div:first-child  {
    padding-right: 0px !important;
  }
  .reveal-modal .ipt-btn div:first-child +div  {
    padding-left: 0px !important;
  }
}'
);

$lang = array(
	'en' => array(
		'content_top' => '<p><a href="http://www.prestacheap.com/items/6/Newsletter-Popup-for-PrestaShop" target="_blank"><img src="'.$module->_url.'themes/fashion/images/mode.png" alt="prestacheap special offer" width="100%" height="auto" /></a></p>',
		'input_text' => 'enter your e-mail',
		'button_text' => 'Subscribe',
		'content_bottom' => '<h1 style="background: none;">Subscribe to our Newsletter</h1>
<p>Subscribe to our newsletter and discover the best offers and news, subscribe today and you will receive a voucher code, which could be used in your next purchase.</p>'),

	'fr' => array(
		'content_top' => '<p><a href="http://www.prestacheap.com/items/6/Newsletter-Popup-for-PrestaShop" target="_blank"><img src="'.$module->_url.'themes/fashion/images/mode.png" alt="prestacheap offre spéciale" width="100%" height="auto" /></a></p>',
		'input_text' => 'entrez votre e-mail',
		'button_text' => 'Souscrire',
		'content_bottom' => '<h1 style="background: none;">Abonnez-vous à notre Newsletter</h1>
<p>Abonnez-vous à notre newsletter et découvrir les meilleures offres et des nouvelles, abonnez-vous dès aujourd hui et vous recevrez un code de réduction, ce qui pourrait être utilisé dans votre prochain achat.</p>'),

	'es' => array(
		'content_top' => '<p><a href="http://www.prestacheap.com/items/6/Newsletter-Popup-for-PrestaShop" target="_blank"><img src="'.$module->_url.'themes/fashion/images/mode.png" alt="prestacheap oferta especial" width="100%" height="auto" /></a></p>',
		'input_text' => 'escriba su e-mail',
		'button_text' => 'Suscribirse',
		'content_bottom' => '<h1 style="background: none;">Suscribase a nuestro Boletin</h1>
<p>Suscribase a nuestro boletin y descubra las mejores ofertas y novedades, suscribase hoy y usted recibira un codigo de descuento, que se podra utilizar en su proxima compra.</p>')
);