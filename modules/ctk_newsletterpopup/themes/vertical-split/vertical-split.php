<?php
/**
  * Newsletter Popup for PrestaShop by COTOKO.com
  * Version: 2.0.13 -- Apr 20 2015
  * Author: Sergio Quiñonez
  * copyright 2011-2015 COTOKO.com™
  *
  * More info: http://www.prestacheap.com/items/6/Newsletter-Popup-for-PrestaShop
  *
  **/

include_once('../../../config/config.inc.php');
include_once('../ctk_newsletterpopup.php');

$module = new CTK_NewsletterPopup();

$design = array(
	'theme' => 'vertical-split',
	'show_on_devices' => '1',
	'show_content_top' => '1',
	'show_news_form' => '1',
	'show_content_bottom' => '1',
	'effect' => 'fade',
	'speed' => '400',
	'position' => 'absolute',
	'margin_top' => '30',
	'width' => 'medium',
	'screen_background' => '000000',
	'screen_opacity' => '0.55',
	'popup_background' => 'FFFFFF',
	'border_size' => '1',
	'border_radius' => '0',
	'border_color' => '343b49',
	'custom_css' => '.reveal-modal button, .reveal-modal .button {
  background-color: #343b49;
   border-color: #2f3542; }
.reveal-modal button:hover, .reveal-modal button:focus, .reveal-modal .button:hover, .reveal-modal .button:focus {
  background-color: #2f3542;
  color: white; }
.reveal-modal button.disabled, .reveal-modal button[disabled], .reveal-modal .button.disabled, .reveal-modal .button[disabled] {
  background-color: #2f3542; }
.reveal-modal input[type="text"][name=np_email] {
  padding-left: 65px !important;
  background: url('.$module->_url.'themes/vertical-split/images/email.png) #ffffff no-repeat 10px; !important;
  border-color: #343b49;
  color: #31191b; }
.reveal-modal input[type="text"][name=np_email]:focus {
  background: url('.$module->_url.'themes/vertical-split/images/email.png) #fefdee no-repeat 10px; !important;
  border-color: #2f3542; }
.reveal-modal input[type="text"][name=np_email][disabled] {
  background-color: #ebebeb !important; }
.reveal-modal .close-reveal-modal {
  color: #343b49; }
.reveal-modal .close-reveal-modal:hover {
  color: #2f3542;
  text-shadow: 0 0 5px #343b49; }
.reveal-modal .cnt-btm {
  margin-right: 0 !important; }
  @media only screen and (max-width: 1025px) {
    .reveal-modal .cnt-btm {
      margin-left: 0 !important; } }
@media only screen and (min-width: 1026px) {
  .reveal-modal .row .ipt-btn .large-8 {
    padding-left: 0;
    padding-right: 0; }
  .reveal-modal .row .ipt-btn .large-4 {
    padding-left: 0; }
  .reveal-modal .row .alr-bx .large-12 {
    padding-left: 0; } }'
);

$lang = array(
	'en' => array(
		'content_top' => '<p><a href="http://www.prestacheap.com/items/6/Newsletter-Popup-for-PrestaShop" target="_blank"><img src="'.$module->_url.'themes/vertical-split/images/popup-banner.jpg" alt="prestacheap special offer" width="100%" height="auto" /></a></p>',
		'input_text' => 'enter your e-mail',
		'button_text' => 'Subscribe',
		'content_bottom' => '<div style="text-align: center; margin-top: 15%;"><p><img src="'.$module->_url.'themes/vertical-split/images/your-logo.png" alt=""></p><p>&nbsp;</p><p>Subscribe to our newsletter and discover the best offers and news, subscribe today and you will receive a voucher code, which could be used in your next purchase.</p><p>&nbsp;</p></div>'),

	'fr' => array(
		'content_top' => '<p><a href="http://www.prestacheap.com/items/6/Newsletter-Popup-for-PrestaShop" target="_blank"><img src="'.$module->_url.'themes/vertical-split/images/popup-banner.jpg" alt="prestacheap offre spéciale" width="100%" height="auto" /></a></p>',
		'input_text' => 'entrez votre e-mail',
		'button_text' => 'Souscrire',
		'content_bottom' => '<div style="text-align: center; margin-top: 15%;"><p><img src="'.$module->_url.'themes/vertical-split/images/your-logo.png" alt=""></p><p>&nbsp;</p><p>Abonnez-vous à notre newsletter et découvrir les meilleures offres et des nouvelles, abonnez-vous dès aujourd hui et vous recevrez un code de réduction, ce qui pourrait être utilisé dans votre prochain achat.</p><p>&nbsp;</p></div>'),

	'es' => array(
		'content_top' => '<p><a href="http://www.prestacheap.com/items/6/Newsletter-Popup-for-PrestaShop" target="_blank"><img src="'.$module->_url.'themes/vertical-split/images/popup-banner.jpg" alt="prestacheap oferta especial" width="100%" height="auto" /></a></p>',
		'input_text' => 'escriba su e-mail',
		'button_text' => 'Suscribirse',
		'content_bottom' => '<div style="text-align: center; margin-top: 15%;"><p><img src="'.$module->_url.'themes/vertical-split/images/your-logo.png" alt=""></p><p>&nbsp;</p><p>Suscribase a nuestro boletin y descubra las mejores ofertas y novedades, suscribase hoy y usted recibira un codigo de descuento, que se podra utilizar en su proxima compra.</p><p>&nbsp;</p></div>')
);