<?php
/**
  * Newsletter Popup for PrestaShop by COTOKO.com
  * Version: 2.0.13 -- Apr 20 2015
  * Author: Sergio Quiñonez
  * copyright 2011-2015 COTOKO.com™
  *
  * More info: http://www.prestacheap.com/items/6/Newsletter-Popup-for-PrestaShop
  *
  **/

include('../../config/config.inc.php');
include('ctk_newsletterpopup.php');

$module = new CTK_NewsletterPopup(true);

if (!Module::isInstalled($module->name))
	exit;

$email = Tools::getValue('subs_newsletter_email');
$token = Tools::getValue('token');

require_once('../../header.php');
$result = $module->confirmEmail($module->_id_np, $email, $token);
echo '	<script type="text/javascript">
			var ctk_np_disPopup = true;
		</script>
		<link rel="stylesheet" type="text/css" href="'._MODULE_DIR_.'ctk_newsletterpopup/css/verification.css"/>
		<div class="'.($result['err'] == 1 ? 'warning_inline' : 'success_inline').'">'.$result['msg'].'</div>';
require_once('../../footer.php');
