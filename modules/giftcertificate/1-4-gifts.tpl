<div class="block">
<h4 class="block title_block">{l s='Gifts that I bought' mod='giftcertificate'}</h4>

{if isset($gift_sent)}
	<p class="alert alert-success">
		{l s='Your friend received gift certificate' mod='giftcertificate'}
	</p>
{/if}

<table id="order-list" class="table table-bordered footab default footable-loaded footable">
		<thead> 
			<tr>
				<th class="first_item">{l s='Gift:' mod='giftcertificate'}</th>
                <th class="item">{l s='Gift from:' mod='giftcertificate'}</th>
				<th class="item">{l s='friend email:' mod='giftcertificate'}</th>
                <th class="item">{l s='friend name:' mod='giftcertificate'}</th>
				<th class="item">{l s='Send' mod='giftcertificate'}</th>
			</tr>
		</thead>
	<tbody>
		{foreach from=$gifts key=id item=gift}
				<tr class="first_item ">
                    <form method="POST" id="form_gift_{$gift.id_giftcertlist}" name="form_gift_{$gift.id_giftcertlist}">
                        <input type="hidden" name="send_gift_voucher" value="{$gift.voucher}"/>
                        <input type="hidden" name="send_gift_idgiftcertlist" value="{$gift.id_giftcertlist}"/>
                        <input type="hidden" name="send_gift_idorder" value="{$gift.id_order}"/>
    					<td>{$gift.voucher}</td>
                        <td><input type="text" name="from" id="from" value="{$gift.from}"/></td>
    					<td><input type="text" name="femail" id="femail" value="{$gift.femail}"/></td>
                        <td><input type="text" name="fname" id="fname" value="{$gift.fname}"/></td>
                        <td><a class="button" onclick="form_gift_{$gift.id_giftcertlist}.submit();">{l s='Send' mod='giftcertificate'}</a></td>
                    </form>
				</tr>
		{/foreach}
	</tbody>
</table>

</div>