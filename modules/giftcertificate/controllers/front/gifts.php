<?php
include_once(dirname(__FILE__).'../../../giftcertificate.php');
class giftcertificategiftsModuleFrontController extends ModuleFrontController{	
	public function initContent(){
	   if (Tools::isSubmit('send_gift_voucher')){
	       $thismodule = new giftcertificate();
	       $verify=giftcertlist::getByIdCustomer(Context::getContext()->customer->id, Tools::getValue('send_gift_idorder'));
           if (isset($verify[0]['voucher']) && Validate::isEmail(Tools::getValue('femail'))){
               $giftcertlist = new giftcertlist($verify[0]['id_giftcertlist']);
               $giftcertlist->femail=Tools::getValue('femail');
               $giftcertlist->fname=Tools::getValue('fname');
               $giftcertlist->from=Tools::getValue('from');
               $giftcertlist->update();

               $giftorder=new Order($giftcertlist->id_order);
               
    	       $templateVars['{voucher}']=Tools::getValue('send_gift_voucher');
               $templateVars['{friend}']=Tools::getValue('fname');
               
                        
                         if ($thismodule->psversion()==5 || $thismodule->psversion()==6){
                            $cartRule=new CartRule(CartRule::getIdByCode(Tools::getValue('send_gift_voucher')));
                            $voucher_value=null;
                            if ($cartRule->reduction_amount>0){
                                $voucher_currency = new Currency($cartRule->reduction_currency);
                                $voucher_currency_sign = $voucher_currency->sign;
                                $voucher_value = $cartRule->reduction_amount." ". $voucher_currency_sign;
                                if ($cartRule->free_shipping==1){
                                    if ($voucher_value==null){
                                       $voucher_value=$this->l('Free shipping');
                                    } else {
                                       $voucher_value.=" + ".$this->l('Free shipping');
                                    }
                                }                                
                            } elseif ($cartRule->reduction_percent>0){
                                $voucher_value=$cartRule->reduction_percent."%";
                                if ($cartRule->free_shipping==1){
                                    if ($voucher_value==null){
                                       $voucher_value=$this->l('Free shipping');
                                    } else {
                                       $voucher_value.=" + ".$this->l('Free shipping');
                                    }
                                }
                            } elseif ($cartRule->free_shipping==1){
                                if ($voucher_value==null){
                                   $voucher_value=$thismodule->l('Free shipping');
                                } else {
                                   $voucher_value.=" + ".$thismodule->l('Free shipping');
                                }
                            }
                        }
                        
                        $templateVars['{voucher_date_from}']=$cartRule->date_from;
                        $templateVars['{voucher_date_to}']=$cartRule->date_to;
                        $templateVars['{voucher_value}']=$voucher_value;
                        $templateVars['{from}']=Tools::getValue('from');
                        $templateVars['{voucher_description}']=$cartRule->description;
               
               
               
               
               
               
               $id_lang=$giftorder->id_lang;
               $id_shop=NULL;
               if ($id_lang == null){ $id_lang = Context::getContext()->language->id;}
               if ($id_shop == null){ $id_shop = Context::getContext()->shop->id;}
               if (Mail::Send(
                    $id_lang,
                    $giftcertlist->mail_template_friend,
                    Mail::l('Gift voucher from friend', $id_lang), 
                    $templateVars, 
                    strval(Tools::getValue('femail')), 
         			NULL, 
         			strval(Configuration::get('PS_SHOP_EMAIL', null, null, $id_shop)), 
         			strval(Configuration::get('PS_SHOP_NAME', null, null, $id_shop)), 
         			NULL, 
         			NULL, 
         			dirname(__FILE__).'/../../mails/',
         			false,
         			$id_shop
        	   )){
        	       $this->context->smarty->assign(array('gift_sent' =>1));
        	   }
           }
	   }
       
	   $module=new giftcertificate();
       parent::initContent();
	   $this->context->smarty->assign(
           array(
              'gifts' => giftcertlist::getByIdCustomer($this->context->customer->id),
        	  'customer' => $this->context->customer
           )
       );
        
		$this->setTemplate('gifts.tpl');	
	}
}