<?php 
/**
 * @author MyPresta.eu | Milos "VEKIA" Myszczuk <support@mypresta.eu>
 * All rights reserved! Copying, duplication strictly prohibited
 * http://www.mypresta.eu - prestashop modules
 */
 
class giftcertificate extends Module {
	function __construct(){
        ini_set("display_errors", 0);
        error_reporting(0);
		$this->name = 'giftcertificate';
		$this->tab = 'advertising_marketing';
        $this->author = 'MyPresta.eu';
		$this->version = '1.2.8';
		parent::__construct();
        $this->trusted();
		$this->displayName = $this->l('Gift certificate - sell gift cards - voucher codes');
		$this->description = $this->l('This module allows you to sell gift certificates, customer will be able to order personal voucher codes, like a gift card');
   
        $this->addproduct=$this->l('Add');
        $this->noproductsfound=$this->l('No products found');
        
        //voucher engine fields to translate
        $this->l('Tax excluded');
        $this->l('Tax included');
        $this->l('Shipping excluded');
        $this->l('Shipping included');
        $this->l('Enabled');
        $this->l('Disabled');
        $this->l('Percent(%)');
        $this->l('Amount');
        $this->l('None');
        $this->l('Value');
        $this->l('Amount');
        $this->l('Order (without shipping)');
        $this->l('Specific product');
        $this->l('Product ID');
        $this->l('enter product ID number');
        $this->l('how to get product id?');
        $this->l('Select categories from list above, use CTRL+click to select multiple categories, CTRL+A to select all of them');
        $this->l('Select products from list above, use CTRL+click to select multiple products, CTRL+A to select all of them');
        $this->l('General settings');
        $this->l('Name');
        $this->l('This will be displayed in the cart summary, as well as on the invoice');
        $this->l('Description');
        $this->l('For your eyes only. This will never be displayed to the customer');
        $this->l('Voucher length');
        $this->l('How many characters will be used to generate voucher code');
        $this->l('Enable sufix');
        $this->l('Turn this option on if you want to enable sufix for your voucher code. It will be added AFTER generated code like CODE_sufix.');
        $this->l('Sufix');
        $this->l('Define sufix for your voucher code');
        $this->l('Enable prefix');
        $this->l('Turn this option on if you want to enable prefix for your voucher code. It will be added BEFORE generated code like prefix_CODE.');
        $this->l('Prefix');
        $this->l('Define prefix for your voucher code');
        $this->l('Highlight');
        $this->l('If the voucher is not yet in the cart, it will be displayed in the cart summary.');
        $this->l('Partial use');
        $this->l('Only applicable if the voucher value is greater than the cart total.
If you do not allow partial use, the voucher value will be lowered to the total order amount. If you allow partial use, however, a new voucher will be created with the remainder.');
        $this->l('Priority');
        $this->l('Cart rules are applied by priority. A cart rule with a priority of "1" will be processed before a cart rule with a priority of "2".');
        $this->l('Active');
        $this->l('Conditions');
        $this->l('Expiration time');
        $this->l('Define how long (in days) voucher code will be active');
        $this->l('Minimum amount');
        $this->l('You can choose a minimum amount for the cart either with or without the taxes and shipping.');
        $this->l('Total available');
        $this->l('The cart rule will be applied to the first "X" customers only.');
        $this->l('Total available for each user');
        $this->l('A customer will only be able to use the cart rule "X" time(s).');
        $this->l('Add rule concerning categories');
        $this->l('Add rule concerning products');
        $this->l('Actions');
        $this->l('Free shipping');
        $this->l('Apply a discount');
        $this->l('Apply discount to');
        $this->l('Turn this option on if you want dont want to allow to use this code with other voucher codes');
        $this->l('Uncombinable with other codes');
        $this->l('Select manufacturers from list above, use CTRL+click to select multiple products, CTRL+A to select all of them');
        $this->l('Add rule concerning manufacturers');
        $this->l('Add rule concerning attributes');
        $this->l('Select Attributes from list above, use CTRL+click to select multiple products, CTRL+A to select all of them');
        $this->l('Cheapest product');
        $this->l('Selected products');
        
        
        //automatic update notification code
        $this->mkey="nlc";       
        if (@file_exists('../modules/'.$this->name.'/key.php'))
            @require_once ('../modules/'.$this->name.'/key.php');
        else if (@file_exists(dirname(__FILE__) . $this->name.'/key.php'))
            @require_once (dirname(__FILE__) . $this->name.'/key.php');
        else if (@file_exists('modules/'.$this->name.'/key.php'))
            @require_once ('modules/'.$this->name.'/key.php');                        
        $this->checkforupdates();     
           
    }
    
    function checkforupdates(){
            
    }      

	public static function psversion($part=1) {
		$version=_PS_VERSION_;
		$exp=$explode=explode(".",$version);
        if ($part==1)
		  return $exp[1];
        if ($part==2)
		  return $exp[2];
        if ($part==3)
		  return $exp[3];
	}

        function trusted(){
            if (_PS_VERSION_ <= "1.6.0.8"){
                if (isset($_GET['controller'])){
                    if ($_GET['controller']=="AdminModules"){
                        if (defined('self::CACHE_FILE_TRUSTED_MODULES_LIST')==true){
                            $context = Context::getContext();
                    		$theme = new Theme($context->shop->id_theme);
                            $xml= simplexml_load_string(file_get_contents(_PS_ROOT_DIR_.self::CACHE_FILE_TRUSTED_MODULES_LIST));
                            if ($xml){
                                $css=$xml->modules->addChild('module');
                                $css->addAttribute('name',$this->name);
                                $xmlcode=$xml->asXML();
                                if (!strpos(file_get_contents(_PS_ROOT_DIR_.self::CACHE_FILE_TRUSTED_MODULES_LIST),$this->name))
                                    file_put_contents(_PS_ROOT_DIR_.self::CACHE_FILE_TRUSTED_MODULES_LIST,$xmlcode);
                            }
                            if (defined('self::CACHE_FILE_DEFAULT_COUNTRY_MODULES_LIST')==true){
                                $xml= simplexml_load_string(file_get_contents(_PS_ROOT_DIR_.self::CACHE_FILE_DEFAULT_COUNTRY_MODULES_LIST));
                                //$xml=new SimpleXMLElement('<modules/>');
                                //$cs=$xml->addChild('modules');
                                if ($xml){
                                $css=$xml->addChild('module');
                                $css->addChild('id',0);
                                $css->addChild('name',"<![CDATA[".$this->name."]]>");
                                $xmlcode=$xml->asXML();
                                $xmlcode=str_replace('&lt;',"<",$xmlcode);
                                $xmlcode=str_replace('&gt;',">",$xmlcode);
                                if (!strpos(file_get_contents(_PS_ROOT_DIR_.self::CACHE_FILE_DEFAULT_COUNTRY_MODULES_LIST),$this->name))
                                    file_put_contents(_PS_ROOT_DIR_.self::CACHE_FILE_DEFAULT_COUNTRY_MODULES_LIST,$xmlcode);                                    
                                }
                            }
                        }
                    }
                }
            }
        }

	function install(){
	    if ($this->psversion()==5 || $this->psversion()==6){
            if (parent::install() == false 
            OR !Configuration::updateValue('update_'.$this->name,'0')
            OR $this->registerHook('actionOrderStatusUpdate') == false
            OR !$this->registerHook('displayCustomerAccount')
            OR $this->installdb() == false      
            ){
                return false;
            }
        } elseif ($this->psversion()==4) {
            if (parent::install() == false 
            OR !Configuration::updateValue('update_'.$this->name,'0')
            OR $this->registerHook('updateOrderStatus') == false
            OR !$this->registerHook('customerAccount')
            OR $this->installdb() == false     
            ){
                return false;
            }
        }
        return true;
	}
    
    private function installdb() {
        $prefix = _DB_PREFIX_;
    	$engine = _MYSQL_ENGINE_;
    	$statements = array(); 
        $statements[] 	= "CREATE TABLE IF NOT EXISTS `${prefix}giftcert` ("
    					. '`id_giftcert` int(10) NOT NULL AUTO_INCREMENT,'
                        . '`id_product` int(10) NOT NULL,'
    					. '`internal_name` VARCHAR(50),'
    					. 'PRIMARY KEY (`id_giftcert`)'
    					. ")";
        $statements[] 	= "CREATE TABLE IF NOT EXISTS `${prefix}giftcertlist` ("
    					. '`id_giftcertlist` int(10) NOT NULL AUTO_INCREMENT,'
                        . '`id_customer` int(10) NOT NULL,'
                        . '`id_order` int(10) NOT NULL,'
    					. 'PRIMARY KEY (`id_giftcertlist`)'
    					. ")";



        foreach ($statements as $statement) {
    		if (!Db :: getInstance()->Execute($statement)) {
    			return false;
    		}
    	}
    	return true;					
   	} 
    
    public function msg_saved(){
        return "<div class=\"conf confirm\">".$this->l('Saved')."</div>";
    }
    
	public function getContent(){
	   	$output=""; 
        
        if (Tools::isSubmit('selecttab')){
            Configuration::updateValue('gc_lasttab',"{$_POST['selecttab']}");
        }
        
        if (Tools::isSubmit('gc_ostates_submit')){
            $output=$this->msg_saved();
            $ostates='';
            foreach (Tools::getValue('gc_ostates') AS $k=>$v){
                $ostates.=$v.",";
            }
            $ostates=substr($ostates,0,-1);
            Configuration::updateValue("gc_ostates", $ostates);
            Configuration::updateValue("gc_counter",Tools::getValue('gc_counter'));
		}
        
        if (Tools::isSubmit('giftcert_new')){
            Configuration::updateValue('gc_lasttab',2);
            $output=$this->msg_saved();
            $ocoupon=new giftcert();
            $ocoupon->internal_name=Tools::getValue('internal_name');
            $ocoupon->id_product=Tools::getValue('id_product');
	        $ocoupon->mail_template=Tools::getValue('mail_template');
	        $ocoupon->mail_template_friend=Tools::getValue('mail_template_friend');
            $ocoupon->add();
        }
        
        if (Tools::isSubmit('giftcert_edit')){
            Configuration::updateValue('gc_lasttab',2);
            $output=$this->msg_saved();
            $ocoupon=new giftcert($_POST['giftcert_edit']);
            $ocoupon->id_product=Tools::getValue('id_product');
            $ocoupon->internal_name=Tools::getValue('internal_name');
	        $ocoupon->mail_template=Tools::getValue('mail_template');
	        $ocoupon->mail_template_friend=Tools::getValue('mail_template_friend');
            $ocoupon->save();
        }
        
        if (Tools::isSubmit('giftcert_remove')){
            Configuration::updateValue('gc_lasttab',2);
            $output=$this->msg_saved();
            $ocoupon=new giftcert($_POST['id_giftcert']);
            $ocoupon->delete();
        }             
        
        if (Tools::isSubmit('save_voucher_settings')){
            $output=$this->msg_saved();
            giftcertificateVoucherEngine::updateVoucher(Tools::getValue('voucherPrefix'),$_POST);
        }
        
       	$output.="";
        return $output.$this->displayForm();
	}
    
     public function searchproduct($search){
        if ($this->psversion()==5 || $this->psversion()==6){
            return Db::getInstance()->ExecuteS('SELECT `id_product`,`name` FROM `'._DB_PREFIX_.'product_lang` WHERE `name` like "%'.$search.'%" AND id_lang="'.Configuration::get('PS_LANG_DEFAULT').'" AND id_shop="'.$this->context->shop->id.'" LIMIT 10');
        } elseif ($this->psversion()==4){
            return Db::getInstance()->ExecuteS('SELECT `id_product`,`name` FROM `'._DB_PREFIX_.'product_lang` WHERE `name` like "%'.$search.'%" LIMIT 10');
        }
    }  
       
    public function prepareorderstates($varname){
        if ($this->psversion()==4){
            global $cookie;
            $this->context = new StdClass();
            $this->context->language = new StdClass();
            $this->context->language->id = $cookie->id_lang;
        }
        $ostates = OrderState::getOrderStates((int)$this->context->language->id);
        foreach (explode(",",Configuration::get($varname)) AS $k=>$v){
            $array[]=$v;    
        }
        $rstates = '';
        foreach ($ostates AS $k=>$v){
            $rstates.="<tr><td><label>".$v['name']."</label></td><td><input type=\"checkbox\" name=\"".$varname."[]\" value=".$v['id_order_state']." ".(in_array($v['id_order_state'],$array) ? 'checked':'')."></td></tr>";
        }
        return $rstates;
    }
    


    public function inconsistency(){
        $prefix = _DB_PREFIX_;
		$engine = _MYSQL_ENGINE_;
        $table['giftcertlist']['id_voucher']['type']="INT";
        $table['giftcertlist']['id_voucher']['length']="9";
        $table['giftcertlist']['id_voucher']['default']="NULL";
        $table['giftcertlist']['voucher']['type']="VARCHAR";
        $table['giftcertlist']['voucher']['length']="60";
        $table['giftcertlist']['voucher']['default']="NULL";
        $table['giftcertlist']['femail']['type']="VARCHAR";
        $table['giftcertlist']['femail']['length']="60";
        $table['giftcertlist']['femail']['default']="NULL";
        $table['giftcertlist']['fname']['type']="VARCHAR";
        $table['giftcertlist']['fname']['length']="60";
        $table['giftcertlist']['fname']['default']="NULL";
        $table['giftcertlist']['from']['type']="VARCHAR";
        $table['giftcertlist']['from']['length']="60";
        $table['giftcertlist']['from']['default']="NULL";
	    $table['giftcertlist']['mail_template_friend']['type']="VARCHAR";
	    $table['giftcertlist']['mail_template_friend']['length']="250";
	    $table['giftcertlist']['mail_template_friend']['default']="gcrt_friend";
	    $table['giftcert']['mail_template']['type']="VARCHAR";
	    $table['giftcert']['mail_template']['length']="250";
	    $table['giftcert']['mail_template']['default']="gcrt_voucher";
	    $table['giftcert']['mail_template_friend']['type']="VARCHAR";
	    $table['giftcert']['mail_template_friend']['length']="250";
	    $table['giftcert']['mail_template_friend']['default']="gcrt_friend";
        $return='';

        
        
        //giftcertlist
        foreach (Db::getInstance()->executeS("SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA ='"._DB_NAME_."' AND TABLE_NAME='"._DB_PREFIX_."giftcertlist'") AS $key => $column){
            $return[$column['COLUMN_NAME']]="1";
        }
        foreach ($table['giftcertlist'] as $key => $field){
            if (!isset($return[$key])){
                $error[$key]['type']="0";
                $error[$key]['message']=$this->l('Database inconsistency, column does not exist');
                if($field['default']=="NULL"){
                   if ($this->runStatement("ALTER TABLE `${prefix}giftcertlist` ADD COLUMN `".$key."` ".$field['type']."(".$field['length'].") NULL DEFAULT NULL")){
                        $error[$key]['fixed']=$this->l('... FIXED!');
                    } else {
                        $error[$key]['fixed']=$this->l('... ERROR!');
                    } 
                } elseif ($field['default']!="X"){
                    if ($this->runStatement("ALTER TABLE `${prefix}giftcertlist` ADD COLUMN `".$key."` ".$field['type']."(".$field['length'].") NULL DEFAULT '".$field['default']."'")){
                        $error[$key]['fixed']=$this->l('... FIXED!');
                    } else {
                        $error[$key]['fixed']=$this->l('... ERROR!');
                    }
                } else {
                     if ($this->runStatement("ALTER TABLE `${prefix}giftcertlist` ADD COLUMN `".$key."` ".$field['type'])){
                         $error[$key]['fixed']=$this->l('... FIXED!');
                     } else {
                         $error[$key]['fixed']=$this->l('... ERROR!');
                     }
                }
                if (isset($field['config'])){
                    Configuration::updateValue($field['config'],'1');
                }
            } else {
                $error[$key]['type']="1";
                $error[$key]['message']=$this->l('OK!');
                $error[$key]['fixed']=$this->l('');
                if (isset($field['config'])){
                    Configuration::updateValue($field['config'],'1');
                }
            }
        }

	    foreach (Db::getInstance()->executeS("SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA ='"._DB_NAME_."' AND TABLE_NAME='"._DB_PREFIX_."giftcert'") AS $key => $column){
		    $return[$column['COLUMN_NAME']]="1";
	    }
	    foreach ($table['giftcert'] as $key => $field){
		    if (!isset($return[$key])){
			    $error[$key]['type']="0";
			    $error[$key]['message']=$this->l('Database inconsistency, column does not exist');
			    if ($field['default']!="X"){
				    if ($this->runStatement("ALTER TABLE `${prefix}giftcert` ADD COLUMN `".$key."` ".$field['type']."(".$field['length'].") NULL DEFAULT '".$field['default']."'")){
					    $error[$key]['fixed']=$this->l('... FIXED!');
				    } else {
					    $error[$key]['fixed']=$this->l('... ERROR!');
				    }
			    } else {
				    if ($this->runStatement("ALTER TABLE `${prefix}giftcert` ADD COLUMN `".$key."` ".$field['type'])){
					    $error[$key]['fixed']=$this->l('... FIXED!');
				    } else {
					    $error[$key]['fixed']=$this->l('... ERROR!');
				    }
			    }
			    if (isset($field['config'])){
				    Configuration::updateValue($field['config'],'1');
			    }
		    } else {
			    $error[$key]['type']="1";
			    $error[$key]['message']=$this->l('OK!');
			    $error[$key]['fixed']=$this->l('');
			    if (isset($field['config'])){
				    Configuration::updateValue($field['config'],'1');
			    }
		    }
	    }
        
        $form.='<table class="inconsistency"><tr><td colspan="4" style="text-align:center">'.$this->l('GIFT CARDS').'</td></tr>';
        foreach ($error as $column => $info){
            $form.="<tr><td class='inconsistency".$info['type']."'></td><td>".$column."</td><td>".$info['message']."</td><td>".$info['fixed']."</td></tr>";
        }
        $form.="</table>";
        return $form;
    }
    
    public function runStatement($statement){
       if (@!Db :: getInstance()->Execute($statement)) {
        return false;
	   }
        return true;
    }    
    
    

	public function displayForm(){
        if (Configuration::get('gc_lasttab')==1){$selected1="active";}else{$selected1="";}
        if (Configuration::get('gc_lasttab')==21){$selected21="active";}else{$selected21="";}
        if (Configuration::get('gc_lasttab')==2 || Configuration::get('gc_lasttab')==3 || Configuration::get('gc_lasttab')==5 || Configuration::get('gc_lasttab')==6){$selected2="active";}else{$selected2="";}
        if (Configuration::get('gc_lasttab')==333){$selectedXXX="active";}else{$selectedXXX="";}
        
        


        if (Configuration::get('gc_lasttab')==1){
            $form.= '
            <form id="gc_states_form" action="'.$_SERVER['REQUEST_URI'].'" method="post" enctype="multipart/form-data" >
            <fieldset style="position:relative; margin-top:20px;">
			<legend>'.$this->l('Main settings').'</legend>
                <div style="display:block; clear:both; text-align:center; overflow:hidden;">
                    <div style="display:block; clear:both; margin-top:20px;">
    			        <label>'.$this->l('Create voucher code only if order status is').':</label>
    			        <div class="margin-form" style="text-align:left;">
                        <table>'.$this->prepareorderstates('gc_ostates').'</table>
    			        </div>	
                    </div>
                </div>
                    <div style="margin-top:20px; clear:both; overflow:hidden; display:block; text-align:center">
                	   <input type="submit" name="gc_ostates_submit" class="button" value="'.$this->l('save').'">
                   	</div>
            </fieldset>
            </form>
            ';  
        }
        
        if (Configuration::get('gc_lasttab')==21){
            $form.=$this->inconsistency();
        }
        
        if (Configuration::get('gc_lasttab')==2){
            if ($this->psversion()==4){
                global $cookie;
                $this->context->language->id=$cookie->id_lang;
            }
            $form.='<div id="topmenu-horizontal-module">
                        <div class="addnew" onclick="selectform3.submit();"><span class="img"></span><span class="txt">'.$this->l('Add new').'</span></div>
                    </div>';
            $form.='
            <fieldset style="position:relative; margin-top:20px;">
            <legend><img src="'.$this->_path.'logo.gif" alt="" title="" />'.$this->l('Gift Certificates').'</legend>'."
            <ul class=\"slides\">";
            foreach (giftcert::getThemAll() as $k=>$v){
               $giftproduct=new Product($v['id_product'],false,$this->context->language->id);
               $form.="
               <li>
                   <span class=\"nb\">".$v['id_giftcert']."</span>
                   <span class='name'>".$v['internal_name']."</span>
                   <span class='product'>".$this->l('Gift Card Product').": ".$giftproduct->name."</span>
                   <span class=\"coupon\" onclick=\"gc_coupon".$v['id_giftcert'].".submit();\"></span>
                   <span class=\"edit\" onclick=\"gc_couponedit".$v['id_giftcert'].".submit();\"></span>
                   <span class=\"remove\" onclick=\"gc_couponremove".$v['id_giftcert'].".submit();\"></span>".'
                   <form name="giftcertcoupon" id="gc_coupon'.$v['id_giftcert'].'" action="'.$_SERVER['REQUEST_URI'].'" method="post"><input type="hidden" name="selecttab" value="5"><input type="hidden" name="id_giftcert" value="'.$v['id_giftcert'].'"></form>
                   <form name="giftcertcouponedit" id="gc_couponedit'.$v['id_giftcert'].'" action="'.$_SERVER['REQUEST_URI'].'" method="post"><input type="hidden" name="selecttab" value="6"><input type="hidden" name="id_giftcert" value="'.$v['id_giftcert'].'"></form>
                   <form name="giftcertcouponremove" id="gc_couponremove'.$v['id_giftcert'].'" action="'.$_SERVER['REQUEST_URI'].'" method="post"><input type="hidden" name="giftcert_remove" value="1"/><input type="hidden" name="selecttab" value="2"><input type="hidden" name="id_giftcert" value="'.$v['id_giftcert'].'"></form>'."
               </li>";
            }
            $form.="</ul>
            <div style=\"margin-top:20px;\">
                <span class=\"coupon\" style=\"width:335px; text-align:right; background-position:left;\">".$this->l('click on coupon image to personalize voucher code')."</span>
            </div>
            </fieldset>";    
        }
        
        if (Configuration::get('gc_lasttab')==3){

	        $mail_friend = '';
	        $mail_owner = '';
	        foreach ($this->getMailFiles() as $key => $value){
		        $selected_friend = '';
		        $selected_owner = '';
		        if ($key == 'gcrt_friend')
		        {
			        $selected_friend = 'selected="yes"';
		        }
		        if ($key == 'gcrt_voucher')
		        {
			        $selected_owner = 'selected="yes"';
		        }
	            $mail_friend .= '<option value="'.$key.'" '.$selected_friend.'>'.$key.'</option>';
		        $mail_owner .= '<option value="'.$key.'" '.$selected_owner.'>'.$key.'</option>';
	        }

            $form='<div id="topmenu-horizontal-module">
                    <div class="btnpro back" onclick="selectform2.submit();"><span class="img"></span><span class="txt">'.$this->l('list').'</span></div>
                    <div class="btnpro save" onclick="giftcert_new.submit();"><span class="img"></span><span class="txt">'.$this->l('save').'</span></div>
                   </div>';
            $form.='
                <form id="giftcert_new" action="'.$_SERVER['REQUEST_URI'].'" method="post">
                <input type="hidden" name="giftcert_new" value="1"/>
                <fieldset style="position:relative; margin-top:20px;">
                <legend><img src="'.$this->_path.'logo.gif" alt="" title="" />'.$this->l('Define Gift Certificate').'</legend>
                
                <label>'.$this->l('Name').'</label>
                <div class="margin-form">    
                    <input type="text" name="internal_name" value="" />
                    <p class="clear">'.$this->l('Internal name of action, it will be visible only for you').'</p>
                </div>
                
                <label>'.$this->l('Product').'</label>
                <div class="margin-form">    
                    <input type="text" name="product_name" value="" class="gc_search"/>
                    <div class="gc_search_result">
                    
                    </div>
                    <input type="hidden" name="id_product" id="gc_idproduct"/>
                    <p class="clear">'.$this->l('Search for product, it will be treated as a gift code').'</p>
                    <div class="bootstrap">
                        <div class="warn warning alert alert-info">
                            '.$this->l('Just type product name in field above, then just select product').'
                        </div>
                    </div>
                </div>

                <label>'.$this->l('Mail template (gift owner)').'</label>
                <div class="margin-form">
                    <select name="mail_template"/>
                        '.$mail_owner.'
                    </select>
                    <p class="clear"></p>
                    <div class="bootstrap">
                        <div class="warn warning alert alert-info">
                            '.$this->l('Module will use this email template when someone will order this gift').'
                        </div>
                    </div>
                </div>

                <label>'.$this->l('Mail template (mail to friend)').'</label>
                <div class="margin-form">
                    <select name="mail_template_friend"/>
                        '.$mail_friend.'
                    </select>
                    <p class="clear"></p>
                    <div class="bootstrap">
                        <div class="warn warning alert alert-info">
                            '.$this->l('Module will use this email template when someone will send gift to friend').'
                        </div>
                    </div>
                </div>
                
                </fieldset>
                </form>
            ';                   
        }
        
        if (Configuration::get('gc_lasttab')==5){
            $form='<div id="topmenu-horizontal-module">
                    <div class="btnpro back" onclick="selectform2.submit();"><span class="img"></span><span class="txt">'.$this->l('list').'</span></div>
                   </div>';
            $vn=new giftcertificateVoucherEngine("gcrt".(int)Tools::getValue('id_giftcert'));
            
            $form.='
		    <form id="oc'.(int)Tools::getValue('id_giftcert').'form" action="'.$_SERVER['REQUEST_URI'].'" method="post" enctype="multipart/form-data" >
            <input type="hidden" name="id_giftcert" value="'.Tools::getValue('id_giftcert').'">
                <fieldset style="margin-bottom:10px;">
                    <legend>'.$this->l('Voucher settings').'</legend>
                    '.$vn->generateForm().'
                    <input type="submit" name="save_voucher_settings" class="button" value="'.$this->l('Save').'"/>
                </fieldset>
            </form>';         
        }

        if (Configuration::get('gc_lasttab')==6){
            $giftcert = new giftcert(Tools::getValue('id_giftcert'));
            $product=new Product($giftcert->id_product,false,$this->context->language->id);

	        $mail_friend = '';
	        $mail_owner = '';
	        foreach ($this->getMailFiles() as $key => $value){
		        $selected_friend = '';
		        $selected_owner = '';
		        if ($giftcert->mail_template_friend == $key)
		        {
			        $selected_friend = 'selected="yes"';
		        }
		        if ($giftcert->mail_template == $key)
		        {
			        $selected_owner = 'selected="yes"';
		        }
		        $mail_friend .= '<option value="'.$key.'" '.$selected_friend.'>'.$key.'</option>';
		        $mail_owner .= '<option value="'.$key.'" '.$selected_owner.'>'.$key.'</option>';
	        }

            
            $form='<div id="topmenu-horizontal-module">
                    <div class="btnpro back" onclick="selectform2.submit();"><span class="img"></span><span class="txt">'.$this->l('list').'</span></div>
                    <div class="btnpro save" onclick="giftcert_edit.submit();"><span class="img"></span><span class="txt">'.$this->l('save').'</span></div>
                   </div>';
            $form.='
                <form id="giftcert_edit" action="'.$_SERVER['REQUEST_URI'].'" method="post">
                <input type="hidden" name="giftcert_edit" value="'.Tools::getValue('id_giftcert').'"/>
                <fieldset style="position:relative; margin-top:20px;">
                <legend><img src="'.$this->_path.'logo.gif" alt="" title="" />'.$this->l('Define Gift Certificate').'</legend>
                
                <label>'.$this->l('Name').'</label>
                <div class="margin-form">    
                    <input type="text" name="internal_name" value="'.$giftcert->internal_name.'" />
                    <p class="clear">'.$this->l('Internal name of action, it will be visible only for you').'</p>
                </div>
                
                <label>'.$this->l('Product').'</label>
                <div class="margin-form">
                    <input type="text" name="product_name" value="'.$product->name.'" class="gc_search"/> '.$this->l('or enter ID of product:').'
                    <input type="text" name="id_product" value="'.$product->id.'" id="gc_idproduct"/> 
                    <div class="gc_search_result">
                    
                    </div>
                    <p class="clear">'.$this->l('Search for product, it will be treated as a gift code').'</p>
                    <div class="bootstrap">
                        <div class="warn warning alert alert-info">
                            '.$this->l('Just type product name in field above, then just select product').'
                        </div>
                    </div>
                </div>

                <label>'.$this->l('Mail template (gift owner)').'</label>
                <div class="margin-form">
                    <select name="mail_template"/>
                        '.$mail_owner.'
                    </select>
                    <p class="clear"></p>
                    <div class="bootstrap">
                        <div class="warn warning alert alert-info">
                            '.$this->l('Module will use this email template when someone will order this gift').'
                        </div>
                    </div>
                </div>

                <label>'.$this->l('Mail template (mail to friend)').'</label>
                <div class="margin-form">
                    <select name="mail_template_friend"/>
                        '.$mail_friend.'
                    </select>
                    <p class="clear"></p>
                    <div class="bootstrap">
                        <div class="warn warning alert alert-info">
                            '.$this->l('Module will use this email template when someone will send gift to friend').'
                        </div>
                    </div>
                </div>
                </fieldset>
                </form>
            ';                   
        }      
        
        
        if (Configuration::get('gc_lasttab')==333){
         $form.='
              
                <fieldset style="position:relative; margin-top:20px;">
                <legend><img src="'.$this->_path.'logo.gif" alt="" title="" />'.$this->l('Guide').'</legend>
                <h1>'.$this->l('Define gift card products').'</h1>
                <iframe width="560" height="315" src="//www.youtube.com/embed/iY1kiu3NaX0" frameborder="0" allowfullscreen></iframe>
                </br></br></br>
                <h1>'.$this->l('Install & configure module').'</h1>
                <iframe width="560" height="315" src="//www.youtube.com/embed/z6SeQITtgZk" frameborder="0" allowfullscreen></iframe>
                </fieldset>';
        }
        $form.='<script src="../modules/giftcertificate/js/script.js"></script>';
	    return '
        <style>
            .language_flags {text-align:left;}
            #topmenu-horizontal-module {overflow:hidden; background-color: #F8F8F8; border: 1px solid #CCCCCC; margin-bottom: 10px; padding: 10px 0; border-radius: 3px; -moz-border-radius: 3px; -webkit-border-radius: 3px;}
            #topmenu-horizontal-module .addnew, #topmenu-horizontal-module .btnpro {-webkit-border-radius:4px; -moz-border-radius:4px; -border-radius:4px; padding:5px; margin-right:10px; cursor:pointer; width:52px; height:52px; display:inline-block; float:right; text-align:center; border:1px dotted #c0c0c0; }
            #topmenu-horizontal-module .addnew:hover, #topmenu-horizontal-module .btnpro:hover {border:1px solid #bfbfbf; background:#f3f3f3;}
            #topmenu-horizontal-module span.img {margin:auto; width:32px; height:32px; display:block;}
            #topmenu-horizontal-module span.txt {margin-top:3px; width:52px; display:block; text-align:center;}
            #topmenu-horizontal-module .addnew span.img {background:url(\''._MODULE_DIR_.$this->name.'/img/add.png\') no-repeat center;}
            #topmenu-horizontal-module .save span.img {background:url(\''._MODULE_DIR_.$this->name.'/img/on.png\') no-repeat center;}
            #topmenu-horizontal-module .back span.img {background:url(\''._MODULE_DIR_.$this->name.'/img/back.png\') no-repeat center;}
            
                .slides li {font-size:10px!important; list-style: none; margin: 0 0 4px 0; padding: 15px 10px; background-color: #F4E6C9; border: #CCCCCC solid 1px; color:#000;}
                .slides li:hover {border:1px #000 dashed;}
                .slides li .name {font-size:18px!important;}
                .slides li .nb {color:#FFF; background:#000; padding:5px 10px; font-size:18px; font-weight:bold; margin-right:10px; }
                .slides .product {margin-left:50px}
               
                .activate {display:inline-block; float:left; padding-right:3px;}
                .remove {opacity:0.3; position:relative; top:-1px; width:24px; height:24px; display:inline-block; float:right; background:url("../modules/giftcertificate/img/trash.png") top no-repeat; cursor:pointer;}
                .edit {margin-right:6px; opacity:0.3; position:relative;  width:24px; height:24px; display:inline-block; float:right; background:url("../modules/giftcertificate/img/edit.png") top no-repeat; cursor:pointer;}
                .coupon {opacity:0.9; position:relative;  top:-1px; width:31px; height:24px; display:inline-block; float:right; background:url("../modules/giftcertificate/img/coupon.png") top no-repeat; cursor:pointer;}
                
                .coupon:hover, .remove:hover, .edit:hover, .activate:hover { opacity:1.0; }
                .edit,.remove {margin-right:5px;}
                
                
        </style>   
        <form name="selectform1" id="selectform1" action="'.$_SERVER['REQUEST_URI'].'" method="post"><input type="hidden" name="selecttab" value="1"></form>
        <form name="selectform2" id="selectform2" action="'.$_SERVER['REQUEST_URI'].'" method="post"><input type="hidden" name="selecttab" value="2"></form>
        <form name="selectform3" id="selectform3" action="'.$_SERVER['REQUEST_URI'].'" method="post"><input type="hidden" name="selecttab" value="3"></form>
        <form name="selectform21" id="selectform21" action="'.$_SERVER['REQUEST_URI'].'" method="post"><input type="hidden" name="selecttab" value="21"></form>
        <form name="selectformXXX" id="selectformXXX" action="'.$_SERVER['REQUEST_URI'].'" method="post"><input type="hidden" name="selecttab" value="333"></form>
        '."<div id='cssmenu'>
            <ul>
               <li class='bgver'><a><span>v".$this->version."</span></a></li>
               <li class='$selected1'><a href='#' onclick=\"selectform1.submit()\"><span>".$this->l('Main settings')."</span></a></li>
               <li class='$selected2'><a href='#' onclick=\"selectform2.submit()\"><span>".$this->l('Define vouchers')."</span></a></li>
               <li class='$selectedXXX'><a href='#' onclick=\"selectformXXX.submit()\"><span>".$this->l('Guide')."</span></a></li>
               <li class='$selected21'><a href='#' onclick=\"selectform21.submit()\"><span>".$this->l('Upgrade check')."</span></a></li>
               <li style='position:relative; display:inline-block; float:right; width:65px;'><a href='http://mypresta.eu' target='_blank' title='prestashop modules'><img src='../modules/".$this->name."/logo-white.png' alt='prestashop modules' style=\"position:absolute; top:17px; right:16px;\"/></a></li>
               <li style='position:relative; display:inline-block; float:right;'><a href='http://mypresta.eu/contact.html' target='_blank'><span>".$this->l('Support')."</span></a></li>
               <li style='position:relative; display:inline-block; float:right;'><a href='http://mypresta.eu/modules/advertising-and-marketing/gift-cards-sell-voucher-codes.html' target='_blank'><span>".$this->l('Updates')."</span></a></li>
            </ul>
        </div>".'<link href="../modules/'.$this->name.'/css.css" rel="stylesheet" type="text/css" />'.$form.'
        <!-- Start of MyPresta Zendesk Widget script -->
<script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(c){n=document.domain,r.src=\'javascript:var d=document.open();d.domain="\'+n+\'";void(0);\',o=s}o.open()._l=function(){var o=this.createElement("script");n&&(this.domain=n),o.id="js-iframe-async",o.src=e,this.t=+new Date,this.zendeskHost=t,this.zEQueue=a,this.body.appendChild(o)},o.write(\'<body onload="document._l();">\'),o.close()}("//assets.zendesk.com/embeddable_framework/main.js","prestasupport.zendesk.com");/*]]>*/</script>
<!-- End of MyPresta Zendesk Widget script -->';
	}  
    
    public function getOrdersNbByIdCustomer($id){
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT count(*) as count FROM `'._DB_PREFIX_.'orders` WHERE id_customer="'.$id.'" AND current_state IN ('.Configuration::get('gc_ostates').')');
    }

	public function getMailFiles(){
		$dir = "../modules/".$this->name."/mails/en/";
		$dh  = opendir($dir);
		while (false !== ($filename = readdir($dh))) {
			if ($filename != ".." && $filename != "." && $filename != ""){
				$explode = explode(".",$filename);
				$files[$explode[0]] = $explode[0];
			}
		}
		return $files;
	}
 
    public function generateModCurrencySelect($name,$selected=null){
        $input='<select name="'.$name.'">';
        foreach (Currency::getCurrencies() AS $currency){
            $input.="<option ".($selected==$currency['id_currency'] ? 'selected="yes"':'')." value=\"".$currency['id_currency']."\">".$currency['name']." ".$currency['sign']."</option>";
        }
        $input.='</select>';
        return $input;
    } 
    
    public function hookcustomerAccount($params){
        global $smarty;
        global $cookie;
        if ($this->psversion()==4){
            $link = new Link();
            $smarty->assign(array("link"=>$link));
            if ($cookie->logged==1){
                return $this->display(__FILE__, 'views/templates/hook/my-account.tpl');
            }    
        } else {
            if ($this->context->customer->isLogged()==1){
                if ($this->psversion()==6 || $this->psversion()==5){
      		        return $this->display(__FILE__, 'my-account-16.tpl');
       	        } 
            }
        }
    }
    
    public function hookupdateOrderStatus($params){
        return $this->hookactionOrderStatusUpdate($params);
    }
        
    public function hookactionOrderStatusUpdate($params){
        $jest=0;
        foreach (explode(",",Configuration::get('gc_ostates')) AS $k=>$v){
             if ($v==$params['newOrderStatus']->id){
                $jest=1;
             }
        }

        
        $die=0;
        if ($jest==1){
            $order = new Order ($params['id_order']);
            $customer = new Customer ($order->id_customer);
        
        
            $gift_product_is_in_order=0;
            foreach (giftcert::getThemAll() AS $key=>$value){
                foreach ($order->getProducts() AS $number=>$product){
                    if ($value['id_product']==$product['product_id']){
                       for ($i=1; $i<=$product['product_quantity']; $i++){
                           $coupon_code[]=$value['id_giftcert'];     
                       } 
                       $gift_product_is_in_order=1;
                    }
                }        
            }
            
            $verify=giftcertlist::getByIdCustomer($order->id_customer, $order->id);
            if (isset($verify['0']['id_giftcertlist'])){
                $already_in_db=1;
            } else {
                $already_in_db=0;
            }
            if ($already_in_db==0 && $jest==1 && $gift_product_is_in_order==1){
                if ($this->psversion()==5 || $this->psversion()==6 || $this->psversion()==4){
                    foreach ($coupon_code AS $k=>$v){
                        $gcrt=new giftcertificateVoucherEngine("gcrt".$v);
                        $voucher=$gcrt->AddVoucherCode("gcrt".$v);
	                    $gift_template = new giftcert($v);
               
                        $giftcertlist = new giftcertlist();
                        $giftcertlist->id_order=$order->id;
                        $giftcertlist->id_customer=$order->id_customer;
                        $giftcertlist->id_voucher=$voucher->id;
	                    $giftcertlist->mail_template_friend = $gift_template->mail_template_friend;
                        if ($this->psversion()==4){
                            $giftcertlist->voucher=$voucher->name;
                        } else {
                            $giftcertlist->voucher=$voucher->code;    
                        }
                        $giftcertlist->add();
                        
                        
                         if ($this->psversion()==5 || $this->psversion()==6){
                            $cartRule=new CartRule(CartRule::getIdByCode($voucher->code));
                            $voucher_value=null;
                            if ($cartRule->reduction_amount>0){
                                $voucher_currency = new Currency($cartRule->reduction_currency);
                                $voucher_currency_sign = $voucher_currency->sign;
                                $voucher_value = $cartRule->reduction_amount." ". $voucher_currency_sign;
                                if ($cartRule->free_shipping==1){
                                    if ($voucher_value==null){
                                       $voucher_value=$this->l('Free shipping');
                                    } else {
                                       $voucher_value.=" + ".$this->l('Free shipping');
                                    }
                                }
                            } elseif ($cartRule->reduction_percent>0){
                                $voucher_value=$cartRule->reduction_percent."%";
                                if ($cartRule->free_shipping==1){
                                    if ($voucher_value==null){
                                       $voucher_value=$this->l('Free shipping');
                                    } else {
                                       $voucher_value.=" + ".$this->l('Free shipping');
                                    }
                                }                                
                            } elseif ($cartRule->free_shipping==1){
                                if ($voucher_value==null){
                                   $voucher_value=$this->l('Free shipping');
                                } else {
                                   $voucher_value.=" + ".$this->l('Free shipping');
                                }
                            }
                        } else {
                            $cartRule=new Discount(Discount::getIdByName($voucher->name));
                            if ($cartRule->id_discount_type==2){
                                $voucher_currency = new Currency($cartRule->id_currency);
                                $voucher_currency_sign = $voucher_currency->sign;
                                $voucher_value = $cartRule->value." ". $voucher_currency_sign;   
                            } elseif ($cartRule->id_discount_type==1){
                                $voucher_value=$cartRule->value."%";
                            } elseif ($cartRule->id_discount_type==3){
                                $voucher_value=$this->l('Free shipping');
                            }
                        }
                        
                        if ($this->psversion()==4){
                            $templateVars['{voucher}']=$voucher->name;
                        } else {
                            $templateVars['{voucher}']=$voucher->code;
                        }
                        $templateVars['{voucher_date_from}']=$cartRule->date_from;
                        $templateVars['{voucher_date_to}']=$cartRule->date_to;
                        $templateVars['{voucher_value}']=$voucher_value;
                        $templateVars['{voucher_description}']=$cartRule->description;
                        
                                                
                        if ($this->psversion()==4){
                                global $cookie;
                                 if ($id_lang == null)
                                    $id_lang = $cookie->id_lang;
                                    
                                    if(Mail::Send(
                                    $order->id_lang,
                                    $gift_template->mail_template,
                                    Mail::l('Your Gift Certificate', $order->id_lang), 
                                    $templateVars, 
                                    strval($customer->email), 
                         			NULL, 
                         			strval(Configuration::get('PS_SHOP_EMAIL', null, null)), 
                         			strval(Configuration::get('PS_SHOP_NAME', null, null)), 
                         			NULL, 
                         			NULL, 
                         			dirname(__FILE__).'/mails/',
                         			false)){
                         			}
                        } else {
                      		if ($id_lang == null){ $id_lang = Context::getContext()->language->id;}
                            if ($id_shop == null){ $id_shop = Context::getContext()->shop->id;}
                                Mail::Send(
                                    $order->id_lang,
                                    $gift_template->mail_template,
                                    Mail::l('Your Gift Certificate', $order->id_lang), 
                                    $templateVars, 
                                    strval($customer->email), 
                         			NULL, 
                         			strval(Configuration::get('PS_SHOP_EMAIL', null, null, $id_shop)), 
                         			strval(Configuration::get('PS_SHOP_NAME', null, null, $id_shop)), 
                         			NULL, 
                         			NULL, 
                         			dirname(__FILE__).'/mails/',
                         			false,
                         			$id_shop
                        	   );
                           }
                       }
                }  
            } 
        }    
    } 		      
}

if (file_exists(_PS_MODULE_DIR_ .'giftcertificate/voucherengine/engine.php')){ 
    require_once _PS_MODULE_DIR_ .'giftcertificate/voucherengine/engine.php'; 
}
if (giftcertificate::psversion()==4){require_once _PS_MODULE_DIR_ . 'giftcertificate/models/giftcert14.php';} else { require_once _PS_MODULE_DIR_ . 'giftcertificate/models/giftcert.php';}
if (giftcertificate::psversion()==4){require_once _PS_MODULE_DIR_ . 'giftcertificate/models/giftcertlist14.php';} else { require_once _PS_MODULE_DIR_ . 'giftcertificate/models/giftcertlist.php';}

class giftcertificateUpdate extends giftcertificate {  
    public static function version($version){
        $version=(int)str_replace(".","",$version);
        if (strlen($version)==3){$version=(int)$version."0";}
        if (strlen($version)==2){$version=(int)$version."00";}
        if (strlen($version)==1){$version=(int)$version."000";}
        if (strlen($version)==0){$version=(int)$version."0000";}
        return (int)$version;
    }
    
    public static function encrypt($string){
        return base64_encode($string);
    }
    
    public static function verify($module,$key,$version){
        if (ini_get("allow_url_fopen")) {
             if (function_exists("file_get_contents")){
                $actual_version = @file_get_contents('http://dev.mypresta.eu/update/get.php?module='.$module."&version=".self::encrypt($version)."&lic=$key&u=".self::encrypt(_PS_BASE_URL_.__PS_BASE_URI__));
             }
        }
        Configuration::updateValue("update_".$module,date("U"));
        Configuration::updateValue("updatev_".$module,$actual_version); 
        return $actual_version;
    }
}

?>