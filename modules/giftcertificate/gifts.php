<?php
require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');
include_once(dirname(__FILE__).'/giftcertificate.php');

       
	   $module=new giftcertificate();
       global $smarty;
       global $cookie;
       $thismodule = new StdClass();
       $thismodule->context = new StdClass();
       $thismodule->context->customer = new StdClass();
       //parent::initContent();
       $thismodule->context->customer->id = $cookie->id_customer;


	   if (Tools::isSubmit('send_gift_voucher')){
	       $verify=giftcertlist::getByIdCustomer($thismodule->context->customer->id, Tools::getValue('send_gift_idorder'));
           if (isset($verify[0]['voucher']) && Validate::isEmail(Tools::getValue('femail'))){
               $giftcertlist = new giftcertlist($verify[0]['id_giftcertlist']);
               $giftcertlist->id_giftcertlist = $verify[0]['id_giftcertlist'];
               $giftcertlist->femail=Tools::getValue('femail');
               $giftcertlist->fname=Tools::getValue('fname');
               $giftcertlist->from=Tools::getValue('from');
               $giftcertlist->update();
    	       $templateVars['{voucher}']=Tools::getValue('send_gift_voucher');
               $templateVars['{friend}']=Tools::getValue('fname');
               $templateVars['{from}']=Tools::getValue('from');
               $id_lang=NULL;
               $id_shop=NULL;
               
               $giftorder=new Order($giftcertlist->id_order);
               $id_lang=$giftorder->id_lang;
               
               if ($id_lang == null)
                 $id_lang = $cookie->id_lang;
                 
                 
               if (Mail::Send(
                    $id_lang,
                    $giftcertlist->mail_template_friend,
                    Mail::l('Gift voucher from your friend', $id_lang), 
                    $templateVars, 
                    strval(Tools::getValue('femail')), 
         			NULL, 
         			strval(Configuration::get('PS_SHOP_EMAIL', null, null)), 
         			strval(Configuration::get('PS_SHOP_NAME', null, null)), 
         			NULL, 
         			NULL, 
         			dirname(__FILE__).'/mails/',
         			false
        	   )){
        	       $smarty->assign(array('gift_sent' =>1));
        	   }
           }
	   }

	   $smarty->assign(
           array(
              'gifts' => giftcertlist::getByIdCustomer($thismodule->context->customer->id,NULL),
        	  'customer' => $thismodule->context->customer
           )
       );
        
    include(dirname(__FILE__).'/../../header.php');  
	echo Module::display(dirname(__FILE__).'/giftcertificate.php', '1-4-gifts.tpl');
    include(dirname(__FILE__).'/../../footer.php');	
