<?php

/**
 * ObjectModel for gift certificate module table. 
 * @author MyPresta.eu | Milos "VEKIA" Myszczuk <support@mypresta.eu>
 */

class giftcert extends ObjectModel {
	public $id_giftcert;   
    public $internal_name;
    public $id_product;
	public $mail_template;
	public $mail_template_friend;
	public static $definition = array(
  		'table' 	=> 'giftcert',
  		'primary' 	=> 'id_giftcert',
  		'multilang' => false,
 		'fields' => array(
   			'id_giftcert' => array('type' => ObjectModel :: TYPE_INT),
            'id_product' => array('type' => ObjectModel :: TYPE_INT),        
            'internal_name' => array('type' => ObjectModel :: TYPE_STRING),
		    'mail_template' => array('type' => ObjectModel :: TYPE_STRING),
		    'mail_template_friend' => array('type' => ObjectModel :: TYPE_STRING)
  		),
	);
    
    public function __construct($id_giftcert = null){
		parent::__construct($id_giftcert);
	}
    
    public static function getThemAll(){
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT * FROM `'._DB_PREFIX_.'giftcert`');
    }

}