<?php

/**
 * ObjectModel for gift certificate module table. 
 * @author MyPresta.eu | Milos "VEKIA" Myszczuk <support@mypresta.eu>
 */

class giftcert extends ObjectModel {
	public $id_giftcert;   
    public $internal_name;
	public $mail_template;
	public $mail_template_friend;
    protected $table = 'giftcert';
	protected $identifier = 'id_giftcert';
    protected $fieldsValidate = array(
    'id_giftcert' => 'isInt',
    'id_product' => 'isInt',
	'mail_template' => 'isAnything',
	'mail_template_friend' => 'isAnything',
    'internal_name' => 'isAnything');
    
	public function getFields(){
		parent::validateFields();
		$fields['id_giftcert'] = (int)($this->id_giftcert);
        $fields['id_product'] = (int)($this->id_product);
		$fields['mail_template_friend'] = (string)($this->mail_template_friend);
		$fields['mail_template'] = (string)($this->mail_template);
        $fields['internal_name'] = ($this->internal_name);
		return $fields;
	}    

    public function __construct($id_giftcert = null){
		parent::__construct($id_giftcert);
	}
    
    public static function getThemAll(){
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT * FROM `'._DB_PREFIX_.'giftcert`');
    }  
}