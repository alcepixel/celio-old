<?php

/**
 * ObjectModel for gift certificate module table. 
 * @author MyPresta.eu | Milos "VEKIA" Myszczuk <support@mypresta.eu>
 */

class giftcertlist extends ObjectModel {
	public $id_giftcertlist;   
    public $id_customer;
    public $id_order;
    public $id_voucher;
    public $from;
    public $voucher;
    public $femail;
    public $fname;
	public $mail_template_friend;
	public static $definition = array(
  		'table' 	=> 'giftcertlist',
  		'primary' 	=> 'id_giftcertlist',
  		'multilang' => false,
 		'fields' => array(
   			'id_giftcertlist' => array('type' => ObjectModel :: TYPE_INT),
            'id_customer' => array('type' => ObjectModel :: TYPE_INT),
            'id_order' => array('type' => ObjectModel :: TYPE_INT),
            'id_voucher' => array('type' => ObjectModel :: TYPE_INT),
            'from' => array('type' => ObjectModel :: TYPE_STRING),
            'voucher' => array('type' => ObjectModel :: TYPE_STRING),
            'femail' => array('type' => ObjectModel :: TYPE_STRING),
            'fname' => array('type' => ObjectModel :: TYPE_STRING),
		    'mail_template_friend' => array('type' => ObjectModel :: TYPE_STRING),
  		),
	);
    
    public function __construct($id_giftcertlist = null){
		parent::__construct($id_giftcertlist);
	}
    
    public static function getThemAll(){
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT * FROM `'._DB_PREFIX_.'giftcert`');
    }
    
    public static function getByIdCustomer($id_customer, $id_order=NULL){
        if ($id_order!=NULL){
            return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT * FROM `'._DB_PREFIX_.'giftcertlist` WHERE id_customer="'.$id_customer.'" AND id_order="'.$id_order.'" ');
        } else {
            return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT * FROM `'._DB_PREFIX_.'giftcertlist` WHERE id_customer="'.$id_customer.'"');
        }
    } 

}