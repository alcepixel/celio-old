<?php

/**
 * ObjectModel for gift certificate module table. 
 * @author MyPresta.eu | Milos "VEKIA" Myszczuk <support@mypresta.eu>
 */

class giftcertlist extends ObjectModel {
	public $id_giftcertlist;
    public $id_customer;
    public $id_order;
    public $id_voucher;
    public $fname;
    public $from;
    public $femail;
    public $voucher;
	public $mail_template_friend;
    protected $table = 'giftcertlist';
	protected $identifier = 'id_giftcertlist';
    protected $fieldsValidate = array(
    'id_giftcert' => 'isInt',
    'id_customer' => 'isInt',
    'from' => 'isAnything',
    'femail' => 'isAnything',
    'fname' => 'isAnything',
    'id_voucher' => 'isInt',
    'voucher' => 'isAnything',
    'mail_template_friend' => 'isAnything',
    'id_order' => 'isInt');
    
	public function getFields(){
		parent::validateFields();
		$fields['id_giftcertlist'] = (int)($this->id_giftcertlist);
        $fields['id_customer'] = (int)($this->id_customer);
        $fields['id_voucher'] = (int)($this->id_voucher);
        $fields['femail'] = ($this->femail);
        $fields['voucher'] = ($this->voucher);
        $fields['fname'] = ($this->fname);
        $fields['from'] = ($this->from);
		$fields['mail_template_friend'] = ($this->mail_template_friend);
        $fields['id_order'] = (int)($this->id_order);
		return $fields;
	}    

    public function __construct($id_giftcert = null){
		parent::__construct($id_giftcert);
	}
    
    public static function getThemAll(){
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT * FROM `'._DB_PREFIX_.'giftcertlist`');
    }

    public static function getByIdCustomer($id_customer, $id_order=NULL){
        if ($id_order!=NULL){
            return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT * FROM `'._DB_PREFIX_.'giftcertlist` WHERE id_customer="'.$id_customer.'" AND id_order="'.$id_order.'" ');
        } else {
            return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT * FROM `'._DB_PREFIX_.'giftcertlist` WHERE id_customer="'.$id_customer.'"'); 
        }
    } 
    
}