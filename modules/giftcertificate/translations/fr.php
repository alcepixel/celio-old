<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{giftcertificate}prestashop>giftcertificate_570337c1e963ddb8b1d5a131cc25b0ea'] = 'Bon après commande -  de récompenses';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_c6cb3526601e2ae8949e027ed75f0a8a'] = 'Commandez programme de récompenses pour votre prestashop, module de loyauté de la clientèle - coupons de subvention pour vos clients fidèles';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_befcac0f9644a7abee43e69f49252ac4'] = 'HT';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_f4a0d7cb0cd45214c8ca5912c970de13'] = 'TTC';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_89bea8045e50a337d8ce9849a4e1633c'] = 'Frais de port exclus';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_f7b8d9d5bbba937644a29be14e2654ed'] = 'Frais de port inclus';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Activé';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_b9f5c797ebbf55adccdd8539a65a0241'] = 'Désactivé';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_564a74f3d3576da5e4fd105d38d6ef2e'] = 'Pour cent (%)';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_b2f40690858b404ed10e62bdf422c704'] = 'montant';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_6adf97f83acf6453d4a6a4b1070f3754'] = 'aucun';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_689202409e48743b914713f96d93947c'] = 'valeur';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_629b9591d5085cf14edd26c9d376bb3a'] = 'Classement (hors frais de port)';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_1b343552de249749420ddd0fff731e0c'] = 'produit spécifique';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_97f08a40f22a625d0cbfe03db3349108'] = 'ID du produit';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_8b49bc762911d74d6dd1349f7ac6fb43'] = 'entrer le numéro d\'identification produit';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_fbf0982b71be3d5034953d0a872153d7'] = 'la façon d\'obtenir l\'identité de produit?';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_4c355c9d6f89662acc3eed126e542389'] = 'Sélectionnez les catégories de la liste ci-dessus, utilisez CTRL + clic pour sélectionner plusieurs catégories, CTRL + A pour sélectionner tous les';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_782c3d46506f65edf485511745a1d285'] = 'Sélectionnez des produits de la liste ci-dessus, utilisez CTRL + clic pour sélectionner plusieurs produits, CTRL + A pour sélectionner tous les';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_de62775a71fc2bf7a13d7530ae24a7ed'] = 'paramètres généraux';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_49ee3087348e8d44e1feda1917443987'] = 'nom';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_4c475566b05830fe1becc2968b859a44'] = 'Il apparaîtra dans la charrette résumé, ainsi que sur la facture';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_b5a7adde1af5c87d7fd797b6245c2a39'] = 'Description';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_69023ed432595b46e74a711654eca10b'] = 'Pour vos yeux seulement. Ce ne sera jamais affiché pour le client';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_bdeda8d77da8afb65101774e3ccd74f6'] = 'bon longueur';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_a6ba7d164fcff15b45f9bb1790dc2223'] = 'Combien de caractères sera utilisé pour générer le code coupon';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_eb544f51f31da6a80eec11967ce90476'] = 'Activer Sufix';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_e7bcf45b0f49a89272fd0cb768be8167'] = 'Activez cette option si vous souhaitez activer Sufix pour votre code de bon. Il sera ajouté code généré APRÈS comme CODE_sufix.';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_98351bc5bca4eeccc4435655a946ee4a'] = 'Sufix';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_4270f20ee2f5d11e67701843ad8c05c2'] = 'Définir Sufix pour votre code de réduction';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_d44e84b45a9c714937234c6f6f8d9fe6'] = 'Activer préfixe';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_a0c4762707178773b63e6ecfb2ce4e63'] = 'Activez cette option si vous souhaitez activer le préfixe de votre code de bon. Il sera ajouté AVANT code généré comme prefix_CODE.';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_716de874a0d74f25c0aa8c444c3a7539'] = 'préfixe';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_40920c2f76d35d98063f761f577bb1dd'] = 'Définir le préfixe de votre code de réduction';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_0b90582f4589d84be89f5b847d4d1ed1'] = 'Mettez en surbrillance.';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_9c0cc5911e6caceaa3f19e4bcf264ab0'] = 'Si le bon n\'est pas encore dans le panier, il sera affiché dans le panier résumé.';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_f507c7574b6368f942ea9a2b959d8ec9'] = 'utilisation partielle';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_502996d9790340c5fd7b86a5b93b1c9f'] = 'priorité';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_0fcc630f536dae07ee7c1ac612654c1d'] = 'Panier règles sont appliquées par ordre de priorité. Une règle de panier avec une priorité de \"1\" sera traitée avant une règle de panier avec une priorité de \"2\".';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_4d3d769b812b6faa6b76e1a8abaece2d'] = 'actif';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_229eb04083e06f419f9ac494329f957d'] = 'Conditions';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_6daa5846ade3355d4c3cf75f789f38f3'] = 'Temps d\'expiration';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_ac8b0d789becf11dc0a218cdbc8cfd00'] = 'Définir combien de temps (en jours) code coupon sera actif';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_9f6e99bdd4184b83dc478d0ab1b4cbf7'] = 'montant minimum';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_5b41d7dc2da5ea15baf3e2d62d820ac2'] = 'Vous pouvez choisir un montant minimum pour le panier avec ou sans les taxes et frais d\'expédition.';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_7d058d4cdbc3a0f480bd472b62a0bf53'] = 'totale disponible';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_cfc9489072f04e808ce4e01948b7fef8'] = 'Le panier règle sera appliquée pour les premiers clients «X» uniquement.';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_9379f6f6c128b182600047d1b3090bc9'] = 'Total disponible pour chaque utilisateur';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_15391e9d908034a4458cd78296a945a8'] = 'Un client ne sera en mesure d\'utiliser la règle de panier de temps \"X\" (s).';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_8468e6555c4c3a561d6ea5a99fb7b147'] = 'Ajouter une règle concernant les catégories';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_7f1d1563173fe93ac28538261e1240a5'] = 'Ajouter une règle concernant les produits';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_06df33001c1d7187fdd81ea1f5b277aa'] = 'actes';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_29aa46cc3d2677c7e0f216910df600ff'] = 'livraison gratuite';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_fc6341fa76fe93b837d748563e0a60c1'] = 'Appliquer une réduction';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_4ec534e482b91898178fa7c6b2235459'] = 'Appliquer une réduction à';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_05b6d221e28ef1eb21637a949988bc1b'] = 'Activez cette option si vous voulez ne voulez pas autoriser à utiliser ce code avec d\'autres codes de réduction';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_32d683c07e54a344c639af0ac9c4c33b'] = 'Uncombinable avec d\'autres codes';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_fcd31b9b85290f1554b870023d143907'] = 'Sélectionnez les fabricants de la liste ci-dessus, utilisez CTRL + clic pour sélectionner plusieurs produits, CTRL + A pour sélectionner tous les';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_e3df379bfcc158b6aa402229320e4735'] = 'Ajouter une règle concernant les fabricants';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_612af40daf77226c3bb88023cc57647e'] = 'Ajouter une règle concernant les attributs';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_8dba76f054a0384b61ef1028c40bd0f5'] = 'Sélectionnez les attributs de la liste ci-dessus, utilisez CTRL + clic pour sélectionner plusieurs produits, CTRL + A pour sélectionner tous les';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_8bab8ac32605948e59399033c7606222'] = 'produit le moins cher';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_3db9cd44dbbffa69b2ff71ff4d1844ca'] = 'produits sélectionnés';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_5ee348002fb5ec8e0abee8d46d279c75'] = 'Cumulable avec les réductions de prix';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_34846c7932a848c9dae4edfabfe06a08'] = 'Activez cette option si vous voulez autoriser à utiliser ce code avec des réductions de prix';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_50d9d560b468ec07f8b4fe721fcae24e'] = 'Date à partir de';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_bad39a65b5302bb8628585aae59da36b'] = 'Date de fin';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_deaebdcedc2c48ed9b6717fa274c0bb1'] = 'Date d\'expiration, format: YYYY-MM-DD HH:MM:SS';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_9d5361ba29dd9ec9f091bd4590d6681e'] = 'Date, format de départ: YYYY-MM-DD HH:MM:SS';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_4c73ce9e52a25c69cf0dce126d548f98'] = 'Nouvelle version disponible, vérifiez www.MyPresta.eu pour plus d\'informations';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_248336101b461380a4b2391a7625493d'] = 'Enregistré.';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_61093077b60bcb6882d14cb16b730312'] = 'En raison de l\'évolution de l\'âme - ce module doit réinstallation';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_8103112e88495d4caaa29312e5e29314'] = 'principaux paramètres';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_26086925f59ae469e169361a4078159d'] = 'Créer un code de bon que si l\'état des commandes est';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_eda427097375bed8c7653615b0937f13'] = 'Afin contre basée sur';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_ec1ee973b8947391f8f014c76ac7379f'] = 'Les ordres valides';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_c20f8e9f332abef2c33223cd07596994'] = 'Coupons générés pour les commandes';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_aa7ea2281f4de21300b1b23195b3ea79'] = 'Envoyer copie de cet e-mail avec le propriétaire du code de la boutique';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_43781db5c40ecc39fd718685594f0956'] = 'sauver';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_ef61fb324d729c341ea8ab9901e23566'] = 'Ajouter un nouveau';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_1ec032a3b9f7f3fc620b49fe1af1708a'] = 'actions définies';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_c2f0efe562a92dd4972b20b47ab46983'] = 'cliquez sur l\'image pour personnaliser le code coupon de coupons';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_10ae9fc7d453b0dd525d0edf2ede7961'] = 'listes';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_b93704f189c9f34a249fa8d770445e73'] = 'Définir une nouvelle action';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_cc2e276282c5534d5949653750bc9200'] = 'Nom interne de l\'action, il ne sera visible que pour vous';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_3de22f8e0c0e4e67a063e39c7f0908ae'] = 'Les commandes provenant de';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_e0893a6c66e0961abed21bceaea8bf53'] = 'est:';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_388f3fbdbbfd98fae4a0e56c5c444eac'] = 'Combien de commandes recevoir un coupon, définissez gamme de commandes';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_a77dc4aeebad942aad0bb5b57b9e32ae'] = 'Vous pouvez spécifier gamme, par exmaple, le client recevra le coupon si son / sa nombre de commandes est entre vous spécifiez des valeurs dans ce domaine. Exmaple \"de 1 à 3\", \"de 4 à 20\"';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_5494d34a78817e9d30b985a35276f658'] = 'Si vous souhaitez accorder coupon seulement pour première, deuxième, troisième ordre, etc. utiliser des plages comme  DU 1 AU 1, DU 2 AU 2, DU 3 AU 3';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_84311657be7570a479cc7be41bbef0b9'] = 'Montant minimum de commande';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_b5e485b86c321ad1f4c17207e0cb4ddf'] = 'Cochez cette option si vous voulez envoyer un code de bon seulement si la valeur de commande min est égale (ou plus) que ce montant:';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_55ddcecc7577a1e2c7ab3e8b8088a35e'] = 'Spécifiez valeur minimale de commande';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_54472e513b933b2385e504ffe6f93555'] = 'Valeur de la commande maximum';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_ea9216903981cca50d11d5bdbeb84670'] = 'Cochez cette option si vous voulez envoyer un code de bon que si la valeur maximale de l\'ordre est égal (ou moins) est la quantité:';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_1a21f597cd952d3d44bad8a9235b5729'] = 'Spécifiez la valeur maximale de l\'ordre';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_2377be3c2ad9b435ba277a73f0f1ca76'] = 'fabricants';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_8c902089632075bec3e5a9ae0df05cc2'] = 'Cochez cette option si vous voulez envoyer un code de bon uniquement si le produit acheté à la clientèle associée avec des fabricants sélectionnés';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_575a3a0a587f35a1d6ba99d6e2cd470b'] = 'paramètres Voucher';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_c9cc8cce247e49bae79f15173ce97354'] = 'sauver';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_de7b5abed4db9abc9547b5875890446f'] = 'Cochez cette option si vous voulez envoyer un code de bon seulement si la valeur min de commande est égal ce montant:';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_60c5df1932e998c10c06c2b3f81f57cf'] = 'définir bons';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_db5eb84117d06047c97c9a0191b5fffe'] = 'soutien';
$_MODULE['<{giftcertificate}prestashop>giftcertificate_9ac41b6a577daadd588f0fcde0071e8b'] = 'Mises à jour';
