<?php

/**
 * @author www.MyPresta.eu | Milos "VEKIA" Myszczuk <support@mypresta.eu>
 * @version 1.1.0
 * All rights reserved! Copying, duplication strictly prohibited
 * http://www.mypresta.eu - prestashop modules
 */


include_once('../../../config/config.inc.php');
include_once('../../../init.php');
@ini_set("display_errors", 0);
@error_reporting(0);

if (Tools::getValue('action') == 'updateSlidesPosition'){
	$slides = Tools::getValue('elements'.Tools::getValue('hook'));
    foreach ($slides as $position => $id_slide){
	   $res = Db::getInstance()->execute('
			UPDATE `'._DB_PREFIX_.'hbp_block` SET `position` = '.(int)$position.'
			WHERE `id` = '.(int)$id_slide
		);
    }
}

if (isset ($_POST['search'])){
     $result = searchproduct(Tools::getValue('search'));
         if (count($result)>0){
            foreach ($result AS $key=>$value){
                echo '<p style="display:block; clear:both; padding:0px; padding-top:2px; margin:0px;">'.$value['name'].'<span style="display:inline-block; background:#FFF; cursor:pointer; border:1px solid black; padding:0px 3px;margin-left:5px;" onclick="$(\'#selectbox_'.Tools::getValue('selectbox_prefix').'restriction_products_pr\').append(\'<option selected value='.$value['id_product'].'>'.$value['name'].'</option>\');"> ></span></p>';
            }
         } else {
            //echo $thismodule->noproductsfound;
         }
}


function searchproduct($search){
    return Db::getInstance()->ExecuteS('SELECT `id_product`,`name` FROM `'._DB_PREFIX_.'product_lang` WHERE `name` like "%'.$search.'%" AND id_lang="'.Configuration::get('PS_LANG_DEFAULT').'" LIMIT 7');
}