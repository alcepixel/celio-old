<?php
/**
* 2014 NextGroup
*
* NOTICE OF LICENSE
*
** DISCLAIMER
*
*  @author    NextGroup
*  @copyright 2014 NextGroup
*  @license   License of NextGroup
*/

class Config
{
	public $enable;
	public $start_date;
	public $end_date;
	public $bg_color;
	public $content;
	public $newsletter;
	public $newsletter_title;
	public $newsletter_subtitle;

	public function __construct($setting = null, $module_instance)
	{
		$this->enable = new FieldHelper();
		$this->enable->label = $module_instance->l('Enable');
		$this->enable->type = 'yesno';
		$this->enable->name = 'landing[enable]';
		$this->enable->value = isset($setting['enable']) ? $setting['enable'] : '0';

		$this->start_date = new FieldHelper();
		$this->start_date->label = $module_instance->l('Start Date');
		$this->start_date->type = 'date';
		$this->start_date->name = 'landing[start_date]';
		$this->start_date->value = isset($setting['start_date']) ? $setting['start_date'] : '';

		$this->end_date = new FieldHelper();
		$this->end_date->label = $module_instance->l('End Date');
		$this->end_date->type = 'date';
		$this->end_date->name = 'landing[end_date]';
		$this->end_date->value = isset($setting['end_date']) ? $setting['end_date'] : '';

		$this->bg_color = new FieldHelper();
		$this->bg_color->label = $module_instance->l('Background Color');
		$this->bg_color->type = 'color';
		$this->bg_color->name = 'landing[bg_color]';
		$this->bg_color->value = isset($setting['bg_color']) ? $setting['bg_color'] : '#ffffff';
		$this->bg_color->size = 39;
		$this->bg_color->classname = 'mColorPicker';
		$this->bg_color->note = $module_instance->l('Choose a Color with the Color Picker, or Enter an HTML Color');

		$this->content = new FieldHelper();
		$this->content->label = $module_instance->l('Content');
		$this->content->type = 'textarea';
		$this->content->name = 'landing[content]';
		$this->content->value = isset($setting['content']) ? $setting['content'] : '';
		$this->content->lang = true;
		$this->content->classname = 'rte';

		$this->newsletter = new FieldHelper();
		$this->newsletter->label = $module_instance->l('Show Newsletter Subscribe');
		$this->newsletter->type = 'yesno';
		$this->newsletter->name = 'landing[newsletter]';
		$this->newsletter->value = isset($setting['newsletter']) ? $setting['newsletter'] : '1';

		$this->newsletter_title = new FieldHelper();
		$this->newsletter_title->label = $module_instance->l('Newsletter Title');
		$this->newsletter_title->type = 'text';
		$this->newsletter_title->name = 'landing[newsletter_title]';
		$this->newsletter_title->value = isset($setting['newsletter_title']) ? $setting['newsletter_title'] : '';
		$this->newsletter_title->lang = true;

		$this->newsletter_subtitle = new FieldHelper();
		$this->newsletter_subtitle->label = $module_instance->l('Newsletter Sub Title');
		$this->newsletter_subtitle->type = 'text';
		$this->newsletter_subtitle->name = 'landing[newsletter_subtitle]';
		$this->newsletter_subtitle->value = isset($setting['newsletter_subtitle']) ? $setting['newsletter_subtitle'] : '';
		$this->newsletter_subtitle->lang = true;
	}

	public function generateHtml()
	{
		$html = '';

		$html .= $this->enable->generateField();
		$html .= $this->start_date->generateField();
		$html .= $this->end_date->generateField();
		$html .= $this->bg_color->generateField();
		$html .= $this->content->generateField();
		$html .= $this->newsletter->generateField();
		$html .= $this->newsletter_title->generateField();
		$html .= $this->newsletter_subtitle->generateField();
		$html .= '<input type="hidden" name="landing[savingtime]" value="nl_'.date('Y-m-d.H:i:s').'" />';

		return $html;
	}
}
