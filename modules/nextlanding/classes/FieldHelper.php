<?php
/**
* 2014 NextGroup
*
* NOTICE OF LICENSE
*
** DISCLAIMER
*
*  @author    NextGroup
*  @copyright 2014 NextGroup
*  @license   License of NextGroup
*/

class FieldHelper
{
	public $label = '';
	public $type = 'text';
	public $name = '';
	public $id = '';
	public $value = '';
	public $size = 42;
	public $note = '';
	public $lang = false;
	public $classname = '';

	public function generateField()
	{
		$html = '<div class="form-group"><label class="control-label col-lg-3">'.$this->label.'</label><div class="col-lg-9">';

		if ($this->lang) /* lang = true */
		{
			$languages = Language::getLanguages(false);
			$id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');

			foreach ($languages as $language)
			{
				$ele_name = $this->name.'['.$language['id_lang'].']';
				$ele_id = $this->id != '' ? $this->id.'-'.$language['id_lang'] : '';
				$ele_value = isset($this->value[$language['id_lang']]) ? $this->value[$language['id_lang']] : '';

				$html .= '<div class="translatable-field lang-'.$language['id_lang'].'" '.($language['id_lang'] != $id_lang_default ? 'style="display:none"' : '').'>';

				if ($this->type == 'text')				// text
					$html .= '<input type="text" name="'.$ele_name.'" id="'.$ele_id.'" value="'.$ele_value.'" size="'.$this->size.'" class="'.$this->classname.'" />';
				elseif ($this->type == 'textarea')		// textarea
					$html .= '<textarea name="'.$ele_name.'" id="'.$ele_id.'" class="'.$this->classname.'">'.$ele_value.'</textarea>';

				$html .= '<div class="display-lang"><button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">'.$language['iso_code'].' <i class="icon-caret-down"></i></button><div class="dropdown-lang">';

				foreach ($languages as $language)
					$html .= '<p><a href="javascript:hideOtherLanguage('.$language['id_lang'].');" tabindex="-1">'.$language['name'].'</a></p>';

				$html .= '</div></div></div>';
			}
		}
		else 		/* lang = false */
		{
			if ($this->type == 'text')				// text
				$html .= '<input type="text" name="'.$this->name.'" id="'.$this->id.'" value="'.$this->value.'" size="'.$this->size.'" class="'.$this->classname.'" />';

			elseif ($this->type == 'textarea')		// textarea
				$html .= '<textarea name="'.$this->name.'" id="'.$this->id.'" rows="10" cols="80" class="'.$this->classname.'">'.$this->value.'</textarea>';

			elseif ($this->type == 'select') 		// select
			{
				$html .= '<select name="'.$this->name.'" id="'.$this->id.'" style="min-width: 248px;" class="'.$this->classname.'">';

				foreach ($this->size as $option => $value)
					if ($value == $this->value)
						$html .= '<option value="'.$value.'" selected="selected">'.$option.'</option>';
					else
						$html .= '<option value="'.$value.'">'.$option.'</option>';

				$html .= '</select>';
			}
			elseif ($this->type == 'yesno')			// yesno
				$html .= '<img src="../img/admin/enabled.gif" alt="Yes" title="Yes" /><input type="radio" name="'.$this->name.'" id="'.($this->id != '' ? $this->id.'_yes' : '').'" '.($this->value == 1 ? 'checked="checked"' : '').' value="1" /><label class="t" for="'.$this->id.'_yes">Yes</label><img src="../img/admin/disabled.gif" alt="No" title="No" style="margin-left: 10px;" /><input type="radio" name="'.$this->name.'" id="'.($this->id != '' ? $this->id.'_no' : '').'" '.($this->value == 0 ? 'checked="checked" ' : '').' value="0" /><label class="t" for="'.$this->id.'_no">No</label>';

			elseif ($this->type == 'color')			// color
				$html .= '<input type="color" name="'.$this->name.'" id="'.$this->id.'" value="'.$this->value.'" size="'.$this->size.'" class="'.$this->classname.'" data-hex="true" />';

			elseif ($this->type == 'autocomplete')	// autocomplete
				$html .= '<input type="text" id="'.$this->id.'" autocomplete="off" size="42" class="'.$this->classname.'" />';

			elseif ($this->type == 'radio_image')	// radio_image
				foreach ($this->size as $option => $value)
					if ($value == $this->value)
						$html .= '<div class="radio-image"><label class="radioCheck" for="'.($this->id != '' ? $this->id.'_'.$value : '').'">'.$option.'</label><input type="radio" name="'.$this->name.'" value="'.$value.'" checked="checked" id="'.($this->id != '' ? $this->id.'_'.$value : '').'"></div>';
					else
						$html .= '<div class="radio-image"><label class="radioCheck" for="'.($this->id != '' ? $this->id.'_'.$value : '').'">'.$option.'</label><input type="radio" name="'.$this->name.'" value="'.$value.'" id="'.($this->id != '' ? $this->id.'_'.$value : '').'"></div>';

			elseif ($this->type == 'radio') 		// radio
				foreach ($this->size as $option => $value)
					if ($value == $this->value)
						$html .= '<input type="radio" name="'.$this->name.'" value="'.$value.'" checked="checked" id="'.($this->id != '' ? $this->id.'_'.$value : '').'"> <label class="t" for="'.($this->id != '' ? $this->id.'_'.$value : '').'">'.$option.'</label>';
					else
						$html .= '<input type="radio" name="'.$this->name.'" value="'.$value.'" id="'.($this->id != '' ? $this->id.'_'.$value : '').'"> <label class="t" for="'.($this->id != '' ? $this->id.'_'.$value : '').'">'.$option.'</label>';

			elseif ($this->type == 'date')			// date
				$html .= '<input type="text" size="'.$this->size.'"	data-hex="true" class="datepicker" name="'.$this->name.'" value="'.$this->value.'" />';
		}

		$html .= '<p class="help-block">'.$this->note.'</p></div></div>';

		if ($this->type == 'autocomplete') 	// autocomplete
		{
			$html .= '<div id="'.$this->id.'Div" class="list-products">';
			foreach ($this->size as $id => $name)
				$html .= '<p id="product-'.$id.'">#'.$id.' - '.$name.' <span style="cursor: pointer;" onclick="$(this).parent().remove();"><img src="../img/admin/delete.gif" /></span><input type="hidden" name="'.$this->name.'[]" value="'.$id.'" /></p>';

			$html .= '</div>';
		}

		return $html;
	}
}