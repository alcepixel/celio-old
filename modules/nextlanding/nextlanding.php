<?php
/**
* 2014 NextGroup
*
* NOTICE OF LICENSE
*
** DISCLAIMER
*
*  @author    NextGroup
*  @copyright 2014 NextGroup
*  @license   License of NextGroup
*/

if (!defined('_PS_VERSION_'))
	exit;

class NextLanding extends Module
{
	private $_html = '';

	public function __construct()
	{
		$this->name = 'nextlanding';
		$this->tab = 'front_office_features';
		$this->version = '1.0';
		$this->author = 'Nexthemes';
		$this->need_instance = 0;

		parent::__construct();

		$this->displayName = $this->l('Next (02) - Landing Popup');
		$this->description = $this->l('Showing a Landing Popup on the first page load.');

		$this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
	}

	public function install()
	{
		if (!parent::install() ||
			!$this->registerHook('displayFooter') ||
			!Configuration::updateValue('NEXT_LANDING_CONFIG', ''))
			return false;
		return true;
	}

	public function uninstall()
	{
		//$this->_deleteCache();

		if (!parent::uninstall() ||
			!Configuration::deleteByName('NEXT_LANDING_CONFIG'))
			return false;
		return true;
	}

	public function getContent()
	{
		include_once(_PS_MODULE_DIR_.$this->name.'/classes/FieldHelper.php');
		include_once(_PS_MODULE_DIR_.$this->name.'/classes/Config.php');

		if (Tools::isSubmit('landing'))
			$this->_postProcess();

		$this->_html .= $this->_adminHeader();

		$this->_displayForm();

		return $this->_html;
	}

	private function _postProcess()
	{
		$landing = Tools::getValue('landing');

		if (!Configuration::hasKey('NEXT_LANDING_CONFIG', 0, Shop::getContextShopGroupID(true), Shop::getContextShopID(true)))
			Configuration::updateValue('NEXT_LANDING_CONFIG', '');

		Configuration::updateValue('NEXT_LANDING_CONFIG', base64_encode(serialize($landing)), true);

		//$this->_deleteCache();

		$this->_html .= $this->displayConfirmation($this->l('Your settings have been updated.'));
	}

	private function _displayForm()
	{
		$this->_html .= '<form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" method="post" enctype="multipart/form-data" id="form">';

		$this->_fieldsetConfig();

		$this->_html .= '</form>';

		$this->_html .= $this->_javascript();
	}

	private function _fieldsetConfig()
	{
		$base64 = base64_decode(Configuration::get('NEXT_LANDING_CONFIG'), true);
		if ($base64)
			$setting_config = Tools::unSerialize($base64);
		else
			$setting_config = Tools::unSerialize(Configuration::get('NEXT_LANDING_CONFIG'));

		$config = new Config($setting_config, $this);

		$this->_html .= '<fieldset id="set_Config"><legend><img src="'.$this->_path.'logo.gif" alt="" title="" />'.$this->l('Settings').'</legend>';

		$this->_html .= $config->generateHtml();

		$this->_html .= '</fieldset>';
	}

	private function _adminHeader()
	{
		$this->context->controller->addJS(_PS_JS_DIR_.'tiny_mce/tiny_mce.js');
		$this->context->controller->addJS(_PS_JS_DIR_.'tinymce.inc.js');
		$this->context->controller->addJS(_PS_JS_DIR_.'admin/tinymce.inc.js');
		$this->context->controller->addJqueryPlugin('colorpicker');
		$this->context->controller->addJqueryUI('ui.datepicker');

		$html = '
		<style>
			.nobootstrap{min-width: 905px;}.nobootstrap label{width: 180px;}p.help-block{clear:both;}
			.radio-image{float: left;margin-right: 15px;margin-bottom: 15px;}.radio-image input{display: block;margin: 0 auto;}.radio-image label{float: none;}
			.col-lg-9{padding-left:10px;max-width: 680px;}.col-lg-9 .display-lang{float: right;display: inline;}.col-lg-9 .dropdown-lang{display: none;}.col-lg-9 .open .dropdown-lang{display: block;}
			.dropdown-lang{position: absolute;background: #F1F1F1;padding: 0 8px;border-radius: 3px;border: 1px solid #C9C9C9;}.dropdown-lang a{text-decoration: none;}.dropdown-lang a:hover{text-decoration: underline;}
		</style>
		<script type="text/javascript">allowEmployeeFormLang = 0;</script>

		<div class="bootstrap" style="text-align: right;"><h3 style="float: left;">'.$this->displayName.'</h3><button type="submit" class="btn btn-default" onclick="$(\'#form\').submit();"><i class="process-icon-save"></i>'.$this->l('Save').'</button></div>
		';

		return $html;
	}

	private function _javascript()
	{
		$html = '
			<script type="text/javascript">
				var iso = "'.$this->context->language->iso_code.'" ;
				var pathCSS = "'._THEME_CSS_DIR_.'" ;
				var ad = "'.dirname($_SERVER['PHP_SELF']).'" ;
				
				$(document).ready(function(){
					tinySetup();
					$(".datepicker").datepicker({
						prevText: "",
						nextText: "",
						dateFormat: "yy-mm-dd"
					});
				});
			</script>
		';
		return $html;
	}

	private function _deleteCache()
	{
		$this->_clearCache('nextlanding.tpl');
	}

	private function preProcess()
	{
		$base64 = base64_decode(Configuration::get('NEXT_LANDING_CONFIG'), true);
		if ($base64)
			$landing = Tools::unSerialize($base64);
		else
			$landing = Tools::unSerialize(Configuration::get('NEXT_LANDING_CONFIG'));
		$id_lang = $this->context->language->id;

		if (!$landing['enable'])
			return false;

		$this->smarty->assign(array(
			'bg_color' => $landing['bg_color'] != '' ? $landing['bg_color'] : '#fff',
			'content' => isset($landing['content'][$id_lang]) ? $landing['content'][$id_lang] : '',
			'newsletter' => $landing['newsletter'],
			'newsletter_title' => isset($landing['newsletter_title'][$id_lang]) ? $landing['newsletter_title'][$id_lang] : '',
			'newsletter_subtitle' => isset($landing['newsletter_subtitle'][$id_lang]) ? $landing['newsletter_subtitle'][$id_lang] : '',
			'start_date' => strtotime($landing['start_date']) * 1000,
			'end_date' => strtotime($landing['end_date']) * 1000,
			'savingtime' => $landing['savingtime'],
			'subcribe_url' => $this->_path.'subcribe.php'
		));

		return true;
	}

	public function hookDisplayFooter()
	{
		if ($this->preProcess())
			return $this->display(__FILE__, 'nextlanding.tpl');
	}
}
