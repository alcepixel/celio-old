<?php
/**
* 2014 NextGroup
*
* NOTICE OF LICENSE
*
** DISCLAIMER
*
*  @author    NextGroup
*  @copyright 2014 NextGroup
*  @license   License of NextGroup
*/

require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');
require_once(dirname(__FILE__).'/nextlanding.php');

$context = Context::getContext();
$action = Tools::getValue('action');

$blocknewsletter = ModuleCore::getInstanceByName('blocknewsletter');
$html = $blocknewsletter->hookDisplayLeftColumn(null);

echo Tools::jsonEncode($html);
