{*
* 2014 NextGroup
*
* NOTICE OF LICENSE
*
** DISCLAIMER
*
*  @author    NextGroup
*  @copyright 2014 NextGroup
*  @license   License of NextGroup
*}

<!-- MODULE Next - Landing Popup -->
<div class="next-landing" id="next_landing" style="background-color: {$bg_color|trim}; display: none;">
	<div class="content">
		{$content|stripslashes}
	</div>
	{if $newsletter}
	<div class="newsletter-form">
		<h3>{$newsletter_title|escape:html:'UTF-8'}</h3>
		<p class="sub-title">{$newsletter_subtitle|escape:html:'UTF-8'}</p>
		<form action="" method="post" id="landing_newsletter_form">
			<div><input class="inputNew" id="newsletter-input" type="text" name="email" size="18" placeholder="{l s='Put your email here...' mod='nextlanding'}" /></div>
			<input type="hidden" name="action" value="0" />
			<input type="hidden" name="submitNewsletter" value="1" />
			<div class="result" style="display: none;"></div>
			<div class="surscribe-thanks">
				<input type="submit" value="{l s='Subscribe' mod='nextlanding'}" class="button subscribe" />
				<a href="no-thanks" class="button nothanks">{l s='No, Thanks' mod='nextlanding'}</a>
			</div>
		</form>
	</div>
	{/if}
	<a title="Close" class="nextlanding-close" href="close"><i class="fa fa-times"></i></a>
</div>
<script type="text/javascript">
//<![CDATA[
//




$(window).load(function () {

	/*!
 * jQuery Cookie Plugin
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2011, Klaus Hartl
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.opensource.org/licenses/GPL-2.0
 */
(function($) {
	$.cookie = function(key, value, options) {

		// key and at least value given, set cookie...
		if (arguments.length > 1 && (!/Object/.test(Object.prototype.toString.call(value)) || value === null || value === undefined)) {
			options = $.extend({}, options);

			if (value === null || value === undefined) {
				options.expires = -1;
			}

			if (typeof options.expires === 'number') {
				var days = options.expires, t = options.expires = new Date();
				t.setDate(t.getDate() + days);
			}

			value = String(value);

			return (document.cookie = [
				encodeURIComponent(key), '=', options.raw ? value : encodeURIComponent(value),
				options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
				options.path    ? '; path=' + options.path : '',
				options.domain  ? '; domain=' + options.domain : '',
				options.secure  ? '; secure' : ''
			].join(''));
		}

		// key and possibly options given, get cookie...
		options = value || {};
		var decode = options.raw ? function(s) { return s; } : decodeURIComponent;

		var pairs = document.cookie.split('; ');
		for (var i = 0, pair; pair = pairs[i] && pairs[i].split('='); i++) {
			if (decode(pair[0]) === key) return decode(pair[1] || ''); // IE saves cookies with empty string as "c; ", e.g. without "=" as opposed to EOMB, thus pair[1] may be undefined
		}
		return null;
	};
})(jQuery);


	//var l_close = $.totalStorage('nl_2015-11-19.11:44:24');
	var l_close = $.cookie('nl_2015-11-19.11:44:24');
	var ls_date =	{$start_date|escape:'htmlall':'UTF-8'};
	var le_date = 	{$end_date|escape:'htmlall':'UTF-8'};
	var now = $.now();
	if (!l_close && now > ls_date && now < le_date && !!$.prototype.fancybox) {
		$.fancybox( $('#next_landing'),
		  {
			type: 'inline',
			padding: 0,
			closeBtn: false,
			tpl: {
				closeBtn: '<a title="Close" class="fancybox-item fancybox-close nextlanding-close" href="close"></a>'
			}
		  }
		);
	}
	$('#landing_newsletter_form .subscribe').click(function(e) {
		e.preventDefault();
		subscribeNewsletter('/modules/nextlanding/subcribe.php', '#landing_newsletter_form');
	});
	
	$('#landing_newsletter_form .nothanks, .nextlanding-close').click(function(e) {
		e.preventDefault();
		//$.totalStorage('nl_2015-11-19.11:44:24', '1');
		$.cookie('nl_2015-11-19.11:44:24', '1', { expires: 1 });
		$.fancybox.close();
	});
});

function subscribeNewsletter(subcribe_url, id_form) {
	$.ajax({
        type: "POST",
        url: subcribe_url,
		data: $(id_form).serialize(),
        dataType: "json",
        success: function(data) {
        	$('#landing_newsletter_form .result').html('Registrado Corectamente').fadeIn();
        	setTimeout(function() {
			      $('.nextlanding-close').click();
			}, 3000);
        },
        error: function(XMLHttpRequest)
		{
			alert("Response Text:\n" + XMLHttpRequest.responseText);
		}
    });
}
//
//]]>
</script>

<style type="text/css">/* Next Landing */
	.next-landing {
		max-width: 100%;
		color: #3D3D3D;
		overflow: hidden;
	}
	.next-landing .content {
		max-width: 100%;
	}
	.next-landing img {
		height: auto;
	}
	.next-landing .newsletter-form {
		text-align: center;
	}
	.next-landing .newsletter-form h3 {
		font-size: 20px;
		font-weight: 800;
		text-transform: uppercase;
	}
	.next-landing .newsletter-form .inputNew {
		text-align: center;
		font-size: 16px;
		font-weight: 500;
		height: 45px;
		border: 1px solid #ddd;
		border-radius: 0;
		margin-top: 10px;
		width: 80%;
	}
	.next-landing .newsletter-form .surscribe-thanks {
		padding: 20px;
	}
	.next-landing .newsletter-form .surscribe-thanks .button {
		margin: 0;
		padding: 0 20px;
		font-size: 14px;
		border: none;
		background: #ffa728;
		color: #fff;
		font-weight: 500;
		height: 30px;
		line-height: 30px;
		display: inline-block;
		text-transform: uppercase;
	}
	.next-landing .newsletter-form .surscribe-thanks .button:hover {
		text-decoration: underline;
	}
	.next-landing .newsletter-form .surscribe-thanks .nothanks {
		background: #dfdfdf;
		color: inherit;
	}
	.next-landing .nextlanding-close {
		position: absolute;
		right: 0;
		top: 0;
		background: #fff;
		width: 30px;
		height: 30px;
		line-height: 30px;
		color: rgba(255, 255, 255, 0.5);
		font-size: 16px;
		text-align: center;
	}
	.next-landing .nextlanding-close:hover {
		color: #fff;
	}
	.next-landing .result {
		margin-top: 8px;
		font-size: 15px;
		color: #31708F;
	} 
	.next-landing .error {
		color: #A94442;
	}
	.fancybox-lock .fancybox-overlay {
		overflow-y: auto;
	}
	.cart_voucher fieldset #discount_name {
		width: 80%;
	}
	#stores img {
		max-width: none;
	}</style>
<!-- /MODULE Next - Landing Popup -->