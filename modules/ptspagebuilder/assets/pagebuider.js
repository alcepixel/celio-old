/**
 *  Pts Prestashop Theme Framework for Prestashop 1.6.x
 *
 * @package   ptspagebuilder
 * @version   5.0
 * @author    http://www.prestabrain.com
 * @copyright Copyright (C) October 2013 prestabrain.com <@emai:prestabrain@gmail.com>
 *               <info@prestabrain.com>.All rights reserved.
 * @license   GNU General Public License version 2
 * 
 */
function ptssocialsharing_twitter_click(message)
{
	if (typeof message === 'undefined')
		message = encodeURIComponent(location.href);
	window.open('https://twitter.com/intent/tweet?text=' + message, 'sharertwt', 'toolbar=0,status=0,width=640,height=445');
}

function ptssocialsharing_facebook_click(message)
{
	window.open('http://www.facebook.com/sharer.php?u=' + encodeURIComponent(location.href), 'sharer', 'toolbar=0,status=0,width=660,height=445');
}

function ptssocialsharing_google_click(message)
{
	window.open('https://plus.google.com/share?url=' + encodeURIComponent(location.href), 'sharergplus', 'toolbar=0,status=0,width=660,height=445');
}

function ptssocialsharing_pinterest_click(message)
{
	window.open('http://www.pinterest.com/pin/create/button/?url=' + encodeURIComponent(location.href), 'sharerpinterest', 'toolbar=0,status=0,width=660,height=445');
}

$(document).ready(function() {
	$(".pts-popup").fancybox();
	$('.pts-carousel').carousel();

	$('.pts-tab .nav > li:first').addClass( 'active');
	$('.pts-tab .tab-content > .tab-pane:first').addClass( 'active');
	

});
