<?php
/**
 * Pts Prestashop Theme Framework for Prestashop 1.6.x
 *
 * @package   ptspagebuilder
 * @version   5.0
 * @author    http://www.prestabrain.com
 * @copyright Copyright (C) October 2013 prestabrain.com <@emai:prestabrain@gmail.com>
 *               <info@prestabrain.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */

class PtsWidgetCall_to_action extends PtsWidgetPageBuilder {
		
		/**
		 *
		 */
		protected $max_image_size = 1048576;

		public $name = 'call_to_action';
		public $group = 'call_to_action';
		/**
		 *
		 */
		public function beforeAdminProcess($controller)
		{
			if( !Tools::getValue('widgetaction') ){ 
				$controller->addJS( __PS_BASE_URI__.'modules/ptspagebuilder/assets/admin/image_gallery.js' );
			}

		}

		public static function getWidgetInfo()
		{
			return array('label' =>  ('Call To Action'), 'explain' => 'Create a block call to action', 'group' => 'prestabrain'  );
		}
		/**
		 *
		 */
		public function renderForm($args=null, $data)
		{
			$key = time();

			$helper = $this->getFormHelper();

		 	$this->fields_form[1]['form'] = array(
	            'legend' => array(
	                'title' => $this->l('Widget Form.'),
	            ),
	            'input' => array(
                	array(
	                    'type'  => 'text',
	                    'label' => $this->l('Icon File'),
	                    'name'  => 'iconfile',
	                    'class' => 'imageupload',
	                    'default'=> '',
	                    'id'	 => 'iconfile'.$key,
	                    'desc'	=> $this->l('Put image folder in the image folder')._PAGEBUILDER_IMAGE_URL_.'images/'
	                ),
	                array(
	                    'type'  => 'text',
	                    'label' => $this->l('Icon Class'),
	                    'name'  => 'iconclass',
	                    'class' => 'image',
	                    'default'=> '',
	                    'desc'	=> $this->l('Example: fa-umbrella fa-2 radius-x')
	                ),
	                array(
	                    'type' => 'textarea',
	                    'label' => $this->l('Content'),
	                    'name' => 'htmlcontent',
	                    'cols' => 40,
	                    'rows' => 10,
	                    'value' => '',
	                    'lang'  => true,
	                    'default'=> '',
	                    'autoload_rte' => true,
	                ),
	                array(
	                    'type'  => 'text',
	                    'label' => $this->l('Text Link 1'),
	                    'name'  => 'call_to_action_text_link_1',
	                    'default'=> '',
	                    'lang'  => true,
	                ),
	                array(
	                    'type'  => 'text',
	                    'label' => $this->l('Link 1'),
	                    'name'  => 'call_to_action_link_1',
	                    'class' => 'link',
	                    'default'=> '',
	                    'lang'  => true,
	                    'desc'	=> $this->l('Enter url if you want')
	                ),
	                array(
	                    'type'  => 'text',
	                    'label' => $this->l('Text Link 2'),
	                    'name'  => 'call_to_action_text_link_2',
	                    'default'=> '',
	                    'lang'  => true,
	                ),
	                array(
	                    'type'  => 'text',
	                    'label' => $this->l('Link 2'),
	                    'name'  => 'call_to_action_link_2',
	                    'class' => 'link',
	                    'default'=> '',
	                    'lang'  => true,
	                    'desc'	=> $this->l('Enter url if you want')
	                ),
	                array(
	                    'type' => 'select',
	                    'label' => $this->l( 'Styles' ),
	                    'name' => 'call_to_action_style',
	                    'desc'  => 'Select image alignment',
	                    'options' => array(  'query' => array(
	                        array('id' => 'call-to-action-v1', 'name' => $this->l('Call to action v1')),
	                        array('id' => 'call-to-action-v2', 'name' => $this->l('Call to action v2')),
                          	array('id' => 'call-to-action-v3', 'name' => $this->l('Call to action v3')),
                          	array('id' => 'call-to-action-v4', 'name' => $this->l('Call to action v4')),
                          	array('id' => '', 'name' => $this->l('default')),
	                    ),
	                    'id' => 'id',
	                    'name' => 'name' ),
	                    'default' => "",
	                ),
	            ),
	      		 'submit' => array(
	                'title' => $this->l('Save'),
	                'class' => 'button'
           		 )
	        );

		 	$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
			
			$helper->tpl_vars = array(
	                'fields_value' => $this->getConfigFieldsValues( $data ),
	                'languages' => Context::getContext()->controller->getLanguages(),
	                'id_language' => $default_lang
        	);  

			$string = '
					 <script type="text/javascript">
						$(".imageupload").WPO_Gallery({gallery:false} );
					</script>
		 
			';
			return  '<div id="imageslist'.$key.'">'.$helper->generateForm( $this->fields_form ) .$string."</div>" ;
		}
 	
		 
 		/**
		 *
		 */
		public function renderContent($args, $setting)
		{
			$t  = array(
				'name'=> '',
				'iconfile'	=> '',
				'call_to_action_iconurl' => '',
			 	'iconclass' => '',
			 	'call_to_action_text_link_1' => '',
			 	'call_to_action_link_1' => '',
			 	'call_to_action_text_link_2' => '',
			 	'call_to_action_link_2' => '',
			 	'call_to_action_style' => '',
			 	'htmlcontent' => ''
			);

			$setting = array_merge( $t, $setting );

			$languageID = Context::getContext()->language->id;
			$setting['htmlcontent'] = isset($setting['htmlcontent_'.$languageID])?($setting['htmlcontent_'.$languageID]): '';
			$setting['call_to_action_text_link_1'] = isset($setting['call_to_action_text_link_1_'.$languageID])?($setting['call_to_action_text_link_1_'.$languageID]): '';
			$setting['call_to_action_link_1'] = isset($setting['call_to_action_link_1_'.$languageID])?($setting['call_to_action_link_1_'.$languageID]): '';
			$setting['call_to_action_text_link_2'] = isset($setting['call_to_action_text_link_2_'.$languageID])?($setting['call_to_action_text_link_2_'.$languageID]): '';
			$setting['call_to_action_link_2'] = isset($setting['call_to_action_link_2_'.$languageID])?($setting['call_to_action_link_2_'.$languageID]): '';

			if(!empty($setting['iconfile'])){
				$setting['call_to_action_iconurl'] = _PAGEBUILDER_IMAGE_URL_.$setting['iconfile'];
			}

			//d($setting);

			$output = array('type'=>'call_to_action','data' => $setting );
			
	  		return $output;
		}
		

	}
?>