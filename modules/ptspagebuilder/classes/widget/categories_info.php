<?php
/**
 * Pts Prestashop Theme Framework for Prestashop 1.6.x
 *
 * @package   ptspagebuilder
 * @version   5.0
 * @author    http://www.prestabrain.com
 * @copyright Copyright (C) October 2013 prestabrain.com <@emai:prestabrain@gmail.com>
 *               <info@prestabrain.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */

class PtsWidgetCategories_info extends PtsWidgetPageBuilder {

		public $name = 'categories_info';

	

		public  static function getWidgetInfo(){
			return array( 'label' => 'Categories Info', 'explain' => 'Show Categories info to Front Office', 'group' => 'prestashop'  );
		}

		public static function renderButton(){

		}

		public function renderForm( $args, $data ){
			$helper = $this->getFormHelper();

			$this->fields_form[1]['form'] = array(
	            'input' => array (
	                array (
                        'type'  => 'categories_select',
                        'label' => $this->l('Categories:'),
                        'name'  => 'categoryBox',
                        'default' => '1,2,3',
                    )
	            )
	        );
			$values = $this->getConfigFieldsValues( $data  );

			$selected_cat = $values['categoryBox'];
	        $categories = explode(',', $selected_cat);
	        $root = Category::getRootCategory();

	        $tree = new HelperTreeCategories('associated-categories-tree', 'Associated categories');
	        $tree->setRootCategory($root->id)
	            ->setUseCheckBox(true)
	            ->setUseSearch(true)
	            ->setSelectedCategories($categories);
	        $category_tpl = $tree->render();

	        $soption = array(
	            array(
	                'id' => 'active_on',
	                'value' => 1,
	                'label' => $this->l('Enabled')
	            ),
	            array(
	                'id' => 'active_off',
	                'value' => 0,
	                'label' => $this->l('Disabled')
	            )
	        );

			$this->fields_form[1]['form'] = array(
	            'legend' => array(
	                'title' => $this->l('Widget Form.'),
	            ),
	            'input' => array(
	                array(
                        'type'  => 'categories_select',
                        'label' => $this->l('Categories:'),
                        'name'  => 'categoryBox',
                        'category_tree'  =>  $category_tpl,
                        'default' => '1,2,3',
                    ),
                    array(
	                    'type' 	  => 'switch',
	                    'label'   => $this->l( 'Show Image' ),
	                    'name' 	  => 'show_image',
	                    'values'  => $soption,
	                    'default' => "1"
	                ),
	                array(
	                    'type' 	  => 'switch',
	                    'label'   => $this->l( 'Show Category Title' ),
	                    'name' 	  => 'show_title',
	                    'values'  => $soption,
	                    'default' => "1"
	                ),
	                array(
	                    'type' 	  => 'switch',
	                    'label'   => $this->l( 'Show Category Description' ),
	                    'name' 	  => 'show_description',
	                    'values'  => $soption,
	                    'default' => "0"
	                ),
	                array(
	                    'type'    => 'text',
	                    'label'   => $this->l('Category Description Limit'),
	                    'name'    => 'limit_description',
	                    'default' => 25,
	                ),
	                array(
	                    'type' 	  => 'switch',
	                    'label'   => $this->l( 'Show Sub Categories' ),
	                    'name' 	  => 'show_sub_category',
	                    'values'  => $soption,
	                    'default' => "0"
	                ),
	                array(
	                    'type'    => 'text',
	                    'label'   => $this->l('Sub Category Limit'),
	                    'name'    => 'limit_subcategory',
	                    'default' => 5,
	                ),
	                array(
	                    'type' 	  => 'switch',
	                    'label'   => $this->l( 'Show Product Number' ),
	                    'name' 	  => 'show_nb_product',
	                    'values'  => $soption,
	                    'default' => "0"
	                ),
	                array(
	                    'type' 	  => 'switch',
	                    'label'   => $this->l( 'Show Products List' ),
	                    'name' 	  => 'show_products',
	                    'values'  => $soption,
	                    'default' => "0"
	                ),
	                array(
	                    'type'  => 'text',
	                    'label' => $this->l('Limit'),
	                    'name'  => 'limit',
	                    'default'=> 6,
	                ),
	     			array(
	                    'type'  => 'text',
	                    'label' => $this->l('Column'),
	                    'name'  => 'column',
	                    'default'=> 4,
	                    'desc'	=> $this->l('Show In Carousel with N Column in each page')
	                ),
	                array(
	                    'type'  => 'text',
	                    'label' => $this->l('Items Per Page'),
	                    'name'  => 'itemsperpage',
	                    'default'=> 4,
	                    'desc'	=> $this->l('Show In Carousel, Max Products in each page')
	                ),
	            ),
	      		 'submit' => array(
	                'title' => $this->l('Save'),
	                'class' => 'button'
           		 )
	        );

 
		 	$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
			
			$helper->tpl_vars = array(
	                'fields_value' => $this->getConfigFieldsValues( $data  ),
	                'languages' => Context::getContext()->controller->getLanguages(),
	                'id_language' => $default_lang
        	);  
			return  $helper->generateForm( $this->fields_form );

		}

		 

		public function renderContent(  $args, $setting ){
		 
			$t  = array(
				'categoryBox' => '2,3',
				'show_image' => 1,
				'show_title' => 1,
				'show_description' => 0,
				'limit_description' => 25,
				'show_sub_category' => 0,
				'limit_subcategory' => 5,
				'show_nb_product' => 0,
				'show_products' => 0,
				'limit' => 6,
				'column' => 4,
				'itemsperpage' => 4,
			);

			$setting = array_merge( $t, $setting );
			$context = Context::getContext();
			$categories = $this->getCategories($setting['categoryBox'], $context->language->id);
			if($categories){
				foreach ($categories as &$category) {
					$obj = new Category($category['id_category']);
					$category['nb_products'] = $obj->getProducts($context->language->id, 0, 1, null, null, true);
					$category['products'] = $obj->getProducts($context->language->id, 0, $setting['limit'], null, null, false);
					$category['id_image'] = file_exists(_PS_CAT_IMG_DIR_.(int)$category['id_category'].'.jpg') ? (int)$category['id_category'] : false;
					$category['subcategories'] = $this->getSubCategories($category['id_category'], $setting['limit_subcategory'], $context->language->id);
				}
			}
			$dir = _PS_MODULE_DIR_.'/ptspagebuilder/views/templates/front/widgets/sub/products.tpl';
			$tdir = _PS_ALL_THEMES_DIR_ . _THEME_NAME_ . '/modules/ptspagebuilder/views/templates/front/widgets/sub/products.tpl';
			
            if (file_exists($tdir)) {
                $dir = $tdir;
            }

            $setting['product_tpl']  = $dir;

			$setting['categories_info'] = $categories;
			$output = array('type'=>'categories_info','data' => $setting );

	  		return $output;
		}


		public function getCategories($id_categories, $id_lang = false, $active = true, $sql_filter = '', $sql_sort = '', $sql_limit = '')
		{
		 	if (!Validate::isBool($active))
		 		die(Tools::displayError());

			$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
				SELECT *
				FROM `'._DB_PREFIX_.'category` c
				'.Shop::addSqlAssociation('category', 'c').'
				LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON c.`id_category` = cl.`id_category`'.Shop::addSqlRestrictionOnLang('cl').'
				WHERE 1 '.$sql_filter.' '.($id_lang ? 'AND `id_lang` = '.(int)$id_lang : '').($id_categories ? ' AND c.id_category IN ('.$id_categories.')' : '').'
				'.($active ? 'AND `active` = 1' : '').'
				'.(!$id_lang ? 'GROUP BY c.id_category' : '').'
				'.($sql_sort != '' ? $sql_sort : 'ORDER BY c.`level_depth` ASC, category_shop.`position` ASC').'
				'.($sql_limit != '' ? $sql_limit : '')
			);

			return $result;
		}

		public function getSubCategories($id_category, $nb = 5, $id_lang, $active = true)
		{
			$sql_groups_where = '';
			$sql_groups_join = '';
			if (Group::isFeatureActive())
			{
				$sql_groups_join = 'LEFT JOIN `'._DB_PREFIX_.'category_group` cg ON (cg.`id_category` = c.`id_category`)';
				$groups = FrontController::getCurrentCustomerGroups();
				$sql_groups_where = 'AND cg.`id_group` '.(count($groups) ? 'IN ('.implode(',', $groups).')' : '='.(int)Group::getCurrent()->id);
			}

			$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT c.*, cl.id_lang, cl.name, cl.description, cl.link_rewrite, cl.meta_title, cl.meta_keywords, cl.meta_description
			FROM `'._DB_PREFIX_.'category` c
			'.Shop::addSqlAssociation('category', 'c').'
			LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (c.`id_category` = cl.`id_category` AND `id_lang` = '.(int)$id_lang.' '.Shop::addSqlRestrictionOnLang('cl').')
			'.$sql_groups_join.'
			WHERE `id_parent` = '.(int)$id_category.'
			'.($active ? 'AND `active` = 1' : '').'
			'.$sql_groups_where.'
			GROUP BY c.`id_category`
			ORDER BY `level_depth` ASC, category_shop.`position` ASC
			LIMIT 0,'.(int)$nb );

			foreach ($result as &$row)
			{
				$row['id_image'] = Tools::file_exists_cache(_PS_CAT_IMG_DIR_.$row['id_category'].'.jpg') ? (int)$row['id_category'] : Language::getIsoById($id_lang).'-default';
				$row['legend'] = 'no picture';
			}
			return $result;
		}
	}
?>