<?php
/**
 * Pts Prestashop Theme Framework for Prestashop 1.6.x
 *
 * @package   ptspagebuilder
 * @version   5.0
 * @author    http://www.prestabrain.com
 * @copyright Copyright (C) October 2013 prestabrain.com <@emai:prestabrain@gmail.com>
 *               <info@prestabrain.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */

class PtsWidgetCategories_list extends PtsWidgetPageBuilder {

	public $name = 'categories_list';
	public $group = 'product';
		
	public static function getWidgetInfo()
    {
		return array('label' => ('Categories List'), 'explain' => 'Show Category to Front Office', 'group' => 'prestashop'  );
	}

	public function renderForm($args=null, $data)
    {
		$helper = $this->getFormHelper();

        /*Widget Form Config*/
		$this->fields_form[1]['form'] = array(
            'legend' => array(
                'title' => $this->l('Widget Form.'),
            ),
            'input' => array(
				array(
                    'type' => 'text',
                    'label' => $this->l('Categories'),
                    'name' => 'catids',
                    'desc' => '',
                    'default' => '3,4,5,6,7,8',
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Categories per page'),
                    'name' => 'cat_itemspage',
                    'desc' => $this->l('The maximum number of categories in each page (default: 3)'),
                    'default' => '3',
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Colums In Categories tab'),
                    'name' => 'cat_columns',
                    'desc' => $this->l('The maximum number of columns in each page tab (default: 3)'),
                    'default' => '3',
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Categories in tab'),
                    'name' => 'cat_itemstab',
                    'desc' => $this->l('The maximum number of categories in each page tab (default: 6)'),
                    'default' => '6',
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Image width:'),
                    'name' => 'image_width',
                    'desc' => $this->l('Config width for Category image.'),
                    'default' => '380',
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Image height:'),
                    'name' => 'image_height',
                    'desc' => $this->l('Config height for Category image.'),
                    'default' => '200',
                ),
            ),
      		 'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'button'
       		 )
        );

	 	$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
		
		$helper->tpl_vars = array(
                'fields_value' => $this->getConfigFieldsValues( $data  ),
                'languages' => Context::getContext()->controller->getLanguages(),
                'id_language' => $default_lang
    	);

		return  $helper->generateForm( $this->fields_form );
	}

	public function renderContent($args, $setting)
    {
		$t  = array(
			'catids'        => '3,4,5,6,7,8',
			'cat_itemspage' => '3',
			'cat_columns'   => '3',
			'cat_itemstab'  => '6',
			'url_image'     => _THEME_CAT_DIR_,
			'image_width'   => '380',
			'image_height'  => '200',
			'total'			=> '12',
		);
		$setting = array_merge( $t, $setting );

		$catids = explode(",", $setting['catids']);

		/*$porder = $setting['porder'];
        $porder = preg_split("#\s+#", $porder);
        if (!isset($porder[1])) {
            $porder[1] = null;
        }*/

        $categories = array();
        foreach ($catids as $catid) {
        	$category = new Category( $catid, (int)$this->langID );
        	if ($category->id) {
                $categories[$catid]['id']       = $category->id;
                $categories[$catid]['name']     = $category->name;
                $categories[$catid]['link']     = $category->getLink();
                //$categories[$catid]['products'] = $category->getProducts((int)$this->langID, 1, $nb, $porder[0], $porder[1]);
            }
        }

		$setting['categories_list'] = $categories;
		$output = array('type'=>'categories_list','data' => $setting );
 
		return $output;
	}
}
?>