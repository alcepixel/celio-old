<?php
/**
 * Pts Prestashop Theme Framework for Prestashop 1.6.x
 *
 * @package   ptspagebuilder
 * @version   5.0
 * @author    http://www.prestabrain.com
 * @copyright Copyright (C) October 2013 prestabrain.com <@emai:prestabrain@gmail.com>
 *               <info@prestabrain.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */

class PtsWidgetCategories_tabs extends PtsWidgetPageBuilder {

		public $name = 'categories_tabs';
		public $group = 'product';
		
		public static function getWidgetInfo(){
			return array('label' =>  ('Categories Tabs'), 'explain' => 'Dislay Categories Tabs', 'group' => 'prestashop'  );
		}


		public function renderForm( $args, $data ){
			$helper = $this->getFormHelper();
			$types = array();	
		 	
			$this->fields_form[1]['form'] = array(
	            'input' => array(
	                array(
	                    'type'    => 'category_tab',
	                    'label'   => 'Categories',
	                    'name'    => 'categorytab',
	                    'default' => '',
	                ),
	                array(
	                    'type'    => 'categoryBox',
	                    'label'   => 'Categories',
	                    'name'    => 'categoryBox',
	                    'default' => '',
	                ),
        		)
            );

			$values = $this->getConfigFieldsValues( $data  );
	 		$selected_cat = $values['categoryBox'];
	        $categories = explode(',', $selected_cat);
	        $root = Category::getRootCategory();

	        $tree = new HelperTreeCategories('associated-categories-tree', 'Associated categories');
	        $tree->setRootCategory($root->id)
	            ->setUseCheckBox(true)
	            ->setUseSearch(true)
	            ->setSelectedCategories($categories);
	        $category_tpl = $tree->render();

	        $orders = array(
	            0 => array('value' => 'date_add', 'name' => $this->l('Date Add')),
	            1 => array('value' => 'date_add DESC', 'name' => $this->l('Date Add DESC')),
	            2 => array('value' => 'name', 'name' => $this->l('Name')),
	            3 => array('value' => 'name DESC', 'name' => $this->l('Name DESC')),
	            4 => array('value' => 'quantity', 'name' => $this->l('Quantity')),
	            5 => array('value' => 'quantity DESC', 'name' => $this->l('Quantity DESC')),
	            6 => array('value' => 'price', 'name' => $this->l('Price')),
	            7 => array('value' => 'price DESC', 'name' => $this->l('Price DESC'))
	        );
	        
			$this->fields_form[1]['form'] = array(
	            'legend' => array(
	                'title' => $this->l('Widget Form.'),
	            ),
	            'input' => array(
	                array(
	                    'type'    => 'category_tab',
	                    'label'   => $this->l('Categories'),
	                    'name'    => 'categorytab',
	                    'category_tpl' => $category_tpl,
	                    'default' => '',
	                ),
	                array(
	                    'type'    => 'categoryBox',
	                    'label'   => '',
	                    'name'    => 'categoryBox',
	                    'default' => '',
	                ),
 					array(
	                    'type'    => 'text',
	                    'label'   => $this->l('Limit'),
	                    'name'    => 'limit',
	                    'default' => 6,
	                ),
	     			array(
	                    'type'  => 'text',
	                    'label' => $this->l('Column'),
	                    'name'  => 'column',
	                    'default'=> 4,
	                    'desc'	=> $this->l('Show In Carousel with N Column in each page')
	                ),
	                array(
	                    'type'  => 'text',
	                    'label' => $this->l('Items Per Page'),
	                    'name'  => 'itemsperpage',
	                    'default'=> 4,
	                    'desc'	=> $this->l('Show In Carousel, Max Products in each page')
	                ),
	                array(
	                    'type' 	  => 'select',
	                    'label'   => $this->l( 'Order By' ),
	                    'name' 	  => 'order_by',
	                    'options' => array(  'query' => $orders ,
	                    'id' 	  => 'value',
	                    'name' 	  => 'name' ),
	                    'default' => "date_add DESC"
	                )
	            ),
	      		 'submit' => array(
	                'title' => $this->l('Save'),
	                'class' => 'button'
           		 )
	        );

 
		 	$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
			
			$fields_value = $this->getConfigFieldsValues( $data  );
	 		$selected_cat = $values['categoryBox'];
			$fields_value['categoryBox'] = $values['categoryBox'] ? explode(',', $values['categoryBox']) : array();
			$fields_value['categorytab'] = $values['categorytab'] ? Tools::jsonDecode($values['categorytab'], true) : '';

			$helper->tpl_vars = array(
	                'fields_value' => $fields_value,
	                'languages' => Context::getContext()->controller->getLanguages(),
	                'id_language' => $default_lang
        	);  

			return  $helper->generateForm( $this->fields_form );
		}

		/**
		 *
		 */
		public function renderContent( $args, $setting ){ 

			$t = array(
				'categorytab'=> '',
				'categoryBox'=>'',
				'limit' => 6,

				'itemsperpage'	=> 4,
				'column'		=> 4,
				'order_by' => 'date_add DESC'
			);

			$setting = array_merge( $t, $setting );
 			
 			$porder = preg_split("#\s+#", $setting['order_by']);
            if (!isset($porder[1])) {
                $porder[1] = null;
            }

 			$output = array();
 			$context = Context::getContext();
 			$categories = $setting['categoryBox'] ? explode(',', $setting['categoryBox']) : false;
 			$categorytab = $setting['categorytab'] ? Tools::jsonDecode($setting['categorytab'],true) : array();

 			if( $categories ){
 				$tg = array();
 				foreach ($categories as $id_category) {
 					$obj = new Category($id_category, $context->language->id);
 					$tg['category_info'] = isset($categorytab[$id_category]) ? $categorytab[$id_category] : '';

 					if( is_file(_PAGEBUILDER_IMAGE_DIR_.$tg['category_info']['icon'])){
 						$tg['category_info']['icon'] = _PAGEBUILDER_IMAGE_URL_.$tg['category_info']['icon'];
 					}else{
 						$tg['category_info']['icon'] = '';
 					}

 					$tg['category_obj'] = $obj;
 					$tg['products'] = $obj->getProducts($context->language->id, 0, $setting['limit'], $porder[0], $porder[1]);
 					$output[] = $tg;
 				}
 			}
			 

			$dir = _PS_MODULE_DIR_.'/ptspagebuilder/views/templates/front/widgets/sub/products.tpl';
            $tdir = _PS_ALL_THEMES_DIR_ . _THEME_NAME_ . '/modules/ptspagebuilder/views/templates/front/widgets/sub/products.tpl';

            if (file_exists($tdir)) {
                $dir = $tdir;
            }

            
			$setting['product_tpl']  = $dir;
			$setting['categories_tabs']  = $output;
			$link = Context::getContext()->link; 
			$setting['link'] = $link;

			$output = array('type'=>'categories_tabs','data' => $setting );
 
			return $output;
		}

		protected function renderImage( $url, $image, $size ){

			$setting = array();

			$setting['thumbnailurl'] = _PAGEBUILDER_IMAGE_URL_.$image;
			$setting['imageurl'] = _PAGEBUILDER_IMAGE_URL_.$image;
			if( count($size) == 2 ){
				$cache = _PS_CACHE_DIR_.'ptspagebuilder/';
				if( !file_exists($cache.$image) ){
					if( !is_dir($cache) ){
						mkdir( $cache, 0755 );
					}
					if( ImageManager::resize( _PAGEBUILDER_IMAGE_DIR_.$image, $cache.$image, $size[0], $size[1] ) ){
						$setting['thumbnailurl'] = $url.'cache/ptspagebuilder/'.$image;
					}
				}else {
					$setting['thumbnailurl'] = $url.'cache/ptspagebuilder/'.$image;
				}	
			}
			return $setting;
		}	
	}
?>