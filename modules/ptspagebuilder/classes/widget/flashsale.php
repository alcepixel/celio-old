<?php
/**
 * Pts Prestashop Theme Framework for Prestashop 1.6.x
 *
 * @package   ptspagebuilder
 * @version   5.0
 * @author    http://www.prestabrain.com
 * @copyright Copyright (C) October 2013 prestabrain.com <@emai:prestabrain@gmail.com>
 *               <info@prestabrain.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */

class PtsWidgetFlashsale extends PtsWidgetPageBuilder {

		public $name = 'flashsale';
		public $group = 'prestashop';
		
		public static function getWidgetInfo(){
			return array('label' => ('Flash Sale'), 'explain' => 'Play Countdown For Flashsale', 'group' => 'prestashop'  );
		}


		public function renderForm( $args, $data ){

			$helper = $this->getFormHelper();
 

			$this->fields_form[1]['form'] = array(
	            'legend' => array(
	                'title' => $this->l('Widget Form.'),
	            ),
	            'input' => array(
	            	array(
	                    'type'  => 'datetime',
	                    'label' => $this->l('End date'),
	                    'name'  => 'end_date',
	                    'default'=> '',
	                ),
	                array(
	                    'type'  => 'text',
	                    'label' => $this->l('Limit'),
	                    'name'  => 'limit',
	                    'default'=> 6,
	                ),
	                array(
	                    'type'  => 'text',
	                    'label' => $this->l('Column'),
	                    'name'  => 'column',
	                    'default'=> 4,
	                    'desc'	=> $this->l('Show In Carousel with N Column in each page')
	                ),
	                array(
	                    'type'  => 'text',
	                    'label' => $this->l('Items Per Page'),
	                    'name'  => 'itemsperpage',
	                    'default'=> 4,
	                    'desc'	=> $this->l('Show In Carousel, Max Products in each page')
	                ),
	            ),
	      		 'submit' => array(
	                'title' => $this->l('Save'),
	                'class' => 'button'
           		 )
	        );

 
		 	$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
			
			$helper->tpl_vars = array(
	                'fields_value' => $this->getConfigFieldsValues( $data  ),
	                'languages' => Context::getContext()->controller->getLanguages(),
	                'id_language' => $default_lang
        	);  
			Context::getContext()->controller->addJqueryUI('ui.datepicker');
			Context::getContext()->controller->addCSS(array(
				_PS_JS_DIR_.'jquery/plugins/timepicker/jquery-ui-timepicker-addon.css'
			));
			return  $helper->generateForm( $this->fields_form );
		}

		public function renderContent(  $args, $setting ){
			$t  = array(
				'end_date'=> '',
				'limit'   => '12',
				'image_width'=>'200',
				'image_height' =>'200',
			);
			$setting = array_merge( $t, $setting );

			$time = strtotime($setting['end_date']);
			$setting['dates'] 	= array(
                    'month' => date('m',$time),
                    'day' => date('d',$time),
                    'year' => date('Y',$time),
                    'hour' => date('H',$time),
                    'minute' => date('i',$time),
                    'seconds' => date('s',$time)
                );

			$nb = (int)$setting['limit'];

			$special = Product::getPricesDrop( (int)$this->langID , 0, $nb, false, null, null, false, $setting['end_date']);
			
			$dir = _PS_MODULE_DIR_.'/ptspagebuilder/views/templates/front/widgets/sub/products.tpl';
            $tdir = _PS_ALL_THEMES_DIR_ . _THEME_NAME_ . '/modules/ptspagebuilder/views/templates/front/widgets/sub/products.tpl';

            if (file_exists($tdir)) {
                $dir = $tdir;
            }

			$setting['product_tpl'] = $dir;
			
			$setting['products'] = $special;
			$output = array('type' => 'flashsale','data' => $setting);
 
			return $output;
		}
	}
?>