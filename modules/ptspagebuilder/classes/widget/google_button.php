<?php
/**
 * Pts Prestashop Theme Framework for Prestashop 1.6.x
 *
 * @package   ptspagebuilder
 * @version   5.0
 * @author    http://www.prestabrain.com
 * @copyright Copyright (C) October 2013 prestabrain.com <@emai:prestabrain@gmail.com>
 *               <info@prestabrain.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */

class PtsWidgetGoogle_button extends PtsWidgetPageBuilder {
	public $name = 'google_button';

	public  static function getWidgetInfo()
    {
		return array( 'label' => 'Google button', 'explain' => 'Google button' , 'group' => 'social'  );
	}

	public function renderForm($args=null, $data)
    {
		$helper = $this->getFormHelper();

		$button_size = array(
			array('id' => '', 'name' => $this->l('Standard')),
			array('id' => 'small', 'name' => $this->l('Small')),
			array('id' => 'medium', 'name' => $this->l('Medium')),
			array('id' => 'tall', 'name' => $this->l('Tall')),
		);

		$annotation = array(
			array('id' => 'inline', 'name' => $this->l('Inline')),
			array('id' => '', 'name' => $this->l('Bubble')),
			array('id' => 'none', 'name' => $this->l('None')),
		);

		$this->fields_form[1]['form'] = array(
            'legend' => array(
                'title' => $this->l('Widget Google Button Form.'),
            ),

            'input' => array(
				array(
                    'type' => 'select',
                    'label' => $this->l('Button size'),
                    'desc' => $this->l('Select size suitable for your google button'),
                    'name' => 'size',
                    'options' => array(
                        'query' => $button_size,
                        'id' => 'id',
                        'name' => 'name',
                    ),
                    'default' => '',
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Annotation'),
                    'desc' => $this->l('Select annotation suitable for your google button'),
                    'name' => 'annotation',
                    'options' => array(
                        'query' => $annotation,
                        'id' => 'id',
                        'name' => 'name',
                    ),
                    'default' => 'inline',
                ),
                array(
                    'type'  => 'text',
                    'label' => $this->l('Width'),
                    'name'  => 'width',
                    'desc'  => $this->l('Maximum 1600 characters.'),
                    'default'=> '300',
                ),
			),		 
 				 
        
  		 	'submit' => array(
	            'title' => $this->l('Save'),
	            'class' => 'button'
   		 	)
        );


	 	$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
		
		$helper->tpl_vars = array(
                'fields_value' => $this->getConfigFieldsValues( $data  ),
                'languages' => Context::getContext()->controller->getLanguages(),
                'id_language' => $default_lang
    	);  
		return  $helper->generateForm( $this->fields_form );
	}

	public function renderContent($args, $setting)
    {
		$t  = array(
			'size'       => '',
			'annotation' => 'inline',
			'width'      => '300'
		);
		$setting    = array_merge( $t, $setting );

		$output = array('type'=>'google_button','data' => $setting );
		return $output;
	}
}