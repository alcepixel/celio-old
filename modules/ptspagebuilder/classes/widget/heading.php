<?php
/**
 * Pts Prestashop Theme Framework for Prestashop 1.6.x
 *
 * @package   ptspagebuilder
 * @version   5.0
 * @author    http://www.prestabrain.com
 * @copyright Copyright (C) October 2013 prestabrain.com <@emai:prestabrain@gmail.com>
 *               <info@prestabrain.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */

class PtsWidgetHeading extends PtsWidgetPageBuilder {

		public $name = 'heading';

		
		public static function getWidgetInfo(){
			return array('label' =>  ('Heading'), 'explain' => 'Heading block', 'group' => 'others'  );
		}

		public function beforeAdminProcess($controller)
		{
			if( !Tools::getValue('widgetaction') ){ 
				$controller->addJS( __PS_BASE_URI__.'modules/ptspagebuilder/assets/admin/image_gallery.js' );
			}

		}

		public function renderForm( $args, $data ){
			$helper = $this->getFormHelper();

			$this->fields_form[1]['form'] = array(
	            'legend' => array(
	                'title' => $this->l('Widget Form.'),
	            ),
	            'input' => array(
 					array(
	                    'type'  => 'text',
	                    'label' => $this->l('Icon File'),
	                    'name'  => 'iconfile',
	                    'class' => 'imageupload',
	                    'default'=> '',
	                    'id'	 => 'iconfile'.$key,
	                    'desc'	=> $this->l('Put image folder in the image folder')._PAGEBUILDER_IMAGE_URL_.'images/'
	                ),
	                array(
	                    'type'  => 'text',
	                    'label' => $this->l('Icon Class'),
	                    'name'  => 'iconclass',
	                    'class' => 'image',
	                    'default'=> '',
	                    'desc'	=> $this->l('Example: fa-umbrella fa-2 radius-x')
	                ),
	                array(
	                    'type' => 'textarea',
	                    'label' => $this->l('Content'),
	                    'name' => 'content_html',
	                    'cols' => 40,
	                    'rows' => 10,
	                    'value' => true,
	    
	                    'default'=> '',
	                    'autoload_rte' => false,
	                    'desc'	=> 'Enter HTML CODE in here'
	                ),
	                array(
	                    'type' => 'select',
	                    'label' => $this->l( 'Styles' ),
	                    'name' => 'headingstyle',
	                    'desc'  => 'Select image alignment',
	                    'options' => array(  'query' => array(
	                        array('id' => 'heading-v1', 'name' => $this->l('Heading v1')),
	                        array('id' => 'heading-v2', 'name' => $this->l('Heading v2')),
	                        array('id' => 'heading-v3', 'name' => $this->l('Heading v3')),
	                        array('id' => 'heading-v4', 'name' => $this->l('Heading v4')),
	                        array('id' => 'heading-v5', 'name' => $this->l('Heading v5')),
	                        array('id' => 'heading-v6', 'name' => $this->l('Heading v6')),
	                        array('id' => 'heading-v7', 'name' => $this->l('Heading v7')),
	                        array('id' => 'heading-v8', 'name' => $this->l('Heading v8')),
	                        array('id' => 'heading-v9', 'name' => $this->l('Heading v9')),
	                        array('id' => 'heading-icon', 'name' => $this->l('Heading icon')),
	                    ),
	                    'id' => 'id',
	                    'name' => 'name' ),
	                    'default' => "heading-v1",
	                ),
	            ),
	      		 'submit' => array(
	                'title' => $this->l('Save'),
	                'class' => 'button'
           		 )
	        );

 
		 	$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
			
			$helper->tpl_vars = array(
	                'fields_value' => $this->getConfigFieldsValues( $data  ),
	                'languages' => Context::getContext()->controller->getLanguages(),
	                'id_language' => $default_lang
        	);  


			$string = '
					 <script type="text/javascript">
						$(".imageupload").WPO_Gallery({gallery:false} );
					</script>
			';
			return  '<div id="imageslist'.$key.'">'.$helper->generateForm( $this->fields_form ) .$string."</div>";
		}

		public function renderContent(  $args, $setting ){
			
			$t  = array(
				'name'=> '',
				'iconfile' => '',
				'iconclass' => '',
				'content_html'   => '',
				'headingstyle' => ''
			);

			$setting = array_merge( $t, $setting );
			$html =  $setting['content_html'];
			
	 		$html = html_entity_decode( $html, ENT_QUOTES, 'UTF-8' );
			

			$output = array('type'=>'heading','data' => $setting );
	  		return $output;
		}
	}
?>