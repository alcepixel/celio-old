<?php
/**
 * Pts Prestashop Theme Framework for Prestashop 1.6.x
 *
 * @package   ptspagebuilder
 * @version   5.0
 * @author    http://www.prestabrain.com
 * @copyright Copyright (C) October 2013 prestabrain.com <@emai:prestabrain@gmail.com>
 *               <info@prestabrain.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */

class PtsWidgetManufacture extends PtsWidgetPageBuilder {

		public $name = 'Manufacture';

		
		public static function getWidgetInfo(){
			return  array('label' => ('Manufacture Logos'), 'explain' => 'Manufacture Logo', 'group' => 'prestashop'  ) ;
		}


		public function renderForm( $args, $data ){

			$helper = $this->getFormHelper();
 

			$this->fields_form[1]['form'] = array(
	            'legend' => array(
	                'title' => $this->l('Widget Form.'),
	            ),
	            'input' => array(
	                array(
	                    'type'  => 'text',
	                    'label' => $this->l('Limit'),
	                    'name'  => 'limit',
	                    'default'=> 12,
	                ),
	                array(
	                    'type'  => 'text',
	                    'label' => $this->l('Column'),
	                    'name'  => 'column',
	                    'default'=> 6,
	                ),
	                 array(
	                    'type'  => 'text',
	                    'label' => $this->l('Items Per Page'),
	                    'name'  => 'itemsperpage',
	                    'default'=> 6,
	                )
	            ),
	      		 'submit' => array(
	                'title' => $this->l('Save'),
	                'class' => 'button'
           		 )
	        );

 
		 	$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
			
			$helper->tpl_vars = array(
	                'fields_value' => $this->getConfigFieldsValues( $data  ),
	                'languages' => Context::getContext()->controller->getLanguages(),
	                'id_language' => $default_lang
        	);  


			return  $helper->generateForm( $this->fields_form );

		}

		public function renderContent( $args, $setting ){
			$t  = array(
				'name'			=> '',
				'html'  	 	=> '',
				'column'		=> 6,
				'itemsperpage'	=> 6,
				'widgetid'		=> 'manu-'.time()

			);
			$setting = array_merge( $t, $setting );
			
		 

			$manufacturers = Manufacturer::getManufacturers();
			foreach ($manufacturers as &$manufacturer)
			{
				$manufacturer['image'] =Context::getContext()->language->iso_code.'-default';
				if (file_exists(_PS_MANU_IMG_DIR_.$manufacturer['id_manufacturer'].'-'.ImageType::getFormatedName('medium').'.jpg'))
					$manufacturer['image'] = $manufacturer['id_manufacturer'];
			}

 	 
			$setting['manufacturers'] = $manufacturers; 
			$setting['scolumn']		= floor(12/$setting['column']);
			$output = array('type'=>'manufacture','data' => $setting );


			return $output;
		} 

	}
?>