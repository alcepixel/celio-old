<?php
/**
 * Pts Prestashop Theme Framework for Prestashop 1.6.x
 *
 * @package   ptspagebuilder
 * @version   5.0
 * @author    http://www.prestabrain.com
 * @copyright Copyright (C) October 2013 prestabrain.com <@emai:prestabrain@gmail.com>
 *               <info@prestabrain.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */

class PtsWidgetNewsletter extends PtsWidgetPageBuilder {

		public $name = 'map';

		
		public static function getWidgetInfo(){
			return array('label' => ('Newsletter Form'), 'explain' => 'Create Newsletter Form Working With Newsletter Block Of Prestashop.', 'group' => 'prestashop'  );
		}


		public function renderForm( $args, $data ){
			$helper = $this->getFormHelper();
			
			$key = time();
			$types = array();
			$types[] = array('value' => 'newsletter-v1', 'text'  => $this->l('Newsletter v1'));
			$types[] = array('value' => 'newsletter-v2', 'text'  => $this->l('Newsletter v2'));
			$types[] = array('value' => 'newsletter-v3', 'text'  => $this->l('Newsletter v3'));
			$types[] = array('value' => 'newsletter-v4', 'text'  => $this->l('Newsletter v4'));
			$types[] = array('value' => 'newsletter-v5', 'text'  => $this->l('Newsletter v5'));
			$types[] = array('value' => 'newsletter-v6', 'text'  => $this->l('Newsletter v6'));
			$types[] = array('value' => 'newsletter-v7', 'text'  => $this->l('Newsletter v7'));
			$types[] = array('value' => 'newsletter-v8', 'text'  => $this->l('Newsletter v8'));
			
			$this->fields_form[1]['form'] = array(
	            'legend' => array(
	                'title' => $this->l('Widget Form.'),
	            ),
	            'input' => array(
 					array(
	                    'type'  => 'text',
	                    'label' => $this->l('Css Class'),
	                    'name'  => 'class',
	                    'default'=> "pts-newsletter",
	                ),
	                array(
	                    'type'  => 'text',
	                    'label' => $this->l('Image File'),
	                    'name'  => 'imagefile',
	                    'class' => 'imageupload',
	                    'default'=> '',
	                    'id'	 => 'imagefile'.$key,
	                    'desc'	=> 'Put image folder in the image folder ROOT_SHOP_DIR/img/'
	                ),
	                array(
	                    'type' => 'textarea',
	                    'label' => $this->l('Information'),
	                    'name' => 'information',
	                    'cols' => 20,
	                    'rows' => 10,
	                    'value' => true,
	                    'lang'  => true,
	                    'default'=> 'Sign up to our newsletter and get exclusive deals you wont find anywhere else straight to your inbox!',
	                    'autoload_rte' => true,
	                ),
					array(
	                    'type' 	  => 'select',
	                    'label'   => $this->l( 'Style' ),
	                    'name' 	  => 'newsletter_style',
	                    'options' => array(  'query' => $types ,
	                    'id' 	  => 'value',
	                    'name' 	  => 'text' ),
	                    'default' => "style1"
	                ),
	            ),
	      		 'submit' => array(
	                'title' => $this->l('Save'),
	                'class' => 'button'
           		 )
	        );

 
		 	$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
			
			$helper->tpl_vars = array(
	                'fields_value' => $this->getConfigFieldsValues( $data  ),
	                'languages' => Context::getContext()->controller->getLanguages(),
	                'id_language' => $default_lang
        	);  

			$string = '
				<script type="text/javascript">
						$(".imageupload").WPO_Gallery({gallery:false} );
				</script>
			';
			return  $helper->generateForm( $this->fields_form ).$string;
		}

		public function renderContent( $args, $setting ){
			$t = array(
				'newsletter_style' => "newsletter-v1",
				'class' => "pts-newsletter",
				'imagefile'=> ''
			);
			$setting = array_merge( $t, $setting );

			
			$languageID = $this->langID;	
			$setting['information']= isset($setting['information_'.$languageID])?html_entity_decode($setting['information_'.$languageID],ENT_QUOTES,'UTF-8'): '';

			 
			$setting['background'] = _PAGEBUILDER_IMAGE_URL_.''.$setting['imagefile']	;
 			
			$output = array('type'=>'newsletter','data' => $setting );

			return $output;
		}

	}
?>