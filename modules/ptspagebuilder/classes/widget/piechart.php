<?php
/**
 * Pts Prestashop Theme Framework for Prestashop 1.6.x
 *
 * @package   ptspagebuilder
 * @version   5.0
 * @author    http://www.prestabrain.com
 * @copyright Copyright (C) October 2013 prestabrain.com <@emai:prestabrain@gmail.com>
 *               <info@prestabrain.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */

class PtsWidgetPiechart extends PtsWidgetPageBuilder {

		public $name = 'piechart';

		public $group = 'piechart';

		public static function getWidgetInfo()
		{
			return array('label' =>  ('Pie Chart'), 'explain' => 'Create a pie chart', 'group' => 'prestabrain' );
		}


		public function renderForm( $args, $data )
		{
			$key = time();
			$helper = $this->getFormHelper();
			$types = array();
			$types[] = array('value' => 'bar-chart', 'text'  => $this->l('Bar chart'));
			$types[] = array('value' => 'bar-chart-icon', 'text'  => $this->l('Bar chart icon'));
			$types[] = array('value' => 'circle', 'text'  => $this->l('Circle chart'));
			$types[] = array('value' => 'circle-icon', 'text'  => $this->l('Circle Icon chart'));


			$this->fields_form[1]['form'] = array(
	            'legend' => array(
	                'title' => $this->l('Widget Form.'),
	            ),
	            'input' => array(
	                array(
	                    'type'  => 'text',
	                    'label' => $this->l('Icon File'),
	                    'name'  => 'iconfile',
	                    'class' => 'imageupload',
	                    'default'=> '',
	                    'id'	 => 'iconfile'.$key,
	                    'desc'	=> $this->l('Put image folder in the image folder')._PAGEBUILDER_IMAGE_URL_.'images/'
	                ),
	                array(
	                    'type'  => 'text',
	                    'label' => $this->l('Icon Class'),
	                    'name'  => 'iconclass',
	                    'class' => 'image',
	                    'default'=> '',
	                    'desc'	=> $this->l('Example: fa-umbrella fa-2 radius-x')
	                ),
	                array(
	                    'type'  => 'text',
	                    'label' => $this->l('Number'),
	                    'name'  => 'number_percentage',
	                    'default'=> '',
	                    'desc'	=> $this->l('Show %'),
	                    'surfix' => '%'
	                ),
	                array(
	                    'type' => 'textarea',
	                    'label' => $this->l('Content'),
	                    'name' => 'htmlcontent',
	                    'cols' => 40,
	                    'rows' => 20,
	                    'value' => true,
	                    'lang'  => true,
	                    'default'=> ''
	                ),
	                array(
	                    'type' 	  => 'select',
	                    'label'   => $this->l( 'Style' ),
	                    'name' 	  => 'piechart_style',
	                    'options' => array(  'query' => $types ,
	                    'id' 	  => 'value',
	                    'name' 	  => 'text' ),
	                    'default' => "1",
	                    'desc'    => $this->l( 'Select a style type' )
	                ),

	            ),
	      		 'submit' => array(
	                'title' => $this->l('Save'),
	                'class' => 'button'
           		 )
	        );

 
		 	$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
			$a = $this->getConfigFieldsValues( $data  );
 
			$helper->tpl_vars = array(
                'fields_value' => $a,
                'languages' => Context::getContext()->controller->getLanguages(),
                'id_language' => $default_lang
        	);  

			$string = '
				<script type="text/javascript">
					$(".imageupload").WPO_Gallery({gallery:false} );
				</script>
			';
			return  $helper->generateForm( $this->fields_form ).$string;
		}

		public function renderContent($args, $setting)
		{
			$t  = array(
				'name'=> '',
				'iconurl' => '',
				'iconfile' => '',
				'iconclass' => '',
				'number_percentage' => '0',
				'htmlcontent'   => '',
				'piechart_style'	=> 'bar-chart'
			);
			$setting = array_merge( $t, $setting );
			$languageID = Context::getContext()->language->id;
			
	 		$languageID = Context::getContext()->language->id;
			$setting['htmlcontent'] = isset($setting['htmlcontent_'.$languageID])?($setting['htmlcontent_'.$languageID]): '';
 			if(!empty($setting['iconfile'])){
				$setting['iconurl'] = _PAGEBUILDER_IMAGE_URL_.$setting['iconfile'];
			}

			$output = array('type'=>'piechart','data' => $setting );

	  		return $output;
		}
	}
?>