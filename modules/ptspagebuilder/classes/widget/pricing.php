<?php
/**
 * Pts Prestashop Theme Framework for Prestashop 1.6.x
 *
 * @package   ptspagebuilder
 * @version   5.0
 * @author    http://www.prestabrain.com
 * @copyright Copyright (C) October 2013 prestabrain.com <@emai:prestabrain@gmail.com>
 *               <info@prestabrain.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */

class PtsWidgetPricing extends PtsWidgetPageBuilder {

		public $name = 'Manufacture';

		
		public static function getWidgetInfo()
		{
			return  array('label' => ('Pricing Plan'), 'explain' => 'Plan Membership', 'group' => 'prestashop'  ) ;
		}


		public function renderForm($args=null, $data)
		{
			$helper = $this->getFormHelper();
 
				$soption = array(
	            array(
	                'id' => 'active_on',
	                'value' => 1,
	                'label' => $this->l('Enabled')
	            ),
	            array(
	                'id' => 'active_off',
	                'value' => 0,
	                'label' => $this->l('Disabled')
	            )
	        );
			$this->fields_form[1]['form'] = array(
	            'legend' => array(
	                'title' => $this->l('Widget Form.'),
	            ),
	            'input' => array(
	            	 array(
	                    'type'  => 'text',
	                    'label' => $this->l('Subtitle'),
	                    'name'  => 'subtitle',
	                    'default'=> '',
	                    'lang'	=> 'true'
	                ),
	                array(
	                    'type'  => 'text',
	                    'label' => $this->l('Price'),
	                    'name'  => 'price',
	                    'default'=> 10,
	                ),
	                array(
	                    'type'  => 'text',
	                    'label' => $this->l('Currency'),
	                    'name'  => 'currency',
	                    'default'=> 'USD',
	                ),
	                array(
	                    'type'  => 'text',
	                    'label' => $this->l('Period'),
	                    'name'  => 'period',
	                    'default'=> '1 Month',
	                ),
	                array(
	                    'type'  => 'text',
	                    'label' => $this->l('Link Title'),
	                    'name'  => 'linktitle',
	                    'default'=> 'Buy Now',
	                    'lang'  => true,
	                ),

	                array(
	                    'type'  => 'text',
	                    'label' => $this->l('Link'),
	                    'name'  => 'link',
	                    'default'=> '#',
	                ),

	                array(
	                    'type' 	  => 'switch',
	                    'label' => $this->l('Is Featured'),
	                    'name'  => 'isfeatured',
	                    'values'  => $soption,  
	                    'default'=> '0',
	                ),
	                array(
	                   'type'  => 'textarea',
	                    'label' => $this->l('Content'),
	                    'name'  => 'content',
	                    'default'=> '',
	                    'cols' => 40,
	                    'rows' => 10,
	                    'value' => true,
	                    'lang'  => true,
	              
	                    'autoload_rte' => true,

	                    'desc'	=> $this->l('Enter Content 1')
	                ),

	            ),
	      		 'submit' => array(
	                'title' => $this->l('Save'),
	                'class' => 'button'
           		 )
	        );

 
		 	$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
			
			$helper->tpl_vars = array(
	                'fields_value' => $this->getConfigFieldsValues( $data  ),
	                'languages' => Context::getContext()->controller->getLanguages(),
	                'id_language' => $default_lang
        	);  

			return  $helper->generateForm( $this->fields_form );
		}	

		public function renderContent($args, $setting)
		{
			$t  = array(
				'name'=> '',
				'html'   => '',
				'widget_heading'=> '',
				'currency'=> '',
				'linktitle' => '#',
				'class' => '',
				'link'	=> ''
 			);
			$setting = array_merge( $t, $setting );
			

			$setting['subtitle'] = $this->getValueByLang( $setting, 'subtitle' );
			$setting['content'] = $this->getValueByLang( $setting, 'content' );
 
			$setting['linktitle'] = $this->getValueByLang( $setting, 'linktitle' );
 

			$output = array('type'=>'pricing','data' => $setting );

		///	d( $output );
			return $output;
		} 

	}
?>