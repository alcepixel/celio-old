<?php
/**
 * Pts Prestashop Theme Framework for Prestashop 1.6.x
 *
 * @package   ptspagebuilder
 * @version   5.0
 * @author    http://www.prestabrain.com
 * @copyright Copyright (C) October 2013 prestabrain.com <@emai:prestabrain@gmail.com>
 *               <info@prestabrain.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */

class PtsWidgetProduct_category extends PtsWidgetPageBuilder {

		public $name = 'product_category';
		public $group = 'product';
		
		public static function getWidgetInfo(){
			return array('label' => ('Products By Category ID'), 'explain' => 'Created Product List From Category ID', 'group' => 'prestashop'  );
		}


		public function renderForm( $args, $data ){

			$helper = $this->getFormHelper();
 

			$this->fields_form[1]['form'] = array(
	            'legend' => array(
	                'title' => $this->l('Widget Form.'),
	            ),
	            'input' => array(
	                array(
	                    'type'  => 'text',
	                    'label' => $this->l('Category ID'),
	                    'name'  => 'category_id',
	                    'default'=> 5,
	                ),
	                array(
	                    'type'  => 'text',
	                    'label' => $this->l('Limit'),
	                    'name'  => 'limit',
	                    'default'=> 8,
	                ),
	                array(
	                    'type'  => 'text',
	                    'label' => $this->l('Column'),
	                    'name'  => 'column',
	                    'default'=> 4,
	                    'desc'	=> $this->l('Show In Carousel with N Column in each page')
	                ),
	                array(
	                    'type'  => 'text',
	                    'label' => $this->l('Items Per Page'),
	                    'name'  => 'itemsperpage',
	                    'default'=> 4,
	                    'desc'	=> $this->l('Show In Carousel, Max Products in each page')
	                ),
	            ),
	      		 'submit' => array(
	                'title' => $this->l('Save'),
	                'class' => 'button'
           		 )
	        );

 
		 	$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
			
			$helper->tpl_vars = array(
	                'fields_value' => $this->getConfigFieldsValues( $data  ),
	                'languages' => Context::getContext()->controller->getLanguages(),
	                'id_language' => $default_lang
        	);  


			return  $helper->generateForm( $this->fields_form );
		}

		public function renderContent(  $args, $setting ){
			$t  = array(
				'category_id'=> '',
				'limit'   => '12',
				'image_width'=>'200',
				'image_height' =>'200',
				'itemsperpage'	=> 4,
				'column'		=> 4,
			);
			$setting = array_merge( $t, $setting );
			$nb = (int)$setting['limit'];


			$category = new Category( $setting['category_id'], $this->langID );
			$products = $category->getProducts((int)$this->langID, 1, ($nb ? $nb : 8));

			$dir = _PS_MODULE_DIR_.'/ptspagebuilder/views/templates/front/widgets/sub/products.tpl';
            $tdir = _PS_ALL_THEMES_DIR_ . _THEME_NAME_ . '/modules/ptspagebuilder/views/templates/front/widgets/sub/products.tpl';

            if (file_exists($tdir)) {
                $dir = $tdir;
            }

            
 			$link = Context::getContext()->link;  
			
			$setting['link'] = $link;

			$setting['product_tpl']  = $dir;

			$setting['products'] = $products;
			$output = array('type'=>'products','data' => $setting );

		//	d( $output );
			return $output;

		 
		}	
	}
?>