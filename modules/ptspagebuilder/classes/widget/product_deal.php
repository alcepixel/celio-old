<?php
/**
 * Pts Prestashop Theme Framework for Prestashop 1.6.x
 *
 * @package   ptspagebuilder
 * @version   5.0
 * @author    http://www.prestabrain.com
 * @copyright Copyright (C) October 2013 prestabrain.com <@emai:prestabrain@gmail.com>
 *               <info@prestabrain.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */

class PtsWidgetProduct_deal extends PtsWidgetPageBuilder {

		public $name = 'product_deal';
		public $group = 'prestashop';
		
		public static function getWidgetInfo(){
			return array('label' => ('Product Deal'), 'explain' => 'Play Countdown in ProductSales', 'group' => 'prestashop'  );
		}


		public function renderForm( $args, $data ){

			$helper = $this->getFormHelper();
 

			$this->fields_form[1]['form'] = array(
	            'legend' => array(
	                'title' => $this->l('Widget Form.'),
	            ),
	            'input' => array(
	                array(
	                    'type'  => 'text',
	                    'label' => $this->l('Limit'),
	                    'name'  => 'limit',
	                    'default'=> 6,
	                ),
	                array(
	                    'type'  => 'text',
	                    'label' => $this->l('Column'),
	                    'name'  => 'column',
	                    'default'=> 4,
	                    'desc'	=> $this->l('Show In Carousel with N Column in each page')
	                ),
	                array(
	                    'type'  => 'text',
	                    'label' => $this->l('Items Per Page'),
	                    'name'  => 'itemsperpage',
	                    'default'=> 4,
	                    'desc'	=> $this->l('Show In Carousel, Max Products in each page')
	                ),
	            ),
	      		 'submit' => array(
	                'title' => $this->l('Save'),
	                'class' => 'button'
           		 )
	        );

 
		 	$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
			
			$helper->tpl_vars = array(
	                'fields_value' => $this->getConfigFieldsValues( $data  ),
	                'languages' => Context::getContext()->controller->getLanguages(),
	                'id_language' => $default_lang
        	);  


			return  $helper->generateForm( $this->fields_form );
		}

		public function renderContent(  $args, $setting ){
			$t  = array(
				'category_id'=> '',
				'limit'   => '12',
				'image_width'=>'200',
				'image_height' =>'200',
			);
			$setting = array_merge( $t, $setting );
			$nb = (int)$setting['limit'];


			$porder =  'date add' ;
			$porder = preg_split("#\s+#", $porder);  

			$special = Product::getPricesDrop( (int)$this->langID , 0, $nb,false   );
			
			if($special)
	            foreach ($special as &$row){
	                $time = '';
	                if($row['specific_prices']['to'] == '0000-00-00 00:00:00'){
	                    $row['js'] = 'unlimited';
	                    $row['finish'] = $this->l('Unlimited');
	                    $row['check_status'] = 1;
	                    $row['lofdate'] = $this->l("Unlimited");
	                }else{
	                    $time = strtotime($row['specific_prices']['to']);
	                    $row['finish'] = $this->l('Expired');;
	                    $row['check_status'] = 1;
	                    $row['lofdate'] = $row['specific_prices']['to'];
	                }
	                if($time){
	                    $row['js'] 	= array(
	                        'month' => date('m',$time),
	                        'day' => date('d',$time),
	                        'year' => date('Y',$time),
	                        'hour' => date('H',$time),
	                        'minute' => date('i',$time),
	                        'seconds' => date('s',$time)
	                    );
	                }
	            }


			$dir = _PS_MODULE_DIR_.'/ptspagebuilder/views/templates/front/widgets/sub/products_countdown.tpl';
            $tdir = _PS_ALL_THEMES_DIR_ . _THEME_NAME_ . '/modules/ptspagebuilder/views/templates/front/widgets/sub/products_countdown.tpl';

            if (file_exists($tdir)) {
                $dir = $tdir;
            }


			$setting['product_tpl']  = $dir;
			
			$setting['products'] = $special;
			$output = array('type'=>'product_deal','data' => $setting );
 
			return $output;

		 
		}	
	}
?>