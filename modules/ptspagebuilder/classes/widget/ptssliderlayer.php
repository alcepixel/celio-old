<?php
/**
 * Pts Prestashop Theme Framework for Prestashop 1.6.x
 *
 * @package   ptspagebuilder
 * @version   5.0
 * @author    http://www.prestabrain.com
 * @copyright Copyright (C) October 2013 prestabrain.com <@emai:prestabrain@gmail.com>
 *               <info@prestabrain.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */

class PtsWidgetPtsSliderlayer extends PtsWidgetPageBuilder {

		public $name = 'ptssliderlayer';

	

		public  static function getWidgetInfo(){
			return array( 'label' => 'Pts Slider Layer', 'explain' => 'Integrate with Pts Slider layer Module to get slider', 'group' => 'others'  );
		}

		public static function renderButton(){

		}

		public function renderForm( $args, $data ){
			$helper = $this->getFormHelper();

			$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
			
			$helper->tpl_vars = array(
	                'fields_value' => $this->getConfigFieldsValues( $data  ),
	                'languages' => Context::getContext()->controller->getLanguages(),
	                'id_language' => $default_lang
        	); 


			$module = Module::getInstanceByName('ptssliderlayer');
			if(!$module || (isset($module->id) && (!$module->id || !$module->active))){

				$this->fields_form[1]['form'] = array(
		            'legend' => array(
		                'title' => $this->l('Widget Form.'),
		                'desc' => $this->l('You need install or active the module ptssliderlayer before')
		            ),
	            );
				return  $helper->generateForm( $this->fields_form );
			}
			$obj = new PtsSliderGroup();
			$id_shop = $this->context->shop->id;
        	$groups = $obj->getGroups(null, $id_shop);
        	
			$this->fields_form[1]['form'] = array(
	            'legend' => array(
	                'title' => $this->l('Widget Form.'),
	            ),
	            'input' => array(
	                
	                array(
	                    'type' 	  => 'select',
	                    'label'   => $this->l( 'Groups' ),
	                    'name' 	  => 'id_group',
	                    'options' => array(  'query' => $groups ,
		                    'id' 	  => 'id_ptssliderlayer_groups',
		                    'name' 	  => 'title' ),
	                    'default' => "1",
	                    'desc'    => $this->l( 'Select a Group to display' )
	                ),
	 				 
	            ),
	      		 'submit' => array(
	                'title' => $this->l('Save'),
	                'class' => 'button'
           		 )
	        );

 			$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
			
			$helper->tpl_vars = array(
	                'fields_value' => $this->getConfigFieldsValues( $data  ),
	                'languages' => Context::getContext()->controller->getLanguages(),
	                'id_language' => $default_lang
        	);
		 	 
			return  $helper->generateForm( $this->fields_form );

		}

		 

		public function renderContent(  $args, $setting ){
		 
			$t  = array(
				'id_group'=> '1',
				'sliderlayer_html'=> '',
			);

			$setting = array_merge( $t, $setting );

			$module = Module::getInstanceByName('ptssliderlayer');
			if(!$module || (isset($module->id) && (!$module->id || !$module->active))){
				$output = array('type'=>'sliderlayer','data' => $setting );
				return $output;
			}
			$obj = new PtsSliderGroup($setting['id_group']);
			$group = array(
				'id_ptssliderlayer_groups' => $obj->id,
				'title' => $obj->title,
				'active' => $obj->active,
				'hook' => $obj->hook,
				'id_shop' => $obj->id_shop,
				'params' => $obj->params,
			);
			
			$html = $module->processHook('sliderlayer', $group);
			
			$setting['sliderlayer_html'] = $html;
			$output = array('type'=>'ptssliderlayer','data' => $setting );
			//echo "<pre>".print_r($setting,1);die;
			return $output;
		}
		 
	}
?>