<?php
/**
 * Pts Prestashop Theme Framework for Prestashop 1.6.x
 *
 * @package   ptspagebuilder
 * @version   5.0
 * @author    http://www.prestabrain.com
 * @copyright Copyright (C) October 2013 prestabrain.com <@emai:prestabrain@gmail.com>
 *               <info@prestabrain.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */

class PtsWidgetSeparator_text extends PtsWidgetPageBuilder {
	public $name = 'separator_text';

	public  static function getWidgetInfo(){
		return array( 'label' => 'Separator text', 'explain' => 'Separator text', 'group' => 'others'  );
	}

	public function renderForm( $args, $data ){
			
		$helper = $this->getFormHelper();

		$align = array(
			array('id' => 'separator_align_center', 'name' => $this->l('Align center')),
			array('id' => 'separator_align_left', 'name' => $this->l('Align left')),
			array('id' => 'separator_align_right', 'name' => $this->l('Align right')),
		);

		$this->fields_form[1]['form'] = array(
            'legend' => array(
                'title' => $this->l('Widget Separator Form.'),
            ),

            'input' => array(
            	array(
					'type' => 'text',
					'label' => $this->l('Title'),
					'name' => 'separ_title',
					//'lang' => true,
					'desc' => $this->l('will be showed on separator.'),
					'default' => '',
				),
				array(
                    'type' => 'select',
                    'label' => $this->l('Title position'),
                    'desc' => $this->l('Select title location.'),
                    'name' => 'title_align',
                    'options' => array(
                        'query' => $align,
                        'id' => 'id',
                        'name' => 'name',
                    ),
                    'default' => 'separator_align_center',
                ),
                array(
					'type' => 'text',
					'label' => $this->l('Extra class name'),
					'name' => 'el_class',
					'desc' => $this->l('If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.'),
					'default' => '',
				),
			),		 
 				 
        
  		 	'submit' => array(
	            'title' => $this->l('Save'),
	            'class' => 'button'
   		 	)
        );


	 	$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
		
		$helper->tpl_vars = array(
                'fields_value' => $this->getConfigFieldsValues( $data  ),
                'languages' => Context::getContext()->controller->getLanguages(),
                'id_language' => $default_lang
    	);  
		return  $helper->generateForm( $this->fields_form );
	}

	public function renderContent(  $args, $setting ){
		$t  = array(
			'separ_title' => 'separator here',
			'title_align' => 'separator_align_center',
			'el_class'    => '' ,
		);

		$setting    = array_merge( $t, $setting );

		$output = array('type'=>'separator_text','data' => $setting );
		return $output;
	}
}