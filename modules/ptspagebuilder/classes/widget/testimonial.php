<?php
/**
 * Pts Prestashop Theme Framework for Prestashop 1.6.x
 *
 * @package   ptspagebuilder
 * @version   5.0
 * @author    http://www.prestabrain.com
 * @copyright Copyright (C) October 2013 prestabrain.com <@emai:prestabrain@gmail.com>
 *               <info@prestabrain.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */

class PtsWidgetTestimonial extends PtsWidgetPageBuilder {

		public $name = 'testimonial';

	

		public  static function getWidgetInfo(){
			return array( 'label' => 'Testimonials', 'explain' => 'Integrate with Testimonial Module to show testimonials', 'group' => 'prestabrain'  );
		}

		public static function renderButton(){

		}

		public function renderForm( $args, $data ){
			$helper = $this->getFormHelper();

			$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
			
			$helper->tpl_vars = array(
	                'fields_value' => $this->getConfigFieldsValues( $data  ),
	                'languages' => Context::getContext()->controller->getLanguages(),
	                'id_language' => $default_lang
        	); 

			$module = Module::getInstanceByName('ptsbttestimonials');
			if(!$module || (isset($module->id) && (!$module->id || !$module->active))){
				$this->fields_form[1]['form'] = array(
		            'legend' => array(
		                'title' => $this->l('Widget Form.'),
		                'desc' => $this->l('You need install or active the module ptsbttestimonials before')
		            ),
	            );
				return  $helper->generateForm( $this->fields_form );
			}
        	$styles = array(
        		array('id' => 'testimonials-v1', 'name' => $this->l('Testimonial v1')),
        		array('id' => 'testimonials-v2', 'name' => $this->l('Testimonial v2')),
        		array('id' => 'testimonials-v3', 'name' => $this->l('Testimonial v3')),
        		array('id' => 'testimonials-v4', 'name' => $this->l('Testimonial v4')),
        		array('id' => 'testimonials-v5', 'name' => $this->l('Testimonial v5')),
        		array('id' => 'testimonials-v6', 'name' => $this->l('Testimonial v6')),
    		);
        	$positions = array(
        		array('id' => 'left', 'name' => $this->l('Left')),
        		array('id' => 'center', 'name' => $this->l('Center')),
        		array('id' => 'right', 'name' => $this->l('Right')),
    		);
			$this->fields_form[1]['form'] = array(
	            'legend' => array(
	                'title' => $this->l('Widget Form.'),
	            ),
	            'input' => array(
	                array(
	                    'type' 	  => 'select',
	                    'label'   => $this->l( 'Testimonial Style' ),
	                    'name' 	  => 'testimonial_style',
	                    'options' => array(  'query' => $styles ,
		                    'id' 	  => 'id',
		                    'name' 	  => 'name' ),
	                    'default' => "testimonial-v1"
	                ),
	                array(
	                    'type' 	  => 'select',
	                    'label'   => $this->l('Testimonial Position'),
	                    'name' 	  => 'testimonial_position',
	                    'options' => array(  'query' => $positions ,
		                    'id' 	  => 'id',
		                    'name' 	  => 'name' ),
	                    'default' => "center",
	                    'desc' => $this->l('Apply for Testimonial Style: V1 annd V3')
	                ),
	 				array(
	                    'type'  => 'text',
	                    'label' => $this->l('Limit'),
	                    'name'  => 'testimonial_limit',
	                    'class' => 'testimonial_limit',
	                    'default'=> '6'
	                ),
	     			array(
	                    'type'  => 'text',
	                    'label' => $this->l('Column'),
	                    'name'  => 'column',
	                    'default'=> 3,
	                    'desc'	=> $this->l('Show In Carousel with N Column in each page')
	                ),
	                array(
	                    'type'  => 'text',
	                    'label' => $this->l('Items Per Page'),
	                    'name'  => 'itemsperpage',
	                    'default'=> 3,
	                    'desc'	=> $this->l('Show In Carousel, Max Products in each page')
	                ),
	            ),
	      		'submit' => array(
	                'title' => $this->l('Save'),
	                'class' => 'button'
           		)
	        );

 			$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
			
			$helper->tpl_vars = array(
	                'fields_value' => $this->getConfigFieldsValues( $data  ),
	                'languages' => Context::getContext()->controller->getLanguages(),
	                'id_language' => $default_lang
        	);
		 	 
			return  $helper->generateForm( $this->fields_form );

		}
		
		public function renderContent(  $args, $setting ){
			$t = array(
				'testimonial_limit' => 6,
				'testimonial_style' => 'testimonials-v1',
				'testimonial_position' => 'center',
				'column' => 3,
				'itemsperpage' => 3,
			);

			$setting = array_merge( $t, $setting );

			$module = Module::getInstanceByName('ptsbttestimonials');
			if(!$module || (isset($module->id) && (!$module->id || !$module->active))){
				$output = array('type'=>'testimonial','data' => $setting );
				return $output;
			}
			$testimonials = $module->getTestimonials(true, $setting['testimonial_limit']);
			$setting['testimonials'] = $testimonials;
			$setting['testimonial_img_link'] = _MODULE_DIR_.'ptsbttestimonials/images/';
			$setting['testimonial_key'] = rand(0, 1000);
			
			$output = array('type'=>'testimonial', 'data' => $setting);
			return $output;
		}
		 
	}
?>