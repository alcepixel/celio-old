<?php
/**
 * Pts Prestashop Theme Framework for Prestashop 1.6.x
 *
 * @package   ptspagebuilder
 * @version   5.0
 * @author    http://www.prestabrain.com
 * @copyright Copyright (C) October 2013 prestabrain.com <@emai:prestabrain@gmail.com>
 *               <info@prestabrain.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */

class PtsWidgetUtilproduct_list extends PtsWidgetPageBuilder {

		public $name = 'utilproduct_list';
		public $group = 'product';
		
		public static function getWidgetInfo()
		{
			return array('label' =>  ('Util Product List'), 'explain' => 'Product List With Option: Newest, Bestseller, Special, Featured using utilcarousel jquery plugin', 'group' => 'prestashop'  );
		}


		public function renderForm($args=null, $data)
		{
			$helper = $this->getFormHelper();

			$types = array();	
		 	$types[] = array(
		 		'value' => 'newest',
		 		'text'  => $this->l('Products Newest')
		 	);
		 	$types[] = array(
		 		'value' => 'bestseller',
		 		'text'  => $this->l('Products Bestseller')
		 	);

		 	$types[] = array(
		 		'value' => 'special',
		 		'text'  => $this->l('Products Special')
		 	);

		 	$types[] = array(
		 		'value' => 'featured',
		 		'text'  => $this->l('Products Featured')
		 	);

		 	$types[] = array(
		 		'value' => 'toprating',
		 		'text'  => $this->l('Products Top Rating')
		 	);

		 	$types[] = array(
		 		'value' => 'mostview',
		 		'text'  => $this->l('Products Most View')
		 	);
			$lists = array();
		 	$lists[] = array(
		 		'value' => 'list1',
		 		'text'  => $this->l('List1')
		 	);
			$lists[] = array(
		 		'value' => 'list2',
		 		'text'  => $this->l('List2')
		 	);

		 	$lists[] = array(
		 		'value' => 'grid',
		 		'text'  => $this->l('Grid')
		 	);
			
			$this->fields_form[1]['form'] = array(
	            'legend' => array(
	                'title' => $this->l('Widget Form.'),
	            ),
	            'input' => array(
	                
 					 array(
	                    'type'  => 'text',
	                    'label' => $this->l('Limit'),
	                    'name'  => 'limit',
	                    'default'=> 6,
	                ),
	     			array(
                        'type' => 'text',
                        'label' => $this->l('Item width range min:'),
                        'name' => 'utilmin_width',
                        'desc' => $this->l('The item width range when responsiveMode in \'itemWidthRange\'. Use it like [200, 400],means UtilCarousel will keep the item\'s width between 200px to 400px and auto caculate how many items to show.'),
                        'default' => 180,
                        'suffix' => 'px'
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Item width range max:'),
                        'name' => 'utilmax_width',
                        //'desc' => $this->l('The item width range when responsiveMode in \'itemWidthRange\'. Use it like [200, 400],means UtilCarousel will keep the item\'s width between 200px to 400px and auto caculate how many items to show.'),
                        'default' => 220,
                        'suffix' => 'px'
                    ),


	                array(
	                    'type' 	  => 'select',
	                    'label'   => $this->l( 'Products List Type' ),
	                    'name' 	  => 'list_type',
	                    'options' => array(  'query' => $types ,
	                    'id' 	  => 'value',
	                    'name' 	  => 'text' ),
	                    'default' => "newest",
	                    'desc'    => $this->l( 'Select a Product List Type' )
	                ),
	                /*
					array(
	                    'type' 	  => 'select',
	                    'label'   => $this->l( 'List Mode' ),
	                    'name' 	  => 'list_mode',
	                    'options' => array(  'query' => $lists ,
	                    'id' 	  => 'value',
	                    'name' 	  => 'text' ),
	                    'default' => "grid",
	                ),
					*/
	                array(
                        'type' => 'text',
                        'label' => $this->l('Interval'),
                        'name' => 'utilinterval',
                        'desc' => $this->l('Enter Time(miniseconds) to play carousel. Value 0 to stop.'),
                        'default' => '0',
                        'suffix' => 'ms'
                    ),
	            ),
	      		 'submit' => array(
	                'title' => $this->l('Save'),
	                'class' => 'button'
           		 )
	        );

 
		 	$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
			
			$helper->tpl_vars = array(
	                'fields_value' => $this->getConfigFieldsValues( $data  ),
	                'languages' => Context::getContext()->controller->getLanguages(),
	                'id_language' => $default_lang
        	);  

			return  $helper->generateForm( $this->fields_form );
		}

		public function renderContent($args, $setting)
		{ 
			$t = array(
				'list_type'=> '',
				'list_mode'=> '',
				'limit' => 12,
				'image_width'=>'200',
				'image_height' =>'200',
				'utilmin_width'	=> 180,
				'utilmax_width'	=> 220,
			); 

			$products = array();
			$setting = array_merge( $t, $setting );
			// die('');
			//

			switch ( $setting['list_type'] ) {
				case 'newest':
				 	 $products = Product::getNewProducts(  $this->langID, 0, (int)$setting['limit'] );
					break;
				
	 			case 'featured':
	 				$category = new Category(Context::getContext()->shop->getCategory(), $this->langID );
					$nb = (int)$setting['limit'];
			 		$products = $category->getProducts((int)$this->langID, 1, ($nb ? $nb : 8));
			 		break;
			 	case 'bestseller':
					$products = ProductSale::getBestSalesLight((int)$this->langID, 0, (int)$setting['limit']);
			 		break;	
			 	case 'special': 
			 		 $products = Product::getPricesDrop( $this->langID, 0, (int)$setting['limit'] );
			 		break;
		 		case 'toprating': 
			 		 $products = self::getProducts( 0, (int)$setting['limit'] );
			 		break;
		 		case 'mostview': 
			 		 $products = self::getProducts( 0, (int)$setting['limit'], true, null, 'mostview' );
			 		break;
			}
			/*
			$dir = _PS_MODULE_DIR_.'/ptspagebuilder/views/templates/front/widgets/sub/products.tpl';
			if($setting['list_mode'] == 'list1'){
				$tdir = _PS_ALL_THEMES_DIR_ . _THEME_NAME_ . '/modules/ptspagebuilder/views/templates/front/widgets/sub/products_list1.tpl';
			}
			elseif($setting['list_mode'] == 'list2'){
				$tdir = _PS_ALL_THEMES_DIR_ . _THEME_NAME_ . '/modules/ptspagebuilder/views/templates/front/widgets/sub/products_list2.tpl';
			}
			else{
				$tdir = _PS_ALL_THEMES_DIR_ . _THEME_NAME_ . '/modules/ptspagebuilder/views/templates/front/widgets/sub/products.tpl';
			}
            if (file_exists($tdir)) {
                $dir = $tdir;
            }

            $setting['product_tpl']  = $dir;
			*/
			//echo "<pre>".print_r($products,1);die;
			$setting['products'] = $products;
			$output = array('type'=>'utilproducts','data' => $setting );
 

			return $output;
		}

		public static function getProducts($p = 1, $n, $active = true, Context $context = null, $type = 'toprating')
	    {
	        if (!$context)
	            $context = Context::getContext();
	        $id_lang = $context->language->id;

	        $front = true;
	        if (!in_array($context->controller->controller_type, array('front', 'modulefront')))
	            $front = false;

	        if ($p < 1) $p = 1;

	        $id_supplier = (int)Tools::getValue('id_supplier');

	        $sql = 'SELECT p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity, MAX(product_attribute_shop.id_product_attribute) id_product_attribute, product_attribute_shop.minimal_quantity AS product_attribute_minimal_quantity, pl.`description`, pl.`description_short`, pl.`available_now`,
	                pl.`available_later`, pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`, pl.`name`, MAX(image_shop.`id_image`) id_image,
	                il.`legend`, m.`name` AS manufacturer_name, cl.`name` AS category_default,
	                DATEDIFF(product_shop.`date_add`, DATE_SUB(NOW(),
	                INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).'
	                    DAY)) > 0 AS new, product_shop.price AS orderprice'. ($type == 'mostview' ? ', count(cnn.id_connections) as pages' : ''). ($type == 'toprating' ? ', AVG(pc.grade) as avg_grade' : '').'
	            FROM `'._DB_PREFIX_.'category_product` cp
	            LEFT JOIN `'._DB_PREFIX_.'product` p
	                ON p.`id_product` = cp.`id_product`
	            '.Shop::addSqlAssociation('product', 'p').'
	            LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa
	            ON (p.`id_product` = pa.`id_product`)
	            '.Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.`default_on` = 1').'
	            '.Product::sqlStock('p', 'product_attribute_shop', false, $context->shop).'
	            LEFT JOIN `'._DB_PREFIX_.'category_lang` cl
	                ON (product_shop.`id_category_default` = cl.`id_category`
	                AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('cl').')
	            LEFT JOIN `'._DB_PREFIX_.'product_lang` pl
	                ON (p.`id_product` = pl.`id_product`
	                AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').')
	            LEFT JOIN `'._DB_PREFIX_.'image` i
	                ON (i.`id_product` = p.`id_product`)'.
	            Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
	            LEFT JOIN `'._DB_PREFIX_.'image_lang` il
	                ON (image_shop.`id_image` = il.`id_image`
	                AND il.`id_lang` = '.(int)$id_lang.')
	            LEFT JOIN `'._DB_PREFIX_.'manufacturer` m
	                ON m.`id_manufacturer` = p.`id_manufacturer`

	            '. ($type == 'mostview' ? '
				LEFT JOIN `'._DB_PREFIX_.'page` pag ON (cp.id_product = pag.id_object)
				LEFT JOIN `'._DB_PREFIX_.'connections` cnn ON (cnn.id_page = pag.id_page)
				LEFT JOIN `'._DB_PREFIX_.'page_type` pat ON (pat.id_page_type = pag.id_page_type)
				' : '').
				($type == 'toprating' ? '
				JOIN `'._DB_PREFIX_.'product_comment` pc ON (cp.id_product = pc.id_product AND pc.validate = 1)
				' : '').'

	            WHERE product_shop.`id_shop` = '.(int)$context->shop->id.($type == 'mostview' ? ' AND pat.`name` = \'product\'' : '')
	            .($active ? ' AND product_shop.`active` = 1' : '')
	            .($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '')
	            .($id_supplier ? ' AND p.id_supplier = '.(int)$id_supplier : '');
				
			if($type == 'mostview'){
				$sql .=' GROUP BY cnn.id_page';
				$sql .= ' ORDER BY pages DESC ';
			}elseif($type == 'toprating'){
				$sql .= ' GROUP BY product_shop.id_product ';
	        	$sql .= ' ORDER BY avg_grade DESC ';
	        }
	        $sql .=' LIMIT '.(((int)$p - 1) * (int)$n).','.(int)$n;
	        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
	        if (!$result)
	            return array();
	        /* Modify SQL result */
	        return Product::getProductsProperties($id_lang, $result);
	    }

	}
?>