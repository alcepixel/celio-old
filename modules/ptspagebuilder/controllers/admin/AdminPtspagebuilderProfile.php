<?php
/**
 * Pts Prestashop Theme Framework for Prestashop 1.6.x
 *
 * @package   ptspagebuilder
 * @version   5.0
 * @author    http://www.prestabrain.com
 * @copyright Copyright (C) October 2013 prestabrain.com <@emai:prestabrain@gmail.com>
 *               <info@prestabrain.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */

require_once( _PS_MODULE_DIR_.'ptspagebuilder/loader.php' );

class AdminPtspagebuilderProfileController extends ModuleAdminController {
		
	public $widget = null;
    public $profile; 
 	public function __construct()
	{
		$this->bootstrap = true;
		$this->display = 'view';
		$this->addRowAction('view');
        
        parent::__construct(); 
		$this->profile = new PtsPagebuilderprofile( Tools::getValue('id_pagebuilderprofile'), null, $this->context->shop->id );
		$this->profile->loadWidgets();

		if(Tools::getValue('dupplicatforalllanguages')){
            $this->module->dupplicatForAllLanguages();
        }
 	}


	public function initPageHeaderToolbar()
	{	
		parent::initPageHeaderToolbar();

	    $this->context->controller->addCSS( __PS_BASE_URI__.'modules/ptspagebuilder/assets/admin/style.css' ); 	
		if( Tools::getValue('widgetaction') ) {
		 	$this->context->controller->addCSS( __PS_BASE_URI__.'modules/ptspagebuilder/assets/admin/widgetaction.css' ); 
    	 	return ;
    	}

    	$this->context->controller->addJqueryUI('ui.sortable');
    	$this->context->controller->addJqueryUI('ui.resizable');
    	$this->context->controller->addJqueryUI('ui.dialog');
	

    	$this->profile->widgetBeforeProcess( $this );
    	if(file_exists(_PS_ROOT_DIR_.'/js/admin/tinymce.inc.js'))
    		$tinymce_inc = _PS_JS_DIR_.'admin/tinymce.inc.js';
    	else
    		$tinymce_inc = _PS_JS_DIR_.'tinymce.inc.js';
    	$this->addJS(array(
			_PS_JS_DIR_.'tiny_mce/tiny_mce.js',
			$tinymce_inc
		));

		$this->addCSS(array(
			_PS_JS_DIR_.'jquery/plugins/timepicker/jquery-ui-timepicker-addon.css',
		));
		  
		$this->context->controller->addJqueryPlugin('colorpicker');

    	$this->context->controller->addJS( __PS_BASE_URI__.'modules/ptspagebuilder/assets/admin/data.js' ); 
    	$this->context->controller->addJS( __PS_BASE_URI__.'modules/ptspagebuilder/assets/admin/layout.js' ); 
    	$this->context->controller->addJS( __PS_BASE_URI__.'modules/ptspagebuilder/assets/admin/widget.js' ); 

		$this->page_header_toolbar_title = $this->l('Page Builder Editor');

		if( $this->profile->id ){
			$this->page_header_toolbar_title .= ' - '.$this->l( 'Edit:' ).''.$this->profile->name.''; 
		}else {
			$this->page_header_toolbar_title .= ' - '.$this->l( 'Create New A Profile' );
		}
		$this->page_header_toolbar_btn = array();

 
		$this->page_header_toolbar_btn['save'] = array(
            'href' => 'index.php?tab=AdminPtspagebuilderProfile&token='.Tools::getAdminTokenLite('AdminPtspagebuilderProfile').'&action=savelayout',
			'id'   => 'frmsavealll',
            'desc' => $this->l('Save Layout Profile'),
            'js' => "$('#frmsavealll').submit(); return false;"
        );
		$token = Tools::getAdminTokenLite('AdminPtspagebuilderProfile');

		if( $this->profile->id ) {
			
			$this->page_header_toolbar_btn['default'] = array(
	            'href' => 'index.php?tab=AdminPtspagebuilderProfile&token='.Tools::getAdminTokenLite('AdminPtspagebuilderProfile').'&setdefault=1&id_pagebuilderprofile='.$this->profile->id,
				'id'   => 'default',
	            'desc' => $this->l('Set This As Default'),
                'icon' => 'process-icon-save'
	        );

			$this->page_header_toolbar_btn['duplicate'] = array(
				'short' => $this->l('Duplicate', null, null, false),
				'href' => '#',
				'desc' => $this->l('Duplicate', null, null, false),
				'confirm' => 1,
				'js' => 'if (confirm(\''.$this->l('Are you sure to copy this?', null, true, false).' ?\')) document.location = \''.$this->context->link->getAdminLink('AdminPtspagebuilderProfile', null, true, false).'&token='.$token.'&id_pagebuilderprofile='.(int)$this->profile->id.'&duplicateprofile\'; else document.location = \''.$this->context->link->getAdminLink('AdminPtspagebuilderProfile', null, true, false).'&id_pagebuilderprofile='.(int)$this->profile->id.'&token='.$token.'\';'
			);

			
			$this->page_header_toolbar_btn['export'] = array(
	            'href' => 'index.php?tab=AdminPtspagebuilderProfile&token='.Tools::getAdminTokenLite('AdminPtspagebuilderProfile').'&exportProfile=1&id_pagebuilderprofile='.$this->profile->id,
				'id'   => 'export',
	            'desc' => $this->l('Export This')
	        );


		
		}

	}

	public function processDuplicate()
	{
		$profile = new PtsPagebuilderprofile( Tools::getValue('id_pagebuilderprofile') );
			$profile->name = $this->l( 'Copy Of' ).'  '.$profile->name;
			$profile->id = 0;
			$profile->id_pagebuilderprofile = 0;
			$errors = array();
			if ( $profile->id<=0 && !$profile->add() ) {
			$errors[] = $this->displayError($this->l('The slide could not be added.'));
		}	

		Tools::redirectAdmin(AdminController::$currentIndex.'&id_pagebuilderprofile='.$profile->id.'&token='.Tools::getAdminTokenLite('AdminPtspagebuilderProfile'));
			
	}

	protected function isValidPostData()
	{
		return is_array($_POST); 
	}

	/**
	 *
	 */
	public function postProcess()
	{
		if( Tools::getValue('exportProfile') ){
			if( $this->profile->id ) {
				 $export = array();
				 $export['layout'] = $this->profile->layout;
				 $export['widget'] = $this->profile->widget;
				

				header("Content-Type: plain/text");
				header("Content-Disposition: Attachment; filename=".$this->profile->name.".txt");
				header("Pragma: no-cache");

				echo  serialize($export); exit;
			}else {

			}	
		}

		/* import data */
		if( Tools::isSubmit('submitptspagebuilderImport') && isset($_FILES['import_file']) ){
			$tmp = $_FILES['import_file']['tmp_name'];

			$content = trim(Tools::file_get_contents( $tmp )); 

			if( $content ){
				$data = unserialize( $content );
				if( isset($data['layout']) && isset($data['widget']) ){
					$profile = new PtsPagebuilderprofile();
					$profile->layout =  $data['layout'];
					$name = trim( Tools::getValue('import_name') );   
					$profile->name = $name?$name:"profile-" . time();
					$profile->widget =  $data['widget'];	
					if ( $profile->id<=0 ) {
						if(!$profile->add())
							$this->displayError( $this->l('The slide could not be added.') );
					}else {
						$this->errors[] = Tools::displayError('You do not have permission to add this.');
					}	
				} 	
			}
			$this->module->_clearBLHLCache();
		}

		if (Tools::getIsset('duplicateprofile'))
		{  
			if ($this->tabAccess['add'] === '1'){
				$this->processDuplicate();
			}else{
				$this->errors[] = Tools::displayError('You do not have permission to add this.');
			}
			$this->module->_clearBLHLCache();
		}
		
		if( Tools::isSubmit('savelayoutbuilder') && $this->isValidPostData()  ){
			$profile = new PtsPagebuilderprofile( Tools::getValue('id_pagebuilderprofile') );
			$profile->layout = Tools::getValue( 'wpolayout' );	
			
			$wpowidget = Tools::getValue( 'wpowidget' );
			if($wpowidget){
				foreach ($wpowidget as &$value) {
					$value['config'] = Tools::stripslashes($value['config']);
				}
			}
			$profile->widget =   serialize(  $wpowidget );
 

			$name = trim( Tools::getValue('name') ); 
			$profile->name = $name?$name:"profile-" . time();
			$errors = array();
			
			if(  $profile->id && !$profile->update() ){ 
				$errors[] = $this->displayError($this->l('The slide could not be updated.'));
			}else {
				if ( $profile->id<=0 && !$profile->add() ) {
					$errors[] = $this->displayError($this->l('The slide could not be added.'));
				}	
			}
			$this->module->_clearBLHLCache();
			Tools::redirectAdmin(AdminController::$currentIndex.'&id_pagebuilderprofile='.$profile->id.'&token='.Tools::getAdminTokenLite('AdminPtspagebuilderProfile'));
		}

		if( Tools::getValue('setdefault') ){
			$profile = new PtsPagebuilderprofile( Tools::getValue('id_pagebuilderprofile') );
			$profile->setDefault();

			$this->module->_clearBLHLCache();
			Tools::redirectAdmin(AdminController::$currentIndex.'&id_pagebuilderprofile='.$profile->id.'&token='.Tools::getAdminTokenLite('AdminPtspagebuilderProfile'));
		}
	}

	public function ajaxDoDeleteProfile()
	{
		if( Tools::getValue('id_pagebuilderprofile') ){
		 	$profile = new PtsPagebuilderprofile( Tools::getValue('id_pagebuilderprofile') );
		 	if( !$profile->isDefault() ){
		 		$profile->delete();
		 	}
		}
		$output = new stdClass();
		$output->delete=1;
		$output->id = (int)Tools::getValue('id_pagebuilderprofile');
	    echo Tools::jsonEncode( $output );exit();
	}	

	public function ajaxDoWidgetdata()
	{
		$widgets =  ( trim($this->profile->widget) );
		$widgets = unserialize( $widgets );

		$wkey = Tools::getValue( 'wkey' ); 
		if( isset($widgets[$wkey]) && isset($widgets[$wkey]['config']) ){
			$data = ( ( $widgets[$wkey]['config'] ) );
			echo $data;
		}
		exit();			
	}

	public function ajaxDoSavewidget()
	{
		if( Tools::getValue('controller') &&  isset($_POST) && $_POST){
		 	$wpost = $_POST;
		 	$exls = array('action', 'ajax', 'controller', 'id_tab') ;

		 	foreach( $exls as $e ){
		 		if( isset($wpost[$e]) ){
		 			unset($wpost[$e]);
		 		}
		 	}
		 	$post = array();
		 	$post['widget'] = $wpost;
		 	$post['wkey'] = Tools::getValue('wkey');


	 		foreach( $post['widget'] as $key=>$value ){
	 			if(is_array($value)){
	 				foreach ($value as &$val) {
	 					$val = html_entity_decode( $val , ENT_QUOTES, 'UTF-8');
	 				}
	 				$post['widget'][$key] = base64_encode( implode(',', $value ) );
	 			}else{
		 			$value = html_entity_decode( $value , ENT_QUOTES, 'UTF-8');
		 			$post['widget'][$key] = preg_replace( "#\n|\r|\t#","", base64_encode( $value ) );
	 			}
		 	}

		 	if(Tools::getIsset('categorytab') && Tools::getValue('categorytab')){
		 		$post['widget']['categorytab'] = base64_encode(Tools::jsonEncode(Tools::getValue('categorytab')));
		 	}
		 	if(Tools::getIsset('promotions') && Tools::getValue('promotions')){
		 		$post['widget']['promotions'] = base64_encode(Tools::jsonEncode(Tools::getValue('promotions')));
		 	}

			$content = trim(( serialize($post) )) ;  
			 
			$output = new stdClass();
			$output->wkey     = $post['wkey'];
		 	$output->config = $content;
			$output->name  = isset($wpost['widget_name'])?$wpost['widget_name']:"";
			 
			
			echo Tools::jsonEncode( $output );exit();
		}  		 
	}

	
	public function ajaxDoWidgetform()
	{
		if( Tools::getValue('wtype') ){ 

			$template = $this->createTemplate('widgetform.tpl');
			$data = Tools::getValue('data') ? unserialize( ( trim(Tools::getValue('data')) ) ) : array(); 

			foreach( $data['widget'] as $key => $value ){
			 	$data['widget'][$key] = Tools::stripslashes(base64_decode(str_replace( " ", "+", ($value) )));
			}
				
				//d( $data );
			
		 	$data['widget']['wkey'] =  Tools::getValue('wkey');
			$form = $this->profile->renderForm( Tools::getValue('wtype'), array(), array('params'=>$data['widget']) );

			$template->assign( array(
				'showed' 		 => 1,	
				'wkey' => Tools::getValue('wkey'),
				'form' => $form, 
			));

			return $template->fetch();
		}	
	}

	public function ajaxDoEditwidget()
	{
		 return $this->ajaxDoWidgetform();
	}



	public function getConfigFieldsValues( $data = null )
	{
        $fields_values = array();

        foreach ( $this->_fields_form as $k => $f ) {
            foreach ( $f['form']['input'] as $i => $input ) {
                if( isset($input['lang']) ) {
                    foreach ( $this->languages() as $lang ) {
                        $values = Tools::getValue( $input['name'].'_'.$lang['id_lang'], Configuration::get($input['name'], $lang['id_lang']) );
                        $fields_values[$input['name']][$lang['id_lang']] = $values ? $values : $input['default'];
                    }
                }else {
                    $values = Tools::getValue( $input['name'], Configuration::get($input['name']) );
                    $fields_values[$input['name']] = $values ? $values : $input['default'];
                }
            }
        }
    
        return $fields_values;
    }


	public function ajaxDoListwidgets()
	{
		$template = $this->createTemplate('widgets.tpl');
		$widgets =  $this->profile->getButtons();

		$template->assign( array(
			'showed' 		 => 1,
			'groups'         => 	$widgets['groups'] ,
			'widgets'        =>	$widgets['widgets'] 
		));

		return $template->fetch();
	}

	public function renderView()
	{ 
		if( Tools::getValue('widgetaction') ){
			$wobj = $this->profile->loadWidgetObject( Tools::getValue('type'), $this  );

			if( $wobj ){
				$template = $this->createTemplate('widget_action_content.tpl');
				$content = $wobj->renderAdminContent();

				$template->assign( array(
					'content' => $content ,
					'test'    => ''	
				));
					
			} else {
				$template = $this->createTemplate('widegt_error.tpl');
			}
			return $template->fetch();
		} else {
			if( $this->ajax ){
				$method = "ajaxDo".Tools::ucfirst(trim( Tools::getValue('action')) );
				if( method_exists( $this , $method )){
					echo $this->{$method}();
				}
				exit();
			}else {
				$link = $this->context->link;

				$quicktools = array();

				$template = $this->createTemplate('editor.tpl');

	 			$profiles = $this->profile->getList();

	 		 	$layoutjson = '';
	 		 	if( $this->profile->layout ){
	 		 		$layoutjson =  ( trim($this->profile->layout) );
	 		 	}	 


	 		 	$output = array(); 
	 		 	$profiles = $this->profile->getList();
	 		    foreach( $profiles as $row ){ 
					$output[] = array( 'name'=> ($row['name'] )?$row['name']:$this->l('No Name'), 'id'=> $row['id_pagebuilderprofile'] ,'isdefault' => $row['isdefault']) ;	
				}

				$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
					$url = _PAGEBUILDER_IMAGE_URL_;
					// Tools::htmlentitiesutf8($protocol . $_SERVER['HTTP_HOST'] . __PS_BASE_URI__) . //
					/** 
					 * Column Form
					 */	 			 		
			 		$sfxclss = PtsPagebuilderHelper::detectSfxClasses(  );


	 				// d( $profiles );
   				// echo '<pre>'.print_r( json_decode($layoutjson), 1 );die; 

				$template->assign( array(
			 		'listwidgets'	 => $link->getAdminLink('AdminPtspagebuilderProfile')."&ajax=true",
			 		'savelayout'	 => $link->getAdminLink('AdminPtspagebuilderProfile')."&savelayout=true",
			 		'widgetform'	 => $link->getAdminLink('AdminPtspagebuilderProfile')."&ajax=true",
			 		'filemagement_uri'					=>$link->getAdminLink('AdminPtspagebuilderImage'),
			 		'id_pagebuilderprofile' => $this->profile->id ,
			 		'widgetdata'	=> $link->getAdminLink('AdminPtspagebuilderProfile')."&ajax=true&id_pagebuilderprofile=".Tools::getValue('id_pagebuilderprofile'),
					'showed' 		 => 1,
					'profile'		=> $this->profile,
                    'moduleInShop' => $this->profile->checkProfileInShop(),
		 			'profiles'		 => $output,
		 			'profile_link'	 =>$link->getAdminLink('AdminPtspagebuilderProfile'),
		 			'layoutjson'     => $layoutjson,
		 			'frontend_link' => $url,
		 			'widgetdata'	 => '',
		 			'sfxclss'		=> $sfxclss 
	
				));

				return $template->fetch();
			}	
		}	
	}
	
	 
 }
?>