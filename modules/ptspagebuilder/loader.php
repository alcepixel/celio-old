<?php
/**
 * Pts Prestashop Theme Framework for Prestashop 1.6.x
 *
 * @package   ptspagebuilder
 * @version   5.0
 * @author    http://www.prestabrain.com
 * @copyright Copyright (C) October 2013 prestabrain.com <@emai:prestabrain@gmail.com>
 *               <info@prestabrain.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */

define( "_PAGEBUILDER_WIDGET_DIR_", _PS_MODULE_DIR_.'ptspagebuilder/classes/widget/' );

define( "_PAGEBUILDER_IMAGE_DIR_",  _PS_MODULE_DIR_.'ptspagebuilder/images/' );
define( "_PAGEBUILDER_IMAGE_URL_",  _MODULE_DIR_.'ptspagebuilder/images/' );

 
if( !is_dir(_PAGEBUILDER_IMAGE_DIR_) ){
	mkdir(_PAGEBUILDER_IMAGE_DIR_, 0777);
}

require_once( _PS_MODULE_DIR_.'ptspagebuilder/classes/helper.php' );
require_once( _PS_MODULE_DIR_.'ptspagebuilder/classes/widgetbase.php' );
require_once( _PS_MODULE_DIR_.'ptspagebuilder/classes/profile.php' );

?>