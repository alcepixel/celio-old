<?php
/**
 * Pts Prestashop Theme Framework for Prestashop 1.6.x
 *
 * @package   ptspagebuilder
 * @version   5.0
 * @author    http://www.prestabrain.com
 * @copyright Copyright (C) October 2013 prestabrain.com <@emai:prestabrain@gmail.com>
 *               <info@prestabrain.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */

if (!defined('_PS_VERSION_'))
    exit;

require_once( _PS_MODULE_DIR_.'ptspagebuilder/loader.php' );

class PtsPageBuilder extends Module {
    /**
     * @var String $_prefix
     */
    private $_prefix;

    private $_fields_form = array();

    private $profile = null;

    private $themeName = null;

    public function __construct() {
        $this->name = 'ptspagebuilder';
        $this->tab = 'pricing_promotion';
        $this->version = '5.0';
        $this->author = 'PrestaBrain';
        $this->need_instance = 0;

        $this->bootstrap = true;
        parent::__construct();
        $this->_prefix = 'ptspagebuilder';

        $this->displayName = $this->l('Pts Page Builder');
        $this->description = $this->l('Lightweigh and flexiable to builder great home page having the best look.');
    }

    public function install() {
        $a = (parent::install() AND $this->registerHook('home') AND $this->registerHook('header') AND $this->registerHook('categoryAddition') AND
			$this->registerHook('categoryUpdate') AND
			$this->registerHook('categoryDeletion') AND
			$this->registerHook('actionAdminMetaControllerUpdate_optionsBefore')
			AND $this->registerHook('addproduct')
			AND $this->registerHook('updateproduct')
            AND $this->registerHook('deleteproduct')
			AND $this->registerHook('actionObjectLanguageAddAfter')
			);
			
        $this->_clearBLHLCache();
        $this->createTables();
        
        $this->installModuleTab( 'Page Builder Profiles', 'profile',  'AdminParentModules' );
        $this->installModuleTab( 'Footer Builder Profiles', 'footer',  'AdminParentModules' );
        $this->installModuleTab( 'Page Images Management', 'image',  'AdminParentModules' );

        return $a;
    }
    /**
     * Creates tables
     */
    protected function createTables() {
        return true;
        $res = 1;
        include_once( dirname(__FILE__) . '/install/install.php' );
        return $res;
    }
    /**
     * Uninstall
     */
    private function uninstallModuleTab($class_sfx = '') {
        $tabClass = 'Admin' . Tools::ucfirst($this->name) . Tools::ucfirst($class_sfx);

        $idTab = Tab::getIdFromClassName($tabClass);
        if ($idTab != 0) {
            $tab = new Tab($idTab);
            $tab->delete();
            return true;
        }
        return false;
    }
    /**
     * Install Module Tabs
     */
    private function installModuleTab($title, $class_sfx = '', $parent = '') {
        $class = 'Admin' . Tools::ucfirst($this->name) . Tools::ucfirst($class_sfx);
        if ($parent == '') {
            $position = Tab::getCurrentTabId();
        } else {
            $position = Tab::getIdFromClassName($parent);
        }

        $tab = new Tab();
        $tab->class_name = $class;
        $tab->module = $this->name;
        $tab->id_parent = (int)$position;
        $langs = Language::getLanguages(false);
        foreach ($langs as $l) {
            $tab->name[$l['id_lang']] = $title;
        }
        $id_tab = $tab->add(true, false);
    }

    public function uninstall() {
        $this->_clearBLHLCache();
        
        $this->uninstallModuleTab("footer");
        $this->uninstallModuleTab("profile");
        $this->uninstallModuleTab("image");

        return parent::uninstall();
    }

    public function getContent() {
        
        $profile_id = PtsPagebuilderprofile::getDefaultProfile();
        Tools::redirectAdmin('index.php?tab=AdminPtspagebuilderProfile&id_pagebuilderprofile='.$profile_id.'&token='.Tools::getAdminTokenLite('AdminPtspagebuilderProfile'));
        
        return ;
    }
    
    public function hookdisplayHeader( $params ){
        $this->themeName = Context::getContext()->shop->getTheme();
        $this->context->controller->addCSS( $this->_path . '/assets/pagebuilder.css', 'all' );
        if( isset($this->context->controller->php_self) && $this->context->controller->php_self =='index' && ( $this->context->controller instanceof IndexController)){
            $this->context->controller->addCSS( $this->_path . '/assets/util.carousel.css', 'all' );
            $this->context->controller->addJS(($this->_path) . '/assets/pagebuilder.js', 'all');
            $this->context->controller->addJS(($this->_path) . '/assets/countdown.js', 'all');
            $this->context->controller->addJS(($this->_path) . '/assets/jquery.utilcarousel.min.js', 'all');
            $this->context->smarty->assign(array(
                'PTS_PAGEBUILDER_ACTIVED'        =>  true,
                'PTS_PAGEBUILDER_FULL'          => true,
                'PTS_PAGEBUILDER_CONTENT'       => $this->_processHook( $params )
            ));
        } 

        if( $this->getThemeConfig('enable_fbuilder') ){  
            $this->context->smarty->assign(array(
                'PTS_FOOTERBUILDER_ACTIVED'        =>  true,
                'PTS_FOOTERBUILDER_CONTENT'       => $this->_processHook( $params , true )
            ));
        }    
    }

    public function hookDisplayHome($params) {
        //return $this->_processHook($params);
    }

    /**
     *
     */
    public function hookDisplaySlideshow($params) {
        return $this->_processHook($params);
    }

    /**
     *
     */
    public function hookDisplayPromoteTop($params) {
        return $this->_processHook($params);
    }

    /**
     *
     */
    public function hookDisplayBottom($params) {
        return $this->_processHook($params);
    }

    /**
     *
     */
    public function hookDisplayContentBottom($params) {
        return $this->_processHook($params);
    }

    public function hookActionObjectLanguageAddAfter($params){
        $id_lang_default = Configuration::get('PS_LANG_DEFAULT');
        $id_lang_current = $params['object']->id;
        
        $obj = new PtsPagebuilderprofile();
        $profiles = $obj->getList();
		$fprofiles = $obj->getListByFooter();
        
        $profiles = array_merge($profiles, $fprofiles);
		
        if($profiles){
            foreach ($profiles as &$profile) {
                $ws  = unserialize( PtsPagebuilderHelper::clearUnexpected(trim($profile['widget'])) );
                if($ws){
                    foreach ($ws as &$config) {
                        $configs = unserialize(PtsPagebuilderHelper::clearUnexpected(trim($config['config'])));
                        if(isset($configs['widget']) && $configs['widget']){
                            foreach ($configs['widget'] as $k => $p) {
                                $arrs = explode('_', $k);
                                $end = end($arrs);
                                $new = array_pop($arrs);
                                if($end == $id_lang_default){
                                    $configs['widget'][implode('_', $arrs).'_'.$id_lang_current] = $p;
                                }
                            }
                        }
                        $config['config'] = serialize($configs);
                    }
                    $profile['widget'] = serialize($ws);
                }
                // save data here
                $newObj = new PtsPagebuilderprofile($profile['id_pagebuilderprofile']);
                $newObj->widget = $profile['widget'];
                $newObj->update();
                
            }
        }
    }

    public function dupplicatForAllLanguages(){
        $id_lang_default = Configuration::get('PS_LANG_DEFAULT');
        $languages = Language::getLanguages(false);

        $obj = new PtsPagebuilderprofile();
        $profiles = $obj->getList();
        $fprofiles = $obj->getListByFooter();
        
        $profiles = array_merge($profiles, $fprofiles);
		
        if($profiles){
            foreach ($profiles as &$profile) {
                $ws  = unserialize( PtsPagebuilderHelper::clearUnexpected(trim($profile['widget'])) );
                if($ws){
                    foreach ($ws as &$config) {
                        $configs = unserialize(PtsPagebuilderHelper::clearUnexpected(trim($config['config'])));
                        if(isset($configs['widget']) && $configs['widget']){
                            foreach ($configs['widget'] as $k => $p) {
                                $arrs = explode('_', $k);
                                $end = end($arrs);
                                $new = array_pop($arrs);
                                if($end == $id_lang_default){
                                    foreach ($languages as $language) {
                                        if($language['id_lang'] != $id_lang_default){
                                            $configs['widget'][implode('_', $arrs).'_'.$language['id_lang']] = $p;
                                        }
                                    }
                                }
                            }
                        }
                        $config['config'] = serialize($configs);
                    }
                    $profile['widget'] = serialize($ws);
                }
                // save data here
                $newObj = new PtsPagebuilderprofile($profile['id_pagebuilderprofile']);
                $newObj->widget = $profile['widget'];
                $newObj->update();
            }
        }
    }
    /**
     *
     */
    public function getThemeConfig( $key, $value='' ){
        return Configuration::get( 'PTS_CP_'.Tools::strtoupper(trim($key)), $value );
    }

    public function getCustomConfig( $name, $value ){
        $kc = $this->themeName . "_" . $name; 
        if( $this->context->cookie->__isset($kc) ){  
            return $this->context->cookie->__get($kc);
        }
        return $value; 
    }

    /**
     *
     */
    public function _processHook($params, $isfootter=false) {
        $cgtheme  = _PS_MODE_DEMO_ ? $this->getCustomConfig( "themeskin", $this->getThemeConfig('theme') ) :  $this->getThemeConfig('theme'); 
        if( Tools::isSubmit('applyCustomSkinChanger')  ){
            $cgtheme =   Tools::getValue('themeskin');
     
        }
        $pbkey = $isfootter?  $this->getThemeConfig( $cgtheme.'fbuilder' ) : $this->getThemeConfig( $cgtheme.'pbuilder' );
       
        if (!$this->isCached('ptspagebuilder.tpl', $this->getCacheId(null, null, $pbkey))) {
            
            $id =   $pbkey? PtsPagebuilderprofile::getIdByKey($pbkey) : PtsPagebuilderprofile::getDefaultProfile();
            $profile = new PtsPagebuilderprofile( $id , $this->context->language->id );


            $layout = $profile->getLayout();
      
            if( !empty($layout) ){
                $profile->loadWidgets();
                $ws = $profile->getWidgets(  $this->context->controller );
                $layout = $this->buildLayoutData( $layout, $ws, $profile, 1 );
            }
            
            $dir = _PS_MODULE_DIR_.'ptspagebuilder/builderlayout.tpl';
            $tdir = _PS_ALL_THEMES_DIR_ . _THEME_NAME_ . '/modules/ptspagebuilder/builderlayout.tpl';
            if (file_exists($tdir)) {
                $dir = $tdir;
            }
            $this->smarty->assign(array(
                'layout'  => $layout,
                'layout_tpl' => $dir,
                'prefix' => $isfootter?'footerbuilder':'pagebuilder'
            ));
        }    
        return $this->display(__FILE__, 'ptspagebuilder.tpl', $this->getCacheId(null, null, $pbkey) );
    }

    /**
     *
     *
     */
    public function buildLayoutData( $rows , $ws, $profile, $rl=1 ){ 
        $layout = array();
    
        foreach( $rows as $rkey =>  $row ){
            $row->level=$rl;

            $row = PtsPagebuilderHelper::mergeRowData( $row );
 
            foreach( $row->cols as $ckey => $col ){
                $col = PtsPagebuilderHelper::mergeColData( $col );
                foreach( $col->widgets as  $wkey => $w ){
                    $w->wkey = (string)$w->wkey; 
                    if( $w && isset($w->wkey) && isset($ws[$w->wkey]) ){   
                        $content = trim($ws[$w->wkey]['config']);
                        if( $content ) {
                            $widget =  unserialize(  PtsPagebuilderHelper::clearUnexpected($content) );
							if(isset($widget['widget'])){
								
								foreach( $widget['widget'] as $k => $v ){
									$widget['widget'][$k] = base64_decode( $v );
								}
					  
								if( isset($widget['widget']['wtype']) ){ 
									$type = $widget['widget']['wtype'];
									$profile->loadWidgetObject( $type,  $this->context->controller );
									$tmp = $profile->getWidgetContent( $type, $widget['widget'] );
									$col->widgets[$wkey]->content =  $this->getWidgetContent( $w->wkey, $tmp['type'], $tmp['data'] );   
								}  
							}
                        }
                    } 
                }
                if( isset($col->rows) ){
                    $col->rows = $this->buildLayoutData( $col->rows , $ws, $profile,$rl+1 );     
                }
                $row->cols[$ckey] = $col;
            }
   
            $layout[$rkey] = $row;
        }

        
        return $layout;
    }
    /**
     *
     */
    public function getWidgetContent( $id, $type, $data ){
  
        $data['id_lang'] =   $this->context->language->id;
        if($data)
            foreach ($data as $key => $value) {
                if(is_string($value) && $key != 'product_tpl' && $key != 'branche_tpl_path' && $key != 'THEME_SKIN_DIR'){
                    $data[$key] = stripslashes($value);
                }
            }
        $this->smarty->assign( $data );  
        $output = '<div class="pts-widget" id="wid-'.$id.'">';
            $output .= $this->display(__FILE__, 'views/templates/front/widgets/widget_'.$type.'.tpl' );
        $output .= '</div>';

        return $output;
    }
    
    protected function getCacheId($name = null, $hook = '', $key = '') {
        $cache_array = array(
            $name !== null ? $name : $this->name,
            $hook,
            $key,
            date('Ymd'),
            (int) Tools::usingSecureMode(),
            (int) $this->context->shop->id,
            (int) Group::getCurrent()->id,
            (int) $this->context->language->id,
            (int) $this->context->currency->id,
            (int) $this->context->country->id
        );

        return implode('|', $cache_array);
    }

    public function _clearBLHLCache() {
        $this->_clearCache('builderlayout.tpl');
        $this->_clearCache('ptspagebuilder.tpl');
    }
    public function hookCategoryAddition($params)
	{
		$this->_clearBLHLCache();
	}

	public function hookCategoryUpdate($params)
	{
		$this->_clearBLHLCache();
	}

	public function hookCategoryDeletion($params)
	{
		$this->_clearBLHLCache();
	}

	public function hookActionAdminMetaControllerUpdate_optionsBefore($params)
	{
		$this->_clearBLHLCache();
	}
	
    public function hookAddProduct($params) {
        $this->_clearBLHLCache();
    }

    public function hookUpdateProduct($params) {
        $this->_clearBLHLCache();
    }

    public function hookDeleteProduct($params) {
        $this->_clearBLHLCache();
    }

}