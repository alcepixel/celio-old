{*
* Pts Prestashop Theme Framework for Prestashop 1.6.x
*
* @package   ptspagebuilder
* @version   5.0
* @author    http://www.prestabrain.com
* @copyright Copyright (C) October 2013 prestabrain.com <@emai:prestabrain@gmail.com>
*               <info@prestabrain.com>.All rights reserved.
* @license   GNU General Public License version 2
*}
{extends file="helpers/form/form.tpl"}

{block name="field"}
    {if $input.type == 'category_tab'}
        <div class="form-group">
            <div class="col-lg-9 col-lg-offset-3">
                {$input.category_tpl}
                <div style="clear:both;"></div>
                <div id="category_tab_image">
                    {if $fields_value['categorytab']}
                        {foreach $fields_value['categorytab'] as $key => $val}
                            
                            <div id="categorytab_{$key}">
                                <label>Set icon for: <strong>{$key}</strong></label>

                                <div class="pts-container" style="padding:10px; border: 1px solid #e6e6e6;">

                                    <label>Icon Class</label>
                                    <input type="text" name="categorytab[{$key}][icon_class]" class="icon_class_category" value="{$val['icon_class']}">

                                    <label>Icon</label>
                                    <input type="text" name="categorytab[{$key}][icon]" class="icon_category" value="{$val['icon']}">
                                </div>
                                <br><br>
                            </div>

                            <script type="text/javascript">
                                $( '#categorytab_{$key} .icon_category' ).WPO_Gallery();
                            </script>
                        {/foreach}
                    {/if}
                </div>
            </div>
        </div>
    {/if}
    {if $input.type == 'promotions'}
        <div class="form-group">
            <div class="col-lg-9 col-lg-offset-3">
                <div style="clear:both;"></div>
                <div id="promotions_image_mng">
                    {if $fields_value['promotions']}
                        {foreach $fields_value['promotions'] as $key => $val}
                            <div id="promotions_{$key}" class="promotion_item">
                                <div class="pts-container" style="padding:10px; border: 1px solid #e6e6e6;">
                                    <label>Title</label>
                                    <input type="text" name="promotions[{$key}][title]" class="title_promotion" value="{$val['title']}">

                                    <label>Description</label>
                                    <textarea class="rte_promotion rte_promotion_{$key}" name="promotions[{$key}][description]">{$val['description']}</textarea>

                                    <label>Url</label>
                                    <input type="text" name="promotions[{$key}][url]" class="url_promotion" value="{$val['url']}">

                                    <label>Image</label>
                                    <input type="text" name="promotions[{$key}][image]" class="image_promotion" value="{$val['image']}">

                                </div>
                                <br><br>
                            </div>
                        {/foreach}
                    {else}
                        <div id="promotions_0" class="promotion_item">
                            <div class="pts-container" style="padding:10px; border: 1px solid #e6e6e6;">
                                
                                <label>Title</label>
                                <input type="text" name="promotions[0][title]" class="title_promotion" value="">

                                <label>Description (Raw HTML)</label>
                                <textarea class="rte_promotion rte_promotion_0" name="promotions[0][description]">{$val['description']}</textarea>

                                <label>Url</label>
                                <input type="text" name="promotions[0][url]" class="url_promotion" value="">

                                <label>Image</label>
                                <input type="text" name="promotions[0][image]" class="image_promotion" value="{$val['image']}">
                            </div>
                            <br><br>
                        </div>
                    {/if}
                    

                    <script type="text/javascript">
                        $( '.image_promotion' ).WPO_Gallery();
                    </script>
                </div>
                <button class="btn btn-warning removePromotion" type="button">Delete</button>
                <button class="btn btn-primary addNewPromotion showinform" type="button" style="display: inline-block;">Add New Promotion</button>
            </div>
        </div>

    {/if}
    {$smarty.block.parent}
{/block}