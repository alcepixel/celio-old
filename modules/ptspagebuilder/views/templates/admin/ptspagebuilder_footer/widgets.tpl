{*
* Pts Prestashop Theme Framework for Prestashop 1.6.x
*
* @package   ptspagebuilder
* @version   5.0
* @author    http://www.prestabrain.com
* @copyright Copyright (C) October 2013 prestabrain.com <@emai:prestabrain@gmail.com>
*               <info@prestabrain.com>.All rights reserved.
* @license   GNU General Public License version 2
*}
<div class="wpo-widgets clearfix">
	
	<div class="widgets-filter">
		<div class="form-group has-success clearfix">
			<label class="col-lg-2 control-label" for="searchwidgets">{l s='Filter By Name' mod='ptspagebuilder'}</label>
			<div class="col-lg-10">
				<input type="text" id="searchwidgets"  name="searchwidgets">
			</div>
		</div>

		<div class="form-group clearfix">
			<label class="col-lg-2 control-label" for="searchwidgets">{l s='Filter By Group' mod='ptspagebuilder'}</label>
			<div class="col-lg-10" id="filterbygroups">
				 <ul class="nav nav-pills">
				 	<li class="filter-option" data-option="all">
						<a href="#">{l s='All' mod='ptspagebuilder'}<span class="badge"></span></a>
					</li>

				 	{foreach from=$groups item=group name=group}
					<li class="filter-option" data-option="{$group.key}">
						<a href="#">{$group.group}<span class="badge"></span></a>
					</li>
			 		{/foreach}
				</ul>
	      </div>  
		</div>
 	</div>

	<ul>
		{foreach from=$widgets item=widget name=widget}
		<li class="wpo-wg-button" data-group="{$widget.group}"> 
			 {$widget.button}
		</li>
		{/foreach}
 	</ul>
 	<div class="clearfix"></div>
</div>

 