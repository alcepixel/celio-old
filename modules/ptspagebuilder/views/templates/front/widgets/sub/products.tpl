{*
* Pts Prestashop Theme Framework for Prestashop 1.6.x
*
* @package   ptspagebuilder
* @version   5.0
* @author    http://www.prestabrain.com
* @copyright Copyright (C) October 2013 prestabrain.com <@emai:prestabrain@gmail.com>
*               <info@prestabrain.com>.All rights reserved.
* @license   GNU General Public License version 2
*}
{if !empty($products)}
	{*define numbers of product per line in other page for desktop*}
	{if $page_name !='index' && $page_name !='product'}
		{assign var='nbItemsPerLineTablet' value=2}
		{assign var='nbItemsPerLineMobile' value=3}
	{else}
		{assign var='nbItemsPerLineTablet' value=3}
		{assign var='nbItemsPerLineMobile' value=2}
	{/if}
	{assign var='nbItemsPerLine' value=12/$scolumn}
	{*define numbers of product per line in other page for tablet*}
	{assign var='nbLi' value=$products|@count}
	{math equation="nbLi/nbItemsPerLine" nbLi=$nbLi nbItemsPerLine=$nbItemsPerLine assign=nbLines}
	{math equation="nbLi/nbItemsPerLineTablet" nbLi=$nbLi nbItemsPerLineTablet=$nbItemsPerLineTablet assign=nbLinesTablet}
<div class="pts-carousel slide" id="{$tabname}">
	{if count($products)>$itemsperpage}	 
		<div class="carousel-controls">
		 	<a class="carousel-control left" href="#{$tabname}"   data-slide="prev">&lsaquo;</a>
			<a class="carousel-control right" href="#{$tabname}"  data-slide="next">&rsaquo;</a>
		</div>
	{/if}
	<div class="carousel-inner">
	{$mproducts=array_chunk($products,$itemsperpage)}
	{foreach from=$mproducts item=products name=mypLoop}
			<!-- Products list -->
			<ul{if isset($id) && $id} id="{$id}"{/if} class="product_list products-block grid  row{if isset($class) && $class} {$class}{/if}{if isset($active) && $active == 1} active{/if} item {if $smarty.foreach.mypLoop.first}active{/if}">
			{foreach from=$products item=product name=products}
				{math equation="(total%perLine)" total=$smarty.foreach.products.total perLine=$nbItemsPerLine assign=totModulo}
				{math equation="(total%perLineT)" total=$smarty.foreach.products.total perLineT=$nbItemsPerLineTablet assign=totModuloTablet}
				{math equation="(total%perLineT)" total=$smarty.foreach.products.total perLineT=$nbItemsPerLineMobile assign=totModuloMobile}
				{if $totModulo == 0}{assign var='totModulo' value=$nbItemsPerLine}{/if}
				{if $totModuloTablet == 0}{assign var='totModuloTablet' value=$nbItemsPerLineTablet}{/if}
				{if $totModuloMobile == 0}{assign var='totModuloMobile' value=$nbItemsPerLineMobile}{/if}
				<li class="ajax_block_product col-xs-12 col-sm-4 col-md-{$scolumn} {if $smarty.foreach.products.iteration%$nbItemsPerLine == 0} last-in-line{elseif $smarty.foreach.products.iteration%$nbItemsPerLine == 1} first-in-line{/if}{if $smarty.foreach.products.iteration > ($smarty.foreach.products.total - $totModulo)} last-line{/if}{if $smarty.foreach.products.iteration%$nbItemsPerLineTablet == 0} last-item-of-tablet-line{elseif $smarty.foreach.products.iteration%$nbItemsPerLineTablet == 1} first-item-of-tablet-line{/if}{if $smarty.foreach.products.iteration%$nbItemsPerLineMobile == 0} last-item-of-mobile-line{elseif $smarty.foreach.products.iteration%$nbItemsPerLineMobile == 1} first-item-of-mobile-line{/if}{if $smarty.foreach.products.iteration > ($smarty.foreach.products.total - $totModuloMobile)} last-mobile-line{/if}">
					 	
<div class="product-block" itemscope="" itemtype="http://schema.org/Product"><div class="product-container">
			<div class="product-image-container image">
						<a class="img product_img_link"	href="{$product.link|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" itemprop="url">
							<img class="replace-2x img-responsive" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html':'UTF-8'}" alt="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}" title="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}" {if isset($homeSize)} width="{$homeSize.width}" height="{$homeSize.height}"{/if} itemprop="image" />
						</a>
						{if isset($product.new) && $product.new == 1}
							<span class="product-label new-box">
								<span class="new-label">{l s='New' mod='ptspagebuilder'}</span>
							</span>
						{/if}
						{if isset($product.on_sale) && $product.on_sale && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}
							<span class="product-label  sale-box">
								<span class="sale-label">{l s='Sale!' mod='ptspagebuilder'}</span>
							</span>
						{/if}
					


				<div class="right">
					<div class="action">
					

						<div class="product-zoom">
							<a class="btn-tooltip pts-fancybox btn btn-highlighted cboxElement" href="{$link->getImageLink($product.link_rewrite, $product.id_image, 'large_default')|escape:'html':'UTF-8'}" rel="nofollow" data-toggle="tooltip" title="{l s='zoom' mod='ptspagebuilder'}">
								<i class="icon-search-plus"></i>
								<span>{l s='zoom' mod='ptspagebuilder'}</span>
							</a>
						</div>	
						
						{hook h='displayProductListFunctionalButtons' product=$product}
						
						{if isset($comparator_max_item) && $comparator_max_item}
							<div class="compare"><a class="btn-tooltip btn btn-highlighted add_to_compare" href="{$product.link|escape:'html':'UTF-8'}" data-id-product="{$product.id_product}" data-toggle="tooltip" title="{l s='Compare' mod='ptspagebuilder'}">
								<i class="icon icon-retweet"></i><span>{l s='Compare' mod='ptspagebuilder'}</span></a>
							</div>
						{/if}
					</div>
				</div>
				</div>
				<div class="product-meta">		
					<div class="left">
							<h3 class="name" itemprop="name">
								{if isset($product.pack_quantity) && $product.pack_quantity}{$product.pack_quantity|intval|cat:' x '}{/if}
								<a href="{$product.link|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" itemprop="url" >
									{$product.name|truncate:45:'...'|escape:'html':'UTF-8'}
								</a>
							</h3>
							<div class="product-desc description" itemprop="description">
								{$product.description_short|strip_tags:'UTF-8'|truncate:200:'...'}
							</div>

							{hook h='displayProductListReviews' product=$product}


							{if isset($product.color_list)}
								<div class="color-list-container product-colors">{$product.color_list} </div>
							{/if}
		
					
							<div class="product-flags">
								{if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
									{if isset($product.online_only) && $product.online_only}
										<span class="online_only">{l s='Online only' mod='ptspagebuilder'}</span>
									{/if}
								{/if}
								{if isset($product.on_sale) && $product.on_sale && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}
									{elseif isset($product.reduction) && $product.reduction && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}
										<span class="discount">{l s='Reduced price!' mod='ptspagebuilder'}</span>
									{/if}
							</div>
							{if (!$PS_CATALOG_MODE && $PS_STOCK_MANAGEMENT && ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
								{if isset($product.available_for_order) && $product.available_for_order && !isset($restricted_country_mode)}
									<span itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="availability">
										{if ($product.allow_oosp || $product.quantity > 0)}
											<span class="{if $product.quantity <= 0}out-of-stock{else}available-now{/if}">
												<link itemprop="availability" href="http://schema.org/InStock" />
												{if $product.quantity <= 0}
													{if $product.allow_oosp}
														{$product.available_later}
													{else}
														{l s='Out of stock' mod='ptspagebuilder'}
													{/if}
												{else}
												{if isset($product.available_now) && $product.available_now}{$product.available_now}{else}{l s='In Stock' mod='ptspagebuilder'}{/if}{/if}
											</span>
										{elseif (isset($product.quantity_all_versions) && $product.quantity_all_versions > 0)}
											<span class="available-dif">
												<link itemprop="availability" href="http://schema.org/LimitedAvailability" />{l s='Product available with different options' mod='ptspagebuilder'}
											</span>
										{else}
											<span class="out-of-stock">
												<link itemprop="availability" href="http://schema.org/OutOfStock" />{l s='Out of stock' mod='ptspagebuilder'}
											</span>
										{/if}
									</span>
								{/if}
							{/if}
						

							<div class="bottom">					
								<div class="wrap-hover">
									{if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
									<div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="content_price price">

										{if isset($product.show_price) && $product.show_price && !isset($restricted_country_mode)}
											{if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
												<span class="old-price">
													{displayWtPrice p=$product.price_without_reduction}
												</span>
												
											{/if}
											<span itemprop="price" class="product-price">
												{if !$priceDisplay}{convertPrice price=$product.price}{else}{convertPrice price=$product.price_tax_exc}{/if}
											</span>
											<meta itemprop="priceCurrency" content="{$priceDisplay}" />
											{if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
											
												{if $product.specific_prices.reduction_type == 'percentage'}
												<span class="content_price_percent sale-percent-box" itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
													<span class="price-percent-reduction">-{$product.specific_prices.reduction * 100|string_format:"%d"}%</span>
												</span>
												{/if}
											{/if}
										{/if}
									</div>
									{/if}						
								</div>

							<div class="action-btn">
								{if ($product.id_product_attribute == 0 || (isset($add_prod_display) && ($add_prod_display == 1))) && $product.available_for_order && !isset($restricted_country_mode) && $product.minimal_quantity <= 1 && $product.customizable != 2 && !$PS_CATALOG_MODE}
									{if ($product.allow_oosp || $product.quantity > 0)}
										{if isset($static_token)}
											<div class="addtocart cart"><a data-toggle="tooltip"  class="btn btn-shopping-cart btn-outline ajax_add_to_cart_button" href="{$link->getPageLink('cart',false, NULL, "add=1&amp;id_product={$product.id_product|intval}&amp;token={$static_token}", false)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Add to cart' mod='ptspagebuilder'}" data-id-product="{$product.id_product|intval}">
												<i class="icon-shopping-cart"></i>
												<em>{l s='Add to cart' mod='ptspagebuilder'}</em>
											</a></div>
										{else}
											<div class="addtocart cart"><a data-toggle="tooltip"  class="btn btn-shopping-cart btn-outline ajax_add_to_cart_button" href="{$link->getPageLink('cart',false, NULL, 'add=1&amp;id_product={$product.id_product|intval}', false)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Add to cart' mod='ptspagebuilder'}" data-id-product="{$product.id_product|intval}">
												<i class="icon-shopping-cart"></i>
												<span>{l s='Add to cart' mod='ptspagebuilder'}</span>
											</a></div>
										{/if}						
									{else}
										<div class="addtocart cart"><span data-toggle="tooltip"  class="btn btn-shopping-cart btn-outline ajax_add_to_cart_button disabled">
											<i class="icon-shopping-cart"></i>
											<span>{l s='Add to cart' mod='ptspagebuilder'}</span>
										</span></div>
									{/if}
								{/if}


								{if isset($quick_view) && $quick_view}
									<div class="quickview"><a class="quick-view pts-colorbox btn btn-outline cboxElement" href="{$product.link|escape:'html':'UTF-8'}" rel="{$product.link|escape:'html':'UTF-8'}">
										<i class="icon-eye"></i>
										<span>{l s='Quick view' mod='ptspagebuilder'}</span>
									</a></div>
								{/if}	
							</div>
										
							</div>						
						
						
					</div>
				</div>
				
			

				
		</div><!-- .product-container> --></div>
				</li>
			{/foreach}
			</ul>	
	{/foreach}
	</div>
</div>
{/if}