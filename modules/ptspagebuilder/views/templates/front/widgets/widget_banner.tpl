{*
* Pts Prestashop Theme Framework for Prestashop 1.6.x
*
* @package   ptspagebuilder
* @version   5.0
* @author    http://www.prestabrain.com
* @copyright Copyright (C) October 2013 prestabrain.com <@emai:prestabrain@gmail.com>
*               <info@prestabrain.com>.All rights reserved.
* @license   GNU General Public License version 2
*}
<div class="widget-images block {if isset($stylecls)&&$stylecls}block-{$stylecls}{/if}">

	{if $subtitle}
		<small>{$subtitle}</small>
	{/if}
	{if isset($widget_heading)&&!empty($widget_heading)}
	<h4 class="title_block">
		{$widget_heading}
	</h4>
	{/if}
	<div class="widget-inner block_content clearfix">
		<div class="interactive-banner {$addition_cls} interactive-banner-v1">
		    <div class="interactive-banner-body">
		        <a href="{$imageurl}" class="pts-popup fancybox" title="{$widget_heading}">
					<img src="{$imageurl}" alt="{$widget_heading}">
				</a>
		        <div class="interactive-banner-profile text-center">
					{if $description}
						{$description}
					{/if}
		        </div>
		        <a class="mask-link" href="{$link_url}"></a>
		    </div>
		</div>
	</div>



</div>

