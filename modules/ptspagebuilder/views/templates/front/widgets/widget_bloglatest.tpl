{*
* Pts Prestashop Theme Framework for Prestashop 1.6.x
*
* @package   ptspagebuilder
* @version   5.0
* @author    http://www.prestabrain.com
* @copyright Copyright (C) October 2013 prestabrain.com <@emai:prestabrain@gmail.com>
*               <info@prestabrain.com>.All rights reserved.
* @license   GNU General Public License version 2
*}

<!-- MODULE Block ptsblockblogstabs --> 

<div id="ptsblockblogstabs{$tabname}" class="widget-latestblog block {$addition_cls} {if isset($stylecls)&&$stylecls}block-{$stylecls}{/if}">
	<h4 class="title_block">
		<span class="blog_title">{l s='Latest Blogs' mod='ptspagebuilder'}</span>
		{if $config->get('blockpts_blogs_show',1)}
			<a class="pull-right" href="{$view_all_link}" title="{l s='View all' mod='ptspagebuilder'}"></a>
		{/if}
	</h4>
	<div class="block_content">	
		{if !empty($blogs )}
	 
			{if !empty($blogs)}
		<div class="slide" id="{$tabname}">
			 {if count($blogs)>$itemsperpage}
			 <div class="carousel-controls">	 
			 	<a class="carousel-control left" href="#{$tabname}"   data-slide="prev">&lsaquo;</a>
				<a class="carousel-control right" href="#{$tabname}"  data-slide="next">&rsaquo;</a>
			</div>
			{/if}
			<div class="carousel-inner">
			{$mblogs=array_chunk($blogs,$itemsperpage)}
			{foreach from=$mblogs item=blogs name=mypLoop}
				<div class="item {if $smarty.foreach.mypLoop.first}active{/if}">
						{foreach from=$blogs item=blog name=blogs}
						{if $blog@iteration%$columnspage==1&&$columnspage>1}
						  <div class="row">
						{/if}
						<div class="col-xs-12 col-sm-6 col-md-{$scolumn} col-lg-{$scolumn} blog_block ajax_block_blog {if $smarty.foreach.blogs.first}first_item{elseif $smarty.foreach.blogs.last}last_item{/if}">
							{if $list_mode == 'grid'}
								<div class="blog_container clearfix">
	
									{if $blog.image && $show_image}
									<div class="blog-image">
										<a href="{$blog.link}" title="{$blog.title|escape:'html':'UTF-8'}" class="link">
											<img src="{$blog.preview_url}" title="{$blog.title}" class="img-responsive"/>
										</a>
									</div>
									{/if}

									<div class="blog-meta">
										{if $show_title}
											<h5>
												<a href="{$blog.link}" title="{$blog.title}">{$blog.title}</a>
											</h5>
										{/if}
										{if $show_category}
											<span class="blog-cat"> <span class="icon-list">{l s='In' mod='ptspagebuilder'}</span> 
												<a href="{$blog.category_link}" title="{$blog.category_title|escape:'html':'UTF-8'}">{$blog.category_title}</a>
											</span>
										{/if}
										{if $show_dateadd}
											<span class="blog-created"><span class=""></span>
												{strtotime($blog.date_add)|date_format:"%A, %B %e, %Y"}
											</span>
										{/if}
										{if $show_comment}<span class="blog-ctncomment">
											<span class="icon-comment"> {l s='Comment' mod='ptspagebuilder'}:</span> {$blog.comment_count}</span>
										{/if}
										{if $show_description}
											<div class="blog-shortinfo">
												{$blog.description|strip_tags|truncate:80}
											</div>
										{/if}
										{if $show_readmore}
											<div class="readmore">
												<p>
													<a href="{$blog.link}" title="{$blog.title|escape:'html':'UTF-8'}" class="btn btn-default">{l s='Read more' mod='ptspagebuilder'}</a>
												</p>
											</div>
										{/if}
									</div>
								</div>
							{else}
								<div class="blog_container clearfix media">
	
									{if $blog.image && $show_image}
									<div class="blog-image pull-left">
										<a href="{$blog.link}" title="{$blog.title|escape:'html':'UTF-8'}" class="link">
											<img src="{$blog.preview_url}" title="{$blog.title}" class="img-responsive"/>
										</a>
									</div>
									{/if}

									<div class="blog-meta media-body">
										{if $show_title}
											<h5>
												<a href="{$blog.link}" title="{$blog.title}">{$blog.title}</a>
											</h5>
										{/if}
										{if $show_category}
											<span class="blog-cat"> <span class="icon-list">{l s='In' mod='ptspagebuilder'}</span> 
												<a href="{$blog.category_link}" title="{$blog.category_title|escape:'html':'UTF-8'}">{$blog.category_title}</a>
											</span>
										{/if}
										{if $show_dateadd}
											<span class="blog-created"><span class=""></span>
												{strtotime($blog.date_add)|date_format:"%A, %B %e, %Y"}
											</span>
										{/if}
										{if $show_comment}<span class="blog-ctncomment">
											<span class="icon-comment"> {l s='Comment' mod='ptspagebuilder'}:</span> {$blog.comment_count}</span>
										{/if}
										{if $show_description}
											<div class="blog-shortinfo">
												{$blog.description|strip_tags|truncate:80}
											</div>
										{/if}
										{if $show_readmore}
											<div class="readmore">
												<p>
													<a href="{$blog.link}" title="{$blog.title|escape:'html':'UTF-8'}" class="btn btn-default">{l s='Read more' mod='ptspagebuilder'}</a>
												</p>
											</div>
										{/if}
									</div>
								</div>
							{/if}
						</div>
						
						{if ($blog@iteration%$columnspage==0||$smarty.foreach.blogs.last)&&$columnspage>1}
						</div>
						{/if}
							
						{/foreach}
				</div>		
			{/foreach}
			</div>
		</div>
		{/if}
		{/if}
	</div>
	{if $config->get('blockpts_blogs_show',1)}
		<div class="link-view">
			<a class="pull-right" href="{$view_all_link}" title="{l s='View All' mod='ptspagebuilder'}">
				<span>{l s='View All' mod='ptspagebuilder'}</span>
			</a>
		</div>
	{/if}	
</div>

<!-- /MODULE Block ptsblockblogstabs -->
<script type="text/javascript">
$(document).ready(function() {
    $('{$tabname}').each(function(){
        $(this).carousel({
            pause: true,
            interval: {$interval}
        });
    });
});
</script>
 