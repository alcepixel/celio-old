{*
* Pts Prestashop Theme Framework for Prestashop 1.6.x
*
* @package   ptspagebuilder
* @version   5.0
* @author    http://www.prestabrain.com
* @copyright Copyright (C) October 2013 prestabrain.com <@emai:prestabrain@gmail.com>
*               <info@prestabrain.com>.All rights reserved.
* @license   GNU General Public License version 2
*}
{if isset($products) && !empty($products)}
<div class="widget widget-products {$addition_cls} {if isset($stylecls)&&$stylecls}block-{$stylecls}{/if} block">
	{if isset($widget_heading)&&!empty($widget_heading)}
	<h4 class="title_block">
		{$widget_heading}
	</h4>
	{/if}
	<div class="widget-inner block_content">
		{$key=rand()}
		<div class="pts-flashsale-{$key}"></div>
		{if isset($products) AND $products}
	 		{$tabname=rand()+count($products)}

			{$scolumn=floor(12/$column)}
			{include file="{$product_tpl}" products=$products scolumn=$scolumn}  
		{/if}
	</div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($){
        $(".pts-flashsale-{$key}").lofCountDown({
            TargetDate:"{$dates.month}/{$dates.day}/{$dates.year} {$dates.hour}:{$dates.minute}:{$dates.seconds}",
            DisplayFormat:"<ul><li><div class=\"countdown_num\">%%D%% </div><div>{l s='Days' mod='ptspagebuilder'}</div></li><li><div class=\"countdown_num\">%%H%% </div><div>{l s='Hours' mod='ptspagebuilder'}</div></li><li><div class=\"countdown_num\">%%M%%</div> <div>{l s='Minutes' mod='ptspagebuilder'}</div></li><li><div class=\"countdown_num\">%%S%%</div><div> {l s='Seconds' mod='ptspagebuilder'}</div></li></ul>",
            FinishMessage: ""
        });
    });
</script>
{/if}