{*
* Pts Prestashop Theme Framework for Prestashop 1.6.x
*
* @package   ptspagebuilder
* @version   5.0
* @author    http://www.prestabrain.com
* @copyright Copyright (C) October 2013 prestabrain.com <@emai:prestabrain@gmail.com>
*               <info@prestabrain.com>.All rights reserved.
* @license   GNU General Public License version 2
*}

<!-- Block Newsletter module-->
<div id="newsletter_block_footer" class="block pts-newsletter {$addition_cls} {if isset($stylecls)&&$stylecls}block-{$stylecls}{/if}">
{if $newsletter_style == 'newsletter-v1'}
    <div class="newsletter-v1 space-tb-50 space-padding-30">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="newsletter-heading">
                    <h2>Newsletter</h2>
                
                </div>
                <p>Subscribe to our newsletters and keep in touch with the latest news a couple times a month</p>
                
                <input type="email" placeholder="Enter email" class="form-control text-center">
                <button class="btn btn-default btn-block space-top-15" type="button">Subscribe</button>
            </div>
        </div>
</div>

{elseif $newsletter_style == 'newsletter-v2'}
    <div class="newsletter-v2 background-img-v3 space-50">
            <div class="row">
                <div class="col-md-4 col-lg-4">                
                    <h4 class="newsletter-label">{l s='Sign up for newsletter' mod='ptspagebuilder'}</h4>               
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="input-group">
                        <form action="{$link->getPageLink('index')|escape:'html'}" method="post">
                            <div class="input-group">
                                <input   class="form-control radius-left-5x" id="newsletter-input-footer" type="text" name="email"  placeholder="{if isset($value) && $value}{$value}{else}{l s='your e-mail' mod='ptspagebuilder'}{/if}" />
                                <input type="hidden" name="action" value="0" />
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default btn-lg radius-right-5x" name="submitNewsletter" >{l s='Subscribe' mod='ptspagebuilder'}</button>       
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div><!--/end row-->
    </div>
{elseif $newsletter_style == 'newsletter-v3'}
    <div class="newsletter-v3 bg-primary space-padding-tb-80">
            <div class="row">
                <div class="col-md-4 col-lg-4">
                    <h4 class="newsletter-label">{l s='Newsletter' mod='ptspagebuilder'}</h4>
                    {if $information}
                        <p class="text-muted">{$information}</p>
                    {/if}
                </div>
                <div class="col-md-6 col-lg-6">
                    <form action="{$link->getPageLink('index')|escape:'html'}" method="post">
                        <div class="input-group">
                            <input   class="form-control radius-6x" id="newsletter-input-footer" type="text" name="email"  placeholder="{if isset($value) && $value}{$value}{else}{l s='your e-mail' mod='ptspagebuilder'}{/if}" />
                            <input type="hidden" name="action" value="0" />
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-success btn-lg radius-6x space-left-10" name="submitNewsletter" >{l s='Subscribe' mod='ptspagebuilder'}</button>       
                            </div>
                        </div>
                    </form>

                </div>
            </div><!--/end row-->
    </div>
{elseif $newsletter_style == 'newsletter-v4'}
    <div class="newsletter-v4 newsletter-center space-50">
            <div class="newsletter-heading">
                <h2>{l s='Newsletter' mod='ptspagebuilder'}</h2>
                {if $information}
                    <p>{$information}</p>
                {/if}
            </div>
            <div class="row">
                <div class="col-md-6 col-md-offset-3 input-group">
                    <form action="{$link->getPageLink('index')|escape:'html'}" method="post">
                        <input   class="form-control radius-left-5x" id="newsletter-input-footer" type="text" name="email"  placeholder="{if isset($value) && $value}{$value}{else}{l s='your e-mail' mod='ptspagebuilder'}{/if}" />
                        <input type="hidden" name="action" value="0" />
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-success btn-lg radius-right-5x" name="submitNewsletter" >{l s='Subscribe' mod='ptspagebuilder'}</button>       
                        </div>
                    </form>
                </div>
            </div><!--/end row--> 
    </div>
{elseif $newsletter_style == 'newsletter-v5'}
    <div class="newsletter-v5 newsletter-center space-50">
        <div class="row">
                <div class="col-md-4 col-lg-4">
                    <h4 class="newsletter-label">{l s='Sign up for newsletter' mod='ptspagebuilder'}</h4>
                    {if $information}
                        <p>{$information}</p>
                    {/if}
                </div>
                <div class="col-md-6 col-lg-6">
                    <form action="{$link->getPageLink('index')|escape:'html'}" method="post">
                        <div class="input-group">
                            <input   class="form-control radius-6x" id="newsletter-input-footer" type="text" name="email"  placeholder="{if isset($value) && $value}{$value}{else}{l s='your e-mail' mod='ptspagebuilder'}{/if}" />
                            <input type="hidden" name="action" value="0" />
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-success btn-lg radius-6x space-left-10" name="submitNewsletter" >{l s='Subscribe' mod='ptspagebuilder'}</button>       
                            </div>

                        </div>
                    </form>
                </div>
            </div><!--/end row-->
    </div>
{elseif $newsletter_style == 'newsletter-v6'}
    <div class="newsletter-v6 bg-primary light-style space-50">
        <div class="row">
            <div class="col-md-3 col-md-offset-1 text-right xs-space-20">                
                <h4 class="newsletter-label">{l s='Get the Latest News' mod='ptspagebuilder'}</h4>
                {if $information}
                    <p>{$information}</p>
                {/if}
            </div>
            <div class="col-md-6">
                <form action="{$link->getPageLink('index')|escape:'html'}" method="post">
                    <div class="input-group">
                        <input class="form-control radius-left-5x" id="newsletter-input-footer" type="text" name="email"  placeholder="{if isset($value) && $value}{$value}{else}{l s='your e-mail' mod='ptspagebuilder'}{/if}" />
                        <input type="hidden" name="action" value="0" />
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-lg btn-outline-light radius-right-5x" name="submitNewsletter" >{l s='Subscribe' mod='ptspagebuilder'}</button>       
                        </div>
                    </div>
                </form>
            </div>
        </div><!--/end row-->
    </div><!--/end container-->
{elseif $newsletter_style == 'newsletter-v7'}
    <div class="newsletter-v7 newsletter-border background-img-v3 space-50">
        <div class="row">
            <div class="col-md-3 col-md-offset-1 text-right xs-space-20">
                <span class="special-label">{l s='Newsletter:' mod='ptspagebuilder'}</span>
            </div>
            <div class="col-md-6">
                <form action="{$link->getPageLink('index')|escape:'html'}" method="post">
                    <div class="input-group">
                        <input class="form-control radius-left-5x" id="newsletter-input-footer" type="text" name="email"  placeholder="{if isset($value) && $value}{$value}{else}{l s='your e-mail' mod='ptspagebuilder'}{/if}" />
                        <input type="hidden" name="action" value="0" />
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-lg btn-default radius-right-5x" name="submitNewsletter" >{l s='Subscribe' mod='ptspagebuilder'}</button>       
                        </div>
                    </div>
                </form>
            </div>
        </div><!--/end row-->
    </div><!--/end container-->
{elseif $newsletter_style == 'newsletter-v8'}
    <div class="newsletter-v8 space-50 bg-success">
            <div class="heading heading-light">
                <h2>{l s='Sign up for newsletter' mod='ptspagebuilder'}</h2>
                {if $information}
                    <span>{$information}</span>
                {/if}   
            </div>

            <form role="form">
                <form action="{$link->getPageLink('index')|escape:'html'}" method="post">
                    <div class="input-group newsletter-group">
                        <input class="newsletter-input form-control radius-6x input-lg" id="newsletter-input-footer" type="text" name="email"  placeholder="{if isset($value) && $value}{$value}{else}{l s='your e-mail' mod='ptspagebuilder'}{/if}" />
                        <input type="hidden" name="action" value="0" />
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-lg btn-primary radius-6x" name="submitNewsletter" >{l s='Subscribe' mod='ptspagebuilder'}</button>       
                        </div>
                    </div>
                </form>
            </form>
        </div>
{/if}
</div>
<!-- /Block Newsletter module-->
 


<script type="text/javascript">
    var placeholder = "{l s='your e-mail' mod='ptspagebuilder' js=1}";
    {literal}
        $(document).ready(function() {
            $('#newsletter-input-footer').on({
                focus: function() {
                    if ($(this).val() == placeholder) {
                        $(this).val('');
                    }
                },
                blur: function() {
                    if ($(this).val() == '') {
                        $(this).val(placeholder);
                    }
                }
            });

            $("#newsletter_block_footer form").submit( function(){  
                if ( $('#newsletter-input-footer').val() == placeholder) {
                    $("#newsletter_block_footer .alert").removeClass("hide");
                    return false;
                }else {
                     $("#newsletter_block_footer .alert").addClass("hide");
                     return true;
                }
            } );
        });

    {/literal}
</script>