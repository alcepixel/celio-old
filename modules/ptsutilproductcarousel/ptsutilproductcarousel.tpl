{*
* Pts Prestashop Theme Framework for Prestashop 1.6.x
*
* @package   ptsutilproductcarousel
* @version   2.0
* @author    http://www.prestabrain.com
* @copyright Copyright (C) October 2013 prestabrain.com <@emai:prestabrain@gmail.com>
*               <info@prestabrain.com>.All rights reserved.
* @license   GNU General Public License version 2
*}
<div class="block products_block ptsutilproductcarousel">
	<h3>{l s='Latest Products' mod='ptsutilproductcarousel'}</h3>
	<div class="block_content">	
		{if !empty($products)}
			{$tabname="ptsutilproductcarousel"}
			{include file="{$product_tpl}"} 
		{/if}
	</div>
</div>
<!-- /MODULE Block ptsutilproductcarousel -->
{literal}
<script type="text/javascript">
    $(document).ready(function() {
        $('#{/literal}{$tabname}'{literal}).utilCarousel({
            pagination : false,
            pause: 'hover',
            interval: {/literal}{$interval}{literal},
            autoPlay : {/literal}{if $interval==0}false{else}true{/if}{literal},
            responsiveMode : 'itemWidthRange',
            itemWidthRange : [{/literal}{$min_width}{literal}, {/literal}{$max_width}{literal}],
            navigation : true,
            navigationText : ['<i class="icon-large icon-chevron-left"></i>', '<i class="icon-large icon-chevron-right"></i>']
        });
    });
</script>
{/literal}
 