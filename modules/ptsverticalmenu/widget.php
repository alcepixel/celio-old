<?php
/**
 * Pts Prestashop Theme Framework for Prestashop 1.6.x
 *
 * @package   ptsverticalmenu
 * @version   1.4
 * @author    http://www.prestabrain.com
 * @copyright Copyright (C) October 2013 prestabrain.com <@emai:prestabrain@gmail.com>
 *               <info@prestabrain.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */

include_once('../../config/config.inc.php');
include_once('../../init.php');
include_once(dirname(__FILE__).'/ptsverticalmenu.php');
global $cookie;

if( Tools::getValue('showlist') ){
	$context = Context::getContext();
	$module = new PtsVerticalMenu();

	$model = $module->widget;
    $widgets = $model->getWidgets();
    $model->loadEngines();

?>
	 <option value=""><?php echo $module->l(''); ?></option>
	 <?php foreach( $widgets as $w ) { ?>
	 <?php 
	 	$more = '';
    	if( $info = $model->getWidgetInfo( $w['type'] ) ) {
    		$more  = '( '.$info['label'].' )';
 
    	}

	 ?>
	 	<option value="<?php echo $w['key_widget']; ?>"><?php echo $more . '  ' . $w['name']; ?></option>
	 <?php } ?>
<?php }else {


	$context = Context::getContext();
	$module = new PtsVerticalMenu();
	echo $module->renderWidgetButton();
}
die;
