<?php
include_once('../../config/config.inc.php');
include_once('../../init.php');

if(!Tools::getValue('secure_key') || Tools::getValue('secure_key') !== Tools::encrypt('smartshortcode')) die('wrong action.');

$iframedir = _PS_MODULE_DIR_.'/smartshortcode/plugins/shortcode/iframes';

$action = Tools::getValue('smartShortcodeAction');

//work here
switch($action){
    case 'font_awesome':
        include $iframedir.'/font_awesome.php';
        break;
    case 'button':
        include $iframedir.'/button.php';
        break;
    case 'alert_box':
        include $iframedir.'/notification.php';
        break;
    case 'blockquote':
        include $iframedir.'/blockquote.php';
        break;
    case 'testimonial':
        include $iframedir.'/testimonial.php';
        break;
    case 'testimonial_slider':
        include $iframedir.'/testimonial_slider.php';
        break;
    case 'accordion':
        include $iframedir.'/accordion.php';
        break;
    case 'spoiler':
        include $iframedir.'/spoiler.php';
        break;
    case 'tabs':
        include $iframedir.'/tabs.php';
        break;
    case 'featured_products':
        include $iframedir.'/featured_products.php';
        break;
    case 'bestsellers_products':
        include $iframedir.'/bestsellers_products.php';
        break;
    case 'new_products':
        include $iframedir.'/new_products.php';
        break;
    case 'specials_products':
        include $iframedir.'/specials_products.php';
        break;
    case 'category_products':
        include $iframedir.'/category_products.php';
        break;
    case 'column':
        include $iframedir.'/column.php';
        break;
    case 'sds_video':
        include $iframedir.'/sds_video.php';
        break;
    case 'sds_slider':
        include $iframedir.'/sds_slider.php';
        break;
    case 'sds_gallery':
        include $iframedir.'/sds_gallery.php';
        break;
    case 'sds_image':
        include $iframedir.'/sds_image.php';
        break;
    case 'gmap':
        include $iframedir.'/gmap.php';
        break;    
    case 'social':
        include $iframedir.'/social.php';
        break;
    case 'rev_slider':
        include $iframedir.'/rev_slider.php';
        break;
    
    case 'sds_features':
        include $iframedir.'/sds_features.php';
        break;
    
    case 'sds_services':
        include $iframedir.'/sds_services.php';
        break;
    
    case 'smart_blog':
        include $iframedir.'/smart_blog.php';
        break;    
    
    case 'smartcomment':
        include $iframedir.'/smart_comment.php';
        break;
    
    default :

        break;

}


die();

?>