<?php
class smartanywherecontent extends ObjectModel
{
        public $id_smart_contentanywhere;	
        public $active = 1;
        public $id_category;
        public $id_product;
        public $hook_name;
        //lang field
	    public $title;
        public $content;
        
	public static $definition = array(
		'table' => 'smart_contentanywhere',
		'primary' => 'id_smart_contentanywhere',
        'multilang'=>true,
		'fields' => array(
                     'id_category' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
                     'id_product' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
                     'active' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
                     'hook_name' => array('type' => self::TYPE_STRING, 'validate' => 'isString','required' => true),
                        'title' => array('type' => self::TYPE_STRING, 'lang'=>true, 'validate' => 'isString','required' => true),
                        'content' => array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString','required'=>true)
		),
	);
    public function __construct($id = null, $id_lang = null, $id_shop = null)
        {
            Shop::addTableAssociation('smart_contentanywhere', array('type' => 'shop'));
                    parent::__construct($id, $id_lang, $id_shop);
        }

    public static function GetHookVale($hook_name,$id_lang = null){
        if($id_lang == null){
                    $id_lang = (int)Context::getContext()->language->id;
                }
        $sql = 'SELECT * FROM '._DB_PREFIX_.'smart_contentanywhere p INNER JOIN 
                '._DB_PREFIX_.'smart_contentanywhere_lang pl ON p.id_smart_contentanywhere=pl.id_smart_contentanywhere INNER JOIN 
                '._DB_PREFIX_.'smart_contentanywhere_shop ps ON p.id_smart_contentanywhere = ps.id_smart_contentanywhere AND ps.id_shop = '.(int) Context::getContext()->shop->id.' 
                WHERE pl.id_lang='.$id_lang.'
                AND p.hook_name= "'.$hook_name.'" AND p.active = 1';
        if (!$posts = Db::getInstance()->executeS($sql))
            return false;
        $i = 0;

            foreach($posts as $post){
                $result[$i]['id_smart_contentanywhere'] = $post['id_smart_contentanywhere'];
                $result[$i]['id_category'] = $post['id_category'];
                $result[$i]['id_product'] = $post['id_product'];
                $result[$i]['active'] = $post['active'];
                $result[$i]['hook_name'] = $post['hook_name'];
                $result[$i]['title'] = $post['title'];
                if((Module::isEnabled('smartshortcode') == 1) && (Module::isInstalled('smartshortcode') == 1)){
                require_once(_PS_MODULE_DIR_ . 'smartshortcode/smartshortcode.php');
                $smartshortcode = new SmartShortCode();
                
                $result[$i]['content'] = $smartshortcode->parse($post['content'],$hook_name);
                
                }else{
                 $result[$i]['content'] = $post['content'];
                 }
                $i++;
            }
        return $result;
    }
    public static function GetHookValeByCat($hook_name,$id_category,$id_lang = null){
        if($id_lang == null){
                    $id_lang = (int)Context::getContext()->language->id;
                }
        $sql = 'SELECT * FROM '._DB_PREFIX_.'smart_contentanywhere p INNER JOIN 
                '._DB_PREFIX_.'smart_contentanywhere_lang pl ON p.id_smart_contentanywhere=pl.id_smart_contentanywhere INNER JOIN 
                '._DB_PREFIX_.'smart_contentanywhere_shop ps ON p.id_smart_contentanywhere = ps.id_smart_contentanywhere AND ps.id_shop = '.(int) Context::getContext()->shop->id.' 
                WHERE pl.id_lang='.$id_lang.'
                AND p.hook_name= "'.$hook_name.'" AND p.active = 1 AND p.id_category = "'.$id_category.'"';
        
        if (!$posts = Db::getInstance()->executeS($sql))
            return false;
            
            $i = 0;
            $result = array();
            foreach($posts as $post){
                $result[$i] = array();
                $result[$i]['id_smart_contentanywhere'] = $post['id_smart_contentanywhere'];
                $result[$i]['id_category'] = $post['id_category'];
                $result[$i]['id_product'] = $post['id_product'];
                $result[$i]['active'] = $post['active'];
                $result[$i]['hook_name'] = $post['hook_name'];
                $result[$i]['title'] = $post['title'];
                if((Module::isEnabled('smartshortcode') == 1) && (Module::isInstalled('smartshortcode') == 1)){
                require_once(_PS_MODULE_DIR_ . 'smartshortcode/smartshortcode.php');
                $smartshortcode = new SmartShortCode();
                
                $result[$i]['content'] = $smartshortcode->parse($post['content'],$hook_name);
                }else{
                 $result[$i]['content'] = $post['content'];
                 }
                $i++;
            }
            
        return $result;
    }
    public static function GetHookValeByPrd($hook_name,$id_product,$id_lang = null){
        if($id_lang == null){
                    $id_lang = (int)Context::getContext()->language->id;
                }
        $sql = 'SELECT * FROM '._DB_PREFIX_.'smart_contentanywhere p INNER JOIN 
                '._DB_PREFIX_.'smart_contentanywhere_lang pl ON p.id_smart_contentanywhere=pl.id_smart_contentanywhere INNER JOIN 
                '._DB_PREFIX_.'smart_contentanywhere_shop ps ON p.id_smart_contentanywhere = ps.id_smart_contentanywhere AND ps.id_shop = '.(int) Context::getContext()->shop->id.' 
                WHERE pl.id_lang='.$id_lang.'
                AND p.hook_name= "'.$hook_name.'" AND p.active = 1 AND p.id_product = "'.$id_product.'"';
        if (!$posts = Db::getInstance()->executeS($sql))
            return false;
        $i = 0;

            foreach($posts as $post){
                $result[$i]['id_smart_contentanywhere'] = $post['id_smart_contentanywhere'];
                $result[$i]['id_category'] = $post['id_category'];
                $result[$i]['id_product'] = $post['id_product'];
                $result[$i]['active'] = $post['active'];
                $result[$i]['hook_name'] = $post['hook_name'];
                $result[$i]['title'] = $post['title'];
                if((Module::isEnabled('smartshortcode') == 1) && (Module::isInstalled('smartshortcode') == 1)){
                require_once(_PS_MODULE_DIR_ . 'smartshortcode/smartshortcode.php');
                $smartshortcode = SmartShortCode::getInstance();                
                $result[$i]['content'] = $smartshortcode->parse($post['content'],$hook_name);
                }else{                    
                 $result[$i]['content'] = $post['content'];
                 }
                $i++;
            }
            
        return $result;
    }
    public static function GetHookValeByNone($hook_name,$id_lang = null){
        $id_category = 'none';
        $id_product = '-1';
        
        if($id_lang == null){
                    $id_lang = (int)Context::getContext()->language->id;
                }
        $sql = 'SELECT * FROM '._DB_PREFIX_.'smart_contentanywhere p INNER JOIN 
                '._DB_PREFIX_.'smart_contentanywhere_lang pl ON p.id_smart_contentanywhere=pl.id_smart_contentanywhere INNER JOIN 
                '._DB_PREFIX_.'smart_contentanywhere_shop ps ON p.id_smart_contentanywhere = ps.id_smart_contentanywhere AND ps.id_shop = '.(int) Context::getContext()->shop->id.' 
                WHERE pl.id_lang='.$id_lang.'
                AND p.hook_name= "'.$hook_name.'" AND p.active = 1 AND p.id_product = "'.$id_product.'" AND p.id_category = "'.$id_category.'"';
        if (!$posts = Db::getInstance()->executeS($sql))
            return false;
        $i = 0;
            
            foreach($posts as $post){
                $result[$i]['id_smart_contentanywhere'] = $post['id_smart_contentanywhere'];
                $result[$i]['id_category'] = $post['id_category'];
                $result[$i]['id_product'] = $post['id_product'];
                $result[$i]['active'] = $post['active'];
                $result[$i]['hook_name'] = $post['hook_name'];
                $result[$i]['title'] = $post['title'];
                if((Module::isEnabled('smartshortcode') == 1) && (Module::isInstalled('smartshortcode') == 1)){
                require_once(_PS_MODULE_DIR_ . 'smartshortcode/smartshortcode.php');                
                $smartshortcode = new SmartShortCode();
                
                $result[$i]['content'] = $smartshortcode->parse($post['content'],$hook_name);
                }else{
                 $result[$i]['content'] = $post['content'];
                 }
                $i++;
            }
        return $result;
    }
}