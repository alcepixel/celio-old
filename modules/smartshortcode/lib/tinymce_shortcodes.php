<?php
//$token = Tools::getToken();

$secure_key = Tools::encrypt('smartshortcode');

//$ajaxurl = Context::getContext()->link->getAdminLink('Smartshortcode_ajax',false)."&token=".$token;
$ajaxurl = __PS_BASE_URI__.'modules/smartshortcode/ajax-shortcode.php';

?><!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Smart Short Code</title>
 <style type="text/css">
body{
    padding:15px;
}
.mce-container-body{
    position: relative;
}
.shortcodes_window{
    width:100%;
    position: absolute;
}
.btn {
    -moz-user-select: none;
    background-image: none;
    border-radius: 3px;
    cursor: pointer;
    display: inline-block;
    font-size: 12px;
    font-weight: normal;
    line-height: 1.42857;
    margin-bottom: 0;
    padding: 4px 8px;
    text-align: center;
    vertical-align: middle;
    white-space: nowrap;
}
#select_shortcode{
    display: block;
    position: relative;
    left: 0;    
}
#select_shortcode a{
    width: 24%;
    display: inline-block;
    color:#444;
    margin-bottom: 5px;
    border:1px solid #999;
    padding: 10px 15px;
    transition: background ease-in 0.2s;
}
#select_shortcode a:hover{
    text-decoration: none;
    background: #78A300;
    color:#fff;
}
.tabfields{
    position: relative;
}
.sds_filemanager{
    position: absolute;
    right: 15px;
    top: 36px;
    cursor: pointer;
}
#select_shortcode a i:before{
    font-size: 18px;
    vertical-align: bottom;
}
input[type=radio], input[type=checkbox] {
    margin: 4px 5px 0 !important;
    margin-top: 1px \9;
    line-height: normal;
}

	 </style>
	<script type="text/javascript" src="<?php echo _PS_JS_DIR_ .  'jquery/jquery-1.11.0.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo _PS_JS_DIR_ ; ?>tiny_mce/tiny_mce.js"></script>
    <link rel="stylesheet" href="<?php echo __PS_BASE_URI__.'modules/smartshortcode/css/bootstrap/css/bootstrap.min.css';?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo __PS_BASE_URI__.'modules/smartshortcode/plugins/fontawesome/css/font-awesome.min.css';?>" type="text/css" />
    <script type="text/javascript">
                
		$(document).ready(function() {
                        $('.shortcodes_window').css({left:-($('.shortcodes_window').width())+'px'});
                        $(document.body).on('click','.shortcodes_window a.backlink',function(){
                            //unbind event
                            $(document.body).off('click','.sds_filemanager');
                            $(document.body).off('click','#add_new');
                            $(document.body).off('click','span.awesome');
                            
                            
                            $('.shortcodes_window').animate({left:-($('.shortcodes_window').width())+'px',width:$('.shortcodes_window').width()+'px'},300,function(){
                                $(this).css({position:'absolute'});
                                $(this).html('');                            
                            });
                            $('#select_shortcode').animate({left:0,width:$('#select_shortcode').width()+'px'},300,function(){
                                $(this).css({position:'relative'});
                            });

                            
                            return false;
                        });

                        var callShortcodeMethod = function(val){
                            
                            $.ajax({                                
                                url: '<?php echo $ajaxurl?>',
                                type : 'POST',
                                data : {
                                    smartShortcodeAction : val,
                                    secure_key : '<?php echo $secure_key?>',
                                },
                                dataType : 'html',
                                success : function(resp){
                                    $('#select_shortcode').css({position:'absolute'}).animate({left:-($('#select_shortcode').width()),width:$('#select_shortcode').width()+'px'},300,function(){                                        
//                                        $(this).css({position:'absolute'});
                                    });
                                    
                                    $('.shortcodes_window').html(resp).animate({left:'0px'},300,function(){
                                        $(this).css({position:'relative'});
                                    });
                                    
                                    
                                }
                             
                            });
                            return false;
                        };

			
			$('#select_shortcode a').click(function() {

                                var val = $(this).attr('href');
                                switch(val){
                                    case 'sds_manufacturers':
                                        var scode = '[sds_manufacturers';
                                        var pval = prompt("Enter slider speed (in miliseconds)",'600');
                                        var sval = prompt("Enter number of slides",'6');
                                        if(pval == null){
                                            alert('Slide speed is required.');                                            
                                            return false;
                                        }
                                        scode += ' speed="'+pval+'"';
                                        scode += ' maxslide="'+sval+'"]';
                                        
                                        parent.tinyMCE.execCommand('mceInsertContent', false,scode);
                                        parent.tinyMCE.activeEditor.windowManager.close();
                                    break;
                                    default:
                                    return callShortcodeMethod(val);
                                }
			});

		});

		

		 
    </script>
</head>
<body>
	
	 <div id="main" class="mce-container-body">
        <div id="select_shortcode">
			
<a href="column"><i class="icon-columns"></i> Bootstrap Grid</a>                           
<a href="font_awesome"><i class="icon-flag"></i> Font Awesome</a>
<a href="social"><i class="icon-thumbs-up-alt"></i> Social icons</a>
<a href="sds_manufacturers"><i class="icon-puzzle-piece"></i> Manufacturers</a>
<a href="featured_products"><i class="icon-briefcase"></i> Featured Products</a>
<a href="bestsellers_products"><i class="icon-briefcase"></i> Bestsellers Products</a>
<a href="new_products"><i class="icon-briefcase"></i> New Products</a>
<a href="specials_products"><i class="icon-briefcase"></i> Specials Products</a>
<a href="category_products"><i class="icon-briefcase"></i> Category wise Product</a>

<a href="blockquote"><i class="icon-quote-left"></i> Block Quote</a>                
<a href="sds_features"><i class="icon-list-ul"></i> Features Element</a>                
<a href="sds_services"><i class="icon-list-ul"></i> Services</a>                

<a href="button"><i class="icon-expand"></i> Button</a>

<a href="alert_box"><i class="icon-info-sign"></i> Notification</a>               

<a href="accordion"><i class="icon-tasks"></i> Accordion</a>
<a href="spoiler"><i class="icon-tasks"></i> Spoiler</a>
<a href="tabs"><i class="icon-tasks"></i> Tabs</a>

<a href="testimonial"><i class="icon-edit"></i> Testimonial</a>
<a href="testimonial_slider"><i class="icon-edit"></i> Testimonial Slider</a>
<a href="sds_video"><i class="icon-play-circle"></i> Video</a>
<a href="sds_slider"><i class="icon-picture"></i> Image Slider</a>
<a href="sds_gallery"><i class="icon-picture"></i> Image Gallery</a>
<a href="gmap"><i class="icon-map-marker"></i> Google Map</a>
<?php 
if(Module::isEnabled('revsliderprestashop') == 1 && Module::isInstalled('revsliderprestashop') == 1){?>
<a href="rev_slider"><i class="icon-picture"></i> Revolution Slider</a>
<?php }?>
<?php if(Module::isEnabled('smartblog') == 1 && Module::isInstalled('smartblog') == 1){?>
<a href="smart_blog"><i class="icon-picture"></i> Smart Blog</a>
<?php }?>
<?php if(Module::isEnabled('smartbloglatestcomments') == 1 && Module::isInstalled('smartbloglatestcomments') == 1){?>
<a href="smartcomment"><i class="icon-picture"></i> Smart Latest Comments</a>
<?php }?>

</div>
<div class="shortcodes_window"></div>

</div>
</body>
</html>