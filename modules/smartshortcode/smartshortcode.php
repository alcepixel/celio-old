<?php
/* 
 * 2007-2014 PrestaShop
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author PrestaShop SA <contact@prestashop.com>
 *  @copyright  2007-2014 PrestaShop SA
 *  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_'))
    exit;
require_once (dirname(__FILE__) . '/classes/smartanywherecontent.php');
require_once (dirname(__FILE__) . '/classes/SmartProductTabCreator.php');
class SmartShortCode extends Module {

    public $shortcodes = array();
    public static $smartshortcode, $static_shortcode_tags = array(),$fires = 0,$sds_current_hook = '';

    public function __construct() {
        $this->name = 'smartshortcode';
        $this->tab = 'front_office_features';
        $this->version = '2.1';
        $this->author = 'SmartDataSoft';
        $this->need_instance = 0;
        
        $this->bootstrap = true;
        parent::__construct();
        
        $this->displayName = $this->l('Smart Short Code');
        $this->description = $this->l('Smart Short Code System');
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        
        if(Module::isEnabled('revsliderprestashop') == 1 &&
            Module::isInstalled('revsliderprestashop') == 1 &&
            file_exists(_PS_MODULE_DIR_.'revsliderprestashop/revprestashoploader.php')){            
            SmartShortCode::add_shortcode('rev_slider',array('RevsliderPrestashop','rev_slider_shortcode'));
        }
    }
    public function install()
    {
        $langs = Language::getLanguages();
    @copy(_PS_ROOT_DIR_."/js/tinymce.inc.js", dirname(__FILE__)."/backup/tinymce.inc.js");
    @copy(dirname(__FILE__) . "/js/lib/tinymce.inc.js",_PS_ROOT_DIR_."/js/tinymce.inc.js");
    //$this->openZip(_PS_MODULE_DIR_."smartshortcode/js/lib/js/tiny_mce/plugins/shortcode.zip");
      if(!parent::install() 
            || !$this->registerHook('displayHeader') 
            || !$this->registerHook('displayBackOfficeHeader')
            || !$this->registerHook('displayBanner')
            || !$this->registerHook('displayFooter')
            || !$this->registerHook('displayFooterProduct')
            || !$this->registerHook('displayHome')
            || !$this->registerHook('displayHomeTab')
            || !$this->registerHook('displayHomeTabContent')
            || !$this->registerHook('displayLeftColumn')
            || !$this->registerHook('displayLeftColumnProduct')
            || !$this->registerHook('displayMaintenance')
            || !$this->registerHook('displayMyAccountBlock')
            || !$this->registerHook('displayMyAccountBlockfooter')
            || !$this->registerHook('displayNav')
            || !$this->registerHook('displayProductTab')
            || !$this->registerHook('displayProductTabContent')
            || !$this->registerHook('displayRightColumn')
            || !$this->registerHook('displayRightColumnProduct')
            || !$this->registerHook('displayTop')
            || !$this->registerHook('displayTopColumn')
            || !$this->moduleControllerRegistration()
        )
            return false;
        /* Start Install Database*/
         $sql = array();
        require_once(dirname(__FILE__) . '/sql/install.php');
        foreach ($sql as $sq) :
            if (!Db::getInstance()->Execute($sq))
                return false;
        endforeach;
        /* End Install Database*/
         $this->CreateShortCodeTabs();
        Configuration::updateGlobalValue('smart_load_font','module');
        Configuration::updateGlobalValue('smart_load_bootstrapcss','theme');
        Configuration::updateGlobalValue('smart_load_bootstrapjs','theme');
        Configuration::updateGlobalValue('smart_shortcode_tab_style','tab');
        return true;
    }
    
    public function uninstall()
    {
        $this->moduleControllerUnRegistration();
        $this->restorfile();
        if(!parent::uninstall())
            return false;
        //Start Uninstall Tab
        require_once(dirname(__FILE__) . '/sql/uninstall_tab.php');
        foreach ($idtabs as $tabid):
            if ($tabid){
                $tab = new Tab($tabid);
                $tab->delete();
            }
        endforeach;
        //End Uninstall Tab
        // Start Uninstall Blog Tables 
                $sql = array();
        require_once(dirname(__FILE__) . '/sql/uninstall.php');
        foreach ($sql as $s) :
            if (!Db::getInstance()->Execute($s))
                return false;
        endforeach;
           // End Uninstall Blog Tables
          return true;
    }
    public function l($str,$specific = false){
        
        return parent::l($str);
    }
    
    private function CreateShortCodeTabs() {
                $langs = Language::getLanguages();
                $id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
                $smarttab = new Tab();
                $smarttab->class_name = "Adminsmartshortcode";
                $smarttab->module = "";
                $smarttab->id_parent = 0;
                foreach($langs as $l){
                    $smarttab->name[$l['id_lang']] = $this->l('Shortcode');
                }
                $smarttab->save();
                $tab_id = $smarttab->id;
                require_once(dirname(__FILE__) . '/sql/install_tab.php');
                foreach ($tabvalue as $tab){
                    $newtab = new Tab();
                    $newtab->class_name = $tab['class_name'];
                    $newtab->id_parent = $tab_id;
                    $newtab->module = $tab['module'];
                    foreach($langs as $l) {
                        $newtab->name[$l['id_lang']] = $this->l($tab['name']);
                    }
                    $newtab->save();
                }
                return true;
            }
    public function executehookvalue($hook_name)
        {  
            $hookresults = smartanywherecontent::GetHookVale($hook_name);
            if(!empty($hookresults))
            {          
                $context = Context::getContext();
                foreach ($hookresults as $hr){
                    $id_category = $hr['id_category'];  
                    $id_product = $hr['id_product']; 

                    
                    if($context->controller->php_self == 'category' && $id_category != 'none')
                    {         
                        if(Tools::getvalue('id_category') == $id_category || $id_category == '2'){
                                $rs = smartanywherecontent::GetHookValeByCat($hook_name,$id_category);
                                $this->smarty->assign( array(
                                    'result' => $rs
                                ));
                            return $this->display(__FILE__, 'views/templates/front/smartshortcontent.tpl');
                        }
                    }
                    elseif($context->controller->php_self == 'product' && $id_product != '-1')
                    {
                        if(Tools::getvalue('id_product') == $id_product || $id_product == '0'){
                            $rs = smartanywherecontent::GetHookValeByPrd($hook_name,$id_product);                            
                            $this->smarty->assign( array(
                                'result'        => $rs
                            ));
                            return $this->display(__FILE__, 'views/templates/front/smartshortcontent.tpl');
                        }
                    }
                    elseif(($context->controller->php_self != 'product' && $context->controller->php_self != 'category'))
                    {                        
                        $rs = smartanywherecontent::GetHookValeByNone($hook_name);
                            $this->smarty->assign( array(
                                'result'        => $rs
                            ));
                        return $this->display(__FILE__, 'views/templates/front/smartshortcontent.tpl');
                    }
                }
            }
        }
        public static function getLatestComments($id_lang = null, $limit = 5){
            $result = array();
            if($id_lang == null){
                    $id_lang = (int)Context::getContext()->language->id;
                }
                 
                $sql = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT * FROM '._DB_PREFIX_.'smart_blog_comment bc INNER JOIN 
                '._DB_PREFIX_.'smart_blog_post_shop bps ON bc.id_post=bps.id_smart_blog_post and bps.id_shop = '.(int) Context::getContext()->shop->id.'
                WHERE  bc.active= 1 ORDER BY bc.id_smart_blog_comment DESC limit 0, '.$limit);
		
                 $i = 0;
            foreach($sql as $post){
                $result[$i]['id_smart_blog_comment'] = $post['id_smart_blog_comment'];
                $result[$i]['id_parent'] = $post['id_parent'];
                $result[$i]['id_customer'] = $post['id_customer'];
                $result[$i]['id_post'] = $post['id_post'];
                $result[$i]['name'] = $post['name'];
                $result[$i]['email'] = $post['email'];
                $result[$i]['website'] = $post['website']; 
                $result[$i]['active'] = $post['active']; 
                $result[$i]['created'] = $post['created']; 
                $result[$i]['content'] = $post['content'];
                $SmartBlogPost = new  SmartBlogPost();
                $result[$i]['slug'] = $SmartBlogPost->GetPostSlugById($post['id_post']);
                $i++;
            }
		return $result;
    }
    public function hookdisplayProductTab($params)
        {
      $results = SmartProductTabClass::getTabTitle(Tools::getValue('id_product'), (int)$this->context->language->id);
                $this->context->smarty->assign(array(
                    'sds_results' => $results
                    ));
           return $this->display(__FILE__, 'views/templates/front/smart_product_tab_creator.tpl');
        }
         
    public function hookdisplayProductTabContent($params)
    {
                $results = SmartProductTabClass::getTabTitle(Tools::getValue('id_product'), (int)$this->context->language->id);
                $this->context->smarty->assign(array(
                    'sds_results' => $results
                    ));
                return $this->display(__FILE__, 'views/templates/front/smartproducttab.tpl');
    }
    public function hookDisplayHome($params)
        {
          return  $this->executehookvalue('DisplayHome');
        }
    public function hookdisplayFooter($params)
        {
            return  $this->executehookvalue('displayFooter');
        }
    public function hookdisplayFooterProduct($params)
        {
            return  $this->executehookvalue('displayFooterProduct');
        }
    public function hookdisplayHomeTab($params)
        {
            return  $this->executehookvalue('displayHomeTab');
        }
    public function hookdisplayHomeTabContent($params)
        {
            return  $this->executehookvalue('displayHomeTabContent');
        }
    public function hookdisplayLeftColumn($params)
        {            
            return  $this->executehookvalue('displayLeftColumn');
        }
    public function hookdisplayLeftColumnProduct($params)
        {
            return  $this->executehookvalue('displayLeftColumnProduct');
        }
    public function hookdisplayMaintenance($params)
        {
            return  $this->executehookvalue('displayMaintenance');
        }
    public function hookdisplayMyAccountBlock($params)
        {
            return  $this->executehookvalue('displayMyAccountBlock');
        }
    public function hookdisplayMyAccountBlockfooter($params)
        {
            return  $this->executehookvalue('displayMyAccountBlockfooter');
        }
    public function hookdisplayNav($params)
        {
            return  $this->executehookvalue('displayNav');
        }
    public function hookdisplayBanner($params)
        {
            return  $this->executehookvalue('displayBanner');
        }
    public function hookdisplayRightColumn($params)
        {
            return  $this->executehookvalue('displayRightColumn');
        }
    public function hookdisplayRightColumnProduct($params)
        {
            return  $this->executehookvalue('displayRightColumnProduct');
        }
    public function hookdisplayTop($params)
        {
            return  $this->executehookvalue('displayTop');
        }
    public function hookdisplayTopColumn($params)
        {
            return  $this->executehookvalue('displayTopColumn');
        }
        public function generatesubCategoriesOption($categories, $items_to_skip = null)
        {
            $subcatvals = array();
            $spacer_size = '5';
             $this->element_index++;
            foreach ($categories as $key => $category)
            {
                $this->smartcat[$this->element_index]['id_category'] = $category['id_category'];
                $this->smartcat[$this->element_index]['name'] = str_repeat('&nbsp;', $spacer_size * (int)$category['level_depth']).$category['name'];
            

                if (isset($category['children']))
                      $this->generatesubCategoriesOption($category['children']);
             

            $this->element_index++;
            }
            return true;
        }
        public function generateCategoriesOption($categories, $items_to_skip = null)
        {
            $subcatvals = array();
            $spacer_size = '3';
            $this->smartcat[0]['id_category'] = 'none';
            $this->smartcat[0]['name'] = 'None';            
            $this->element_index = 1;
            foreach ($categories as $key => $category)
            {
                    $this->smartcat[$this->element_index]['id_category'] = $category['id_category'];
                    $this->smartcat[$this->element_index]['name'] = str_repeat('&nbsp;', $spacer_size * (int)$category['level_depth']).$category['name'];
                if (isset($category['children']))
                      $this->generatesubCategoriesOption($category['children']);
                $this->element_index++;
            }
            
            return $this->smartcat;
        }
        public function getSimpleProducts()
    {
        // if (!$context)
            $context = Context::getContext();
        $id_lang = (int)Context::getContext()->language->id;

        $front = true;
        if (!in_array($context->controller->controller_type, array('front', 'modulefront')))
            $front = false;

        $sql = 'SELECT p.`id_product`, pl.`name`
                FROM `'._DB_PREFIX_.'product` p
                '.Shop::addSqlAssociation('product', 'p').'
                LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` '.Shop::addSqlRestrictionOnLang('pl').')
                WHERE pl.`id_lang` = '.(int)$id_lang.'
                '.($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '').'
                ORDER BY pl.`name`';
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
    }
        public function getproduct()
        {
            $rs = array();
            $rslt[0]['id_product'] = '-1';
            $rslt[0]['name'] = 'None';
            $rslt[1]['id_product'] = 0;
            $rslt[1]['name'] = 'All';
            $sql = 'SELECT p.id_product,pl.name FROM '._DB_PREFIX_.'product p INNER JOIN 
            '._DB_PREFIX_.'product_lang pl ON p.id_product=pl.id_product INNER JOIN 
            '._DB_PREFIX_.'product_shop ps ON pl.id_product = ps.id_product AND ps.id_shop = '.(int) Context::getContext()->shop->id.'
            WHERE pl.id_lang='.(int)Context::getContext()->language->id.'
            AND p.active= 1 ORDER BY p.id_product DESC';
        $rs =  $this->getSimpleProducts();
        $i = 2;
        foreach($rs as $r){
            $rslt[$i]['id_product'] = $r['id_product'];
            $rslt[$i]['name'] = $r['name'];
            $i++;
         }
         return $rslt;
        }
    public function hookdisplayHeader($params) {
		if(Configuration::get('smart_load_font')=='module'){
			$this->context->controller->addCSS($this->_path . 'css/font-awesome.min.css');
		}
		if(Configuration::get('smart_load_bootstrapcss')=='module'){
			$this->context->controller->addCSS($this->_path . 'css/bootstrap/css/bootstrap.min.css');
		}
		if(Configuration::get('smart_load_bootstrapjs')=='module'){
			$this->context->controller->addJS($this->_path . 'css/bootstrap/js/bootstrap.min.js');
		}
        $this->context->controller->addCSS($this->_path . 'css/smartshortcode.css');
        $this->context->controller->addCSS($this->_path . 'css/magnific-popup.css');
        $this->context->controller->addJS('http'.((Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE')) ? 's' : '').'://maps.google.com/maps/api/js?sensor=true');
        $this->context->controller->addJS($this->_path . 'js/jquery.magnific-popup.min.js');
        $this->context->controller->addJS($this->_path . 'js/smartshortcode.js');
        $this->context->controller->addJS($this->_path . 'js/vallenato.js');
        if (isset($this->context->controller->php_self) && $this->context->controller->php_self == 'index')
            $this->context->controller->addCSS(_THEME_CSS_DIR_ . 'product_list.css');
        $this->context->controller->addCSS(($this->_path) . 'homefeatured.css', 'all');

        $this->context->controller->addJS($this->_path . 'js/jquery.flexslider.js');
      $this->context->controller->addCSS(($this->_path) . 'css/flexslider.css', 'all');   
    }

         public static function render_product_ui($cache_products){

        $context = Context::getContext(); 
        $context->smarty->assign(
                        array(
                            'products' => $cache_products,
                            'add_prod_display' => Configuration::get('PS_ATTRIBUTE_CATEGORY_DISPLAY'),
                            'homeSize' => Image::getSize(ImageType::getFormatedName('home')),
                        )
                    );
        $output = $context->smarty->fetch(_PS_MODULE_DIR_.'smartshortcode/smartshortcode.tpl');

        return $output;
        }
   
        public function  hookdisplayBackOfficeHeader($params){
    			$this->smarty->assign( array(
                                  'smartmodules_dir' => __PS_BASE_URI__
                         ));
              return $this->display(__FILE__, 'views/templates/front/addjs.tpl');
    		   //$cntr = new AdminController();
               //$cntr->addJS(_PS_MODULE_DIR_ . 'smartshortcode/js/addjs.js');
    		// return $this->context->controller->addJS(_PS_MODULE_DIR_ . 'smartshortcode/js/addjs.js');
        }
	
        protected function moduleControllerRegistration()
        {
            $tab = new Tab(null, Configuration::get('PS_LANG_DEAFULT'), Configuration::get('PS_SHOP_DEAFULT'));
            $tab->class_name = 'Smartshortcode_ajax';
            $tab->id_parent  = 0;
            $tab->module     = $this->name;
            $tab->name       = "Smart Shortcode Ajax Controller";
            $tab->position   = 10;
            $tab->active     = 0;
            $tab->add();
            if(!$tab->id)
                return FALSE;
            Configuration::updateValue('SMARTSHORTCODE_CONTROLLER_TABS', json_encode(array($tab->id)));
            return true;
        }

        protected function moduleControllerUnRegistration()
        {
            $ids = json_decode(Configuration::get('SMARTSHORTCODE_CONTROLLER_TABS'), true);
            foreach ($ids AS $id)
            {
                $tab = new Tab($id);
                $tab->delete();
            }

            return true;        
        }
        
        public function openZip($file_to_open) {
		//$zip = new ZipArchive;
		$target = _PS_ROOT_DIR_."/js/tiny_mce/plugins";  
		$zip = new ZipArchive();  //This is line 14
		$x = $zip->open($file_to_open);  
		if($x === true) {
			$zip->extractTo($target);  
			$zip->close();
		} 
	}   
	public static function getProductsByCategoryID($category_id,$limit=4, $id_lang = null, $id_shop = null, $child_count = false, $order_by = 'id_product', $order_way = "DESC")
	{
            $context = Context::getContext(); 
            $id_lang = is_null($id_lang) ? $context->language->id : $id_lang ;
            $id_shop = is_null($id_shop) ? $context->shop->id : $id_shop ;
            $id_supplier = (int)Tools::getValue('id_supplier');
            $active = true;
            $front = true;
            $sql = 'SELECT p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity, MAX(product_attribute_shop.id_product_attribute) id_product_attribute, product_attribute_shop.minimal_quantity AS product_attribute_minimal_quantity, pl.`description`, pl.`description_short`, pl.`available_now`,
                                    pl.`available_later`, pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`, pl.`name`, MAX(image_shop.`id_image`) id_image,
                                    il.`legend`, m.`name` AS manufacturer_name, cl.`name` AS category_default,
                                    DATEDIFF(product_shop.`date_add`, DATE_SUB(NOW(),
                                    INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).'
						DAY)) > 0 AS new, product_shop.price AS orderprice
				FROM `'._DB_PREFIX_.'category_product` cp
				LEFT JOIN `'._DB_PREFIX_.'product` p
					ON p.`id_product` = cp.`id_product`
				'.Shop::addSqlAssociation('product', 'p').'
				LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa
				ON (p.`id_product` = pa.`id_product`)
				'.Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.`default_on` = 1').'
				'.Product::sqlStock('p', 'product_attribute_shop', false, $context->shop).'
				LEFT JOIN `'._DB_PREFIX_.'category_lang` cl
					ON (product_shop.`id_category_default` = cl.`id_category`
					AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('cl').')
				LEFT JOIN `'._DB_PREFIX_.'product_lang` pl
					ON (p.`id_product` = pl.`id_product`
					AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').')
				LEFT JOIN `'._DB_PREFIX_.'image` i
					ON (i.`id_product` = p.`id_product`)'.
				Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
				LEFT JOIN `'._DB_PREFIX_.'image_lang` il
					ON (image_shop.`id_image` = il.`id_image`
					AND il.`id_lang` = '.(int)$id_lang.')
				LEFT JOIN `'._DB_PREFIX_.'manufacturer` m
					ON m.`id_manufacturer` = p.`id_manufacturer`
				WHERE product_shop.`id_shop` = '.(int)$context->shop->id.'
					AND cp.`id_category` = '.(int)$category_id
					.($active ? ' AND product_shop.`active` = 1' : '')
					.($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '')
					.($id_supplier ? ' AND p.id_supplier = '.(int)$id_supplier : '')
					.' GROUP BY product_shop.id_product';
                
                
            if (empty($order_by) || $order_by == 'position') $order_by = 'price';
            if (empty($order_way)) $order_way = 'DESC';
            if ($order_by == 'id_product' || $order_by == 'price' || $order_by == 'date_add'  || $order_by == 'date_upd')
                    $order_by_prefix = 'p';
            else if ($order_by == 'name')
                    $order_by_prefix = 'pl';

            $sql .= " ORDER BY {$order_by_prefix}.{$order_by} {$order_way}";
            $sql .= ' LIMIT '.$limit.' '; 
           $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        if(!$result)
            return array();    
		return Product::getProductsProperties($id_lang, $result);
	}
	public function restorfile()
	{
		@unlink(_PS_ROOT_DIR_."/js/tinymce.inc.js");
		//@unlink(_PS_ROOT_DIR_."js/tiny_mce/plugins/shortcode/");
		@copy(dirname(__FILE__)."/backup/tinymce.inc.js",_PS_ROOT_DIR_."/js/tinymce.inc.js");
		//@unlink(_PS_MODULE_DIR_."smartshortcode/backup/tinymce.inc.js");
	}
	/*Start Module Admin Setting*/

    public static function getProductfortab($active = false,$id_lang=1)
    {
        $id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $cache_id = 'smartproducttabcreator::getProduct_'.(bool)$active;
        if (!Cache::isStored($cache_id))
        {
            $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
            SELECT * FROM `'._DB_PREFIX_.'product` p INNER JOIN `'._DB_PREFIX_.'product_lang` pl ON(p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)($id_lang).')');
            Cache::store($cache_id, $result);
        }
        return Cache::retrieve($cache_id);
    }
	/*End Module Admin Setting*/
	
    public static function  add_shortcode($tag,$func){
            self::$static_shortcode_tags[$tag] = $func;
    }
	
    public static function do_shortcode($content,$hook_name='') {
            //$this->shortcodes = self::$static_shortcode_tags;
            $shortcode_tags = self::$static_shortcode_tags;
            if (empty($shortcode_tags) || !is_array($shortcode_tags))
                    return $content;

            $pattern = self::get_shortcode_regex();
            
            self::$sds_current_hook = $hook_name;
            
            
            return preg_replace_callback( "/$pattern/s", array(new SmartShortCode,'do_shortcode_tag'), $content );
            
    }
    public static function do_shortcode_tag( $m ) {
            $shortcode_tags = self::$static_shortcode_tags;

            // allow [[foo]] syntax for escaping a tag
            if ( $m[1] == '[' && $m[6] == ']' ) {
                    return substr($m[0], 1, -1);
            }
          
            $tag = $m[2];
            $attr = self::shortcode_parse_atts( $m[3] );
            
            if ( isset( $m[5] ) ) {
                    // enclosing tag - extra parameter
                    return $m[1] . call_user_func( $shortcode_tags[$tag], $attr, $m[5], $tag, self::$sds_current_hook ) . $m[6];
            } else {
                    // self-closing tag
                    return $m[1] . call_user_func( $shortcode_tags[$tag], $attr, null,  $tag, self::$sds_current_hook ) . $m[6];
            }
    }
    public static function get_shortcode_regex() {
            
            $tagnames = array_keys(self::$static_shortcode_tags);
            $tagregexp = join( '|', array_map('preg_quote', $tagnames) );

            // WARNING! Do not change this regex without changing do_shortcode_tag() and strip_shortcode_tag()
            // Also, see shortcode_unautop() and shortcode.js.
            return
                      '\\['                              // Opening bracket
                    . '(\\[?)'                           // 1: Optional second opening bracket for escaping shortcodes: [[tag]]
                    . "($tagregexp)"                     // 2: Shortcode name
                    . '(?![\\w-])'                       // Not followed by word character or hyphen
                    . '('                                // 3: Unroll the loop: Inside the opening shortcode tag
                    .     '[^\\]\\/]*'                   // Not a closing bracket or forward slash
                    .     '(?:'
                    .         '\\/(?!\\])'               // A forward slash not followed by a closing bracket
                    .         '[^\\]\\/]*'               // Not a closing bracket or forward slash
                    .     ')*?'
                    . ')'
                    . '(?:'
                    .     '(\\/)'                        // 4: Self closing tag ...
                    .     '\\]'                          // ... and closing bracket
                    . '|'
                    .     '\\]'                          // Closing bracket
                    .     '(?:'
                    .         '('                        // 5: Unroll the loop: Optionally, anything between the opening and closing shortcode tags
                    .             '[^\\[]*+'             // Not an opening bracket
                    .             '(?:'
                    .                 '\\[(?!\\/\\2\\])' // An opening bracket not followed by the closing shortcode tag
                    .                 '[^\\[]*+'         // Not an opening bracket
                    .             ')*+'
                    .         ')'
                    .         '\\[\\/\\2\\]'             // Closing shortcode tag
                    .     ')?'
                    . ')'
                    . '(\\]?)';                          // 6: Optional second closing brocket for escaping shortcodes: [[tag]]
    }
        
    public static function shortcode_parse_atts($text) {
            $atts = array();
            $pattern = '/(\w+)\s*=\s*"([^"]*)"(?:\s|$)|(\w+)\s*=\s*\'([^\']*)\'(?:\s|$)|(\w+)\s*=\s*([^\s\'"]+)(?:\s|$)|"([^"]*)"(?:\s|$)|(\S+)(?:\s|$)/';
            $text = preg_replace("/[\x{00a0}\x{200b}]+/u", " ", $text);
            if ( preg_match_all($pattern, $text, $match, PREG_SET_ORDER) ) {
                    foreach ($match as $m) {
                            if (!empty($m[1]))
                                    $atts[strtolower($m[1])] = stripcslashes($m[2]);
                            elseif (!empty($m[3]))
                                    $atts[strtolower($m[3])] = stripcslashes($m[4]);
                            elseif (!empty($m[5]))
                                    $atts[strtolower($m[5])] = stripcslashes($m[6]);
                            elseif (isset($m[7]) and strlen($m[7]))
                                    $atts[] = stripcslashes($m[7]);
                            elseif (isset($m[8]))
                                    $atts[] = stripcslashes($m[8]);
                    }
            } else {
                    $atts = ltrim($text);
            }
            return $atts;
    }
        
    public static function shortcode_atts( $pairs, $atts, $shortcode = '' ) {
        $atts = (array)$atts;
        $out = array();
        foreach($pairs as $name => $default) {
                if ( array_key_exists($name, $atts) )
                        $out[$name] = $atts[$name];
                else
                        $out[$name] = $default;
        }            
        if ( $shortcode )
                $out = apply_filters( "shortcode_atts_{$shortcode}", $out, $pairs, $atts );

        return $out;
    }
        
    public static function strip_shortcodes( $content ) {
            $shortcode_tags = self::$static_shortcode_tags;

            if (empty($shortcode_tags) || !is_array($shortcode_tags))
                    return $content;

            $pattern = $this->get_shortcode_regex();

            return preg_replace_callback( "/$pattern/s", array($this,'strip_shortcode_tag'), $content );
    }

    public static function strip_shortcode_tag( $m ) {
            // allow [[foo]] syntax for escaping a tag
            if ( $m[1] == '[' && $m[6] == ']' ) {
                    return substr($m[0], 1, -1);
            }

            return $m[1] . $m[6];
    }
    
    public function parse ($str,$hook_name = '')
    {                   
        return self::do_shortcode($str,$hook_name);
    } 
	public static function getProductbycat($id_category,$order_by=null,$orderway=null){
                    $id_lang = (int)Context::getContext()->language->id;
                    $id_shop = (int)Context::getContext()->shop->id;
					if($order_by == null){
						$order_by = 'price';
					}
					if($orderway == null){
						$orderway = 'ASC';
					}
      $sql = 'SELECT * FROM '._DB_PREFIX_.'product p INNER JOIN 
                '._DB_PREFIX_.'product_lang pl ON p.id_product=pl.id_product INNER JOIN 
                '._DB_PREFIX_.'product_shop ps ON p.id_product = ps.id_product AND ps.id_category_default= '.$id_category.' INNER JOIN
				'._DB_PREFIX_.'image img ON img.id_product=p.id_product AND img.cover = 1 INNER JOIN
				'._DB_PREFIX_.'category_product cp ON cp.id_product=p.id_product
                WHERE pl.id_lang='.$id_lang.'
                AND p.active= 1 AND ps.id_shop = '.$id_shop.' AND cp.id_category = '.$id_category.' AND p.id_category_default = '.$id_category.'  AND pl.id_shop = '.$id_shop.' ORDER BY p.'.$order_by.' '.$orderway;
        
			$result = Db::getInstance()->executeS($sql);
        return $result;
    }
	public function getBestSellers()
	{
		if (Configuration::get('PS_CATALOG_MODE'))
			return false;

		if (!($result = ProductSale::getBestSalesLight((int)$this->context->language->id, 0, 8)))
			return (Configuration::get('PS_BLOCK_BESTSELLERS_DISPLAY') ? array() : false);

		$currency = new Currency((int)Configuration::get('PS_CURRENCY_DEFAULT'));
		$usetax = (Product::getTaxCalculationMethod((int)$this->context->customer->id) != PS_TAX_EXC);
		foreach ($result as &$row)
			$row['price'] = Tools::displayPrice(Product::getPriceStatic((int)$row['id_product'], $usetax), $currency);

		return $result;
	}
	public function getNewProducts()
	{
		if (!Configuration::get('NEW_PRODUCTS_NBR'))
			return;
		$newProducts = false;
		if (Configuration::get('PS_NB_DAYS_NEW_PRODUCT'))
			$newProducts = Product::getNewProducts((int) $this->context->language->id, 0, (int)Configuration::get('NEW_PRODUCTS_NBR'));

          
                if (!$newProducts && Configuration::get('PS_BLOCK_NEWPRODUCTS_DISPLAY'))
			return;
                
                
		return $newProducts;
	}		
	/**
	 * Get path to front office templates for the module
	 *
	 * @return string
	 */
        
        public static function getInstance(){
            if(SmartShortCode::$smartshortcode instanceof SmartShortCode)
                return SmartShortCode::$smartshortcode;
            else {
                SmartShortCode::$smartshortcode = new SmartShortCode;
                return SmartShortCode::$smartshortcode;                
            }
        }
	
}




    function bold_cb($atts,$content){    
        extract(SmartShortCode::shortcode_atts(
                array(
                    'size'=>'12'
                ),$atts
        ));
        $html = '<strong>';
        if(isset($size) && !empty($size)){        
            $html .= "<span style='font-size:{$size}px'>{$content}</span>";        
        }
        $html .= "</strong>";
        return $html;
    }
    SmartShortCode::add_shortcode('bold','bold_cb');

    SmartShortCode::add_shortcode('sds_manufacturers','sds_manufacturers_cb');
    function sds_manufacturers_cb($atts = array(), $content){
        extract(SmartShortCode::shortcode_atts(
            array(
                'speed'=>'600',
                'maxslide'=>'6'
            ),$atts
        ));
        $context = Context::getContext(); 
        $manufacturers = Manufacturer::getManufacturers(false,$context->language->id, true);
        $context->smarty->assign(
            array(
                'manufacturers' => $manufacturers,
                'speed' => $speed,
                'maxslide' => $maxslide
            )
        );
        $output = $context->smarty->fetch(_PS_MODULE_DIR_.'smartshortcode/smartmanufacturer.tpl');

        return $output;
        
    }

 
    SmartShortCode::add_shortcode('gmap', 'sds_gmap_cb');

    function sds_gmap_cb($atts = array()) {

        extract(SmartShortCode::shortcode_atts(array(
            'height' => '440',
            'lat' => '23.7467330',
            'lng' => '90.4202590',
            'type' => 'ROADMAP',
            'zoom' => '17',
            'mapTypeControl' => 'true',
            'scrollwheel' => 'false'
            ), $atts));

        if (empty($lat) || empty($lng))
            return false;
        
        $map_id = "map_".rand(00000,99999);
        
        ob_start();
        ?>
        <div class="google-maps">
            <div id="<?php echo $map_id?>"></div>        
        </div>
        <script type="text/javascript">
            jQuery(function($){
                $(window).load(function(){
                    var mapelem = $("#<?php echo $map_id?>");
                    mapelem.css({width:'100%',height:'<?php echo $height?>px'});

                    var latlng = new google.maps.LatLng(parseFloat(<?php echo $lat?>),parseFloat(<?php echo $lng?>));

                    // Creating an object literal containing the properties we want to pass to the map  
                    var options = {  
                        zoom: <?php echo $zoom?>, // This number can be set to define the initial zoom level of the map
                        center: latlng,
                        mapTypeId: google.maps.MapTypeId.<?php echo $type?>, // This value can be set to define the map type ROADMAP/SATELLITE/HYBRID/TERRAIN
                        mapTypeControl: <?php echo $mapTypeControl ?>,
                        scrollwheel: <?php echo $scrollwheel ?>
                    };  
                    // Calling the constructor, thereby initializing the map  
                    var map = new google.maps.Map(document.getElementById("<?php echo $map_id?>"), options);
                }); 
           }); 
        </script>    
        <?php
        $contents = ob_get_contents();
        ob_end_clean();
        return $contents;
    }

    

    function smart_row($atts, $content = null, $tag, $hook_name) {
        return '<div class="row">' . SmartShortCode::do_shortcode($content,$hook_name) . '</div>';
    }
    SmartShortCode::add_shortcode('smart_row', 'smart_row');

    function smart_col($atts, $content = null, $tag, $hook_name) {
        extract(SmartShortCode::shortcode_atts(array(
            "class" => ''
                        ), $atts));
        return '<div class="'.$class.' ">' .SmartShortCode::do_shortcode($content,$hook_name). '</div>';
    }
    SmartShortCode::add_shortcode('smart_col', 'smart_col');

    function shortcode_break($atts, $content = null) {

        return '<div class="break clearfix">&nbsp;</div>';
    }

    SmartShortCode::add_shortcode('break', 'shortcode_break');

//divider

    function shortcode_divider($atts, $content = null) {

        return '<div class="divider clearfix">&nbsp;</div>';
    }

    SmartShortCode::add_shortcode('divider', 'shortcode_divider');

//divider top

    function shortcode_dividertop($atts, $content = null) {

        return '<div class="totop"><div class="gototop"></div></div>';
    }

    SmartShortCode::add_shortcode('dividertop', 'shortcode_dividertop');

//ribbon red

    function shortcode_ribbon_red($atts, $content = null, $tag, $hook_name) {

        extract(SmartShortCode::shortcode_atts(array(
            "url" => ''
                        ), $atts));

        return '<div class="ribbon"><div class="ribbon_left_red"></div><div class="ribbon_center_red"><a href ="' . $url . '">' . SmartShortCode::do_shortcode($content,$hook_name) . '</a></div><div class="ribbon_right_red"></div></div>';
    }

    SmartShortCode::add_shortcode('ribbon_red', 'shortcode_ribbon_red');

//ribbon blue

    function shortcode_ribbon_blue($atts, $content = null, $tag, $hook_name) {

        extract(SmartShortCode::shortcode_atts(array(
            "url" => ''
                        ), $atts));

        return '<div class="ribbon"><div class="ribbon_left_blue"></div><div class="ribbon_center_blue"><a href ="' . $url . '">' . SmartShortCode::do_shortcode($content,$hook_name) . '</a></div><div class="ribbon_right_blue"></div></div>';
    }

    SmartShortCode::add_shortcode('ribbon_blue', 'shortcode_ribbon_blue');

//ribbon yellow

    function shortcode_ribbon_yellow($atts, $content = null, $tag, $hook_name) {

        extract(SmartShortCode::shortcode_atts(array(
            "url" => ''
                        ), $atts));

        return '<div class="ribbon"><div class="ribbon_left_yellow"></div><div class="ribbon_center_yellow"><a href ="' . $url . '">' . SmartShortCode::do_shortcode($content,$hook_name) . '</a></div><div class="ribbon_right_yellow"></div></div>';
    }

    SmartShortCode::add_shortcode('ribbon_yellow', 'shortcode_ribbon_yellow');

//ribbon green

    function shortcode_ribbon_green($atts, $content = null, $tag, $hook_name) {

        extract(SmartShortCode::shortcode_atts(array(
            "url" => ''
                        ), $atts));

        return '<div class="ribbon"><div class="ribbon_left_green"></div><div class="ribbon_center_green"><a href ="' . $url . '">' . SmartShortCode::do_shortcode($content,$hook_name) . '</a></div><div class="ribbon_right_green"></div></div>';
    }

    SmartShortCode::add_shortcode('ribbon_green', 'shortcode_ribbon_green');

//ribbon white

    function shortcode_ribbon_white($atts, $content = null, $tag, $hook_name) {

        extract(SmartShortCode::shortcode_atts(array(
            "url" => ''
                        ), $atts));

        return '<div class="ribbon"><div class="ribbon_left_white"></div><div class="ribbon_center_white"><a href ="' . $url . '">' . SmartShortCode::do_shortcode($content,$hook_name) . '</a></div><div class="ribbon_right_white"></div></div>';
    }

    SmartShortCode::add_shortcode('ribbon_white', 'shortcode_ribbon_white');

//high light dark

    function shortcode_highlight_black($atts, $content = null, $tag, $hook_name) {

        return '<span class="black" >' . SmartShortCode::do_shortcode($content,$hook_name). '</span>';
    }

    SmartShortCode::add_shortcode('highlight_black', 'shortcode_highlight_black');

//high light yellow

    function shortcode_highlight_yellow($atts, $content = null, $tag, $hook_name) {

        return '<span class="yellow" >' . SmartShortCode::do_shortcode($content,$hook_name) . '</span>';
    }

    SmartShortCode::add_shortcode('highlight_yellow', 'shortcode_highlight_yellow');

//high light blue

    function shortcode_highlight_blue($atts, $content = null, $tag, $hook_name) {

        return '<span class="blue" >' . SmartShortCode::do_shortcode($content,$hook_name) . '</span>';
    }

    SmartShortCode::add_shortcode('highlight_blue', 'shortcode_highlight_blue');

//high light green

    function shortcode_highlight_green($atts, $content = null, $tag, $hook_name) {

        return '<span class="green" >' . SmartShortCode::do_shortcode($content,$hook_name). '</span>';
    }

    SmartShortCode::add_shortcode('highlight_green', 'shortcode_highlight_green');

//smart list shortcodes

    function remove_li_shortcode_directives($content) {
        $content = preg_replace('/\]/', '>', preg_replace('/\[/', '<', $content));

        return $content;
    }

    function smart_list_circle($atts, $content = null, $tag, $hook_name) {
        return '<ul>' . remove_li_shortcode_directives($content,$hook_name) . '</ul>';
    }

    SmartShortCode::add_shortcode('smart_list_circle', 'smart_list_circle');

    function smart_list_arrow($atts, $content = null, $tag, $hook_name) {

        return '<ul class="fa-ul i-caret-right" >' . remove_li_shortcode_directives($content,$hook_name) . '</ul>';
    }
    SmartShortCode::add_shortcode('smart_list_arrow', 'smart_list_arrow');

    function smart_list_sin_arrow($atts, $content = null, $tag, $hook_name) {

        return '<ul class="fa-ul i-angle-right" >' . remove_li_shortcode_directives($content,$hook_name) . '</ul>';
    }

    SmartShortCode::add_shortcode('smart_list_sin_arrow', 'smart_list_sin_arrow');

    function smart_list_icon($atts, $content = null, $tag, $hook_name) {

        return '<ul class="fa-ul i-bullseye" >' . remove_li_shortcode_directives($content,$hook_name) . '</ul>';
    }

    SmartShortCode::add_shortcode('smart_list_icon', 'smart_list_icon');

    function shortcode_list_checkbox2($atts, $content = null, $tag, $hook_name) {

        return '<ul class="list_style style5" >' . remove_li_shortcode_directives($content,$hook_name) . '</ul>';
    }

    SmartShortCode::add_shortcode('list_checkbox2', 'shortcode_list_checkbox2');

    function shortcode_list_cross2($atts, $content = null, $tag, $hook_name) {

        return '<ul class="list_style style6" >' . remove_li_shortcode_directives($content,$hook_name) . '</ul>';
    }

    SmartShortCode::add_shortcode('list_cross2', 'shortcode_list_cross2');

    function shortcode_list_rarrow2($atts, $content = null, $tag, $hook_name) {

        return '<ul class="list_style style7" >' . remove_li_shortcode_directives($content,$hook_name) . '</ul>';
    }

    SmartShortCode::add_shortcode('list_rarrow2', 'shortcode_list_rarrow2');

    function shortcode_list_circle2($atts, $content = null, $tag, $hook_name) {

        return '<ul class="list_style style8" >' . remove_li_shortcode_directives($content,$hook_name) . '</ul>';
    }

    SmartShortCode::add_shortcode('list_circle2', 'shortcode_list_circle2');

    function shortcode_list_green_checkbox($atts, $content = null, $tag, $hook_name) {

        return '<ul class="list_style style9" >' . remove_li_shortcode_directives($content,$hook_name) . '</ul>';
    }

    SmartShortCode::add_shortcode('list_checkbox3', 'shortcode_list_green_checkbox');

    function shortcode_list_cross3($atts, $content = null, $tag, $hook_name) {

        return '<ul class="list_style style10" >' . remove_li_shortcode_directives($content,$hook_name) . '</ul>';
    }

    SmartShortCode::add_shortcode('list_cross3', 'shortcode_list_cross3');

    function shortcode_list_rarrow3($atts, $content = null, $tag, $hook_name) {

        return '<ul class="list_style style11" >' . remove_li_shortcode_directives($content,$hook_name) . '</ul>';
    }

    SmartShortCode::add_shortcode('list_rarrow3', 'shortcode_list_rarrow3');

    function shortcode_list_circle3($atts, $content = null, $tag, $hook_name) {

        return '<ul class="list_style style12" >' . remove_li_shortcode_directives($content,$hook_name) . '</ul>';
    }

    SmartShortCode::add_shortcode('list_circle3', 'shortcode_list_circle3');

    //dropcaps

    function shortcode_dropcap_with_bg($atts, $content = null, $tag, $hook_name) {

        return '<span class="dropcap large bg">' . SmartShortCode::do_shortcode($content,$hook_name) . '</span>';
    }

    SmartShortCode::add_shortcode('dropcap_with_bg', 'shortcode_dropcap_with_bg');

    function shortcode_dropcap($atts, $content = null, $tag, $hook_name) {

        return '<span class="dropcap">' . SmartShortCode::do_shortcode($content,$hook_name) . '</span>';
    }

    SmartShortCode::add_shortcode('dropcap', 'shortcode_dropcap');

    //smart blockquotes

    function shortcode_blockquote($atts, $content = null, $tag, $hook_name) {

        return '<div class="block-quote">' . SmartShortCode::do_shortcode($content,$hook_name) . '</div>';
    }

    SmartShortCode::add_shortcode('block-quote', 'shortcode_blockquote');

    function shortcode_blockquote_left($atts, $content = null, $tag, $hook_name) {

        return '<div class="block-quote block-quote-left">' . SmartShortCode::do_shortcode($content,$hook_name). '</div>';
    }

    SmartShortCode::add_shortcode('block-quote-left', 'shortcode_blockquote_left');

    function shortcode_blockquote_right($atts, $content = null, $tag, $hook_name) {

        return '<div class="block-quote block-quote-right">' . SmartShortCode::do_shortcode($content,$hook_name) . '</div>';
    }

    SmartShortCode::add_shortcode('block-quote-right', 'shortcode_blockquote_right');

    //smart testimonial quote

    function shortcode_testmonial_quote($atts, $content = null, $tag, $hook_name) {

        $defaults = array(
            'name' => 'Name'
        );

        extract(SmartShortCode::shortcode_atts($defaults, $atts));

        return "<div class='testimonial-quote'><p>".SmartShortCode::do_shortcode($content,$hook_name)."</p><h6 class='name'>$name</h6></div>";
    }

    SmartShortCode::add_shortcode('testimonial_quote', 'shortcode_testmonial_quote');

    //smart buttons wrap


    function shortcode_button_small($atts, $content = null, $tag, $hook_name) {

        return "<div class='buttons'>" . SmartShortCode::do_shortcode($content,$hook_name) . "</div>";
    }

    SmartShortCode::add_shortcode('buttons_small', 'shortcode_button_small');

    function shortcode_button_medium($atts, $content = null, $tag, $hook_name) {

        return "<div class='buttons medium'>" . SmartShortCode::do_shortcode($content,$hook_name) . "</div>";
    }

    SmartShortCode::add_shortcode('buttons_medium', 'shortcode_button_medium');

    function shortcode_button_large($atts, $content = null, $tag, $hook_name) {

        return "<div class='buttons large'>" . SmartShortCode::do_shortcode($content,$hook_name) . "</div>";
    }

    SmartShortCode::add_shortcode('buttons_large', 'shortcode_button_large');

    //smart buttons


    function shortcode_button($atts, $content = null, $tag, $hook_name) {

        $defaults = array(
            'href' => '#',
            'class' => '#',
    		'target'=>'_blank',
    		'icon'=>''
        );
    	extract(SmartShortCode::shortcode_atts($defaults, $atts));
    if(empty($icon) && $icon == ''){
     return "<a class='sds-btn $class ' href='$href' target='_blank'>".SmartShortCode::do_shortcode($content,$hook_name)."</a>";
    }else{
     return "<a class='sds-btn $class ' href='$href' target='_blank'><i class='$icon'></i> ".SmartShortCode::do_shortcode($content,$hook_name)."</a>";
    }
    }

    SmartShortCode::add_shortcode('button', 'shortcode_button');

    function shortcode_button_gray($atts, $content = null, $tag, $hook_name) {

        $defaults = array(
            'href' => '#'
        );

        extract(SmartShortCode::shortcode_atts($defaults, $atts));

        return "<a class='shortcode-button gray' href='$href'>".SmartShortCode::do_shortcode($content,$hook_name)."</a>";
    }

    SmartShortCode::add_shortcode('button_grey', 'shortcode_button_gray');

    function shortcode_button_red($atts, $content = null, $tag, $hook_name) {

        $defaults = array(
            'href' => '#'
        );

        extract(SmartShortCode::shortcode_atts($defaults, $atts));

        return "<a class='shortcode-button red' href='$href'>".SmartShortCode::do_shortcode($content,$hook_name)."</a>";
    }

    SmartShortCode::add_shortcode('button_red', 'shortcode_button_red');

    function shortcode_button_yellow($atts, $content = null, $tag, $hook_name) {

        $defaults = array(
            'href' => '#'
        );

        extract(SmartShortCode::shortcode_atts($defaults, $atts));

        return "<a class='shortcode-button yellow' href='$href'>".SmartShortCode::do_shortcode($content,$hook_name)."</a>";
    }

    SmartShortCode::add_shortcode('button_yellow', 'shortcode_button_yellow');

    function shortcode_button_olive($atts, $content = null, $tag, $hook_name) {

        $defaults = array(
            'href' => '#'
        );

        extract(SmartShortCode::shortcode_atts($defaults, $atts));

        return "<a class='shortcode-button olive' href='$href'>".SmartShortCode::do_shortcode($content,$hook_name)."</a>";
    }

    SmartShortCode::add_shortcode('button_olive', 'shortcode_button_olive');

    function shortcode_button_lightblue($atts, $content = null, $tag, $hook_name) {

        $defaults = array(
            'href' => '#'
        );

        extract(SmartShortCode::shortcode_atts($defaults, $atts));

        return "<a class='shortcode-button lightblue' href='$href'>".SmartShortCode::do_shortcode($content,$hook_name)."</a>";
    }

    SmartShortCode::add_shortcode('button_lightblue', 'shortcode_button_lightblue');

    function shortcode_button_black($atts, $content = null, $tag, $hook_name) {

        $defaults = array(
            'href' => '#'
        );

        extract(SmartShortCode::shortcode_atts($defaults, $atts));

        return "<a class='shortcode-button black' href='$href'>".SmartShortCode::do_shortcode($content,$hook_name)."</a>";
    }

    SmartShortCode::add_shortcode('button_black', 'shortcode_button_black');

    //button buy

    function shortcode_button_purche($atts, $content = null, $tag, $hook_name) {

        extract(SmartShortCode::shortcode_atts(array(
            "url" => '',
            "bottom_text" => ''
                        ), $atts));

        return '<div class="button_purche"><a href="' . $url . '"><div class="button_purche_left"></div><div class="button_purche_right"><div class="button_purche_right_top">' . SmartShortCode::do_shortcode($content,$hook_name) . '</div><div class="button_purche_right_bottom">' . $bottom_text . '</div></div></a></div>';
    }

    SmartShortCode::add_shortcode('button_purche', 'shortcode_button_purche');

    //button download

    function shortcode_button_download($atts, $content = null, $tag, $hook_name) {

        extract(SmartShortCode::shortcode_atts(array(
            "url" => '',
            "bottom_text" => ''
                        ), $atts));

        return '<div class="button_download"><a href="' . $url . '"><div class="button_download_left"></div><div class="button_download_right"><div class="button_download_right_top">' . SmartShortCode::do_shortcode($content,$hook_name) . '</div><div class="button_download_right_bottom">' . $bottom_text . '</div></div></a></div>';
    }

    SmartShortCode::add_shortcode('button_download', 'shortcode_button_download');

    //button search

    function shortcode_button_search_c($atts, $content = null, $tag, $hook_name) {

        extract(SmartShortCode::shortcode_atts(array(
            "url" => '',
            "bottom_text" => ''
                        ), $atts));

        return '<div class="button_search"><a href="' . $url . '"><div class="button_search_left"></div><div class="button_search_right"><div class="button_search_right_top">' . SmartShortCode::do_shortcode($content,$hook_name) . '</div><div class="button_search_right_bottom">' . $bottom_text . '</div></div></a></div>';
    }

    SmartShortCode::add_shortcode('button_search_c', 'shortcode_button_search_c');

    // message boxes

    function shortcode_msgbox($atts, $content = null, $tag, $hook_name) {

        return '<div class="msgBox">' . SmartShortCode::do_shortcode($content,$hook_name) . '</div>';
    }

    SmartShortCode::add_shortcode('msgbox', 'shortcode_msgbox');

    function shortcode_msgbox2($atts, $content = null, $tag, $hook_name) {

        return '<div class="msgBox bg1">' . SmartShortCode::do_shortcode($content,$hook_name) . '</div>';
    }

    SmartShortCode::add_shortcode('msgbox2', 'shortcode_msgbox2');

    function shortcode_msgbox3($atts, $content = null, $tag, $hook_name) {

        return '<div class="msgBox bg2">' . SmartShortCode::do_shortcode($content,$hook_name) . '</div>';
    }

    SmartShortCode::add_shortcode('msgbox3', 'shortcode_msgbox3');

    function shortcode_msgbox4($atts, $content = null, $tag, $hook_name) {

        return '<div class="msgBox bg4">' . SmartShortCode::do_shortcode($content,$hook_name) . '</div>';
    }

    SmartShortCode::add_shortcode('msgbox4', 'shortcode_msgbox4');

     


    class SDS_UI_Tabs {

        static $tab_counter = 0, $tablink = '', $tabcontent = '';

        static function smart_tabgroup($atts, $contents = null, $tag='',$hook_name='') {
                extract(SmartShortCode::shortcode_atts(array(
                'class' => ''
                            ), $atts));
        
            self::$tab_counter = 1;

            SmartShortCode::do_shortcode($contents,$hook_name);

            return "<div class='".$class."'><ul class='nav-tab'>" . self::$tablink . "</ul><div class='tab-content'>" . SmartShortCode::do_shortcode(self::$tabcontent,$hook_name) . '</div></div><br>';
         }

        static function smart_tab($atts, $contents= null, $tag='',$hook_name='') {

            extract(SmartShortCode::shortcode_atts(array(
                'title' => '',
                'unique' => 'true'
                            ), $atts));

            $unique_tab = !empty($unique) && $unique != 'false' ? rand(0000, 9999) : self::$tab_counter;


            if (self::$tab_counter == 1):

                self::$tablink = "<li class='active'><a href='#tab-{$unique_tab}' data-toggle='tab'>$title</a></li>";

                self::$tabcontent = "<div class='tab-pane fade in active' id='tab-{$unique_tab}'><p>".SmartShortCode::do_shortcode($contents,$hook_name)."</p></div>";

            else:

                self::$tablink .= "<li class=''><a href='#tab-{$unique_tab}' data-toggle='tab'>$title</a></li>";

                self::$tabcontent .= "<div class='tab-pane fade' id='tab-{$unique_tab}'><p>".SmartShortCode::do_shortcode($contents,$hook_name)."</p></div>";

            endif;

            self::$tab_counter++;
        }

    }
    SmartShortCode::add_shortcode('tabgroup', array('SDS_UI_Tabs', 'smart_tabgroup'));
    SmartShortCode::add_shortcode('tab', array('SDS_UI_Tabs', 'smart_tab'));

    function flexslider_shortcode($atts, $content = null, $tag, $hook_name) {

        $defaults = array(
            'class' => 'flexslider slider-pull-left',
            'id' => 'flexslider'
        );

        extract(SmartShortCode::shortcode_atts($defaults, $atts));

        return "<section id='$id' class=\"$class\">
            <ul class=\"slides\">" . SmartShortCode::do_shortcode(str_replace('<br />', '', $content),$hook_name) . "</ul>
        </section>
        <script type='text/javascript'>
            jQuery(function($){         
                $('#$id').flexslider(); 
            });
        </script>";
    }

    SmartShortCode::add_shortcode('flexslider', 'flexslider_shortcode');

    function flextab_shortcode($atts, $content = null, $tag, $hook_name) {

        $defaults = array(
            'link' => '',
            'imageurl' => get_template_directory_uri() . '/image/responsive.png',
            'title' => 'title'
        );

        extract(SmartShortCode::shortcode_atts($defaults, $atts));

        ob_start();
        ?>
        <li>
        <?php if ($link) { ?>
                <a href="<?php echo $link; ?>"><img src="<?php echo $imageurl; ?>" alt="<?php echo $title; ?>" /></a>
        <?php } else { ?>
                <img src="<?php echo $imageurl; ?>" alt="<?php echo $title; ?>" />
        <?php } ?>
        </li> 

        <?php
        $output = ob_get_contents();

        ob_end_clean();

        return $output;
    }

    SmartShortCode::add_shortcode('ftab', 'flextab_shortcode');

    function cameraslider_shortcode($atts, $content = null, $tag, $hook_name) {

        $defaults = array(
            'class' => 'camera_wrap camera_azure_skin',
            'id' => 'camera_wrap'
        );

        extract(SmartShortCode::shortcode_atts($defaults, $atts));

        return "<section class='fluid_container slider-pull-left'><div id='$id' class=\"$class\">" . SmartShortCode::do_shortcode(str_replace('<br />', '', $content),$hook_name) . "</div></section>
        <script type='text/javascript'>
            jQuery(function($){         
                $('#$id').camera({
                    thumbnails: true,
                    loader: true,
                    hover: false
                }); 
            });
        </script>";
    }

    SmartShortCode::add_shortcode('cameraslider', 'cameraslider_shortcode');

    function camtab_shortcode($atts, $content) {

        $defaults = array(
            'link' => '',
            'imageurl' => get_template_directory_uri() . '/image/responsive.png',
        );

        extract(SmartShortCode::shortcode_atts($defaults, $atts));

        ob_start();
        ?>

        <div data-link="<?php echo $link; ?>" data-thumb="<?php echo $imageurl; ?>" data-src="<?php echo $imageurl; ?>"></div>

        <?php
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }

    SmartShortCode::add_shortcode('ctab', 'camtab_shortcode');

    //Today date is 31-03-2014
    function blockquotes_cb($atts, $content = null, $tag, $hook_name) {
        extract(SmartShortCode::shortcode_atts(array(
            'class' => '',
            'author' => ''
        ), $atts));
        return '<blockquote class="'.$class.'"><p>'.SmartShortCode::do_shortcode($content,$hook_name).'</p><br><footer>'.$author.'</footer></blockquote>';
    }
    SmartShortCode::add_shortcode('blockquote', 'blockquotes_cb');


    function shortquotes_block($atts, $content = null, $tag, $hook_name) {
        extract(SmartShortCode::shortcode_atts(array(
            'class' => '',
            'footer_title' => ''
                        ), $atts));
        return '<blockquote class="'.$class.'"><p>'.SmartShortCode::do_shortcode($content,$hook_name).'</p><br><footer>'.$footer_title.'</footer></blockquote>';
    }
    SmartShortCode::add_shortcode('quotes_block', 'shortquotes_block');

    function shortquotes_block_reverse($atts, $content = null, $tag, $hook_name) {
        extract(SmartShortCode::shortcode_atts(array(
            'class' => '',
            'footer_title' => ''
        ), $atts));
        return '<blockquote class="blockquote-reverse '.$class.'"><p>'.SmartShortCode::do_shortcode($content,$hook_name).'</p><br><footer>'.$footer_title.'</footer></blockquote>';
    }
    SmartShortCode::add_shortcode('quotes_block_reverse', 'shortquotes_block_reverse');


    function sds_button_cb($atts, $content = null, $tag, $hook_name) {
        extract(SmartShortCode::shortcode_atts(array(
            'class' => '',
            'href' => '',
            'target' => '_self',
        ), $atts));
        return '<a target="'.$target.'" class="'.$class.'" href="'.$href.'">'.SmartShortCode::do_shortcode($content,$hook_name).'</a>';
        
    }
    SmartShortCode::add_shortcode('button', 'sds_button_cb');

    function sds_alert_box($atts, $content = null, $tag, $hook_name) {
        extract(SmartShortCode::shortcode_atts(array(
            'class' => ''
        ), $atts));
        return '<div class="alert_box fix"><div class="'.$class.'"><p>'.SmartShortCode::do_shortcode($content,$hook_name).'</p><button class="close" type="button" data-dismiss="alert"><i class="icon-remove"></i></button></div></div>';
        
    }
    SmartShortCode::add_shortcode('alert_box', 'sds_alert_box');


    function testimonial_cb($atts, $content = null, $tag, $hook_name) {
        extract(SmartShortCode::shortcode_atts(array(
            'class' => '',
            'author' => '',
            'style' => '0',
            'image_src' => '',
        ), $atts));
        
        
        switch($style):

            case 0:

            case 1:
                return '<div class="'.$class.'">'.SmartShortCode::do_shortcode($content,$hook_name).'<br><br><span>'.$author.'</span></div>';
                break;

            case 2:
                $markup = '<div class="'.$class.'">';
                
                if(!empty($image_src))
                      $markup .= '<img class="img-responsive" alt="" src="'.$image_src.'" />';
                
                $markup .= '<div class="testimonial_3_body_text">'.SmartShortCode::do_shortcode($content,$hook_name).'<br><br><span>'.$author.'</span></div></div>';
                
                return $markup;
                break;

            case 3:
                
                $markup = '<div class="'.$class.'"><div class="popover top"><div class="arrow"></div><div class="popover-content">'.SmartShortCode::do_shortcode($content,$hook_name).'</div>
    </div>';
                if(!empty($image_src))
                    $markup .= '<img alt="client" src="'.$image_src.'" />';
                
                $markup .= '<br><br><span>'.$author.'</span></div>';
                
                return $markup;            
                break;


        endswitch;
                        
        
        
    }
    SmartShortCode::add_shortcode('testimonial', 'testimonial_cb');

    function testimonial_1($atts, $content = null, $tag, $hook_name) {
        extract(SmartShortCode::shortcode_atts(array(
            'class' => '',
            'client' => ''
                        ), $atts));
        return '<div class="shortcode_testimonial_1 fix '.$class.'">'.SmartShortCode::do_shortcode($content,$hook_name).'<br><br><span>'.$client.'</span></div>';
    }
    SmartShortCode::add_shortcode('testimonial_1', 'testimonial_1');

    function testimonial_2($atts, $content = null, $tag, $hook_name) {
        extract(SmartShortCode::shortcode_atts(array(
            'class' => '',
            'client' => ''
                        ), $atts));
        return '<div class="shortcode_testimonial_2 fix '.$class.'">'.SmartShortCode::do_shortcode($content,$hook_name).'<br><br><span>'.$client.'</span></div>';
    }
    SmartShortCode::add_shortcode('testimonial_2', 'testimonial_2');


    function testimonial_3($atts, $content = null, $tag, $hook_name) {
        extract(SmartShortCode::shortcode_atts(array(
            'class' => '',
            'client' => '',
            'image_src' => ''
            ), $atts));
        return '<div class="shortcode_testimonial_3 fix">
    <img class="img-responsive" alt="" src="'.$image_src.'" />
    <div class="testimonial_3_body_text">'.SmartShortCode::do_shortcode($content,$hook_name).'<br><br><span>'.$client.'</span></div>
    </div>';
    }
    SmartShortCode::add_shortcode('testimonial_3', 'testimonial_3');

    function testimonial_4($atts, $content = null, $tag, $hook_name) {
        extract(SmartShortCode::shortcode_atts(array(
            'class' => '',
            'client' => '',
            'image_src' => ''
            ), $atts));
        return '<div class="shortcode_testimonial_4 bs-example-popover fix"><div class="popover top"><div class="arrow"></div><div class="popover-content">'.SmartShortCode::do_shortcode($content,$hook_name).'</div>
    </div><img alt="" src="'.$image_src.'" /><br><br><span>'.$client.'</span></div>';
    }
    SmartShortCode::add_shortcode('testimonial_4', 'testimonial_4');


    class SDS_UI_Accordion{
        
        static $accordion_contents = '', $parent_id, $counter = 0;
        
        public static function accordion_cb($atts, $content = null, $tag='',$hook_name=''){
            self::$parent_id = 'accordion-'.rand(00000,99999);
            SmartShortCode::do_shortcode($content,$hook_name);        
            $realcontent = self::$accordion_contents;
            $parentid = self::$parent_id;
            self::$parent_id = self::$accordion_contents = ''; //reset accordion content....
            self::$counter = 0;
            
            //return '<div class="accordion-container">'.$realcontent.'</div>';
            return '<div id="'.$parentid.'" class="panel-group">'.$realcontent.'</div>';
            
        }
        public static function accordion_tab_cb($atts, $content = null,$tag='',$hook_name=''){
            extract(SmartShortCode::shortcode_atts(array(
                'title' => '',
                'accordion' => 'true'
            ), $atts));
            $id = 'acc-'.rand(00000,99999);
            $parent = $in = '';
            
            if($accordion === 'true'){
                $parent = 'data-parent="#'.self::$parent_id.'"';
                if(++self::$counter < 2)
                    $in = 'in';
            }
            
    //        self::$accordion_contents .= '<h2 class="accordion-header">'.$title.'</h2>';
    //        self::$accordion_contents .= '<div class="accordion-content">'.$content.'</div>';
            self::$accordion_contents .= '<div class="panel panel-default"><div class="panel-heading"><h4 class="panel-title"><a class="collapsed" href="#'.$id.'" '.$parent.' data-toggle="collapse">'.$title.'</a></h2></div>';
            self::$accordion_contents .= '<div id="'.$id.'" class="panel-collapse collapse '.$in.'"><div class="panel-body">'.$content.'</div></div></div>';
            
        }
        
    }


    SmartShortCode::add_shortcode('accordion', array('SDS_UI_Accordion','accordion_cb'));
    SmartShortCode::add_shortcode('atab', array('SDS_UI_Accordion','accordion_tab_cb'));
    /*
     * Spoiler
     */
    SmartShortCode::add_shortcode('spoiler', array('SDS_UI_Accordion','accordion_cb'));

    /*
     * grid
     */


    function sds_grid_row_cb($atts = array(),$content = null, $tag, $hook_name){
        
        
        return "<div class='row'>".SmartShortCode::do_shortcode($content,$hook_name)."</div>";
        
    }
    SmartShortCode::add_shortcode('gridrow', 'sds_grid_row_cb');

    function sds_grid_cb($atts = array(),$content = null, $tag, $hook_name){
        extract(SmartShortCode::shortcode_atts(array(
            'class' => 'col-lg-1 col-md-1 col-sm-1 col-xs-1',
            
        ), $atts));
        
        return "<div class='{$class}'>".SmartShortCode::do_shortcode($content,$hook_name)."</div>";
        
    }
    SmartShortCode::add_shortcode('grid', 'sds_grid_cb');
    /*
     * Video
     */

    function sds_video_cb($atts = array(),$content = null, $tag, $hook_name){
        extract(SmartShortCode::shortcode_atts(array(
            'src' => '',
            'width' => '',
            'height' => '',        
        ), $atts));
        
        return "<iframe src='{$src}' width='{$width}' height='{$height}'></iframe>";
        
    }

    SmartShortCode::add_shortcode('sds_video', 'sds_video_cb');
    /*
     * sds gallery
     */
    function sds_gallery_wrap_cb($atts = array(), $content = null, $tag, $hook_name){
        extract(SmartShortCode::shortcode_atts(array(
            'id' => '',            
        ), $atts));
        
        $html = '<div id="'.$id.'" class="sds_gallery_group">';
        $html .= SmartShortCode::do_shortcode($content,$hook_name);
        $html .= '</div>';
        
        ob_start();
        ?>
        <script type="text/javascript">
            $('#<?php echo $id?>').magnificPopup({
                delegate: 'a', // child items selector, by clicking on it popup will open
                type: 'image',
                gallery: {
                        enabled: true,
                        navigateByImgClick: true,
                        preload: [0,1] // Will preload 0 - before current, and 1 after the current image
                },
                // other options
            });
        </script>
        <?php
        $html .= ob_get_clean();
        return $html;
    }

    SmartShortCode::add_shortcode('sds_gallery_wrap', 'sds_gallery_wrap_cb');

    function sds_gallery_cb($atts = array(),$content = null, $tag, $hook_name){
        extract(SmartShortCode::shortcode_atts(array(
            'src' => '',
            'width' => '',
            'height' => '',
            'title' => '',
            'relation' => '',
        ), $atts));
        $wrs = $wre = $html = '';
        if($relation == 'false'){
            $wrs .= "<a class='sds_image_gallery_single' href='{$src}'>";
            $wre .= '</a>';
        }else{
            $wrs .= "<a href='{$src}'>";
            $wre .= '</a>';
        }
        $html .= "<img src='{$src}' ";
        if(!empty($width) && is_numeric($width))
            $html .= "width='{$width}' ";
        if(!empty($height) && is_numeric($height))
            $html .= "height='{$height}' ";
        $html .= "alt='{$title}' />";
        
        if(!empty($title)){
            $html .= "<span class='gallery_title'>{$title}</span>";
        }
        
        return $wrs.$html.$wre;
        
    }

    SmartShortCode::add_shortcode('sds_gallery', 'sds_gallery_cb');

    /*
     * bx slider
     */

    function sds_slider_cb($atts = array(),$content = null, $tag, $hook_name){
        extract(SmartShortCode::shortcode_atts(array(
            'speed' => '500',
            'width' => '',
            'mode' => 'horizontal',
            'responsive' => 'true',
            'auto' => 'true',   
            'loop' => 'true',        
            'pager' => 'false',        
            'controls' => 'true',
            'autoHover' => 'true',       
        ), $atts));
        
        $gallery_id = 'bx_slider_'.rand(00000,99999);
        $width = (int) $width;
        $return = "<div id='{$gallery_id}' class='sds_gallery'><ul>".SmartShortCode::do_shortcode($content,$hook_name)."</ul></div>";
        ob_start();
        ?>
        <script type="text/javascript">
            
            jQuery(function($){
                $(window).load(function(){
                    var width = parseInt('<?php echo $width?>');
                    var speed = parseInt('<?php echo $speed?>');
                    if($.fn.bxSlider !== undefined){
                        $('#<?php echo $gallery_id?> > ul').bxSlider({
                            mode : '<?php echo $mode?>',                    
                            autoHover : <?php echo $autoHover?>,
                            controls : <?php echo $controls?>,
                            infiniteLoop : <?php echo $loop?>,
                            responsive : <?php echo $responsive?>,
                            speed : speed,
                            <?php if($width != ''){?>
                            slideWidth : width,
                            <?php } ?>
                            pager : <?php echo $pager?>,
                            auto : <?php echo $auto?>,
                            adaptiveHeight: true,
                            useCSS : false,
                        });

                    }
                    <?php if($width != ''){?>
                
                    $('#<?php echo $gallery_id?>').css({width:width+'px'});
                    <?php } ?>
                });
                
            });
        </script>
        <?php
        $return .= ob_get_clean();
        
        return $return;
        
    }
    SmartShortCode::add_shortcode('sds_slider', 'sds_slider_cb');


    function testimonial_slider_cb($atts = array(),$content = null, $tag, $hook_name){
        extract(SmartShortCode::shortcode_atts(array(
            'speed' => '500',
            'width' => '',
            'mode' => 'vertical',
            'responsive' => 'true',
            'auto' => 'true',        
            'loop' => 'true',        
            'controls' => 'true',
            'autoHover' => 'true',        
        ), $atts));
        
        $gallery_id = 'bx_slider_'.rand(00000,99999);
        
        $return = "<div class='testimonial_slider'><ul id='{$gallery_id}'>".SmartShortCode::do_shortcode($content,$hook_name)."</ul></div>";
        ob_start();
        ?>
        <script type="text/javascript">        
            jQuery(function($){            
                 $(window).load(function(){
                    var width = parseInt('<?php echo $width?>');
                    var speed = parseInt('<?php echo $speed?>');
                    
                    if($.fn.bxSlider !== undefined){
                        $('#<?php echo $gallery_id?>').bxSlider({
                            mode : '<?php echo $mode?>',                                        
                            autoHover : <?php echo $autoHover?>,
                            controls : <?php echo $controls?>,
                            infiniteLoop : <?php echo $loop?>,
                            responsive : <?php echo $responsive?>,
                            speed : speed,
                            <?php if($width != ''){?>
                            slideWidth : width,
                            <?php } ?>
                            pager : false,
                            auto : <?php echo $auto?>,
                            adaptiveHeight: true,
                            useCSS : false,
                            infiniteLoop: true,
                            slideMargin: 20,
                            onSliderLoad: function () {
    				            $('#<?php echo $gallery_id?>').parents('.bx-wrapper').find('.bx-controls-direction').hide();
    				            $('#<?php echo $gallery_id?>').parents('.bx-wrapper').hover(
    				            function () { $('#<?php echo $gallery_id?>').parents('.bx-wrapper').find('.bx-controls-direction').fadeIn(300); },
    				            function () { $('#<?php echo $gallery_id?>').parents('.bx-wrapper').find('.bx-controls-direction').fadeOut(300); }
    				            );
    			            }
                        });

                    }
                });
            });
        </script>
        <?php
        $return .= ob_get_clean();
        
        return $return;
        
    }
    SmartShortCode::add_shortcode('testimonial_slider', 'testimonial_slider_cb');

    function sds_tslide_cb($atts = array(),$content = null, $tag, $hook_name){
        extract(SmartShortCode::shortcode_atts(array(        
            'author' => '',             
        ), $atts));
        $return = "<li>";
        
        ob_start();
        ?>
        <div class="quote_bottom_content">        
            <p><?php echo $content?></p>
            <p class="quote_author"><?php echo $author;?></p>
        </div>
        <?php    
        $return .= ob_get_clean();    
        $return .= '</li>';
        
        return $return;
        
    }
    SmartShortCode::add_shortcode('tslide', 'sds_tslide_cb');

    function sds_bx_slide_cb($atts = array(),$content = null, $tag, $hook_name){
        extract(SmartShortCode::shortcode_atts(array(
            'src' => '',
            'title' => '',             
        ), $atts));
        $return = "<li class='homeslider-container'>";
        if(!empty($src))
        $return .= "<img src='{$src}' alt='sds_slide' />";
        $return .= "<div class='homeslider-description'>";
        if(!empty($title)){
           $return .= "<h2>{$title}</h2>"; 
        }
        if(!empty($content)){    
           $return .= "<p>$content</p>"; 
        }
        
        $return .= "</div></li>";
        
        return $return;
        
    }
    SmartShortCode::add_shortcode('bx_slide', 'sds_bx_slide_cb');

    // features columns

    class SDS_Features{
        
        static function sds_features_wrap_cb($atts = array(), $content = null, $tag, $hook_name){
            return '<div class="row clearfix sds_features_wrap">'.SmartShortCode::do_shortcode($content,$hook_name).'</div>';        
        }
        static function sds_feature_cb($atts = array(), $content = null, $tag, $hook_name){
            extract(SmartShortCode::shortcode_atts(array(
                'col' => '4',
                'iconsrc' => '',
                'iconclass' => '',
                'title' => '',
              ), $atts));
            $class = '';
            if(!empty($col) && is_numeric($col)){
                switch((int) $col){                    
                    case 6:
                        $class .= 'col-lg-6 col-md-6 col-sm-6 col-xs-12';
                        break;
                    case 4:
                        $class .= 'col-lg-4 col-md-4 col-sm-4 col-xs-12';
                        break;
                    case 3:
                        $class .= 'col-lg-3 col-md-3 col-sm-4 col-xs-12';
                        break;
                    default: 
                        $class .= 'col-lg-12 col-md-12 col-sm-12 col-xs-12';
                        break;
                }
            }
            
            $html = "<div class='{$class} sds_feature_col'>";
            
            if(!empty($iconclass)){            
                $html .= "<span class='sds_icon'><i class='{$iconclass}'></i></span>";            
            }elseif(!empty($iconsrc)){
                $html .= "<span class='sds_icon'><img src='{$iconsrc}' alt='{$title}' /></span>";
            }
            $html .= "<div class='sds_feature_content'>";
            $html .= "<h3>{$title}</h3>";
            $html .= "<div>".SmartShortCode::do_shortcode($content,$hook_name)."</div>";
            
            $html .= '</div></div>';
            
            return $html;
            
        }
    }

    SmartShortCode::add_shortcode('sds_features_wrap', array('SDS_Features','sds_features_wrap_cb'));
    SmartShortCode::add_shortcode('sds_feature', array('SDS_Features','sds_feature_cb'));

    class SDS_Services{
        
        static function sds_services_wrap_cb($atts = array(), $content = null, $tag, $hook_name){
            return '<div class="row clearfix sds_services_wrap">'.SmartShortCode::do_shortcode($content,$hook_name).'</div>';        
        }
        static function sds_service_cb($atts = array(), $content = null, $tag, $hook_name){
            extract(SmartShortCode::shortcode_atts(array(
                'col' => '4',
                'iconsrc' => '',
                'iconclass' => '',
                'title' => '',
              ), $atts));
            $class = '';
            if(!empty($col) && is_numeric($col)){
                switch((int) $col){                    
                    case 6:
                        $class .= 'col-lg-6 col-md-6 col-sm-6 col-xs-12';
                        break;
                    case 4:
                        $class .= 'col-lg-4 col-md-4 col-sm-4 col-xs-12';
                        break;
                    case 3:
                        $class .= 'col-lg-3 col-md-3 col-sm-4 col-xs-12';
                        break;
                    default: 
                        $class .= 'col-lg-12 col-md-12 col-sm-12 col-xs-12';
                        break;
                }
            }
            
            $html = "<div class='{$class} sds_service_col'>";
            
            if(!empty($iconclass)){            
                $html .= "<span class='sds_icon'><i class='{$iconclass}'></i></span>";            
            }elseif(!empty($iconsrc)){
                $html .= "<span class='sds_icon'><img src='{$iconsrc}' alt='{$title}' /></span>";
            }
            $html .= "<div class='sds_service_content'>";
            $html .= "<h3>{$title}</h3>";
            $html .= "<div>".SmartShortCode::do_shortcode($content,$hook_name)."</div>";
            
            $html .= '</div></div>';
            
            return $html;
            
        }
    }

    SmartShortCode::add_shortcode('sds_services_wrap', array('SDS_Services','sds_services_wrap_cb'));
    SmartShortCode::add_shortcode('sds_service', array('SDS_Services','sds_service_cb'));


    

    function smartshortcode_smart_icon($atts, $content = null, $tag, $hook_name) {
        extract(SmartShortCode::shortcode_atts(array(
                    'name' => '',
                    'class'=>'',
                    'size'=>'',
                    'style'=>''
                        ), $atts));
        return '<i class="'.$name.' '.$class.' '.$size.'" style='.$style.' >' . SmartShortCode::do_shortcode($content,$hook_name) . '
    </i>';
    }

    SmartShortCode::add_shortcode('smart_icon', 'smartshortcode_smart_icon');

    function smartshortcode_smart_social($atts, $content = null, $tag, $hook_name) {
        extract(SmartShortCode::shortcode_atts(array(
                    'name' => '',
                    'link' => '',
                    'title' => '',
                    'class'=>'',
                    'size'=>'',                
        ), $atts));
        $html = $before = $after = '';
        if(!empty($link)){
            $before = "<a class='sds_social' href='{$link}'>";
            $after = "{$title}</a>";
        }
        $html .= '<i class="'.$name.' '.$class.' '.$size.'" ></i>';
        return $before.$html.$after;
    }

    SmartShortCode::add_shortcode('smart_social', 'smartshortcode_smart_social');

    function smartshortcode_slider_cb($atts = array(), $content = null, $tag, $hook_name) {

        //jquery.flexslider.js
        extract( SmartShortCode::shortcode_atts(array(
            'id' => 'page_slider',
                        ), $atts));

    $output = "
    <script type='text/javascript'>
    $(document).ready(function(){ 
    $('#$id').flexslider({
            animation: 'slide',
            controlNav: true,              
            directionNav: false
        }); 
      }); 
    </script>
       "; 
          $output.= '<div id="' . $id . '" class="smart_shortcode_flex_container"><ul class="slides">' . SmartShortCode::do_shortcode($content,$hook_name) . '</ul></div>';
     return $output;
    }

     SmartShortCode::add_shortcode('slider', 'smartshortcode_slider_cb');

    function smartshortcode_slide_cb($atts = array(), $content = null, $tag, $hook_name) {

        extract(SmartShortCode::shortcode_atts(array(
            'imgsrc' => '',
            'imgalt' => ''
        ), $atts));

        return "<li><p class=\"flex-caption\">{$content}</p><img alt='{$imgalt}' src='{$imgsrc}' /></li>";
    }

     
     SmartShortCode::add_shortcode('slide', 'smartshortcode_slide_cb');

    function shortcode_featured_products ($atts, $content = null, $tag, $hook_name) {
      extract(SmartShortCode::shortcode_atts(array(
          'per_page' => '12',
          'orderby' => 'position',
          'order' => 'DESC',
          'random' => 'false',
        ), $atts));
        
    	$context = Context::getContext();

        $category = new Category($context->shop->getCategory(), (int)$context->language->id);

        $cache_products = $category->getProducts((int)$context->language->id, 1, $per_page, $orderby,$order, false, true, (bool)$random, $per_page);
        $ssc = new SmartShortCode();
           if(!empty($cache_products)){
               if($hook_name != 'displayLeftColumn' && $hook_name != 'displayRightColumn' && $hook_name != 'displayFooter'){

                $context->smarty->assign(
                     array(
                         'new_products' => $cache_products,
                         'sds_hook' => $hook_name,                            
                         'sds_title' => $ssc->l('New products')
                     )
                 );

 				$template_file_name = getTemplatePath('blocknewproducts.tpl');
            
 				$out_put = $context->smarty->fetch($template_file_name);
               }else{  
                   
                   $context->smarty->assign(
                        array(
                            'productsViewedObj' => $cache_products,
                            'sds_hook' => $hook_name,                            
                            'sds_title' => $ssc->l('Featured products')
                        )
                    );
                   	$template_file_name = getTemplatePath('blockviewed.tpl');
                	$out_put = $context->smarty->fetch($template_file_name);
               }
        }else{
            $out_put = $ssc->l('No featured products at this time');
        }
        
        return $out_put ;

    }

    SmartShortCode::add_shortcode('featured_products', 'shortcode_featured_products');
     
    function shortcode_bestsellers_products ($atts, $content = null, $tag, $hook_name) {
      extract(SmartShortCode::shortcode_atts(array(
          'per_page' => '12',
          'orderby' => null,
          'order' => 'DESC',
          ), $atts));
            	
    	$ssc = new SmartShortCode();
    	$context = Context::getContext();

        $cache_products = ProductSaleCore::getBestSales((int) Context::getContext()->language->id, 0, $per_page, $orderby, $order);

        if(!empty($cache_products)){
            if($hook_name != 'displayLeftColumn' && $hook_name != 'displayRightColumn' && $hook_name != 'displayFooter'){
                
                $context->smarty->assign(
                     array(
                         'new_products' => $cache_products,
                         'sds_hook' => $hook_name,                            
                         'sds_title' => $ssc->l('New products')
                     )
                 );

 				$template_file_name = getTemplatePath('blocknewproducts.tpl');
            
 				$out_put = $context->smarty->fetch($template_file_name);

            }else{
                $context->smarty->assign(
                     array(
                         'productsViewedObj' => $cache_products,
                         'sds_hook' => $hook_name,                            
                         'sds_title' => $ssc->l('Bestseller products')
                     )
                 );
                $template_file_name = getTemplatePath('blockviewed.tpl');
                $out_put = $context->smarty->fetch($template_file_name);
            } 
        }else{
            $out_put = $ssc->l('No BestSellers products at this time');
        }
             
        return  $out_put;

    }

    SmartShortCode::add_shortcode('bestsellers_products', 'shortcode_bestsellers_products');

    function shortcode_new_products ($atts, $content = null, $tag, $hook_name) {
      extract(SmartShortCode::shortcode_atts(array(
          'per_page' => '12',
          'orderby' => 'position',
          'order' => 'DESC',      
          ), $atts));
    	$context = Context::getContext();
        $ssc = new SmartShortCode();   
        if (!Configuration::get('NEW_PRODUCTS_NBR'))
                    return;
               
        $cache_products = Product::getNewProducts((int) Context::getContext()->language->id, 0, $per_page, false, $orderby, $order);
    	   if(!empty($cache_products)){
               if($hook_name != 'displayLeftColumn' && $hook_name != 'displayRightColumn' && $hook_name != 'displayFooter'){
               	 $context->smarty->assign(
                     array(
                         'new_products' => $cache_products,
                         'sds_hook' => $hook_name,                            
                         'sds_title' => $ssc->l('New products')
                     )
                 );
               	$template_file_name = getTemplatePath('blocknewproducts.tpl') ;	
 				$out_put = $context->smarty->fetch($template_file_name);
                 return   $out_put;
               }else{
                   $context->smarty->assign(
                     array(
                         'productsViewedObj' => $cache_products,
                         'sds_hook' => $hook_name,                            
                         'sds_title' => $ssc->l('New products')
                     )
                 );
 				$template_file_name = getTemplatePath('blockviewed.tpl');	
 				$out_put = $context->smarty->fetch($template_file_name);
 				return   $out_put;
               }
        }else{
            $out_put = $ssc->l('No new product is available at this time');
        }
        return $out_put ;
    }

	function getTemplatePath($template='',$module_name ='smartshortcode'){


  if (Tools::file_exists_cache(_PS_THEME_DIR_.'modules/'.$module_name.'/'.$template))
   return _PS_THEME_DIR_.'modules/'.$module_name.'/'.$template;
  elseif (Tools::file_exists_cache(_PS_THEME_DIR_.'modules/'.$module_name.'/views/templates/front/'.$template))
   return _PS_THEME_DIR_.'modules/'.$module_name.'/views/templates/front/'.$template;
  elseif (Tools::file_exists_cache(_PS_MODULE_DIR_.$module_name.'/views/templates/front/'.$template))
   return _PS_MODULE_DIR_.$module_name.'/views/templates/front/'.$template;

  return false;

}
    SmartShortCode::add_shortcode('new_products', 'shortcode_new_products');

    function shortcode_specials_products ($atts, $content = null, $tag, $hook_name) {
      extract(SmartShortCode::shortcode_atts(array(
          'per_page' => '12',
          'orderby' => 'position',
          'order' => 'DESC',
        ), $atts));
       
    	$context = Context::getContext();

    	if (!($cache_products = Product::getPricesDrop((int) Context::getContext()->language->id, 0, $per_page ,  false, $orderby, $order)))
    				return;

           $ssc = new SmartShortCode();
    	  if(!empty($cache_products)){
    	   
    		$link = new Link(); 
                
                if($hook_name != 'displayLeftColumn' && $hook_name != 'displayRightColumn' && $hook_name != 'displayFooter'){
                    $context->smarty->assign(
	                     array(
	                         'new_products' => $cache_products,
	                         'sds_hook' => $hook_name,                            
	                         'sds_title' => $ssc->l('New products')
	                     )
	                );
	               	$template_file_name = getTemplatePath('blocknewproducts.tpl');
	 				$out_put = $context->smarty->fetch($template_file_name);
	                 return $out_put;
                }else{
                    $context->smarty->assign(
                        array(
                            'specials' => $cache_products,
                            'sds_hook' => $hook_name
                        )
                    );
                    $template_file_name = getTemplatePath('blockspecials.tpl');	
	 				$out_put = $context->smarty->fetch($template_file_name);
                }
        }else{
            $out_put = $ssc->l('No specials products at this time.');
        }
             
        return  $out_put;

    }

    SmartShortCode::add_shortcode('specials_products', 'shortcode_specials_products');

    function shortcode_category_products ($atts, $content = null, $tag, $hook_name) {
        
      extract(SmartShortCode::shortcode_atts(array(
          'id_category' => '1',
          'per_page' => '12',
          'orderby' => 'position',
          'order' => 'DESC',
        ), $atts));
        
    	$context = Context::getContext(); 
        $ssc = new SmartShortCode();
    	$out_put = '';
        $cache_products = SmartShortCode::getProductsByCategoryID($id_category,$per_page, $context->language->id, null, false, $orderby ,$order);    		   
    	  if(is_array($cache_products) && !empty($cache_products)){
                if($hook_name != 'displayLeftColumn' && $hook_name != 'displayRightColumn' && $hook_name != 'displayFooter'){
                    $context->smarty->assign(
                    array(
                         'new_products' => $cache_products,
                         'sds_hook' => $hook_name,                            
                         'sds_title' => $ssc->l('New products')
	                     )
	                );
		 			$template_file_name = getTemplatePath('blocknewproducts.tpl');
		 			$out_put = $context->smarty->fetch($template_file_name);
                }else{
                    $thecats = Category::getCategoryInformations(array($id_category));
                    $sds_title = "";
                    if(isset($thecats[(int)$id_category])){
                        $sds_title .= $thecats[(int)$id_category]['name'].' ';
                    }
                    $sds_title .= $ssc->l('products');
                    $context->smarty->assign(
                        array(
                            'productsViewedObj' => $cache_products,
                            'sds_hook' => $hook_name,                            
                            'sds_title' => $sds_title
                        )
                    );
                    $template_file_name = getTemplatePath('blockviewed.tpl');
		 			$out_put = $context->smarty->fetch($template_file_name);
                }
        }else{
            $out_put = $ssc->l('No products at this Category.');
        }
          return  $out_put ;
    }
    SmartShortCode::add_shortcode('category_products', 'shortcode_category_products');

    /*
     * Smart Blog
     */	
    SmartShortCode::add_shortcode('smartblog', 'sds_smartblog_cb');
    
    
    function sds_smartblog_cb($atts = array(),$content = null, $tag, $hook_name){
        
        if(!class_exists('SmartBlogPost')) return false;
        extract(SmartShortCode::shortcode_atts(array(
          'blog_cat' => '',
          'per_page' => '12',
          'pcats' => '',
          'products' => '',
          'title' => '',            
        ), $atts));
        
        
        $result = array();
        $context = Context::getContext();
        $pcatsarr = explode(',',$pcats);
        $productsarr = explode(',',$products);
        $html = '';
        if(!empty($pcats) && $context->controller->php_self == 'category' && !in_array(Tools::getvalue('id_category'),$pcatsarr))
                return false;        
        elseif(!empty($products) && $context->controller->php_self == 'product' && !in_array(Tools::getvalue('id_product'),$productsarr))
                return false;

        if(!empty($blog_cat) && is_numeric($blog_cat)){
            $posts = BlogPostCategory::getToltalByCategory($context->language->id,$blog_cat,0,$per_page);
        }else{
            $posts = SmartBlogPost::getAllPost($context->language->id,0,$per_page);     
        }
        //start manupulate data for tpl
        $i = 0;
        if(!empty($posts))
            foreach($posts as $post){
                $result[$i]['id'] = $post['id_post'];
                $result[$i]['id_smart_blog_post'] = $post['id_post'];
                $result[$i]['id_post'] = $post['id_post'];
                if(!empty($post['is_featured'])){
                    $result[$i]['is_featured'] = $post['is_featured'];
                }else{
                    $result[$i]['is_featured'] = '';
                }
                if(!empty($post['cat_name'])){
                    $result[$i]['cat_name'] = $post['cat_name'];
                }else{
                    $result[$i]['cat_name'] = '';
                }
                $result[$i]['viewed'] = $post['viewed'];
                $result[$i]['title'] = $post['meta_title'];
                $result[$i]['meta_title'] = $post['meta_title'];
                $result[$i]['meta_description'] = $post['meta_description'];
                $result[$i]['short_description'] = $post['short_description'];
                $result[$i]['content'] = $post['content'];
                $result[$i]['meta_keyword'] = $post['meta_keyword'];
                $result[$i]['id_category'] = $post['id_category']; 
                $result[$i]['link_rewrite'] = $post['link_rewrite'];
                $result[$i]['cat_link_rewrite'] = $post['cat_link_rewrite'];
                $result[$i]['lastname'] = $post['lastname'];
                $result[$i]['firstname'] = $post['firstname'];
                $result[$i]['post_img'] = $post['post_img'];
                $result[$i]['created'] = $post['created'];
                $result[$i]['date_added'] = $post['created'];
                $i++;
            }
            //end manupulate data for tpl

        if($hook_name != 'displayLeftColumn' && $hook_name != 'displayRightColumn' && $hook_name != 'displayFooter'){
            
            $context->smarty->assign(
                array(
                    'view_data' => $result,                                          
                    'sds_title' => $title
                )
            );
            $template_file_name = getTemplatePath('smartblog_latest_news.tpl');
            $html = $context->smarty->fetch($template_file_name);
        }else{
            $context->smarty->assign(
                array(
                    'posts' => $result,                                          
                    'sds_title' => $title
                )
            );
            $template_file_name = getTemplatePath('smartblogrecentposts.tpl');
            $html = $context->smarty->fetch($template_file_name);
        }
        return $html;
    }

function sds_smartcomment_cb($atts = array(),$content=null,$tag,$hook_name){
    if(!class_exists('Blogcomment')) return false;
    
    extract(SmartShortCode::shortcode_atts(array(          
        'limit' => '5',                      
      ), $atts));
    
    $context = Context::getContext();
    $comments = SmartShortCode::getLatestComments($context->language->id,$limit);
    
    
    $context->smarty->assign(
        array(
            'latesComments' => $comments,            
        )
    );

    $template_file_name = getTemplatePath('smartbloglatestcomments.tpl');
    $html = $context->smarty->fetch($template_file_name);
    return $html;
}    
    
SmartShortCode::add_shortcode('smartcomment', 'sds_smartcomment_cb');