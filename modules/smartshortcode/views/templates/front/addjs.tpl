<script type="text/javascript">
    
if( typeof tinymce != 'undefined')
tinymce.PluginManager.load('shortcode', '{$smartmodules_dir}modules/smartshortcode/plugins/shortcode/plugin.min.js');


</script>
<style>
	.icon-Adminsmartshortcode{
		font-family: FontAwesome;
		font-weight: normal;
		font-style: normal;
		text-decoration: inherit;
		-webkit-font-smoothing: antialiased;
		display: inline;
		width: auto;
		height: auto;
		line-height: normal;
		vertical-align: baseline;
		background-image: none;
		background-position: 0% 0%;
		background-repeat: repeat;
		margin-top: 0;
	}
	.icon-Adminsmartshortcode:before{
		content: "\f160";
		text-decoration: inherit;
		display: inline-block;
		speak: none;
	}
</style>