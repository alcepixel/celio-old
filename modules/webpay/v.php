<?php
/****************************************************************/
// Modulo:	Validacion webpay para Prestashop
// Versi�n: 3.0
/****************************************************************/

include_once(dirname(__FILE__).'/../../config/config.inc.php');
include_once(dirname(__FILE__).'../../init.php');
include_once(dirname(__FILE__).'/webpay.php');

echo getcwd();
 // creacion de estructura
$sql = "CREATE TABLE IF NOT EXISTS `webpay` (  `Tbk_tipo_transaccion` varchar(200) NOT NULL,
  `Tbk_respuesta` varchar(200) NOT NULL,  `Tbk_orden_compra` varchar(200) NOT NULL,
  `Tbk_id_sesion` varchar(200) NOT NULL,  `Tbk_codigo_autorizacion` varchar(200) NOT NULL,
  `Tbk_monto` varchar(200) NOT NULL,  `Tbk_numero_tarjeta` varchar(200) NOT NULL,
  `Tbk_numero_final_tarjeta` varchar(200) NOT NULL,  `Tbk_fecha_expiracion` date NOT NULL,
  `Tbk_fecha_contable` date NOT NULL,  `Tbk_fecha_transaccion` varchar(200) NOT NULL,
  `Tbk_hora_transaccion` varchar(200) NOT NULL,  `Tbk_id_transaccion` varchar(200) NOT NULL,
  `Tbk_tipo_pago` varchar(200) NOT NULL,  `Tbk_numero_cuotas` varchar(200) NOT NULL,
  `Tbk_mac` varchar(200) NOT NULL,  `Tbk_monto_cuota` varchar(200) NOT NULL,
  `Tbk_tasa_interes_max` varchar(200) NOT NULL,  `Tbk_ip` varchar(200) NOT NULL,
  UNIQUE KEY `Tbk_tipo_transaccion` (`Tbk_tipo_transaccion`,`Tbk_respuesta`,`Tbk_orden_compra`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;";
$webpay->ejecuta_query($sql);

$sql = "CREATE TABLE `webpay_orden` (
             id MEDIUMINT NOT NULL AUTO_INCREMENT,
             `monto` int(11),
  						`estado` int(11),
             PRIMARY KEY (id)
             );";
$webpay->ejecuta_query($sql);

?>