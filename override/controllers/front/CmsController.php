<?php







require_once(_PS_MODULE_DIR_ . 'smartshortcode/smartshortcode.php');





class CmsController extends CmsControllerCore

{





            

function __construct()

  {

    parent::__construct();

  
    
  }

  function init()

  {

    parent::init();

   
  

  }

  

  /*
	* module: smartshortcode
	* date: 2015-05-19 16:21:13
	* version: 2.1
	*/
	public function initContent()

	{

		parent::initContent();



		$parent_cat = new CMSCategory(1, $this->context->language->id);

		$this->context->smarty->assign('id_current_lang', $this->context->language->id);

		$this->context->smarty->assign('home_title', $parent_cat->name);

		$this->context->smarty->assign('cgv_id', Configuration::get('PS_CONDITIONS_CMS_ID'));

		if (isset($this->cms->id_cms_category) && $this->cms->id_cms_category)

			$path = Tools::getFullPath($this->cms->id_cms_category, $this->cms->meta_title, 'CMS');

		else if (isset($this->cms_category->meta_title))

			$path = Tools::getFullPath(1, $this->cms_category->meta_title, 'CMS');

		if ($this->assignCase == 1)

		{

			  

           if(Module::isEnabled('smartshortcode') != 1){

				$smartshortcode = new SmartShortCode();

			 $this->cms->content = $this->cms->content;

			  }

			  else

			  {

			   $smartshortcode = new SmartShortCode();

				$this->cms->content = $smartshortcode->parse( $this->cms->content );

			  }

                  

				  

			$this->context->smarty->assign(array(

				'cms' => $this->cms,

				'content_only' => (int)(Tools::getValue('content_only')),

				'path' => $path

			));

		}

		else if ($this->assignCase == 2)

		{

			$this->context->smarty->assign(array(

				'category' => $this->cms_category, 
				'cms_category' => $this->cms_category,

				'sub_category' => $this->cms_category->getSubCategories($this->context->language->id),

				'cms_pages' => CMS::getCMSPages($this->context->language->id, (int)($this->cms_category->id) ),

				'path' => ($this->cms_category->id !== 1) ? Tools::getPath($this->cms_category->id, $this->cms_category->name, false, 'CMS') : '',

			));

		}



		$this->setTemplate(_PS_THEME_DIR_.'cms.tpl');

	}

	





}