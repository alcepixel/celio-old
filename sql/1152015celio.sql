-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.16 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table celiodev.ps_access
DROP TABLE IF EXISTS `ps_access`;
CREATE TABLE IF NOT EXISTS `ps_access` (
  `id_profile` int(10) unsigned NOT NULL,
  `id_tab` int(10) unsigned NOT NULL,
  `view` int(11) NOT NULL,
  `add` int(11) NOT NULL,
  `edit` int(11) NOT NULL,
  `delete` int(11) NOT NULL,
  PRIMARY KEY (`id_profile`,`id_tab`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_access: ~439 rows (approximately)
/*!40000 ALTER TABLE `ps_access` DISABLE KEYS */;
INSERT INTO `ps_access` (`id_profile`, `id_tab`, `view`, `add`, `edit`, `delete`) VALUES
	(1, 0, 1, 1, 1, 1),
	(1, 1, 1, 1, 1, 1),
	(1, 5, 1, 1, 1, 1),
	(1, 7, 1, 1, 1, 1),
	(1, 9, 1, 1, 1, 1),
	(1, 10, 1, 1, 1, 1),
	(1, 11, 1, 1, 1, 1),
	(1, 13, 1, 1, 1, 1),
	(1, 14, 1, 1, 1, 1),
	(1, 15, 1, 1, 1, 1),
	(1, 16, 1, 1, 1, 1),
	(1, 19, 1, 1, 1, 1),
	(1, 20, 1, 1, 1, 1),
	(1, 21, 1, 1, 1, 1),
	(1, 22, 1, 1, 1, 1),
	(1, 23, 1, 1, 1, 1),
	(1, 24, 1, 1, 1, 1),
	(1, 25, 1, 1, 1, 1),
	(1, 26, 1, 1, 1, 1),
	(1, 27, 1, 1, 1, 1),
	(1, 28, 1, 1, 1, 1),
	(1, 29, 1, 1, 1, 1),
	(1, 31, 1, 1, 1, 1),
	(1, 32, 1, 1, 1, 1),
	(1, 33, 1, 1, 1, 1),
	(1, 34, 1, 1, 1, 1),
	(1, 35, 1, 1, 1, 1),
	(1, 36, 1, 1, 1, 1),
	(1, 38, 1, 1, 1, 1),
	(1, 39, 1, 1, 1, 1),
	(1, 40, 1, 1, 1, 1),
	(1, 41, 1, 1, 1, 1),
	(1, 42, 1, 1, 1, 1),
	(1, 44, 1, 1, 1, 1),
	(1, 45, 1, 1, 1, 1),
	(1, 48, 1, 1, 1, 1),
	(1, 50, 1, 1, 1, 1),
	(1, 52, 1, 1, 1, 1),
	(1, 53, 1, 1, 1, 1),
	(1, 54, 1, 1, 1, 1),
	(1, 55, 1, 1, 1, 1),
	(1, 56, 1, 1, 1, 1),
	(1, 57, 1, 1, 1, 1),
	(1, 58, 1, 1, 1, 1),
	(1, 59, 1, 1, 1, 1),
	(1, 61, 1, 1, 1, 1),
	(1, 62, 1, 1, 1, 1),
	(1, 63, 1, 1, 1, 1),
	(1, 66, 1, 1, 1, 1),
	(1, 67, 1, 1, 1, 1),
	(1, 68, 1, 1, 1, 1),
	(1, 69, 1, 1, 1, 1),
	(1, 70, 1, 1, 1, 1),
	(1, 71, 1, 1, 1, 1),
	(1, 73, 1, 1, 1, 1),
	(1, 74, 1, 1, 1, 1),
	(1, 75, 1, 1, 1, 1),
	(1, 76, 1, 1, 1, 1),
	(1, 77, 1, 1, 1, 1),
	(1, 78, 1, 1, 1, 1),
	(1, 80, 1, 1, 1, 1),
	(1, 81, 1, 1, 1, 1),
	(1, 82, 1, 1, 1, 1),
	(1, 83, 1, 1, 1, 1),
	(1, 84, 1, 1, 1, 1),
	(1, 86, 1, 1, 1, 1),
	(1, 87, 1, 1, 1, 1),
	(1, 88, 1, 1, 1, 1),
	(1, 89, 1, 1, 1, 1),
	(1, 92, 1, 1, 1, 1),
	(1, 93, 1, 1, 1, 1),
	(1, 94, 1, 1, 1, 1),
	(1, 95, 1, 1, 1, 1),
	(1, 96, 1, 1, 1, 1),
	(1, 99, 1, 1, 1, 1),
	(1, 100, 1, 1, 1, 1),
	(1, 101, 1, 1, 1, 1),
	(1, 102, 1, 1, 1, 1),
	(1, 103, 1, 1, 1, 1),
	(1, 104, 1, 1, 1, 1),
	(1, 105, 1, 1, 1, 1),
	(1, 106, 1, 1, 1, 1),
	(1, 107, 1, 1, 1, 1),
	(1, 108, 1, 1, 1, 1),
	(1, 109, 1, 1, 1, 1),
	(1, 110, 1, 1, 1, 1),
	(1, 111, 1, 1, 1, 1),
	(1, 112, 1, 1, 1, 1),
	(1, 113, 1, 1, 1, 1),
	(1, 114, 1, 1, 1, 1),
	(1, 115, 1, 1, 1, 1),
	(2, 0, 1, 1, 1, 1),
	(2, 1, 0, 0, 0, 0),
	(2, 2, 0, 0, 0, 0),
	(2, 3, 0, 0, 0, 0),
	(2, 4, 0, 0, 0, 0),
	(2, 5, 0, 0, 0, 0),
	(2, 6, 0, 0, 0, 0),
	(2, 7, 0, 0, 0, 0),
	(2, 8, 0, 0, 0, 0),
	(2, 9, 1, 1, 1, 1),
	(2, 10, 1, 1, 1, 1),
	(2, 11, 1, 1, 1, 1),
	(2, 12, 0, 0, 0, 0),
	(2, 13, 1, 0, 1, 0),
	(2, 14, 1, 1, 1, 1),
	(2, 15, 0, 0, 0, 0),
	(2, 16, 0, 0, 0, 0),
	(2, 17, 0, 0, 0, 0),
	(2, 18, 0, 0, 0, 0),
	(2, 19, 0, 0, 0, 0),
	(2, 20, 1, 1, 1, 1),
	(2, 21, 1, 1, 1, 1),
	(2, 22, 1, 1, 1, 1),
	(2, 23, 1, 1, 1, 1),
	(2, 24, 0, 0, 0, 0),
	(2, 25, 0, 0, 0, 0),
	(2, 26, 0, 0, 0, 0),
	(2, 27, 1, 1, 1, 1),
	(2, 28, 0, 0, 0, 0),
	(2, 29, 0, 0, 0, 0),
	(2, 30, 1, 1, 1, 1),
	(2, 31, 1, 1, 1, 1),
	(2, 32, 1, 1, 1, 1),
	(2, 33, 1, 1, 1, 1),
	(2, 34, 1, 1, 1, 1),
	(2, 35, 1, 1, 1, 1),
	(2, 36, 0, 0, 0, 0),
	(2, 37, 1, 1, 1, 1),
	(2, 38, 1, 1, 1, 1),
	(2, 39, 0, 0, 0, 0),
	(2, 40, 0, 0, 0, 0),
	(2, 41, 0, 0, 0, 0),
	(2, 42, 0, 0, 0, 0),
	(2, 43, 0, 0, 0, 0),
	(2, 44, 0, 0, 0, 0),
	(2, 45, 0, 0, 0, 0),
	(2, 46, 0, 0, 0, 0),
	(2, 47, 0, 0, 0, 0),
	(2, 48, 1, 1, 1, 1),
	(2, 49, 1, 1, 1, 1),
	(2, 50, 0, 0, 0, 0),
	(2, 51, 0, 0, 0, 0),
	(2, 52, 0, 0, 0, 0),
	(2, 53, 0, 0, 0, 0),
	(2, 54, 0, 0, 0, 0),
	(2, 55, 0, 0, 0, 0),
	(2, 56, 0, 0, 0, 0),
	(2, 57, 0, 0, 0, 0),
	(2, 58, 0, 0, 0, 0),
	(2, 59, 0, 0, 0, 0),
	(2, 60, 1, 0, 1, 0),
	(2, 61, 0, 0, 0, 0),
	(2, 62, 0, 0, 0, 0),
	(2, 63, 0, 0, 0, 0),
	(2, 64, 0, 0, 0, 0),
	(2, 65, 0, 0, 0, 0),
	(2, 66, 0, 0, 0, 0),
	(2, 67, 0, 0, 0, 0),
	(2, 68, 0, 0, 0, 0),
	(2, 69, 0, 0, 0, 0),
	(2, 70, 0, 0, 0, 0),
	(2, 71, 0, 0, 0, 0),
	(2, 72, 0, 0, 0, 0),
	(2, 73, 0, 0, 0, 0),
	(2, 74, 0, 0, 0, 0),
	(2, 75, 0, 0, 0, 0),
	(2, 76, 0, 0, 0, 0),
	(2, 77, 0, 0, 0, 0),
	(2, 78, 0, 0, 0, 0),
	(2, 79, 0, 0, 0, 0),
	(2, 80, 0, 0, 0, 0),
	(2, 81, 0, 0, 0, 0),
	(2, 82, 0, 0, 0, 0),
	(2, 83, 0, 0, 0, 0),
	(2, 84, 0, 0, 0, 0),
	(2, 85, 0, 0, 0, 0),
	(2, 86, 0, 0, 0, 0),
	(2, 87, 0, 0, 0, 0),
	(2, 88, 0, 0, 0, 0),
	(2, 89, 0, 0, 0, 0),
	(2, 90, 0, 0, 0, 0),
	(2, 91, 0, 0, 0, 0),
	(2, 92, 0, 0, 0, 0),
	(2, 93, 0, 0, 0, 0),
	(2, 94, 1, 1, 1, 1),
	(2, 95, 1, 1, 1, 1),
	(2, 96, 1, 1, 1, 1),
	(2, 97, 0, 0, 0, 0),
	(2, 98, 0, 0, 0, 0),
	(2, 99, 1, 1, 1, 1),
	(2, 100, 1, 1, 1, 1),
	(2, 101, 0, 0, 0, 0),
	(2, 102, 0, 0, 0, 0),
	(2, 103, 0, 0, 0, 0),
	(2, 104, 0, 0, 0, 0),
	(2, 105, 0, 0, 0, 0),
	(2, 106, 0, 0, 0, 0),
	(2, 107, 0, 0, 0, 0),
	(2, 108, 0, 0, 0, 0),
	(2, 109, 0, 0, 0, 0),
	(2, 110, 0, 0, 0, 0),
	(2, 111, 0, 0, 0, 0),
	(2, 112, 0, 0, 0, 0),
	(2, 113, 0, 0, 0, 0),
	(2, 114, 0, 0, 0, 0),
	(2, 115, 0, 0, 0, 0),
	(3, 0, 1, 1, 1, 1),
	(3, 1, 0, 0, 0, 0),
	(3, 2, 0, 0, 0, 0),
	(3, 3, 0, 0, 0, 0),
	(3, 4, 0, 0, 0, 0),
	(3, 5, 1, 0, 0, 0),
	(3, 6, 0, 0, 0, 0),
	(3, 7, 0, 0, 0, 0),
	(3, 8, 0, 0, 0, 0),
	(3, 9, 1, 1, 1, 1),
	(3, 10, 0, 0, 0, 0),
	(3, 11, 0, 0, 0, 0),
	(3, 12, 0, 0, 0, 0),
	(3, 13, 0, 0, 0, 0),
	(3, 14, 0, 0, 0, 0),
	(3, 15, 1, 0, 0, 0),
	(3, 16, 1, 0, 0, 0),
	(3, 17, 0, 0, 0, 0),
	(3, 18, 0, 0, 0, 0),
	(3, 19, 0, 0, 0, 0),
	(3, 20, 0, 0, 0, 0),
	(3, 21, 1, 1, 1, 1),
	(3, 22, 1, 1, 1, 1),
	(3, 23, 0, 0, 0, 0),
	(3, 24, 0, 0, 0, 0),
	(3, 25, 0, 0, 0, 0),
	(3, 26, 0, 0, 0, 0),
	(3, 27, 0, 0, 0, 0),
	(3, 28, 0, 0, 0, 0),
	(3, 29, 0, 0, 0, 0),
	(3, 30, 0, 0, 0, 0),
	(3, 31, 0, 0, 0, 0),
	(3, 32, 0, 0, 0, 0),
	(3, 33, 0, 0, 0, 0),
	(3, 34, 0, 0, 0, 0),
	(3, 35, 0, 0, 0, 0),
	(3, 36, 0, 0, 0, 0),
	(3, 37, 0, 0, 0, 0),
	(3, 38, 0, 0, 0, 0),
	(3, 39, 0, 0, 0, 0),
	(3, 40, 0, 0, 0, 0),
	(3, 41, 0, 0, 0, 0),
	(3, 42, 0, 0, 0, 0),
	(3, 43, 0, 0, 0, 0),
	(3, 44, 0, 0, 0, 0),
	(3, 45, 0, 0, 0, 0),
	(3, 46, 0, 0, 0, 0),
	(3, 47, 0, 0, 0, 0),
	(3, 48, 0, 0, 0, 0),
	(3, 49, 0, 0, 0, 0),
	(3, 50, 0, 0, 0, 0),
	(3, 51, 0, 0, 0, 0),
	(3, 52, 0, 0, 0, 0),
	(3, 53, 0, 0, 0, 0),
	(3, 54, 0, 0, 0, 0),
	(3, 55, 0, 0, 0, 0),
	(3, 56, 0, 0, 0, 0),
	(3, 57, 0, 0, 0, 0),
	(3, 58, 0, 0, 0, 0),
	(3, 59, 1, 1, 1, 1),
	(3, 60, 0, 0, 0, 0),
	(3, 61, 0, 0, 0, 0),
	(3, 62, 0, 0, 0, 0),
	(3, 63, 0, 0, 0, 0),
	(3, 64, 0, 0, 0, 0),
	(3, 65, 0, 0, 0, 0),
	(3, 66, 0, 0, 0, 0),
	(3, 67, 0, 0, 0, 0),
	(3, 68, 0, 0, 0, 0),
	(3, 69, 0, 0, 0, 0),
	(3, 70, 1, 1, 1, 1),
	(3, 71, 0, 0, 0, 0),
	(3, 72, 0, 0, 0, 0),
	(3, 73, 0, 0, 0, 0),
	(3, 74, 0, 0, 0, 0),
	(3, 75, 0, 0, 0, 0),
	(3, 76, 0, 0, 0, 0),
	(3, 77, 0, 0, 0, 0),
	(3, 78, 0, 0, 0, 0),
	(3, 79, 0, 0, 0, 0),
	(3, 80, 0, 0, 0, 0),
	(3, 81, 0, 0, 0, 0),
	(3, 82, 0, 0, 0, 0),
	(3, 83, 0, 0, 0, 0),
	(3, 84, 0, 0, 0, 0),
	(3, 85, 0, 0, 0, 0),
	(3, 86, 0, 0, 0, 0),
	(3, 87, 0, 0, 0, 0),
	(3, 88, 0, 0, 0, 0),
	(3, 89, 0, 0, 0, 0),
	(3, 90, 0, 0, 0, 0),
	(3, 91, 0, 0, 0, 0),
	(3, 92, 0, 0, 0, 0),
	(3, 93, 0, 0, 0, 0),
	(3, 94, 0, 0, 0, 0),
	(3, 95, 0, 0, 0, 0),
	(3, 96, 0, 0, 0, 0),
	(3, 97, 0, 0, 0, 0),
	(3, 98, 0, 0, 0, 0),
	(3, 99, 0, 0, 0, 0),
	(3, 100, 0, 0, 0, 0),
	(3, 101, 0, 0, 0, 0),
	(3, 102, 0, 0, 0, 0),
	(3, 103, 0, 0, 0, 0),
	(3, 104, 0, 0, 0, 0),
	(3, 105, 0, 0, 0, 0),
	(3, 106, 0, 0, 0, 0),
	(3, 107, 0, 0, 0, 0),
	(3, 108, 0, 0, 0, 0),
	(3, 109, 0, 0, 0, 0),
	(3, 110, 0, 0, 0, 0),
	(3, 111, 0, 0, 0, 0),
	(3, 112, 0, 0, 0, 0),
	(3, 113, 0, 0, 0, 0),
	(3, 114, 0, 0, 0, 0),
	(3, 115, 0, 0, 0, 0),
	(4, 0, 1, 1, 1, 1),
	(4, 1, 0, 0, 0, 0),
	(4, 2, 0, 0, 0, 0),
	(4, 3, 0, 0, 0, 0),
	(4, 4, 0, 0, 0, 0),
	(4, 5, 1, 0, 0, 0),
	(4, 6, 0, 0, 0, 0),
	(4, 7, 0, 0, 0, 0),
	(4, 8, 0, 0, 0, 0),
	(4, 9, 1, 1, 1, 1),
	(4, 10, 1, 1, 1, 1),
	(4, 11, 1, 1, 1, 1),
	(4, 12, 0, 0, 0, 0),
	(4, 13, 1, 0, 1, 0),
	(4, 14, 0, 0, 0, 0),
	(4, 15, 0, 0, 0, 0),
	(4, 16, 0, 0, 0, 0),
	(4, 17, 0, 0, 0, 0),
	(4, 18, 0, 0, 0, 0),
	(4, 19, 1, 1, 1, 1),
	(4, 20, 1, 0, 0, 0),
	(4, 21, 1, 1, 1, 1),
	(4, 22, 1, 1, 1, 1),
	(4, 23, 0, 0, 0, 0),
	(4, 24, 0, 0, 0, 0),
	(4, 25, 0, 0, 0, 0),
	(4, 26, 1, 0, 0, 0),
	(4, 27, 0, 0, 0, 0),
	(4, 28, 0, 0, 0, 0),
	(4, 29, 0, 0, 0, 0),
	(4, 30, 1, 1, 1, 1),
	(4, 31, 1, 1, 1, 1),
	(4, 32, 0, 0, 0, 0),
	(4, 33, 0, 0, 0, 0),
	(4, 34, 1, 1, 1, 1),
	(4, 35, 0, 0, 0, 0),
	(4, 36, 1, 1, 1, 1),
	(4, 37, 1, 1, 1, 1),
	(4, 38, 1, 1, 1, 1),
	(4, 39, 1, 1, 1, 1),
	(4, 40, 1, 1, 1, 1),
	(4, 41, 0, 0, 0, 0),
	(4, 42, 0, 0, 0, 0),
	(4, 43, 0, 0, 0, 0),
	(4, 44, 0, 0, 0, 0),
	(4, 45, 0, 0, 0, 0),
	(4, 46, 0, 0, 0, 0),
	(4, 47, 0, 0, 0, 0),
	(4, 48, 0, 0, 0, 0),
	(4, 49, 0, 0, 0, 0),
	(4, 50, 0, 0, 0, 0),
	(4, 51, 0, 0, 0, 0),
	(4, 52, 0, 0, 0, 0),
	(4, 53, 0, 0, 0, 0),
	(4, 54, 0, 0, 0, 0),
	(4, 55, 0, 0, 0, 0),
	(4, 56, 0, 0, 0, 0),
	(4, 57, 0, 0, 0, 0),
	(4, 58, 0, 0, 0, 0),
	(4, 59, 0, 0, 0, 0),
	(4, 60, 1, 0, 1, 0),
	(4, 61, 0, 0, 0, 0),
	(4, 62, 0, 0, 0, 0),
	(4, 63, 0, 0, 0, 0),
	(4, 64, 0, 0, 0, 0),
	(4, 65, 0, 0, 0, 0),
	(4, 66, 0, 0, 0, 0),
	(4, 67, 0, 0, 0, 0),
	(4, 68, 0, 0, 0, 0),
	(4, 69, 0, 0, 0, 0),
	(4, 70, 0, 0, 0, 0),
	(4, 71, 0, 0, 0, 0),
	(4, 72, 0, 0, 0, 0),
	(4, 73, 0, 0, 0, 0),
	(4, 74, 0, 0, 0, 0),
	(4, 75, 0, 0, 0, 0),
	(4, 76, 0, 0, 0, 0),
	(4, 77, 0, 0, 0, 0),
	(4, 78, 0, 0, 0, 0),
	(4, 79, 0, 0, 0, 0),
	(4, 80, 0, 0, 0, 0),
	(4, 81, 0, 0, 0, 0),
	(4, 82, 0, 0, 0, 0),
	(4, 83, 0, 0, 0, 0),
	(4, 84, 1, 1, 1, 1),
	(4, 85, 0, 0, 0, 0),
	(4, 86, 0, 0, 0, 0),
	(4, 87, 0, 0, 0, 0),
	(4, 88, 0, 0, 0, 0),
	(4, 89, 0, 0, 0, 0),
	(4, 90, 0, 0, 0, 0),
	(4, 91, 1, 1, 1, 1),
	(4, 92, 0, 0, 0, 0),
	(4, 93, 1, 1, 1, 1),
	(4, 94, 0, 0, 0, 0),
	(4, 95, 0, 0, 0, 0),
	(4, 96, 0, 0, 0, 0),
	(4, 97, 0, 0, 0, 0),
	(4, 98, 0, 0, 0, 0),
	(4, 99, 1, 0, 0, 0),
	(4, 100, 0, 0, 0, 0),
	(4, 101, 0, 0, 0, 0),
	(4, 102, 0, 0, 0, 0),
	(4, 103, 0, 0, 0, 0),
	(4, 104, 0, 0, 0, 0),
	(4, 105, 0, 0, 0, 0),
	(4, 106, 0, 0, 0, 0),
	(4, 107, 0, 0, 0, 0),
	(4, 108, 0, 0, 0, 0),
	(4, 109, 0, 0, 0, 0),
	(4, 110, 0, 0, 0, 0),
	(4, 111, 0, 0, 0, 0),
	(4, 112, 0, 0, 0, 0),
	(4, 113, 0, 0, 0, 0),
	(4, 114, 0, 0, 0, 0),
	(4, 115, 0, 0, 0, 0);
/*!40000 ALTER TABLE `ps_access` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_accessory
DROP TABLE IF EXISTS `ps_accessory`;
CREATE TABLE IF NOT EXISTS `ps_accessory` (
  `id_product_1` int(10) unsigned NOT NULL,
  `id_product_2` int(10) unsigned NOT NULL,
  KEY `accessory_product` (`id_product_1`,`id_product_2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_accessory: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_accessory` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_accessory` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_address
DROP TABLE IF EXISTS `ps_address`;
CREATE TABLE IF NOT EXISTS `ps_address` (
  `id_address` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_country` int(10) unsigned NOT NULL,
  `id_state` int(10) unsigned DEFAULT NULL,
  `id_customer` int(10) unsigned NOT NULL DEFAULT '0',
  `id_manufacturer` int(10) unsigned NOT NULL DEFAULT '0',
  `id_supplier` int(10) unsigned NOT NULL DEFAULT '0',
  `id_warehouse` int(10) unsigned NOT NULL DEFAULT '0',
  `alias` varchar(32) NOT NULL,
  `company` varchar(64) DEFAULT NULL,
  `lastname` varchar(32) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `address1` varchar(128) NOT NULL,
  `address2` varchar(128) DEFAULT NULL,
  `postcode` varchar(12) DEFAULT NULL,
  `city` varchar(64) NOT NULL,
  `other` text,
  `phone` varchar(32) DEFAULT NULL,
  `phone_mobile` varchar(32) DEFAULT NULL,
  `vat_number` varchar(32) DEFAULT NULL,
  `dni` varchar(16) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_address`),
  KEY `address_customer` (`id_customer`),
  KEY `id_country` (`id_country`),
  KEY `id_state` (`id_state`),
  KEY `id_manufacturer` (`id_manufacturer`),
  KEY `id_supplier` (`id_supplier`),
  KEY `id_warehouse` (`id_warehouse`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_address: ~4 rows (approximately)
/*!40000 ALTER TABLE `ps_address` DISABLE KEYS */;
INSERT INTO `ps_address` (`id_address`, `id_country`, `id_state`, `id_customer`, `id_manufacturer`, `id_supplier`, `id_warehouse`, `alias`, `company`, `lastname`, `firstname`, `address1`, `address2`, `postcode`, `city`, `other`, `phone`, `phone_mobile`, `vat_number`, `dni`, `date_add`, `date_upd`, `active`, `deleted`) VALUES
	(1, 8, 0, 1, 0, 0, 0, 'Mon adresse', 'My Company', 'DOE', 'John', '16, Main street', '2nd floor', '75002', 'Paris ', '', '0102030405', '', '', '', '2015-05-08 14:00:20', '2015-05-08 14:00:20', 1, 0),
	(2, 21, 32, 0, 0, 1, 0, 'supplier', 'Fashion', 'supplier', 'supplier', '767 Fifth Ave.', '', '10153', 'New York', '', '(212) 336-1440', '', '', '', '2015-05-08 14:00:20', '2015-05-08 14:00:20', 1, 0),
	(3, 21, 32, 0, 1, 0, 0, 'manufacturer', 'Fashion', 'manufacturer', 'manufacturer', '767 Fifth Ave.', '', '10154', 'New York', '', '(212) 336-1666', '', '', '', '2015-05-08 14:00:20', '2015-05-08 14:00:20', 1, 0),
	(4, 21, 9, 1, 0, 0, 0, 'My address', 'My Company', 'DOE', 'John', '16, Main street', '2nd floor', '33133', 'Miami', '', '0102030405', '', '', '', '2015-05-08 14:00:20', '2015-05-08 14:00:20', 1, 0);
/*!40000 ALTER TABLE `ps_address` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_address_format
DROP TABLE IF EXISTS `ps_address_format`;
CREATE TABLE IF NOT EXISTS `ps_address_format` (
  `id_country` int(10) unsigned NOT NULL,
  `format` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_country`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_address_format: ~244 rows (approximately)
/*!40000 ALTER TABLE `ps_address_format` DISABLE KEYS */;
INSERT INTO `ps_address_format` (`id_country`, `format`) VALUES
	(1, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(2, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(3, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(4, 'firstname lastname\ncompany\naddress1\naddress2\ncity State:name postcode\nCountry:name\nphone\nphone_mobile'),
	(5, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(6, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(7, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(8, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(9, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(10, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nState:name\nCountry:name\nphone\nphone_mobile'),
	(11, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nState:name\nCountry:name\nphone\nphone_mobile'),
	(12, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(13, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(14, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(15, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(16, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(17, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\ncity\npostcode\nCountry:name\nphone\nphone_mobile'),
	(18, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(19, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(20, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(21, 'firstname lastname\ncompany\naddress1 address2\ncity, State:name postcode\nCountry:name\nphone\nphone_mobile'),
	(22, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(23, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(24, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(25, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(26, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(27, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(28, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(29, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(30, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(31, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(32, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(33, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(34, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(35, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(36, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(37, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(38, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(39, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(40, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(41, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(42, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(43, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(44, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nState:name\nCountry:name\nphone\nphone_mobile'),
	(45, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(46, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(47, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(48, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(49, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(50, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(51, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(52, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(53, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(54, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(55, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(56, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(57, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(58, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(59, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(60, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(61, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(62, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(63, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(64, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(65, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(66, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(67, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(68, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(69, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(70, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(71, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(72, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(73, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(74, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(75, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(76, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(77, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(78, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(79, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(80, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(81, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(82, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(83, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(84, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(85, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(86, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(87, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(88, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(89, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(90, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(91, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(92, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(93, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(94, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(95, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(96, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(97, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(98, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(99, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(100, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(101, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(102, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(103, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(104, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(105, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(106, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(107, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(108, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(109, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(110, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(111, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nState:name\nCountry:name\nphone\nphone_mobile'),
	(112, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(113, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(114, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(115, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(116, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(117, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(118, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(119, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(120, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(121, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(122, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(123, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(124, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(125, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(126, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(127, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(128, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(129, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(130, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(131, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(132, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(133, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(134, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(135, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(136, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(137, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(138, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(139, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(140, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(141, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(142, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(143, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(144, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(145, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nState:name\nCountry:name\nphone\nphone_mobile'),
	(146, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(147, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(148, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(149, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(150, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(151, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(152, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(153, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(154, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(155, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(156, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(157, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(158, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(159, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(160, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(161, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(162, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(163, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(164, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(165, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(166, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(167, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(168, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(169, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(170, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(171, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(172, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(173, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(174, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(175, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(176, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(177, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(178, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(179, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(180, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(181, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(182, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(183, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(184, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(185, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(186, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(187, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(188, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(189, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(190, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(191, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(192, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(193, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(194, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(195, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(196, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(197, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(198, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(199, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(200, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(201, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(202, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(203, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(204, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(205, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(206, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(207, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(208, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(209, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(210, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(211, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(212, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(213, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(214, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(215, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(216, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(217, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(218, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(219, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(220, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(221, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(222, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(223, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(224, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(225, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(226, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(227, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(228, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(229, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(230, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(231, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(232, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(233, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(234, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(235, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(236, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(237, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(238, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(239, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(240, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(241, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(242, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(243, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
	(244, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile');
/*!40000 ALTER TABLE `ps_address_format` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_advice
DROP TABLE IF EXISTS `ps_advice`;
CREATE TABLE IF NOT EXISTS `ps_advice` (
  `id_advice` int(11) NOT NULL AUTO_INCREMENT,
  `id_ps_advice` int(11) NOT NULL,
  `id_tab` int(11) NOT NULL,
  `ids_tab` text,
  `validated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `hide` tinyint(1) NOT NULL DEFAULT '0',
  `location` enum('after','before') NOT NULL,
  `selector` varchar(255) DEFAULT NULL,
  `start_day` int(11) NOT NULL DEFAULT '0',
  `stop_day` int(11) NOT NULL DEFAULT '0',
  `weight` int(11) DEFAULT '1',
  PRIMARY KEY (`id_advice`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_advice: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_advice` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_advice` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_advice_lang
DROP TABLE IF EXISTS `ps_advice_lang`;
CREATE TABLE IF NOT EXISTS `ps_advice_lang` (
  `id_advice` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `html` text,
  PRIMARY KEY (`id_advice`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_advice_lang: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_advice_lang` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_advice_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_alias
DROP TABLE IF EXISTS `ps_alias`;
CREATE TABLE IF NOT EXISTS `ps_alias` (
  `id_alias` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `alias` varchar(255) NOT NULL,
  `search` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_alias`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_alias: ~2 rows (approximately)
/*!40000 ALTER TABLE `ps_alias` DISABLE KEYS */;
INSERT INTO `ps_alias` (`id_alias`, `alias`, `search`, `active`) VALUES
	(1, 'bloose', 'blouse', 1),
	(2, 'blues', 'blouse', 1);
/*!40000 ALTER TABLE `ps_alias` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_attachment
DROP TABLE IF EXISTS `ps_attachment`;
CREATE TABLE IF NOT EXISTS `ps_attachment` (
  `id_attachment` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file` varchar(40) NOT NULL,
  `file_name` varchar(128) NOT NULL,
  `file_size` bigint(10) unsigned NOT NULL DEFAULT '0',
  `mime` varchar(128) NOT NULL,
  PRIMARY KEY (`id_attachment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_attachment: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_attachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_attachment` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_attachment_lang
DROP TABLE IF EXISTS `ps_attachment_lang`;
CREATE TABLE IF NOT EXISTS `ps_attachment_lang` (
  `id_attachment` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id_attachment`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_attachment_lang: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_attachment_lang` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_attachment_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_attribute
DROP TABLE IF EXISTS `ps_attribute`;
CREATE TABLE IF NOT EXISTS `ps_attribute` (
  `id_attribute` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_attribute_group` int(10) unsigned NOT NULL,
  `color` varchar(32) DEFAULT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_attribute`),
  KEY `attribute_group` (`id_attribute_group`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_attribute: ~24 rows (approximately)
/*!40000 ALTER TABLE `ps_attribute` DISABLE KEYS */;
INSERT INTO `ps_attribute` (`id_attribute`, `id_attribute_group`, `color`, `position`) VALUES
	(1, 1, '', 0),
	(2, 1, '', 1),
	(3, 1, '', 2),
	(4, 1, '', 3),
	(5, 3, '#AAB2BD', 0),
	(6, 3, '#CFC4A6', 1),
	(7, 3, '#f5f5dc', 2),
	(8, 3, '#ffffff', 3),
	(9, 3, '#faebd7', 4),
	(10, 3, '#E84C3D', 5),
	(11, 3, '#434A54', 6),
	(12, 3, '#C19A6B', 7),
	(13, 3, '#F39C11', 8),
	(14, 3, '#5D9CEC', 9),
	(15, 3, '#A0D468', 10),
	(16, 3, '#F1C40F', 11),
	(17, 3, '#964B00', 12),
	(18, 2, '', 0),
	(19, 2, '', 1),
	(20, 2, '', 2),
	(21, 2, '', 3),
	(22, 2, '', 4),
	(23, 2, '', 5),
	(24, 3, '#FCCACD', 13);
/*!40000 ALTER TABLE `ps_attribute` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_attribute_group
DROP TABLE IF EXISTS `ps_attribute_group`;
CREATE TABLE IF NOT EXISTS `ps_attribute_group` (
  `id_attribute_group` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `is_color_group` tinyint(1) NOT NULL DEFAULT '0',
  `group_type` enum('select','radio','color') NOT NULL DEFAULT 'select',
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_attribute_group`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_attribute_group: ~3 rows (approximately)
/*!40000 ALTER TABLE `ps_attribute_group` DISABLE KEYS */;
INSERT INTO `ps_attribute_group` (`id_attribute_group`, `is_color_group`, `group_type`, `position`) VALUES
	(1, 0, 'select', 0),
	(2, 0, 'select', 1),
	(3, 1, 'color', 2);
/*!40000 ALTER TABLE `ps_attribute_group` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_attribute_group_lang
DROP TABLE IF EXISTS `ps_attribute_group_lang`;
CREATE TABLE IF NOT EXISTS `ps_attribute_group_lang` (
  `id_attribute_group` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(128) NOT NULL,
  `public_name` varchar(64) NOT NULL,
  PRIMARY KEY (`id_attribute_group`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_attribute_group_lang: ~3 rows (approximately)
/*!40000 ALTER TABLE `ps_attribute_group_lang` DISABLE KEYS */;
INSERT INTO `ps_attribute_group_lang` (`id_attribute_group`, `id_lang`, `name`, `public_name`) VALUES
	(1, 1, 'Size', 'Size'),
	(2, 1, 'Shoes Size', 'Size'),
	(3, 1, 'Color', 'Color');
/*!40000 ALTER TABLE `ps_attribute_group_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_attribute_group_shop
DROP TABLE IF EXISTS `ps_attribute_group_shop`;
CREATE TABLE IF NOT EXISTS `ps_attribute_group_shop` (
  `id_attribute_group` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_attribute_group`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_attribute_group_shop: ~3 rows (approximately)
/*!40000 ALTER TABLE `ps_attribute_group_shop` DISABLE KEYS */;
INSERT INTO `ps_attribute_group_shop` (`id_attribute_group`, `id_shop`) VALUES
	(1, 1),
	(2, 1),
	(3, 1);
/*!40000 ALTER TABLE `ps_attribute_group_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_attribute_impact
DROP TABLE IF EXISTS `ps_attribute_impact`;
CREATE TABLE IF NOT EXISTS `ps_attribute_impact` (
  `id_attribute_impact` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_product` int(11) unsigned NOT NULL,
  `id_attribute` int(11) unsigned NOT NULL,
  `weight` decimal(20,6) NOT NULL,
  `price` decimal(17,2) NOT NULL,
  PRIMARY KEY (`id_attribute_impact`),
  UNIQUE KEY `id_product` (`id_product`,`id_attribute`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_attribute_impact: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_attribute_impact` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_attribute_impact` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_attribute_lang
DROP TABLE IF EXISTS `ps_attribute_lang`;
CREATE TABLE IF NOT EXISTS `ps_attribute_lang` (
  `id_attribute` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`id_attribute`,`id_lang`),
  KEY `id_lang` (`id_lang`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_attribute_lang: ~24 rows (approximately)
/*!40000 ALTER TABLE `ps_attribute_lang` DISABLE KEYS */;
INSERT INTO `ps_attribute_lang` (`id_attribute`, `id_lang`, `name`) VALUES
	(18, 1, '35'),
	(19, 1, '36'),
	(20, 1, '37'),
	(21, 1, '38'),
	(22, 1, '39'),
	(23, 1, '40'),
	(16, 1, 'Amarillo'),
	(14, 1, 'Azul'),
	(7, 1, 'Beige'),
	(8, 1, 'Blanco'),
	(9, 1, 'Blanco roto'),
	(12, 1, 'Camel'),
	(5, 1, 'Gris'),
	(6, 1, 'Gris pardo'),
	(3, 1, 'L'),
	(2, 1, 'M'),
	(17, 1, 'Marrón'),
	(13, 1, 'Naranja'),
	(11, 1, 'Negro'),
	(10, 1, 'Rojo'),
	(24, 1, 'Rosa'),
	(1, 1, 'S'),
	(4, 1, 'Talla única'),
	(15, 1, 'Verde');
/*!40000 ALTER TABLE `ps_attribute_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_attribute_shop
DROP TABLE IF EXISTS `ps_attribute_shop`;
CREATE TABLE IF NOT EXISTS `ps_attribute_shop` (
  `id_attribute` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_attribute`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_attribute_shop: ~24 rows (approximately)
/*!40000 ALTER TABLE `ps_attribute_shop` DISABLE KEYS */;
INSERT INTO `ps_attribute_shop` (`id_attribute`, `id_shop`) VALUES
	(1, 1),
	(2, 1),
	(3, 1),
	(4, 1),
	(5, 1),
	(6, 1),
	(7, 1),
	(8, 1),
	(9, 1),
	(10, 1),
	(11, 1),
	(12, 1),
	(13, 1),
	(14, 1),
	(15, 1),
	(16, 1),
	(17, 1),
	(18, 1),
	(19, 1),
	(20, 1),
	(21, 1),
	(22, 1),
	(23, 1),
	(24, 1);
/*!40000 ALTER TABLE `ps_attribute_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_badge
DROP TABLE IF EXISTS `ps_badge`;
CREATE TABLE IF NOT EXISTS `ps_badge` (
  `id_badge` int(11) NOT NULL AUTO_INCREMENT,
  `id_ps_badge` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `id_group` int(11) NOT NULL,
  `group_position` int(11) NOT NULL,
  `scoring` int(11) NOT NULL,
  `awb` int(11) DEFAULT '0',
  `validated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_badge`)
) ENGINE=InnoDB AUTO_INCREMENT=239 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_badge: ~236 rows (approximately)
/*!40000 ALTER TABLE `ps_badge` DISABLE KEYS */;
INSERT INTO `ps_badge` (`id_badge`, `id_ps_badge`, `type`, `id_group`, `group_position`, `scoring`, `awb`, `validated`) VALUES
	(1, 159, 'feature', 41, 1, 5, 1, 0),
	(2, 160, 'feature', 41, 2, 10, 1, 0),
	(3, 161, 'feature', 41, 3, 15, 1, 0),
	(4, 162, 'feature', 41, 4, 20, 1, 0),
	(5, 163, 'feature', 41, 1, 5, 1, 0),
	(6, 164, 'feature', 41, 2, 10, 1, 0),
	(7, 165, 'feature', 41, 3, 15, 1, 0),
	(8, 166, 'feature', 41, 4, 20, 1, 0),
	(9, 233, 'feature', 41, 1, 5, 1, 0),
	(10, 234, 'feature', 41, 2, 10, 1, 0),
	(11, 235, 'feature', 41, 3, 15, 1, 0),
	(12, 236, 'feature', 41, 4, 20, 1, 0),
	(13, 249, 'feature', 41, 1, 5, 1, 0),
	(14, 250, 'feature', 41, 2, 10, 1, 0),
	(15, 251, 'feature', 41, 3, 15, 1, 0),
	(16, 252, 'feature', 41, 4, 20, 1, 0),
	(17, 253, 'feature', 41, 1, 5, 1, 0),
	(18, 254, 'feature', 41, 2, 10, 1, 0),
	(19, 255, 'feature', 41, 3, 15, 1, 0),
	(20, 256, 'feature', 41, 4, 20, 1, 0),
	(21, 261, 'feature', 41, 1, 5, 1, 0),
	(22, 262, 'feature', 41, 2, 10, 1, 0),
	(23, 263, 'feature', 41, 3, 15, 1, 1),
	(24, 264, 'feature', 41, 4, 20, 1, 1),
	(25, 269, 'feature', 41, 1, 5, 1, 0),
	(26, 270, 'feature', 41, 2, 10, 1, 0),
	(27, 271, 'feature', 41, 3, 15, 1, 0),
	(28, 272, 'feature', 41, 4, 20, 1, 0),
	(29, 273, 'feature', 41, 1, 5, 1, 0),
	(30, 274, 'feature', 41, 2, 10, 1, 0),
	(31, 275, 'feature', 41, 3, 15, 1, 0),
	(32, 276, 'feature', 41, 4, 20, 1, 0),
	(33, 277, 'feature', 41, 1, 5, 1, 0),
	(34, 278, 'feature', 41, 2, 10, 1, 0),
	(35, 279, 'feature', 41, 3, 15, 1, 0),
	(36, 280, 'feature', 41, 4, 20, 1, 0),
	(37, 281, 'feature', 41, 1, 5, 1, 0),
	(38, 282, 'feature', 41, 2, 10, 1, 0),
	(39, 283, 'feature', 41, 3, 15, 1, 0),
	(40, 284, 'feature', 41, 4, 20, 1, 0),
	(41, 285, 'feature', 41, 1, 5, 1, 0),
	(42, 286, 'feature', 41, 2, 10, 1, 0),
	(43, 287, 'feature', 41, 3, 15, 1, 0),
	(44, 288, 'feature', 41, 4, 20, 1, 0),
	(45, 289, 'feature', 41, 1, 5, 1, 0),
	(46, 290, 'feature', 41, 2, 10, 1, 0),
	(47, 291, 'feature', 41, 3, 15, 1, 0),
	(48, 292, 'feature', 41, 4, 20, 1, 0),
	(49, 293, 'feature', 41, 1, 5, 1, 0),
	(50, 294, 'feature', 41, 2, 10, 1, 0),
	(51, 295, 'feature', 41, 3, 15, 1, 0),
	(52, 296, 'feature', 41, 4, 20, 1, 0),
	(53, 297, 'feature', 41, 1, 5, 1, 0),
	(54, 298, 'feature', 41, 2, 10, 1, 0),
	(55, 299, 'feature', 41, 3, 15, 1, 0),
	(56, 300, 'feature', 41, 4, 20, 1, 0),
	(57, 301, 'feature', 41, 1, 5, 1, 0),
	(58, 302, 'feature', 41, 2, 10, 1, 0),
	(59, 303, 'feature', 41, 3, 15, 1, 0),
	(60, 304, 'feature', 41, 4, 20, 1, 0),
	(61, 305, 'feature', 41, 1, 5, 1, 0),
	(62, 306, 'feature', 41, 2, 10, 1, 0),
	(63, 307, 'feature', 41, 3, 15, 1, 0),
	(64, 308, 'feature', 41, 4, 20, 1, 0),
	(65, 309, 'feature', 41, 1, 5, 1, 0),
	(66, 310, 'feature', 41, 2, 10, 1, 0),
	(67, 311, 'feature', 41, 3, 15, 1, 0),
	(68, 312, 'feature', 41, 4, 20, 1, 0),
	(69, 313, 'feature', 41, 1, 5, 1, 0),
	(70, 314, 'feature', 41, 2, 10, 1, 0),
	(71, 315, 'feature', 41, 3, 15, 1, 0),
	(72, 316, 'feature', 41, 4, 20, 1, 0),
	(73, 317, 'feature', 41, 1, 5, 1, 0),
	(74, 318, 'feature', 41, 2, 10, 1, 0),
	(75, 319, 'feature', 41, 3, 15, 1, 0),
	(76, 320, 'feature', 41, 4, 20, 1, 0),
	(77, 321, 'feature', 41, 1, 5, 1, 0),
	(78, 322, 'feature', 41, 2, 10, 1, 0),
	(79, 323, 'feature', 41, 3, 15, 1, 0),
	(80, 324, 'feature', 41, 4, 20, 1, 0),
	(81, 325, 'feature', 41, 1, 5, 1, 0),
	(82, 326, 'feature', 41, 2, 10, 1, 0),
	(83, 327, 'feature', 41, 3, 15, 1, 0),
	(84, 328, 'feature', 41, 4, 20, 1, 0),
	(85, 329, 'feature', 41, 1, 5, 1, 0),
	(86, 330, 'feature', 41, 2, 10, 1, 0),
	(87, 331, 'feature', 41, 3, 15, 1, 0),
	(88, 332, 'feature', 41, 4, 20, 1, 0),
	(89, 333, 'feature', 41, 1, 5, 1, 0),
	(90, 334, 'feature', 41, 2, 10, 1, 0),
	(91, 335, 'feature', 41, 3, 15, 1, 0),
	(92, 336, 'feature', 41, 4, 20, 1, 0),
	(93, 337, 'feature', 41, 1, 5, 1, 0),
	(94, 338, 'feature', 41, 2, 10, 1, 0),
	(95, 339, 'feature', 41, 3, 15, 1, 0),
	(96, 340, 'feature', 41, 4, 20, 1, 0),
	(97, 341, 'feature', 41, 1, 5, 1, 0),
	(98, 342, 'feature', 41, 2, 10, 1, 0),
	(99, 343, 'feature', 41, 3, 15, 1, 0),
	(100, 344, 'feature', 41, 4, 20, 1, 0),
	(101, 345, 'feature', 41, 1, 5, 1, 0),
	(102, 346, 'feature', 41, 2, 10, 1, 0),
	(103, 347, 'feature', 41, 3, 15, 1, 0),
	(104, 348, 'feature', 41, 4, 20, 1, 0),
	(105, 349, 'feature', 41, 1, 5, 1, 0),
	(106, 350, 'feature', 41, 2, 10, 1, 0),
	(107, 351, 'feature', 41, 3, 15, 1, 0),
	(108, 352, 'feature', 41, 4, 20, 1, 0),
	(109, 353, 'feature', 41, 1, 5, 1, 0),
	(110, 354, 'feature', 41, 2, 10, 1, 0),
	(111, 355, 'feature', 41, 3, 15, 1, 0),
	(112, 356, 'feature', 41, 4, 20, 1, 0),
	(113, 357, 'feature', 41, 1, 5, 1, 0),
	(114, 358, 'feature', 41, 2, 10, 1, 0),
	(115, 359, 'feature', 41, 3, 15, 1, 0),
	(116, 360, 'feature', 41, 4, 20, 1, 0),
	(117, 1, 'feature', 1, 1, 10, 0, 1),
	(118, 2, 'feature', 2, 1, 10, 0, 0),
	(119, 3, 'feature', 2, 2, 15, 0, 0),
	(120, 4, 'feature', 3, 1, 15, 0, 0),
	(121, 5, 'feature', 3, 2, 15, 0, 0),
	(122, 6, 'feature', 4, 1, 15, 0, 0),
	(123, 7, 'feature', 4, 2, 15, 0, 0),
	(124, 8, 'feature', 5, 1, 5, 0, 0),
	(125, 9, 'feature', 5, 2, 10, 0, 0),
	(126, 10, 'feature', 6, 1, 15, 0, 0),
	(127, 11, 'feature', 6, 2, 10, 0, 0),
	(128, 12, 'feature', 6, 3, 10, 0, 0),
	(129, 13, 'feature', 5, 3, 10, 0, 0),
	(130, 14, 'feature', 5, 4, 15, 0, 0),
	(131, 15, 'feature', 5, 5, 20, 0, 0),
	(132, 16, 'feature', 5, 6, 20, 0, 0),
	(133, 17, 'achievement', 7, 1, 5, 0, 1),
	(134, 18, 'achievement', 7, 2, 10, 0, 0),
	(135, 19, 'feature', 8, 1, 15, 0, 0),
	(136, 20, 'feature', 8, 2, 15, 0, 0),
	(137, 21, 'feature', 9, 1, 15, 0, 0),
	(138, 22, 'feature', 10, 1, 10, 0, 0),
	(139, 23, 'feature', 10, 2, 10, 0, 0),
	(140, 24, 'feature', 10, 3, 10, 0, 0),
	(141, 25, 'feature', 10, 4, 10, 0, 0),
	(142, 26, 'feature', 10, 5, 10, 0, 0),
	(143, 27, 'feature', 4, 3, 10, 0, 0),
	(144, 28, 'feature', 3, 3, 10, 0, 0),
	(145, 29, 'achievement', 11, 1, 5, 0, 0),
	(146, 30, 'achievement', 11, 2, 10, 0, 0),
	(147, 31, 'achievement', 11, 3, 15, 0, 0),
	(148, 32, 'achievement', 11, 4, 20, 0, 0),
	(149, 33, 'achievement', 11, 5, 25, 0, 0),
	(150, 34, 'achievement', 11, 6, 30, 0, 0),
	(151, 35, 'achievement', 7, 3, 15, 0, 0),
	(152, 36, 'achievement', 7, 4, 20, 0, 0),
	(153, 37, 'achievement', 7, 5, 25, 0, 0),
	(154, 38, 'achievement', 7, 6, 30, 0, 0),
	(155, 39, 'achievement', 12, 1, 5, 0, 0),
	(156, 40, 'achievement', 12, 2, 10, 0, 0),
	(157, 41, 'achievement', 12, 3, 15, 0, 0),
	(158, 42, 'achievement', 12, 4, 20, 0, 0),
	(159, 43, 'achievement', 12, 5, 25, 0, 0),
	(160, 44, 'achievement', 12, 6, 30, 0, 0),
	(161, 45, 'achievement', 13, 1, 5, 0, 0),
	(162, 46, 'achievement', 13, 2, 10, 0, 0),
	(163, 47, 'achievement', 13, 3, 15, 0, 0),
	(164, 48, 'achievement', 13, 4, 20, 0, 0),
	(165, 49, 'achievement', 13, 5, 25, 0, 0),
	(166, 50, 'achievement', 13, 6, 30, 0, 0),
	(167, 51, 'achievement', 14, 1, 5, 0, 0),
	(168, 52, 'achievement', 14, 2, 10, 0, 0),
	(169, 53, 'achievement', 14, 3, 15, 0, 0),
	(170, 54, 'achievement', 14, 4, 20, 0, 0),
	(171, 55, 'achievement', 14, 5, 25, 0, 0),
	(172, 56, 'achievement', 14, 6, 30, 0, 0),
	(173, 57, 'achievement', 15, 1, 5, 0, 0),
	(174, 58, 'achievement', 15, 2, 10, 0, 0),
	(175, 59, 'achievement', 15, 3, 15, 0, 0),
	(176, 60, 'achievement', 15, 4, 20, 0, 0),
	(177, 61, 'achievement', 15, 5, 25, 0, 0),
	(178, 62, 'achievement', 15, 6, 30, 0, 0),
	(179, 63, 'achievement', 16, 1, 5, 0, 0),
	(180, 64, 'achievement', 16, 2, 10, 0, 0),
	(181, 65, 'achievement', 16, 3, 15, 0, 0),
	(182, 66, 'achievement', 16, 4, 20, 0, 0),
	(183, 67, 'achievement', 16, 5, 25, 0, 0),
	(184, 68, 'achievement', 16, 6, 30, 0, 0),
	(185, 74, 'international', 22, 1, 10, 0, 0),
	(186, 75, 'international', 23, 1, 10, 0, 0),
	(189, 83, 'international', 31, 1, 10, 0, 0),
	(190, 85, 'international', 32, 1, 10, 0, 0),
	(191, 86, 'international', 33, 1, 10, 0, 0),
	(192, 87, 'international', 34, 1, 10, 0, 0),
	(193, 88, 'feature', 35, 1, 5, 0, 1),
	(194, 89, 'feature', 35, 2, 10, 0, 0),
	(195, 90, 'feature', 35, 3, 10, 0, 0),
	(196, 91, 'feature', 35, 4, 10, 0, 0),
	(197, 92, 'feature', 35, 5, 10, 0, 0),
	(198, 93, 'feature', 35, 6, 10, 0, 0),
	(199, 94, 'feature', 36, 1, 5, 0, 0),
	(200, 95, 'feature', 36, 2, 5, 0, 0),
	(201, 96, 'feature', 36, 3, 10, 0, 0),
	(202, 97, 'feature', 36, 4, 10, 0, 0),
	(203, 98, 'feature', 36, 5, 20, 0, 0),
	(204, 99, 'feature', 36, 6, 20, 0, 0),
	(205, 100, 'feature', 8, 3, 15, 0, 0),
	(206, 101, 'achievement', 37, 1, 5, 0, 0),
	(207, 102, 'achievement', 37, 2, 5, 0, 0),
	(208, 103, 'achievement', 37, 3, 10, 0, 0),
	(209, 104, 'achievement', 37, 4, 10, 0, 0),
	(210, 105, 'achievement', 37, 5, 15, 0, 0),
	(211, 106, 'achievement', 37, 6, 15, 0, 0),
	(212, 107, 'achievement', 38, 1, 10, 0, 0),
	(213, 108, 'achievement', 38, 2, 10, 0, 0),
	(214, 109, 'achievement', 38, 3, 15, 0, 0),
	(215, 110, 'achievement', 38, 4, 20, 0, 0),
	(216, 111, 'achievement', 38, 5, 25, 0, 0),
	(217, 112, 'achievement', 38, 6, 30, 0, 0),
	(218, 113, 'achievement', 39, 1, 10, 0, 0),
	(219, 114, 'achievement', 39, 2, 20, 0, 0),
	(220, 115, 'achievement', 39, 3, 30, 0, 0),
	(221, 116, 'achievement', 39, 4, 40, 0, 0),
	(222, 117, 'achievement', 39, 5, 50, 0, 0),
	(223, 118, 'achievement', 39, 6, 50, 0, 0),
	(224, 119, 'feature', 40, 1, 10, 0, 0),
	(225, 120, 'feature', 40, 2, 15, 0, 0),
	(226, 121, 'feature', 40, 3, 20, 0, 0),
	(227, 122, 'feature', 40, 4, 25, 0, 0),
	(228, 195, 'feature', 41, 1, 5, 1, 0),
	(229, 196, 'feature', 41, 2, 10, 1, 0),
	(230, 229, 'feature', 41, 1, 5, 1, 0),
	(231, 230, 'feature', 41, 2, 10, 1, 0),
	(232, 231, 'feature', 41, 3, 15, 1, 0),
	(233, 232, 'feature', 41, 4, 20, 1, 0),
	(234, 241, 'feature', 41, 1, 5, 1, 0),
	(235, 242, 'feature', 41, 2, 10, 1, 0),
	(236, 243, 'feature', 41, 3, 15, 1, 0),
	(237, 244, 'feature', 41, 4, 20, 1, 0),
	(238, 84, 'international', 25, 1, 10, 0, 0);
/*!40000 ALTER TABLE `ps_badge` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_badge_lang
DROP TABLE IF EXISTS `ps_badge_lang`;
CREATE TABLE IF NOT EXISTS `ps_badge_lang` (
  `id_badge` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `group_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_badge`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_badge_lang: ~236 rows (approximately)
/*!40000 ALTER TABLE `ps_badge_lang` DISABLE KEYS */;
INSERT INTO `ps_badge_lang` (`id_badge`, `id_lang`, `name`, `description`, `group_name`) VALUES
	(1, 1, 'Shopgate installed', 'You have installed the Shopgate module', 'Partners'),
	(2, 1, 'Shopgate configured', 'You have configured the Shopgate module', 'Partners'),
	(3, 1, 'Shopgate active', 'Your Shopgate module is active', 'Partners'),
	(4, 1, 'Shopgate very active', 'Your Shopgate module is very active', 'Partners'),
	(5, 1, 'Skrill installed', 'You have installed the Skrill module', 'Partners'),
	(6, 1, 'Skrill configured', 'You have configured the Skrill module', 'Partners'),
	(7, 1, 'Skrill active', 'Your Skrill module is active', 'Partners'),
	(8, 1, 'Skrill very active', 'Your Skrill module is very active', 'Partners'),
	(9, 1, 'Authorize Aim installed', 'You have installed the Authorize Aim module', 'Partners'),
	(10, 1, 'Authorize Aim configured', 'You have configured the Authorize Aim module', 'Partners'),
	(11, 1, 'Authorize Aim active', 'Your Authorize Aim module is active', 'Partners'),
	(12, 1, 'Authorize Aim very active', 'Your Authorize Aim module is very active', 'Partners'),
	(13, 1, 'Ebay installed', 'You have installed the Ebay module', 'Partners'),
	(14, 1, 'Ebay configured', 'You have configured the Ebay module', 'Partners'),
	(15, 1, 'Ebay active', 'Your Ebay module is active', 'Partners'),
	(16, 1, 'Ebay very active', 'Your Ebay module is very active', 'Partners'),
	(17, 1, 'PayPlug installed', 'You have installed the PayPlug module', 'Partners'),
	(18, 1, 'PayPlug configured', 'You have configured the PayPlug module', 'Partners'),
	(19, 1, 'PayPlug active', 'Your PayPlug module is active', 'Partners'),
	(20, 1, 'PayPlug very active', 'Your PayPlug module is very active', 'Partners'),
	(21, 1, 'Affinity Items installed', 'You have installed the Affinity Items module', 'Partners'),
	(22, 1, 'Affinity Items configured', 'You have configured the Affinity Items module', 'Partners'),
	(23, 1, 'Affinity Items active', 'Your Affinity Items module is active', 'Partners'),
	(24, 1, 'Affinity Items very active', 'Your Affinity Items module is very active', 'Partners'),
	(25, 1, 'DPD Poland installed', 'You have installed the DPD Poland module', 'Partners'),
	(26, 1, 'DPD Poland configured', 'You have configured the DPD Poland module', 'Partners'),
	(27, 1, 'DPD Poland active', 'Your DPD Poland module is active', 'Partners'),
	(28, 1, 'DPD Poland very active', 'Your DPD Poland module is very active', 'Partners'),
	(29, 1, 'Envoimoinscher installed', 'You have installed the Envoimoinscher module', 'Partners'),
	(30, 1, 'Envoimoinscher configured', 'You have configured the Envoimoinscher module', 'Partners'),
	(31, 1, 'Envoimoinscher active', 'Your Envoimoinscher module is active', 'Partners'),
	(32, 1, 'Envoimoinscher very active', 'Your Envoimoinscher module is very active', 'Partners'),
	(33, 1, 'Klik&Pay installed', 'You have installed the Klik&Pay module', 'Partners'),
	(34, 1, 'Klik&Pay configured', 'You have configured the Klik&Pay module', 'Partners'),
	(35, 1, 'Klik&Pay active', 'Your Klik&Pay module is active', 'Partners'),
	(36, 1, 'Klik&Pay very active', 'Your Klik&Pay module is very active', 'Partners'),
	(37, 1, 'Clickline installed', 'You have installed the Clickline module', 'Partners'),
	(38, 1, 'Clickline configured', 'You have configured the Clickline module', 'Partners'),
	(39, 1, 'Clickline active', 'Your Clickline module is active', 'Partners'),
	(40, 1, 'Clickline very active', 'Your Clickline module is very active', 'Partners'),
	(41, 1, 'CDiscount installed', 'You have installed the CDiscount module', 'Partners'),
	(42, 1, 'CDiscount configured', 'You have configured the CDiscount module', 'Partners'),
	(43, 1, 'CDiscount active', 'Your CDiscount module is active', 'Partners'),
	(44, 1, 'CDiscount very active', 'Your CDiscount module is very active', 'Partners'),
	(45, 1, 'illicoPresta installed', 'You have installed the illicoPresta module', 'Partners'),
	(46, 1, 'illicoPresta configured', 'You have configured the illicoPresta module', 'Partners'),
	(47, 1, 'illicoPresta active', 'Your illicoPresta module is active', 'Partners'),
	(48, 1, 'illicoPresta very active', 'Your illicoPresta module is very active', 'Partners'),
	(49, 1, 'NetReviews installed', 'You have installed the NetReviews module', 'Partners'),
	(50, 1, 'NetReviews configured', 'You have configured the NetReviews module', 'Partners'),
	(51, 1, 'NetReviews active', 'Your NetReviews module is active', 'Partners'),
	(52, 1, 'NetReviews very active', 'Your NetReviews module is very active', 'Partners'),
	(53, 1, 'Bluesnap installed', 'You have installed the Bluesnap module', 'Partners'),
	(54, 1, 'Bluesnap configured', 'You have configured the Bluesnap module', 'Partners'),
	(55, 1, 'Bluesnap active', 'Your Bluesnap module is active', 'Partners'),
	(56, 1, 'Bluesnap very active', 'Your Bluesnap module is very active', 'Partners'),
	(57, 1, 'Desjardins installed', 'You have installed the Desjardins module', 'Partners'),
	(58, 1, 'Desjardins configured', 'You have configured the Desjardins module', 'Partners'),
	(59, 1, 'Desjardins active', 'Your Desjardins module is active', 'Partners'),
	(60, 1, 'Desjardins very active', 'Your Desjardins module is very active', 'Partners'),
	(61, 1, 'First Data installed', 'You have installed the First Data module', 'Partners'),
	(62, 1, 'First Data configured', 'You have configured the First Data module', 'Partners'),
	(63, 1, 'First Data active', 'Your First Data module is active', 'Partners'),
	(64, 1, 'First Data very active', 'Your First Data module is very active', 'Partners'),
	(65, 1, 'Give.it installed', 'You have installed the Give.it module', 'Partners'),
	(66, 1, 'Give.it configured', 'You have configured the Give.it module', 'Partners'),
	(67, 1, 'Give.it active', 'Your Give.it module is active', 'Partners'),
	(68, 1, 'Give.it very active', 'Your Give.it module is very active', 'Partners'),
	(69, 1, 'Google Analytics installed', 'You have installed the Google Analytics module', 'Partners'),
	(70, 1, 'Google Analytics configured', 'You have configured the Google Analytics module', 'Partners'),
	(71, 1, 'Google Analytics active', 'Your Google Analytics module is active', 'Partners'),
	(72, 1, 'Google Analytics very active', 'Your Google Analytics module is very active', 'Partners'),
	(73, 1, 'PagSeguro installed', 'You have installed the PagSeguro module', 'Partners'),
	(74, 1, 'PagSeguro configured', 'You have configured the PagSeguro module', 'Partners'),
	(75, 1, 'PagSeguro active', 'Your PagSeguro module is active', 'Partners'),
	(76, 1, 'PagSeguro very active', 'Your PagSeguro module is very active', 'Partners'),
	(77, 1, 'Paypal MX installed', 'You have installed the Paypal MX module', 'Partners'),
	(78, 1, 'Paypal MX configured', 'You have configured the Paypal MX module', 'Partners'),
	(79, 1, 'Paypal MX active', 'Your Paypal MX module is active', 'Partners'),
	(80, 1, 'Paypal MX very active', 'Your Paypal MX module is very active', 'Partners'),
	(81, 1, 'Paypal USA installed', 'You have installed the Paypal USA module', 'Partners'),
	(82, 1, 'Paypal USA configured', 'You have configured the Paypal USA module', 'Partners'),
	(83, 1, 'Paypal USA active', 'Your Paypal USA module is active', 'Partners'),
	(84, 1, 'Paypal USA very active', 'Your Paypal USA module is very active', 'Partners'),
	(85, 1, 'PayULatam installed', 'You have installed the PayULatam module', 'Partners'),
	(86, 1, 'PayULatam configured', 'You have configured the PayULatam module', 'Partners'),
	(87, 1, 'PayULatam active', 'Your PayULatam module is active', 'Partners'),
	(88, 1, 'PayULatam very active', 'Your PayULatam module is very active', 'Partners'),
	(89, 1, 'PrestaStats installed', 'You have installed the PrestaStats module', 'Partners'),
	(90, 1, 'PrestaStats configured', 'You have configured the PrestaStats module', 'Partners'),
	(91, 1, 'PrestaStats active', 'Your PrestaStats module is active', 'Partners'),
	(92, 1, 'PrestaStats very active', 'Your PrestaStats module is very active', 'Partners'),
	(93, 1, 'Riskified installed', 'You have installed the Riskified module', 'Partners'),
	(94, 1, 'Riskified configured', 'You have configured the Riskified module', 'Partners'),
	(95, 1, 'Riskified active', 'Your Riskified module is active', 'Partners'),
	(96, 1, 'Riskified very active', 'Your Riskified module is very active', 'Partners'),
	(97, 1, 'Simplify installed', 'You have installed the Simplify module', 'Partners'),
	(98, 1, 'Simplify configured', 'You have configured the Simplify module', 'Partners'),
	(99, 1, 'Simplify active', 'Your Simplify module is active', 'Partners'),
	(100, 1, 'Simplify very active', 'Your Simplify module is very active', 'Partners'),
	(101, 1, 'VTPayment installed', 'You have installed the VTPayment module', 'Partners'),
	(102, 1, 'VTPayment configured', 'You have configured the VTPayment module', 'Partners'),
	(103, 1, 'VTPayment active', 'Your VTPayment module is active', 'Partners'),
	(104, 1, 'VTPayment very active', 'Your VTPayment module is very active', 'Partners'),
	(105, 1, 'Yotpo installed', 'You have installed the Yotpo module', 'Partners'),
	(106, 1, 'Yotpo configured', 'You have configured the Yotpo module', 'Partners'),
	(107, 1, 'Yotpo active', 'Your Yotpo module is active', 'Partners'),
	(108, 1, 'Yotpo very active', 'Your Yotpo module is very active', 'Partners'),
	(109, 1, 'Youstice installed', 'You have installed the Youstice module', 'Partners'),
	(110, 1, 'Youstice configured', 'You have configured the Youstice module', 'Partners'),
	(111, 1, 'Youstice active', 'Your Youstice module is active', 'Partners'),
	(112, 1, 'Youstice very active', 'Your Youstice module is very active', 'Partners'),
	(113, 1, 'Loyalty Lion installed', 'You have installed the Loyalty Lion module', 'Partners'),
	(114, 1, 'Loyalty Lion configured', 'You have configured the Loyalty Lion module', 'Partners'),
	(115, 1, 'Loyalty Lion active', 'Your Loyalty Lion module is active', 'Partners'),
	(116, 1, 'Loyalty Lion very active', 'Your Loyalty Lion module is very active', 'Partners'),
	(117, 1, 'SEO', 'You enabled the URL rewriting through the tab "Preferences > SEO and URLs".', 'SEO'),
	(118, 1, 'Site Performance', 'You enabled CCC (Combine, Compress and Cache), Rijndael and Smarty through the tab \r\nAdvanced Parameters > Performance.', 'Site Performance'),
	(119, 1, 'Site Performance', 'You enabled media servers through the tab "Advanced parameters > Performance".', 'Site Performance'),
	(120, 1, 'Payment', 'You configured a payment solution on your shop.', 'Payment'),
	(121, 1, 'Payment', 'You offer two different payment methods to your customers.', 'Payment'),
	(122, 1, 'Shipping', 'You configured a carrier on your shop.', 'Shipping'),
	(123, 1, 'Shipping', 'You offer two shipping solutions (carriers) to your customers.', 'Shipping'),
	(124, 1, 'Catalog Size', 'You added your first product to your catalog!', 'Catalog Size'),
	(125, 1, 'Catalog Size', 'You have 10 products within your catalog.', 'Catalog Size'),
	(126, 1, 'Contact information', 'You configured your phone number so your customers can reach you!', 'Contact information'),
	(127, 1, 'Contact information', 'You added a third email address to your contact form.', 'Contact information'),
	(128, 1, 'Contact information', 'You suggest a total of 5 departments to be reached by your customers via your contact form.', 'Contact information'),
	(129, 1, 'Catalog Size', 'You have 100 products within your catalog.', 'Catalog Size'),
	(130, 1, 'Catalog Size', 'You have 1,000 products within your catalog.', 'Catalog Size'),
	(131, 1, 'Catalog Size', 'You have 10,000 products within your catalog.', 'Catalog Size'),
	(132, 1, 'Catalog Size', 'You have 100,000 products within your catalog.', 'Catalog Size'),
	(133, 1, 'Days of Experience', 'You just installed PrestaShop!', 'Days of Experience'),
	(134, 1, 'Days of Experience', 'You installed PrestaShop a week ago!', 'Days of Experience'),
	(135, 1, 'Customization', 'You uploaded your own logo.', 'Customization'),
	(136, 1, 'Customization', 'You installed a new template.', 'Customization'),
	(137, 1, 'Addons', 'You connected your back-office to the Addons platform using your PrestaShop Addons account.', 'Addons'),
	(138, 1, 'Multistores', 'You enabled the Multistores feature.', 'Multistores'),
	(139, 1, 'Multistores', 'You manage two shops with the Multistores feature.', 'Multistores'),
	(140, 1, 'Multistores', 'You manage two different groups of shops using the Multistores feature.', 'Multistores'),
	(141, 1, 'Multistores', 'You manage five shops with the Multistores feature.', 'Multistores'),
	(142, 1, 'Multistores', 'You manage five different groups of shops using the Multistores feature.', 'Multistores'),
	(143, 1, 'Shipping', 'You offer three different shipping solutions (carriers) to your customers.', 'Shipping'),
	(144, 1, 'Payment', 'You offer three different payment methods to your customers.', 'Payment'),
	(145, 1, 'Revenue', 'You get this badge when you reach 200 USD in sales.', 'Revenue'),
	(146, 1, 'Revenue', 'You get this badge when you reach 1000 USD in sales.', 'Revenue'),
	(147, 1, 'Revenue', 'You get this badge when you reach 1000 USD in sales.', 'Revenue'),
	(148, 1, 'Revenue', 'You get this badge when you reach 200 USD in sales.', 'Revenue'),
	(149, 1, 'Revenue', 'You get this badge when you reach 1000 USD in sales.', 'Revenue'),
	(150, 1, 'Revenue', 'You get this badge when you reach 1000 USD in sales.', 'Revenue'),
	(151, 1, 'Days of Experience', 'You installed PrestaShop a month ago!', 'Days of Experience'),
	(152, 1, 'Days of Experience', 'You installed PrestaShop six months ago!', 'Days of Experience'),
	(153, 1, 'Days of Experience', 'You installed PrestaShop a year ago!', 'Days of Experience'),
	(154, 1, 'Days of Experience', 'You installed PrestaShop two years ago!', 'Days of Experience'),
	(155, 1, 'Visitors', 'You reached 10 visitors!', 'Visitors'),
	(156, 1, 'Visitors', 'You reached 100 visitors!', 'Visitors'),
	(157, 1, 'Visitors', 'You reached 1,000 visitors!', 'Visitors'),
	(158, 1, 'Visitors', 'You reached 10,000 visitors!', 'Visitors'),
	(159, 1, 'Visitors', 'You reached 100,000 visitors!', 'Visitors'),
	(160, 1, 'Visitors', 'You reached 1,000,000 visitors!', 'Visitors'),
	(161, 1, 'Customer Carts', 'Two carts have been created by visitors', 'Customer Carts'),
	(162, 1, 'Customer Carts', 'Ten carts have been created by visitors.', 'Customer Carts'),
	(163, 1, 'Customer Carts', 'A hundred carts have been created by visitors on your shop.', 'Customer Carts'),
	(164, 1, 'Customer Carts', 'A thousand carts have been created by visitors on your shop.', 'Customer Carts'),
	(165, 1, 'Customer Carts', '10,000 carts have been created by visitors.', 'Customer Carts'),
	(166, 1, 'Customer Carts', '100,000 carts have been created by visitors.', 'Customer Carts'),
	(167, 1, 'Orders', 'You received your first order.', 'Orders'),
	(168, 1, 'Orders', '10 orders have been placed through your online shop.', 'Orders'),
	(169, 1, 'Orders', 'You received 100 orders through your online shop!', 'Orders'),
	(170, 1, 'Orders', 'You received 1,000 orders through your online shop, congrats!', 'Orders'),
	(171, 1, 'Orders', 'You received 10,000 orders through your online shop, cheers!', 'Orders'),
	(172, 1, 'Orders', 'You received 100,000 orders through your online shop!', 'Orders'),
	(173, 1, 'Customer Service Threads', 'You received  your first customer\'s message.', 'Customer Service Threads'),
	(174, 1, 'Customer Service Threads', 'You received 10 messages from your customers.', 'Customer Service Threads'),
	(175, 1, 'Customer Service Threads', 'You received 100 messages from your customers.', 'Customer Service Threads'),
	(176, 1, 'Customer Service Threads', 'You received 1,000 messages from your customers.', 'Customer Service Threads'),
	(177, 1, 'Customer Service Threads', 'You received 10,000 messages from your customers.', 'Customer Service Threads'),
	(178, 1, 'Customer Service Threads', 'You received 100,000 messages from your customers.', 'Customer Service Threads'),
	(179, 1, 'Customers', 'You got the first customer registered on your shop!', 'Customers'),
	(180, 1, 'Customers', 'You have over 10 customers registered on your shop.', 'Customers'),
	(181, 1, 'Customers', 'You have over 100 customers registered on your shop.', 'Customers'),
	(182, 1, 'Customers', 'You have over 1,000 customers registered on your shop.', 'Customers'),
	(183, 1, 'Customers', 'You have over 10,000 customers registered on your shop.', 'Customers'),
	(184, 1, 'Customers', 'You have over 100,000 customers registered on your shop.', 'Customers'),
	(185, 1, 'North America', 'You got your first sale in North America', 'North America'),
	(186, 1, 'Oceania', 'You got your first sale in Oceania', 'Oceania'),
	(189, 1, 'Asia', 'You got your first sale in Asia', 'Asia'),
	(190, 1, 'Europe', 'You got your first sale in  Europe!', 'Europe'),
	(191, 1, 'Africa', 'You got your first sale in Africa', 'Africa'),
	(192, 1, 'Maghreb', 'You got your first sale in Maghreb', 'Maghreb'),
	(193, 1, 'Your Team\'s Employees', 'First employee account added to your shop', 'Your Team\'s Employees'),
	(194, 1, 'Your Team\'s Employees', '3 employee accounts added to your shop', 'Your Team\'s Employees'),
	(195, 1, 'Your Team\'s Employees', '5 employee accounts added to your shop', 'Your Team\'s Employees'),
	(196, 1, 'Your Team\'s Employees', '10 employee accounts added to your shop', 'Your Team\'s Employees'),
	(197, 1, 'Your Team\'s Employees', '20 employee accounts added to your shop', 'Your Team\'s Employees'),
	(198, 1, 'Your Team\'s Employees', '40 employee accounts added to your shop', 'Your Team\'s Employees'),
	(199, 1, 'Product Pictures', 'First photo added to your catalog', 'Product Pictures'),
	(200, 1, 'Product Pictures', '50 photos added to your catalog', 'Product Pictures'),
	(201, 1, 'Product Pictures', '100 photos added to your catalog', 'Product Pictures'),
	(202, 1, 'Product Pictures', '1,000 photos added to your catalog', 'Product Pictures'),
	(203, 1, 'Product Pictures', '10,000 photos added to your catalog', 'Product Pictures'),
	(204, 1, 'Product Pictures', '50,000 photos added to your catalog', 'Product Pictures'),
	(205, 1, 'Customization', 'First CMS page added to your catalog', 'Customization'),
	(206, 1, 'Cart Rules', 'First cart rules configured on your shop', 'Cart Rules'),
	(207, 1, 'Cart Rules', 'You have 10 cart rules configured on your shop', 'Cart Rules'),
	(208, 1, 'Cart Rules', 'You have 100 cart rules configured on your shop', 'Cart Rules'),
	(209, 1, 'Cart Rules', 'You have 500 cart rules configured on your shop', 'Cart Rules'),
	(210, 1, 'Cart Rules', 'You have 1,000 cart rules configured on your shop', 'Cart Rules'),
	(211, 1, 'Cart Rules', 'You have 5,000 cart rules configured on your shop', 'Cart Rules'),
	(212, 1, 'International Orders', 'First international order placed on your shop.', 'International Orders'),
	(213, 1, 'International Orders', '10 international orders placed on your shop.', 'International Orders'),
	(214, 1, 'International Orders', '100 international orders placed on your shop!', 'International Orders'),
	(215, 1, 'International Orders', '1,000 international orders placed on your shop!', 'International Orders'),
	(216, 1, 'International Orders', '5,000 international orders placed on your shop!', 'International Orders'),
	(217, 1, 'International Orders', '10,000 international orders placed on your shop!', 'International Orders'),
	(218, 1, 'Store', 'First store configured on your shop!', 'Store'),
	(219, 1, 'Store', 'You have 2 stores configured on your shop', 'Store'),
	(220, 1, 'Store', 'You have 5 stores configured on your shop', 'Store'),
	(221, 1, 'Store', 'You have 10 stores configured on your shop', 'Store'),
	(222, 1, 'Store', 'You have 20 stores configured on your shop', 'Store'),
	(223, 1, 'Store', 'You have 50 stores configured on your shop', 'Store'),
	(224, 1, 'Webservice x1', 'First webservice account added to your shop', 'WebService'),
	(225, 1, 'Webservice x2', '2 webservice accounts added to your shop', 'WebService'),
	(226, 1, 'Webservice x3', '3 webservice accounts added to your shop', 'WebService'),
	(227, 1, 'Webservice x4', '4 webservice accounts added to your shop', 'WebService'),
	(228, 1, 'Shopping Feed installed', 'You have installed the Shopping Feed module', 'Partners'),
	(229, 1, 'Shopping Feed configured', 'You have configured the Shopping Feed module', 'Partners'),
	(230, 1, 'Alliance Payment installed', 'You have installed the Alliance Payment module', 'Partners'),
	(231, 1, 'Alliance Payment configured', 'You have configured the Alliance Payment module', 'Partners'),
	(232, 1, 'Alliance Payment active', 'Your Alliance Payment module is active', 'Partners'),
	(233, 1, 'Alliance Payment very active', 'Your Alliance Payment module is very active', 'Partners'),
	(234, 1, 'Blue Pay installed', 'You have installed the Blue Pay module', 'Partners'),
	(235, 1, 'Blue Pay configured', 'You have configured the Blue Pay module', 'Partners'),
	(236, 1, 'Blue Pay active', 'Your Blue Pay module is active', 'Partners'),
	(237, 1, 'Blue Pay very active', 'Your Blue Pay module is very active', 'Partners'),
	(238, 1, 'South America', 'You got your first sale in South America', 'South America');
/*!40000 ALTER TABLE `ps_badge_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_blocklink
DROP TABLE IF EXISTS `ps_blocklink`;
CREATE TABLE IF NOT EXISTS `ps_blocklink` (
  `id_blocklink` int(10) NOT NULL AUTO_INCREMENT,
  `url` varchar(254) NOT NULL,
  `new_window` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_blocklink`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_blocklink: ~1 rows (approximately)
/*!40000 ALTER TABLE `ps_blocklink` DISABLE KEYS */;
INSERT INTO `ps_blocklink` (`id_blocklink`, `url`, `new_window`) VALUES
	(2, 'http://celiodev.com/index.php', 0);
/*!40000 ALTER TABLE `ps_blocklink` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_blocklink_lang
DROP TABLE IF EXISTS `ps_blocklink_lang`;
CREATE TABLE IF NOT EXISTS `ps_blocklink_lang` (
  `id_blocklink` int(10) NOT NULL,
  `id_lang` int(10) NOT NULL,
  `text` varchar(62) NOT NULL,
  PRIMARY KEY (`id_blocklink`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_blocklink_lang: ~1 rows (approximately)
/*!40000 ALTER TABLE `ps_blocklink_lang` DISABLE KEYS */;
INSERT INTO `ps_blocklink_lang` (`id_blocklink`, `id_lang`, `text`) VALUES
	(2, 1, 'Home');
/*!40000 ALTER TABLE `ps_blocklink_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_blocklink_shop
DROP TABLE IF EXISTS `ps_blocklink_shop`;
CREATE TABLE IF NOT EXISTS `ps_blocklink_shop` (
  `id_blocklink` int(10) NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) NOT NULL,
  PRIMARY KEY (`id_blocklink`,`id_shop`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_blocklink_shop: ~1 rows (approximately)
/*!40000 ALTER TABLE `ps_blocklink_shop` DISABLE KEYS */;
INSERT INTO `ps_blocklink_shop` (`id_blocklink`, `id_shop`) VALUES
	(2, 1);
/*!40000 ALTER TABLE `ps_blocklink_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_carrier
DROP TABLE IF EXISTS `ps_carrier`;
CREATE TABLE IF NOT EXISTS `ps_carrier` (
  `id_carrier` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_reference` int(10) unsigned NOT NULL,
  `id_tax_rules_group` int(10) unsigned DEFAULT '0',
  `name` varchar(64) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `shipping_handling` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `range_behavior` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_module` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_free` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `shipping_external` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `need_range` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `external_module_name` varchar(64) DEFAULT NULL,
  `shipping_method` int(2) NOT NULL DEFAULT '0',
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `max_width` int(10) DEFAULT '0',
  `max_height` int(10) DEFAULT '0',
  `max_depth` int(10) DEFAULT '0',
  `max_weight` decimal(20,6) DEFAULT '0.000000',
  `grade` int(10) DEFAULT '0',
  PRIMARY KEY (`id_carrier`),
  KEY `deleted` (`deleted`,`active`),
  KEY `id_tax_rules_group` (`id_tax_rules_group`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_carrier: ~2 rows (approximately)
/*!40000 ALTER TABLE `ps_carrier` DISABLE KEYS */;
INSERT INTO `ps_carrier` (`id_carrier`, `id_reference`, `id_tax_rules_group`, `name`, `url`, `active`, `deleted`, `shipping_handling`, `range_behavior`, `is_module`, `is_free`, `shipping_external`, `need_range`, `external_module_name`, `shipping_method`, `position`, `max_width`, `max_height`, `max_depth`, `max_weight`, `grade`) VALUES
	(1, 1, 0, '0', '', 1, 0, 0, 0, 0, 1, 0, 0, '', 0, 0, 0, 0, 0, 0.000000, 0),
	(2, 2, 0, 'My carrier', '', 1, 0, 1, 0, 0, 0, 0, 0, '', 0, 1, 0, 0, 0, 0.000000, 0);
/*!40000 ALTER TABLE `ps_carrier` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_carrier_group
DROP TABLE IF EXISTS `ps_carrier_group`;
CREATE TABLE IF NOT EXISTS `ps_carrier_group` (
  `id_carrier` int(10) unsigned NOT NULL,
  `id_group` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_carrier`,`id_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_carrier_group: ~6 rows (approximately)
/*!40000 ALTER TABLE `ps_carrier_group` DISABLE KEYS */;
INSERT INTO `ps_carrier_group` (`id_carrier`, `id_group`) VALUES
	(1, 1),
	(1, 2),
	(1, 3),
	(2, 1),
	(2, 2),
	(2, 3);
/*!40000 ALTER TABLE `ps_carrier_group` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_carrier_lang
DROP TABLE IF EXISTS `ps_carrier_lang`;
CREATE TABLE IF NOT EXISTS `ps_carrier_lang` (
  `id_carrier` int(10) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_lang` int(10) unsigned NOT NULL,
  `delay` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_lang`,`id_shop`,`id_carrier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_carrier_lang: ~2 rows (approximately)
/*!40000 ALTER TABLE `ps_carrier_lang` DISABLE KEYS */;
INSERT INTO `ps_carrier_lang` (`id_carrier`, `id_shop`, `id_lang`, `delay`) VALUES
	(1, 1, 1, 'Recoger en tienda'),
	(2, 1, 1, '¡Envío en 24h!');
/*!40000 ALTER TABLE `ps_carrier_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_carrier_shop
DROP TABLE IF EXISTS `ps_carrier_shop`;
CREATE TABLE IF NOT EXISTS `ps_carrier_shop` (
  `id_carrier` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_carrier`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_carrier_shop: ~2 rows (approximately)
/*!40000 ALTER TABLE `ps_carrier_shop` DISABLE KEYS */;
INSERT INTO `ps_carrier_shop` (`id_carrier`, `id_shop`) VALUES
	(1, 1),
	(2, 1);
/*!40000 ALTER TABLE `ps_carrier_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_carrier_tax_rules_group_shop
DROP TABLE IF EXISTS `ps_carrier_tax_rules_group_shop`;
CREATE TABLE IF NOT EXISTS `ps_carrier_tax_rules_group_shop` (
  `id_carrier` int(11) unsigned NOT NULL,
  `id_tax_rules_group` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_carrier`,`id_tax_rules_group`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_carrier_tax_rules_group_shop: ~2 rows (approximately)
/*!40000 ALTER TABLE `ps_carrier_tax_rules_group_shop` DISABLE KEYS */;
INSERT INTO `ps_carrier_tax_rules_group_shop` (`id_carrier`, `id_tax_rules_group`, `id_shop`) VALUES
	(1, 1, 1),
	(2, 1, 1);
/*!40000 ALTER TABLE `ps_carrier_tax_rules_group_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_carrier_zone
DROP TABLE IF EXISTS `ps_carrier_zone`;
CREATE TABLE IF NOT EXISTS `ps_carrier_zone` (
  `id_carrier` int(10) unsigned NOT NULL,
  `id_zone` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_carrier`,`id_zone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_carrier_zone: ~3 rows (approximately)
/*!40000 ALTER TABLE `ps_carrier_zone` DISABLE KEYS */;
INSERT INTO `ps_carrier_zone` (`id_carrier`, `id_zone`) VALUES
	(1, 1),
	(2, 1),
	(2, 2);
/*!40000 ALTER TABLE `ps_carrier_zone` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_cart
DROP TABLE IF EXISTS `ps_cart`;
CREATE TABLE IF NOT EXISTS `ps_cart` (
  `id_cart` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_group` int(11) unsigned NOT NULL DEFAULT '1',
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_carrier` int(10) unsigned NOT NULL,
  `delivery_option` text NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `id_address_delivery` int(10) unsigned NOT NULL,
  `id_address_invoice` int(10) unsigned NOT NULL,
  `id_currency` int(10) unsigned NOT NULL,
  `id_customer` int(10) unsigned NOT NULL,
  `id_guest` int(10) unsigned NOT NULL,
  `secure_key` varchar(32) NOT NULL DEFAULT '-1',
  `recyclable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `gift` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `gift_message` text,
  `mobile_theme` tinyint(1) NOT NULL DEFAULT '0',
  `allow_seperated_package` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_cart`),
  KEY `cart_customer` (`id_customer`),
  KEY `id_address_delivery` (`id_address_delivery`),
  KEY `id_address_invoice` (`id_address_invoice`),
  KEY `id_carrier` (`id_carrier`),
  KEY `id_lang` (`id_lang`),
  KEY `id_currency` (`id_currency`),
  KEY `id_guest` (`id_guest`),
  KEY `id_shop_group` (`id_shop_group`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_cart: ~6 rows (approximately)
/*!40000 ALTER TABLE `ps_cart` DISABLE KEYS */;
INSERT INTO `ps_cart` (`id_cart`, `id_shop_group`, `id_shop`, `id_carrier`, `delivery_option`, `id_lang`, `id_address_delivery`, `id_address_invoice`, `id_currency`, `id_customer`, `id_guest`, `secure_key`, `recyclable`, `gift`, `gift_message`, `mobile_theme`, `allow_seperated_package`, `date_add`, `date_upd`) VALUES
	(1, 1, 1, 2, 'a:1:{i:3;s:2:"2,";}', 1, 4, 4, 1, 1, 2, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 0, 0, '', 0, 0, '2015-05-08 14:00:21', '2015-05-08 14:00:21'),
	(2, 1, 1, 2, 'a:1:{i:3;s:2:"2,";}', 1, 4, 4, 1, 1, 1, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 0, 0, '', 0, 0, '2015-05-08 14:00:21', '2015-05-08 14:00:21'),
	(3, 1, 1, 2, 'a:1:{i:3;s:2:"2,";}', 1, 4, 4, 1, 1, 1, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 0, 0, '', 0, 0, '2015-05-08 14:00:21', '2015-05-08 14:00:21'),
	(4, 1, 1, 2, 'a:1:{i:3;s:2:"2,";}', 1, 4, 4, 1, 1, 1, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 0, 0, '', 0, 0, '2015-05-08 14:00:21', '2015-05-08 14:00:21'),
	(5, 1, 1, 2, 'a:1:{i:3;s:2:"2,";}', 1, 4, 4, 1, 1, 1, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 0, 0, '', 0, 0, '2015-05-08 14:00:21', '2015-05-08 14:00:21'),
	(6, 1, 1, 0, '', 1, 0, 0, 1, 0, 7, '', 0, 0, '', 0, 0, '2015-05-12 16:17:04', '2015-05-12 16:45:04');
/*!40000 ALTER TABLE `ps_cart` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_cart_cart_rule
DROP TABLE IF EXISTS `ps_cart_cart_rule`;
CREATE TABLE IF NOT EXISTS `ps_cart_cart_rule` (
  `id_cart` int(10) unsigned NOT NULL,
  `id_cart_rule` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_cart`,`id_cart_rule`),
  KEY `id_cart_rule` (`id_cart_rule`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_cart_cart_rule: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_cart_cart_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_cart_cart_rule` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_cart_product
DROP TABLE IF EXISTS `ps_cart_product`;
CREATE TABLE IF NOT EXISTS `ps_cart_product` (
  `id_cart` int(10) unsigned NOT NULL,
  `id_product` int(10) unsigned NOT NULL,
  `id_address_delivery` int(10) unsigned DEFAULT '0',
  `id_shop` int(10) unsigned NOT NULL DEFAULT '1',
  `id_product_attribute` int(10) unsigned DEFAULT NULL,
  `quantity` int(10) unsigned NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  KEY `cart_product_index` (`id_cart`,`id_product`),
  KEY `id_product_attribute` (`id_product_attribute`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_cart_product: ~16 rows (approximately)
/*!40000 ALTER TABLE `ps_cart_product` DISABLE KEYS */;
INSERT INTO `ps_cart_product` (`id_cart`, `id_product`, `id_address_delivery`, `id_shop`, `id_product_attribute`, `quantity`, `date_add`) VALUES
	(1, 2, 3, 1, 10, 1, '0000-00-00 00:00:00'),
	(1, 3, 3, 1, 13, 1, '0000-00-00 00:00:00'),
	(2, 2, 3, 1, 10, 1, '0000-00-00 00:00:00'),
	(2, 6, 3, 1, 32, 1, '0000-00-00 00:00:00'),
	(2, 7, 3, 1, 34, 1, '0000-00-00 00:00:00'),
	(3, 2, 3, 1, 10, 1, '0000-00-00 00:00:00'),
	(3, 6, 3, 1, 32, 1, '0000-00-00 00:00:00'),
	(3, 1, 3, 1, 1, 1, '0000-00-00 00:00:00'),
	(4, 1, 3, 1, 1, 1, '0000-00-00 00:00:00'),
	(4, 3, 3, 1, 13, 1, '0000-00-00 00:00:00'),
	(4, 7, 3, 1, 34, 1, '0000-00-00 00:00:00'),
	(4, 5, 3, 1, 19, 1, '0000-00-00 00:00:00'),
	(5, 1, 3, 1, 1, 1, '0000-00-00 00:00:00'),
	(5, 2, 3, 1, 7, 1, '0000-00-00 00:00:00'),
	(5, 3, 3, 1, 13, 1, '0000-00-00 00:00:00'),
	(6, 3, 0, 1, 13, 1, '2015-05-12 17:45:04');
/*!40000 ALTER TABLE `ps_cart_product` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_cart_rule
DROP TABLE IF EXISTS `ps_cart_rule`;
CREATE TABLE IF NOT EXISTS `ps_cart_rule` (
  `id_cart_rule` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_customer` int(10) unsigned NOT NULL DEFAULT '0',
  `date_from` datetime NOT NULL,
  `date_to` datetime NOT NULL,
  `description` text,
  `quantity` int(10) unsigned NOT NULL DEFAULT '0',
  `quantity_per_user` int(10) unsigned NOT NULL DEFAULT '0',
  `priority` int(10) unsigned NOT NULL DEFAULT '1',
  `partial_use` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `code` varchar(254) NOT NULL,
  `minimum_amount` decimal(17,2) NOT NULL DEFAULT '0.00',
  `minimum_amount_tax` tinyint(1) NOT NULL DEFAULT '0',
  `minimum_amount_currency` int(10) unsigned NOT NULL DEFAULT '0',
  `minimum_amount_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `country_restriction` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `carrier_restriction` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `group_restriction` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `cart_rule_restriction` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `product_restriction` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `shop_restriction` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `free_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `reduction_percent` decimal(5,2) NOT NULL DEFAULT '0.00',
  `reduction_amount` decimal(17,2) NOT NULL DEFAULT '0.00',
  `reduction_tax` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `reduction_currency` int(10) unsigned NOT NULL DEFAULT '0',
  `reduction_product` int(10) NOT NULL DEFAULT '0',
  `gift_product` int(10) unsigned NOT NULL DEFAULT '0',
  `gift_product_attribute` int(10) unsigned NOT NULL DEFAULT '0',
  `highlight` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_cart_rule`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_cart_rule: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_cart_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_cart_rule` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_cart_rule_carrier
DROP TABLE IF EXISTS `ps_cart_rule_carrier`;
CREATE TABLE IF NOT EXISTS `ps_cart_rule_carrier` (
  `id_cart_rule` int(10) unsigned NOT NULL,
  `id_carrier` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_cart_rule`,`id_carrier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_cart_rule_carrier: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_cart_rule_carrier` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_cart_rule_carrier` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_cart_rule_combination
DROP TABLE IF EXISTS `ps_cart_rule_combination`;
CREATE TABLE IF NOT EXISTS `ps_cart_rule_combination` (
  `id_cart_rule_1` int(10) unsigned NOT NULL,
  `id_cart_rule_2` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_cart_rule_1`,`id_cart_rule_2`),
  KEY `id_cart_rule_1` (`id_cart_rule_1`),
  KEY `id_cart_rule_2` (`id_cart_rule_2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_cart_rule_combination: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_cart_rule_combination` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_cart_rule_combination` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_cart_rule_country
DROP TABLE IF EXISTS `ps_cart_rule_country`;
CREATE TABLE IF NOT EXISTS `ps_cart_rule_country` (
  `id_cart_rule` int(10) unsigned NOT NULL,
  `id_country` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_cart_rule`,`id_country`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_cart_rule_country: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_cart_rule_country` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_cart_rule_country` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_cart_rule_group
DROP TABLE IF EXISTS `ps_cart_rule_group`;
CREATE TABLE IF NOT EXISTS `ps_cart_rule_group` (
  `id_cart_rule` int(10) unsigned NOT NULL,
  `id_group` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_cart_rule`,`id_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_cart_rule_group: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_cart_rule_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_cart_rule_group` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_cart_rule_lang
DROP TABLE IF EXISTS `ps_cart_rule_lang`;
CREATE TABLE IF NOT EXISTS `ps_cart_rule_lang` (
  `id_cart_rule` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(254) NOT NULL,
  PRIMARY KEY (`id_cart_rule`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_cart_rule_lang: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_cart_rule_lang` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_cart_rule_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_cart_rule_product_rule
DROP TABLE IF EXISTS `ps_cart_rule_product_rule`;
CREATE TABLE IF NOT EXISTS `ps_cart_rule_product_rule` (
  `id_product_rule` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_product_rule_group` int(10) unsigned NOT NULL,
  `type` enum('products','categories','attributes','manufacturers','suppliers') NOT NULL,
  PRIMARY KEY (`id_product_rule`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_cart_rule_product_rule: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_cart_rule_product_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_cart_rule_product_rule` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_cart_rule_product_rule_group
DROP TABLE IF EXISTS `ps_cart_rule_product_rule_group`;
CREATE TABLE IF NOT EXISTS `ps_cart_rule_product_rule_group` (
  `id_product_rule_group` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cart_rule` int(10) unsigned NOT NULL,
  `quantity` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_product_rule_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_cart_rule_product_rule_group: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_cart_rule_product_rule_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_cart_rule_product_rule_group` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_cart_rule_product_rule_value
DROP TABLE IF EXISTS `ps_cart_rule_product_rule_value`;
CREATE TABLE IF NOT EXISTS `ps_cart_rule_product_rule_value` (
  `id_product_rule` int(10) unsigned NOT NULL,
  `id_item` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_product_rule`,`id_item`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_cart_rule_product_rule_value: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_cart_rule_product_rule_value` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_cart_rule_product_rule_value` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_cart_rule_shop
DROP TABLE IF EXISTS `ps_cart_rule_shop`;
CREATE TABLE IF NOT EXISTS `ps_cart_rule_shop` (
  `id_cart_rule` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_cart_rule`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_cart_rule_shop: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_cart_rule_shop` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_cart_rule_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_category
DROP TABLE IF EXISTS `ps_category`;
CREATE TABLE IF NOT EXISTS `ps_category` (
  `id_category` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_parent` int(10) unsigned NOT NULL,
  `id_shop_default` int(10) unsigned NOT NULL DEFAULT '1',
  `level_depth` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `nleft` int(10) unsigned NOT NULL DEFAULT '0',
  `nright` int(10) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `is_root_category` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_category`),
  KEY `category_parent` (`id_parent`),
  KEY `nleftright` (`nleft`,`nright`),
  KEY `nleftrightactive` (`nleft`,`nright`,`active`),
  KEY `level_depth` (`level_depth`),
  KEY `nright` (`nright`),
  KEY `nleft` (`nleft`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_category: ~18 rows (approximately)
/*!40000 ALTER TABLE `ps_category` DISABLE KEYS */;
INSERT INTO `ps_category` (`id_category`, `id_parent`, `id_shop_default`, `level_depth`, `nleft`, `nright`, `active`, `date_add`, `date_upd`, `position`, `is_root_category`) VALUES
	(1, 0, 1, 0, 1, 36, 1, '2015-05-08 13:59:53', '2015-05-08 13:59:53', 0, 0),
	(2, 1, 1, 1, 2, 35, 1, '2015-05-08 13:59:53', '2015-05-08 13:59:53', 0, 1),
	(3, 2, 1, 2, 3, 34, 1, '2015-05-08 14:00:21', '2015-05-12 11:27:19', 1, 0),
	(4, 3, 1, 3, 4, 11, 1, '2015-05-08 14:00:21', '2015-05-12 09:36:46', 1, 0),
	(5, 4, 1, 4, 5, 6, 1, '2015-05-08 14:00:21', '2015-05-12 09:40:37', 1, 0),
	(6, 4, 1, 4, 7, 8, 0, '2015-05-08 14:00:21', '2015-05-08 14:00:21', 0, 0),
	(7, 4, 1, 4, 9, 10, 1, '2015-05-08 14:00:21', '2015-05-12 09:41:37', 3, 0),
	(8, 3, 1, 3, 12, 19, 1, '2015-05-08 14:00:21', '2015-05-12 09:44:21', 2, 0),
	(9, 8, 1, 4, 13, 14, 1, '2015-05-08 14:00:21', '2015-05-12 09:45:43', 1, 0),
	(10, 8, 1, 4, 15, 16, 1, '2015-05-08 14:00:21', '2015-05-12 09:46:23', 2, 0),
	(11, 8, 1, 4, 17, 18, 0, '2015-05-08 14:00:21', '2015-05-12 09:44:31', 3, 0),
	(16, 3, 1, 3, 20, 25, 1, '2015-05-12 10:16:52', '2015-05-12 10:16:52', 0, 0),
	(17, 16, 1, 4, 21, 22, 1, '2015-05-12 10:17:30', '2015-05-12 10:17:30', 0, 0),
	(18, 16, 1, 4, 23, 24, 1, '2015-05-12 10:17:59', '2015-05-12 10:17:59', 0, 0),
	(19, 3, 1, 3, 26, 33, 1, '2015-05-12 10:19:10', '2015-05-12 10:19:10', 0, 0),
	(20, 19, 1, 4, 27, 28, 1, '2015-05-12 10:19:31', '2015-05-12 10:19:31', 0, 0),
	(21, 19, 1, 4, 29, 30, 1, '2015-05-12 10:19:59', '2015-05-12 10:19:59', 0, 0),
	(22, 19, 1, 4, 31, 32, 1, '2015-05-12 10:20:20', '2015-05-12 10:20:20', 0, 0);
/*!40000 ALTER TABLE `ps_category` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_category_group
DROP TABLE IF EXISTS `ps_category_group`;
CREATE TABLE IF NOT EXISTS `ps_category_group` (
  `id_category` int(10) unsigned NOT NULL,
  `id_group` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_category`,`id_group`),
  KEY `id_category` (`id_category`),
  KEY `id_group` (`id_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_category_group: ~52 rows (approximately)
/*!40000 ALTER TABLE `ps_category_group` DISABLE KEYS */;
INSERT INTO `ps_category_group` (`id_category`, `id_group`) VALUES
	(2, 0),
	(2, 1),
	(2, 2),
	(2, 3),
	(3, 1),
	(3, 2),
	(3, 3),
	(4, 1),
	(4, 2),
	(4, 3),
	(5, 1),
	(5, 2),
	(5, 3),
	(6, 1),
	(6, 2),
	(6, 3),
	(7, 1),
	(7, 2),
	(7, 3),
	(8, 1),
	(8, 2),
	(8, 3),
	(9, 1),
	(9, 2),
	(9, 3),
	(10, 1),
	(10, 2),
	(10, 3),
	(11, 1),
	(11, 2),
	(11, 3),
	(16, 1),
	(16, 2),
	(16, 3),
	(17, 1),
	(17, 2),
	(17, 3),
	(18, 1),
	(18, 2),
	(18, 3),
	(19, 1),
	(19, 2),
	(19, 3),
	(20, 1),
	(20, 2),
	(20, 3),
	(21, 1),
	(21, 2),
	(21, 3),
	(22, 1),
	(22, 2),
	(22, 3);
/*!40000 ALTER TABLE `ps_category_group` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_category_lang
DROP TABLE IF EXISTS `ps_category_lang`;
CREATE TABLE IF NOT EXISTS `ps_category_lang` (
  `id_category` int(10) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text,
  `link_rewrite` varchar(128) NOT NULL,
  `meta_title` varchar(128) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_category`,`id_shop`,`id_lang`),
  KEY `category_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_category_lang: ~18 rows (approximately)
/*!40000 ALTER TABLE `ps_category_lang` DISABLE KEYS */;
INSERT INTO `ps_category_lang` (`id_category`, `id_shop`, `id_lang`, `name`, `description`, `link_rewrite`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
	(1, 1, 1, 'Raíz', '', 'raiz', '', '', ''),
	(2, 1, 1, 'Inicio', '', 'inicio', '', '', ''),
	(3, 1, 1, 'Vestuario', '', 'vestuario', '', '', ''),
	(4, 1, 1, 'Camisas', '', 'camisas', '', '', ''),
	(5, 1, 1, 'Manga larga', '', 'manga-larga', '', '', ''),
	(6, 1, 1, 'Tops', 'Te ofrecemos una amplia variedad de tops para que puedas escoger el que mejor te quede.', 'top', '', '', ''),
	(7, 1, 1, 'Manga corta', '<p>Combina tus blusas preferidas con los accesorios perfectos para un look deslumbrante.</p>', 'manga-corta', '', '', ''),
	(8, 1, 1, 'Poleras', '', 'poleras', '', '', ''),
	(9, 1, 1, 'Manga larga', '', 'manga-larga', '', '', ''),
	(10, 1, 1, 'Manga corta', '', 'manga-corta', '', '', ''),
	(11, 1, 1, 'Vestidos de verano', 'Cortos, largos, de seda, estampados... aquí encontrarás el vestido perfecto para el verano.', 'vestidos-verano', '', '', ''),
	(16, 1, 1, 'Chaquetas', '', 'chaquetas', '', '', ''),
	(17, 1, 1, 'Parkas', '', 'parkas', '', '', ''),
	(18, 1, 1, 'Blazer', '', 'blazer', '', '', ''),
	(19, 1, 1, 'Sweaters & Polerones', '', 'sweaters-polerones', '', '', ''),
	(20, 1, 1, 'Cardigan', '', 'cardigan', '', '', ''),
	(21, 1, 1, 'Cuello redondo', '', 'cuello-redondo', '', '', ''),
	(22, 1, 1, 'Cuello tortuga', '', 'cuello-tortuga', '', '', '');
/*!40000 ALTER TABLE `ps_category_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_category_product
DROP TABLE IF EXISTS `ps_category_product`;
CREATE TABLE IF NOT EXISTS `ps_category_product` (
  `id_category` int(10) unsigned NOT NULL,
  `id_product` int(10) unsigned NOT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_category`,`id_product`),
  KEY `id_product` (`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_category_product: ~39 rows (approximately)
/*!40000 ALTER TABLE `ps_category_product` DISABLE KEYS */;
INSERT INTO `ps_category_product` (`id_category`, `id_product`, `position`) VALUES
	(2, 1, 0),
	(2, 2, 1),
	(2, 3, 2),
	(2, 4, 3),
	(2, 5, 4),
	(2, 6, 5),
	(2, 7, 6),
	(3, 1, 0),
	(3, 2, 1),
	(3, 3, 2),
	(3, 4, 3),
	(3, 5, 4),
	(3, 6, 5),
	(3, 7, 6),
	(4, 1, 0),
	(4, 2, 1),
	(5, 1, 0),
	(7, 1, 1),
	(7, 2, 0),
	(8, 1, 5),
	(8, 3, 0),
	(8, 4, 1),
	(8, 5, 2),
	(8, 6, 3),
	(8, 7, 4),
	(9, 1, 1),
	(9, 3, 0),
	(10, 1, 1),
	(10, 4, 0),
	(11, 5, 0),
	(11, 6, 1),
	(11, 7, 2),
	(16, 1, 0),
	(17, 1, 0),
	(18, 1, 0),
	(19, 1, 0),
	(20, 1, 0),
	(21, 1, 0),
	(22, 1, 0);
/*!40000 ALTER TABLE `ps_category_product` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_category_shop
DROP TABLE IF EXISTS `ps_category_shop`;
CREATE TABLE IF NOT EXISTS `ps_category_shop` (
  `id_category` int(11) NOT NULL,
  `id_shop` int(11) NOT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_category`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_category_shop: ~18 rows (approximately)
/*!40000 ALTER TABLE `ps_category_shop` DISABLE KEYS */;
INSERT INTO `ps_category_shop` (`id_category`, `id_shop`, `position`) VALUES
	(1, 1, 1),
	(2, 1, 1),
	(3, 1, 1),
	(4, 1, 1),
	(5, 1, 1),
	(6, 1, 2),
	(7, 1, 3),
	(8, 1, 2),
	(9, 1, 1),
	(10, 1, 2),
	(11, 1, 3),
	(16, 1, 3),
	(17, 1, 1),
	(18, 1, 2),
	(19, 1, 4),
	(20, 1, 1),
	(21, 1, 2),
	(22, 1, 3);
/*!40000 ALTER TABLE `ps_category_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_cms
DROP TABLE IF EXISTS `ps_cms`;
CREATE TABLE IF NOT EXISTS `ps_cms` (
  `id_cms` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cms_category` int(10) unsigned NOT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `indexation` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_cms`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_cms: ~5 rows (approximately)
/*!40000 ALTER TABLE `ps_cms` DISABLE KEYS */;
INSERT INTO `ps_cms` (`id_cms`, `id_cms_category`, `position`, `active`, `indexation`) VALUES
	(1, 1, 0, 1, 0),
	(2, 1, 1, 1, 0),
	(3, 1, 2, 1, 0),
	(4, 1, 3, 1, 0),
	(5, 1, 4, 1, 0);
/*!40000 ALTER TABLE `ps_cms` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_cms_block
DROP TABLE IF EXISTS `ps_cms_block`;
CREATE TABLE IF NOT EXISTS `ps_cms_block` (
  `id_cms_block` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cms_category` int(10) unsigned NOT NULL,
  `location` tinyint(1) unsigned NOT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `display_store` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_cms_block`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_cms_block: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_cms_block` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_cms_block` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_cms_block_lang
DROP TABLE IF EXISTS `ps_cms_block_lang`;
CREATE TABLE IF NOT EXISTS `ps_cms_block_lang` (
  `id_cms_block` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(40) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_cms_block`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_cms_block_lang: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_cms_block_lang` DISABLE KEYS */;
INSERT INTO `ps_cms_block_lang` (`id_cms_block`, `id_lang`, `name`) VALUES
	(1, 1, 'Información');
/*!40000 ALTER TABLE `ps_cms_block_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_cms_block_page
DROP TABLE IF EXISTS `ps_cms_block_page`;
CREATE TABLE IF NOT EXISTS `ps_cms_block_page` (
  `id_cms_block_page` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cms_block` int(10) unsigned NOT NULL,
  `id_cms` int(10) unsigned NOT NULL,
  `is_category` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id_cms_block_page`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_cms_block_page: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_cms_block_page` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_cms_block_page` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_cms_block_shop
DROP TABLE IF EXISTS `ps_cms_block_shop`;
CREATE TABLE IF NOT EXISTS `ps_cms_block_shop` (
  `id_cms_block` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_cms_block`,`id_shop`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_cms_block_shop: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_cms_block_shop` DISABLE KEYS */;
INSERT INTO `ps_cms_block_shop` (`id_cms_block`, `id_shop`) VALUES
	(1, 1);
/*!40000 ALTER TABLE `ps_cms_block_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_cms_category
DROP TABLE IF EXISTS `ps_cms_category`;
CREATE TABLE IF NOT EXISTS `ps_cms_category` (
  `id_cms_category` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_parent` int(10) unsigned NOT NULL,
  `level_depth` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_cms_category`),
  KEY `category_parent` (`id_parent`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_cms_category: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_cms_category` DISABLE KEYS */;
INSERT INTO `ps_cms_category` (`id_cms_category`, `id_parent`, `level_depth`, `active`, `date_add`, `date_upd`, `position`) VALUES
	(1, 0, 1, 1, '2015-05-08 13:59:53', '2015-05-08 13:59:53', 0);
/*!40000 ALTER TABLE `ps_cms_category` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_cms_category_lang
DROP TABLE IF EXISTS `ps_cms_category_lang`;
CREATE TABLE IF NOT EXISTS `ps_cms_category_lang` (
  `id_cms_category` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL DEFAULT '1',
  `name` varchar(128) NOT NULL,
  `description` text,
  `link_rewrite` varchar(128) NOT NULL,
  `meta_title` varchar(128) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_cms_category`,`id_shop`,`id_lang`),
  KEY `category_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_cms_category_lang: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_cms_category_lang` DISABLE KEYS */;
INSERT INTO `ps_cms_category_lang` (`id_cms_category`, `id_lang`, `id_shop`, `name`, `description`, `link_rewrite`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
	(1, 1, 1, 'Inicio', '', 'inicio', '', '', '');
/*!40000 ALTER TABLE `ps_cms_category_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_cms_category_shop
DROP TABLE IF EXISTS `ps_cms_category_shop`;
CREATE TABLE IF NOT EXISTS `ps_cms_category_shop` (
  `id_cms_category` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_cms_category`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_cms_category_shop: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_cms_category_shop` DISABLE KEYS */;
INSERT INTO `ps_cms_category_shop` (`id_cms_category`, `id_shop`) VALUES
	(1, 1);
/*!40000 ALTER TABLE `ps_cms_category_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_cms_lang
DROP TABLE IF EXISTS `ps_cms_lang`;
CREATE TABLE IF NOT EXISTS `ps_cms_lang` (
  `id_cms` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL DEFAULT '1',
  `meta_title` varchar(128) NOT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `content` longtext,
  `link_rewrite` varchar(128) NOT NULL,
  PRIMARY KEY (`id_cms`,`id_shop`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_cms_lang: ~5 rows (approximately)
/*!40000 ALTER TABLE `ps_cms_lang` DISABLE KEYS */;
INSERT INTO `ps_cms_lang` (`id_cms`, `id_lang`, `id_shop`, `meta_title`, `meta_description`, `meta_keywords`, `content`, `link_rewrite`) VALUES
	(1, 1, 1, 'Envío', 'Nuestros términos y condiciones de envío', 'condiciones, entrega, plazo, envío, paquete', '<h2>Envíos y devoluciones</h2><h3>Envío del paquete</h3><p>Como norma general, los paquetes se envían dentro de las 48 horas siguientes a la recepción del pago, través de UPS con número de seguimiento y entrega sin firma. Si prefieres el envío certificado mediante UPS Extra, se aplicará un cargo adicional. Ponte en contacto con nosotros antes de solicitar esta opción. Sea cual sea la forma de envío que elijas, te proporcionaremos un enlace para que puedas seguir tu pedido en línea.</p><p>Los gastos de envío incluyen los gastos de manipulación y empaquetado, así como los gastos postales. Los gastos de manipulación tienen un precio fijo, mientras que los gastos de transporte pueden variar según el peso total del paquete. Te aconsejamos que agrupes todos tus artículos en un mismo pedido. No podemos combinar dos pedidos diferentes, y los gastos de envío se aplicarán para cada uno de manera individual. No nos hacemos responsables de los daños que pueda sufrir tu paquete tras el envío, pero hacemos todo lo posible para proteger todos los artículos frágiles.<br /><br />Las cajas son grandes y tus artículos estarán bien protegidos.</p>', 'entrega'),
	(2, 1, 1, 'Aviso legal', 'Aviso legal', 'aviso, legal, créditos', '<h2>Legal</h2><h3>Créditos</h3><p>Concepto y producción:</p><p>Esta tienda online fue creada utilizando el <a href="http://www.prestashop.com">Software Prestashop Shopping Cart</a>. No olvides echarle un vistazo al <a href="http://www.prestashop.com/blog/en/">blog de comercio electrónico</a> de PrestaShop para estar al día y leer todos los consejos sobre la venta online y sobre cómo gestionar tu web de comercio electrónico.</p>', 'aviso-legal'),
	(3, 1, 1, 'Términos y condiciones', 'Nuestros términos y condiciones', 'condiciones, términos, uso, venta', '<h1 class="page-heading">Términos y condiciones</h1>\n<h3 class="page-subheading">Norma 1</h3>\n<p class="bottom-indent">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n<h3 class="page-subheading">Norma 2</h3>\n<p class="bottom-indent">Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam&#1102;</p>\n<h3 class="page-subheading">Norma 3</h3>\n<p class="bottom-indent">Tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam&#1102;</p>', 'terminos-y-condiciones-de-uso'),
	(4, 1, 1, 'Sobre nosotros', 'Averigüe más sobre nosotros', 'sobre nosotros, información', '<h1 class="page-heading bottom-indent">Sobre nosotros</h1>\n<div class="row">\n<div class="col-xs-12 col-sm-4">\n<div class="cms-block">\n<h3 class="page-subheading">Nuestra empresa</h3>\n<p><strong class="dark">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididun.</strong></p>\n<p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam. Lorem ipsum dolor sit amet conse ctetur adipisicing elit.</p>\n<ul class="list-1">\n<li><em class="icon-ok"></em>Productos de alta calidad</li>\n<li><em class="icon-ok"></em>El mejor servicio de atención al cliente</li>\n<li><em class="icon-ok"></em>Garantía de devolución en 30 días</li>\n</ul>\n</div>\n</div>\n<div class="col-xs-12 col-sm-4">\n<div class="cms-box">\n<h3 class="page-subheading">Nuestro equipo</h3>\n<img title="cms-img" src="../img/cms/cms-img.jpg" alt="cms-img" width="370" height="192" />\n<p><strong class="dark">Lorem set sint occaecat cupidatat non </strong></p>\n<p>Eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>\n</div>\n</div>\n<div class="col-xs-12 col-sm-4">\n<div class="cms-box">\n<h3 class="page-subheading">Opiniones</h3>\n<div class="testimonials">\n<div class="inner"><span class="before">“</span>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim.<span class="after">”</span></div>\n</div>\n<p><strong class="dark">Lorem ipsum dolor sit</strong></p>\n<div class="testimonials">\n<div class="inner"><span class="before">“</span>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet conse ctetur adipisicing elit. Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod.<span class="after">”</span></div>\n</div>\n<p><strong class="dark">Ipsum dolor sit</strong></p>\n</div>\n</div>\n</div>', 'sobre-nosotros'),
	(5, 1, 1, 'Pago seguro', 'Nuestra forma de pago segura', 'pago seguro, ssl, visa, mastercard, paypal', '<h2>Pago seguro</h2>\n<h3>Nuestro pago seguro</h3><p>Con SSL</p>\n<h3>Utilizando Visa/Mastercard/Paypal</h3><p>Sobre este servicio</p>', 'pago-seguro');
/*!40000 ALTER TABLE `ps_cms_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_cms_shop
DROP TABLE IF EXISTS `ps_cms_shop`;
CREATE TABLE IF NOT EXISTS `ps_cms_shop` (
  `id_cms` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_cms`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_cms_shop: ~5 rows (approximately)
/*!40000 ALTER TABLE `ps_cms_shop` DISABLE KEYS */;
INSERT INTO `ps_cms_shop` (`id_cms`, `id_shop`) VALUES
	(1, 1),
	(2, 1),
	(3, 1),
	(4, 1),
	(5, 1);
/*!40000 ALTER TABLE `ps_cms_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_compare
DROP TABLE IF EXISTS `ps_compare`;
CREATE TABLE IF NOT EXISTS `ps_compare` (
  `id_compare` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_customer` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_compare`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_compare: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_compare` DISABLE KEYS */;
INSERT INTO `ps_compare` (`id_compare`, `id_customer`) VALUES
	(1, 0);
/*!40000 ALTER TABLE `ps_compare` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_compare_product
DROP TABLE IF EXISTS `ps_compare_product`;
CREATE TABLE IF NOT EXISTS `ps_compare_product` (
  `id_compare` int(10) unsigned NOT NULL,
  `id_product` int(10) unsigned NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_compare`,`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_compare_product: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_compare_product` DISABLE KEYS */;
INSERT INTO `ps_compare_product` (`id_compare`, `id_product`, `date_add`, `date_upd`) VALUES
	(1, 1, '2015-05-08 17:19:01', '2015-05-08 17:19:01');
/*!40000 ALTER TABLE `ps_compare_product` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_condition
DROP TABLE IF EXISTS `ps_condition`;
CREATE TABLE IF NOT EXISTS `ps_condition` (
  `id_condition` int(11) NOT NULL AUTO_INCREMENT,
  `id_ps_condition` int(11) NOT NULL,
  `type` enum('configuration','install','sql') NOT NULL,
  `request` text,
  `operator` varchar(32) DEFAULT NULL,
  `value` varchar(64) DEFAULT NULL,
  `result` varchar(64) DEFAULT NULL,
  `calculation_type` enum('hook','time') DEFAULT NULL,
  `calculation_detail` varchar(64) DEFAULT NULL,
  `validated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_condition`,`id_ps_condition`)
) ENGINE=InnoDB AUTO_INCREMENT=247 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_condition: ~244 rows (approximately)
/*!40000 ALTER TABLE `ps_condition` DISABLE KEYS */;
INSERT INTO `ps_condition` (`id_condition`, `id_ps_condition`, `type`, `request`, `operator`, `value`, `result`, `calculation_type`, `calculation_detail`, `validated`, `date_add`, `date_upd`) VALUES
	(1, 159, 'install', '', '<=', '90', '1', 'time', '2', 1, '2015-05-12 11:52:26', '2015-05-12 11:52:26'),
	(2, 158, 'install', '', '>=', '90', '', 'time', '2', 0, '2015-05-12 11:52:26', '2015-05-12 11:52:26'),
	(3, 19, 'install', '', '>', '0', '1', 'time', '1', 1, '2015-05-12 11:52:26', '2015-05-12 11:52:26'),
	(4, 40, 'install', '', '>=', '730', '', 'time', '2', 0, '2015-05-12 11:52:26', '2015-05-12 11:52:26'),
	(5, 509, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%payulatam%"', '==', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-12 11:52:47', '2015-05-12 11:52:47'),
	(6, 55, 'sql', 'SELECT COUNT(*) FROM ps_orders WHERE reference NOT IN ("XKBKNABJK", "OHSATSERP", "FFATNOMMJ", "UOYEVOLI", "KHWLILZLL")', '>=', '100', '0', 'hook', 'actionObjectOrderAddAfter', 0, '2015-05-12 11:52:26', '2015-05-12 11:52:26'),
	(7, 12, 'sql', 'SELECT COUNT(*) FROM PREFIX_product WHERE reference NOT LIKE "demo_%"', '>', '99', '0', 'hook', 'actionObjectProductAddAfter', 0, '2015-05-12 11:52:26', '2015-05-12 11:52:26'),
	(8, 39, 'install', '', '>=', '365', '', 'time', '2', 0, '2015-05-12 11:52:26', '2015-05-12 11:52:27'),
	(9, 1, 'configuration', 'PS_REWRITING_SETTINGS', '==', '1', '1', 'hook', 'actionAdminMetaControllerUpdate_optionsAfter', 1, '2015-05-12 11:52:27', '2015-05-12 11:52:27'),
	(10, 2, 'configuration', 'PS_SMARTY_FORCE_COMPILE', '!=', '2', '1', 'hook', 'actionAdminPerformanceControllerSaveAfter', 1, '2015-05-12 11:52:27', '2015-05-12 11:52:27'),
	(11, 3, 'configuration', 'PS_CSS_THEME_CACHE', '==', '1', '', 'hook', 'actionAdminPerformanceControllerSaveAfter', 0, '2015-05-12 11:52:27', '2015-05-12 14:48:53'),
	(12, 4, 'configuration', 'PS_CIPHER_ALGORITHM', '==', '1', '1', 'hook', 'actionAdminPerformanceControllerSaveAfter', 1, '2015-05-12 11:52:27', '2015-05-12 11:52:27'),
	(13, 5, 'configuration', 'PS_MEDIA_SERVERS', '==', '1', '', 'hook', 'actionAdminPerformanceControllerSaveAfter', 0, '2015-05-12 11:52:27', '2015-05-12 14:48:53'),
	(14, 6, 'sql', 'SELECT COUNT(distinct m.id_module) FROM PREFIX_hook h LEFT JOIN PREFIX_hook_module hm ON h.id_hook = hm.id_hook LEFT JOIN PREFIX_module m ON hm.id_module = m.id_module\r\nWHERE (h.name = "displayPayment" OR h.name = "payment") AND m.name NOT IN ("bankwire", "cheque", "cashondelivery")', '>', '0', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-12 11:52:27', '2015-05-12 11:52:27'),
	(15, 7, 'sql', 'SELECT COUNT(distinct m.id_module) FROM PREFIX_hook h LEFT JOIN PREFIX_hook_module hm ON h.id_hook = hm.id_hook LEFT JOIN PREFIX_module m ON hm.id_module = m.id_module\r\nWHERE (h.name = "displayPayment" OR h.name = "payment") AND m.name NOT IN ("bankwire", "cheque", "cashondelivery")', '>', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-12 11:52:27', '2015-05-12 11:52:27'),
	(16, 8, 'sql', 'SELECT COUNT(*) FROM PREFIX_carrier WHERE name NOT IN ("0", "My carrier")', '>', '0', '0', 'hook', 'actionObjectCarrierAddAfter', 0, '2015-05-12 11:52:27', '2015-05-12 11:52:27'),
	(17, 9, 'sql', 'SELECT COUNT(*) FROM PREFIX_carrier WHERE name NOT IN ("0", "My carrier")', '>', '1', '0', 'hook', 'actionObjectCarrierAddAfter', 0, '2015-05-12 11:52:27', '2015-05-12 11:52:27'),
	(18, 10, 'sql', 'SELECT COUNT(*) FROM PREFIX_product WHERE reference NOT LIKE "demo_%"', '>', '0', '0', 'hook', 'actionObjectProductAddAfter', 0, '2015-05-12 11:52:27', '2015-05-12 11:52:27'),
	(19, 11, 'sql', 'SELECT COUNT(*) FROM PREFIX_product WHERE reference NOT LIKE "demo_%"', '>', '9', '0', 'hook', 'actionObjectProductAddAfter', 0, '2015-05-12 11:52:27', '2015-05-12 11:52:27'),
	(20, 16, 'configuration', 'PS_SHOP_PHONE', '!=', '0', '', 'hook', 'actionAdminStoresControllerUpdate_optionsAfter', 0, '2015-05-12 11:52:27', '2015-05-12 11:52:27'),
	(21, 17, 'sql', 'SELECT COUNT(*) FROM PREFIX_contact', '>', '2', '2', 'hook', 'actionObjectContactAddAfter', 0, '2015-05-12 11:52:27', '2015-05-12 11:52:27'),
	(22, 18, 'sql', 'SELECT COUNT(*) FROM PREFIX_contact', '>', '4', '2', 'hook', 'actionObjectContactAddAfter', 0, '2015-05-12 11:52:28', '2015-05-12 11:52:28'),
	(23, 13, 'sql', 'SELECT COUNT(*) FROM PREFIX_product WHERE reference NOT LIKE "demo_%"', '>', '999', '0', 'hook', 'actionObjectProductAddAfter', 0, '2015-05-12 11:52:28', '2015-05-12 11:52:28'),
	(24, 14, 'sql', 'SELECT COUNT(*) FROM PREFIX_product WHERE reference NOT LIKE "demo_%"', '>', '9999', '0', 'hook', 'actionObjectProductAddAfter', 0, '2015-05-12 11:52:28', '2015-05-12 11:52:28'),
	(25, 15, 'sql', 'SELECT COUNT(*) FROM PREFIX_product WHERE reference NOT LIKE "demo_%"', '>', '99999', '0', 'hook', 'actionObjectProductAddAfter', 0, '2015-05-12 11:52:28', '2015-05-12 11:52:28'),
	(26, 20, 'install', '', '>=', '7', '', 'time', '1', 0, '2015-05-12 11:52:28', '2015-05-12 11:52:28'),
	(27, 21, 'configuration', 'PS_LOGO', '!=', 'logo.jpg', '', 'hook', 'actionAdminThemesControllerUpdate_optionsAfter', 0, '2015-05-12 11:52:28', '2015-05-12 11:52:28'),
	(28, 22, 'sql', 'SELECT COUNT(*) FROM PREFIX_theme WHERE directory != "default" AND directory != "prestashop" AND directory ! "default-bootstrap"', '>', '0', '0', 'hook', 'actionObjectShopUpdateAfter', 0, '2015-05-12 11:52:28', '2015-05-12 11:52:28'),
	(29, 23, 'configuration', 'PS_LOGGED_ON_ADDONS', '==', '1', '', 'time', '1', 0, '2015-05-12 11:52:28', '2015-05-12 11:52:28'),
	(30, 24, 'configuration', 'PS_MULTISHOP_FEATURE_ACTIVE', '==', '1', '', 'hook', 'actionAdminPreferencesControllerUpdate_optionsAfter', 0, '2015-05-12 11:52:28', '2015-05-12 11:52:28'),
	(31, 25, 'sql', 'SELECT COUNT(*) FROM PREFIX_shop', '>', '1', '1', 'hook', 'actionObjectShopAddAfter', 0, '2015-05-12 11:52:28', '2015-05-12 11:52:28'),
	(32, 28, 'sql', 'SELECT COUNT(*) FROM PREFIX_shop_group', '>', '1', '1', 'hook', 'actionObjectShopGroupAddAfter', 0, '2015-05-12 11:52:28', '2015-05-12 11:52:28'),
	(33, 26, 'sql', 'SELECT COUNT(*) FROM PREFIX_shop', '>', '4', '1', 'hook', 'actionObjectShopAddAfter', 0, '2015-05-12 11:52:28', '2015-05-12 11:52:28'),
	(34, 27, 'sql', 'SELECT COUNT(*) FROM PREFIX_shop_group', '>', '5', '1', 'hook', 'actionObjectShopGroupAddAfter 	', 0, '2015-05-12 11:52:28', '2015-05-12 11:52:28'),
	(35, 30, 'sql', 'SELECT COUNT(*) FROM PREFIX_carrier WHERE name NOT IN ("0", "My carrier")', '>', '2', '0', 'hook', 'actionObjectCarrierAddAfter', 0, '2015-05-12 11:52:29', '2015-05-12 11:52:29'),
	(36, 29, 'sql', 'SELECT COUNT(distinct m.id_module) FROM PREFIX_hook h LEFT JOIN PREFIX_hook_module hm ON h.id_hook = hm.id_hook LEFT JOIN PREFIX_module m ON hm.id_module = m.id_module\r\nWHERE (h.name = "displayPayment" OR h.name = "payment") AND m.name NOT IN ("bankwire", "cheque", "cashondelivery")', '>', '2', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-12 11:52:29', '2015-05-12 11:52:29'),
	(37, 31, 'sql', 'SELECT SUM(total_paid_tax_excl / c.conversion_rate)\r\nFROM PREFIX_orders o INNER JOIN PREFIX_currency c ON c.id_currency = o.id_currency WHERE valid = 1 AND reference != "XKBKNABJK"', '>=', '200', '0', 'hook', 'actionOrderStatusUpdate', 0, '2015-05-12 11:52:29', '2015-05-12 11:52:29'),
	(38, 32, 'sql', ' 	SELECT SUM(total_paid_tax_excl / c.conversion_rate) FROM PREFIX_orders o INNER JOIN PREFIX_currency c ON c.id_currency = o.id_currency WHERE valid = 1 AND reference != "XKBKNABJK"', '>=', '2000', '0', 'hook', 'actionOrderStatusUpdate', 0, '2015-05-12 11:52:29', '2015-05-12 11:52:29'),
	(39, 33, 'sql', ' 	SELECT SUM(total_paid_tax_excl / c.conversion_rate) FROM PREFIX_orders o INNER JOIN PREFIX_currency c ON c.id_currency = o.id_currency WHERE valid = 1 AND reference != "XKBKNABJK"', '>=', '10000', '0', 'time', '1', 0, '2015-05-12 11:52:27', '2015-05-12 11:52:27'),
	(40, 34, 'sql', ' 	SELECT SUM(total_paid_tax_excl / c.conversion_rate) FROM PREFIX_orders o INNER JOIN PREFIX_currency c ON c.id_currency = o.id_currency WHERE valid = 1', '>=', '200000', '0', 'time', '7', 0, '2015-05-12 11:52:29', '2015-05-12 11:52:29'),
	(41, 35, 'sql', ' 	SELECT SUM(total_paid_tax_excl / c.conversion_rate) FROM PREFIX_orders o INNER JOIN PREFIX_currency c ON c.id_currency = o.id_currency WHERE valid = 1', '>=', '2000000', '0', 'time', '7', 0, '2015-05-12 11:52:29', '2015-05-12 11:52:29'),
	(42, 36, 'sql', ' 	SELECT SUM(total_paid_tax_excl / c.conversion_rate) FROM PREFIX_orders o INNER JOIN PREFIX_currency c ON c.id_currency = o.id_currency WHERE valid = 1', '>=', '20000000', '0', 'time', '7', 0, '2015-05-12 11:52:29', '2015-05-12 11:52:29'),
	(43, 37, 'install', '', '>=', '30', '', 'time', '1', 0, '2015-05-12 11:52:29', '2015-05-12 11:52:29'),
	(44, 38, 'install', '', '>=', '182', '', 'time', '2', 0, '2015-05-12 11:52:29', '2015-05-12 11:52:29'),
	(45, 41, 'sql', 'SELECT COUNT(*) FROM PREFIX_guest', '>=', '10', '7', 'time', '1', 0, '2015-05-12 11:52:29', '2015-05-12 11:52:29'),
	(46, 42, 'sql', 'SELECT COUNT(*) FROM PREFIX_guest', '>=', '100', '7', 'time', '1', 0, '2015-05-12 11:52:29', '2015-05-12 11:52:29'),
	(47, 43, 'sql', 'SELECT COUNT(*) FROM PREFIX_guest', '>=', '1000', '7', 'time', '1', 0, '2015-05-12 11:52:29', '2015-05-12 11:52:29'),
	(48, 44, 'sql', 'SELECT COUNT(*) FROM PREFIX_guest', '>=', '10000', '1', 'time', '2', 0, '2015-05-12 11:52:29', '2015-05-12 11:52:29'),
	(49, 45, 'sql', 'SELECT COUNT(*) FROM PREFIX_guest', '>=', '100000', '1', 'time', '3', 0, '2015-05-12 11:52:29', '2015-05-12 11:52:29'),
	(50, 46, 'sql', 'SELECT COUNT(*) FROM PREFIX_guest', '>=', '1000000', '1', 'time', '4', 0, '2015-05-12 11:52:30', '2015-05-12 11:52:30'),
	(51, 47, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart WHERE secure_key != "b44a6d9efd7a0076a0fbce6b15eaf3b1"', '>=', '2', '1', 'hook', 'actionObjectCartAddAfter', 0, '2015-05-12 11:52:30', '2015-05-12 16:17:04'),
	(52, 48, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart WHERE secure_key != "b44a6d9efd7a0076a0fbce6b15eaf3b1"', '>=', '10', '1', 'hook', 'actionObjectCartAddAfter', 0, '2015-05-12 11:52:30', '2015-05-12 16:17:04'),
	(53, 49, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart WHERE secure_key != "b44a6d9efd7a0076a0fbce6b15eaf3b1"', '>=', '100', '1', 'hook', 'actionObjectCartAddAfter', 0, '2015-05-12 11:52:30', '2015-05-12 16:17:04'),
	(54, 50, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart WHERE secure_key != "b44a6d9efd7a0076a0fbce6b15eaf3b1"', '>=', '1000', '0', 'time', '1', 0, '2015-05-12 11:52:30', '2015-05-12 11:52:30'),
	(55, 51, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart WHERE secure_key != "b44a6d9efd7a0076a0fbce6b15eaf3b1"', '>=', '10000', '0', 'time', '4', 0, '2015-05-12 11:52:30', '2015-05-12 11:52:30'),
	(56, 52, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart WHERE secure_key != "b44a6d9efd7a0076a0fbce6b15eaf3b1"', '>=', '100000', '0', 'time', '8', 0, '2015-05-12 11:52:30', '2015-05-12 11:52:30'),
	(57, 53, 'sql', 'SELECT COUNT(*) FROM ps_orders WHERE reference NOT IN ("XKBKNABJK", "OHSATSERP", "FFATNOMMJ", "UOYEVOLI", "KHWLILZLL")', '>=', '1', '0', 'hook', 'actionObjectOrderAddAfter', 0, '2015-05-12 11:52:30', '2015-05-12 11:52:30'),
	(58, 54, 'sql', 'SELECT COUNT(*) FROM ps_orders WHERE reference NOT IN ("XKBKNABJK", "OHSATSERP", "FFATNOMMJ", "UOYEVOLI", "KHWLILZLL")', '>=', '10', '0', 'hook', 'actionObjectOrderAddAfter', 0, '2015-05-12 11:52:30', '2015-05-12 11:52:30'),
	(59, 56, 'sql', 'SELECT COUNT(*) FROM ps_orders WHERE reference NOT IN ("XKBKNABJK", "OHSATSERP", "FFATNOMMJ", "UOYEVOLI", "KHWLILZLL")', '>=', '1000', '0', 'time', '2', 0, '2015-05-12 11:52:27', '2015-05-12 11:52:27'),
	(60, 57, 'sql', 'SELECT COUNT(*) FROM ps_orders WHERE reference NOT IN ("XKBKNABJK", "OHSATSERP", "FFATNOMMJ", "UOYEVOLI", "KHWLILZLL")', '>=', '10000', '0', 'time', '4', 0, '2015-05-12 11:52:30', '2015-05-12 11:52:30'),
	(61, 58, 'sql', 'SELECT COUNT(*) FROM ps_orders WHERE reference NOT IN ("XKBKNABJK", "OHSATSERP", "FFATNOMMJ", "UOYEVOLI", "KHWLILZLL")', '>=', '100000', '0', 'time', '8', 0, '2015-05-12 11:52:30', '2015-05-12 11:52:30'),
	(62, 65, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer_thread', '>=', '1', '0', 'hook', 'actionObjectCustomerThreadAddAfter', 0, '2015-05-12 11:52:30', '2015-05-12 11:52:30'),
	(63, 66, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer_thread', '>=', '10', '0', 'hook', 'actionObjectCustomerThreadAddAfter', 0, '2015-05-12 11:52:30', '2015-05-12 11:52:30'),
	(64, 67, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer_thread', '>=', '100', '0', 'hook', 'actionObjectCustomerThreadAddAfter', 0, '2015-05-12 11:52:30', '2015-05-12 11:52:30'),
	(65, 68, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer_thread', '>=', '1000', '0', 'time', '2', 0, '2015-05-12 11:52:31', '2015-05-12 11:52:31'),
	(66, 69, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer_thread', '>=', '10000', '0', 'time', '4', 0, '2015-05-12 11:52:31', '2015-05-12 11:52:31'),
	(67, 70, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer_thread', '>=', '100000', '0', 'time', '8', 0, '2015-05-12 11:52:31', '2015-05-12 11:52:31'),
	(68, 59, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer WHERE email != "pub@prestashop.com"', '>=', '1', '0', 'hook', 'actionObjectCustomerAddAfter', 0, '2015-05-12 11:52:31', '2015-05-12 11:52:31'),
	(69, 60, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer WHERE email != "pub@prestashop.com"', '>=', '10', '0', 'hook', 'actionObjectCustomerAddAfter', 0, '2015-05-12 11:52:31', '2015-05-12 11:52:31'),
	(70, 61, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer WHERE email != "pub@prestashop.com"', '>=', '100', '0', 'hook', 'actionObjectCustomerAddAfter', 0, '2015-05-12 11:52:27', '2015-05-12 11:52:27'),
	(71, 62, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer WHERE email != "pub@prestashop.com"', '>=', '1000', '0', 'time', '1', 0, '2015-05-12 11:52:31', '2015-05-12 11:52:31'),
	(72, 63, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer WHERE email != "pub@prestashop.com"', '>=', '10000', '0', 'time', '2', 0, '2015-05-12 11:52:31', '2015-05-12 11:52:31'),
	(73, 64, 'sql', 'SELECT COUNT(*) FROM PREFIX_customer WHERE email != "pub@prestashop.com"', '>=', '100000', '0', 'time', '4', 0, '2015-05-12 11:52:31', '2015-05-12 11:52:31'),
	(74, 76, 'sql', 'SELECT IFNULL(id_order, 0) FROM PREFIX_orders o LEFT JOIN PREFIX_address a ON o.id_address_delivery = a.id_address LEFT JOIN PREFIX_country c ON c.id_country = a.id_country WHERE o.valid = 1 AND a.id_country != "{config}PS_COUNTRY_DEFAULT{/config}" AND c.iso_code IN (\r\n"CA",\r\n"GL",\r\n"PM",\r\n"US"\r\n)', '!=', '0', '0', 'hook', 'actionOrderStatusUpdate', 0, '2015-05-12 11:52:31', '2015-05-12 11:52:31'),
	(75, 79, 'sql', 'SELECT IFNULL(id_order, 0) FROM PREFIX_orders o LEFT JOIN PREFIX_address a ON o.id_address_delivery = a.id_address LEFT JOIN PREFIX_country c ON c.id_country = a.id_country WHERE o.valid = 1 AND a.id_country != "{config}PS_COUNTRY_DEFAULT{/config}" AND c.iso_code IN (\r\n"UM",\r\n"AS",\r\n"AU",\r\n"CK",\r\n"FJ",\r\n"FM",\r\n"GU",\r\n"KI",\r\n"MH,"\r\n"MP",\r\n"NC",\r\n"NF",\r\n"NR",\r\n"NU",\r\n"NZ",\r\n"PF",\r\n"PG",\r\n"PN",\r\n"PW",\r\n"SB",\r\n"TK",\r\n"TO",\r\n"TV",\r\n"VU",\r\n"WF",\r\n"WS"\r\n)', '!=', '0', '0', 'hook', 'actionOrderStatusUpdate', 0, '2015-05-12 11:52:31', '2015-05-12 11:52:31'),
	(78, 85, 'sql', 'SELECT IFNULL(id_order, 0) FROM PREFIX_orders o LEFT JOIN PREFIX_address a ON o.id_address_delivery = a.id_address LEFT JOIN PREFIX_country c ON c.id_country = a.id_country WHERE o.valid = 1 AND a.id_country != "{config}PS_COUNTRY_DEFAULT{/config}" AND c.iso_code IN (\r\n"KG",\r\n"KZ",\r\n"TJ",\r\n"TM",\r\n"UZ",\r\n"AE",\r\n"AM",\r\n"AZ",\r\n"BH",\r\n"CY",\r\n"GE",\r\n"IL",\r\n"IQ",\r\n"IR",\r\n"JO",\r\n"KW",\r\n"LB",\r\n"OM",\r\n"QA",\r\n"SA",\r\n"SY",\r\n"TR",\r\n"YE",\r\n"AF",\r\n"BD",\r\n"BT",\r\n"IN",\r\n"IO",\r\n"LK",\r\n"MV",\r\n"NP",\r\n"PK",\r\n"CN",\r\n"HK",\r\n"JP",\r\n"KP",\r\n"KR",\r\n"MO",\r\n"TW",\r\n"MN",\r\n"BN",\r\n"CC",\r\n"CX",\r\n"ID",\r\n"KH",\r\n"LA",\r\n"MM",\r\n"MY",\r\n"PH",\r\n"SG",\r\n"TH",\r\n"TP",\r\n"VN"\r\n)', '!=', '0', '0', 'hook', 'actionOrderStatusUpdate', 0, '2015-05-12 11:52:31', '2015-05-12 11:52:31'),
	(79, 87, 'sql', 'SELECT IFNULL(id_order, 0) FROM PREFIX_orders o LEFT JOIN PREFIX_address a ON o.id_address_delivery = a.id_address LEFT JOIN PREFIX_country c ON c.id_country = a.id_country WHERE o.valid = 1 AND a.id_country != "{config}PS_COUNTRY_DEFAULT{/config}" AND c.iso_code IN (\r\n"BE",\r\n"DE",\r\n"FR",\r\n"FX",\r\n"GB",\r\n"IE",\r\n"LU",\r\n"MC",\r\n"NL",\r\n"IT",\r\n"MT",\r\n"SM",\r\n"VA",\r\n"AD",\r\n"ES",\r\n"GI",\r\n"PT",\r\n"BY",\r\n"EE",\r\n"LT",\r\n"LV",\r\n"MD",\r\n"PL",\r\n"UA",\r\n"AL",\r\n"BA",\r\n"BG",\r\n"GR",\r\n"HR",\r\n"MK",\r\n"RO",\r\n"SI",\r\n"YU",\r\n"RU",\r\n"AT",\r\n"CH",\r\n"CZ",\r\n"HU",\r\n"LI",\r\n"SK",\r\n"DK",\r\n"FI",\r\n"FO",\r\n"IS",\r\n"NO",\r\n"SE",\r\n"SJ"\r\n)', '!=', '0', '0', 'hook', 'actionOrderStatusUpdate', 0, '2015-05-12 11:52:32', '2015-05-12 11:52:32'),
	(80, 88, 'sql', 'SELECT IFNULL(id_order, 0) FROM PREFIX_orders o LEFT JOIN PREFIX_address a ON o.id_address_delivery = a.id_address LEFT JOIN PREFIX_country c ON c.id_country = a.id_country WHERE o.valid = 1 AND a.id_country != "{config}PS_COUNTRY_DEFAULT{/config}" AND c.iso_code IN (\r\n"BI",\r\n"CF",\r\n"CG",\r\n"RW",\r\n"TD",\r\n"ZR",\r\n"DJ",\r\n"ER",\r\n"ET",\r\n"KE",\r\n"SO",\r\n"TZ",\r\n"UG",\r\n"KM",\r\n"MG",\r\n"MU",\r\n"RE",\r\n"SC",\r\n"YT",\r\n"AO",\r\n"BW",\r\n"LS",\r\n"MW",\r\n"MZ",\r\n"NA",\r\n"SZ",\r\n"ZA",\r\n"ZM",\r\n"ZW",\r\n"BF",\r\n"BJ",\r\n"CI",\r\n"CM",\r\n"CV",\r\n"GA",\r\n"GH",\r\n"GM",\r\n"GN",\r\n"GQ",\r\n"GW",\r\n"LR",\r\n"ML",\r\n"MR",\r\n"NE",\r\n"NG",\r\n"SL",\r\n"SN",\r\n"ST",\r\n"TG"\r\n)', '!=', '0', '0', 'hook', 'actionOrderStatusUpdate', 0, '2015-05-12 11:52:32', '2015-05-12 11:52:32'),
	(81, 89, 'sql', 'SELECT IFNULL(id_order, 0) FROM PREFIX_orders o LEFT JOIN PREFIX_address a ON o.id_address_delivery = a.id_address LEFT JOIN PREFIX_country c ON c.id_country = a.id_country WHERE o.valid = 1 AND a.id_country != "{config}PS_COUNTRY_DEFAULT{/config}" AND c.iso_code IN (\r\n"DZ",\r\n"EG",\r\n"EH",\r\n"LY",\r\n"MA",\r\n"SD",\r\n"TN"\r\n)', '!=', '0', '0', 'hook', 'actionOrderStatusUpdate', 0, '2015-05-12 11:52:32', '2015-05-12 11:52:32'),
	(82, 90, 'sql', 'SELECT COUNT(*) FROM PREFIX_employee', '>=', '2', '2', 'hook', 'actionObjectEmployeeAddAfter', 1, '2015-05-12 11:52:32', '2015-05-12 11:52:32'),
	(83, 91, 'sql', 'SELECT COUNT(*) FROM PREFIX_employee', '>=', '3', '2', 'hook', 'actionObjectEmployeeAddAfter', 0, '2015-05-12 11:52:32', '2015-05-12 11:52:32'),
	(84, 92, 'sql', 'SELECT COUNT(*) FROM PREFIX_employee', '>=', '5', '2', 'hook', 'actionObjectEmployeeAddAfter', 0, '2015-05-12 11:52:32', '2015-05-12 11:52:32'),
	(85, 93, 'sql', 'SELECT COUNT(*) FROM PREFIX_employee', '>=', '10', '2', 'hook', 'actionObjectEmployeeAddAfter', 0, '2015-05-12 11:52:32', '2015-05-12 11:52:32'),
	(86, 94, 'sql', 'SELECT COUNT(*) FROM PREFIX_employee', '>=', '20', '2', 'hook', 'actionObjectEmployeeAddAfter', 0, '2015-05-12 11:52:32', '2015-05-12 11:52:32'),
	(87, 95, 'sql', 'SELECT COUNT(*) FROM PREFIX_employee', '>=', '40', '2', 'hook', 'actionObjectEmployeeAddAfter', 0, '2015-05-12 11:52:32', '2015-05-12 11:52:32'),
	(88, 96, 'sql', 'SELECT id_image FROM PREFIX_image WHERE id_image > 26', '>', '0', '0', 'hook', 'actionObjectImageAddAfter', 0, '2015-05-12 11:52:32', '2015-05-12 11:52:32'),
	(89, 97, 'sql', 'SELECT COUNT(*) FROM PREFIX_image', '>=', '50', '23', 'hook', 'actionObjectImageAddAfter', 0, '2015-05-12 11:52:32', '2015-05-12 11:52:32'),
	(90, 98, 'sql', 'SELECT COUNT(*) FROM PREFIX_image', '>=', '100', '23', 'hook', 'actionObjectImageAddAfter', 0, '2015-05-12 11:52:32', '2015-05-12 11:52:32'),
	(91, 99, 'sql', 'SELECT COUNT(*) FROM PREFIX_image', '>=', '1000', '23', 'time', '2', 0, '2015-05-12 11:52:32', '2015-05-12 11:52:32'),
	(92, 100, 'sql', 'SELECT COUNT(*) FROM PREFIX_image', '>=', '10000', '23', 'time', '4', 0, '2015-05-12 11:52:33', '2015-05-12 11:52:33'),
	(93, 101, 'sql', 'SELECT COUNT(*) FROM PREFIX_image', '>=', '50000', '23', 'time', '8', 0, '2015-05-12 11:52:33', '2015-05-12 11:52:33'),
	(94, 102, 'sql', 'SELECT id_cms FROM PREFIX_cms WHERE id_cms > 5', '>', '0', '0', 'hook', 'actionObjectCMSAddAfter', 0, '2015-05-12 11:52:33', '2015-05-12 11:52:33'),
	(95, 103, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart_rule', '>=', '1', '0', 'hook', 'actionObjectCartRuleAddAfter 	', 0, '2015-05-12 11:52:33', '2015-05-12 11:52:33'),
	(96, 104, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart_rule', '>=', '10', '0', 'hook', 'actionObjectCartRuleAddAfter 	', 0, '2015-05-12 11:52:33', '2015-05-12 11:52:33'),
	(97, 105, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart_rule', '>=', '100', '0', 'hook', 'actionObjectCartRuleAddAfter 	', 0, '2015-05-12 11:52:33', '2015-05-12 11:52:33'),
	(98, 107, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart_rule', '>=', '500', '0', 'hook', 'actionObjectCartRuleAddAfter 	', 0, '2015-05-12 11:52:33', '2015-05-12 11:52:33'),
	(99, 106, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart_rule', '>=', '1000', '0', 'hook', 'actionObjectCartRuleAddAfter 	', 0, '2015-05-12 11:52:33', '2015-05-12 11:52:33'),
	(100, 108, 'sql', 'SELECT COUNT(*) FROM PREFIX_cart_rule', '>=', '5000', '0', 'hook', 'actionObjectCartRuleAddAfter 	', 0, '2015-05-12 11:52:33', '2015-05-12 11:52:33'),
	(101, 109, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o INNER JOIN PREFIX_address a ON a.id_address = o.id_address_delivery\r\nWHERE reference NOT IN ("XKBKNABJK", "OHSATSERP", "FFATNOMMJ", "UOYEVOLI", "KHWLILZLL") AND a.id_country != "{config}PS_COUNTRY_DEFAULT{/config}"', '>=', '1', '0', 'hook', 'newOrder', 0, '2015-05-12 11:52:34', '2015-05-12 11:52:34'),
	(102, 110, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o INNER JOIN PREFIX_address a ON a.id_address = o.id_address_delivery\r\nWHERE reference NOT IN ("XKBKNABJK", "OHSATSERP", "FFATNOMMJ", "UOYEVOLI", "KHWLILZLL") AND a.id_country != "{config}PS_COUNTRY_DEFAULT{/config}"', '>=', '10', '0', 'hook', 'actionOrderStatusUpdate', 0, '2015-05-12 11:52:34', '2015-05-12 11:52:34'),
	(103, 111, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o INNER JOIN PREFIX_address a ON a.id_address = o.id_address_delivery\r\nWHERE reference NOT IN ("XKBKNABJK", "OHSATSERP", "FFATNOMMJ", "UOYEVOLI", "KHWLILZLL") AND a.id_country != "{config}PS_COUNTRY_DEFAULT{/config}"', '>=', '100', '0', 'hook', 'actionOrderStatusUpdate', 0, '2015-05-12 11:52:34', '2015-05-12 11:52:34'),
	(104, 113, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o INNER JOIN PREFIX_address a ON a.id_address = o.id_address_delivery\r\nWHERE reference NOT IN ("XKBKNABJK", "OHSATSERP", "FFATNOMMJ", "UOYEVOLI", "KHWLILZLL") AND a.id_country != "{config}PS_COUNTRY_DEFAULT{/config}"', '>=', '1000', '0', 'hook', 'actionOrderStatusUpdate', 0, '2015-05-12 11:52:34', '2015-05-12 11:52:34'),
	(105, 114, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o INNER JOIN PREFIX_address a ON a.id_address = o.id_address_delivery\r\nWHERE reference NOT IN ("XKBKNABJK", "OHSATSERP", "FFATNOMMJ", "UOYEVOLI", "KHWLILZLL") AND a.id_country != "{config}PS_COUNTRY_DEFAULT{/config}"', '>=', '5000', '0', 'hook', 'actionOrderStatusUpdate', 0, '2015-05-12 11:52:34', '2015-05-12 11:52:34'),
	(106, 112, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o INNER JOIN PREFIX_address a ON a.id_address = o.id_address_delivery\r\nWHERE reference NOT IN ("XKBKNABJK", "OHSATSERP", "FFATNOMMJ", "UOYEVOLI", "KHWLILZLL") AND a.id_country != "{config}PS_COUNTRY_DEFAULT{/config}"', '>=', '10000', '0', 'hook', 'actionOrderStatusUpdate', 0, '2015-05-12 11:52:34', '2015-05-12 11:52:34'),
	(107, 165, 'sql', 'SELECT COUNT(s.`id_store`) FROM PREFIX_store s WHERE `latitude` NOT IN (\'25.76500500\', \'26.13793600\', \'26.00998700\', \'25.73629600\', \'25.88674000\') AND `longitude` NOT IN (\'-80.24379700\', \'-80.13943500\', \'-80.29447200\', \'-80.24479700\', \'-80.16329200\')', '>', '0', '0', 'hook', 'actionAdminStoresControllerSaveAfter', 0, '2015-05-12 11:52:34', '2015-05-12 11:52:34'),
	(108, 166, 'sql', 'SELECT COUNT(s.`id_store`) FROM PREFIX_store s WHERE `latitude` NOT IN (\'25.76500500\', \'26.13793600\', \'26.00998700\', \'25.73629600\', \'25.88674000\') AND `longitude` NOT IN (\'-80.24379700\', \'-80.13943500\', \'-80.29447200\', \'-80.24479700\', \'-80.16329200\')', '>', '1', '0', 'hook', 'actionAdminStoresControllerSaveAfter', 0, '2015-05-12 11:52:34', '2015-05-12 11:52:34'),
	(109, 167, 'sql', 'SELECT COUNT(s.`id_store`) FROM PREFIX_store s WHERE `latitude` NOT IN (\'25.76500500\', \'26.13793600\', \'26.00998700\', \'25.73629600\', \'25.88674000\') AND `longitude` NOT IN (\'-80.24379700\', \'-80.13943500\', \'-80.29447200\', \'-80.24479700\', \'-80.16329200\')', '>', '4', '0', 'hook', 'actionAdminStoresControllerSaveAfter', 0, '2015-05-12 11:52:35', '2015-05-12 11:52:35'),
	(110, 168, 'sql', 'SELECT COUNT(s.`id_store`) FROM PREFIX_store s WHERE `latitude` NOT IN (\'25.76500500\', \'26.13793600\', \'26.00998700\', \'25.73629600\', \'25.88674000\') AND `longitude` NOT IN (\'-80.24379700\', \'-80.13943500\', \'-80.29447200\', \'-80.24479700\', \'-80.16329200\')', '>', '9', '0', 'hook', 'actionAdminStoresControllerSaveAfter', 0, '2015-05-12 11:52:35', '2015-05-12 11:52:35'),
	(111, 169, 'sql', 'SELECT COUNT(s.`id_store`) FROM PREFIX_store s WHERE `latitude` NOT IN (\'25.76500500\', \'26.13793600\', \'26.00998700\', \'25.73629600\', \'25.88674000\') AND `longitude` NOT IN (\'-80.24379700\', \'-80.13943500\', \'-80.29447200\', \'-80.24479700\', \'-80.16329200\')', '>', '19', '0', 'hook', 'actionAdminStoresControllerSaveAfter', 0, '2015-05-12 11:52:35', '2015-05-12 11:52:35'),
	(112, 170, 'sql', 'SELECT COUNT(s.`id_store`) FROM PREFIX_store s WHERE `latitude` NOT IN (\'25.76500500\', \'26.13793600\', \'26.00998700\', \'25.73629600\', \'25.88674000\') AND `longitude` NOT IN (\'-80.24379700\', \'-80.13943500\', \'-80.29447200\', \'-80.24479700\', \'-80.16329200\')', '>', '49', '0', 'hook', 'actionAdminStoresControllerSaveAfter', 0, '2015-05-12 11:52:35', '2015-05-12 11:52:35'),
	(113, 171, 'sql', 'SELECT COUNT(*) FROM PREFIX_webservice_account', '>=', '1', '0', 'hook', 'actionAdminWebserviceControllerSaveAfter', 0, '2015-05-12 11:52:35', '2015-05-12 11:52:35'),
	(114, 172, 'sql', 'SELECT COUNT(*) FROM PREFIX_webservice_account', '>=', '2', '0', 'hook', 'actionAdminWebserviceControllerSaveAfter', 0, '2015-05-12 11:52:35', '2015-05-12 11:52:35'),
	(115, 173, 'sql', 'SELECT COUNT(*) FROM PREFIX_webservice_account', '>=', '3', '0', 'hook', 'actionAdminWebserviceControllerSaveAfter', 0, '2015-05-12 11:52:35', '2015-05-12 11:52:35'),
	(116, 174, 'sql', 'SELECT COUNT(*) FROM PREFIX_webservice_account', '>=', '4', '0', 'hook', 'actionAdminWebserviceControllerSaveAfter', 0, '2015-05-12 11:52:35', '2015-05-12 11:52:35'),
	(117, 320, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%shopgate%" ', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-12 11:52:35', '2015-05-12 11:52:35'),
	(118, 322, 'configuration', 'SHOPGATE_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2015-05-12 11:52:35', '2015-05-12 11:52:35'),
	(119, 375, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%shopgate%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '1', '0', 'time', '1', 0, '2015-05-12 11:52:36', '2015-05-12 11:52:36'),
	(120, 376, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%shopgate%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '1', 0, '2015-05-12 11:52:36', '2015-05-12 11:52:36'),
	(121, 140, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%moneybookers%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-12 11:52:36', '2015-05-12 11:52:36'),
	(122, 326, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE \'MONEYBOOKERS_CONFIGURATION_OK\') AND ( value = \'1\')) OR (( name LIKE \'MB_PAY_TO_EMAIL \') AND ( value != \'testaccount2@moneybookers.com \'))', '==', '2', '0', 'time', '1', 0, '2015-05-12 11:52:36', '2015-05-12 11:52:36'),
	(123, 377, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%moneybookers%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '1', '0', 'time', '1', 0, '2015-05-12 11:52:36', '2015-05-12 11:52:36'),
	(124, 394, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%sofortbanking%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '1', 0, '2015-05-12 11:52:36', '2015-05-12 11:52:36'),
	(125, 428, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%authorizeaim%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-12 11:52:37', '2015-05-12 11:52:37'),
	(126, 429, 'configuration', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE \'AUTHORIZEAIM_CONFIGURATION_OK\') AND ( value = \'1\')) OR (( name LIKE \'AUTHORIZE_AIM_SANDBOX\') AND ( value = \'0\'))', '==', '2', '', 'time', '2', 0, '2015-05-12 11:52:37', '2015-05-12 11:52:37'),
	(127, 430, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%authorizeaim%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2015-05-12 11:52:37', '2015-05-12 11:52:37'),
	(128, 431, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%authorizeaim%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2015-05-12 11:52:37', '2015-05-12 11:52:37'),
	(129, 136, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%ebay%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-12 11:52:38', '2015-05-12 11:52:38'),
	(130, 209, 'configuration', 'EBAY_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2015-05-12 11:52:38', '2015-05-12 11:52:38'),
	(131, 358, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%ebay%" AND os.logable = 1', '>=', '1', '0', 'time', '1', 0, '2015-05-12 11:52:38', '2015-05-12 11:52:38'),
	(132, 359, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%ebay%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '1', 0, '2015-05-12 11:52:38', '2015-05-12 11:52:38'),
	(133, 438, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%payplug%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-12 11:52:38', '2015-05-12 11:52:38'),
	(134, 439, 'configuration', 'PAYPLUG_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2015-05-12 11:52:38', '2015-05-12 11:52:38'),
	(135, 440, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%payplug%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2015-05-12 11:52:38', '2015-05-12 11:52:38'),
	(136, 441, 'sql', 'SELECT SUM(o.total_paid) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%payplug%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '10000', '0', 'time', '7', 0, '2015-05-12 11:52:38', '2015-05-12 11:52:38'),
	(137, 442, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%affinityitems%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-12 11:52:39', '2015-05-12 11:52:39'),
	(138, 443, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE name LIKE \'AFFINITYITEMS_CONFIGURATION_OK\' AND value = \'1\'', '==', '1', '0', 'time', '1', 0, '2015-05-12 11:52:39', '2015-05-12 11:52:39'),
	(139, 444, 'configuration', 'SELECT 1', '!=', '1', '1', 'time', '100', 1, '2015-05-12 11:52:39', '2015-05-12 11:52:39'),
	(140, 445, 'configuration', 'SELECT 1', '!=', '1', '1', 'time', '100', 1, '2015-05-12 11:52:40', '2015-05-12 11:52:40'),
	(141, 446, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%dpdpoland%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-12 11:52:40', '2015-05-12 11:52:40'),
	(142, 447, 'configuration', 'DPDPOLAND_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2015-05-12 11:52:40', '2015-05-12 11:52:40'),
	(143, 448, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state LEFT JOIN PREFIX_carrier c ON c.id_carrier = o.id_carrier WHERE c.name like "%dpdpoland%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2015-05-12 11:52:40', '2015-05-12 11:52:40'),
	(144, 449, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state LEFT JOIN PREFIX_carrier c ON c.id_carrier = o.id_carrier WHERE c.name like "%dpdpoland%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '100', '0', 'time', '7', 0, '2015-05-12 11:52:40', '2015-05-12 11:52:40'),
	(145, 450, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%envoimoinscher%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-12 11:52:40', '2015-05-12 11:52:40'),
	(146, 451, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE \'ENVOIMOINSCHER_CONFIGURATION_OK\') AND ( value = \'1\')) OR (( name LIKE \'EMC_ENV \') AND ( value != \'TEST\'))', '==', '2', '0', 'time', '1', 0, '2015-05-12 11:52:41', '2015-05-12 11:52:41'),
	(147, 452, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state LEFT JOIN PREFIX_carrier c ON c.id_carrier = o.id_carrier WHERE c.name like "%envoimoinscher%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2015-05-12 11:52:41', '2015-05-12 11:52:41'),
	(148, 453, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state LEFT JOIN PREFIX_carrier c ON c.id_carrier = o.id_carrier WHERE c.name like "%envoimoinscher%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '100', '0', 'time', '7', 0, '2015-05-12 11:52:41', '2015-05-12 11:52:41'),
	(149, 454, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%klikandpay%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-12 11:52:41', '2015-05-12 11:52:41'),
	(150, 455, 'configuration', 'KLIKANDPAY_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2015-05-12 11:52:41', '2015-05-12 11:52:41'),
	(151, 456, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%klikandpay%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2015-05-12 11:52:41', '2015-05-12 11:52:41'),
	(152, 457, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%klikandpay%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2015-05-12 11:52:41', '2015-05-12 11:52:41'),
	(153, 458, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%clickline%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-12 11:52:41', '2015-05-12 11:52:41'),
	(154, 459, 'configuration', 'CLICKLINE_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2015-05-12 11:52:41', '2015-05-12 11:52:41'),
	(155, 460, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state LEFT JOIN PREFIX_carrier c ON c.id_carrier = o.id_carrier WHERE c.name like "%clickline%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2015-05-12 11:52:42', '2015-05-12 11:52:42'),
	(156, 461, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state LEFT JOIN PREFIX_carrier c ON c.id_carrier = o.id_carrier WHERE c.name like "%clickline%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '100', '0', 'time', '7', 0, '2015-05-12 11:52:42', '2015-05-12 11:52:42'),
	(157, 462, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%cdiscount%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-12 11:52:42', '2015-05-12 11:52:42'),
	(158, 463, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '100', 0, '2015-05-12 11:52:42', '2015-05-12 11:52:42'),
	(159, 464, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%cdiscount%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2015-05-12 11:52:42', '2015-05-12 11:52:42'),
	(160, 465, 'sql', 'SELECT SUM(o.total_paid) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%cdiscount%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 365 DAY)', '>=', '500', '0', 'time', '7', 0, '2015-05-12 11:52:42', '2015-05-12 11:52:42'),
	(161, 467, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%erpillicopresta%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-12 11:52:42', '2015-05-12 11:52:42'),
	(162, 468, 'configuration', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE \'ERPILLICOPRESTA_CONFIGURATION_OK\') AND ( value = \'1\')) OR (( name LIKE \'ERP_LICENCE_VALIDITY \') AND ( value == \'1\')) OR (( name LIKE \'ERP_MONTH_FREE_ACTIVE \') AND ( value == \'0\'))', '==', '3', '', 'time', '1', 0, '2015-05-12 11:52:42', '2015-05-12 11:52:42'),
	(163, 469, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '100', 0, '2015-05-12 11:52:42', '2015-05-12 11:52:42'),
	(164, 470, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '100', 0, '2015-05-12 11:52:42', '2015-05-12 11:52:42'),
	(165, 471, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%netreviews%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-12 11:52:43', '2015-05-12 11:52:43'),
	(166, 472, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE \'NETREVIEWS_CONFIGURATION_OK\') AND ( value = \'1\')) OR (( name LIKE \'AVISVERIFIES_URLCERTIFICAT \') AND ( value IS NOT LIKE \'%preprod%\'))', '==', '2', '0', 'time', '1', 0, '2015-05-12 11:52:43', '2015-05-12 11:52:43'),
	(167, 473, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '100', 0, '2015-05-12 11:52:43', '2015-05-12 11:52:43'),
	(168, 474, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '100', 0, '2015-05-12 11:52:43', '2015-05-12 11:52:43'),
	(169, 475, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%bluesnap%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-12 11:52:43', '2015-05-12 11:52:43'),
	(170, 476, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE \'BLUESNAP_CONFIGURATION_OK\') AND ( value = \'1\')) OR (( name LIKE \'BLUESNAP_SANDBOX \') AND ( value NOT LIKE \'%sandbox%\'))', '==', '2', '0', 'time', '1', 0, '2015-05-12 11:52:43', '2015-05-12 11:52:43'),
	(171, 477, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%bluesnap%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2015-05-12 11:52:43', '2015-05-12 11:52:43'),
	(172, 478, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%bluesnap%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2015-05-12 11:52:43', '2015-05-12 11:52:43'),
	(173, 479, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%desjardins%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-12 11:52:43', '2015-05-12 11:52:43'),
	(174, 480, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE \'DESJARDINS_CONFIGURATION_OK\') AND ( value = \'1\')) OR (( name LIKE \'DESJARDINS_MODE \') AND ( value NOT LIKE \'%test%\'))', '==', '2', '0', 'time', '1', 0, '2015-05-12 11:52:43', '2015-05-12 11:52:43'),
	(175, 481, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%desjardins%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2015-05-12 11:52:43', '2015-05-12 11:52:43'),
	(176, 482, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%desjardins%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2015-05-12 11:52:43', '2015-05-12 11:52:43'),
	(177, 483, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%firstdata%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-12 11:52:44', '2015-05-12 11:52:44'),
	(178, 484, 'configuration', 'FIRSTDATA_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2015-05-12 11:52:44', '2015-05-12 11:52:44'),
	(179, 485, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%firstdata%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2015-05-12 11:52:44', '2015-05-12 11:52:44'),
	(180, 486, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%firstdata%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2015-05-12 11:52:44', '2015-05-12 11:52:44'),
	(181, 487, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%giveit%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-12 11:52:44', '2015-05-12 11:52:44'),
	(182, 488, 'sql', 'GIVEIT_CONFIGURATION_OK', '>=', '1', '0', 'time', '1', 0, '2015-05-12 11:52:44', '2015-05-12 11:52:44'),
	(183, 489, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2015-05-12 11:52:44', '2015-05-12 11:52:44'),
	(184, 490, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2015-05-12 11:52:45', '2015-05-12 11:52:45'),
	(185, 491, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%ganalytics%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-12 11:52:45', '2015-05-12 11:52:45'),
	(186, 492, 'configuration', 'GANALYTICS_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2015-05-12 11:52:45', '2015-05-12 11:52:45'),
	(187, 493, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '1', 0, '2015-05-12 11:52:45', '2015-05-12 11:52:45'),
	(188, 494, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2015-05-12 11:52:45', '2015-05-12 11:52:45'),
	(189, 496, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%pagseguro%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-12 11:52:46', '2015-05-12 11:52:46'),
	(190, 497, 'configuration', 'PAGSEGURO_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2015-05-12 11:52:46', '2015-05-12 11:52:46'),
	(191, 498, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%pagseguro%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2015-05-12 11:52:46', '2015-05-12 11:52:46'),
	(192, 499, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%pagseguro%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2015-05-12 11:52:46', '2015-05-12 11:52:46'),
	(193, 500, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%paypalmx%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-12 11:52:46', '2015-05-12 11:52:46'),
	(194, 501, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE \'PAYPALMX_CONFIGURATION_OK\') AND ( value = \'1\')) OR (( name LIKE \'PAYPAL_MX_SANDBOX\') AND ( value = \'0\'))', '==', '2', '0', 'time', '1', 0, '2015-05-12 11:52:46', '2015-05-12 11:52:46'),
	(195, 502, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%paypalmx%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2015-05-12 11:52:46', '2015-05-12 11:52:46'),
	(196, 503, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%paypalmx%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2015-05-12 11:52:46', '2015-05-12 11:52:46'),
	(197, 505, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%paypalusa%"', '==', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-12 11:52:46', '2015-05-12 11:52:46'),
	(198, 506, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE \'PAYPALUSA_CONFIGURATION_OK\') AND ( value = \'1\')) OR (( name LIKE \'PAYPAL_USA_SANDBOX\') AND ( value = \'0\'))', '==', '2', '0', 'time', '1', 0, '2015-05-12 11:52:47', '2015-05-12 11:52:47'),
	(199, 507, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%paypalusa%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2015-05-12 11:52:47', '2015-05-12 11:52:47'),
	(200, 508, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%paypalmx%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2015-05-12 11:52:47', '2015-05-12 11:52:47'),
	(201, 510, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE \'PAYULATAM_CONFIGURATION_OK\') AND ( value = \'1\')) OR (( name LIKE \'PAYU_LATAM_TEST\') AND ( value = \'1\'))', '==', '2', '0', 'time', '1', 0, '2015-05-12 11:52:47', '2015-05-12 11:52:47'),
	(202, 511, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%payulatam%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2015-05-12 11:52:47', '2015-05-12 11:52:47'),
	(203, 512, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%payulatam%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2015-05-12 11:52:47', '2015-05-12 11:52:47'),
	(204, 513, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%prestastats%"', '==', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-12 11:52:48', '2015-05-12 11:52:48'),
	(205, 514, 'configuration', 'PRESTASTATS_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2015-05-12 11:52:48', '2015-05-12 11:52:48'),
	(206, 515, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2015-05-12 11:52:48', '2015-05-12 11:52:48'),
	(207, 516, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2015-05-12 11:52:48', '2015-05-12 11:52:48'),
	(208, 517, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%riskified%"', '==', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-12 11:52:48', '2015-05-12 11:52:48'),
	(209, 518, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE \'RISKIFIED_CONFIGURATION_OK\') AND ( value = \'1\')) OR (( name LIKE \'RISKIFIED_MODE\') AND ( value = \'1\'))', '==', '2', '0', 'time', '1', 0, '2015-05-12 11:52:48', '2015-05-12 11:52:48'),
	(210, 519, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%riskified%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2015-05-12 11:52:48', '2015-05-12 11:52:48'),
	(211, 520, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%riskified%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2015-05-12 11:52:48', '2015-05-12 11:52:48'),
	(212, 521, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%simplifycommerce%"', '==', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-12 11:52:26', '2015-05-12 11:52:26'),
	(213, 522, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE \'SIMPLIFY_CONFIGURATION_OK\') AND ( value = \'1\')) OR (( name LIKE \'SIMPLIFY_MODE\') AND ( value = \'1\'))', '==', '2', '0', 'time', '1', 0, '2015-05-08 14:03:58', '2015-05-12 09:04:21'),
	(214, 523, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%simplifycommerce%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2015-05-08 14:03:58', '2015-05-11 09:06:38'),
	(215, 524, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%simplifycommerce%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2015-05-08 14:03:58', '2015-05-08 14:03:58'),
	(216, 525, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%vtpayment%"', '==', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-08 14:03:58', '2015-05-11 11:35:08'),
	(217, 526, 'configuration', 'VTPAYMENT_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2015-05-08 14:03:58', '2015-05-12 09:04:21'),
	(218, 527, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%vtpayment%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2015-05-08 14:03:58', '2015-05-11 09:06:38'),
	(219, 528, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%vtpayment%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2015-05-08 14:03:58', '2015-05-08 14:03:58'),
	(220, 529, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%yotpo%"', '==', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-08 14:03:58', '2015-05-11 11:35:08'),
	(221, 530, 'configuration', 'YOTPO_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2015-05-08 14:03:58', '2015-05-12 09:04:21'),
	(222, 531, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2015-05-08 14:03:58', '2015-05-08 14:03:58'),
	(223, 532, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2015-05-08 14:03:58', '2015-05-08 14:03:58'),
	(224, 533, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%yotpo%"', '==', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-12 11:52:26', '2015-05-12 11:52:26'),
	(225, 534, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE \'YOUSTICERESOLUTIONSYSTEM_CONF_OK\') AND ( value = \'1\')) OR (( name LIKE \'YRS_SANDBOX\') AND ( value = \'0\'))', '==', '2', '0', 'time', '1', 0, '2015-05-08 14:03:58', '2015-05-12 09:04:21'),
	(226, 535, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2015-05-08 14:03:58', '2015-05-08 14:03:58'),
	(227, 536, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2015-05-08 14:03:58', '2015-05-08 14:03:58'),
	(228, 537, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%loyaltylion%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-08 14:03:58', '2015-05-11 11:35:08'),
	(229, 538, 'configuration', 'LOYALTYLION_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2015-05-08 14:03:58', '2015-05-12 09:04:21'),
	(230, 539, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2015-05-08 14:03:58', '2015-05-08 14:03:58'),
	(231, 540, 'sql', 'SELECT 1', '!=', '1', '1', 'time', '365', 0, '2015-05-08 14:03:58', '2015-05-08 14:03:58'),
	(232, 324, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%shoppingfluxexport%" ', '==', '0', '0', 'time', '1', 1, '2015-05-12 11:52:26', '2015-05-12 11:52:26'),
	(233, 399, 'sql', 'SELECT COUNT(*) FROM PREFIX_product WHERE reference NOT LIKE "demo_%"', '>', '499', '', 'hook', 'actionObjectProductAddAfter', 0, '2015-05-12 11:52:26', '2015-05-12 11:52:26'),
	(234, 323, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%shoppingfluxexport%" ', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-12 11:52:26', '2015-05-12 11:52:26'),
	(235, 132, 'sql', 'SELECT count(id_configuration) FROM PREFIX_configuration WHERE `name` = \'PS_SHOP_DOMAIN\' AND value IN (\'127.0.0.1\', \'localhost\' )', '==', '1', '0', 'time', '1', 0, '2015-05-12 11:52:27', '2015-05-12 11:52:27'),
	(236, 175, 'sql', 'SELECT count(*) FROM	 PREFIX_configuration WHERE name = \'PS_HOSTED_MODE\'', '==', '0', '0', 'time', '1', 1, '2015-05-12 11:52:27', '2015-05-12 11:52:27'),
	(237, 86, 'sql', 'SELECT IFNULL(id_order, 0) FROM PREFIX_orders o LEFT JOIN PREFIX_address a ON o.id_address_delivery = a.id_address LEFT JOIN PREFIX_country c ON c.id_country = a.id_country WHERE o.valid = 1 AND a.id_country != "{config}PS_COUNTRY_DEFAULT{/config}" AND c.iso_code IN (\r\n"BZ",\r\n"CR",\r\n"GT",\r\n"HN",\r\n"MX",\r\n"NI",\r\n"PA",\r\n"SV",\r\n"AG",\r\n"AI",\r\n"AN",\r\n"AW",\r\n"BB",\r\n"BM",\r\n"BS",\r\n"CU",\r\n"DM",\r\n"DO",\r\n"GD",\r\n"GP",\r\n"HT",\r\n"JM",\r\n"KN",\r\n"KY",\r\n"LC",\r\n"MQ",\r\n"MS",\r\n"PR",\r\n"TC",\r\n"TT",\r\n"VC",\r\n"VG",\r\n"VI",\r\n"AR",\r\n"BO",\r\n"BR",\r\n"CL",\r\n"CO",\r\n"EC",\r\n"FK",\r\n"GF",\r\n"GY",\r\n"PE",\r\n"PY",\r\n"SR",\r\n"UY",\r\n"VE"\r\n)', '!=', '0', '', 'hook', 'actionOrderStatusUpdate', 0, '2015-05-12 11:52:32', '2015-05-12 11:52:32'),
	(238, 325, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE name LIKE \'SHOPPINGFLUXEXPORT_CONFIGURATION_OK\' OR name LIKE \'SHOPPINGFLUXEXPORT_CONFIGURED\'', '>=', '1', '0', 'time', '1', 0, '2015-05-12 11:52:36', '2015-05-12 11:52:36'),
	(239, 424, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%alliance3%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-12 11:52:36', '2015-05-12 11:52:36'),
	(240, 425, 'sql', 'SELECT COUNT(*) FROM PREFIX_configuration WHERE (( name LIKE \'ALLIANCE3_CONFIGURATION_OK\') AND ( value = \'1\')) OR (( name LIKE \'ALLIANCE_DEMO\') AND ( value = \'0\'))', '==', '2', '0', 'time', '1', 0, '2015-05-12 11:52:36', '2015-05-12 11:52:36'),
	(241, 426, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%alliance3%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2015-05-12 11:52:36', '2015-05-12 11:52:36'),
	(242, 427, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%alliance3%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2015-05-12 11:52:37', '2015-05-12 11:52:37'),
	(243, 434, 'sql', 'SELECT COUNT( id_module ) FROM PREFIX_module WHERE `name` like "%bluepay%"', '>=', '1', '0', 'hook', 'actionModuleInstallAfter', 0, '2015-05-12 11:52:37', '2015-05-12 11:52:37'),
	(244, 435, 'configuration', 'BLUEPAY_CONFIGURATION_OK', '==', '1', '', 'time', '1', 0, '2015-05-12 11:52:37', '2015-05-12 11:52:37'),
	(245, 436, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%bluepay%" AND os.logable = 1', '>=', '1', '0', 'time', '2', 0, '2015-05-12 11:52:37', '2015-05-12 11:52:37'),
	(246, 437, 'sql', 'SELECT COUNT(*) FROM PREFIX_orders o LEFT JOIN PREFIX_order_state os ON os.id_order_state = o.current_state WHERE o.module like "%bluepay%" AND os.logable = 1 AND o.date_add > DATE_SUB(NOW(), INTERVAL 90 DAY)', '>=', '30', '0', 'time', '7', 0, '2015-05-12 11:52:37', '2015-05-12 11:52:37');
/*!40000 ALTER TABLE `ps_condition` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_condition_advice
DROP TABLE IF EXISTS `ps_condition_advice`;
CREATE TABLE IF NOT EXISTS `ps_condition_advice` (
  `id_condition` int(11) NOT NULL,
  `id_advice` int(11) NOT NULL,
  `display` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_condition`,`id_advice`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_condition_advice: ~61 rows (approximately)
/*!40000 ALTER TABLE `ps_condition_advice` DISABLE KEYS */;
INSERT INTO `ps_condition_advice` (`id_condition`, `id_advice`, `display`) VALUES
	(1, 1, 1),
	(1, 16, 1),
	(2, 1, 0),
	(2, 16, 0),
	(3, 2, 1),
	(3, 3, 1),
	(3, 4, 1),
	(3, 5, 1),
	(3, 6, 1),
	(3, 8, 1),
	(3, 11, 1),
	(3, 12, 1),
	(3, 14, 1),
	(3, 15, 1),
	(3, 17, 1),
	(3, 18, 1),
	(3, 19, 1),
	(3, 21, 1),
	(3, 22, 1),
	(3, 23, 1),
	(3, 24, 1),
	(3, 25, 1),
	(3, 28, 1),
	(3, 29, 1),
	(3, 30, 1),
	(3, 31, 1),
	(3, 32, 1),
	(3, 33, 1),
	(3, 38, 1),
	(4, 2, 0),
	(4, 3, 0),
	(4, 5, 0),
	(4, 8, 0),
	(4, 17, 0),
	(4, 18, 0),
	(4, 21, 0),
	(4, 25, 0),
	(4, 32, 0),
	(4, 36, 0),
	(4, 37, 0),
	(5, 7, 1),
	(6, 9, 1),
	(6, 10, 1),
	(6, 26, 1),
	(6, 27, 1),
	(7, 13, 1),
	(7, 34, 1),
	(8, 13, 0),
	(8, 14, 0),
	(8, 15, 0),
	(8, 34, 0),
	(39, 35, 0),
	(59, 35, 1),
	(70, 37, 1),
	(212, 23, 0),
	(224, 24, 0),
	(232, 20, 1),
	(233, 20, 1),
	(234, 20, 0),
	(235, 36, 1),
	(236, 36, 1);
/*!40000 ALTER TABLE `ps_condition_advice` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_condition_badge
DROP TABLE IF EXISTS `ps_condition_badge`;
CREATE TABLE IF NOT EXISTS `ps_condition_badge` (
  `id_condition` int(11) NOT NULL,
  `id_badge` int(11) NOT NULL,
  PRIMARY KEY (`id_condition`,`id_badge`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_condition_badge: ~240 rows (approximately)
/*!40000 ALTER TABLE `ps_condition_badge` DISABLE KEYS */;
INSERT INTO `ps_condition_badge` (`id_condition`, `id_badge`) VALUES
	(3, 133),
	(4, 154),
	(5, 85),
	(6, 169),
	(7, 129),
	(8, 153),
	(9, 117),
	(10, 118),
	(11, 118),
	(12, 118),
	(13, 119),
	(14, 120),
	(15, 121),
	(16, 122),
	(17, 123),
	(18, 124),
	(19, 125),
	(20, 126),
	(21, 127),
	(22, 128),
	(23, 130),
	(24, 131),
	(25, 132),
	(26, 134),
	(27, 135),
	(28, 136),
	(29, 137),
	(30, 138),
	(31, 139),
	(32, 140),
	(33, 141),
	(34, 142),
	(35, 143),
	(36, 144),
	(37, 145),
	(38, 146),
	(39, 147),
	(40, 148),
	(41, 149),
	(42, 150),
	(43, 151),
	(44, 152),
	(45, 155),
	(46, 156),
	(47, 157),
	(48, 158),
	(49, 159),
	(50, 160),
	(51, 161),
	(52, 162),
	(53, 163),
	(54, 164),
	(55, 165),
	(56, 166),
	(57, 167),
	(58, 168),
	(59, 170),
	(60, 171),
	(61, 172),
	(62, 173),
	(63, 174),
	(64, 175),
	(65, 176),
	(66, 177),
	(67, 178),
	(68, 179),
	(69, 180),
	(70, 181),
	(71, 182),
	(72, 183),
	(73, 184),
	(74, 185),
	(75, 186),
	(76, 187),
	(77, 188),
	(78, 189),
	(79, 190),
	(80, 191),
	(81, 192),
	(82, 193),
	(83, 194),
	(84, 195),
	(85, 196),
	(86, 197),
	(87, 198),
	(88, 199),
	(89, 200),
	(90, 201),
	(91, 202),
	(92, 203),
	(93, 204),
	(94, 205),
	(95, 206),
	(96, 207),
	(97, 208),
	(98, 209),
	(99, 210),
	(100, 211),
	(101, 212),
	(102, 213),
	(103, 214),
	(104, 215),
	(105, 216),
	(106, 217),
	(107, 218),
	(108, 219),
	(109, 220),
	(110, 221),
	(111, 222),
	(112, 223),
	(113, 224),
	(114, 225),
	(115, 226),
	(116, 227),
	(117, 1),
	(118, 2),
	(119, 3),
	(120, 4),
	(121, 5),
	(122, 6),
	(123, 7),
	(124, 8),
	(125, 9),
	(126, 10),
	(127, 11),
	(128, 12),
	(129, 13),
	(130, 14),
	(131, 15),
	(132, 16),
	(133, 17),
	(134, 18),
	(135, 19),
	(136, 20),
	(137, 21),
	(138, 22),
	(139, 23),
	(140, 24),
	(141, 25),
	(142, 26),
	(143, 27),
	(144, 28),
	(145, 29),
	(146, 30),
	(147, 31),
	(148, 32),
	(149, 33),
	(150, 34),
	(151, 35),
	(152, 36),
	(153, 37),
	(154, 38),
	(155, 39),
	(156, 40),
	(157, 41),
	(158, 42),
	(159, 43),
	(160, 44),
	(161, 45),
	(162, 46),
	(163, 47),
	(164, 48),
	(165, 49),
	(166, 50),
	(167, 51),
	(168, 52),
	(169, 53),
	(170, 54),
	(171, 55),
	(172, 56),
	(173, 57),
	(174, 58),
	(175, 59),
	(176, 60),
	(177, 61),
	(178, 62),
	(179, 63),
	(180, 64),
	(181, 65),
	(182, 66),
	(183, 67),
	(184, 68),
	(185, 69),
	(186, 70),
	(187, 71),
	(188, 72),
	(189, 73),
	(190, 74),
	(191, 75),
	(192, 76),
	(193, 77),
	(194, 78),
	(195, 79),
	(196, 80),
	(197, 81),
	(198, 82),
	(199, 83),
	(200, 84),
	(201, 86),
	(202, 87),
	(203, 88),
	(204, 89),
	(205, 90),
	(206, 91),
	(207, 92),
	(208, 93),
	(209, 94),
	(210, 95),
	(211, 96),
	(212, 97),
	(213, 98),
	(214, 99),
	(215, 100),
	(216, 101),
	(217, 102),
	(218, 103),
	(219, 104),
	(220, 105),
	(221, 106),
	(222, 107),
	(223, 108),
	(224, 109),
	(225, 110),
	(226, 111),
	(227, 112),
	(228, 113),
	(229, 114),
	(230, 115),
	(231, 116),
	(234, 228),
	(237, 238),
	(238, 229),
	(239, 230),
	(240, 231),
	(241, 232),
	(242, 233),
	(243, 234),
	(244, 235),
	(245, 236),
	(246, 237);
/*!40000 ALTER TABLE `ps_condition_badge` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_configuration
DROP TABLE IF EXISTS `ps_configuration`;
CREATE TABLE IF NOT EXISTS `ps_configuration` (
  `id_configuration` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_group` int(11) unsigned DEFAULT NULL,
  `id_shop` int(11) unsigned DEFAULT NULL,
  `name` varchar(254) NOT NULL,
  `value` text,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_configuration`),
  KEY `name` (`name`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_group` (`id_shop_group`)
) ENGINE=InnoDB AUTO_INCREMENT=641 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_configuration: ~634 rows (approximately)
/*!40000 ALTER TABLE `ps_configuration` DISABLE KEYS */;
INSERT INTO `ps_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES
	(1, NULL, NULL, 'PS_LANG_DEFAULT', '1', '2015-05-08 13:59:41', '2015-05-08 13:59:41'),
	(2, NULL, NULL, 'PS_VERSION_DB', '1.6.0.13', '2015-05-08 13:59:41', '2015-05-08 14:03:29'),
	(3, NULL, NULL, 'PS_INSTALL_VERSION', '1.6.0.6', '2015-05-08 13:59:41', '2015-05-08 14:03:29'),
	(4, NULL, NULL, 'PS_CARRIER_DEFAULT', '3', '2015-05-08 13:59:53', '2015-05-08 14:03:29'),
	(5, NULL, NULL, 'PS_GROUP_FEATURE_ACTIVE', '1', '2015-05-08 13:59:53', '2015-05-08 13:59:53'),
	(6, NULL, NULL, 'PS_SEARCH_INDEXATION', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(7, NULL, NULL, 'PS_ONE_PHONE_AT_LEAST', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(8, NULL, NULL, 'PS_CURRENCY_DEFAULT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(9, NULL, NULL, 'PS_COUNTRY_DEFAULT', '21', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(10, NULL, NULL, 'PS_REWRITING_SETTINGS', '0', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(11, NULL, NULL, 'PS_ORDER_OUT_OF_STOCK', '1', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(12, NULL, NULL, 'PS_LAST_QTIES', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(13, NULL, NULL, 'PS_CART_REDIRECT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(14, NULL, NULL, 'PS_CONDITIONS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(15, NULL, NULL, 'PS_RECYCLABLE_PACK', '1', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(16, NULL, NULL, 'PS_GIFT_WRAPPING', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(17, NULL, NULL, 'PS_GIFT_WRAPPING_PRICE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(18, NULL, NULL, 'PS_STOCK_MANAGEMENT', '0', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(19, NULL, NULL, 'PS_NAVIGATION_PIPE', '&gt;', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(20, NULL, NULL, 'PS_PRODUCTS_PER_PAGE', '12', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(21, NULL, NULL, 'PS_PURCHASE_MINIMUM', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(22, NULL, NULL, 'PS_PRODUCTS_ORDER_WAY', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(23, NULL, NULL, 'PS_PRODUCTS_ORDER_BY', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(24, NULL, NULL, 'PS_DISPLAY_QTIES', '0', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(25, NULL, NULL, 'PS_SHIPPING_HANDLING', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(26, NULL, NULL, 'PS_SHIPPING_FREE_PRICE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(27, NULL, NULL, 'PS_SHIPPING_FREE_WEIGHT', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(28, NULL, NULL, 'PS_SHIPPING_METHOD', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(29, NULL, NULL, 'PS_TAX', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(30, NULL, NULL, 'PS_SHOP_ENABLE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(31, NULL, NULL, 'PS_NB_DAYS_NEW_PRODUCT', '299999', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(32, NULL, NULL, 'PS_SSL_ENABLED', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(33, NULL, NULL, 'PS_WEIGHT_UNIT', 'lb', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(34, NULL, NULL, 'PS_BLOCK_CART_AJAX', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(35, NULL, NULL, 'PS_ORDER_RETURN', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(36, NULL, NULL, 'PS_ORDER_RETURN_NB_DAYS', '7', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(37, NULL, NULL, 'PS_MAIL_TYPE', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(38, NULL, NULL, 'PS_PRODUCT_PICTURE_MAX_SIZE', '8388608', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(39, NULL, NULL, 'PS_PRODUCT_PICTURE_WIDTH', '65', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(40, NULL, NULL, 'PS_PRODUCT_PICTURE_HEIGHT', '79', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(41, NULL, NULL, 'PS_INVOICE_PREFIX', 'IN', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(42, NULL, NULL, 'PS_DELIVERY_PREFIX', 'DE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(43, NULL, NULL, 'PS_DELIVERY_NUMBER', NULL, '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(44, NULL, NULL, 'PS_INVOICE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(45, NULL, NULL, 'PS_PASSWD_TIME_BACK', '360', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(46, NULL, NULL, 'PS_PASSWD_TIME_FRONT', '360', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(47, NULL, NULL, 'PS_DISP_UNAVAILABLE_ATTR', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(48, NULL, NULL, 'PS_SEARCH_MINWORDLEN', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(49, NULL, NULL, 'PS_SEARCH_BLACKLIST', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(50, NULL, NULL, 'PS_SEARCH_WEIGHT_PNAME', '6', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(51, NULL, NULL, 'PS_SEARCH_WEIGHT_REF', '10', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(52, NULL, NULL, 'PS_SEARCH_WEIGHT_SHORTDESC', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(53, NULL, NULL, 'PS_SEARCH_WEIGHT_DESC', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(54, NULL, NULL, 'PS_SEARCH_WEIGHT_CNAME', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(55, NULL, NULL, 'PS_SEARCH_WEIGHT_MNAME', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(56, NULL, NULL, 'PS_SEARCH_WEIGHT_TAG', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(57, NULL, NULL, 'PS_SEARCH_WEIGHT_ATTRIBUTE', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(58, NULL, NULL, 'PS_SEARCH_WEIGHT_FEATURE', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(59, NULL, NULL, 'PS_SEARCH_AJAX', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(60, NULL, NULL, 'PS_TIMEZONE', 'US/Eastern', '0000-00-00 00:00:00', '2015-05-08 14:00:04'),
	(61, NULL, NULL, 'PS_THEME_V11', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(62, NULL, NULL, 'PRESTASTORE_LIVE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(63, NULL, NULL, 'PS_TIN_ACTIVE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(64, NULL, NULL, 'PS_SHOW_ALL_MODULES', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(65, NULL, NULL, 'PS_BACKUP_ALL', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(66, NULL, NULL, 'PS_1_3_UPDATE_DATE', '2011-12-27 10:20:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(67, NULL, NULL, 'PS_PRICE_ROUND_MODE', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(68, NULL, NULL, 'PS_1_3_2_UPDATE_DATE', '2011-12-27 10:20:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(69, NULL, NULL, 'PS_CONDITIONS_CMS_ID', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(70, NULL, NULL, 'TRACKING_DIRECT_TRAFFIC', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(71, NULL, NULL, 'PS_META_KEYWORDS', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(72, NULL, NULL, 'PS_DISPLAY_JQZOOM', '1', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(73, NULL, NULL, 'PS_VOLUME_UNIT', 'gal', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(74, NULL, NULL, 'PS_CIPHER_ALGORITHM', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(75, NULL, NULL, 'PS_ATTRIBUTE_CATEGORY_DISPLAY', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(76, NULL, NULL, 'PS_CUSTOMER_SERVICE_FILE_UPLOAD', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(77, NULL, NULL, 'PS_CUSTOMER_SERVICE_SIGNATURE', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(78, NULL, NULL, 'PS_BLOCK_BESTSELLERS_DISPLAY', '1', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(79, NULL, NULL, 'PS_BLOCK_NEWPRODUCTS_DISPLAY', '1', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(80, NULL, NULL, 'PS_BLOCK_SPECIALS_DISPLAY', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(81, NULL, NULL, 'PS_STOCK_MVT_REASON_DEFAULT', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(82, NULL, NULL, 'PS_COMPARATOR_MAX_ITEM', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(83, NULL, NULL, 'PS_ORDER_PROCESS_TYPE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(84, NULL, NULL, 'PS_SPECIFIC_PRICE_PRIORITIES', 'id_shop;id_currency;id_country;id_group', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(85, NULL, NULL, 'PS_TAX_DISPLAY', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(86, NULL, NULL, 'PS_SMARTY_FORCE_COMPILE', '2', '0000-00-00 00:00:00', '2015-05-12 14:48:49'),
	(87, NULL, NULL, 'PS_DISTANCE_UNIT', 'mi', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(88, NULL, NULL, 'PS_STORES_DISPLAY_CMS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(89, NULL, NULL, 'PS_STORES_DISPLAY_FOOTER', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(90, NULL, NULL, 'PS_STORES_SIMPLIFIED', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(91, NULL, NULL, 'SHOP_LOGO_WIDTH', '327', '0000-00-00 00:00:00', '2015-05-08 13:54:44'),
	(92, NULL, NULL, 'SHOP_LOGO_HEIGHT', '88', '0000-00-00 00:00:00', '2015-05-08 13:54:44'),
	(93, NULL, NULL, 'EDITORIAL_IMAGE_WIDTH', '530', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(94, NULL, NULL, 'EDITORIAL_IMAGE_HEIGHT', '228', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(95, NULL, NULL, 'PS_STATSDATA_CUSTOMER_PAGESVIEWS', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(96, NULL, NULL, 'PS_STATSDATA_PAGESVIEWS', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(97, NULL, NULL, 'PS_STATSDATA_PLUGINS', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(98, NULL, NULL, 'PS_GEOLOCATION_ENABLED', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(99, NULL, NULL, 'PS_ALLOWED_COUNTRIES', 'AF;ZA;AX;AL;DZ;DE;AD;AO;AI;AQ;AG;AN;SA;AR;AM;AW;AU;AT;AZ;BS;BH;BD;BB;BY;BE;BZ;BJ;BM;BT;BO;BA;BW;BV;BR;BN;BG;BF;MM;BI;KY;KH;CM;CA;CV;CF;CL;CN;CX;CY;CC;CO;KM;CG;CD;CK;KR;KP;CR;CI;HR;CU;DK;DJ;DM;EG;IE;SV;AE;EC;ER;ES;EE;ET;FK;FO;FJ;FI;FR;GA;GM;GE;GS;GH;GI;GR;GD;GL;GP;GU;GT;GG;GN;GQ;GW;GY;GF;HT;HM;HN;HK;HU;IM;MU;VG;VI;IN;ID;IR;IQ;IS;IL;IT;JM;JP;JE;JO;KZ;KE;KG;KI;KW;LA;LS;LV;LB;LR;LY;LI;LT;LU;MO;MK;MG;MY;MW;MV;ML;MT;MP;MA;MH;MQ;MR;YT;MX;FM;MD;MC;MN;ME;MS;MZ;NA;NR;NP;NI;NE;NG;NU;NF;NO;NC;NZ;IO;OM;UG;UZ;PK;PW;PS;PA;PG;PY;NL;PE;PH;PN;PL;PF;PR;PT;QA;DO;CZ;RE;RO;GB;RU;RW;EH;BL;KN;SM;MF;PM;VA;VC;LC;SB;WS;AS;ST;SN;RS;SC;SL;SG;SK;SI;SO;SD;LK;SE;CH;SR;SJ;SZ;SY;TJ;TW;TZ;TD;TF;TH;TL;TG;TK;TO;TT;TN;TM;TC;TR;TV;UA;UY;US;VU;VE;VN;WF;YE;ZM;ZW', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(100, NULL, NULL, 'PS_GEOLOCATION_BEHAVIOR', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(101, NULL, NULL, 'PS_LOCALE_LANGUAGE', 'en', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(102, NULL, NULL, 'PS_LOCALE_COUNTRY', 'us', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(103, NULL, NULL, 'PS_ATTACHMENT_MAXIMUM_SIZE', '8', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(104, NULL, NULL, 'PS_SMARTY_CACHE', '0', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(105, NULL, NULL, 'PS_DIMENSION_UNIT', 'in', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(106, NULL, NULL, 'PS_GUEST_CHECKOUT_ENABLED', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(107, NULL, NULL, 'PS_DISPLAY_SUPPLIERS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(108, NULL, NULL, 'PS_DISPLAY_BEST_SELLERS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(109, NULL, NULL, 'PS_CATALOG_MODE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(110, NULL, NULL, 'PS_GEOLOCATION_WHITELIST', '127;209.185.108;209.185.253;209.85.238;209.85.238.11;209.85.238.4;216.239.33.96;216.239.33.97;216.239.33.98;216.239.33.99;216.239.37.98;216.239.37.99;216.239.39.98;216.239.39.99;216.239.41.96;216.239.41.97;216.239.41.98;216.239.41.99;216.239.45.4;216.239.46;216.239.51.96;216.239.51.97;216.239.51.98;216.239.51.99;216.239.53.98;216.239.53.99;216.239.57.96;216.239.57.97;216.239.57.98;216.239.57.99;216.239.59.98;216.239.59.99;216.33.229.163;64.233.173.193;64.233.173.194;64.233.173.195;64.233.173.196;64.233.173.197;64.233.173.198;64.233.173.199;64.233.173.200;64.233.173.201;64.233.173.202;64.233.173.203;64.233.173.204;64.233.173.205;64.233.173.206;64.233.173.207;64.233.173.208;64.233.173.209;64.233.173.210;64.233.173.211;64.233.173.212;64.233.173.213;64.233.173.214;64.233.173.215;64.233.173.216;64.233.173.217;64.233.173.218;64.233.173.219;64.233.173.220;64.233.173.221;64.233.173.222;64.233.173.223;64.233.173.224;64.233.173.225;64.233.173.226;64.233.173.227;64.233.173.228;64.233.173.229;64.233.173.230;64.233.173.231;64.233.173.232;64.233.173.233;64.233.173.234;64.233.173.235;64.233.173.236;64.233.173.237;64.233.173.238;64.233.173.239;64.233.173.240;64.233.173.241;64.233.173.242;64.233.173.243;64.233.173.244;64.233.173.245;64.233.173.246;64.233.173.247;64.233.173.248;64.233.173.249;64.233.173.250;64.233.173.251;64.233.173.252;64.233.173.253;64.233.173.254;64.233.173.255;64.68.80;64.68.81;64.68.82;64.68.83;64.68.84;64.68.85;64.68.86;64.68.87;64.68.88;64.68.89;64.68.90.1;64.68.90.10;64.68.90.11;64.68.90.12;64.68.90.129;64.68.90.13;64.68.90.130;64.68.90.131;64.68.90.132;64.68.90.133;64.68.90.134;64.68.90.135;64.68.90.136;64.68.90.137;64.68.90.138;64.68.90.139;64.68.90.14;64.68.90.140;64.68.90.141;64.68.90.142;64.68.90.143;64.68.90.144;64.68.90.145;64.68.90.146;64.68.90.147;64.68.90.148;64.68.90.149;64.68.90.15;64.68.90.150;64.68.90.151;64.68.90.152;64.68.90.153;64.68.90.154;64.68.90.155;64.68.90.156;64.68.90.157;64.68.90.158;64.68.90.159;64.68.90.16;64.68.90.160;64.68.90.161;64.68.90.162;64.68.90.163;64.68.90.164;64.68.90.165;64.68.90.166;64.68.90.167;64.68.90.168;64.68.90.169;64.68.90.17;64.68.90.170;64.68.90.171;64.68.90.172;64.68.90.173;64.68.90.174;64.68.90.175;64.68.90.176;64.68.90.177;64.68.90.178;64.68.90.179;64.68.90.18;64.68.90.180;64.68.90.181;64.68.90.182;64.68.90.183;64.68.90.184;64.68.90.185;64.68.90.186;64.68.90.187;64.68.90.188;64.68.90.189;64.68.90.19;64.68.90.190;64.68.90.191;64.68.90.192;64.68.90.193;64.68.90.194;64.68.90.195;64.68.90.196;64.68.90.197;64.68.90.198;64.68.90.199;64.68.90.2;64.68.90.20;64.68.90.200;64.68.90.201;64.68.90.202;64.68.90.203;64.68.90.204;64.68.90.205;64.68.90.206;64.68.90.207;64.68.90.208;64.68.90.21;64.68.90.22;64.68.90.23;64.68.90.24;64.68.90.25;64.68.90.26;64.68.90.27;64.68.90.28;64.68.90.29;64.68.90.3;64.68.90.30;64.68.90.31;64.68.90.32;64.68.90.33;64.68.90.34;64.68.90.35;64.68.90.36;64.68.90.37;64.68.90.38;64.68.90.39;64.68.90.4;64.68.90.40;64.68.90.41;64.68.90.42;64.68.90.43;64.68.90.44;64.68.90.45;64.68.90.46;64.68.90.47;64.68.90.48;64.68.90.49;64.68.90.5;64.68.90.50;64.68.90.51;64.68.90.52;64.68.90.53;64.68.90.54;64.68.90.55;64.68.90.56;64.68.90.57;64.68.90.58;64.68.90.59;64.68.90.6;64.68.90.60;64.68.90.61;64.68.90.62;64.68.90.63;64.68.90.64;64.68.90.65;64.68.90.66;64.68.90.67;64.68.90.68;64.68.90.69;64.68.90.7;64.68.90.70;64.68.90.71;64.68.90.72;64.68.90.73;64.68.90.74;64.68.90.75;64.68.90.76;64.68.90.77;64.68.90.78;64.68.90.79;64.68.90.8;64.68.90.80;64.68.90.9;64.68.91;64.68.92;66.249.64;66.249.65;66.249.66;66.249.67;66.249.68;66.249.69;66.249.70;66.249.71;66.249.72;66.249.73;66.249.78;66.249.79;72.14.199;8.6.48', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(111, NULL, NULL, 'PS_LOGS_BY_EMAIL', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(112, NULL, NULL, 'PS_COOKIE_CHECKIP', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(113, NULL, NULL, 'PS_STORES_CENTER_LAT', '25.948969', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(114, NULL, NULL, 'PS_STORES_CENTER_LONG', '-80.226439', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(115, NULL, NULL, 'PS_USE_ECOTAX', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(116, NULL, NULL, 'PS_CANONICAL_REDIRECT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(117, NULL, NULL, 'PS_IMG_UPDATE_TIME', '1431463787', '0000-00-00 00:00:00', '2015-05-12 16:49:47'),
	(118, NULL, NULL, 'PS_BACKUP_DROP_TABLE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(119, NULL, NULL, 'PS_OS_CHEQUE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(120, NULL, NULL, 'PS_OS_PAYMENT', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(121, NULL, NULL, 'PS_OS_PREPARATION', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(122, NULL, NULL, 'PS_OS_SHIPPING', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(123, NULL, NULL, 'PS_OS_DELIVERED', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(124, NULL, NULL, 'PS_OS_CANCELED', '6', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(125, NULL, NULL, 'PS_OS_REFUND', '7', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(126, NULL, NULL, 'PS_OS_ERROR', '8', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(127, NULL, NULL, 'PS_OS_OUTOFSTOCK', '9', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(128, NULL, NULL, 'PS_OS_BANKWIRE', '10', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(129, NULL, NULL, 'PS_OS_PAYPAL', '11', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(130, NULL, NULL, 'PS_OS_WS_PAYMENT', '12', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(131, NULL, NULL, 'PS_OS_OUTOFSTOCK_PAID', '9', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(132, NULL, NULL, 'PS_OS_OUTOFSTOCK_UNPAID', '13', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(133, NULL, NULL, 'PS_OS_COD_VALIDATION', '14', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(134, NULL, NULL, 'PS_LEGACY_IMAGES', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(135, NULL, NULL, 'PS_IMAGE_QUALITY', 'png', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(136, NULL, NULL, 'PS_PNG_QUALITY', '7', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(137, NULL, NULL, 'PS_JPEG_QUALITY', '90', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(138, NULL, NULL, 'PS_COOKIE_LIFETIME_FO', '480', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(139, NULL, NULL, 'PS_COOKIE_LIFETIME_BO', '480', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(140, NULL, NULL, 'PS_RESTRICT_DELIVERED_COUNTRIES', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(141, NULL, NULL, 'PS_SHOW_NEW_ORDERS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(142, NULL, NULL, 'PS_SHOW_NEW_CUSTOMERS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(143, NULL, NULL, 'PS_SHOW_NEW_MESSAGES', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(144, NULL, NULL, 'PS_FEATURE_FEATURE_ACTIVE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(145, NULL, NULL, 'PS_COMBINATION_FEATURE_ACTIVE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(146, NULL, NULL, 'PS_SPECIFIC_PRICE_FEATURE_ACTIVE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(147, NULL, NULL, 'PS_SCENE_FEATURE_ACTIVE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(148, NULL, NULL, 'PS_VIRTUAL_PROD_FEATURE_ACTIVE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(149, NULL, NULL, 'PS_CUSTOMIZATION_FEATURE_ACTIVE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(150, NULL, NULL, 'PS_CART_RULE_FEATURE_ACTIVE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(151, NULL, NULL, 'PS_PACK_FEATURE_ACTIVE', NULL, '0000-00-00 00:00:00', '2015-05-12 10:23:27'),
	(152, NULL, NULL, 'PS_ALIAS_FEATURE_ACTIVE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(153, NULL, NULL, 'PS_TAX_ADDRESS_TYPE', 'id_address_delivery', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(154, NULL, NULL, 'PS_SHOP_DEFAULT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(155, NULL, NULL, 'PS_CARRIER_DEFAULT_SORT', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(156, NULL, NULL, 'PS_STOCK_MVT_INC_REASON_DEFAULT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(157, NULL, NULL, 'PS_STOCK_MVT_DEC_REASON_DEFAULT', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(158, NULL, NULL, 'PS_ADVANCED_STOCK_MANAGEMENT', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(159, NULL, NULL, 'PS_ADMINREFRESH_NOTIFICATION', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(160, NULL, NULL, 'PS_STOCK_MVT_TRANSFER_TO', '7', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(161, NULL, NULL, 'PS_STOCK_MVT_TRANSFER_FROM', '6', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(162, NULL, NULL, 'PS_CARRIER_DEFAULT_ORDER', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(163, NULL, NULL, 'PS_STOCK_MVT_SUPPLY_ORDER', '8', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(164, NULL, NULL, 'PS_STOCK_CUSTOMER_ORDER_REASON', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(165, NULL, NULL, 'PS_UNIDENTIFIED_GROUP', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(166, NULL, NULL, 'PS_GUEST_GROUP', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(167, NULL, NULL, 'PS_CUSTOMER_GROUP', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(168, NULL, NULL, 'PS_SMARTY_CONSOLE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(169, NULL, NULL, 'PS_INVOICE_MODEL', 'invoice', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(170, NULL, NULL, 'PS_LIMIT_UPLOAD_IMAGE_VALUE', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(171, NULL, NULL, 'PS_LIMIT_UPLOAD_FILE_VALUE', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(172, NULL, NULL, 'MB_PAY_TO_EMAIL', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(173, NULL, NULL, 'MB_SECRET_WORD', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(174, NULL, NULL, 'MB_HIDE_LOGIN', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(175, NULL, NULL, 'MB_ID_LOGO', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(176, NULL, NULL, 'MB_ID_LOGO_WALLET', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(177, NULL, NULL, 'MB_PARAMETERS', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(178, NULL, NULL, 'MB_PARAMETERS_2', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(179, NULL, NULL, 'MB_DISPLAY_MODE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(180, NULL, NULL, 'MB_CANCEL_URL', 'http://www.yoursite.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(181, NULL, NULL, 'MB_LOCAL_METHODS', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(182, NULL, NULL, 'MB_INTER_METHODS', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(183, NULL, NULL, 'BANK_WIRE_CURRENCIES', '2,1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(184, NULL, NULL, 'CHEQUE_CURRENCIES', '2,1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(185, NULL, NULL, 'PRODUCTS_VIEWED_NBR', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(186, NULL, NULL, 'BLOCK_CATEG_DHTML', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(187, NULL, NULL, 'BLOCK_CATEG_MAX_DEPTH', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(188, NULL, NULL, 'MANUFACTURER_DISPLAY_FORM', '1', '0000-00-00 00:00:00', '2015-05-08 14:00:40'),
	(189, NULL, NULL, 'MANUFACTURER_DISPLAY_TEXT', '1', '0000-00-00 00:00:00', '2015-05-08 14:00:40'),
	(190, NULL, NULL, 'MANUFACTURER_DISPLAY_TEXT_NB', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(191, NULL, NULL, 'NEW_PRODUCTS_NBR', '5', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(192, NULL, NULL, 'PS_TOKEN_ENABLE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(193, NULL, NULL, 'PS_STATS_RENDER', 'graphnvd3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(194, NULL, NULL, 'PS_STATS_OLD_CONNECT_AUTO_CLEAN', 'never', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(195, NULL, NULL, 'PS_STATS_GRID_RENDER', 'gridhtml', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(196, NULL, NULL, 'BLOCKTAGS_NBR', '10', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(197, NULL, NULL, 'CHECKUP_DESCRIPTIONS_LT', '100', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(198, NULL, NULL, 'CHECKUP_DESCRIPTIONS_GT', '400', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(199, NULL, NULL, 'CHECKUP_IMAGES_LT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(200, NULL, NULL, 'CHECKUP_IMAGES_GT', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(201, NULL, NULL, 'CHECKUP_SALES_LT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(202, NULL, NULL, 'CHECKUP_SALES_GT', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(203, NULL, NULL, 'CHECKUP_STOCK_LT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(204, NULL, NULL, 'CHECKUP_STOCK_GT', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(205, NULL, NULL, 'FOOTER_CMS', '0_2|0_3|0_4|0_5|0_6', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(206, NULL, NULL, 'FOOTER_BLOCK_ACTIVATION', '1', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(207, NULL, NULL, 'FOOTER_POWEREDBY', '0', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(208, NULL, NULL, 'BLOCKADVERT_LINK', 'http://www.prestashop.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(209, NULL, NULL, 'BLOCKSTORE_IMG', 'store.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(210, NULL, NULL, 'BLOCKADVERT_IMG_EXT', 'jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(211, NULL, NULL, 'MOD_BLOCKTOPMENU_ITEMS', 'CMS_CAT1,CMS4', '0000-00-00 00:00:00', '2015-05-12 15:58:51'),
	(212, NULL, NULL, 'MOD_BLOCKTOPMENU_SEARCH', NULL, '0000-00-00 00:00:00', '2015-05-12 15:58:52'),
	(213, NULL, NULL, 'BLOCKSOCIAL_FACEBOOK', 'http://www.facebook.com/prestashop', '0000-00-00 00:00:00', '2015-05-08 14:00:39'),
	(214, NULL, NULL, 'BLOCKSOCIAL_TWITTER', 'http://www.twitter.com/prestashop', '0000-00-00 00:00:00', '2015-05-08 14:00:39'),
	(215, NULL, NULL, 'BLOCKSOCIAL_RSS', 'http://www.prestashop.com/blog/en/feed/', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(216, NULL, NULL, 'BLOCKCONTACTINFOS_COMPANY', 'Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sosqu ad litora torquent per conubia ', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(217, NULL, NULL, 'BLOCKCONTACTINFOS_ADDRESS', 'No 1104 Sky Tower,  Newyork, USA', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(218, NULL, NULL, 'BLOCKCONTACTINFOS_PHONE', '0123-456-789', '0000-00-00 00:00:00', '2015-05-08 14:00:40'),
	(219, NULL, NULL, 'BLOCKCONTACTINFOS_EMAIL', 'sales@yourcompany.com', '0000-00-00 00:00:00', '2015-05-08 14:00:40'),
	(220, NULL, NULL, 'BLOCKCONTACT_TELNUMBER', '0123-456-789', '0000-00-00 00:00:00', '2015-05-08 14:00:40'),
	(221, NULL, NULL, 'BLOCKCONTACT_EMAIL', 'sales@yourcompany.com', '0000-00-00 00:00:00', '2015-05-08 14:00:40'),
	(222, NULL, NULL, 'SUPPLIER_DISPLAY_TEXT', '1', '0000-00-00 00:00:00', '2015-05-08 14:00:40'),
	(223, NULL, NULL, 'SUPPLIER_DISPLAY_TEXT_NB', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(224, NULL, NULL, 'SUPPLIER_DISPLAY_FORM', '1', '0000-00-00 00:00:00', '2015-05-08 14:00:40'),
	(225, NULL, NULL, 'BLOCK_CATEG_NBR_COLUMN_FOOTER', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(226, NULL, NULL, 'UPGRADER_BACKUPDB_FILENAME', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(227, NULL, NULL, 'UPGRADER_BACKUPFILES_FILENAME', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(228, NULL, NULL, 'BLOCKREINSURANCE_NBBLOCKS', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(229, NULL, NULL, 'HOMESLIDER_WIDTH', '779', '0000-00-00 00:00:00', '2015-05-08 14:00:41'),
	(230, NULL, NULL, 'HOMESLIDER_SPEED', '500', '0000-00-00 00:00:00', '2015-05-08 14:00:41'),
	(231, NULL, NULL, 'HOMESLIDER_PAUSE', '3000', '0000-00-00 00:00:00', '2015-05-08 14:00:41'),
	(232, NULL, NULL, 'HOMESLIDER_LOOP', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(233, NULL, NULL, 'PS_BASE_DISTANCE_UNIT', 'ft', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(234, NULL, NULL, 'PS_SHOP_DOMAIN', '192.168.1.27', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(235, NULL, NULL, 'PS_SHOP_DOMAIN_SSL', '192.168.1.27', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(236, NULL, NULL, 'PS_SHOP_NAME', 'MixMobile', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(237, NULL, NULL, 'PS_SHOP_EMAIL', 'admin@admin.com', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(238, NULL, NULL, 'PS_MAIL_METHOD', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(239, NULL, NULL, 'PS_SHOP_ACTIVITY', '0', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(240, NULL, NULL, 'PS_LOGO', 'celio-logo-1431107684.jpg', '0000-00-00 00:00:00', '2015-05-08 13:54:44'),
	(241, NULL, NULL, 'PS_FAVICON', 'favicon.ico', '0000-00-00 00:00:00', '2015-05-08 13:55:22'),
	(242, NULL, NULL, 'PS_STORES_ICON', 'logo_stores.gif', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(243, NULL, NULL, 'PS_ROOT_CATEGORY', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(244, NULL, NULL, 'PS_HOME_CATEGORY', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(245, NULL, NULL, 'PS_CONFIGURATION_AGREMENT', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(246, NULL, NULL, 'PS_MAIL_SERVER', 'smtp.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(247, NULL, NULL, 'PS_MAIL_USER', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(248, NULL, NULL, 'PS_MAIL_PASSWD', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(249, NULL, NULL, 'PS_MAIL_SMTP_ENCRYPTION', 'off', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(250, NULL, NULL, 'PS_MAIL_SMTP_PORT', '25', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(251, NULL, NULL, 'PS_MAIL_COLOR', '#db3484', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(252, NULL, NULL, 'NW_SALT', 'DpFnKIPaN11fBBPU', '0000-00-00 00:00:00', '2015-05-08 14:03:29'),
	(253, NULL, NULL, 'PS_PAYMENT_LOGO_CMS_ID', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(254, NULL, NULL, 'HOME_FEATURED_NBR', '8', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(255, NULL, NULL, 'SEK_MIN_OCCURENCES', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(256, NULL, NULL, 'SEK_FILTER_KW', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(257, NULL, NULL, 'PS_ALLOW_MOBILE_DEVICE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(258, NULL, NULL, 'PS_CUSTOMER_CREATION_EMAIL', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(259, NULL, NULL, 'PS_SMARTY_CONSOLE_KEY', 'SMARTY_DEBUG', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(260, NULL, NULL, 'PS_DASHBOARD_USE_PUSH', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(261, NULL, NULL, 'PS_ATTRIBUTE_ANCHOR_SEPARATOR', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(262, NULL, NULL, 'CONF_AVERAGE_PRODUCT_MARGIN', '40', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(263, NULL, NULL, 'PS_DASHBOARD_SIMULATION', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(264, NULL, NULL, 'PS_QUICK_VIEW', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(265, NULL, NULL, 'PS_USE_HTMLPURIFIER', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(266, NULL, NULL, 'PS_SMARTY_CACHING_TYPE', 'filesystem', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(267, NULL, NULL, 'PS_SMARTY_CLEAR_CACHE', 'everytime', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(268, NULL, NULL, 'PS_DETECT_LANG', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(269, NULL, NULL, 'PS_DETECT_COUNTRY', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(270, NULL, NULL, 'PS_ROUND_TYPE', '1', '0000-00-00 00:00:00', '2015-05-08 14:03:30'),
	(271, NULL, NULL, 'PS_PRICE_DISPLAY_PRECISION', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(272, NULL, NULL, 'PS_LOG_EMAILS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(273, NULL, NULL, 'PS_CUSTOMER_NWSL', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(274, NULL, NULL, 'PS_CUSTOMER_OPTIN', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(275, NULL, NULL, 'PS_PACK_STOCK_TYPE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(276, NULL, NULL, 'PS_SC_TWITTER', '1', '2015-05-08 14:00:39', '2015-05-12 14:04:39'),
	(277, NULL, NULL, 'PS_SC_FACEBOOK', '1', '2015-05-08 14:00:39', '2015-05-12 14:04:39'),
	(278, NULL, NULL, 'PS_SC_GOOGLE', '1', '2015-05-08 14:00:39', '2015-05-12 14:04:39'),
	(279, NULL, NULL, 'PS_SC_PINTEREST', '1', '2015-05-08 14:00:39', '2015-05-12 14:04:39'),
	(280, NULL, NULL, 'BLOCKBANNER_IMG', NULL, '2015-05-08 14:00:39', '2015-05-08 14:00:39'),
	(281, NULL, NULL, 'BLOCKBANNER_LINK', NULL, '2015-05-08 14:00:39', '2015-05-08 14:00:39'),
	(282, NULL, NULL, 'BLOCKBANNER_DESC', NULL, '2015-05-08 14:00:39', '2015-05-08 14:00:39'),
	(283, NULL, NULL, 'CONF_BANKWIRE_FIXED', '0.2', '2015-05-08 14:00:39', '2015-05-08 14:00:39'),
	(284, NULL, NULL, 'CONF_BANKWIRE_VAR', '2', '2015-05-08 14:00:39', '2015-05-08 14:00:39'),
	(285, NULL, NULL, 'CONF_BANKWIRE_FIXED_FOREIGN', '0.2', '2015-05-08 14:00:39', '2015-05-08 14:00:39'),
	(286, NULL, NULL, 'CONF_BANKWIRE_VAR_FOREIGN', '2', '2015-05-08 14:00:39', '2015-05-08 14:00:39'),
	(287, NULL, NULL, 'PS_BLOCK_BESTSELLERS_TO_DISPLAY', '10', '2015-05-08 14:00:39', '2015-05-08 14:00:39'),
	(288, NULL, NULL, 'PS_BLOCK_CART_XSELL_LIMIT', '12', '2015-05-08 14:00:39', '2015-05-08 14:00:39'),
	(289, NULL, NULL, 'PS_BLOCK_CART_SHOW_CROSSSELLING', '1', '2015-05-08 14:00:39', '2015-05-08 14:00:39'),
	(290, NULL, NULL, 'BLOCKSOCIAL_YOUTUBE', '#', '2015-05-08 14:00:39', '2015-05-08 14:03:29'),
	(291, NULL, NULL, 'BLOCKSOCIAL_GOOGLE_PLUS', '#', '2015-05-08 14:00:39', '2015-05-08 14:03:29'),
	(292, NULL, NULL, 'BLOCKSOCIAL_PINTEREST', '#', '2015-05-08 14:00:39', '2015-05-08 14:03:29'),
	(293, NULL, NULL, 'BLOCKSOCIAL_VIMEO', NULL, '2015-05-08 14:00:39', '2015-05-08 14:00:39'),
	(294, NULL, NULL, 'BLOCKSOCIAL_INSTAGRAM', NULL, '2015-05-08 14:00:39', '2015-05-08 14:00:39'),
	(295, NULL, NULL, 'BLOCK_CATEG_ROOT_CATEGORY', '1', '2015-05-08 14:00:39', '2015-05-08 14:00:39'),
	(296, NULL, NULL, 'blockfacebook_url', 'https://www.facebook.com/prestashop', '2015-05-08 14:00:39', '2015-05-08 14:00:39'),
	(297, NULL, NULL, 'PS_LAYERED_HIDE_0_VALUES', '1', '2015-05-08 14:00:39', '2015-05-08 14:00:39'),
	(298, NULL, NULL, 'PS_LAYERED_SHOW_QTIES', '1', '2015-05-08 14:00:39', '2015-05-08 14:00:39'),
	(299, NULL, NULL, 'PS_LAYERED_FULL_TREE', '1', '2015-05-08 14:00:39', '2015-05-08 14:00:39'),
	(300, NULL, NULL, 'PS_LAYERED_FILTER_PRICE_USETAX', '1', '2015-05-08 14:00:39', '2015-05-08 14:00:39'),
	(301, NULL, NULL, 'PS_LAYERED_FILTER_CATEGORY_DEPTH', '1', '2015-05-08 14:00:39', '2015-05-12 10:28:29'),
	(302, NULL, NULL, 'PS_LAYERED_FILTER_INDEX_QTY', '0', '2015-05-08 14:00:39', '2015-05-08 14:00:39'),
	(303, NULL, NULL, 'PS_LAYERED_FILTER_INDEX_CDT', '0', '2015-05-08 14:00:39', '2015-05-08 14:00:39'),
	(304, NULL, NULL, 'PS_LAYERED_FILTER_INDEX_MNF', '0', '2015-05-08 14:00:39', '2015-05-08 14:00:39'),
	(305, NULL, NULL, 'PS_LAYERED_FILTER_INDEX_CAT', '0', '2015-05-08 14:00:39', '2015-05-08 14:00:39'),
	(306, NULL, NULL, 'PS_LAYERED_INDEXED', '1', '2015-05-08 14:00:40', '2015-05-08 14:00:40'),
	(307, NULL, NULL, 'FOOTER_PRICE-DROP', '1', '2015-05-08 14:00:40', '2015-05-08 14:00:40'),
	(308, NULL, NULL, 'FOOTER_NEW-PRODUCTS', '1', '2015-05-08 14:00:40', '2015-05-08 14:00:40'),
	(309, NULL, NULL, 'FOOTER_BEST-SALES', '1', '2015-05-08 14:00:40', '2015-05-08 14:00:40'),
	(310, NULL, NULL, 'FOOTER_CONTACT', '1', '2015-05-08 14:00:40', '2015-05-08 14:00:40'),
	(311, NULL, NULL, 'FOOTER_SITEMAP', '1', '2015-05-08 14:00:40', '2015-05-08 14:00:40'),
	(312, NULL, NULL, 'BLOCKSPECIALS_NB_CACHES', '20', '2015-05-08 14:00:40', '2015-05-08 14:00:40'),
	(313, NULL, NULL, 'BLOCKSPECIALS_SPECIALS_NBR', '5', '2015-05-08 14:00:40', '2015-05-08 14:00:40'),
	(314, NULL, NULL, 'BLOCKTAGS_MAX_LEVEL', '3', '2015-05-08 14:00:40', '2015-05-08 14:00:40'),
	(315, NULL, NULL, 'CONF_CHEQUE_FIXED', '0.2', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(316, NULL, NULL, 'CONF_CHEQUE_VAR', '2', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(317, NULL, NULL, 'CONF_CHEQUE_FIXED_FOREIGN', '0.2', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(318, NULL, NULL, 'CONF_CHEQUE_VAR_FOREIGN', '2', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(319, NULL, NULL, 'DASHACTIVITY_CART_ACTIVE', '30', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(320, NULL, NULL, 'DASHACTIVITY_CART_ABANDONED_MIN', '24', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(321, NULL, NULL, 'DASHACTIVITY_CART_ABANDONED_MAX', '48', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(322, NULL, NULL, 'DASHACTIVITY_VISITOR_ONLINE', '30', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(323, NULL, NULL, 'PS_DASHGOALS_CURRENT_YEAR', '2014', '2015-05-08 14:00:41', '2015-05-08 14:03:30'),
	(324, NULL, NULL, 'DASHGOALS_TRAFFIC_01_2015', '600', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(325, NULL, NULL, 'DASHGOALS_CONVERSION_01_2015', '2', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(326, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_01_2015', '80', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(327, NULL, NULL, 'DASHGOALS_TRAFFIC_02_2015', '600', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(328, NULL, NULL, 'DASHGOALS_CONVERSION_02_2015', '2', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(329, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_02_2015', '80', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(330, NULL, NULL, 'DASHGOALS_TRAFFIC_03_2015', '600', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(331, NULL, NULL, 'DASHGOALS_CONVERSION_03_2015', '2', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(332, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_03_2015', '80', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(333, NULL, NULL, 'DASHGOALS_TRAFFIC_04_2015', '600', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(334, NULL, NULL, 'DASHGOALS_CONVERSION_04_2015', '2', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(335, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_04_2015', '80', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(336, NULL, NULL, 'DASHGOALS_TRAFFIC_05_2015', '600', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(337, NULL, NULL, 'DASHGOALS_CONVERSION_05_2015', '2', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(338, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_05_2015', '80', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(339, NULL, NULL, 'DASHGOALS_TRAFFIC_06_2015', '600', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(340, NULL, NULL, 'DASHGOALS_CONVERSION_06_2015', '2', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(341, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_06_2015', '80', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(342, NULL, NULL, 'DASHGOALS_TRAFFIC_07_2015', '600', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(343, NULL, NULL, 'DASHGOALS_CONVERSION_07_2015', '2', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(344, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_07_2015', '80', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(345, NULL, NULL, 'DASHGOALS_TRAFFIC_08_2015', '600', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(346, NULL, NULL, 'DASHGOALS_CONVERSION_08_2015', '2', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(347, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_08_2015', '80', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(348, NULL, NULL, 'DASHGOALS_TRAFFIC_09_2015', '600', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(349, NULL, NULL, 'DASHGOALS_CONVERSION_09_2015', '2', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(350, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_09_2015', '80', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(351, NULL, NULL, 'DASHGOALS_TRAFFIC_10_2015', '600', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(352, NULL, NULL, 'DASHGOALS_CONVERSION_10_2015', '2', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(353, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_10_2015', '80', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(354, NULL, NULL, 'DASHGOALS_TRAFFIC_11_2015', '600', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(355, NULL, NULL, 'DASHGOALS_CONVERSION_11_2015', '2', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(356, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_11_2015', '80', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(357, NULL, NULL, 'DASHGOALS_TRAFFIC_12_2015', '600', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(358, NULL, NULL, 'DASHGOALS_CONVERSION_12_2015', '2', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(359, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_12_2015', '80', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(360, NULL, NULL, 'DASHPRODUCT_NBR_SHOW_LAST_ORDER', '10', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(361, NULL, NULL, 'DASHPRODUCT_NBR_SHOW_BEST_SELLER', '10', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(362, NULL, NULL, 'DASHPRODUCT_NBR_SHOW_MOST_VIEWED', '10', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(363, NULL, NULL, 'DASHPRODUCT_NBR_SHOW_TOP_SEARCH', '10', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(364, NULL, NULL, 'HOME_FEATURED_CAT', '2', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(365, NULL, NULL, 'PRODUCTPAYMENTLOGOS_IMG', 'payment-logo.png', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(366, NULL, NULL, 'PRODUCTPAYMENTLOGOS_LINK', NULL, '2015-05-08 14:00:41', '2015-05-08 14:03:30'),
	(367, NULL, NULL, 'PRODUCTPAYMENTLOGOS_TITLE', NULL, '2015-05-08 14:00:41', '2015-05-08 14:03:30'),
	(368, NULL, NULL, 'PS_TC_THEMES', 'a:9:{i:0;s:6:"theme1";i:1;s:6:"theme2";i:2;s:6:"theme3";i:3;s:6:"theme4";i:4;s:6:"theme5";i:5;s:6:"theme6";i:6;s:6:"theme7";i:7;s:6:"theme8";i:8;s:6:"theme9";}', '2015-05-08 14:00:41', '2015-05-08 14:03:30'),
	(369, NULL, NULL, 'PS_TC_FONTS', 'a:10:{s:5:"font1";s:9:"Open Sans";s:5:"font2";s:12:"Josefin Slab";s:5:"font3";s:4:"Arvo";s:5:"font4";s:4:"Lato";s:5:"font5";s:7:"Volkorn";s:5:"font6";s:13:"Abril Fatface";s:5:"font7";s:6:"Ubuntu";s:5:"font8";s:7:"PT Sans";s:5:"font9";s:15:"Old Standard TT";s:6:"font10";s:10:"Droid Sans";}', '2015-05-08 14:00:41', '2015-05-08 14:03:30'),
	(370, NULL, NULL, 'PS_TC_THEME', NULL, '2015-05-08 14:00:41', '2015-05-08 14:03:30'),
	(371, NULL, NULL, 'PS_TC_FONT', NULL, '2015-05-08 14:00:41', '2015-05-08 14:03:30'),
	(372, NULL, NULL, 'PS_TC_ACTIVE', '1', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(373, NULL, NULL, 'PS_SET_DISPLAY_SUBCATEGORIES', '1', '2015-05-08 14:00:41', '2015-05-08 14:00:41'),
	(374, NULL, NULL, 'GF_INSTALL_CALC', '1', '2015-05-08 14:01:08', '2015-05-08 14:02:36'),
	(375, NULL, NULL, 'GF_CURRENT_LEVEL', '4', '2015-05-08 14:01:08', '2015-05-08 13:51:54'),
	(376, NULL, NULL, 'GF_CURRENT_LEVEL_PERCENT', '0', '2015-05-08 14:01:08', '2015-05-08 13:51:54'),
	(377, NULL, NULL, 'GF_NOTIFICATION', '44', '2015-05-08 14:01:08', '2015-05-08 13:51:54'),
	(378, NULL, NULL, 'PRODUCT_COMMENTS_MINIMAL_TIME', '2000', '2015-05-08 14:01:08', '2015-05-08 14:03:30'),
	(379, NULL, NULL, 'PRODUCT_COMMENTS_ALLOW_GUESTS', '1', '2015-05-08 14:01:08', '2015-05-08 14:03:30'),
	(380, NULL, NULL, 'PRODUCT_COMMENTS_MODERATE', '1', '2015-05-08 14:01:08', '2015-05-08 14:01:08'),
	(381, NULL, NULL, 'CRONJOBS_ADMIN_DIR', '31ae34f57eeaa3364e6d4a179e3f0b33', '2015-05-08 14:01:08', '2015-05-08 13:40:42'),
	(382, NULL, NULL, 'CRONJOBS_MODE', 'webservice', '2015-05-08 14:01:08', '2015-05-08 14:01:08'),
	(383, NULL, NULL, 'CRONJOBS_MODULE_VERSION', '1.2.6', '2015-05-08 14:01:08', '2015-05-08 14:01:08'),
	(384, NULL, NULL, 'CRONJOBS_WEBSERVICE_ID', '0', '2015-05-08 14:01:08', '2015-05-08 14:01:08'),
	(385, NULL, NULL, 'CRONJOBS_EXECUTION_TOKEN', '0b77951c2f965fb42165e0e5d00c97b3', '2015-05-08 14:01:08', '2015-05-08 14:01:08'),
	(386, NULL, NULL, 'PS_ONBOARDING_CURRENT_STEP', '2', '2015-05-08 14:01:20', '2015-05-08 15:04:45'),
	(387, NULL, NULL, 'PS_ONBOARDING_LAST_VALIDATE_STEP', '0', '2015-05-08 14:01:20', '2015-05-08 14:01:20'),
	(388, NULL, NULL, 'PS_ONBOARDING_STEP_1_COMPLETED', '0', '2015-05-08 14:01:20', '2015-05-08 14:01:20'),
	(389, NULL, NULL, 'PS_ONBOARDING_STEP_2_COMPLETED', '0', '2015-05-08 14:01:20', '2015-05-08 14:01:20'),
	(390, NULL, NULL, 'PS_ONBOARDING_STEP_3_COMPLETED', '0', '2015-05-08 14:01:20', '2015-05-08 14:01:20'),
	(391, NULL, NULL, 'PS_ONBOARDING_STEP_4_COMPLETED', '0', '2015-05-08 14:01:20', '2015-05-08 14:01:20'),
	(392, NULL, NULL, 'GF_NOT_VIEWED_BADGE', '193', '2015-05-08 14:02:37', '2015-05-08 13:51:54'),
	(393, NULL, NULL, 'btmenu_iscache', '1', '2015-05-08 14:03:21', '2015-05-08 14:03:21'),
	(394, NULL, NULL, 'btmenu_cachetime', '24', '2015-05-08 14:03:21', '2015-05-08 14:03:21'),
	(395, NULL, NULL, 'LEOBLOG_CATEORY_MENU', '1', '2015-05-08 14:03:23', '2015-05-08 14:03:23'),
	(396, NULL, NULL, 'BLEOBLOGS_NBR', '6', '2015-05-08 14:03:23', '2015-05-08 14:03:23'),
	(397, NULL, NULL, 'BLEOBLOGS_PAGE', '3', '2015-05-08 14:03:23', '2015-05-08 14:03:23'),
	(398, NULL, NULL, 'BLEOBLOGS_COL', '3', '2015-05-08 14:03:23', '2015-05-08 14:03:23'),
	(399, NULL, NULL, 'BLEOBLOGS_INTV', '8000', '2015-05-08 14:03:23', '2015-05-08 14:03:23'),
	(400, NULL, NULL, 'BLEOBLOGS_SHOW', '0', '2015-05-08 14:03:23', '2015-05-08 14:03:29'),
	(401, NULL, NULL, 'BLEOBLOGS_WIDTH', '338', '2015-05-08 14:03:23', '2015-05-08 14:03:29'),
	(402, NULL, NULL, 'BLEOBLOGS_HEIGHT', '120', '2015-05-08 14:03:23', '2015-05-08 14:03:29'),
	(403, NULL, NULL, 'BLEOBLOGS_SCATE', '1', '2015-05-08 14:03:23', '2015-05-08 14:03:23'),
	(404, NULL, NULL, 'BLEOBLOGS_SIMA', '1', '2015-05-08 14:03:23', '2015-05-08 14:03:23'),
	(405, NULL, NULL, 'BLEOBLOGS_SAUT', '0', '2015-05-08 14:03:23', '2015-05-08 14:03:23'),
	(406, NULL, NULL, 'BLEOBLOGS_SCAT', '0', '2015-05-08 14:03:23', '2015-05-08 14:03:23'),
	(407, NULL, NULL, 'BLEOBLOGS_SCRE', '1', '2015-05-08 14:03:23', '2015-05-08 14:03:23'),
	(408, NULL, NULL, 'BLEOBLOGS_STITLE', '1', '2015-05-08 14:03:23', '2015-05-08 14:03:23'),
	(409, NULL, NULL, 'BLEOBLOGS_SCOUN', '0', '2015-05-08 14:03:23', '2015-05-08 14:03:23'),
	(410, NULL, NULL, 'BLEOBLOGS_SHITS', '0', '2015-05-08 14:03:23', '2015-05-08 14:03:23'),
	(411, NULL, NULL, 'LEOSLIDERLAYER_GROUP_DE', '4', '2015-05-08 14:03:23', '2015-05-08 14:16:00'),
	(412, NULL, NULL, 'PTSMAPL_HEIGHT', '500', '2015-05-08 14:03:24', '2015-05-08 14:03:24'),
	(413, NULL, NULL, 'PTSMAPL_DESCRIPTION', NULL, '2015-05-08 14:03:24', '2015-05-08 14:03:24'),
	(414, NULL, NULL, 'PTS_CP_ACTIVE', '1', '2015-05-08 14:03:26', '2015-05-08 14:03:26'),
	(415, NULL, NULL, 'PTSBLOCKREINSURANCE_NBBLOCKS', '5', '2015-05-08 14:03:27', '2015-05-08 14:03:27'),
	(416, NULL, NULL, 'PTS_CP_COPYRIGHT', NULL, '2015-05-08 14:03:28', '2015-05-08 13:49:36'),
	(417, NULL, NULL, 'PTS_CP_LAYOUT', 'fullwidth', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(418, NULL, NULL, 'PTS_CP_LOGOTYPE', 'logo-theme', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(419, NULL, NULL, 'PTS_CP_QUICKVIEW', '1', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(420, NULL, NULL, 'PTS_CP_THEME', 'fashion', '2015-05-08 14:03:28', '2015-05-08 13:49:36'),
	(421, NULL, NULL, 'PTS_CP_', NULL, '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(422, NULL, NULL, 'PTS_CP_CUSTOMCSS', NULL, '2015-05-08 14:03:28', '2015-05-08 13:49:36'),
	(423, NULL, NULL, 'PTS_CP_CUSTOMJS', NULL, '2015-05-08 14:03:28', '2015-05-08 13:49:36'),
	(424, NULL, NULL, 'PTS_CP_ENABLE_PHTML', '1', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(425, NULL, NULL, 'PTS_CP_PRODUCTHTML', NULL, '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(426, NULL, NULL, 'PTS_CP_PRODUCTS_ITEMROW', '3', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(427, NULL, NULL, 'PTS_CP_PRODUCT_ACCROW', '1', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(428, NULL, NULL, 'PTS_CP_PRODUCT_LAYOUT', 'swap', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(429, NULL, NULL, 'PTS_CP_PHTMLTAB', 'Custom Tab', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(430, NULL, NULL, 'PTS_CP_REHOOK', '0', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(431, NULL, NULL, 'PTS_CP_PRODUCT_STYLE', 'style1', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(432, NULL, NULL, 'PTS_CP_PROFILESASIGN', '1', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(433, NULL, NULL, 'PTS_CP_DEFAULTPBUILDER', '1414834909', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(434, NULL, NULL, 'PTS_CP_DEFAULTFBUILDER', '1414831728', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(435, NULL, NULL, 'PTS_CP_FASHIONPBUILDER', '1414836318', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(436, NULL, NULL, 'PTS_CP_FASHIONFBUILDER', '1414840717', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(437, NULL, NULL, 'PTS_CP_SPORTPBUILDER', '1414839482', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(438, NULL, NULL, 'PTS_CP_SPORTFBUILDER', '1414833517', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(439, NULL, NULL, 'PTS_CP_ENABLE_PBUILDER', '1', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(440, NULL, NULL, 'PTS_CP_ENABLE_FBUILDER', '1', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(441, NULL, NULL, 'PTS_CP_GIFTSPBUILDER', '1414838613', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(442, NULL, NULL, 'PTS_CP_GIFTSFBUILDER', '1414832874', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(443, NULL, NULL, 'PTS_CP_CDPBUILDER', '1414849996', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(444, NULL, NULL, 'PTS_CP_CDFBUILDER', '1414833478', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(445, NULL, NULL, 'PTS_MEGAMENU_PARAMS', '[{"align":"aligned-fullwidth","submenu":1,"subwidth":1170,"cols":1,"group":0,"id":15,"rows":[{"cols":[{"widgets":"wid-1395715955|wid-1400146905|wid-1411638230","colwidth":4},{"widgets":"wid-1411639541","colwidth":8}]}]},{"submenu":"0","subwidth":650,"id":4,"align":"aligned-center","cols":1,"group":0,"rows":[]},{"submenu":"0","subwidth":500,"id":7,"align":"aligned-center","cols":1,"group":0,"rows":[{"cols":[{"widgets":"wid-1395715955|wid-1395714496|wid-1400147913|wid-1411639034","colwidth":8},{"widgets":"wid-1411638230","colwidth":4}]}]}]', '2015-05-08 14:03:28', '2015-05-08 15:01:27'),
	(446, NULL, NULL, 'PTS_RELATE_PRO_COLUMNS', '4', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(447, NULL, NULL, 'PTS_RELATE_PRO_ITEMSPAGE', '4', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(448, NULL, NULL, 'PTS_RELATE_PRO_ITEMSTAB', '8', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(449, NULL, NULL, 'PTS_RELATE_PRO_PORDER', 'date_add DESC', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(450, NULL, NULL, 'PTS_TEST_LIMIT', '5', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(451, NULL, NULL, 'PTS_TEST_WIDTH', '450', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(452, NULL, NULL, 'PTS_TEST_HEIGHT', '250', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(453, NULL, NULL, 'PTS_TEST_MEDIA_WIDTH', '55', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(454, NULL, NULL, 'PTS_TEST_MEDIA_HEIGHT', '55', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(455, NULL, NULL, 'PTS_TEST_SPEED', '3000', '2015-05-08 14:03:28', '2015-05-08 14:03:28'),
	(456, NULL, NULL, 'FOOTER_CMS_TEXT_1', NULL, '2015-05-08 14:03:29', '2015-05-08 14:03:29'),
	(457, NULL, NULL, 'FOOTER_CMS_TEXT_2', NULL, '2015-05-08 14:03:29', '2015-05-08 14:03:29'),
	(458, NULL, NULL, 'FOOTER_CMS_TEXT_3', NULL, '2015-05-08 14:03:29', '2015-05-08 14:03:29'),
	(459, NULL, NULL, 'FOOTER_CMS_TEXT_4', NULL, '2015-05-08 14:03:29', '2015-05-08 14:03:29'),
	(460, NULL, NULL, 'FOOTER_CMS_TEXT_5', NULL, '2015-05-08 14:03:29', '2015-05-08 14:03:29'),
	(461, NULL, NULL, 'FOOTER_CMS_TEXT_6', NULL, '2015-05-08 14:03:29', '2015-05-08 14:03:29'),
	(462, NULL, NULL, 'FOOTER_CMS_TEXT_7', NULL, '2015-05-08 14:03:29', '2015-05-08 14:03:29'),
	(463, NULL, NULL, 'FOOTER_CMS_TEXT_8', NULL, '2015-05-08 14:03:29', '2015-05-08 14:03:29'),
	(464, NULL, NULL, 'FOOTER_CMS_TEXT_9', NULL, '2015-05-08 14:03:29', '2015-05-08 14:03:29'),
	(465, NULL, NULL, 'BLEOBLOGS_SDES', '1', '2015-05-08 14:03:29', '2015-05-08 14:03:29'),
	(466, NULL, NULL, 'PTS_MANUF_COL', '6', '2015-05-08 14:03:29', '2015-05-08 14:03:29'),
	(467, NULL, NULL, 'PTS_MANUF_INTV', '8000', '2015-05-08 14:03:29', '2015-05-08 14:03:29'),
	(468, NULL, NULL, 'PTS_MANUF_MODULE_TITLE', 'Product Brands', '2015-05-08 14:03:29', '2015-05-08 14:03:29'),
	(469, NULL, NULL, 'PTS_MANUF_PAGE', '6', '2015-05-08 14:03:29', '2015-05-08 14:03:29'),
	(470, NULL, NULL, 'PTS_MANUF_ACTIVE_TITLE', '0', '2015-05-08 14:03:29', '2015-05-08 14:03:29'),
	(471, NULL, NULL, 'PTS_MANUF_ACTIVE_PLAY', '0', '2015-05-08 14:03:29', '2015-05-08 14:03:29'),
	(472, NULL, NULL, 'PTS_MANUF_TYPE_IMG', 'logo_brand', '2015-05-08 14:03:29', '2015-05-08 14:03:29'),
	(473, NULL, NULL, 'PTSPROTAB_ALL', '0', '2015-05-08 14:03:29', '2015-05-08 14:03:29'),
	(474, NULL, NULL, 'PTSPROTAB_BESTSELLER', '1', '2015-05-08 14:03:29', '2015-05-08 14:03:29'),
	(475, NULL, NULL, 'PTSPROTAB_COLUMNS_PAGE', '3', '2015-05-08 14:03:29', '2015-05-08 14:03:29'),
	(476, NULL, NULL, 'PTSPROTAB_FEATURED', '1', '2015-05-08 14:03:29', '2015-05-08 14:03:29'),
	(477, NULL, NULL, 'PTSPROTAB_INTERVAL', '8000', '2015-05-08 14:03:29', '2015-05-08 14:03:29'),
	(478, NULL, NULL, 'PTSPROTAB_ITEMS_PAGE', '6', '2015-05-08 14:03:29', '2015-05-08 14:03:29'),
	(479, NULL, NULL, 'PTSPROTAB_ITEMS_TAB', '24', '2015-05-08 14:03:29', '2015-05-08 14:03:29'),
	(480, NULL, NULL, 'PTSPROTAB_NEWARRIALS', '1', '2015-05-08 14:03:29', '2015-05-08 14:03:29'),
	(481, NULL, NULL, 'PTSPROTAB_SPECIALS_DISPLAY', '1', '2015-05-08 14:03:29', '2015-05-08 14:03:29'),
	(482, NULL, NULL, 'PTSPROTAB_TOPRATING', '1', '2015-05-08 14:03:29', '2015-05-08 14:03:29'),
	(483, NULL, NULL, 'PTS_VERTICAL_MEGAMENU_PARAMS', '[{"id":2,"group":0,"cols":1,"subwidth":450,"submenu":1,"align":"aligned-left","rows":[{"cols":[{"widgets":"wid-1403689966|wid-1411640411","colwidth":12,"colclass":""}]}]},{"submenu":1,"subwidth":600,"id":3,"group":0,"cols":1,"align":"aligned-left","rows":[{"cols":[{"widgets":"wid-1403856546|wid-1403859237|wid-1411641320","colwidth":4},{"widgets":"wid-1403859237|wid-1411641320","colwidth":4},{"widgets":"wid-1403859237|wid-1411641320","colwidth":4}]},{"cols":[{"widgets":"wid-1403855869|wid-1411643422","colwidth":12}]}]},{"id":4,"group":0,"cols":1,"subwidth":640,"submenu":1,"align":"aligned-left","rows":[{"cols":[{"widgets":"wid-1403854487|wid-1411641757","colwidth":3},{"widgets":"wid-1403689971|wid-1411640508","colwidth":9}]}]},{"id":5,"group":0,"cols":1,"subwidth":700,"submenu":1,"align":"aligned-left","rows":[{"cols":[{"widgets":"wid-1403858121|wid-1411640595","colwidth":12}]}]},{"id":6,"group":0,"cols":1,"subwidth":800,"submenu":1,"align":"aligned-left","rows":[{"cols":[{"widgets":"wid-1403859852|wid-1411640692","colwidth":12}]}]},{"id":7,"group":0,"cols":1,"subwidth":700,"submenu":1,"align":"aligned-left","rows":[{"cols":[{"widgets":"wid-1403861031|wid-1411641757","colwidth":4},{"widgets":"wid-1403855952|wid-1411642153","colwidth":8}]}]}]', '2015-05-08 14:03:29', '2015-05-08 14:03:29'),
	(484, NULL, NULL, 'PS_HELPBOX', '1', '2015-05-08 14:03:29', '2015-05-08 14:03:29'),
	(485, NULL, NULL, 'PS_INVOICE_NUMBER', '1', '2015-05-08 14:03:29', '2015-05-08 14:03:29'),
	(486, NULL, NULL, 'HOMESLIDER_HEIGHT', '300', '2015-05-08 14:03:29', '2015-05-08 14:03:29'),
	(487, NULL, NULL, 'DASHGOALS_TRAFFIC_01_2014', '600', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(488, NULL, NULL, 'DASHGOALS_CONVERSION_01_2014', '2', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(489, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_01_2014', '80', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(490, NULL, NULL, 'DASHGOALS_TRAFFIC_02_2014', '600', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(491, NULL, NULL, 'DASHGOALS_CONVERSION_02_2014', '2', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(492, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_02_2014', '80', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(493, NULL, NULL, 'DASHGOALS_TRAFFIC_03_2014', '600', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(494, NULL, NULL, 'DASHGOALS_CONVERSION_03_2014', '2', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(495, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_03_2014', '80', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(496, NULL, NULL, 'DASHGOALS_TRAFFIC_04_2014', '600', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(497, NULL, NULL, 'DASHGOALS_CONVERSION_04_2014', '2', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(498, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_04_2014', '80', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(499, NULL, NULL, 'DASHGOALS_TRAFFIC_05_2014', '600', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(500, NULL, NULL, 'DASHGOALS_CONVERSION_05_2014', '2', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(501, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_05_2014', '80', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(502, NULL, NULL, 'DASHGOALS_TRAFFIC_06_2014', '600', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(503, NULL, NULL, 'DASHGOALS_CONVERSION_06_2014', '2', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(504, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_06_2014', '80', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(505, NULL, NULL, 'DASHGOALS_TRAFFIC_07_2014', '600', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(506, NULL, NULL, 'DASHGOALS_CONVERSION_07_2014', '2', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(507, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_07_2014', '80', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(508, NULL, NULL, 'DASHGOALS_TRAFFIC_08_2014', '600', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(509, NULL, NULL, 'DASHGOALS_CONVERSION_08_2014', '2', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(510, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_08_2014', '80', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(511, NULL, NULL, 'DASHGOALS_TRAFFIC_09_2014', '600', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(512, NULL, NULL, 'DASHGOALS_CONVERSION_09_2014', '2', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(513, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_09_2014', '80', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(514, NULL, NULL, 'DASHGOALS_TRAFFIC_10_2014', '600', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(515, NULL, NULL, 'DASHGOALS_CONVERSION_10_2014', '2', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(516, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_10_2014', '80', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(517, NULL, NULL, 'DASHGOALS_TRAFFIC_11_2014', '600', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(518, NULL, NULL, 'DASHGOALS_CONVERSION_11_2014', '2', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(519, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_11_2014', '80', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(520, NULL, NULL, 'DASHGOALS_TRAFFIC_12_2014', '600', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(521, NULL, NULL, 'DASHGOALS_CONVERSION_12_2014', '2', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(522, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_12_2014', '80', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(523, NULL, NULL, 'PS_ALLOW_HTML_IFRAME', '1', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(524, NULL, NULL, 'PS_MULTISHOP_FEATURE_ACTIVE', '0', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(525, NULL, NULL, 'PS_CSS_THEME_CACHE', '0', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(526, NULL, NULL, 'PS_JS_THEME_CACHE', '0', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(527, NULL, NULL, 'PS_HTML_THEME_COMPRESSION', '0', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(528, NULL, NULL, 'PS_JS_HTML_THEME_COMPRESSION', '0', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(529, NULL, NULL, 'PS_HTACCESS_CACHE_CONTROL', '0', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(530, NULL, NULL, 'PS_SHOW_CAT_MODULES_1', NULL, '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(531, NULL, NULL, 'PS_ALLOW_ACCENTED_CHARS_URL', '0', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(532, NULL, NULL, 'PS_HTACCESS_DISABLE_MULTIVIEWS', '0', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(533, NULL, NULL, 'PS_HTACCESS_DISABLE_MODSEC', '0', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(534, NULL, NULL, 'PS_ROUTE_product_rule', NULL, '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(535, NULL, NULL, 'PS_ROUTE_category_rule', NULL, '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(536, NULL, NULL, 'PS_ROUTE_layered_rule', NULL, '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(537, NULL, NULL, 'PS_ROUTE_supplier_rule', NULL, '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(538, NULL, NULL, 'PS_ROUTE_manufacturer_rule', NULL, '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(539, NULL, NULL, 'PS_ROUTE_cms_rule', NULL, '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(540, NULL, NULL, 'PS_ROUTE_cms_category_rule', NULL, '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(541, NULL, NULL, 'PS_ROUTE_module', 'module/{module}{/:controller}', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(542, NULL, NULL, 'PS_DISABLE_NON_NATIVE_MODULE', '0', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(543, NULL, NULL, 'PS_DISABLE_OVERRIDES', '1', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(544, NULL, NULL, 'PTS_CATS_ACTIVE_PLAY', '0', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(545, NULL, NULL, 'PTS_CATS_CATIDS', '5,6,7,9,10,11,13,14', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(546, NULL, NULL, 'PTS_CATS_CAT_COLUMNS', '4', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(547, NULL, NULL, 'PTS_CATS_CAT_ITEMSPAGE', '4', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(548, NULL, NULL, 'PTS_CATS_CAT_ITEMSTAB', '8', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(549, NULL, NULL, 'PTS_CATS_INTERVAL', '8000', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(550, NULL, NULL, 'PTS_CATS_PORDER', 'price', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(551, NULL, NULL, 'PTS_CATS_PRO_COLUMNS', '4', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(552, NULL, NULL, 'PTS_CATS_PRO_ITEMSPAGE', '4', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(553, NULL, NULL, 'PTS_CATS_PRO_ITEMSTAB', '8', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(554, NULL, NULL, 'PTS_ACTIVEDTHEME', 'pf_mixstore', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(555, NULL, NULL, 'PS_LOGO_MOBILE', 'prestashop-demo-1399603636.jpg', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(556, NULL, NULL, 'PS_LOGO_MAIL', 'prestashop-demo-1399603636.jpg', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(557, NULL, NULL, 'PS_LOGO_INVOICE', 'prestashop-demo-1399603636.jpg', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(558, NULL, NULL, 'PS_STORES_DISPLAY_SITEMAP', '0', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(559, NULL, NULL, 'PS_SHOP_DETAILS', NULL, '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(560, NULL, NULL, 'PS_SHOP_ADDR1', NULL, '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(561, NULL, NULL, 'PS_SHOP_ADDR2', NULL, '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(562, NULL, NULL, 'PS_SHOP_CODE', NULL, '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(563, NULL, NULL, 'PS_SHOP_CITY', NULL, '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(564, NULL, NULL, 'PS_SHOP_COUNTRY_ID', '21', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(565, NULL, NULL, 'PS_SHOP_COUNTRY', 'United States', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(566, NULL, NULL, 'PS_SHOP_PHONE', NULL, '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(567, NULL, NULL, 'PS_SHOP_FAX', NULL, '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(568, NULL, NULL, 'PS_CCCJS_VERSION', '22', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(569, NULL, NULL, 'PS_CCCCSS_VERSION', '22', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(570, NULL, NULL, 'PS_IMAGE_GENERATION_METHOD', '0', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(571, NULL, NULL, 'PS_SHIP_WHEN_AVAILABLE', '0', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(572, NULL, NULL, 'PS_GIFT_WRAPPING_TAX_RULES_GROUP', '0', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(573, NULL, NULL, 'PS_REFERRERS_CACHE_LIKE', ' \'2014-04-08 00:00:00\' AND \'2014-05-08 23:59:59\' ', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(574, NULL, NULL, 'PS_REFERRERS_CACHE_DATE', '2014-05-20 07:08:06', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(575, NULL, NULL, 'PS_PRODUCT_SHORT_DESC_LIMIT', '0', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(576, NULL, NULL, 'PS_QTY_DISCOUNT_ON_COMBINATION', '0', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(577, NULL, NULL, 'PS_FORCE_FRIENDLY_PRODUCT', '0', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(578, NULL, NULL, 'PS_DISPLAY_DISCOUNT_PRICE', '0', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(579, NULL, NULL, 'PS_FORCE_ASM_NEW_PRODUCT', '0', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(580, NULL, NULL, 'PTSCOUNTDOWN_CATIDS', NULL, '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(581, NULL, NULL, 'PS_SHOW_TYPE_MODULES_1', 'allModules', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(582, NULL, NULL, 'PS_SHOW_INSTALLED_MODULES_1', 'installedUninstalled', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(583, NULL, NULL, 'PS_SHOW_ENABLED_MODULES_1', 'enabledDisabled', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(584, NULL, NULL, 'PTSSLIDERLAYER_GROUP_DE', '1', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(585, NULL, NULL, 'PS_AUTOUPDATE_MODULE_IDTAB', '127', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(586, NULL, NULL, 'PS_LAST_VERSION_CHECK', '1409387528', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(587, NULL, NULL, 'PS_UPGRADE_CHANNEL', 'major', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(588, NULL, NULL, 'PS_AUTOUP_UPDATE_DEFAULT_THEME', '1', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(589, NULL, NULL, 'PS_AUTOUP_KEEP_MAILS', '1', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(590, NULL, NULL, 'PS_AUTOUP_CUSTOM_MOD_DESACT', '1', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(591, NULL, NULL, 'PS_AUTOUP_MANUAL_MODE', '0', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(592, NULL, NULL, 'PS_AUTOUP_PERFORMANCE', '1', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(593, NULL, NULL, 'PS_DISPLAY_ERRORS', '0', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(594, NULL, NULL, 'PS_FOLLOWUP_SECURE_KEY', 'BIWC9LHYHT497VLP', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(595, NULL, NULL, 'LEOBLG_CFG_GLOBAL', 'a:76:{s:17:"saveConfiguration";s:1:"1";s:8:"template";s:7:"default";s:17:"blog_link_title_1";s:4:"Blog";s:17:"blog_link_title_2";s:4:"Blog";s:17:"blog_link_title_3";s:4:"Blog";s:17:"blog_link_title_4";s:4:"Blog";s:17:"blog_link_title_5";s:4:"Blog";s:17:"blog_link_title_6";s:4:"Blog";s:17:"blog_link_title_7";s:4:"Blog";s:17:"blog_link_title_8";s:4:"Blog";s:17:"blog_link_title_9";s:4:"Blog";s:12:"link_rewrite";s:4:"blog";s:12:"meta_title_1";s:4:"Blog";s:12:"meta_title_2";s:4:"Blog";s:12:"meta_title_3";s:4:"Blog";s:12:"meta_title_4";s:4:"Blog";s:12:"meta_title_5";s:4:"Blog";s:12:"meta_title_6";s:4:"Blog";s:12:"meta_title_7";s:4:"Blog";s:12:"meta_title_8";s:4:"Blog";s:12:"meta_title_9";s:4:"Blog";s:18:"meta_description_1";s:0:"";s:18:"meta_description_2";s:0:"";s:18:"meta_description_3";s:0:"";s:18:"meta_description_4";s:0:"";s:18:"meta_description_5";s:0:"";s:18:"meta_description_6";s:0:"";s:18:"meta_description_7";s:0:"";s:18:"meta_description_8";s:0:"";s:18:"meta_description_9";s:0:"";s:15:"meta_keywords_1";s:0:"";s:15:"meta_keywords_2";s:0:"";s:15:"meta_keywords_3";s:0:"";s:15:"meta_keywords_4";s:0:"";s:15:"meta_keywords_5";s:0:"";s:15:"meta_keywords_6";s:0:"";s:15:"meta_keywords_7";s:0:"";s:15:"meta_keywords_8";s:0:"";s:15:"meta_keywords_9";s:0:"";s:10:"indexation";s:1:"0";s:14:"rss_limit_item";s:2:"20";s:14:"rss_title_item";s:8:"RSS FEED";s:25:"listing_show_categoryinfo";s:1:"1";s:26:"listing_show_subcategories";s:1:"1";s:22:"listing_leading_column";s:1:"1";s:27:"listing_leading_limit_items";s:1:"2";s:25:"listing_leading_img_width";s:3:"850";s:26:"listing_leading_img_height";s:3:"302";s:24:"listing_secondary_column";s:1:"1";s:29:"listing_secondary_limit_items";s:1:"3";s:27:"listing_secondary_img_width";s:3:"850";s:28:"listing_secondary_img_height";s:3:"302";s:18:"listing_show_title";s:1:"1";s:24:"listing_show_description";s:1:"1";s:21:"listing_show_readmore";s:1:"1";s:18:"listing_show_image";s:1:"1";s:19:"listing_show_author";s:1:"0";s:21:"listing_show_category";s:1:"0";s:20:"listing_show_created";s:1:"0";s:16:"listing_show_hit";s:1:"0";s:20:"listing_show_counter";s:1:"0";s:14:"item_img_width";s:3:"850";s:15:"item_img_height";s:3:"303";s:21:"item_show_description";s:1:"1";s:15:"item_show_image";s:1:"1";s:16:"item_show_author";s:1:"1";s:18:"item_show_category";s:1:"0";s:17:"item_show_created";s:1:"1";s:13:"item_show_hit";s:1:"1";s:17:"item_show_counter";s:1:"1";s:11:"social_code";s:0:"";s:19:"item_comment_engine";s:5:"local";s:19:"item_limit_comments";s:2:"10";s:19:"item_diquis_account";s:13:"demo4leotheme";s:19:"item_facebook_appid";s:12:"100858303516";s:19:"item_facebook_width";s:3:"600";}', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(596, NULL, NULL, 'PS_DEFAULT_WAREHOUSE_NEW_PRODUCT', NULL, '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(597, NULL, NULL, 'PTSSOCIAL_THEME', 'transparent', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(598, NULL, NULL, 'PTSSOCIAL_RESPONSIVE_MAXWIDTH', '979', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(599, NULL, NULL, 'PTSSOCIAL_RESPONSIVE_MINWIDTH', '0', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(600, NULL, NULL, 'PTSSOCIAL_DOMAIN', 'mysite.com', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(601, NULL, NULL, 'PTSSOCIAL_IDADDTHIS', NULL, '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(602, NULL, NULL, 'PTSSOCIAL_MOBILE_POSITION', 'top', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(603, NULL, NULL, 'PTSSOCIAL_MOBILE_THEME', 'transparent', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(604, NULL, NULL, 'PTSSOCIAL_MOBILE_STATUS', '1', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(605, NULL, NULL, 'PTSSOCIAL_SHARE_POSITION', 'right', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(606, NULL, NULL, 'PTSSOCIAL_SHARE_SERVICE', 'facebook,twitter,zingme,linkedin,favorites,google_plusone_share', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(607, NULL, NULL, 'PTSSOCIAL_SHARE_TITLE', 'Thanks for sharing!', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(608, NULL, NULL, 'PTSSOCIAL_SHARE_MGS', 'Follow us', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(609, NULL, NULL, 'PTSSOCIAL_SHARE_THEME', 'transparent', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(610, NULL, NULL, 'PTSSOCIAL_SHARE_DESKTOP', '1', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(611, NULL, NULL, 'PTSSOCIAL_SHARE_MOBILE', '1', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(612, NULL, NULL, 'PTSSOCIAL_FOLLOW_TITLE', 'Follow', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(613, NULL, NULL, 'PTSSOCIAL_FOLLOW_THEME', 'transparent', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(614, NULL, NULL, 'PTSSOCIAL_FOLLOW_THANK', 'Thanks for following!', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(615, NULL, NULL, 'PTSSOCIAL_FOLLOW_DESKTOP', '1', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(616, NULL, NULL, 'PTSSOCIAL_FOLLOW_MOBILE', '1', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(617, NULL, NULL, 'PTSSOCIAL_FOLLOW_FACEBOOK', NULL, '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(618, NULL, NULL, 'PTSSOCIAL_FOLLOW_TWITTER', NULL, '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(619, NULL, NULL, 'PTSSOCIAL_FOLLOW_GOOGLE', NULL, '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(620, NULL, NULL, 'PTSSOCIAL_FOLLOW_YOUTUBE', NULL, '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(621, NULL, NULL, 'PTSSOCIAL_FOLLOW_FLICKR', NULL, '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(622, NULL, NULL, 'PTSSOCIAL_FOLLOW_VIMEO', NULL, '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(623, NULL, NULL, 'PTSSOCIAL_FOLLOW_PINTEREST', NULL, '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(624, NULL, NULL, 'PTSSOCIAL_FOLLOW_INSTAGRAM', NULL, '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(625, NULL, NULL, 'PTSSOCIAL_FOLLOW_LINKEDIN', NULL, '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(626, NULL, NULL, 'PS_JS_DEFER', '0', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(627, NULL, NULL, 'PTSPROCAR_CATIDS', '1,2,3', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(628, NULL, NULL, 'PTSPROCAR_PORDER', 'date_add DESC', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(629, NULL, NULL, 'PTSPROCAR_MIN_WIDTH', '205', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(630, NULL, NULL, 'PTSPROCAR_MAX_WIDTH', '215', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(631, NULL, NULL, 'PTSPROCAR_LIMIT_ITEM', '15', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(632, NULL, NULL, 'PTSPROCAR_INTERVAL', '0', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(633, NULL, NULL, 'PS_HIDE_OPTIMIZATION_TIPS', '0', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(634, NULL, NULL, 'PS_NEED_REBUILD_INDEX', '1', '2015-05-08 14:03:30', '2015-05-08 14:03:30'),
	(635, NULL, NULL, 'PS_GRID_PRODUCT', '0', '2015-05-08 13:49:36', '2015-05-08 13:49:36'),
	(636, NULL, NULL, 'PS_SHOW_TYPE_MODULES_2', 'allModules', '2015-05-08 13:56:57', '2015-05-08 13:56:57'),
	(637, NULL, NULL, 'PS_SHOW_INSTALLED_MODULES_2', 'installedUninstalled', '2015-05-08 13:56:58', '2015-05-11 09:13:23'),
	(638, NULL, NULL, 'PS_SHOW_ENABLED_MODULES_2', 'enabledDisabled', '2015-05-08 13:56:58', '2015-05-11 09:13:23'),
	(639, NULL, NULL, 'PS_SHOW_CAT_MODULES_2', NULL, '2015-05-08 14:03:35', '2015-05-12 13:16:43'),
	(640, NULL, NULL, 'PS_BLOCKLINK_TITLE', NULL, '2015-05-11 09:14:29', '2015-05-11 09:14:29');
/*!40000 ALTER TABLE `ps_configuration` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_configuration_kpi
DROP TABLE IF EXISTS `ps_configuration_kpi`;
CREATE TABLE IF NOT EXISTS `ps_configuration_kpi` (
  `id_configuration_kpi` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_group` int(11) unsigned DEFAULT NULL,
  `id_shop` int(11) unsigned DEFAULT NULL,
  `name` varchar(32) NOT NULL,
  `value` text,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_configuration_kpi`),
  KEY `name` (`name`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_group` (`id_shop_group`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_configuration_kpi: ~35 rows (approximately)
/*!40000 ALTER TABLE `ps_configuration_kpi` DISABLE KEYS */;
INSERT INTO `ps_configuration_kpi` (`id_configuration_kpi`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES
	(1, NULL, NULL, 'UPDATE_MODULES', '9', '2015-05-08 13:48:15', '2015-05-12 17:07:35'),
	(2, NULL, NULL, 'UPDATE_MODULES', '22', '2015-05-08 13:48:15', '2015-05-08 13:48:15'),
	(3, NULL, NULL, 'DISABLED_MODULES', '4', '2015-05-08 13:48:17', '2015-05-12 17:07:33'),
	(4, NULL, NULL, 'DISABLED_MODULES_EXPIRE', '1431464973', '2015-05-08 13:48:17', '2015-05-12 17:07:33'),
	(5, NULL, NULL, 'INSTALLED_MODULES', '83', '2015-05-08 13:48:18', '2015-05-12 17:07:33'),
	(6, NULL, NULL, 'INSTALLED_MODULES_EXPIRE', '1431464973', '2015-05-08 13:48:18', '2015-05-12 17:07:33'),
	(7, NULL, NULL, 'UPDATE_MODULES_EXPIRE', '1431464975', '2015-05-08 13:48:19', '2015-05-12 17:07:35'),
	(8, NULL, NULL, 'DISABLED_PRODUCTS', '0%', '2015-05-08 15:44:03', '2015-05-08 15:44:03'),
	(9, NULL, NULL, 'PRODUCT_AVG_GROSS_MARGIN', '70%', '2015-05-08 15:44:03', '2015-05-08 15:44:03'),
	(10, NULL, NULL, '8020_SALES_CATALOG', '0% de su Catálogo', '2015-05-08 15:44:03', '2015-05-08 15:44:03'),
	(11, NULL, NULL, 'PRODUCT_AVG_GROSS_MARGIN_EXPIRE', '1431457512', '2015-05-08 15:44:03', '2015-05-12 09:05:12'),
	(12, NULL, NULL, '8020_SALES_CATALOG_EXPIRE', '1431479112', '2015-05-08 15:44:03', '2015-05-12 09:05:12'),
	(13, NULL, NULL, 'DISABLED_PRODUCTS_EXPIRE', '1431464179', '2015-05-08 15:44:03', '2015-05-12 14:56:19'),
	(14, NULL, NULL, 'TOP_CATEGORY', NULL, '2015-05-11 11:50:48', '2015-05-11 11:50:48'),
	(15, NULL, NULL, 'DISABLED_CATEGORIES', '2', '2015-05-11 11:50:48', '2015-05-12 11:26:50'),
	(16, NULL, NULL, 'EMPTY_CATEGORIES', '0', '2015-05-11 11:50:48', '2015-05-11 11:50:48'),
	(17, NULL, NULL, 'DISABLED_CATEGORIES_EXPIRE', '1431451610', '2015-05-11 11:50:48', '2015-05-12 11:26:50'),
	(18, NULL, NULL, 'TOP_CATEGORY_EXPIRE', NULL, '2015-05-11 11:50:48', '2015-05-11 11:50:48'),
	(19, NULL, NULL, 'EMPTY_CATEGORIES_EXPIRE', '1431451610', '2015-05-11 11:50:48', '2015-05-12 11:26:50'),
	(20, NULL, NULL, 'PRODUCTS_PER_CATEGORY', '0', '2015-05-11 11:50:48', '2015-05-12 11:26:50'),
	(21, NULL, NULL, 'PRODUCTS_PER_CATEGORY_EXPIRE', '1431448010', '2015-05-11 11:50:49', '2015-05-12 11:26:50'),
	(22, NULL, NULL, 'CONVERSION_RATE', '0%', '2015-05-11 11:54:22', '2015-05-11 11:54:22'),
	(23, NULL, NULL, 'CONVERSION_RATE_EXPIRE', '1431489600', '2015-05-11 11:54:22', '2015-05-12 11:26:48'),
	(24, NULL, NULL, 'NETPROFIT_VISIT', '0,00 $', '2015-05-11 11:54:22', '2015-05-11 11:54:22'),
	(25, NULL, NULL, 'AVG_ORDER_VALUE', '0,00 $', '2015-05-11 11:54:22', '2015-05-11 11:54:22'),
	(26, NULL, NULL, 'NETPROFIT_VISIT_EXPIRE', '1431489600', '2015-05-11 11:54:23', '2015-05-12 11:26:48'),
	(27, NULL, NULL, 'AVG_ORDER_VALUE_EXPIRE', '1431489600', '2015-05-11 11:54:23', '2015-05-12 11:26:48'),
	(28, NULL, NULL, 'ABANDONED_CARTS', '0', '2015-05-11 11:54:24', '2015-05-11 11:54:24'),
	(29, NULL, NULL, 'ABANDONED_CARTS_EXPIRE', '1431448007', '2015-05-11 11:54:24', '2015-05-12 11:26:47'),
	(30, NULL, NULL, 'MAIN_COUNTRY', NULL, '2015-05-12 10:35:48', '2015-05-12 10:35:48'),
	(31, NULL, NULL, 'ENABLED_LANGUAGES', '1', '2015-05-12 10:35:48', '2015-05-12 10:35:48'),
	(32, NULL, NULL, 'FRONTOFFICE_TRANSLATIONS', '0%', '2015-05-12 10:35:48', '2015-05-12 10:35:48'),
	(33, NULL, NULL, 'ENABLED_LANGUAGES_EXPIRE', '1431456179', '2015-05-12 10:35:48', '2015-05-12 14:41:59'),
	(34, NULL, NULL, 'FRONTOFFICE_TRANSLATIONS_EXPIRE', '1431456239', '2015-05-12 10:35:48', '2015-05-12 14:41:59'),
	(35, NULL, NULL, 'MAIN_COUNTRY_EXPIRE', NULL, '2015-05-12 10:35:48', '2015-05-12 10:35:48');
/*!40000 ALTER TABLE `ps_configuration_kpi` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_configuration_kpi_lang
DROP TABLE IF EXISTS `ps_configuration_kpi_lang`;
CREATE TABLE IF NOT EXISTS `ps_configuration_kpi_lang` (
  `id_configuration_kpi` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `value` text,
  `date_upd` datetime DEFAULT NULL,
  PRIMARY KEY (`id_configuration_kpi`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_configuration_kpi_lang: ~4 rows (approximately)
/*!40000 ALTER TABLE `ps_configuration_kpi_lang` DISABLE KEYS */;
INSERT INTO `ps_configuration_kpi_lang` (`id_configuration_kpi`, `id_lang`, `value`, `date_upd`) VALUES
	(14, 1, 'Vestidos de verano', '2015-05-11 11:50:48'),
	(18, 1, '1431445848', '2015-05-11 11:50:49'),
	(30, 1, 'Ningún pedido', '2015-05-12 10:35:48'),
	(35, 1, '1431527748', '2015-05-12 10:35:48');
/*!40000 ALTER TABLE `ps_configuration_kpi_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_configuration_lang
DROP TABLE IF EXISTS `ps_configuration_lang`;
CREATE TABLE IF NOT EXISTS `ps_configuration_lang` (
  `id_configuration` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `value` text,
  `date_upd` datetime DEFAULT NULL,
  PRIMARY KEY (`id_configuration`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_configuration_lang: ~13 rows (approximately)
/*!40000 ALTER TABLE `ps_configuration_lang` DISABLE KEYS */;
INSERT INTO `ps_configuration_lang` (`id_configuration`, `id_lang`, `value`, `date_upd`) VALUES
	(41, 1, 'FACT.', NULL),
	(42, 1, 'ALB. ENTR.', NULL),
	(49, 1, 'un|una|unas|unos|uno|sobre|todo|también|tras|otro|algún|alguno|alguna|algunos|algunas|ser|es|soy|eres|somos|sois|estoy|esta|estamos|estais|estan|como|en|para|atras|porque|por qué|estado|estaba|ante|antes|siendo|ambos|pero|por|poder|puede|puedo|podemos|podeis|pueden|fui|fue|fuimos|fueron|hacer|hago|hace|hacemos|haceis|hacen|cada|fin|incluso|primero|desde|conseguir|consigo|consigue|consigues|conseguimos|consiguen|ir|voy|va|vamos|vais|van|vaya|gueno|ha|tener|tengo|tiene|tenemos|teneis|tienen|el|la|lo|las|los|su|aqui|mio|tuyo|ellos|ellas|nos|nosotros|vosotros|vosotras|si|dentro|solo|solamente|saber|sabes|sabe|sabemos|sabeis|saben|ultimo|largo|bastante|haces|muchos|aquellos|aquellas|sus|entonces|tiempo|verdad|verdadero|verdadera|cierto|ciertos|cierta|ciertas|intentar|intento|intenta|intentas|intentamos|intentais|intentan|dos|bajo|arriba|encima|usar|uso|usas|usa|usamos|usais|usan|emplear|empleo|empleas|emplean|ampleamos|empleais|valor|muy|era|eras|eramos|eran|modo|bien|cual|cuando|donde|mientras|quien|con|entre|sin|trabajo|trabajar|trabajas|trabaja|trabajamos|trabajais|trabajan|podria|podrias|podriamos|podrian|podriais|yo|aquel', NULL),
	(71, 1, '0', NULL),
	(77, 1, 'Estimado cliente:\n\nSaludos,\nServicio de atención al cliente', NULL),
	(280, 1, 'sale70.png', '2015-05-08 14:00:39'),
	(281, 1, '', '2015-05-08 14:00:39'),
	(282, 1, '', '2015-05-08 14:00:39'),
	(413, 1, '', '2015-05-08 14:03:24'),
	(425, 1, '<p>Your Store Copyright Here</p>', '2015-05-08 13:49:36'),
	(429, 1, 'Custom Tab', '2015-05-08 13:49:36'),
	(640, 1, 'Block link', '2015-05-11 09:14:30'),
	(640, 2, 'Bloc lien', '2015-05-11 09:14:30');
/*!40000 ALTER TABLE `ps_configuration_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_connections
DROP TABLE IF EXISTS `ps_connections`;
CREATE TABLE IF NOT EXISTS `ps_connections` (
  `id_connections` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_group` int(11) unsigned NOT NULL DEFAULT '1',
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_guest` int(10) unsigned NOT NULL,
  `id_page` int(10) unsigned NOT NULL,
  `ip_address` bigint(20) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `http_referer` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_connections`),
  KEY `id_guest` (`id_guest`),
  KEY `date_add` (`date_add`),
  KEY `id_page` (`id_page`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_connections: ~21 rows (approximately)
/*!40000 ALTER TABLE `ps_connections` DISABLE KEYS */;
INSERT INTO `ps_connections` (`id_connections`, `id_shop_group`, `id_shop`, `id_guest`, `id_page`, `ip_address`, `date_add`, `http_referer`) VALUES
	(1, 1, 1, 1, 1, 2130706433, '2015-05-08 14:00:22', 'http://www.prestashop.com'),
	(2, 1, 1, 2, 1, 2130706433, '2015-05-08 14:04:03', ''),
	(3, 1, 1, 3, 2, 2130706433, '2015-05-08 14:04:03', ''),
	(4, 1, 1, 4, 1, 2130706433, '2015-05-08 13:39:53', ''),
	(5, 1, 1, 4, 1, 2130706433, '2015-05-11 08:58:27', ''),
	(6, 1, 1, 4, 1, 2130706433, '2015-05-11 09:34:10', ''),
	(7, 1, 1, 4, 1, 2130706433, '2015-05-11 10:37:18', ''),
	(8, 1, 1, 5, 1, 2130706433, '2015-05-11 11:05:37', ''),
	(9, 1, 1, 6, 1, 2130706433, '2015-05-11 11:08:35', ''),
	(10, 1, 1, 5, 3, 2130706433, '2015-05-11 12:02:47', ''),
	(11, 1, 1, 5, 3, 2130706433, '2015-05-11 13:03:13', ''),
	(12, 1, 1, 4, 1, 2130706433, '2015-05-11 13:55:35', ''),
	(13, 1, 1, 5, 1, 2130706433, '2015-05-11 14:07:31', ''),
	(14, 1, 1, 7, 1, 2130706433, '2015-05-11 14:16:09', ''),
	(15, 1, 1, 7, 1, 2130706433, '2015-05-11 14:47:31', ''),
	(16, 1, 1, 7, 1, 2130706433, '2015-05-11 15:18:38', ''),
	(17, 1, 1, 7, 1, 2130706433, '2015-05-11 15:50:51', ''),
	(18, 1, 1, 7, 1, 2130706433, '2015-05-11 16:21:17', ''),
	(19, 1, 1, 7, 1, 2130706433, '2015-05-12 08:59:02', ''),
	(20, 1, 1, 7, 1, 2130706433, '2015-05-12 10:54:00', ''),
	(21, 1, 1, 7, 1, 2130706433, '2015-05-12 17:08:52', '');
/*!40000 ALTER TABLE `ps_connections` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_connections_page
DROP TABLE IF EXISTS `ps_connections_page`;
CREATE TABLE IF NOT EXISTS `ps_connections_page` (
  `id_connections` int(10) unsigned NOT NULL,
  `id_page` int(10) unsigned NOT NULL,
  `time_start` datetime NOT NULL,
  `time_end` datetime DEFAULT NULL,
  PRIMARY KEY (`id_connections`,`id_page`,`time_start`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_connections_page: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_connections_page` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_connections_page` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_connections_source
DROP TABLE IF EXISTS `ps_connections_source`;
CREATE TABLE IF NOT EXISTS `ps_connections_source` (
  `id_connections_source` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_connections` int(10) unsigned NOT NULL,
  `http_referer` varchar(255) DEFAULT NULL,
  `request_uri` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_connections_source`),
  KEY `connections` (`id_connections`),
  KEY `orderby` (`date_add`),
  KEY `http_referer` (`http_referer`),
  KEY `request_uri` (`request_uri`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_connections_source: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_connections_source` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_connections_source` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_contact
DROP TABLE IF EXISTS `ps_contact`;
CREATE TABLE IF NOT EXISTS `ps_contact` (
  `id_contact` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL,
  `customer_service` tinyint(1) NOT NULL DEFAULT '0',
  `position` tinyint(2) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_contact`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_contact: ~2 rows (approximately)
/*!40000 ALTER TABLE `ps_contact` DISABLE KEYS */;
INSERT INTO `ps_contact` (`id_contact`, `email`, `customer_service`, `position`) VALUES
	(1, 'andres@alce.cl', 1, 0),
	(2, 'andres@alce.cl', 1, 0);
/*!40000 ALTER TABLE `ps_contact` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_contact_lang
DROP TABLE IF EXISTS `ps_contact_lang`;
CREATE TABLE IF NOT EXISTS `ps_contact_lang` (
  `id_contact` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text,
  PRIMARY KEY (`id_contact`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_contact_lang: ~2 rows (approximately)
/*!40000 ALTER TABLE `ps_contact_lang` DISABLE KEYS */;
INSERT INTO `ps_contact_lang` (`id_contact`, `id_lang`, `name`, `description`) VALUES
	(1, 1, 'Webmaster', 'En caso de problema técnico en esta página web'),
	(2, 1, 'Servicio de atención al cliente', 'Para cualquier pregunta sobre un artículo, un pedido');
/*!40000 ALTER TABLE `ps_contact_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_contact_shop
DROP TABLE IF EXISTS `ps_contact_shop`;
CREATE TABLE IF NOT EXISTS `ps_contact_shop` (
  `id_contact` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_contact`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_contact_shop: ~2 rows (approximately)
/*!40000 ALTER TABLE `ps_contact_shop` DISABLE KEYS */;
INSERT INTO `ps_contact_shop` (`id_contact`, `id_shop`) VALUES
	(1, 1),
	(2, 1);
/*!40000 ALTER TABLE `ps_contact_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_country
DROP TABLE IF EXISTS `ps_country`;
CREATE TABLE IF NOT EXISTS `ps_country` (
  `id_country` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_zone` int(10) unsigned NOT NULL,
  `id_currency` int(10) unsigned NOT NULL DEFAULT '0',
  `iso_code` varchar(3) NOT NULL,
  `call_prefix` int(10) NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `contains_states` tinyint(1) NOT NULL DEFAULT '0',
  `need_identification_number` tinyint(1) NOT NULL DEFAULT '0',
  `need_zip_code` tinyint(1) NOT NULL DEFAULT '1',
  `zip_code_format` varchar(12) NOT NULL DEFAULT '',
  `display_tax_label` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_country`),
  KEY `country_iso_code` (`iso_code`),
  KEY `country_` (`id_zone`)
) ENGINE=InnoDB AUTO_INCREMENT=245 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_country: ~244 rows (approximately)
/*!40000 ALTER TABLE `ps_country` DISABLE KEYS */;
INSERT INTO `ps_country` (`id_country`, `id_zone`, `id_currency`, `iso_code`, `call_prefix`, `active`, `contains_states`, `need_identification_number`, `need_zip_code`, `zip_code_format`, `display_tax_label`) VALUES
	(1, 1, 0, 'DE', 49, 0, 0, 0, 1, 'NNNNN', 1),
	(2, 1, 0, 'AT', 43, 0, 0, 0, 1, 'NNNN', 1),
	(3, 1, 0, 'BE', 32, 0, 0, 0, 1, 'NNNN', 1),
	(4, 2, 0, 'CA', 1, 0, 1, 0, 1, 'LNL NLN', 0),
	(5, 3, 0, 'CN', 86, 0, 0, 0, 1, 'NNNNNN', 1),
	(6, 1, 0, 'ES', 34, 0, 0, 1, 1, 'NNNNN', 1),
	(7, 1, 0, 'FI', 358, 0, 0, 0, 1, 'NNNNN', 1),
	(8, 1, 0, 'FR', 33, 0, 0, 0, 1, 'NNNNN', 1),
	(9, 1, 0, 'GR', 30, 0, 0, 0, 1, 'NNNNN', 1),
	(10, 1, 0, 'IT', 39, 0, 1, 0, 1, 'NNNNN', 1),
	(11, 3, 0, 'JP', 81, 0, 1, 0, 1, 'NNN-NNNN', 1),
	(12, 1, 0, 'LU', 352, 0, 0, 0, 1, 'NNNN', 1),
	(13, 1, 0, 'NL', 31, 0, 0, 0, 1, 'NNNN LL', 1),
	(14, 1, 0, 'PL', 48, 0, 0, 0, 1, 'NN-NNN', 1),
	(15, 1, 0, 'PT', 351, 0, 0, 0, 1, 'NNNN-NNN', 1),
	(16, 1, 0, 'CZ', 420, 0, 0, 0, 1, 'NNN NN', 1),
	(17, 1, 0, 'GB', 44, 0, 0, 0, 1, '', 1),
	(18, 1, 0, 'SE', 46, 0, 0, 0, 1, 'NNN NN', 1),
	(19, 7, 0, 'CH', 41, 0, 0, 0, 1, 'NNNN', 1),
	(20, 1, 0, 'DK', 45, 0, 0, 0, 1, 'NNNN', 1),
	(21, 2, 0, 'US', 1, 0, 1, 0, 1, 'NNNNN', 0),
	(22, 3, 0, 'HK', 852, 0, 0, 0, 0, '', 1),
	(23, 7, 0, 'NO', 47, 0, 0, 0, 1, 'NNNN', 1),
	(24, 5, 0, 'AU', 61, 0, 0, 0, 1, 'NNNN', 1),
	(25, 3, 0, 'SG', 65, 0, 0, 0, 1, 'NNNNNN', 1),
	(26, 1, 0, 'IE', 353, 0, 0, 0, 0, '', 1),
	(27, 5, 0, 'NZ', 64, 0, 0, 0, 1, 'NNNN', 1),
	(28, 3, 0, 'KR', 82, 0, 0, 0, 1, 'NNN-NNN', 1),
	(29, 3, 0, 'IL', 972, 0, 0, 0, 1, 'NNNNNNN', 1),
	(30, 4, 0, 'ZA', 27, 0, 0, 0, 1, 'NNNN', 1),
	(31, 4, 0, 'NG', 234, 0, 0, 0, 1, '', 1),
	(32, 4, 0, 'CI', 225, 0, 0, 0, 1, '', 1),
	(33, 4, 0, 'TG', 228, 0, 0, 0, 1, '', 1),
	(34, 6, 0, 'BO', 591, 0, 0, 0, 1, '', 1),
	(35, 4, 0, 'MU', 230, 0, 0, 0, 1, '', 1),
	(36, 1, 0, 'RO', 40, 0, 0, 0, 1, 'NNNNNN', 1),
	(37, 1, 0, 'SK', 421, 0, 0, 0, 1, 'NNN NN', 1),
	(38, 4, 0, 'DZ', 213, 0, 0, 0, 1, 'NNNNN', 1),
	(39, 2, 0, 'AS', 0, 0, 0, 0, 1, '', 1),
	(40, 7, 0, 'AD', 376, 0, 0, 0, 1, 'CNNN', 1),
	(41, 4, 0, 'AO', 244, 0, 0, 0, 0, '', 1),
	(42, 8, 0, 'AI', 0, 0, 0, 0, 1, '', 1),
	(43, 2, 0, 'AG', 0, 0, 0, 0, 1, '', 1),
	(44, 6, 0, 'AR', 54, 0, 1, 0, 1, 'LNNNN', 1),
	(45, 3, 0, 'AM', 374, 0, 0, 0, 1, 'NNNN', 1),
	(46, 8, 0, 'AW', 297, 0, 0, 0, 1, '', 1),
	(47, 3, 0, 'AZ', 994, 0, 0, 0, 1, 'CNNNN', 1),
	(48, 2, 0, 'BS', 0, 0, 0, 0, 1, '', 1),
	(49, 3, 0, 'BH', 973, 0, 0, 0, 1, '', 1),
	(50, 3, 0, 'BD', 880, 0, 0, 0, 1, 'NNNN', 1),
	(51, 2, 0, 'BB', 0, 0, 0, 0, 1, 'CNNNNN', 1),
	(52, 7, 0, 'BY', 0, 0, 0, 0, 1, 'NNNNNN', 1),
	(53, 8, 0, 'BZ', 501, 0, 0, 0, 0, '', 1),
	(54, 4, 0, 'BJ', 229, 0, 0, 0, 0, '', 1),
	(55, 2, 0, 'BM', 0, 0, 0, 0, 1, '', 1),
	(56, 3, 0, 'BT', 975, 0, 0, 0, 1, '', 1),
	(57, 4, 0, 'BW', 267, 0, 0, 0, 1, '', 1),
	(58, 6, 0, 'BR', 55, 0, 0, 0, 1, 'NNNNN-NNN', 1),
	(59, 3, 0, 'BN', 673, 0, 0, 0, 1, 'LLNNNN', 1),
	(60, 4, 0, 'BF', 226, 0, 0, 0, 1, '', 1),
	(61, 3, 0, 'MM', 95, 0, 0, 0, 1, '', 1),
	(62, 4, 0, 'BI', 257, 0, 0, 0, 1, '', 1),
	(63, 3, 0, 'KH', 855, 0, 0, 0, 1, 'NNNNN', 1),
	(64, 4, 0, 'CM', 237, 0, 0, 0, 1, '', 1),
	(65, 4, 0, 'CV', 238, 0, 0, 0, 1, 'NNNN', 1),
	(66, 4, 0, 'CF', 236, 0, 0, 0, 1, '', 1),
	(67, 4, 0, 'TD', 235, 0, 0, 0, 1, '', 1),
	(68, 6, 0, 'CL', 56, 1, 0, 0, 1, 'NNN-NNNN', 1),
	(69, 6, 0, 'CO', 57, 0, 0, 0, 1, 'NNNNNN', 1),
	(70, 4, 0, 'KM', 269, 0, 0, 0, 1, '', 1),
	(71, 4, 0, 'CD', 242, 0, 0, 0, 1, '', 1),
	(72, 4, 0, 'CG', 243, 0, 0, 0, 1, '', 1),
	(73, 8, 0, 'CR', 506, 0, 0, 0, 1, 'NNNNN', 1),
	(74, 7, 0, 'HR', 385, 0, 0, 0, 1, 'NNNNN', 1),
	(75, 8, 0, 'CU', 53, 0, 0, 0, 1, '', 1),
	(76, 1, 0, 'CY', 357, 0, 0, 0, 1, 'NNNN', 1),
	(77, 4, 0, 'DJ', 253, 0, 0, 0, 1, '', 1),
	(78, 8, 0, 'DM', 0, 0, 0, 0, 1, '', 1),
	(79, 8, 0, 'DO', 0, 0, 0, 0, 1, '', 1),
	(80, 3, 0, 'TL', 670, 0, 0, 0, 1, '', 1),
	(81, 6, 0, 'EC', 593, 0, 0, 0, 1, 'CNNNNNN', 1),
	(82, 4, 0, 'EG', 20, 0, 0, 0, 0, '', 1),
	(83, 8, 0, 'SV', 503, 0, 0, 0, 1, '', 1),
	(84, 4, 0, 'GQ', 240, 0, 0, 0, 1, '', 1),
	(85, 4, 0, 'ER', 291, 0, 0, 0, 1, '', 1),
	(86, 1, 0, 'EE', 372, 0, 0, 0, 1, 'NNNNN', 1),
	(87, 4, 0, 'ET', 251, 0, 0, 0, 1, '', 1),
	(88, 8, 0, 'FK', 0, 0, 0, 0, 1, 'LLLL NLL', 1),
	(89, 7, 0, 'FO', 298, 0, 0, 0, 1, '', 1),
	(90, 5, 0, 'FJ', 679, 0, 0, 0, 1, '', 1),
	(91, 4, 0, 'GA', 241, 0, 0, 0, 1, '', 1),
	(92, 4, 0, 'GM', 220, 0, 0, 0, 1, '', 1),
	(93, 3, 0, 'GE', 995, 0, 0, 0, 1, 'NNNN', 1),
	(94, 4, 0, 'GH', 233, 0, 0, 0, 1, '', 1),
	(95, 8, 0, 'GD', 0, 0, 0, 0, 1, '', 1),
	(96, 7, 0, 'GL', 299, 0, 0, 0, 1, '', 1),
	(97, 7, 0, 'GI', 350, 0, 0, 0, 1, '', 1),
	(98, 8, 0, 'GP', 590, 0, 0, 0, 1, '', 1),
	(99, 5, 0, 'GU', 0, 0, 0, 0, 1, '', 1),
	(100, 8, 0, 'GT', 502, 0, 0, 0, 1, '', 1),
	(101, 7, 0, 'GG', 0, 0, 0, 0, 1, 'LLN NLL', 1),
	(102, 4, 0, 'GN', 224, 0, 0, 0, 1, '', 1),
	(103, 4, 0, 'GW', 245, 0, 0, 0, 1, '', 1),
	(104, 6, 0, 'GY', 592, 0, 0, 0, 1, '', 1),
	(105, 8, 0, 'HT', 509, 0, 0, 0, 1, '', 1),
	(106, 5, 0, 'HM', 0, 0, 0, 0, 1, '', 1),
	(107, 7, 0, 'VA', 379, 0, 0, 0, 1, 'NNNNN', 1),
	(108, 8, 0, 'HN', 504, 0, 0, 0, 1, '', 1),
	(109, 7, 0, 'IS', 354, 0, 0, 0, 1, 'NNN', 1),
	(110, 3, 0, 'IN', 91, 0, 0, 0, 1, 'NNN NNN', 1),
	(111, 3, 0, 'ID', 62, 0, 1, 0, 1, 'NNNNN', 1),
	(112, 3, 0, 'IR', 98, 0, 0, 0, 1, 'NNNNN-NNNNN', 1),
	(113, 3, 0, 'IQ', 964, 0, 0, 0, 1, 'NNNNN', 1),
	(114, 7, 0, 'IM', 0, 0, 0, 0, 1, 'CN NLL', 1),
	(115, 8, 0, 'JM', 0, 0, 0, 0, 1, '', 1),
	(116, 7, 0, 'JE', 0, 0, 0, 0, 1, 'CN NLL', 1),
	(117, 3, 0, 'JO', 962, 0, 0, 0, 1, '', 1),
	(118, 3, 0, 'KZ', 7, 0, 0, 0, 1, 'NNNNNN', 1),
	(119, 4, 0, 'KE', 254, 0, 0, 0, 1, '', 1),
	(120, 5, 0, 'KI', 686, 0, 0, 0, 1, '', 1),
	(121, 3, 0, 'KP', 850, 0, 0, 0, 1, '', 1),
	(122, 3, 0, 'KW', 965, 0, 0, 0, 1, '', 1),
	(123, 3, 0, 'KG', 996, 0, 0, 0, 1, '', 1),
	(124, 3, 0, 'LA', 856, 0, 0, 0, 1, '', 1),
	(125, 1, 0, 'LV', 371, 0, 0, 0, 1, 'C-NNNN', 1),
	(126, 3, 0, 'LB', 961, 0, 0, 0, 1, '', 1),
	(127, 4, 0, 'LS', 266, 0, 0, 0, 1, '', 1),
	(128, 4, 0, 'LR', 231, 0, 0, 0, 1, '', 1),
	(129, 4, 0, 'LY', 218, 0, 0, 0, 1, '', 1),
	(130, 1, 0, 'LI', 423, 0, 0, 0, 1, 'NNNN', 1),
	(131, 1, 0, 'LT', 370, 0, 0, 0, 1, 'NNNNN', 1),
	(132, 3, 0, 'MO', 853, 0, 0, 0, 0, '', 1),
	(133, 7, 0, 'MK', 389, 0, 0, 0, 1, '', 1),
	(134, 4, 0, 'MG', 261, 0, 0, 0, 1, '', 1),
	(135, 4, 0, 'MW', 265, 0, 0, 0, 1, '', 1),
	(136, 3, 0, 'MY', 60, 0, 0, 0, 1, 'NNNNN', 1),
	(137, 3, 0, 'MV', 960, 0, 0, 0, 1, '', 1),
	(138, 4, 0, 'ML', 223, 0, 0, 0, 1, '', 1),
	(139, 1, 0, 'MT', 356, 0, 0, 0, 1, 'LLL NNNN', 1),
	(140, 5, 0, 'MH', 692, 0, 0, 0, 1, '', 1),
	(141, 8, 0, 'MQ', 596, 0, 0, 0, 1, '', 1),
	(142, 4, 0, 'MR', 222, 0, 0, 0, 1, '', 1),
	(143, 1, 0, 'HU', 36, 0, 0, 0, 1, 'NNNN', 1),
	(144, 4, 0, 'YT', 262, 0, 0, 0, 1, '', 1),
	(145, 2, 0, 'MX', 52, 0, 1, 1, 1, 'NNNNN', 1),
	(146, 5, 0, 'FM', 691, 0, 0, 0, 1, '', 1),
	(147, 7, 0, 'MD', 373, 0, 0, 0, 1, 'C-NNNN', 1),
	(148, 7, 0, 'MC', 377, 0, 0, 0, 1, '980NN', 1),
	(149, 3, 0, 'MN', 976, 0, 0, 0, 1, '', 1),
	(150, 7, 0, 'ME', 382, 0, 0, 0, 1, 'NNNNN', 1),
	(151, 8, 0, 'MS', 0, 0, 0, 0, 1, '', 1),
	(152, 4, 0, 'MA', 212, 0, 0, 0, 1, 'NNNNN', 1),
	(153, 4, 0, 'MZ', 258, 0, 0, 0, 1, '', 1),
	(154, 4, 0, 'NA', 264, 0, 0, 0, 1, '', 1),
	(155, 5, 0, 'NR', 674, 0, 0, 0, 1, '', 1),
	(156, 3, 0, 'NP', 977, 0, 0, 0, 1, '', 1),
	(157, 8, 0, 'AN', 599, 0, 0, 0, 1, '', 1),
	(158, 5, 0, 'NC', 687, 0, 0, 0, 1, '', 1),
	(159, 8, 0, 'NI', 505, 0, 0, 0, 1, 'NNNNNN', 1),
	(160, 4, 0, 'NE', 227, 0, 0, 0, 1, '', 1),
	(161, 5, 0, 'NU', 683, 0, 0, 0, 1, '', 1),
	(162, 5, 0, 'NF', 0, 0, 0, 0, 1, '', 1),
	(163, 5, 0, 'MP', 0, 0, 0, 0, 1, '', 1),
	(164, 3, 0, 'OM', 968, 0, 0, 0, 1, '', 1),
	(165, 3, 0, 'PK', 92, 0, 0, 0, 1, '', 1),
	(166, 5, 0, 'PW', 680, 0, 0, 0, 1, '', 1),
	(167, 3, 0, 'PS', 0, 0, 0, 0, 1, '', 1),
	(168, 8, 0, 'PA', 507, 0, 0, 0, 1, 'NNNNNN', 1),
	(169, 5, 0, 'PG', 675, 0, 0, 0, 1, '', 1),
	(170, 6, 0, 'PY', 595, 0, 0, 0, 1, '', 1),
	(171, 6, 0, 'PE', 51, 0, 0, 0, 1, '', 1),
	(172, 3, 0, 'PH', 63, 0, 0, 0, 1, 'NNNN', 1),
	(173, 5, 0, 'PN', 0, 0, 0, 0, 1, 'LLLL NLL', 1),
	(174, 8, 0, 'PR', 0, 0, 0, 0, 1, 'NNNNN', 1),
	(175, 3, 0, 'QA', 974, 0, 0, 0, 1, '', 1),
	(176, 4, 0, 'RE', 262, 0, 0, 0, 1, '', 1),
	(177, 7, 0, 'RU', 7, 0, 0, 0, 1, 'NNNNNN', 1),
	(178, 4, 0, 'RW', 250, 0, 0, 0, 1, '', 1),
	(179, 8, 0, 'BL', 0, 0, 0, 0, 1, '', 1),
	(180, 8, 0, 'KN', 0, 0, 0, 0, 1, '', 1),
	(181, 8, 0, 'LC', 0, 0, 0, 0, 1, '', 1),
	(182, 8, 0, 'MF', 0, 0, 0, 0, 1, '', 1),
	(183, 8, 0, 'PM', 508, 0, 0, 0, 1, '', 1),
	(184, 8, 0, 'VC', 0, 0, 0, 0, 1, '', 1),
	(185, 5, 0, 'WS', 685, 0, 0, 0, 1, '', 1),
	(186, 7, 0, 'SM', 378, 0, 0, 0, 1, 'NNNNN', 1),
	(187, 4, 0, 'ST', 239, 0, 0, 0, 1, '', 1),
	(188, 3, 0, 'SA', 966, 0, 0, 0, 1, '', 1),
	(189, 4, 0, 'SN', 221, 0, 0, 0, 1, '', 1),
	(190, 7, 0, 'RS', 381, 0, 0, 0, 1, 'NNNNN', 1),
	(191, 4, 0, 'SC', 248, 0, 0, 0, 1, '', 1),
	(192, 4, 0, 'SL', 232, 0, 0, 0, 1, '', 1),
	(193, 1, 0, 'SI', 386, 0, 0, 0, 1, 'C-NNNN', 1),
	(194, 5, 0, 'SB', 677, 0, 0, 0, 1, '', 1),
	(195, 4, 0, 'SO', 252, 0, 0, 0, 1, '', 1),
	(196, 8, 0, 'GS', 0, 0, 0, 0, 1, 'LLLL NLL', 1),
	(197, 3, 0, 'LK', 94, 0, 0, 0, 1, 'NNNNN', 1),
	(198, 4, 0, 'SD', 249, 0, 0, 0, 1, '', 1),
	(199, 8, 0, 'SR', 597, 0, 0, 0, 1, '', 1),
	(200, 7, 0, 'SJ', 0, 0, 0, 0, 1, '', 1),
	(201, 4, 0, 'SZ', 268, 0, 0, 0, 1, '', 1),
	(202, 3, 0, 'SY', 963, 0, 0, 0, 1, '', 1),
	(203, 3, 0, 'TW', 886, 0, 0, 0, 1, 'NNNNN', 1),
	(204, 3, 0, 'TJ', 992, 0, 0, 0, 1, '', 1),
	(205, 4, 0, 'TZ', 255, 0, 0, 0, 1, '', 1),
	(206, 3, 0, 'TH', 66, 0, 0, 0, 1, 'NNNNN', 1),
	(207, 5, 0, 'TK', 690, 0, 0, 0, 1, '', 1),
	(208, 5, 0, 'TO', 676, 0, 0, 0, 1, '', 1),
	(209, 6, 0, 'TT', 0, 0, 0, 0, 1, '', 1),
	(210, 4, 0, 'TN', 216, 0, 0, 0, 1, '', 1),
	(211, 7, 0, 'TR', 90, 0, 0, 0, 1, 'NNNNN', 1),
	(212, 3, 0, 'TM', 993, 0, 0, 0, 1, '', 1),
	(213, 8, 0, 'TC', 0, 0, 0, 0, 1, 'LLLL NLL', 1),
	(214, 5, 0, 'TV', 688, 0, 0, 0, 1, '', 1),
	(215, 4, 0, 'UG', 256, 0, 0, 0, 1, '', 1),
	(216, 1, 0, 'UA', 380, 0, 0, 0, 1, 'NNNNN', 1),
	(217, 3, 0, 'AE', 971, 0, 0, 0, 1, '', 1),
	(218, 6, 0, 'UY', 598, 0, 0, 0, 1, '', 1),
	(219, 3, 0, 'UZ', 998, 0, 0, 0, 1, '', 1),
	(220, 5, 0, 'VU', 678, 0, 0, 0, 1, '', 1),
	(221, 6, 0, 'VE', 58, 0, 0, 0, 1, '', 1),
	(222, 3, 0, 'VN', 84, 0, 0, 0, 1, 'NNNNNN', 1),
	(223, 2, 0, 'VG', 0, 0, 0, 0, 1, 'CNNNN', 1),
	(224, 2, 0, 'VI', 0, 0, 0, 0, 1, '', 1),
	(225, 5, 0, 'WF', 681, 0, 0, 0, 1, '', 1),
	(226, 4, 0, 'EH', 0, 0, 0, 0, 1, '', 1),
	(227, 3, 0, 'YE', 967, 0, 0, 0, 1, '', 1),
	(228, 4, 0, 'ZM', 260, 0, 0, 0, 1, '', 1),
	(229, 4, 0, 'ZW', 263, 0, 0, 0, 1, '', 1),
	(230, 7, 0, 'AL', 355, 0, 0, 0, 1, 'NNNN', 1),
	(231, 3, 0, 'AF', 93, 0, 0, 0, 0, '', 1),
	(232, 5, 0, 'AQ', 0, 0, 0, 0, 1, '', 1),
	(233, 1, 0, 'BA', 387, 0, 0, 0, 1, '', 1),
	(234, 5, 0, 'BV', 0, 0, 0, 0, 1, '', 1),
	(235, 5, 0, 'IO', 0, 0, 0, 0, 1, 'LLLL NLL', 1),
	(236, 1, 0, 'BG', 359, 0, 0, 0, 1, 'NNNN', 1),
	(237, 8, 0, 'KY', 0, 0, 0, 0, 1, '', 1),
	(238, 3, 0, 'CX', 0, 0, 0, 0, 1, '', 1),
	(239, 3, 0, 'CC', 0, 0, 0, 0, 1, '', 1),
	(240, 5, 0, 'CK', 682, 0, 0, 0, 1, '', 1),
	(241, 6, 0, 'GF', 594, 0, 0, 0, 1, '', 1),
	(242, 5, 0, 'PF', 689, 0, 0, 0, 1, '', 1),
	(243, 5, 0, 'TF', 0, 0, 0, 0, 1, '', 1),
	(244, 7, 0, 'AX', 0, 0, 0, 0, 1, 'NNNNN', 1);
/*!40000 ALTER TABLE `ps_country` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_country_lang
DROP TABLE IF EXISTS `ps_country_lang`;
CREATE TABLE IF NOT EXISTS `ps_country_lang` (
  `id_country` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id_country`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_country_lang: ~244 rows (approximately)
/*!40000 ALTER TABLE `ps_country_lang` DISABLE KEYS */;
INSERT INTO `ps_country_lang` (`id_country`, `id_lang`, `name`) VALUES
	(1, 1, 'Alemania'),
	(2, 1, 'Austria'),
	(3, 1, 'Bélgica'),
	(4, 1, 'Canadá'),
	(5, 1, 'China'),
	(6, 1, 'España'),
	(7, 1, 'Finlandia'),
	(8, 1, 'Francia'),
	(9, 1, 'Grecia'),
	(10, 1, 'Italia'),
	(11, 1, 'Japón'),
	(12, 1, 'Luxemburgo'),
	(13, 1, 'Países Bajos'),
	(14, 1, 'Polonia'),
	(15, 1, 'Portugal'),
	(16, 1, 'República Checa'),
	(17, 1, 'Reino Unido'),
	(18, 1, 'Suecia'),
	(19, 1, 'Suiza'),
	(20, 1, 'Dinamarca'),
	(21, 1, 'EE.UU.'),
	(22, 1, 'Hong Kong'),
	(23, 1, 'Noruega'),
	(24, 1, 'Australia'),
	(25, 1, 'Singapur'),
	(26, 1, 'Irlanda'),
	(27, 1, 'Nueva Zelanda'),
	(28, 1, 'Corea del Sur'),
	(29, 1, 'Israel'),
	(30, 1, 'Sudáfrica'),
	(31, 1, 'Nigeria'),
	(32, 1, 'Costa de Marfil'),
	(33, 1, 'Togo'),
	(34, 1, 'Bolivia'),
	(35, 1, 'Mauricio'),
	(36, 1, 'Rumania'),
	(37, 1, 'Eslovaquia'),
	(38, 1, 'Argelia'),
	(39, 1, 'Samoa Americana'),
	(40, 1, 'Andorra'),
	(41, 1, 'Angola'),
	(42, 1, 'Anguila'),
	(43, 1, 'Antigua y Barbuda'),
	(44, 1, 'Argentina'),
	(45, 1, 'Armenia'),
	(46, 1, 'Aruba'),
	(47, 1, 'Azerbaiyán'),
	(48, 1, 'Bahamas'),
	(49, 1, 'Bahrein'),
	(50, 1, 'Bangladesh'),
	(51, 1, 'Barbados'),
	(52, 1, 'Belarús'),
	(53, 1, 'Belice'),
	(54, 1, 'Benin'),
	(55, 1, 'Bermudas'),
	(56, 1, 'Bhután'),
	(57, 1, 'Botswana'),
	(58, 1, 'Brasil'),
	(59, 1, 'Brunei'),
	(60, 1, 'Burkina Faso'),
	(61, 1, 'Birmania (Myanmar)'),
	(62, 1, 'Burundi'),
	(63, 1, 'Camboya'),
	(64, 1, 'Camerún'),
	(65, 1, 'Cabo Verde'),
	(66, 1, 'República Centroafricana'),
	(67, 1, 'Chad'),
	(68, 1, 'Chile'),
	(69, 1, 'Colombia'),
	(70, 1, 'Comoras'),
	(71, 1, 'Congo, Rep. Dem.'),
	(72, 1, 'Congo, República'),
	(73, 1, 'Costa Rica'),
	(74, 1, 'Croacia'),
	(75, 1, 'Cuba'),
	(76, 1, 'Chipre'),
	(77, 1, 'Djibouti'),
	(78, 1, 'Dominica'),
	(79, 1, 'República Dominicana'),
	(80, 1, 'Timor Oriental'),
	(81, 1, 'Ecuador'),
	(82, 1, 'Egipto'),
	(83, 1, 'El Salvador'),
	(84, 1, 'Guinea Ecuatorial'),
	(85, 1, 'Eritrea'),
	(86, 1, 'Estonia'),
	(87, 1, 'Etiopía'),
	(88, 1, 'Islas Malvinas'),
	(89, 1, 'Islas Feroe'),
	(90, 1, 'Fiji'),
	(91, 1, 'Gabón'),
	(92, 1, 'Gambia'),
	(93, 1, 'Georgia'),
	(94, 1, 'Ghana'),
	(95, 1, 'Granada'),
	(96, 1, 'Groenlandia'),
	(97, 1, 'Gibraltar'),
	(98, 1, 'Guadalupe'),
	(99, 1, 'Guam'),
	(100, 1, 'Guatemala'),
	(101, 1, 'Guernsey'),
	(102, 1, 'Guinea'),
	(103, 1, 'Guinea-Bissau'),
	(104, 1, 'Guyana'),
	(105, 1, 'Haití'),
	(106, 1, 'Islas Heard y McDonald Islas'),
	(107, 1, 'Ciudad del Vaticano'),
	(108, 1, 'Honduras'),
	(109, 1, 'Islandia'),
	(110, 1, 'India'),
	(111, 1, 'Indonesia'),
	(112, 1, 'Irán'),
	(113, 1, 'Iraq'),
	(114, 1, 'Man, Isla'),
	(115, 1, 'Jamaica'),
	(116, 1, 'Jersey'),
	(117, 1, 'Jordania'),
	(118, 1, 'Kazajstán'),
	(119, 1, 'Kenya'),
	(120, 1, 'Kiribati'),
	(121, 1, 'KOREA, DEM. República de'),
	(122, 1, 'Kuwait'),
	(123, 1, 'Kirguistán'),
	(124, 1, 'Laos'),
	(125, 1, 'Letonia'),
	(126, 1, 'Líbano'),
	(127, 1, 'Lesotho'),
	(128, 1, 'Liberia'),
	(129, 1, 'Libia'),
	(130, 1, 'Liechtenstein'),
	(131, 1, 'Lituania'),
	(132, 1, 'Macao'),
	(133, 1, 'Macedonia'),
	(134, 1, 'Madagascar'),
	(135, 1, 'Malawi'),
	(136, 1, 'Malasia'),
	(137, 1, 'Maldivas'),
	(138, 1, 'Malí'),
	(139, 1, 'Malta'),
	(140, 1, 'Marshall, Islas'),
	(141, 1, 'Martinica'),
	(142, 1, 'Mauritania'),
	(143, 1, 'Hungría'),
	(144, 1, 'Mayotte'),
	(145, 1, 'México'),
	(146, 1, 'Micronesia'),
	(147, 1, 'Moldavia'),
	(148, 1, 'Mónaco'),
	(149, 1, 'Mongolia'),
	(150, 1, 'Montenegro'),
	(151, 1, 'Montserrat'),
	(152, 1, 'Marruecos'),
	(153, 1, 'Mozambique'),
	(154, 1, 'Namibia'),
	(155, 1, 'Nauru'),
	(156, 1, 'Nepal'),
	(157, 1, 'Antillas Neerlandesas'),
	(158, 1, 'Nueva Caledonia'),
	(159, 1, 'Nicaragua'),
	(160, 1, 'Níger'),
	(161, 1, 'Niue'),
	(162, 1, 'Norfolk Island'),
	(163, 1, 'Islas Marianas del Norte'),
	(164, 1, 'Omán'),
	(165, 1, 'Pakistán'),
	(166, 1, 'Palau'),
	(167, 1, 'Territorios Palestinos'),
	(168, 1, 'Panamá'),
	(169, 1, 'Papua Nueva Guinea'),
	(170, 1, 'Paraguay'),
	(171, 1, 'Perú'),
	(172, 1, 'Filipinas'),
	(173, 1, 'Pitcairn'),
	(174, 1, 'Puerto Rico'),
	(175, 1, 'Qatar'),
	(176, 1, 'Reunión, Isla de la'),
	(177, 1, 'Rusia, Federación de'),
	(178, 1, 'Rwanda'),
	(179, 1, 'San Bartolomé'),
	(180, 1, 'Saint Kitts y Nevis'),
	(181, 1, 'Santa Lucía'),
	(182, 1, 'Saint Martin'),
	(183, 1, 'San Pedro y Miquelón'),
	(184, 1, 'San Vicente y las Granadinas'),
	(185, 1, 'Samoa'),
	(186, 1, 'San Marino'),
	(187, 1, 'Santo Tomé y Príncipe'),
	(188, 1, 'Arabia Saudita'),
	(189, 1, 'Senegal'),
	(190, 1, 'Serbia'),
	(191, 1, 'Seychelles'),
	(192, 1, 'Sierra Leona'),
	(193, 1, 'Eslovenia'),
	(194, 1, 'Salomón, Islas'),
	(195, 1, 'Somalia'),
	(196, 1, 'Georgia del Sur e Islas Sandwich del Sur'),
	(197, 1, 'Sri Lanka'),
	(198, 1, 'Sudán'),
	(199, 1, 'Suriname'),
	(200, 1, 'Svalbard y Jan Mayen'),
	(201, 1, 'Swazilandia'),
	(202, 1, 'Siria'),
	(203, 1, 'Taiwán'),
	(204, 1, 'Tayikistán'),
	(205, 1, 'Tanzania'),
	(206, 1, 'Tailandia'),
	(207, 1, 'Tokelau'),
	(208, 1, 'Tonga'),
	(209, 1, 'Trinidad y Tobago'),
	(210, 1, 'Túnez'),
	(211, 1, 'Turquía'),
	(212, 1, 'Turkmenistán'),
	(213, 1, 'Islas Turcas y Caicos'),
	(214, 1, 'Tuvalu'),
	(215, 1, 'Uganda'),
	(216, 1, 'Ucrania'),
	(217, 1, 'Emiratos ÿrabes Unidos'),
	(218, 1, 'Uruguay'),
	(219, 1, 'Uzbekistán'),
	(220, 1, 'Vanuatu'),
	(221, 1, 'Venezuela'),
	(222, 1, 'Vietnam'),
	(223, 1, 'Islas Vírgenes (Británicas)'),
	(224, 1, 'Islas Vírgenes (EE.UU.)'),
	(225, 1, 'Wallis y Futuna'),
	(226, 1, 'Sáhara Occidental'),
	(227, 1, 'Yemen'),
	(228, 1, 'Zambia'),
	(229, 1, 'Zimbabwe'),
	(230, 1, 'Albania'),
	(231, 1, 'Afganistán'),
	(232, 1, 'Antártida'),
	(233, 1, 'Bosnia y Herzegovina'),
	(234, 1, 'Isla Bouvet'),
	(235, 1, 'British Indian Ocean Territory'),
	(236, 1, 'Bulgaria'),
	(237, 1, 'Caimán, Islas'),
	(238, 1, 'Navidad, Isla de'),
	(239, 1, 'Cocos (Keeling), Islas'),
	(240, 1, 'Cook, Islas'),
	(241, 1, 'Francés Guayana'),
	(242, 1, 'Polinesia francés'),
	(243, 1, 'Territorios del sur francés'),
	(244, 1, 'Islas Åland');
/*!40000 ALTER TABLE `ps_country_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_country_shop
DROP TABLE IF EXISTS `ps_country_shop`;
CREATE TABLE IF NOT EXISTS `ps_country_shop` (
  `id_country` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_country`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_country_shop: ~244 rows (approximately)
/*!40000 ALTER TABLE `ps_country_shop` DISABLE KEYS */;
INSERT INTO `ps_country_shop` (`id_country`, `id_shop`) VALUES
	(1, 1),
	(2, 1),
	(3, 1),
	(4, 1),
	(5, 1),
	(6, 1),
	(7, 1),
	(8, 1),
	(9, 1),
	(10, 1),
	(11, 1),
	(12, 1),
	(13, 1),
	(14, 1),
	(15, 1),
	(16, 1),
	(17, 1),
	(18, 1),
	(19, 1),
	(20, 1),
	(21, 1),
	(22, 1),
	(23, 1),
	(24, 1),
	(25, 1),
	(26, 1),
	(27, 1),
	(28, 1),
	(29, 1),
	(30, 1),
	(31, 1),
	(32, 1),
	(33, 1),
	(34, 1),
	(35, 1),
	(36, 1),
	(37, 1),
	(38, 1),
	(39, 1),
	(40, 1),
	(41, 1),
	(42, 1),
	(43, 1),
	(44, 1),
	(45, 1),
	(46, 1),
	(47, 1),
	(48, 1),
	(49, 1),
	(50, 1),
	(51, 1),
	(52, 1),
	(53, 1),
	(54, 1),
	(55, 1),
	(56, 1),
	(57, 1),
	(58, 1),
	(59, 1),
	(60, 1),
	(61, 1),
	(62, 1),
	(63, 1),
	(64, 1),
	(65, 1),
	(66, 1),
	(67, 1),
	(68, 1),
	(69, 1),
	(70, 1),
	(71, 1),
	(72, 1),
	(73, 1),
	(74, 1),
	(75, 1),
	(76, 1),
	(77, 1),
	(78, 1),
	(79, 1),
	(80, 1),
	(81, 1),
	(82, 1),
	(83, 1),
	(84, 1),
	(85, 1),
	(86, 1),
	(87, 1),
	(88, 1),
	(89, 1),
	(90, 1),
	(91, 1),
	(92, 1),
	(93, 1),
	(94, 1),
	(95, 1),
	(96, 1),
	(97, 1),
	(98, 1),
	(99, 1),
	(100, 1),
	(101, 1),
	(102, 1),
	(103, 1),
	(104, 1),
	(105, 1),
	(106, 1),
	(107, 1),
	(108, 1),
	(109, 1),
	(110, 1),
	(111, 1),
	(112, 1),
	(113, 1),
	(114, 1),
	(115, 1),
	(116, 1),
	(117, 1),
	(118, 1),
	(119, 1),
	(120, 1),
	(121, 1),
	(122, 1),
	(123, 1),
	(124, 1),
	(125, 1),
	(126, 1),
	(127, 1),
	(128, 1),
	(129, 1),
	(130, 1),
	(131, 1),
	(132, 1),
	(133, 1),
	(134, 1),
	(135, 1),
	(136, 1),
	(137, 1),
	(138, 1),
	(139, 1),
	(140, 1),
	(141, 1),
	(142, 1),
	(143, 1),
	(144, 1),
	(145, 1),
	(146, 1),
	(147, 1),
	(148, 1),
	(149, 1),
	(150, 1),
	(151, 1),
	(152, 1),
	(153, 1),
	(154, 1),
	(155, 1),
	(156, 1),
	(157, 1),
	(158, 1),
	(159, 1),
	(160, 1),
	(161, 1),
	(162, 1),
	(163, 1),
	(164, 1),
	(165, 1),
	(166, 1),
	(167, 1),
	(168, 1),
	(169, 1),
	(170, 1),
	(171, 1),
	(172, 1),
	(173, 1),
	(174, 1),
	(175, 1),
	(176, 1),
	(177, 1),
	(178, 1),
	(179, 1),
	(180, 1),
	(181, 1),
	(182, 1),
	(183, 1),
	(184, 1),
	(185, 1),
	(186, 1),
	(187, 1),
	(188, 1),
	(189, 1),
	(190, 1),
	(191, 1),
	(192, 1),
	(193, 1),
	(194, 1),
	(195, 1),
	(196, 1),
	(197, 1),
	(198, 1),
	(199, 1),
	(200, 1),
	(201, 1),
	(202, 1),
	(203, 1),
	(204, 1),
	(205, 1),
	(206, 1),
	(207, 1),
	(208, 1),
	(209, 1),
	(210, 1),
	(211, 1),
	(212, 1),
	(213, 1),
	(214, 1),
	(215, 1),
	(216, 1),
	(217, 1),
	(218, 1),
	(219, 1),
	(220, 1),
	(221, 1),
	(222, 1),
	(223, 1),
	(224, 1),
	(225, 1),
	(226, 1),
	(227, 1),
	(228, 1),
	(229, 1),
	(230, 1),
	(231, 1),
	(232, 1),
	(233, 1),
	(234, 1),
	(235, 1),
	(236, 1),
	(237, 1),
	(238, 1),
	(239, 1),
	(240, 1),
	(241, 1),
	(242, 1),
	(243, 1),
	(244, 1);
/*!40000 ALTER TABLE `ps_country_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_cronjobs
DROP TABLE IF EXISTS `ps_cronjobs`;
CREATE TABLE IF NOT EXISTS `ps_cronjobs` (
  `id_cronjob` int(10) NOT NULL AUTO_INCREMENT,
  `id_module` int(10) DEFAULT NULL,
  `description` text,
  `task` text,
  `hour` int(11) DEFAULT '-1',
  `day` int(11) DEFAULT '-1',
  `month` int(11) DEFAULT '-1',
  `day_of_week` int(11) DEFAULT '-1',
  `updated_at` datetime DEFAULT NULL,
  `one_shot` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) DEFAULT '0',
  `id_shop` int(11) DEFAULT '0',
  `id_shop_group` int(11) DEFAULT '0',
  PRIMARY KEY (`id_cronjob`),
  KEY `id_module` (`id_module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_cronjobs: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_cronjobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_cronjobs` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_currency
DROP TABLE IF EXISTS `ps_currency`;
CREATE TABLE IF NOT EXISTS `ps_currency` (
  `id_currency` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `iso_code` varchar(3) NOT NULL DEFAULT '0',
  `iso_code_num` varchar(3) NOT NULL DEFAULT '0',
  `sign` varchar(8) NOT NULL,
  `blank` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `format` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `decimals` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `conversion_rate` decimal(13,6) NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_currency`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_currency: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_currency` DISABLE KEYS */;
INSERT INTO `ps_currency` (`id_currency`, `name`, `iso_code`, `iso_code_num`, `sign`, `blank`, `format`, `decimals`, `conversion_rate`, `deleted`, `active`) VALUES
	(1, 'Peso', 'CLP', '152', '$', 1, 2, 1, 1.000000, 0, 1);
/*!40000 ALTER TABLE `ps_currency` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_currency_shop
DROP TABLE IF EXISTS `ps_currency_shop`;
CREATE TABLE IF NOT EXISTS `ps_currency_shop` (
  `id_currency` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  `conversion_rate` decimal(13,6) NOT NULL,
  PRIMARY KEY (`id_currency`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_currency_shop: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_currency_shop` DISABLE KEYS */;
INSERT INTO `ps_currency_shop` (`id_currency`, `id_shop`, `conversion_rate`) VALUES
	(1, 1, 1.000000);
/*!40000 ALTER TABLE `ps_currency_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_customer
DROP TABLE IF EXISTS `ps_customer`;
CREATE TABLE IF NOT EXISTS `ps_customer` (
  `id_customer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_group` int(11) unsigned NOT NULL DEFAULT '1',
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_gender` int(10) unsigned NOT NULL,
  `id_default_group` int(10) unsigned NOT NULL DEFAULT '1',
  `id_lang` int(10) unsigned DEFAULT NULL,
  `id_risk` int(10) unsigned NOT NULL DEFAULT '1',
  `company` varchar(64) DEFAULT NULL,
  `siret` varchar(14) DEFAULT NULL,
  `ape` varchar(5) DEFAULT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(128) NOT NULL,
  `passwd` varchar(32) NOT NULL,
  `last_passwd_gen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `birthday` date DEFAULT NULL,
  `newsletter` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ip_registration_newsletter` varchar(15) DEFAULT NULL,
  `newsletter_date_add` datetime DEFAULT NULL,
  `optin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `website` varchar(128) DEFAULT NULL,
  `outstanding_allow_amount` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `show_public_prices` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `max_payment_days` int(10) unsigned NOT NULL DEFAULT '60',
  `secure_key` varchar(32) NOT NULL DEFAULT '-1',
  `note` text,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_guest` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_customer`),
  KEY `customer_email` (`email`),
  KEY `customer_login` (`email`,`passwd`),
  KEY `id_customer_passwd` (`id_customer`,`passwd`),
  KEY `id_gender` (`id_gender`),
  KEY `id_shop_group` (`id_shop_group`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_customer: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_customer` DISABLE KEYS */;
INSERT INTO `ps_customer` (`id_customer`, `id_shop_group`, `id_shop`, `id_gender`, `id_default_group`, `id_lang`, `id_risk`, `company`, `siret`, `ape`, `firstname`, `lastname`, `email`, `passwd`, `last_passwd_gen`, `birthday`, `newsletter`, `ip_registration_newsletter`, `newsletter_date_add`, `optin`, `website`, `outstanding_allow_amount`, `show_public_prices`, `max_payment_days`, `secure_key`, `note`, `active`, `is_guest`, `deleted`, `date_add`, `date_upd`) VALUES
	(1, 1, 1, 1, 3, 1, 0, '', '', '', 'John', 'DOE', 'pub@prestashop.com', 'c4dbdde4573b68d1f9ee455b78805863', '2015-05-08 08:00:20', '1970-01-15', 1, '', '2013-12-13 08:19:15', 1, '', 0.000000, 0, 0, 'ca5aaf42f6e4f993fc7587812d228563', '', 1, 0, 0, '2015-05-08 14:00:20', '2015-05-08 14:00:20');
/*!40000 ALTER TABLE `ps_customer` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_customer_group
DROP TABLE IF EXISTS `ps_customer_group`;
CREATE TABLE IF NOT EXISTS `ps_customer_group` (
  `id_customer` int(10) unsigned NOT NULL,
  `id_group` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_customer`,`id_group`),
  KEY `customer_login` (`id_group`),
  KEY `id_customer` (`id_customer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_customer_group: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_customer_group` DISABLE KEYS */;
INSERT INTO `ps_customer_group` (`id_customer`, `id_group`) VALUES
	(1, 3);
/*!40000 ALTER TABLE `ps_customer_group` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_customer_message
DROP TABLE IF EXISTS `ps_customer_message`;
CREATE TABLE IF NOT EXISTS `ps_customer_message` (
  `id_customer_message` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_customer_thread` int(11) DEFAULT NULL,
  `id_employee` int(10) unsigned DEFAULT NULL,
  `message` text NOT NULL,
  `file_name` varchar(18) DEFAULT NULL,
  `ip_address` varchar(16) DEFAULT NULL,
  `user_agent` varchar(128) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `private` tinyint(4) NOT NULL DEFAULT '0',
  `read` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_customer_message`),
  KEY `id_customer_thread` (`id_customer_thread`),
  KEY `id_employee` (`id_employee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_customer_message: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_customer_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_customer_message` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_customer_message_sync_imap
DROP TABLE IF EXISTS `ps_customer_message_sync_imap`;
CREATE TABLE IF NOT EXISTS `ps_customer_message_sync_imap` (
  `md5_header` varbinary(32) NOT NULL,
  KEY `md5_header_index` (`md5_header`(4))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_customer_message_sync_imap: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_customer_message_sync_imap` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_customer_message_sync_imap` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_customer_thread
DROP TABLE IF EXISTS `ps_customer_thread`;
CREATE TABLE IF NOT EXISTS `ps_customer_thread` (
  `id_customer_thread` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_lang` int(10) unsigned NOT NULL,
  `id_contact` int(10) unsigned NOT NULL,
  `id_customer` int(10) unsigned DEFAULT NULL,
  `id_order` int(10) unsigned DEFAULT NULL,
  `id_product` int(10) unsigned DEFAULT NULL,
  `status` enum('open','closed','pending1','pending2') NOT NULL DEFAULT 'open',
  `email` varchar(128) NOT NULL,
  `token` varchar(12) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_customer_thread`),
  KEY `id_shop` (`id_shop`),
  KEY `id_lang` (`id_lang`),
  KEY `id_contact` (`id_contact`),
  KEY `id_customer` (`id_customer`),
  KEY `id_order` (`id_order`),
  KEY `id_product` (`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_customer_thread: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_customer_thread` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_customer_thread` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_customization
DROP TABLE IF EXISTS `ps_customization`;
CREATE TABLE IF NOT EXISTS `ps_customization` (
  `id_customization` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_product_attribute` int(10) unsigned NOT NULL DEFAULT '0',
  `id_address_delivery` int(10) unsigned NOT NULL DEFAULT '0',
  `id_cart` int(10) unsigned NOT NULL,
  `id_product` int(10) NOT NULL,
  `quantity` int(10) NOT NULL,
  `quantity_refunded` int(11) NOT NULL DEFAULT '0',
  `quantity_returned` int(11) NOT NULL DEFAULT '0',
  `in_cart` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_customization`,`id_cart`,`id_product`,`id_address_delivery`),
  KEY `id_product_attribute` (`id_product_attribute`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_customization: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_customization` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_customization` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_customization_field
DROP TABLE IF EXISTS `ps_customization_field`;
CREATE TABLE IF NOT EXISTS `ps_customization_field` (
  `id_customization_field` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_product` int(10) unsigned NOT NULL,
  `type` tinyint(1) NOT NULL,
  `required` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_customization_field`),
  KEY `id_product` (`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_customization_field: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_customization_field` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_customization_field` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_customization_field_lang
DROP TABLE IF EXISTS `ps_customization_field_lang`;
CREATE TABLE IF NOT EXISTS `ps_customization_field_lang` (
  `id_customization_field` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL DEFAULT '1',
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id_customization_field`,`id_lang`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_customization_field_lang: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_customization_field_lang` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_customization_field_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_customized_data
DROP TABLE IF EXISTS `ps_customized_data`;
CREATE TABLE IF NOT EXISTS `ps_customized_data` (
  `id_customization` int(10) unsigned NOT NULL,
  `type` tinyint(1) NOT NULL,
  `index` int(3) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id_customization`,`type`,`index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_customized_data: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_customized_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_customized_data` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_date_range
DROP TABLE IF EXISTS `ps_date_range`;
CREATE TABLE IF NOT EXISTS `ps_date_range` (
  `id_date_range` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `time_start` datetime NOT NULL,
  `time_end` datetime NOT NULL,
  PRIMARY KEY (`id_date_range`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_date_range: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_date_range` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_date_range` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_delivery
DROP TABLE IF EXISTS `ps_delivery`;
CREATE TABLE IF NOT EXISTS `ps_delivery` (
  `id_delivery` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned DEFAULT NULL,
  `id_shop_group` int(10) unsigned DEFAULT NULL,
  `id_carrier` int(10) unsigned NOT NULL,
  `id_range_price` int(10) unsigned DEFAULT NULL,
  `id_range_weight` int(10) unsigned DEFAULT NULL,
  `id_zone` int(10) unsigned NOT NULL,
  `price` decimal(20,6) NOT NULL,
  PRIMARY KEY (`id_delivery`),
  KEY `id_zone` (`id_zone`),
  KEY `id_carrier` (`id_carrier`,`id_zone`),
  KEY `id_range_price` (`id_range_price`),
  KEY `id_range_weight` (`id_range_weight`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_delivery: ~8 rows (approximately)
/*!40000 ALTER TABLE `ps_delivery` DISABLE KEYS */;
INSERT INTO `ps_delivery` (`id_delivery`, `id_shop`, `id_shop_group`, `id_carrier`, `id_range_price`, `id_range_weight`, `id_zone`, `price`) VALUES
	(1, 1, 1, 2, 1, 0, 1, 0.000000),
	(2, 1, 1, 2, 1, 0, 2, 0.000000),
	(3, 1, 1, 2, 0, 1, 1, 0.000000),
	(4, 1, 1, 2, 0, 1, 2, 0.000000),
	(5, NULL, NULL, 2, 0, 1, 1, 5.000000),
	(6, NULL, NULL, 2, 0, 1, 2, 5.000000),
	(7, NULL, NULL, 2, 1, 0, 1, 5.000000),
	(8, NULL, NULL, 2, 1, 0, 2, 5.000000);
/*!40000 ALTER TABLE `ps_delivery` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_employee
DROP TABLE IF EXISTS `ps_employee`;
CREATE TABLE IF NOT EXISTS `ps_employee` (
  `id_employee` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_profile` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL DEFAULT '0',
  `lastname` varchar(32) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `email` varchar(128) NOT NULL,
  `passwd` varchar(32) NOT NULL,
  `last_passwd_gen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `stats_date_from` date DEFAULT NULL,
  `stats_date_to` date DEFAULT NULL,
  `stats_compare_from` date DEFAULT NULL,
  `stats_compare_to` date DEFAULT NULL,
  `stats_compare_option` int(1) unsigned NOT NULL DEFAULT '1',
  `preselect_date_range` varchar(32) DEFAULT NULL,
  `bo_color` varchar(32) DEFAULT NULL,
  `bo_theme` varchar(32) DEFAULT NULL,
  `bo_css` varchar(64) DEFAULT NULL,
  `default_tab` int(10) unsigned NOT NULL DEFAULT '0',
  `bo_width` int(10) unsigned NOT NULL DEFAULT '0',
  `bo_menu` tinyint(1) NOT NULL DEFAULT '1',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `optin` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `id_last_order` int(10) unsigned NOT NULL DEFAULT '0',
  `id_last_customer_message` int(10) unsigned NOT NULL DEFAULT '0',
  `id_last_customer` int(10) unsigned NOT NULL DEFAULT '0',
  `last_connection_date` date DEFAULT '0000-00-00',
  PRIMARY KEY (`id_employee`),
  KEY `employee_login` (`email`,`passwd`),
  KEY `id_employee_passwd` (`id_employee`,`passwd`),
  KEY `id_profile` (`id_profile`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_employee: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_employee` DISABLE KEYS */;
INSERT INTO `ps_employee` (`id_employee`, `id_profile`, `id_lang`, `lastname`, `firstname`, `email`, `passwd`, `last_passwd_gen`, `stats_date_from`, `stats_date_to`, `stats_compare_from`, `stats_compare_to`, `stats_compare_option`, `preselect_date_range`, `bo_color`, `bo_theme`, `bo_css`, `default_tab`, `bo_width`, `bo_menu`, `active`, `optin`, `id_last_order`, `id_last_customer_message`, `id_last_customer`, `last_connection_date`) VALUES
	(1, 1, 1, 'Lagos', 'Andrés', 'andres@alce.cl', '20426a22154473cbfd6b4deae8a81ee4', '2015-05-08 08:00:10', '2015-04-08', '2015-05-08', '0000-00-00', '0000-00-00', 1, NULL, NULL, 'default', 'admin-theme.css', 1, 0, 1, 1, 0, 0, 0, 0, '2015-05-08'),
	(2, 1, 1, 'Neupert', 'Nicolas', 'nicolas@alce.cl', '7f02699d52fdbe08d2398c3e30c6428e', '2015-05-08 07:51:49', '2015-04-08', '2015-05-08', '0000-00-00', '0000-00-00', 1, NULL, NULL, 'default', 'admin-theme.css', 1, 0, 1, 1, 1, 5, 0, 1, '2015-05-12');
/*!40000 ALTER TABLE `ps_employee` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_employee_shop
DROP TABLE IF EXISTS `ps_employee_shop`;
CREATE TABLE IF NOT EXISTS `ps_employee_shop` (
  `id_employee` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_employee`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_employee_shop: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_employee_shop` DISABLE KEYS */;
INSERT INTO `ps_employee_shop` (`id_employee`, `id_shop`) VALUES
	(1, 1),
	(2, 1);
/*!40000 ALTER TABLE `ps_employee_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_feature
DROP TABLE IF EXISTS `ps_feature`;
CREATE TABLE IF NOT EXISTS `ps_feature` (
  `id_feature` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_feature`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_feature: ~7 rows (approximately)
/*!40000 ALTER TABLE `ps_feature` DISABLE KEYS */;
INSERT INTO `ps_feature` (`id_feature`, `position`) VALUES
	(1, 0),
	(2, 1),
	(3, 2),
	(4, 3),
	(5, 4),
	(6, 5),
	(7, 6);
/*!40000 ALTER TABLE `ps_feature` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_feature_lang
DROP TABLE IF EXISTS `ps_feature_lang`;
CREATE TABLE IF NOT EXISTS `ps_feature_lang` (
  `id_feature` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_feature`,`id_lang`),
  KEY `id_lang` (`id_lang`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_feature_lang: ~7 rows (approximately)
/*!40000 ALTER TABLE `ps_feature_lang` DISABLE KEYS */;
INSERT INTO `ps_feature_lang` (`id_feature`, `id_lang`, `name`) VALUES
	(1, 1, 'Altura'),
	(2, 1, 'Anchura'),
	(5, 1, 'Composición'),
	(6, 1, 'Estilos'),
	(4, 1, 'Peso'),
	(3, 1, 'Profundidad'),
	(7, 1, 'Propiedades');
/*!40000 ALTER TABLE `ps_feature_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_feature_product
DROP TABLE IF EXISTS `ps_feature_product`;
CREATE TABLE IF NOT EXISTS `ps_feature_product` (
  `id_feature` int(10) unsigned NOT NULL,
  `id_product` int(10) unsigned NOT NULL,
  `id_feature_value` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_feature`,`id_product`),
  KEY `id_feature_value` (`id_feature_value`),
  KEY `id_product` (`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_feature_product: ~21 rows (approximately)
/*!40000 ALTER TABLE `ps_feature_product` DISABLE KEYS */;
INSERT INTO `ps_feature_product` (`id_feature`, `id_product`, `id_feature_value`) VALUES
	(5, 6, 1),
	(5, 7, 1),
	(5, 4, 3),
	(5, 5, 3),
	(5, 1, 5),
	(5, 2, 5),
	(5, 3, 5),
	(6, 1, 11),
	(6, 2, 11),
	(6, 5, 11),
	(6, 3, 13),
	(6, 6, 13),
	(6, 7, 13),
	(6, 4, 16),
	(7, 1, 17),
	(7, 2, 17),
	(7, 3, 18),
	(7, 4, 19),
	(7, 6, 19),
	(7, 7, 20),
	(7, 5, 21);
/*!40000 ALTER TABLE `ps_feature_product` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_feature_shop
DROP TABLE IF EXISTS `ps_feature_shop`;
CREATE TABLE IF NOT EXISTS `ps_feature_shop` (
  `id_feature` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_feature`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_feature_shop: ~7 rows (approximately)
/*!40000 ALTER TABLE `ps_feature_shop` DISABLE KEYS */;
INSERT INTO `ps_feature_shop` (`id_feature`, `id_shop`) VALUES
	(1, 1),
	(2, 1),
	(3, 1),
	(4, 1),
	(5, 1),
	(6, 1),
	(7, 1);
/*!40000 ALTER TABLE `ps_feature_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_feature_value
DROP TABLE IF EXISTS `ps_feature_value`;
CREATE TABLE IF NOT EXISTS `ps_feature_value` (
  `id_feature_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_feature` int(10) unsigned NOT NULL,
  `custom` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_feature_value`),
  KEY `feature` (`id_feature`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_feature_value: ~37 rows (approximately)
/*!40000 ALTER TABLE `ps_feature_value` DISABLE KEYS */;
INSERT INTO `ps_feature_value` (`id_feature_value`, `id_feature`, `custom`) VALUES
	(1, 5, 0),
	(2, 5, 0),
	(3, 5, 0),
	(4, 5, 0),
	(5, 5, 0),
	(6, 5, 0),
	(7, 5, 0),
	(8, 5, 0),
	(9, 5, 0),
	(10, 6, 0),
	(11, 6, 0),
	(12, 6, 0),
	(13, 6, 0),
	(14, 6, 0),
	(15, 6, 0),
	(16, 6, 0),
	(17, 7, 0),
	(18, 7, 0),
	(19, 7, 0),
	(20, 7, 0),
	(21, 7, 0),
	(22, 1, 1),
	(23, 2, 1),
	(24, 4, 1),
	(25, 3, 1),
	(26, 1, 1),
	(27, 2, 1),
	(28, 4, 1),
	(29, 3, 1),
	(30, 1, 1),
	(31, 2, 1),
	(32, 4, 1),
	(33, 3, 1),
	(34, 4, 0),
	(35, 4, 0),
	(36, 4, 0),
	(37, 4, 0);
/*!40000 ALTER TABLE `ps_feature_value` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_feature_value_lang
DROP TABLE IF EXISTS `ps_feature_value_lang`;
CREATE TABLE IF NOT EXISTS `ps_feature_value_lang` (
  `id_feature_value` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_feature_value`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_feature_value_lang: ~37 rows (approximately)
/*!40000 ALTER TABLE `ps_feature_value_lang` DISABLE KEYS */;
INSERT INTO `ps_feature_value_lang` (`id_feature_value`, `id_lang`, `value`) VALUES
	(1, 1, 'Poliéster'),
	(2, 1, 'Lana'),
	(3, 1, 'Viscosa'),
	(4, 1, 'Elastano'),
	(5, 1, 'Algodón'),
	(6, 1, 'Seda'),
	(7, 1, 'Ante'),
	(8, 1, 'Paja'),
	(9, 1, 'Piel'),
	(10, 1, 'Clásico'),
	(11, 1, 'Informal'),
	(12, 1, 'Militar'),
	(13, 1, 'Femenino'),
	(14, 1, 'Rockero'),
	(15, 1, 'Básico'),
	(16, 1, 'Elegante'),
	(17, 1, 'Manga corta'),
	(18, 1, 'Vestido colorido'),
	(19, 1, 'Vestido corto'),
	(20, 1, 'Vestido a media pierna'),
	(21, 1, 'Vestido largo'),
	(22, 1, '2.75 in'),
	(23, 1, '2.06 in'),
	(24, 1, '49.2 g'),
	(25, 1, '0.26 in'),
	(26, 1, '1.07 in'),
	(27, 1, '1.62 in'),
	(28, 1, '15.5 g'),
	(29, 1, '0.41 in (clip included)'),
	(30, 1, '4.33 in'),
	(31, 1, '2.76 in'),
	(32, 1, '120g'),
	(33, 1, '0.31 in'),
	(34, 1, 'Smart'),
	(35, 1, 'Urban'),
	(36, 1, 'Free'),
	(37, 1, 'Essentials');
/*!40000 ALTER TABLE `ps_feature_value_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_gender
DROP TABLE IF EXISTS `ps_gender`;
CREATE TABLE IF NOT EXISTS `ps_gender` (
  `id_gender` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_gender`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_gender: ~2 rows (approximately)
/*!40000 ALTER TABLE `ps_gender` DISABLE KEYS */;
INSERT INTO `ps_gender` (`id_gender`, `type`) VALUES
	(1, 0),
	(2, 1);
/*!40000 ALTER TABLE `ps_gender` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_gender_lang
DROP TABLE IF EXISTS `ps_gender_lang`;
CREATE TABLE IF NOT EXISTS `ps_gender_lang` (
  `id_gender` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id_gender`,`id_lang`),
  KEY `id_gender` (`id_gender`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_gender_lang: ~2 rows (approximately)
/*!40000 ALTER TABLE `ps_gender_lang` DISABLE KEYS */;
INSERT INTO `ps_gender_lang` (`id_gender`, `id_lang`, `name`) VALUES
	(1, 1, 'Sr.'),
	(2, 1, 'Sra.');
/*!40000 ALTER TABLE `ps_gender_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_group
DROP TABLE IF EXISTS `ps_group`;
CREATE TABLE IF NOT EXISTS `ps_group` (
  `id_group` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reduction` decimal(17,2) NOT NULL DEFAULT '0.00',
  `price_display_method` tinyint(4) NOT NULL DEFAULT '0',
  `show_prices` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_group`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_group: ~3 rows (approximately)
/*!40000 ALTER TABLE `ps_group` DISABLE KEYS */;
INSERT INTO `ps_group` (`id_group`, `reduction`, `price_display_method`, `show_prices`, `date_add`, `date_upd`) VALUES
	(1, 0.00, 0, 1, '2015-05-08 13:59:53', '2015-05-08 13:59:53'),
	(2, 0.00, 0, 1, '2015-05-08 13:59:53', '2015-05-08 13:59:53'),
	(3, 0.00, 0, 1, '2015-05-08 13:59:53', '2015-05-08 13:59:53');
/*!40000 ALTER TABLE `ps_group` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_group_lang
DROP TABLE IF EXISTS `ps_group_lang`;
CREATE TABLE IF NOT EXISTS `ps_group_lang` (
  `id_group` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`id_group`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_group_lang: ~3 rows (approximately)
/*!40000 ALTER TABLE `ps_group_lang` DISABLE KEYS */;
INSERT INTO `ps_group_lang` (`id_group`, `id_lang`, `name`) VALUES
	(1, 1, 'Visitante'),
	(2, 1, 'Invitado'),
	(3, 1, 'Cliente');
/*!40000 ALTER TABLE `ps_group_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_group_reduction
DROP TABLE IF EXISTS `ps_group_reduction`;
CREATE TABLE IF NOT EXISTS `ps_group_reduction` (
  `id_group_reduction` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `id_group` int(10) unsigned NOT NULL,
  `id_category` int(10) unsigned NOT NULL,
  `reduction` decimal(4,3) NOT NULL,
  PRIMARY KEY (`id_group_reduction`),
  UNIQUE KEY `id_group` (`id_group`,`id_category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_group_reduction: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_group_reduction` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_group_reduction` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_group_shop
DROP TABLE IF EXISTS `ps_group_shop`;
CREATE TABLE IF NOT EXISTS `ps_group_shop` (
  `id_group` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_group`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_group_shop: ~3 rows (approximately)
/*!40000 ALTER TABLE `ps_group_shop` DISABLE KEYS */;
INSERT INTO `ps_group_shop` (`id_group`, `id_shop`) VALUES
	(1, 1),
	(2, 1),
	(3, 1);
/*!40000 ALTER TABLE `ps_group_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_guest
DROP TABLE IF EXISTS `ps_guest`;
CREATE TABLE IF NOT EXISTS `ps_guest` (
  `id_guest` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_operating_system` int(10) unsigned DEFAULT NULL,
  `id_web_browser` int(10) unsigned DEFAULT NULL,
  `id_customer` int(10) unsigned DEFAULT NULL,
  `javascript` tinyint(1) DEFAULT '0',
  `screen_resolution_x` smallint(5) unsigned DEFAULT NULL,
  `screen_resolution_y` smallint(5) unsigned DEFAULT NULL,
  `screen_color` tinyint(3) unsigned DEFAULT NULL,
  `sun_java` tinyint(1) DEFAULT NULL,
  `adobe_flash` tinyint(1) DEFAULT NULL,
  `adobe_director` tinyint(1) DEFAULT NULL,
  `apple_quicktime` tinyint(1) DEFAULT NULL,
  `real_player` tinyint(1) DEFAULT NULL,
  `windows_media` tinyint(1) DEFAULT NULL,
  `accept_language` varchar(8) DEFAULT NULL,
  `mobile_theme` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_guest`),
  KEY `id_customer` (`id_customer`),
  KEY `id_operating_system` (`id_operating_system`),
  KEY `id_web_browser` (`id_web_browser`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_guest: ~7 rows (approximately)
/*!40000 ALTER TABLE `ps_guest` DISABLE KEYS */;
INSERT INTO `ps_guest` (`id_guest`, `id_operating_system`, `id_web_browser`, `id_customer`, `javascript`, `screen_resolution_x`, `screen_resolution_y`, `screen_color`, `sun_java`, `adobe_flash`, `adobe_director`, `apple_quicktime`, `real_player`, `windows_media`, `accept_language`, `mobile_theme`) VALUES
	(1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
	(2, 5, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'en', 0),
	(3, 5, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'en', 0),
	(4, 0, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
	(5, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
	(6, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
	(7, 0, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0);
/*!40000 ALTER TABLE `ps_guest` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_homeslider
DROP TABLE IF EXISTS `ps_homeslider`;
CREATE TABLE IF NOT EXISTS `ps_homeslider` (
  `id_homeslider_slides` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_homeslider_slides`,`id_shop`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_homeslider: ~3 rows (approximately)
/*!40000 ALTER TABLE `ps_homeslider` DISABLE KEYS */;
INSERT INTO `ps_homeslider` (`id_homeslider_slides`, `id_shop`) VALUES
	(1, 1),
	(2, 1),
	(3, 1);
/*!40000 ALTER TABLE `ps_homeslider` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_homeslider_slides
DROP TABLE IF EXISTS `ps_homeslider_slides`;
CREATE TABLE IF NOT EXISTS `ps_homeslider_slides` (
  `id_homeslider_slides` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_homeslider_slides`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_homeslider_slides: ~3 rows (approximately)
/*!40000 ALTER TABLE `ps_homeslider_slides` DISABLE KEYS */;
INSERT INTO `ps_homeslider_slides` (`id_homeslider_slides`, `position`, `active`) VALUES
	(1, 1, 1),
	(2, 2, 1),
	(3, 3, 1);
/*!40000 ALTER TABLE `ps_homeslider_slides` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_homeslider_slides_lang
DROP TABLE IF EXISTS `ps_homeslider_slides_lang`;
CREATE TABLE IF NOT EXISTS `ps_homeslider_slides_lang` (
  `id_homeslider_slides` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `legend` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id_homeslider_slides`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_homeslider_slides_lang: ~3 rows (approximately)
/*!40000 ALTER TABLE `ps_homeslider_slides_lang` DISABLE KEYS */;
INSERT INTO `ps_homeslider_slides_lang` (`id_homeslider_slides`, `id_lang`, `title`, `description`, `legend`, `url`, `image`) VALUES
	(1, 1, 'Sample 1', '<h2>EXCEPTEUR<br />OCCAECAT</h2>\n				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique in tortor et dignissim. Quisque non tempor leo. Maecenas egestas sem elit</p>\n				<p><button class="btn btn-default" type="button">Shop now !</button></p>', 'sample-1', 'http://www.prestashop.com/?utm_source=back-office&utm_medium=v16_homeslider&utm_campaign=back-office-ES&utm_content=download', 'sample-1.jpg'),
	(2, 1, 'Sample 2', '<h2>EXCEPTEUR<br />OCCAECAT</h2>\n				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique in tortor et dignissim. Quisque non tempor leo. Maecenas egestas sem elit</p>\n				<p><button class="btn btn-default" type="button">Shop now !</button></p>', 'sample-2', 'http://www.prestashop.com/?utm_source=back-office&utm_medium=v16_homeslider&utm_campaign=back-office-ES&utm_content=download', 'sample-2.jpg'),
	(3, 1, 'Sample 3', '<h2>EXCEPTEUR<br />OCCAECAT</h2>\n				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique in tortor et dignissim. Quisque non tempor leo. Maecenas egestas sem elit</p>\n				<p><button class="btn btn-default" type="button">Shop now !</button></p>', 'sample-3', 'http://www.prestashop.com/?utm_source=back-office&utm_medium=v16_homeslider&utm_campaign=back-office-ES&utm_content=download', 'sample-3.jpg');
/*!40000 ALTER TABLE `ps_homeslider_slides_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_hook
DROP TABLE IF EXISTS `ps_hook`;
CREATE TABLE IF NOT EXISTS `ps_hook` (
  `id_hook` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` text,
  `position` tinyint(1) NOT NULL DEFAULT '1',
  `live_edit` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_hook`),
  UNIQUE KEY `hook_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=173 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_hook: ~169 rows (approximately)
/*!40000 ALTER TABLE `ps_hook` DISABLE KEYS */;
INSERT INTO `ps_hook` (`id_hook`, `name`, `title`, `description`, `position`, `live_edit`) VALUES
	(1, 'displayPayment', 'Payment', 'This hook displays new elements on the payment page', 1, 1),
	(2, 'actionValidateOrder', 'New orders', '', 1, 0),
	(3, 'displayMaintenance', 'Maintenance Page', 'This hook displays new elements on the maintenance page', 1, 0),
	(4, 'actionPaymentConfirmation', 'Payment confirmation', 'This hook displays new elements after the payment is validated', 1, 0),
	(5, 'displayPaymentReturn', 'Payment return', '', 1, 0),
	(6, 'actionUpdateQuantity', 'Quantity update', 'Quantity is updated only when a customer effectively places their order', 1, 0),
	(7, 'displayRightColumn', 'Right column blocks', 'This hook displays new elements in the right-hand column', 1, 1),
	(8, 'displayLeftColumn', 'Left column blocks', 'This hook displays new elements in the left-hand column', 1, 1),
	(9, 'displayHome', 'Homepage content', 'This hook displays new elements on the homepage', 1, 1),
	(10, 'Header', 'Pages html head section', 'This hook adds additional elements in the head section of your pages (head section of html)', 1, 0),
	(11, 'actionCartSave', 'Cart creation and update', 'This hook is displayed when a product is added to the cart or if the cart\'s content is modified', 1, 0),
	(12, 'actionAuthentication', 'Successful customer authentication', 'This hook is displayed after a customer successfully signs in', 1, 0),
	(13, 'actionProductAdd', 'Product creation', 'This hook is displayed after a product is created', 1, 0),
	(14, 'actionProductUpdate', 'Product update', 'This hook is displayed after a product has been updated', 1, 0),
	(15, 'displayTop', 'Top of pages', 'This hook displays additional elements at the top of your pages', 1, 0),
	(16, 'displayRightColumnProduct', 'New elements on the product page (right column)', 'This hook displays new elements in the right-hand column of the product page', 1, 0),
	(17, 'actionProductDelete', 'Product deletion', 'This hook is called when a product is deleted', 1, 0),
	(18, 'displayFooterProduct', 'Product footer', 'This hook adds new blocks under the product\'s description', 1, 1),
	(19, 'displayInvoice', 'Invoice', 'This hook displays new blocks on the invoice (order)', 1, 0),
	(20, 'actionOrderStatusUpdate', 'Order status update - Event', 'This hook launches modules when the status of an order changes.', 1, 0),
	(21, 'displayAdminOrder', 'Display new elements in the Back Office, tab AdminOrder', 'This hook launches modules when the AdminOrder tab is displayed in the Back Office', 1, 0),
	(22, 'displayAdminOrderTabOrder', 'Display new elements in Back Office, AdminOrder, panel Order', 'This hook launches modules when the AdminOrder tab is displayed in the Back Office and extends / override Order panel tabs', 1, 0),
	(23, 'displayAdminOrderTabShip', 'Display new elements in Back Office, AdminOrder, panel Shipping', 'This hook launches modules when the AdminOrder tab is displayed in the Back Office and extends / override Shipping panel tabs', 1, 0),
	(24, 'displayAdminOrderContentOrder', 'Display new elements in Back Office, AdminOrder, panel Order', 'This hook launches modules when the AdminOrder tab is displayed in the Back Office and extends / override Order panel content', 1, 0),
	(25, 'displayAdminOrderContentShip', 'Display new elements in Back Office, AdminOrder, panel Shipping', 'This hook launches modules when the AdminOrder tab is displayed in the Back Office and extends / override Shipping panel content', 1, 0),
	(26, 'displayFooter', 'Footer', 'This hook displays new blocks in the footer', 1, 0),
	(27, 'displayPDFInvoice', 'PDF Invoice', 'This hook allows you to display additional information on PDF invoices', 1, 0),
	(28, 'displayAdminCustomers', 'Display new elements in the Back Office, tab AdminCustomers', 'This hook launches modules when the AdminCustomers tab is displayed in the Back Office', 1, 0),
	(29, 'displayOrderConfirmation', 'Order confirmation page', 'This hook is called within an order\'s confirmation page', 1, 0),
	(30, 'actionCustomerAccountAdd', 'Successful customer account creation', 'This hook is called when a new customer creates an account successfully', 1, 0),
	(31, 'displayCustomerAccount', 'Customer account displayed in Front Office', 'This hook displays new elements on the customer account page', 1, 0),
	(32, 'displayCustomerIdentityForm', 'Customer identity form displayed in Front Office', 'This hook displays new elements on the form to update a customer identity', 1, 0),
	(33, 'actionOrderSlipAdd', 'Order slip creation', 'This hook is called when a new credit slip is added regarding client order', 1, 0),
	(34, 'displayProductTab', 'Tabs on product page', 'This hook is called on the product page\'s tab', 1, 0),
	(35, 'displayProductTabContent', 'Tabs content on the product page', 'This hook is called on the product page\'s tab', 1, 0),
	(36, 'displayShoppingCartFooter', 'Shopping cart footer', 'This hook displays some specific information on the shopping cart\'s page', 1, 0),
	(37, 'displayCustomerAccountForm', 'Customer account creation form', 'This hook displays some information on the form to create a customer account', 1, 0),
	(38, 'displayAdminStatsModules', 'Stats - Modules', '', 1, 0),
	(39, 'displayAdminStatsGraphEngine', 'Graph engines', '', 1, 0),
	(40, 'actionOrderReturn', 'Returned product', 'This hook is displayed when a customer returns a product ', 1, 0),
	(41, 'displayProductButtons', 'Product page actions', 'This hook adds new action buttons on the product page', 1, 0),
	(42, 'displayBackOfficeHome', 'Administration panel homepage', 'This hook is displayed on the admin panel\'s homepage', 1, 0),
	(43, 'displayAdminStatsGridEngine', 'Grid engines', '', 1, 0),
	(44, 'actionWatermark', 'Watermark', '', 1, 0),
	(45, 'actionProductCancel', 'Product cancelled', 'This hook is called when you cancel a product in an order', 1, 0),
	(46, 'displayLeftColumnProduct', 'New elements on the product page (left column)', 'This hook displays new elements in the left-hand column of the product page', 1, 0),
	(47, 'actionProductOutOfStock', 'Out-of-stock product', 'This hook displays new action buttons if a product is out of stock', 1, 0),
	(48, 'actionProductAttributeUpdate', 'Product attribute update', 'This hook is displayed when a product\'s attribute is updated', 1, 0),
	(49, 'displayCarrierList', 'Extra carrier (module mode)', '', 1, 0),
	(50, 'displayShoppingCart', 'Shopping cart - Additional button', 'This hook displays new action buttons within the shopping cart', 1, 0),
	(51, 'actionSearch', 'Search', '', 1, 0),
	(52, 'displayBeforePayment', 'Redirect during the order process', 'This hook redirects the user to the module instead of displaying payment modules', 1, 0),
	(53, 'actionCarrierUpdate', 'Carrier Update', 'This hook is called when a carrier is updated', 1, 0),
	(54, 'actionOrderStatusPostUpdate', 'Post update of order status', '', 1, 0),
	(55, 'displayCustomerAccountFormTop', 'Block above the form for create an account', 'This hook is displayed above the customer\'s account creation form', 1, 0),
	(56, 'displayBackOfficeHeader', 'Administration panel header', 'This hook is displayed in the header of the admin panel', 1, 0),
	(57, 'displayBackOfficeTop', 'Administration panel hover the tabs', 'This hook is displayed on the roll hover of the tabs within the admin panel', 1, 0),
	(58, 'displayBackOfficeFooter', 'Administration panel footer', 'This hook is displayed within the admin panel\'s footer', 1, 0),
	(59, 'actionProductAttributeDelete', 'Product attribute deletion', 'This hook is displayed when a product\'s attribute is deleted', 1, 0),
	(60, 'actionCarrierProcess', 'Carrier process', '', 1, 0),
	(61, 'actionOrderDetail', 'Order detail', 'This hook is used to set the follow-up in Smarty when an order\'s detail is called', 1, 0),
	(62, 'displayBeforeCarrier', 'Before carriers list', 'This hook is displayed before the carrier list in Front Office', 1, 0),
	(63, 'displayOrderDetail', 'Order detail', 'This hook is displayed within the order\'s details in Front Office', 1, 0),
	(64, 'actionPaymentCCAdd', 'Payment CC added', '', 1, 0),
	(65, 'displayProductComparison', 'Extra product comparison', '', 1, 0),
	(66, 'actionCategoryAdd', 'Category creation', 'This hook is displayed when a category is created', 1, 0),
	(67, 'actionCategoryUpdate', 'Category modification', 'This hook is displayed when a category is modified', 1, 0),
	(68, 'actionCategoryDelete', 'Category deletion', 'This hook is displayed when a category is deleted', 1, 0),
	(69, 'actionBeforeAuthentication', 'Before authentication', 'This hook is displayed before the customer\'s authentication', 1, 0),
	(70, 'displayPaymentTop', 'Top of payment page', 'This hook is displayed at the top of the payment page', 1, 0),
	(71, 'actionHtaccessCreate', 'After htaccess creation', 'This hook is displayed after the htaccess creation', 1, 0),
	(72, 'actionAdminMetaSave', 'After saving the configuration in AdminMeta', 'This hook is displayed after saving the configuration in AdminMeta', 1, 0),
	(73, 'displayAttributeGroupForm', 'Add fields to the form \'attribute group\'', 'This hook adds fields to the form \'attribute group\'', 1, 0),
	(74, 'actionAttributeGroupSave', 'Saving an attribute group', 'This hook is called while saving an attributes group', 1, 0),
	(75, 'actionAttributeGroupDelete', 'Deleting attribute group', 'This hook is called while deleting an attributes  group', 1, 0),
	(76, 'displayFeatureForm', 'Add fields to the form \'feature\'', 'This hook adds fields to the form \'feature\'', 1, 0),
	(77, 'actionFeatureSave', 'Saving attributes\' features', 'This hook is called while saving an attributes features', 1, 0),
	(78, 'actionFeatureDelete', 'Deleting attributes\' features', 'This hook is called while deleting an attributes features', 1, 0),
	(79, 'actionProductSave', 'Saving products', 'This hook is called while saving products', 1, 0),
	(80, 'actionProductListOverride', 'Assign a products list to a category', 'This hook assigns a products list to a category', 1, 0),
	(81, 'displayAttributeGroupPostProcess', 'On post-process in admin attribute group', 'This hook is called on post-process in admin attribute group', 1, 0),
	(82, 'displayFeaturePostProcess', 'On post-process in admin feature', 'This hook is called on post-process in admin feature', 1, 0),
	(83, 'displayFeatureValueForm', 'Add fields to the form \'feature value\'', 'This hook adds fields to the form \'feature value\'', 1, 0),
	(84, 'displayFeatureValuePostProcess', 'On post-process in admin feature value', 'This hook is called on post-process in admin feature value', 1, 0),
	(85, 'actionFeatureValueDelete', 'Deleting attributes\' features\' values', 'This hook is called while deleting an attributes features value', 1, 0),
	(86, 'actionFeatureValueSave', 'Saving an attributes features value', 'This hook is called while saving an attributes features value', 1, 0),
	(87, 'displayAttributeForm', 'Add fields to the form \'attribute value\'', 'This hook adds fields to the form \'attribute value\'', 1, 0),
	(88, 'actionAttributePostProcess', 'On post-process in admin feature value', 'This hook is called on post-process in admin feature value', 1, 0),
	(89, 'actionAttributeDelete', 'Deleting an attributes features value', 'This hook is called while deleting an attributes features value', 1, 0),
	(90, 'actionAttributeSave', 'Saving an attributes features value', 'This hook is called while saving an attributes features value', 1, 0),
	(91, 'actionTaxManager', 'Tax Manager Factory', '', 1, 0),
	(93, 'actionModuleInstallBefore', 'actionModuleInstallBefore', '', 1, 0),
	(94, 'actionModuleInstallAfter', 'actionModuleInstallAfter', '', 1, 0),
	(95, 'displayHomeTab', 'Home Page Tabs', 'This hook displays new elements on the homepage tabs', 1, 1),
	(96, 'displayHomeTabContent', 'Home Page Tabs Content', 'This hook displays new elements on the homepage tabs content', 1, 1),
	(97, 'displayTopColumn', 'Top column blocks', 'This hook displays new elements in the top of columns', 1, 1),
	(98, 'displayBackOfficeCategory', 'Display new elements in the Back Office, tab AdminCategories', 'This hook launches modules when the AdminCategories tab is displayed in the Back Office', 1, 0),
	(99, 'displayProductListFunctionalButtons', 'Display new elements in the Front Office, products list', 'This hook launches modules when the products list is displayed in the Front Office', 1, 0),
	(100, 'displayNav', 'Navigation', '', 1, 1),
	(101, 'displayOverrideTemplate', 'Change the default template of current controller', '', 1, 0),
	(102, 'actionObjectProductUpdateAfter', 'actionObjectProductUpdateAfter', '', 0, 0),
	(103, 'actionObjectProductDeleteAfter', 'actionObjectProductDeleteAfter', '', 0, 0),
	(104, 'displayCompareExtraInformation', 'displayCompareExtraInformation', '', 1, 1),
	(105, 'displayBanner', 'displayBanner', '', 1, 1),
	(106, 'actionObjectLanguageAddAfter', 'actionObjectLanguageAddAfter', '', 0, 0),
	(107, 'displayPaymentEU', 'displayPaymentEU', '', 1, 1),
	(108, 'actionCartListOverride', 'actionCartListOverride', '', 0, 0),
	(109, 'actionAdminMetaControllerUpdate_optionsBefore', 'actionAdminMetaControllerUpdate_optionsBefore', '', 0, 0),
	(110, 'actionAdminLanguagesControllerStatusBefore', 'actionAdminLanguagesControllerStatusBefore', '', 0, 0),
	(111, 'actionObjectCmsUpdateAfter', 'actionObjectCmsUpdateAfter', '', 0, 0),
	(112, 'actionObjectCmsDeleteAfter', 'actionObjectCmsDeleteAfter', '', 0, 0),
	(113, 'actionShopDataDuplication', 'actionShopDataDuplication', '', 0, 0),
	(114, 'actionAdminStoresControllerUpdate_optionsAfter', 'actionAdminStoresControllerUpdate_optionsAfter', '', 0, 0),
	(115, 'actionObjectManufacturerDeleteAfter', 'actionObjectManufacturerDeleteAfter', '', 0, 0),
	(116, 'actionObjectManufacturerAddAfter', 'actionObjectManufacturerAddAfter', '', 0, 0),
	(117, 'actionObjectManufacturerUpdateAfter', 'actionObjectManufacturerUpdateAfter', '', 0, 0),
	(119, 'actionModuleRegisterHookAfter', 'actionModuleRegisterHookAfter', '', 0, 0),
	(120, 'actionModuleUnRegisterHookAfter', 'actionModuleUnRegisterHookAfter', '', 0, 0),
	(121, 'displayMyAccountBlockfooter', 'My account block', 'Display extra informations inside the "my account" block', 1, 0),
	(122, 'displayMobileTopSiteMap', 'displayMobileTopSiteMap', '', 1, 1),
	(123, 'actionObjectSupplierDeleteAfter', 'actionObjectSupplierDeleteAfter', '', 0, 0),
	(124, 'actionObjectSupplierAddAfter', 'actionObjectSupplierAddAfter', '', 0, 0),
	(125, 'actionObjectSupplierUpdateAfter', 'actionObjectSupplierUpdateAfter', '', 0, 0),
	(126, 'actionObjectCategoryUpdateAfter', 'actionObjectCategoryUpdateAfter', '', 0, 0),
	(127, 'actionObjectCategoryDeleteAfter', 'actionObjectCategoryDeleteAfter', '', 0, 0),
	(128, 'actionObjectCategoryAddAfter', 'actionObjectCategoryAddAfter', '', 0, 0),
	(129, 'actionObjectCmsAddAfter', 'actionObjectCmsAddAfter', '', 0, 0),
	(130, 'actionObjectProductAddAfter', 'actionObjectProductAddAfter', '', 0, 0),
	(131, 'dashboardZoneOne', 'dashboardZoneOne', '', 0, 0),
	(132, 'dashboardData', 'dashboardData', '', 0, 0),
	(133, 'actionObjectOrderAddAfter', 'actionObjectOrderAddAfter', '', 0, 0),
	(134, 'actionObjectCustomerAddAfter', 'actionObjectCustomerAddAfter', '', 0, 0),
	(135, 'actionObjectCustomerMessageAddAfter', 'actionObjectCustomerMessageAddAfter', '', 0, 0),
	(136, 'actionObjectCustomerThreadAddAfter', 'actionObjectCustomerThreadAddAfter', '', 0, 0),
	(137, 'actionObjectOrderReturnAddAfter', 'actionObjectOrderReturnAddAfter', '', 0, 0),
	(138, 'actionAdminControllerSetMedia', 'actionAdminControllerSetMedia', '', 0, 0),
	(139, 'dashboardZoneTwo', 'dashboardZoneTwo', '', 0, 0),
	(140, 'displayProductListReviews', 'displayProductListReviews', '', 1, 1),
	(141, 'actionAdminMetaControllerUpdate_optionsAfter', 'actionAdminMetaControllerUpdate_optionsAfter', '', 0, 0),
	(142, 'actionAdminPerformanceControllerSaveAfter', 'actionAdminPerformanceControllerSaveAfter', '', 0, 0),
	(143, 'actionObjectCarrierAddAfter', 'actionObjectCarrierAddAfter', '', 0, 0),
	(144, 'actionObjectContactAddAfter', 'actionObjectContactAddAfter', '', 0, 0),
	(145, 'actionAdminThemesControllerUpdate_optionsAfter', 'actionAdminThemesControllerUpdate_optionsAfter', '', 0, 0),
	(146, 'actionObjectShopUpdateAfter', 'actionObjectShopUpdateAfter', '', 0, 0),
	(147, 'actionAdminPreferencesControllerUpdate_optionsAfter', 'actionAdminPreferencesControllerUpdate_optionsAfter', '', 0, 0),
	(148, 'actionObjectShopAddAfter', 'actionObjectShopAddAfter', '', 0, 0),
	(149, 'actionObjectShopGroupAddAfter', 'actionObjectShopGroupAddAfter', '', 0, 0),
	(150, 'actionObjectCartAddAfter', 'actionObjectCartAddAfter', '', 0, 0),
	(151, 'actionObjectEmployeeAddAfter', 'actionObjectEmployeeAddAfter', '', 0, 0),
	(152, 'actionObjectImageAddAfter', 'actionObjectImageAddAfter', '', 0, 0),
	(153, 'actionObjectCartRuleAddAfter', 'actionObjectCartRuleAddAfter', '', 0, 0),
	(154, 'actionAdminStoresControllerSaveAfter', 'actionAdminStoresControllerSaveAfter', '', 0, 0),
	(155, 'actionAdminWebserviceControllerSaveAfter', 'actionAdminWebserviceControllerSaveAfter', '', 0, 0),
	(156, 'displayHeaderRight', 'displayHeaderRight', '', 0, 0),
	(157, 'displaySlideshow', 'displaySlideshow', '', 0, 0),
	(158, 'topNavigation', 'topNavigation', '', 0, 0),
	(159, 'displayPromoteTop', 'displayPromoteTop', '', 0, 0),
	(160, 'displayBottom', 'displayBottom', '', 0, 0),
	(161, 'displayContentBottom', 'displayContentBottom', '', 0, 0),
	(162, 'displayFootNav', 'displayFootNav', '', 0, 0),
	(163, 'displayFooterTop', 'displayFooterTop', '', 0, 0),
	(164, 'displayFooterBottom', 'displayFooterBottom', '', 0, 0),
	(165, 'displayMainmenu', 'displayMainmenu', '', 0, 0),
	(166, 'moduleRoutes', 'moduleRoutes', '', 0, 0),
	(167, 'displayMapLocal', 'displayMapLocal', '', 0, 0),
	(168, 'actionProductListModifier', 'actionProductListModifier', '', 0, 0),
	(169, 'displayProductListSwap', 'displayProductListSwap', '', 1, 1),
	(170, 'displayProductListGallery', 'displayProductListGallery', '', 1, 1),
	(171, 'advancedPaymentApi', 'advancedPaymentApi', '', 0, 0),
	(172, 'displayMyAccountBlock', 'My account block', 'Display extra informations inside the "my account" block', 1, 0);
/*!40000 ALTER TABLE `ps_hook` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_hook_alias
DROP TABLE IF EXISTS `ps_hook_alias`;
CREATE TABLE IF NOT EXISTS `ps_hook_alias` (
  `id_hook_alias` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `alias` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id_hook_alias`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_hook_alias: ~86 rows (approximately)
/*!40000 ALTER TABLE `ps_hook_alias` DISABLE KEYS */;
INSERT INTO `ps_hook_alias` (`id_hook_alias`, `alias`, `name`) VALUES
	(1, 'payment', 'displayPayment'),
	(2, 'newOrder', 'actionValidateOrder'),
	(3, 'paymentConfirm', 'actionPaymentConfirmation'),
	(4, 'paymentReturn', 'displayPaymentReturn'),
	(5, 'updateQuantity', 'actionUpdateQuantity'),
	(6, 'rightColumn', 'displayRightColumn'),
	(7, 'leftColumn', 'displayLeftColumn'),
	(8, 'home', 'displayHome'),
	(9, 'displayHeader', 'Header'),
	(10, 'cart', 'actionCartSave'),
	(11, 'authentication', 'actionAuthentication'),
	(12, 'addproduct', 'actionProductAdd'),
	(13, 'updateproduct', 'actionProductUpdate'),
	(14, 'top', 'displayTop'),
	(15, 'extraRight', 'displayRightColumnProduct'),
	(16, 'deleteproduct', 'actionProductDelete'),
	(17, 'productfooter', 'displayFooterProduct'),
	(18, 'invoice', 'displayInvoice'),
	(19, 'updateOrderStatus', 'actionOrderStatusUpdate'),
	(20, 'adminOrder', 'displayAdminOrder'),
	(21, 'footer', 'displayFooter'),
	(22, 'PDFInvoice', 'displayPDFInvoice'),
	(23, 'adminCustomers', 'displayAdminCustomers'),
	(24, 'orderConfirmation', 'displayOrderConfirmation'),
	(25, 'createAccount', 'actionCustomerAccountAdd'),
	(26, 'customerAccount', 'displayCustomerAccount'),
	(27, 'orderSlip', 'actionOrderSlipAdd'),
	(28, 'productTab', 'displayProductTab'),
	(29, 'productTabContent', 'displayProductTabContent'),
	(30, 'shoppingCart', 'displayShoppingCartFooter'),
	(31, 'createAccountForm', 'displayCustomerAccountForm'),
	(32, 'AdminStatsModules', 'displayAdminStatsModules'),
	(33, 'GraphEngine', 'displayAdminStatsGraphEngine'),
	(34, 'orderReturn', 'actionOrderReturn'),
	(35, 'productActions', 'displayProductButtons'),
	(36, 'backOfficeHome', 'displayBackOfficeHome'),
	(37, 'GridEngine', 'displayAdminStatsGridEngine'),
	(38, 'watermark', 'actionWatermark'),
	(39, 'cancelProduct', 'actionProductCancel'),
	(40, 'extraLeft', 'displayLeftColumnProduct'),
	(41, 'productOutOfStock', 'actionProductOutOfStock'),
	(42, 'updateProductAttribute', 'actionProductAttributeUpdate'),
	(43, 'extraCarrier', 'displayCarrierList'),
	(44, 'shoppingCartExtra', 'displayShoppingCart'),
	(45, 'search', 'actionSearch'),
	(46, 'backBeforePayment', 'displayBeforePayment'),
	(47, 'updateCarrier', 'actionCarrierUpdate'),
	(48, 'postUpdateOrderStatus', 'actionOrderStatusPostUpdate'),
	(49, 'createAccountTop', 'displayCustomerAccountFormTop'),
	(50, 'backOfficeHeader', 'displayBackOfficeHeader'),
	(51, 'backOfficeTop', 'displayBackOfficeTop'),
	(52, 'backOfficeFooter', 'displayBackOfficeFooter'),
	(53, 'deleteProductAttribute', 'actionProductAttributeDelete'),
	(54, 'processCarrier', 'actionCarrierProcess'),
	(55, 'orderDetail', 'actionOrderDetail'),
	(56, 'beforeCarrier', 'displayBeforeCarrier'),
	(57, 'orderDetailDisplayed', 'displayOrderDetail'),
	(58, 'paymentCCAdded', 'actionPaymentCCAdd'),
	(59, 'extraProductComparison', 'displayProductComparison'),
	(60, 'categoryAddition', 'actionCategoryAdd'),
	(61, 'categoryUpdate', 'actionCategoryUpdate'),
	(62, 'categoryDeletion', 'actionCategoryDelete'),
	(63, 'beforeAuthentication', 'actionBeforeAuthentication'),
	(64, 'paymentTop', 'displayPaymentTop'),
	(65, 'afterCreateHtaccess', 'actionHtaccessCreate'),
	(66, 'afterSaveAdminMeta', 'actionAdminMetaSave'),
	(67, 'attributeGroupForm', 'displayAttributeGroupForm'),
	(68, 'afterSaveAttributeGroup', 'actionAttributeGroupSave'),
	(69, 'afterDeleteAttributeGroup', 'actionAttributeGroupDelete'),
	(70, 'featureForm', 'displayFeatureForm'),
	(71, 'afterSaveFeature', 'actionFeatureSave'),
	(72, 'afterDeleteFeature', 'actionFeatureDelete'),
	(73, 'afterSaveProduct', 'actionProductSave'),
	(74, 'productListAssign', 'actionProductListOverride'),
	(75, 'postProcessAttributeGroup', 'displayAttributeGroupPostProcess'),
	(76, 'postProcessFeature', 'displayFeaturePostProcess'),
	(77, 'featureValueForm', 'displayFeatureValueForm'),
	(78, 'postProcessFeatureValue', 'displayFeatureValuePostProcess'),
	(79, 'afterDeleteFeatureValue', 'actionFeatureValueDelete'),
	(80, 'afterSaveFeatureValue', 'actionFeatureValueSave'),
	(81, 'attributeForm', 'displayAttributeForm'),
	(82, 'postProcessAttribute', 'actionAttributePostProcess'),
	(83, 'afterDeleteAttribute', 'actionAttributeDelete'),
	(84, 'afterSaveAttribute', 'actionAttributeSave'),
	(85, 'taxManager', 'actionTaxManager'),
	(86, 'myAccountBlock', 'displayMyAccountBlock');
/*!40000 ALTER TABLE `ps_hook_alias` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_hook_module
DROP TABLE IF EXISTS `ps_hook_module`;
CREATE TABLE IF NOT EXISTS `ps_hook_module` (
  `id_module` int(10) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_hook` int(10) unsigned NOT NULL,
  `position` tinyint(2) unsigned NOT NULL,
  PRIMARY KEY (`id_module`,`id_hook`,`id_shop`),
  KEY `id_hook` (`id_hook`),
  KEY `id_module` (`id_module`),
  KEY `position` (`id_shop`,`position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_hook_module: ~309 rows (approximately)
/*!40000 ALTER TABLE `ps_hook_module` DISABLE KEYS */;
INSERT INTO `ps_hook_module` (`id_module`, `id_shop`, `id_hook`, `position`) VALUES
	(1, 1, 10, 1),
	(1, 1, 102, 1),
	(1, 1, 103, 1),
	(1, 1, 104, 1),
	(2, 1, 105, 1),
	(2, 1, 106, 1),
	(3, 1, 1, 1),
	(3, 1, 5, 1),
	(3, 1, 107, 1),
	(4, 1, 8, 1),
	(4, 1, 13, 1),
	(4, 1, 14, 1),
	(4, 1, 17, 1),
	(4, 1, 54, 1),
	(5, 1, 108, 1),
	(7, 1, 66, 1),
	(7, 1, 67, 1),
	(7, 1, 68, 1),
	(7, 1, 98, 1),
	(7, 1, 109, 1),
	(7, 1, 110, 1),
	(8, 1, 100, 1),
	(11, 1, 73, 1),
	(11, 1, 74, 1),
	(11, 1, 75, 1),
	(11, 1, 76, 1),
	(11, 1, 77, 1),
	(11, 1, 78, 1),
	(11, 1, 79, 1),
	(11, 1, 80, 1),
	(11, 1, 81, 1),
	(11, 1, 82, 1),
	(11, 1, 83, 1),
	(11, 1, 84, 1),
	(11, 1, 85, 1),
	(11, 1, 86, 1),
	(11, 1, 87, 1),
	(11, 1, 88, 1),
	(11, 1, 89, 1),
	(11, 1, 90, 1),
	(12, 1, 111, 1),
	(12, 1, 112, 1),
	(12, 1, 113, 1),
	(12, 1, 114, 1),
	(16, 1, 115, 1),
	(16, 1, 116, 1),
	(16, 1, 117, 1),
	(18, 1, 119, 1),
	(18, 1, 120, 1),
	(19, 1, 95, 1),
	(19, 1, 96, 1),
	(20, 1, 26, 1),
	(20, 1, 30, 1),
	(22, 1, 122, 1),
	(25, 1, 123, 1),
	(25, 1, 124, 1),
	(25, 1, 125, 1),
	(27, 1, 126, 1),
	(27, 1, 127, 1),
	(27, 1, 128, 1),
	(27, 1, 129, 1),
	(27, 1, 130, 1),
	(30, 1, 171, 1),
	(31, 1, 131, 1),
	(31, 1, 132, 1),
	(31, 1, 133, 1),
	(31, 1, 134, 1),
	(31, 1, 135, 1),
	(31, 1, 136, 1),
	(31, 1, 137, 1),
	(31, 1, 138, 1),
	(32, 1, 139, 1),
	(34, 1, 51, 1),
	(35, 1, 39, 1),
	(36, 1, 43, 1),
	(37, 1, 97, 1),
	(40, 1, 38, 1),
	(50, 1, 12, 1),
	(63, 1, 9, 1),
	(63, 1, 56, 1),
	(64, 1, 2, 1),
	(64, 1, 20, 1),
	(64, 1, 94, 1),
	(64, 1, 141, 1),
	(64, 1, 142, 1),
	(64, 1, 143, 1),
	(64, 1, 144, 1),
	(64, 1, 145, 1),
	(64, 1, 146, 1),
	(64, 1, 147, 1),
	(64, 1, 148, 1),
	(64, 1, 149, 1),
	(64, 1, 150, 1),
	(64, 1, 151, 1),
	(64, 1, 152, 1),
	(64, 1, 153, 1),
	(64, 1, 154, 1),
	(64, 1, 155, 1),
	(65, 1, 11, 1),
	(65, 1, 28, 1),
	(65, 1, 31, 1),
	(65, 1, 41, 1),
	(65, 1, 99, 1),
	(66, 1, 34, 1),
	(66, 1, 35, 1),
	(66, 1, 65, 1),
	(66, 1, 140, 1),
	(67, 1, 46, 1),
	(69, 1, 57, 1),
	(71, 1, 165, 1),
	(72, 1, 7, 1),
	(72, 1, 97, 1),
	(72, 1, 157, 1),
	(72, 1, 160, 1),
	(72, 1, 163, 1),
	(72, 1, 164, 1),
	(74, 1, 9, 1),
	(75, 1, 166, 1),
	(78, 1, 167, 1),
	(79, 1, 18, 1),
	(82, 1, 57, 1),
	(82, 1, 93, 1),
	(82, 1, 168, 1),
	(82, 1, 169, 1),
	(82, 1, 170, 1),
	(1, 1, 46, 2),
	(2, 1, 10, 2),
	(6, 1, 26, 2),
	(9, 1, 9, 2),
	(11, 1, 8, 2),
	(11, 1, 66, 2),
	(11, 1, 67, 2),
	(11, 1, 68, 2),
	(14, 1, 100, 2),
	(19, 1, 13, 2),
	(19, 1, 14, 2),
	(19, 1, 17, 2),
	(27, 1, 102, 2),
	(27, 1, 103, 2),
	(27, 1, 111, 2),
	(27, 1, 112, 2),
	(27, 1, 113, 2),
	(27, 1, 115, 2),
	(27, 1, 116, 2),
	(27, 1, 117, 2),
	(27, 1, 123, 2),
	(27, 1, 124, 2),
	(27, 1, 125, 2),
	(30, 1, 1, 2),
	(30, 1, 5, 2),
	(30, 1, 107, 2),
	(32, 1, 54, 2),
	(32, 1, 132, 2),
	(32, 1, 138, 2),
	(33, 1, 139, 2),
	(34, 1, 133, 2),
	(38, 1, 95, 2),
	(38, 1, 96, 2),
	(39, 1, 41, 2),
	(41, 1, 38, 2),
	(50, 1, 30, 2),
	(60, 1, 51, 2),
	(63, 1, 97, 2),
	(63, 1, 106, 2),
	(64, 1, 56, 2),
	(64, 1, 114, 2),
	(64, 1, 129, 2),
	(64, 1, 130, 2),
	(64, 1, 134, 2),
	(64, 1, 136, 2),
	(68, 1, 119, 2),
	(68, 1, 120, 2),
	(70, 1, 115, 2),
	(70, 1, 116, 2),
	(70, 1, 117, 2),
	(72, 1, 106, 2),
	(77, 1, 113, 2),
	(77, 1, 157, 2),
	(80, 1, 122, 2),
	(81, 1, 9, 2),
	(82, 1, 94, 2),
	(83, 1, 160, 2),
	(4, 1, 10, 3),
	(4, 1, 95, 3),
	(4, 1, 96, 3),
	(7, 1, 26, 3),
	(12, 1, 8, 3),
	(13, 1, 9, 3),
	(23, 1, 13, 3),
	(23, 1, 14, 3),
	(23, 1, 17, 3),
	(27, 1, 67, 3),
	(28, 1, 15, 3),
	(33, 1, 132, 3),
	(33, 1, 138, 3),
	(34, 1, 139, 3),
	(37, 1, 113, 3),
	(42, 1, 38, 3),
	(64, 1, 133, 3),
	(68, 1, 56, 3),
	(72, 1, 56, 3),
	(76, 1, 160, 3),
	(77, 1, 97, 3),
	(81, 1, 106, 3),
	(86, 1, 100, 3),
	(88, 1, 119, 3),
	(88, 1, 120, 3),
	(6, 1, 10, 4),
	(12, 1, 26, 4),
	(16, 1, 8, 4),
	(26, 1, 13, 4),
	(26, 1, 14, 4),
	(26, 1, 17, 4),
	(34, 1, 132, 4),
	(35, 1, 138, 4),
	(38, 1, 13, 4),
	(38, 1, 14, 4),
	(38, 1, 17, 4),
	(38, 1, 67, 4),
	(43, 1, 38, 4),
	(65, 1, 15, 4),
	(70, 1, 160, 4),
	(71, 1, 106, 4),
	(75, 1, 56, 4),
	(7, 1, 10, 5),
	(18, 1, 26, 5),
	(40, 1, 15, 5),
	(44, 1, 38, 5),
	(69, 1, 138, 5),
	(72, 1, 8, 5),
	(77, 1, 15, 5),
	(82, 1, 26, 5),
	(82, 1, 56, 5),
	(8, 1, 10, 6),
	(15, 1, 26, 6),
	(21, 1, 8, 6),
	(41, 1, 15, 6),
	(45, 1, 38, 6),
	(72, 1, 15, 6),
	(9, 1, 10, 7),
	(25, 1, 8, 7),
	(46, 1, 38, 7),
	(50, 1, 26, 7),
	(87, 1, 15, 7),
	(10, 1, 10, 8),
	(26, 1, 8, 8),
	(47, 1, 38, 8),
	(63, 1, 26, 8),
	(11, 1, 10, 9),
	(29, 1, 8, 9),
	(48, 1, 38, 9),
	(12, 1, 10, 10),
	(49, 1, 38, 10),
	(63, 1, 8, 10),
	(14, 1, 10, 11),
	(51, 1, 38, 11),
	(86, 1, 8, 11),
	(15, 1, 10, 12),
	(52, 1, 38, 12),
	(88, 1, 8, 12),
	(16, 1, 10, 13),
	(53, 1, 38, 13),
	(18, 1, 10, 14),
	(54, 1, 38, 14),
	(19, 1, 10, 15),
	(55, 1, 38, 15),
	(20, 1, 10, 16),
	(56, 1, 38, 16),
	(21, 1, 10, 17),
	(57, 1, 38, 17),
	(22, 1, 10, 18),
	(58, 1, 38, 18),
	(23, 1, 10, 19),
	(59, 1, 38, 19),
	(24, 1, 10, 20),
	(60, 1, 38, 20),
	(25, 1, 10, 21),
	(61, 1, 38, 21),
	(26, 1, 10, 22),
	(62, 1, 38, 22),
	(28, 1, 10, 23),
	(29, 1, 10, 24),
	(37, 1, 10, 25),
	(70, 1, 10, 26),
	(38, 1, 10, 27),
	(39, 1, 10, 28),
	(63, 1, 10, 29),
	(71, 1, 10, 30),
	(65, 1, 10, 31),
	(72, 1, 10, 32),
	(67, 1, 10, 33),
	(66, 1, 10, 34),
	(73, 1, 10, 35),
	(74, 1, 10, 36),
	(76, 1, 10, 37),
	(77, 1, 10, 38),
	(78, 1, 10, 39),
	(79, 1, 10, 40),
	(80, 1, 10, 41),
	(81, 1, 10, 42),
	(82, 1, 10, 43),
	(83, 1, 10, 44),
	(84, 1, 10, 45),
	(85, 1, 10, 46),
	(5, 1, 10, 47),
	(86, 1, 10, 48),
	(87, 1, 10, 49),
	(88, 1, 10, 50),
	(27, 1, 10, 51);
/*!40000 ALTER TABLE `ps_hook_module` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_hook_module_exceptions
DROP TABLE IF EXISTS `ps_hook_module_exceptions`;
CREATE TABLE IF NOT EXISTS `ps_hook_module_exceptions` (
  `id_hook_module_exceptions` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_module` int(10) unsigned NOT NULL,
  `id_hook` int(10) unsigned NOT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_hook_module_exceptions`),
  KEY `id_module` (`id_module`),
  KEY `id_hook` (`id_hook`)
) ENGINE=InnoDB AUTO_INCREMENT=328 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_hook_module_exceptions: ~322 rows (approximately)
/*!40000 ALTER TABLE `ps_hook_module_exceptions` DISABLE KEYS */;
INSERT INTO `ps_hook_module_exceptions` (`id_hook_module_exceptions`, `id_shop`, `id_module`, `id_hook`, `file_name`) VALUES
	(1, 1, 4, 8, 'category'),
	(2, 1, 16, 8, 'category'),
	(4, 1, 21, 8, 'category'),
	(5, 1, 25, 8, 'category'),
	(8, 1, 74, 9, 'compare'),
	(9, 1, 74, 9, 'orderconfirmation'),
	(10, 1, 74, 9, 'pdforderslip'),
	(11, 1, 74, 9, 'module-leoblog-AdminLeoblogCategories'),
	(12, 1, 74, 9, 'module-blocknewsletter-verification'),
	(13, 1, 74, 9, 'address'),
	(14, 1, 74, 9, 'discount'),
	(15, 1, 74, 9, 'orderfollow'),
	(16, 1, 74, 9, 'product'),
	(17, 1, 74, 9, 'module-leoblog-AdminLeoblogDashboard'),
	(18, 1, 74, 9, 'module-cheque-validation'),
	(19, 1, 74, 9, 'attachment'),
	(20, 1, 74, 9, 'guesttracking'),
	(21, 1, 74, 9, 'orderreturn'),
	(22, 1, 74, 9, 'sitemap'),
	(23, 1, 74, 9, 'module-gamification-AdminGamificationController'),
	(24, 1, 74, 9, 'module-ptsmegamenu-widget'),
	(25, 1, 74, 9, 'bestsales'),
	(26, 1, 74, 9, 'identity'),
	(27, 1, 74, 9, 'pagenotfound'),
	(28, 1, 74, 9, 'stores'),
	(29, 1, 74, 9, 'module-bankwire-validation'),
	(30, 1, 74, 9, 'module-blockwishlist-mywishlist'),
	(31, 1, 74, 9, 'category'),
	(32, 1, 74, 9, 'myaccount'),
	(33, 1, 74, 9, 'password'),
	(34, 1, 74, 9, 'module-blockcategories-AdminBlockCategoriesController'),
	(35, 1, 74, 9, 'module-leoblog-category'),
	(36, 1, 74, 9, 'cms'),
	(37, 1, 74, 9, 'order'),
	(38, 1, 74, 9, 'pdforderreturn'),
	(39, 1, 74, 9, 'module-leoblog-AdminLeoblogBlogs'),
	(40, 1, 74, 9, 'module-leosliderlayer-preview'),
	(41, 1, 74, 9, 'contact'),
	(42, 1, 74, 9, 'orderdetail'),
	(43, 1, 74, 9, 'pricesdrop'),
	(44, 1, 74, 9, 'module-leoblog-AdminLeoblogComments'),
	(45, 1, 74, 9, 'module-cheque-payment'),
	(46, 1, 74, 9, 'addresses'),
	(47, 1, 74, 9, 'getfile'),
	(48, 1, 74, 9, 'orderopc'),
	(49, 1, 74, 9, 'search'),
	(50, 1, 74, 9, 'module-leosliderlayer-AdminLeoSliderLayer'),
	(51, 1, 74, 9, 'module-productcomments-default'),
	(52, 1, 74, 9, 'auth'),
	(53, 1, 74, 9, 'history'),
	(54, 1, 74, 9, 'orderslip'),
	(55, 1, 74, 9, 'statistics'),
	(56, 1, 74, 9, 'module-bankwire-payment'),
	(57, 1, 74, 9, 'module-ptsverticalmenu-widget'),
	(58, 1, 74, 9, 'cart'),
	(59, 1, 74, 9, 'manufacturer'),
	(60, 1, 74, 9, 'parentorder'),
	(61, 1, 74, 9, 'supplier'),
	(62, 1, 74, 9, 'module-leoblog-blog'),
	(63, 1, 74, 9, 'mod'),
	(64, 1, 77, 157, 'module-referralprogram-rules'),
	(65, 1, 77, 157, 'auth'),
	(66, 1, 77, 157, 'module-cheque-validation'),
	(67, 1, 77, 157, 'history'),
	(68, 1, 77, 157, 'orderslip'),
	(69, 1, 77, 157, 'statistics'),
	(70, 1, 77, 157, 'module-ptspagebuilder-AdminPtspagebuilderFooter'),
	(71, 1, 77, 157, 'module-favoriteproducts-actions'),
	(72, 1, 77, 157, 'cart'),
	(73, 1, 77, 157, 'module-ptsmegamenu-widget'),
	(74, 1, 77, 157, 'manufacturer'),
	(75, 1, 77, 157, 'parentorder'),
	(76, 1, 77, 157, 'supplier'),
	(77, 1, 77, 157, 'module-ptspagebuilder-AdminPtspagebuilderProfile'),
	(78, 1, 77, 157, 'module-leoblog-category'),
	(79, 1, 77, 157, 'changecurrency'),
	(80, 1, 77, 157, 'module-blockwishlist-mywishlist'),
	(81, 1, 77, 157, 'newproducts'),
	(82, 1, 77, 157, 'pdfinvoice'),
	(83, 1, 77, 157, 'module-dashgoals-AdminDashgoalsController'),
	(84, 1, 77, 157, 'module-bankwire-validation'),
	(85, 1, 77, 157, 'module-leosliderlayer-preview'),
	(86, 1, 77, 157, 'compare'),
	(87, 1, 77, 157, 'orderconfirmation'),
	(88, 1, 77, 157, 'pdforderslip'),
	(89, 1, 77, 157, 'module-leoblog-AdminLeoblogCategories'),
	(90, 1, 77, 157, 'module-loyalty-default'),
	(91, 1, 77, 157, 'address'),
	(92, 1, 77, 157, 'module-mailalerts-actions'),
	(93, 1, 77, 157, 'discount'),
	(94, 1, 77, 157, 'orderfollow'),
	(95, 1, 77, 157, 'product'),
	(96, 1, 77, 157, 'module-leoblog-AdminLeoblogDashboard'),
	(97, 1, 77, 157, 'module-referralprogram-program'),
	(98, 1, 77, 157, 'attachment'),
	(99, 1, 77, 157, 'module-cheque-payment'),
	(100, 1, 77, 157, 'guesttracking'),
	(101, 1, 77, 157, 'orderreturn'),
	(102, 1, 77, 157, 'sitemap'),
	(103, 1, 77, 157, 'module-gamification-AdminGamificationController'),
	(104, 1, 77, 157, 'module-favoriteproducts-account'),
	(105, 1, 77, 157, 'bestsales'),
	(106, 1, 77, 157, 'module-productcomments-default'),
	(107, 1, 77, 157, 'identity'),
	(108, 1, 77, 157, 'pagenotfound'),
	(109, 1, 77, 157, 'stores'),
	(110, 1, 77, 157, 'module-ptspagebuilder-AdminPtspagebuilderImage'),
	(111, 1, 77, 157, 'module-'),
	(112, 1, 83, 160, 'orderfollow'),
	(113, 1, 83, 160, 'contact'),
	(114, 1, 83, 160, 'product'),
	(115, 1, 83, 160, 'orderdetail'),
	(116, 1, 83, 160, 'module-loyalty-default'),
	(117, 1, 83, 160, 'module-leoblog-AdminLeoblogDashboard'),
	(118, 1, 83, 160, 'module-referralprogram-program'),
	(119, 1, 83, 160, 'pricesdrop'),
	(120, 1, 83, 160, 'module-mailalerts-actions'),
	(121, 1, 83, 160, 'module-leoblog-AdminLeoblogComments'),
	(122, 1, 83, 160, 'attachment'),
	(123, 1, 83, 160, 'module-cheque-payment'),
	(124, 1, 83, 160, 'guesttracking'),
	(125, 1, 83, 160, 'addresses'),
	(126, 1, 83, 160, 'orderreturn'),
	(127, 1, 83, 160, 'getfile'),
	(128, 1, 83, 160, 'sitemap'),
	(129, 1, 83, 160, 'orderopc'),
	(130, 1, 83, 160, 'module-referralprogram-program'),
	(131, 1, 83, 160, 'module-gamification-AdminGamificationController'),
	(132, 1, 83, 160, 'module-favoriteproducts-account'),
	(133, 1, 83, 160, 'search'),
	(134, 1, 83, 160, 'module-cheque-payment'),
	(135, 1, 83, 160, 'module-leosliderlayer-AdminLeoSliderLayer'),
	(136, 1, 83, 160, 'bestsales'),
	(137, 1, 83, 160, 'module-productcomments-default'),
	(138, 1, 83, 160, 'identity'),
	(139, 1, 83, 160, 'auth'),
	(140, 1, 83, 160, 'pagenotfound'),
	(141, 1, 83, 160, 'history'),
	(142, 1, 83, 160, 'stores'),
	(143, 1, 83, 160, 'orderslip'),
	(144, 1, 83, 160, 'module-favoriteproducts-account'),
	(145, 1, 83, 160, 'module-ptspagebuilder-AdminPtspagebuilderImage'),
	(146, 1, 83, 160, 'module-productcomments-default'),
	(147, 1, 83, 160, 'module-leoblog-blog'),
	(148, 1, 83, 160, 'statistics'),
	(149, 1, 83, 160, 'module-ptspagebuilder-AdminPtspagebuilderFooter'),
	(150, 1, 83, 160, 'category'),
	(151, 1, 83, 160, 'module-ptsverticalmenu-widget'),
	(152, 1, 83, 160, 'myaccount'),
	(153, 1, 83, 160, 'cart'),
	(154, 1, 83, 160, 'password'),
	(155, 1, 83, 160, 'manufacturer'),
	(156, 1, 83, 160, 'module-blockcategories-AdminBlockCategoriesController'),
	(157, 1, 83, 160, 'module-bankwire-payment'),
	(158, 1, 83, 160, 'parentorder'),
	(159, 1, 83, 160, 'module-leoblog-blog'),
	(160, 1, 83, 160, 'module-ptsverticalmenu-widge'),
	(161, 1, 4, 8, 'index'),
	(162, 1, 4, 8, 'category'),
	(163, 1, 16, 8, 'category'),
	(165, 1, 21, 8, 'category'),
	(166, 1, 25, 8, 'category'),
	(168, 1, 74, 9, 'compare'),
	(169, 1, 74, 9, 'orderconfirmation'),
	(170, 1, 74, 9, 'pdforderslip'),
	(171, 1, 74, 9, 'module-leoblog-AdminLeoblogCategories'),
	(172, 1, 74, 9, 'module-blocknewsletter-verification'),
	(173, 1, 74, 9, 'address'),
	(174, 1, 74, 9, 'discount'),
	(175, 1, 74, 9, 'orderfollow'),
	(176, 1, 74, 9, 'product'),
	(177, 1, 74, 9, 'module-leoblog-AdminLeoblogDashboard'),
	(178, 1, 74, 9, 'module-cheque-validation'),
	(179, 1, 74, 9, 'attachment'),
	(180, 1, 74, 9, 'guesttracking'),
	(181, 1, 74, 9, 'orderreturn'),
	(182, 1, 74, 9, 'sitemap'),
	(183, 1, 74, 9, 'module-gamification-AdminGamificationController'),
	(184, 1, 74, 9, 'module-ptsmegamenu-widget'),
	(185, 1, 74, 9, 'bestsales'),
	(186, 1, 74, 9, 'identity'),
	(187, 1, 74, 9, 'pagenotfound'),
	(188, 1, 74, 9, 'stores'),
	(189, 1, 74, 9, 'module-bankwire-validation'),
	(190, 1, 74, 9, 'module-blockwishlist-mywishlist'),
	(191, 1, 74, 9, 'category'),
	(192, 1, 74, 9, 'myaccount'),
	(193, 1, 74, 9, 'password'),
	(194, 1, 74, 9, 'module-blockcategories-AdminBlockCategoriesController'),
	(195, 1, 74, 9, 'module-leoblog-category'),
	(196, 1, 74, 9, 'cms'),
	(197, 1, 74, 9, 'order'),
	(198, 1, 74, 9, 'pdforderreturn'),
	(199, 1, 74, 9, 'module-leoblog-AdminLeoblogBlogs'),
	(200, 1, 74, 9, 'module-leosliderlayer-preview'),
	(201, 1, 74, 9, 'contact'),
	(202, 1, 74, 9, 'orderdetail'),
	(203, 1, 74, 9, 'pricesdrop'),
	(204, 1, 74, 9, 'module-leoblog-AdminLeoblogComments'),
	(205, 1, 74, 9, 'module-cheque-payment'),
	(206, 1, 74, 9, 'addresses'),
	(207, 1, 74, 9, 'getfile'),
	(208, 1, 74, 9, 'orderopc'),
	(209, 1, 74, 9, 'search'),
	(210, 1, 74, 9, 'module-leosliderlayer-AdminLeoSliderLayer'),
	(211, 1, 74, 9, 'module-productcomments-default'),
	(212, 1, 74, 9, 'auth'),
	(213, 1, 74, 9, 'history'),
	(214, 1, 74, 9, 'orderslip'),
	(215, 1, 74, 9, 'statistics'),
	(216, 1, 74, 9, 'module-bankwire-payment'),
	(217, 1, 74, 9, 'module-ptsverticalmenu-widget'),
	(218, 1, 74, 9, 'cart'),
	(219, 1, 74, 9, 'manufacturer'),
	(220, 1, 74, 9, 'parentorder'),
	(221, 1, 74, 9, 'supplier'),
	(222, 1, 74, 9, 'module-leoblog-blog'),
	(223, 1, 74, 9, 'mod'),
	(224, 1, 77, 157, 'module-referralprogram-rules'),
	(225, 1, 77, 157, 'auth'),
	(226, 1, 77, 157, 'module-cheque-validation'),
	(227, 1, 77, 157, 'history'),
	(228, 1, 77, 157, 'orderslip'),
	(229, 1, 77, 157, 'statistics'),
	(230, 1, 77, 157, 'module-ptspagebuilder-AdminPtspagebuilderFooter'),
	(231, 1, 77, 157, 'module-favoriteproducts-actions'),
	(232, 1, 77, 157, 'cart'),
	(233, 1, 77, 157, 'module-ptsmegamenu-widget'),
	(234, 1, 77, 157, 'manufacturer'),
	(235, 1, 77, 157, 'parentorder'),
	(236, 1, 77, 157, 'supplier'),
	(237, 1, 77, 157, 'module-ptspagebuilder-AdminPtspagebuilderProfile'),
	(238, 1, 77, 157, 'module-leoblog-category'),
	(239, 1, 77, 157, 'changecurrency'),
	(240, 1, 77, 157, 'module-blockwishlist-mywishlist'),
	(241, 1, 77, 157, 'newproducts'),
	(242, 1, 77, 157, 'pdfinvoice'),
	(243, 1, 77, 157, 'module-dashgoals-AdminDashgoalsController'),
	(244, 1, 77, 157, 'module-bankwire-validation'),
	(245, 1, 77, 157, 'module-leosliderlayer-preview'),
	(246, 1, 77, 157, 'compare'),
	(247, 1, 77, 157, 'orderconfirmation'),
	(248, 1, 77, 157, 'pdforderslip'),
	(249, 1, 77, 157, 'module-leoblog-AdminLeoblogCategories'),
	(250, 1, 77, 157, 'module-loyalty-default'),
	(251, 1, 77, 157, 'address'),
	(252, 1, 77, 157, 'module-mailalerts-actions'),
	(253, 1, 77, 157, 'discount'),
	(254, 1, 77, 157, 'orderfollow'),
	(255, 1, 77, 157, 'product'),
	(256, 1, 77, 157, 'module-leoblog-AdminLeoblogDashboard'),
	(257, 1, 77, 157, 'module-referralprogram-program'),
	(258, 1, 77, 157, 'attachment'),
	(259, 1, 77, 157, 'module-cheque-payment'),
	(260, 1, 77, 157, 'guesttracking'),
	(261, 1, 77, 157, 'orderreturn'),
	(262, 1, 77, 157, 'sitemap'),
	(263, 1, 77, 157, 'module-gamification-AdminGamificationController'),
	(264, 1, 77, 157, 'module-favoriteproducts-account'),
	(265, 1, 77, 157, 'bestsales'),
	(266, 1, 77, 157, 'module-productcomments-default'),
	(267, 1, 77, 157, 'identity'),
	(268, 1, 77, 157, 'pagenotfound'),
	(269, 1, 77, 157, 'stores'),
	(270, 1, 77, 157, 'module-ptspagebuilder-AdminPtspagebuilderImage'),
	(271, 1, 77, 157, 'module-'),
	(272, 1, 83, 160, 'orderfollow'),
	(273, 1, 83, 160, 'contact'),
	(274, 1, 83, 160, 'product'),
	(275, 1, 83, 160, 'orderdetail'),
	(276, 1, 83, 160, 'module-loyalty-default'),
	(277, 1, 83, 160, 'module-leoblog-AdminLeoblogDashboard'),
	(278, 1, 83, 160, 'module-referralprogram-program'),
	(279, 1, 83, 160, 'pricesdrop'),
	(280, 1, 83, 160, 'module-mailalerts-actions'),
	(281, 1, 83, 160, 'module-leoblog-AdminLeoblogComments'),
	(282, 1, 83, 160, 'attachment'),
	(283, 1, 83, 160, 'module-cheque-payment'),
	(284, 1, 83, 160, 'guesttracking'),
	(285, 1, 83, 160, 'addresses'),
	(286, 1, 83, 160, 'orderreturn'),
	(287, 1, 83, 160, 'getfile'),
	(288, 1, 83, 160, 'sitemap'),
	(289, 1, 83, 160, 'orderopc'),
	(290, 1, 83, 160, 'module-referralprogram-program'),
	(291, 1, 83, 160, 'module-gamification-AdminGamificationController'),
	(292, 1, 83, 160, 'module-favoriteproducts-account'),
	(293, 1, 83, 160, 'search'),
	(294, 1, 83, 160, 'module-cheque-payment'),
	(295, 1, 83, 160, 'module-leosliderlayer-AdminLeoSliderLayer'),
	(296, 1, 83, 160, 'bestsales'),
	(297, 1, 83, 160, 'module-productcomments-default'),
	(298, 1, 83, 160, 'identity'),
	(299, 1, 83, 160, 'auth'),
	(300, 1, 83, 160, 'pagenotfound'),
	(301, 1, 83, 160, 'history'),
	(302, 1, 83, 160, 'stores'),
	(303, 1, 83, 160, 'orderslip'),
	(304, 1, 83, 160, 'module-favoriteproducts-account'),
	(305, 1, 83, 160, 'module-ptspagebuilder-AdminPtspagebuilderImage'),
	(306, 1, 83, 160, 'module-productcomments-default'),
	(307, 1, 83, 160, 'module-leoblog-blog'),
	(308, 1, 83, 160, 'statistics'),
	(309, 1, 83, 160, 'module-ptspagebuilder-AdminPtspagebuilderFooter'),
	(310, 1, 83, 160, 'category'),
	(311, 1, 83, 160, 'module-ptsverticalmenu-widget'),
	(312, 1, 83, 160, 'myaccount'),
	(313, 1, 83, 160, 'cart'),
	(314, 1, 83, 160, 'password'),
	(315, 1, 83, 160, 'manufacturer'),
	(316, 1, 83, 160, 'module-blockcategories-AdminBlockCategoriesController'),
	(317, 1, 83, 160, 'module-bankwire-payment'),
	(318, 1, 83, 160, 'parentorder'),
	(319, 1, 83, 160, 'module-leoblog-blog'),
	(320, 1, 83, 160, 'module-ptsverticalmenu-widge'),
	(321, 1, 4, 8, 'index'),
	(322, 1, 4, 8, 'category'),
	(323, 1, 16, 8, 'category'),
	(325, 1, 21, 8, 'category'),
	(326, 1, 25, 8, 'category');
/*!40000 ALTER TABLE `ps_hook_module_exceptions` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_image
DROP TABLE IF EXISTS `ps_image`;
CREATE TABLE IF NOT EXISTS `ps_image` (
  `id_image` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_product` int(10) unsigned NOT NULL,
  `position` smallint(2) unsigned NOT NULL DEFAULT '0',
  `cover` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_image`),
  UNIQUE KEY `idx_product_image` (`id_image`,`id_product`,`cover`),
  KEY `image_product` (`id_product`),
  KEY `id_product_cover` (`id_product`,`cover`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_image: ~23 rows (approximately)
/*!40000 ALTER TABLE `ps_image` DISABLE KEYS */;
INSERT INTO `ps_image` (`id_image`, `id_product`, `position`, `cover`) VALUES
	(1, 1, 1, 1),
	(2, 1, 2, 0),
	(3, 1, 3, 0),
	(4, 1, 4, 0),
	(5, 2, 1, 0),
	(6, 2, 2, 0),
	(7, 2, 3, 1),
	(8, 3, 1, 1),
	(9, 3, 2, 0),
	(10, 4, 1, 1),
	(11, 4, 2, 0),
	(12, 5, 1, 1),
	(13, 5, 2, 0),
	(14, 5, 3, 0),
	(15, 5, 4, 0),
	(16, 6, 1, 1),
	(17, 6, 2, 0),
	(18, 6, 3, 0),
	(19, 6, 4, 0),
	(20, 7, 1, 1),
	(21, 7, 2, 0),
	(22, 7, 3, 0),
	(23, 7, 4, 0);
/*!40000 ALTER TABLE `ps_image` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_image_lang
DROP TABLE IF EXISTS `ps_image_lang`;
CREATE TABLE IF NOT EXISTS `ps_image_lang` (
  `id_image` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `legend` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_image`,`id_lang`),
  KEY `id_image` (`id_image`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_image_lang: ~23 rows (approximately)
/*!40000 ALTER TABLE `ps_image_lang` DISABLE KEYS */;
INSERT INTO `ps_image_lang` (`id_image`, `id_lang`, `legend`) VALUES
	(1, 1, ''),
	(2, 1, ''),
	(3, 1, ''),
	(4, 1, ''),
	(5, 1, ''),
	(6, 1, ''),
	(7, 1, ''),
	(8, 1, ''),
	(9, 1, ''),
	(10, 1, ''),
	(11, 1, ''),
	(12, 1, ''),
	(13, 1, ''),
	(14, 1, ''),
	(15, 1, ''),
	(16, 1, ''),
	(17, 1, ''),
	(18, 1, ''),
	(19, 1, ''),
	(20, 1, ''),
	(21, 1, ''),
	(22, 1, ''),
	(23, 1, '');
/*!40000 ALTER TABLE `ps_image_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_image_shop
DROP TABLE IF EXISTS `ps_image_shop`;
CREATE TABLE IF NOT EXISTS `ps_image_shop` (
  `id_image` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  `cover` tinyint(1) NOT NULL DEFAULT '0',
  KEY `id_image` (`id_image`,`id_shop`,`cover`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_image_shop: ~23 rows (approximately)
/*!40000 ALTER TABLE `ps_image_shop` DISABLE KEYS */;
INSERT INTO `ps_image_shop` (`id_image`, `id_shop`, `cover`) VALUES
	(1, 1, 1),
	(2, 1, 0),
	(3, 1, 0),
	(4, 1, 0),
	(5, 1, 0),
	(6, 1, 0),
	(7, 1, 1),
	(8, 1, 1),
	(9, 1, 0),
	(10, 1, 1),
	(11, 1, 0),
	(12, 1, 1),
	(13, 1, 0),
	(14, 1, 0),
	(15, 1, 0),
	(16, 1, 1),
	(17, 1, 0),
	(18, 1, 0),
	(19, 1, 0),
	(20, 1, 1),
	(21, 1, 0),
	(22, 1, 0),
	(23, 1, 0);
/*!40000 ALTER TABLE `ps_image_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_image_type
DROP TABLE IF EXISTS `ps_image_type`;
CREATE TABLE IF NOT EXISTS `ps_image_type` (
  `id_image_type` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `width` int(10) unsigned NOT NULL,
  `height` int(10) unsigned NOT NULL,
  `products` tinyint(1) NOT NULL DEFAULT '1',
  `categories` tinyint(1) NOT NULL DEFAULT '1',
  `manufacturers` tinyint(1) NOT NULL DEFAULT '1',
  `suppliers` tinyint(1) NOT NULL DEFAULT '1',
  `scenes` tinyint(1) NOT NULL DEFAULT '1',
  `stores` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_image_type`),
  KEY `image_type_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_image_type: ~11 rows (approximately)
/*!40000 ALTER TABLE `ps_image_type` DISABLE KEYS */;
INSERT INTO `ps_image_type` (`id_image_type`, `name`, `width`, `height`, `products`, `categories`, `manufacturers`, `suppliers`, `scenes`, `stores`) VALUES
	(10, 'logo_brand', 150, 50, 0, 0, 1, 0, 0, 1),
	(11, 'banner_category', 260, 290, 0, 1, 0, 0, 0, 1),
	(12, 'cart_default', 135, 143, 1, 0, 0, 0, 0, 1),
	(13, 'small_default', 135, 143, 1, 0, 0, 1, 0, 1),
	(14, 'medium_default', 200, 211, 1, 1, 1, 1, 0, 1),
	(15, 'home_default', 450, 475, 1, 0, 0, 0, 0, 1),
	(16, 'large_default', 450, 475, 1, 0, 0, 1, 0, 1),
	(17, 'thickbox_default', 900, 950, 1, 0, 0, 0, 0, 1),
	(18, 'category_default', 870, 240, 0, 1, 0, 0, 0, 1),
	(19, 'scene_default', 450, 475, 0, 0, 0, 0, 1, 1),
	(20, 'm_scene_default', 160, 169, 0, 0, 0, 0, 1, 1);
/*!40000 ALTER TABLE `ps_image_type` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_import_match
DROP TABLE IF EXISTS `ps_import_match`;
CREATE TABLE IF NOT EXISTS `ps_import_match` (
  `id_import_match` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `match` text NOT NULL,
  `skip` int(2) NOT NULL,
  PRIMARY KEY (`id_import_match`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_import_match: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_import_match` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_import_match` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_info
DROP TABLE IF EXISTS `ps_info`;
CREATE TABLE IF NOT EXISTS `ps_info` (
  `id_info` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_info`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_info: ~2 rows (approximately)
/*!40000 ALTER TABLE `ps_info` DISABLE KEYS */;
INSERT INTO `ps_info` (`id_info`, `id_shop`) VALUES
	(1, 1),
	(2, 1);
/*!40000 ALTER TABLE `ps_info` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_info_lang
DROP TABLE IF EXISTS `ps_info_lang`;
CREATE TABLE IF NOT EXISTS `ps_info_lang` (
  `id_info` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id_info`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_info_lang: ~2 rows (approximately)
/*!40000 ALTER TABLE `ps_info_lang` DISABLE KEYS */;
INSERT INTO `ps_info_lang` (`id_info`, `id_lang`, `text`) VALUES
	(1, 1, '<ul>\n<li><em class="icon-truck" id="icon-truck"></em>\n<div class="type-text">\n<h3>Lorem Ipsum</h3>\n<p>Lorem ipsum dolor sit amet conse ctetur voluptate velit esse cillum dolore eu</p>\n</div>\n</li>\n<li><em class="icon-phone" id="icon-phone"></em>\n<div class="type-text">\n<h3>Dolor Sit Amet</h3>\n<p>Lorem ipsum dolor sit amet conse ctetur voluptate velit esse cillum dolore eu</p>\n</div>\n</li>\n<li><em class="icon-credit-card" id="icon-credit-card"></em>\n<div class="type-text">\n<h3>Ctetur Voluptate</h3>\n<p>Lorem ipsum dolor sit amet conse ctetur voluptate velit esse cillum dolore eu</p>\n</div>\n</li>\n</ul>'),
	(2, 1, '<h3>Custom Block</h3>\n<p><strong class="dark">Lorem ipsum dolor sit amet conse ctetu</strong></p>\n<p>Sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.</p>');
/*!40000 ALTER TABLE `ps_info_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_lang
DROP TABLE IF EXISTS `ps_lang`;
CREATE TABLE IF NOT EXISTS `ps_lang` (
  `id_lang` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `active` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `iso_code` char(2) NOT NULL,
  `language_code` char(5) NOT NULL,
  `date_format_lite` char(32) NOT NULL DEFAULT 'Y-m-d',
  `date_format_full` char(32) NOT NULL DEFAULT 'Y-m-d H:i:s',
  `is_rtl` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_lang`),
  KEY `lang_iso_code` (`iso_code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_lang: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_lang` DISABLE KEYS */;
INSERT INTO `ps_lang` (`id_lang`, `name`, `active`, `iso_code`, `language_code`, `date_format_lite`, `date_format_full`, `is_rtl`) VALUES
	(1, 'Español (Spanish)', 1, 'es', 'es-es', 'd/m/Y', 'd/m/Y H:i:s', 0);
/*!40000 ALTER TABLE `ps_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_lang_shop
DROP TABLE IF EXISTS `ps_lang_shop`;
CREATE TABLE IF NOT EXISTS `ps_lang_shop` (
  `id_lang` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_lang`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_lang_shop: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_lang_shop` DISABLE KEYS */;
INSERT INTO `ps_lang_shop` (`id_lang`, `id_shop`) VALUES
	(1, 1);
/*!40000 ALTER TABLE `ps_lang_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_layered_category
DROP TABLE IF EXISTS `ps_layered_category`;
CREATE TABLE IF NOT EXISTS `ps_layered_category` (
  `id_layered_category` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(11) unsigned NOT NULL,
  `id_category` int(10) unsigned NOT NULL,
  `id_value` int(10) unsigned DEFAULT '0',
  `type` enum('category','id_feature','id_attribute_group','quantity','condition','manufacturer','weight','price') NOT NULL,
  `position` int(10) unsigned NOT NULL,
  `filter_type` int(10) unsigned NOT NULL DEFAULT '0',
  `filter_show_limit` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_layered_category`),
  KEY `id_category` (`id_category`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=latin1;

-- Dumping data for table celiodev.ps_layered_category: ~90 rows (approximately)
/*!40000 ALTER TABLE `ps_layered_category` DISABLE KEYS */;
INSERT INTO `ps_layered_category` (`id_layered_category`, `id_shop`, `id_category`, `id_value`, `type`, `position`, `filter_type`, `filter_show_limit`) VALUES
	(1, 1, 2, NULL, 'category', 1, 0, 0),
	(2, 1, 2, NULL, 'price', 2, 0, 0),
	(3, 1, 2, 1, 'id_attribute_group', 3, 0, 0),
	(4, 1, 2, 3, 'id_attribute_group', 4, 0, 0),
	(5, 1, 2, 5, 'id_feature', 5, 0, 0),
	(6, 1, 2, 6, 'id_feature', 6, 0, 0),
	(7, 1, 3, NULL, 'category', 1, 0, 0),
	(8, 1, 3, NULL, 'price', 2, 0, 0),
	(9, 1, 3, 1, 'id_attribute_group', 3, 0, 0),
	(10, 1, 3, 3, 'id_attribute_group', 4, 0, 0),
	(11, 1, 3, 5, 'id_feature', 5, 0, 0),
	(12, 1, 3, 6, 'id_feature', 6, 0, 0),
	(13, 1, 4, NULL, 'category', 1, 0, 0),
	(14, 1, 4, NULL, 'price', 2, 0, 0),
	(15, 1, 4, 1, 'id_attribute_group', 3, 0, 0),
	(16, 1, 4, 3, 'id_attribute_group', 4, 0, 0),
	(17, 1, 4, 5, 'id_feature', 5, 0, 0),
	(18, 1, 4, 6, 'id_feature', 6, 0, 0),
	(19, 1, 5, NULL, 'category', 1, 0, 0),
	(20, 1, 5, NULL, 'price', 2, 0, 0),
	(21, 1, 5, 1, 'id_attribute_group', 3, 0, 0),
	(22, 1, 5, 3, 'id_attribute_group', 4, 0, 0),
	(23, 1, 5, 5, 'id_feature', 5, 0, 0),
	(24, 1, 5, 6, 'id_feature', 6, 0, 0),
	(25, 1, 7, NULL, 'category', 1, 0, 0),
	(26, 1, 7, NULL, 'price', 2, 0, 0),
	(27, 1, 7, 1, 'id_attribute_group', 3, 0, 0),
	(28, 1, 7, 3, 'id_attribute_group', 4, 0, 0),
	(29, 1, 7, 5, 'id_feature', 5, 0, 0),
	(30, 1, 7, 6, 'id_feature', 6, 0, 0),
	(31, 1, 8, NULL, 'category', 1, 0, 0),
	(32, 1, 8, NULL, 'price', 2, 0, 0),
	(33, 1, 8, 1, 'id_attribute_group', 3, 0, 0),
	(34, 1, 8, 3, 'id_attribute_group', 4, 0, 0),
	(35, 1, 8, 5, 'id_feature', 5, 0, 0),
	(36, 1, 8, 6, 'id_feature', 6, 0, 0),
	(37, 1, 9, NULL, 'category', 1, 0, 0),
	(38, 1, 9, NULL, 'price', 2, 0, 0),
	(39, 1, 9, 1, 'id_attribute_group', 3, 0, 0),
	(40, 1, 9, 3, 'id_attribute_group', 4, 0, 0),
	(41, 1, 9, 5, 'id_feature', 5, 0, 0),
	(42, 1, 9, 6, 'id_feature', 6, 0, 0),
	(43, 1, 10, NULL, 'category', 1, 0, 0),
	(44, 1, 10, NULL, 'price', 2, 0, 0),
	(45, 1, 10, 1, 'id_attribute_group', 3, 0, 0),
	(46, 1, 10, 3, 'id_attribute_group', 4, 0, 0),
	(47, 1, 10, 5, 'id_feature', 5, 0, 0),
	(48, 1, 10, 6, 'id_feature', 6, 0, 0),
	(49, 1, 16, NULL, 'category', 1, 0, 0),
	(50, 1, 16, NULL, 'price', 2, 0, 0),
	(51, 1, 16, 1, 'id_attribute_group', 3, 0, 0),
	(52, 1, 16, 3, 'id_attribute_group', 4, 0, 0),
	(53, 1, 16, 5, 'id_feature', 5, 0, 0),
	(54, 1, 16, 6, 'id_feature', 6, 0, 0),
	(55, 1, 17, NULL, 'category', 1, 0, 0),
	(56, 1, 17, NULL, 'price', 2, 0, 0),
	(57, 1, 17, 1, 'id_attribute_group', 3, 0, 0),
	(58, 1, 17, 3, 'id_attribute_group', 4, 0, 0),
	(59, 1, 17, 5, 'id_feature', 5, 0, 0),
	(60, 1, 17, 6, 'id_feature', 6, 0, 0),
	(61, 1, 18, NULL, 'category', 1, 0, 0),
	(62, 1, 18, NULL, 'price', 2, 0, 0),
	(63, 1, 18, 1, 'id_attribute_group', 3, 0, 0),
	(64, 1, 18, 3, 'id_attribute_group', 4, 0, 0),
	(65, 1, 18, 5, 'id_feature', 5, 0, 0),
	(66, 1, 18, 6, 'id_feature', 6, 0, 0),
	(67, 1, 19, NULL, 'category', 1, 0, 0),
	(68, 1, 19, NULL, 'price', 2, 0, 0),
	(69, 1, 19, 1, 'id_attribute_group', 3, 0, 0),
	(70, 1, 19, 3, 'id_attribute_group', 4, 0, 0),
	(71, 1, 19, 5, 'id_feature', 5, 0, 0),
	(72, 1, 19, 6, 'id_feature', 6, 0, 0),
	(73, 1, 20, NULL, 'category', 1, 0, 0),
	(74, 1, 20, NULL, 'price', 2, 0, 0),
	(75, 1, 20, 1, 'id_attribute_group', 3, 0, 0),
	(76, 1, 20, 3, 'id_attribute_group', 4, 0, 0),
	(77, 1, 20, 5, 'id_feature', 5, 0, 0),
	(78, 1, 20, 6, 'id_feature', 6, 0, 0),
	(79, 1, 21, NULL, 'category', 1, 0, 0),
	(80, 1, 21, NULL, 'price', 2, 0, 0),
	(81, 1, 21, 1, 'id_attribute_group', 3, 0, 0),
	(82, 1, 21, 3, 'id_attribute_group', 4, 0, 0),
	(83, 1, 21, 5, 'id_feature', 5, 0, 0),
	(84, 1, 21, 6, 'id_feature', 6, 0, 0),
	(85, 1, 22, NULL, 'category', 1, 0, 0),
	(86, 1, 22, NULL, 'price', 2, 0, 0),
	(87, 1, 22, 1, 'id_attribute_group', 3, 0, 0),
	(88, 1, 22, 3, 'id_attribute_group', 4, 0, 0),
	(89, 1, 22, 5, 'id_feature', 5, 0, 0),
	(90, 1, 22, 6, 'id_feature', 6, 0, 0);
/*!40000 ALTER TABLE `ps_layered_category` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_layered_filter
DROP TABLE IF EXISTS `ps_layered_filter`;
CREATE TABLE IF NOT EXISTS `ps_layered_filter` (
  `id_layered_filter` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `filters` text,
  `n_categories` int(10) unsigned NOT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_layered_filter`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_layered_filter: ~1 rows (approximately)
/*!40000 ALTER TABLE `ps_layered_filter` DISABLE KEYS */;
INSERT INTO `ps_layered_filter` (`id_layered_filter`, `name`, `filters`, `n_categories`, `date_add`) VALUES
	(1, 'Mi plantilla 2015-05-08', 'a:8:{s:10:"categories";a:15:{i:0;i:2;i:1;i:3;i:2;i:4;i:3;i:5;i:4;i:7;i:5;i:8;i:6;i:9;i:7;i:10;i:8;i:16;i:9;i:17;i:10;i:18;i:11;i:19;i:12;i:20;i:13;i:21;i:14;i:22;}s:9:"shop_list";a:1:{i:0;i:1;}s:31:"layered_selection_subcategories";a:2:{s:11:"filter_type";i:0;s:17:"filter_show_limit";i:0;}s:30:"layered_selection_price_slider";a:2:{s:11:"filter_type";i:0;s:17:"filter_show_limit";i:0;}s:22:"layered_selection_ag_1";a:2:{s:11:"filter_type";i:0;s:17:"filter_show_limit";i:0;}s:22:"layered_selection_ag_3";a:2:{s:11:"filter_type";i:0;s:17:"filter_show_limit";i:0;}s:24:"layered_selection_feat_5";a:2:{s:11:"filter_type";i:0;s:17:"filter_show_limit";i:0;}s:24:"layered_selection_feat_6";a:2:{s:11:"filter_type";i:0;s:17:"filter_show_limit";i:0;}}', 15, '2015-05-12 10:21:24');
/*!40000 ALTER TABLE `ps_layered_filter` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_layered_filter_shop
DROP TABLE IF EXISTS `ps_layered_filter_shop`;
CREATE TABLE IF NOT EXISTS `ps_layered_filter_shop` (
  `id_layered_filter` int(10) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_layered_filter`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_layered_filter_shop: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_layered_filter_shop` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_layered_filter_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_layered_friendly_url
DROP TABLE IF EXISTS `ps_layered_friendly_url`;
CREATE TABLE IF NOT EXISTS `ps_layered_friendly_url` (
  `id_layered_friendly_url` int(11) NOT NULL AUTO_INCREMENT,
  `url_key` varchar(32) NOT NULL,
  `data` varchar(200) NOT NULL,
  `id_lang` int(11) NOT NULL,
  PRIMARY KEY (`id_layered_friendly_url`),
  KEY `id_lang` (`id_lang`),
  KEY `url_key` (`url_key`(5))
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_layered_friendly_url: ~55 rows (approximately)
/*!40000 ALTER TABLE `ps_layered_friendly_url` DISABLE KEYS */;
INSERT INTO `ps_layered_friendly_url` (`id_layered_friendly_url`, `url_key`, `data`, `id_lang`) VALUES
	(1, '4357c275876fdff03d7c4cdf8f882ec5', 'a:1:{s:8:"category";a:1:{i:7;s:1:"7";}}', 1),
	(2, 'd7ca0cab13dc04f35b2c7569f0cc4228', 'a:1:{s:8:"category";a:1:{i:5;s:1:"5";}}', 1),
	(3, '7b850fefd113e4fc1b050b13b68c38d5', 'a:1:{s:8:"category";a:1:{i:2;s:1:"2";}}', 1),
	(4, '8df2d37bcbe9b28ea7596cee5ccd62ab', 'a:1:{s:8:"category";a:1:{i:3;s:1:"3";}}', 1),
	(5, '39fd71c6c16ef7ed6ed2b8ba93e276e2', 'a:1:{s:8:"category";a:1:{i:1;s:1:"1";}}', 1),
	(6, 'b2068d00a9aaf0da051d00ba0a6cc5a2', 'a:1:{s:8:"category";a:1:{i:4;s:1:"4";}}', 1),
	(7, '66e056a1955f601a970f7d9b155e0635', 'a:1:{s:8:"category";a:1:{i:8;s:1:"8";}}', 1),
	(8, '151949f29b2bfc4dededa65b8857b2f2', 'a:1:{s:8:"category";a:1:{i:10;s:2:"10";}}', 1),
	(9, '015741d0472809f158ddf2317bf057c4', 'a:1:{s:8:"category";a:1:{i:11;s:2:"11";}}', 1),
	(10, 'a60cb1416420de423170ef631ba37f05', 'a:1:{s:8:"category";a:1:{i:9;s:1:"9";}}', 1),
	(11, 'd5cbab54a3ca1d7db386ad196407764f', 'a:1:{s:10:"id_feature";a:1:{i:1;s:3:"5_1";}}', 1),
	(12, 'd3f53f3235dbe693e3a5ff6ef132f69f', 'a:1:{s:10:"id_feature";a:1:{i:2;s:3:"5_2";}}', 1),
	(13, '0932c1e2ccda9974a2aa46ffc62a74aa', 'a:1:{s:10:"id_feature";a:1:{i:3;s:3:"5_3";}}', 1),
	(14, 'fdd7832769f92635ea6cf622601bf475', 'a:1:{s:10:"id_feature";a:1:{i:4;s:3:"5_4";}}', 1),
	(15, 'a16cb39c73d3a13133a78276df4255af', 'a:1:{s:10:"id_feature";a:1:{i:5;s:3:"5_5";}}', 1),
	(16, '943b5e6999b97c9d265d190242f0a684', 'a:1:{s:10:"id_feature";a:1:{i:6;s:3:"5_6";}}', 1),
	(17, '235854e8ef39bb0df5697073d3d90540', 'a:1:{s:10:"id_feature";a:1:{i:7;s:3:"5_7";}}', 1),
	(18, '3ae17610775d27b1db8514b2ecd8a69d', 'a:1:{s:10:"id_feature";a:1:{i:8;s:3:"5_8";}}', 1),
	(19, '558e9a195562d0555553d846b5654610', 'a:1:{s:10:"id_feature";a:1:{i:9;s:3:"5_9";}}', 1),
	(20, 'e799f4d0abb0cf8927bcb60375974496', 'a:1:{s:10:"id_feature";a:1:{i:10;s:4:"6_10";}}', 1),
	(21, '46b8400d3be5c11515cb97699b8c2b5f', 'a:1:{s:10:"id_feature";a:1:{i:11;s:4:"6_11";}}', 1),
	(22, 'c41f78f59c8a3d76253f60747dd4e240', 'a:1:{s:10:"id_feature";a:1:{i:12;s:4:"6_12";}}', 1),
	(23, '6fee10826bc5822f3ff569ed13523d59', 'a:1:{s:10:"id_feature";a:1:{i:13;s:4:"6_13";}}', 1),
	(24, 'ee4452d85805c23df068b6c871ae5e81', 'a:1:{s:10:"id_feature";a:1:{i:14;s:4:"6_14";}}', 1),
	(25, 'd9fecf8bec077f0cf78ab8090cb1384b', 'a:1:{s:10:"id_feature";a:1:{i:15;s:4:"6_15";}}', 1),
	(26, 'f2299fcc01ad7282b14837db948c29d1', 'a:1:{s:10:"id_feature";a:1:{i:16;s:4:"6_16";}}', 1),
	(27, '70f2b919cd07d2eedf4d0fa36ddb2ef9', 'a:1:{s:10:"id_feature";a:1:{i:17;s:4:"7_17";}}', 1),
	(28, 'f7efe1215721d20b8c7b67357e220a43', 'a:1:{s:10:"id_feature";a:1:{i:18;s:4:"7_18";}}', 1),
	(29, '3f78db0184270fdeb169ec979846ca50', 'a:1:{s:10:"id_feature";a:1:{i:19;s:4:"7_19";}}', 1),
	(30, '936a5e0d2b18f15a7865846c9ebd2f7a', 'a:1:{s:10:"id_feature";a:1:{i:20;s:4:"7_20";}}', 1),
	(31, 'bc1a747be70119467250821b48533190', 'a:1:{s:10:"id_feature";a:1:{i:21;s:4:"7_21";}}', 1),
	(32, '97d9dd08827238b39342d37e16ee7fc3', 'a:1:{s:18:"id_attribute_group";a:1:{i:1;s:3:"1_1";}}', 1),
	(33, '2f3d5048a6335cac20241e0f8cb5294e', 'a:1:{s:18:"id_attribute_group";a:1:{i:2;s:3:"1_2";}}', 1),
	(34, '19819345209f29bb2865355fa2cdb800', 'a:1:{s:18:"id_attribute_group";a:1:{i:3;s:3:"1_3";}}', 1),
	(35, 'f00b851d158ffd7b8f4750d251caf742', 'a:1:{s:18:"id_attribute_group";a:1:{i:4;s:3:"1_4";}}', 1),
	(36, '955959be60adbc2672d9f475c80427b5', 'a:1:{s:18:"id_attribute_group";a:1:{i:5;s:3:"3_5";}}', 1),
	(37, '302b5943e4f2147546c456adf925016a', 'a:1:{s:18:"id_attribute_group";a:1:{i:6;s:3:"3_6";}}', 1),
	(38, 'f036e061c6e0e9cd6b3c463f72f524a5', 'a:1:{s:18:"id_attribute_group";a:1:{i:7;s:3:"3_7";}}', 1),
	(39, '7da361d2ac219366406c8ba83f839e49', 'a:1:{s:18:"id_attribute_group";a:1:{i:8;s:3:"3_8";}}', 1),
	(40, '8a2e3aa84a460e7eedf0a696a557f87d', 'a:1:{s:18:"id_attribute_group";a:1:{i:9;s:3:"3_9";}}', 1),
	(41, '10d4b015cd4670238f90af49853a0b09', 'a:1:{s:18:"id_attribute_group";a:1:{i:10;s:4:"3_10";}}', 1),
	(42, '3f7f5aaa6d609de3b6a2b3addd27e352', 'a:1:{s:18:"id_attribute_group";a:1:{i:11;s:4:"3_11";}}', 1),
	(43, '5f556205d67d7c26c2726dba638c2d95', 'a:1:{s:18:"id_attribute_group";a:1:{i:12;s:4:"3_12";}}', 1),
	(44, 'e51d8bd9a716af167a1e4e3c3111c597', 'a:1:{s:18:"id_attribute_group";a:1:{i:13;s:4:"3_13";}}', 1),
	(45, '95ed6e1c18ff0e1bd610a517f229f652', 'a:1:{s:18:"id_attribute_group";a:1:{i:14;s:4:"3_14";}}', 1),
	(46, '6dd5d6e16acddb5ab2d612ad65603344', 'a:1:{s:18:"id_attribute_group";a:1:{i:15;s:4:"3_15";}}', 1),
	(47, 'c63c700f59e69866b4619eef8bc6e597', 'a:1:{s:18:"id_attribute_group";a:1:{i:16;s:4:"3_16";}}', 1),
	(48, 'fe4284d073fc299122d1f83ec63488a2', 'a:1:{s:18:"id_attribute_group";a:1:{i:17;s:4:"3_17";}}', 1),
	(49, 'b7ca381eeae1441140d52502461c6a2c', 'a:1:{s:18:"id_attribute_group";a:1:{i:24;s:4:"3_24";}}', 1),
	(50, '44294b0028a0213fc55128fecfbdfbed', 'a:1:{s:8:"quantity";a:1:{i:0;i:0;}}', 1),
	(51, 'dd4f6902b4e7b3fb676b9ce2cedfa9b4', 'a:1:{s:8:"quantity";a:1:{i:0;i:1;}}', 1),
	(52, 'f84cffad3cf7a6b7856c8a72b8c8df90', 'a:1:{s:9:"condition";a:1:{s:3:"new";s:3:"new";}}', 1),
	(53, '14c9246c3ce6dd5317ffefc0545bba2e', 'a:1:{s:9:"condition";a:1:{s:4:"used";s:4:"used";}}', 1),
	(54, '9b16a88b60f3455d51212bf2dc1d3a95', 'a:1:{s:9:"condition";a:1:{s:11:"refurbished";s:11:"refurbished";}}', 1),
	(55, 'e3d5c79878834b2f61e6f37c1425bee9', 'a:1:{s:12:"manufacturer";a:1:{i:1;s:1:"1";}}', 1);
/*!40000 ALTER TABLE `ps_layered_friendly_url` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_layered_indexable_attribute_group
DROP TABLE IF EXISTS `ps_layered_indexable_attribute_group`;
CREATE TABLE IF NOT EXISTS `ps_layered_indexable_attribute_group` (
  `id_attribute_group` int(11) NOT NULL,
  `indexable` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_attribute_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_layered_indexable_attribute_group: ~3 rows (approximately)
/*!40000 ALTER TABLE `ps_layered_indexable_attribute_group` DISABLE KEYS */;
INSERT INTO `ps_layered_indexable_attribute_group` (`id_attribute_group`, `indexable`) VALUES
	(1, 1),
	(2, 1),
	(3, 1);
/*!40000 ALTER TABLE `ps_layered_indexable_attribute_group` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_layered_indexable_attribute_group_lang_value
DROP TABLE IF EXISTS `ps_layered_indexable_attribute_group_lang_value`;
CREATE TABLE IF NOT EXISTS `ps_layered_indexable_attribute_group_lang_value` (
  `id_attribute_group` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `url_name` varchar(20) DEFAULT NULL,
  `meta_title` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_attribute_group`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_layered_indexable_attribute_group_lang_value: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_layered_indexable_attribute_group_lang_value` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_layered_indexable_attribute_group_lang_value` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_layered_indexable_attribute_lang_value
DROP TABLE IF EXISTS `ps_layered_indexable_attribute_lang_value`;
CREATE TABLE IF NOT EXISTS `ps_layered_indexable_attribute_lang_value` (
  `id_attribute` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `url_name` varchar(20) DEFAULT NULL,
  `meta_title` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_attribute`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_layered_indexable_attribute_lang_value: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_layered_indexable_attribute_lang_value` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_layered_indexable_attribute_lang_value` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_layered_indexable_feature
DROP TABLE IF EXISTS `ps_layered_indexable_feature`;
CREATE TABLE IF NOT EXISTS `ps_layered_indexable_feature` (
  `id_feature` int(11) NOT NULL,
  `indexable` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_feature`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_layered_indexable_feature: ~7 rows (approximately)
/*!40000 ALTER TABLE `ps_layered_indexable_feature` DISABLE KEYS */;
INSERT INTO `ps_layered_indexable_feature` (`id_feature`, `indexable`) VALUES
	(1, 1),
	(2, 1),
	(3, 1),
	(4, 1),
	(5, 1),
	(6, 1),
	(7, 1);
/*!40000 ALTER TABLE `ps_layered_indexable_feature` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_layered_indexable_feature_lang_value
DROP TABLE IF EXISTS `ps_layered_indexable_feature_lang_value`;
CREATE TABLE IF NOT EXISTS `ps_layered_indexable_feature_lang_value` (
  `id_feature` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `url_name` varchar(20) NOT NULL,
  `meta_title` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_feature`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_layered_indexable_feature_lang_value: ~1 rows (approximately)
/*!40000 ALTER TABLE `ps_layered_indexable_feature_lang_value` DISABLE KEYS */;
INSERT INTO `ps_layered_indexable_feature_lang_value` (`id_feature`, `id_lang`, `url_name`, `meta_title`) VALUES
	(4, 1, 'peso', '');
/*!40000 ALTER TABLE `ps_layered_indexable_feature_lang_value` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_layered_indexable_feature_value_lang_value
DROP TABLE IF EXISTS `ps_layered_indexable_feature_value_lang_value`;
CREATE TABLE IF NOT EXISTS `ps_layered_indexable_feature_value_lang_value` (
  `id_feature_value` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `url_name` varchar(20) DEFAULT NULL,
  `meta_title` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_feature_value`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_layered_indexable_feature_value_lang_value: ~4 rows (approximately)
/*!40000 ALTER TABLE `ps_layered_indexable_feature_value_lang_value` DISABLE KEYS */;
INSERT INTO `ps_layered_indexable_feature_value_lang_value` (`id_feature_value`, `id_lang`, `url_name`, `meta_title`) VALUES
	(34, 1, '', ''),
	(35, 1, '', ''),
	(36, 1, '', ''),
	(37, 1, '', '');
/*!40000 ALTER TABLE `ps_layered_indexable_feature_value_lang_value` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_layered_price_index
DROP TABLE IF EXISTS `ps_layered_price_index`;
CREATE TABLE IF NOT EXISTS `ps_layered_price_index` (
  `id_product` int(11) NOT NULL,
  `id_currency` int(11) NOT NULL,
  `id_shop` int(11) NOT NULL,
  `price_min` int(11) NOT NULL,
  `price_max` int(11) NOT NULL,
  PRIMARY KEY (`id_product`,`id_currency`,`id_shop`),
  KEY `id_currency` (`id_currency`),
  KEY `price_min` (`price_min`),
  KEY `price_max` (`price_max`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_layered_price_index: ~7 rows (approximately)
/*!40000 ALTER TABLE `ps_layered_price_index` DISABLE KEYS */;
INSERT INTO `ps_layered_price_index` (`id_product`, `id_currency`, `id_shop`, `price_min`, `price_max`) VALUES
	(1, 1, 1, 16, 20),
	(2, 1, 1, 26, 32),
	(3, 1, 1, 25, 31),
	(4, 1, 1, 50, 61),
	(5, 1, 1, 28, 34),
	(6, 1, 1, 30, 36),
	(7, 1, 1, 16, 20);
/*!40000 ALTER TABLE `ps_layered_price_index` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_layered_product_attribute
DROP TABLE IF EXISTS `ps_layered_product_attribute`;
CREATE TABLE IF NOT EXISTS `ps_layered_product_attribute` (
  `id_attribute` int(10) unsigned NOT NULL,
  `id_product` int(10) unsigned NOT NULL,
  `id_attribute_group` int(10) unsigned NOT NULL DEFAULT '0',
  `id_shop` int(10) unsigned NOT NULL DEFAULT '1',
  KEY `id_attribute` (`id_attribute`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_layered_product_attribute: ~36 rows (approximately)
/*!40000 ALTER TABLE `ps_layered_product_attribute` DISABLE KEYS */;
INSERT INTO `ps_layered_product_attribute` (`id_attribute`, `id_product`, `id_attribute_group`, `id_shop`) VALUES
	(1, 2, 1, 1),
	(1, 3, 1, 1),
	(1, 4, 1, 1),
	(1, 5, 1, 1),
	(1, 6, 1, 1),
	(1, 7, 1, 1),
	(2, 2, 1, 1),
	(2, 3, 1, 1),
	(2, 4, 1, 1),
	(2, 5, 1, 1),
	(2, 6, 1, 1),
	(2, 7, 1, 1),
	(3, 2, 1, 1),
	(3, 3, 1, 1),
	(3, 4, 1, 1),
	(3, 5, 1, 1),
	(3, 6, 1, 1),
	(3, 7, 1, 1),
	(7, 4, 3, 1),
	(8, 2, 3, 1),
	(8, 6, 3, 1),
	(11, 2, 3, 1),
	(11, 5, 3, 1),
	(13, 3, 3, 1),
	(13, 5, 3, 1),
	(14, 5, 3, 1),
	(15, 7, 3, 1),
	(16, 5, 3, 1),
	(16, 6, 3, 1),
	(16, 7, 3, 1),
	(24, 4, 3, 1),
	(1, 1, 1, 1),
	(2, 1, 1, 1),
	(3, 1, 1, 1),
	(13, 1, 3, 1),
	(14, 1, 3, 1);
/*!40000 ALTER TABLE `ps_layered_product_attribute` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_leoblogcat
DROP TABLE IF EXISTS `ps_leoblogcat`;
CREATE TABLE IF NOT EXISTS `ps_leoblogcat` (
  `id_leoblogcat` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `id_parent` int(11) NOT NULL,
  `item` varchar(255) DEFAULT NULL,
  `level_depth` smallint(6) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `show_title` tinyint(1) NOT NULL,
  `position` int(11) NOT NULL,
  `submenu_content` text NOT NULL,
  `privacy` smallint(6) DEFAULT NULL,
  `position_type` varchar(25) DEFAULT NULL,
  `menu_class` varchar(25) DEFAULT NULL,
  `content` text,
  `icon_class` varchar(255) DEFAULT NULL,
  `level` int(11) NOT NULL,
  `left` int(11) NOT NULL,
  `right` int(11) NOT NULL,
  `date_add` datetime DEFAULT NULL,
  `date_upd` datetime DEFAULT NULL,
  `template` varchar(200) NOT NULL,
  PRIMARY KEY (`id_leoblogcat`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_leoblogcat: ~4 rows (approximately)
/*!40000 ALTER TABLE `ps_leoblogcat` DISABLE KEYS */;
INSERT INTO `ps_leoblogcat` (`id_leoblogcat`, `image`, `id_parent`, `item`, `level_depth`, `active`, `show_title`, `position`, `submenu_content`, `privacy`, `position_type`, `menu_class`, `content`, `icon_class`, `level`, `left`, `right`, `date_add`, `date_upd`, `template`) VALUES
	(1, '', 0, NULL, 0, 0, 0, 0, '', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, ''),
	(3, 'category-3.jpg', 1, '', 1, 1, 0, 0, '', 0, '', '', '', '', 0, 0, 0, '2013-11-27 01:06:52', '2013-12-18 03:07:22', 'default'),
	(4, 'category-2.jpg', 3, '', 2, 1, 0, 0, '', 0, '', '', '', '', 0, 0, 0, '2013-11-27 01:07:34', '2013-12-18 03:07:50', 'default'),
	(5, 'category-1.jpg', 3, NULL, 2, 1, 0, 1, '', 0, NULL, 'icon', NULL, 'fa-edit', 0, 0, 0, '2013-12-16 08:44:07', '2013-12-18 03:05:46', 'default');
/*!40000 ALTER TABLE `ps_leoblogcat` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_leoblogcat_lang
DROP TABLE IF EXISTS `ps_leoblogcat_lang`;
CREATE TABLE IF NOT EXISTS `ps_leoblogcat_lang` (
  `id_leoblogcat` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content_text` text,
  `description` varchar(200) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `link_rewrite` varchar(250) NOT NULL,
  PRIMARY KEY (`id_leoblogcat`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_leoblogcat_lang: ~4 rows (approximately)
/*!40000 ALTER TABLE `ps_leoblogcat_lang` DISABLE KEYS */;
INSERT INTO `ps_leoblogcat_lang` (`id_leoblogcat`, `id_lang`, `title`, `content_text`, `description`, `meta_keywords`, `meta_description`, `link_rewrite`) VALUES
	(1, 1, 'Root', NULL, '', '', '', ''),
	(3, 1, 'Category 1', '<p>Odio ut pretium ligula quam Vestibulum consequat convallis fringilla Vestibulum nulla. Accumsan morbi tristique auctor. At risus pretium urna tortor metus fringilla interdum mauris tempor congue</p>', '', '', '\r\n', 'category-1'),
	(4, 1, 'Sub Category 1', '<p>Odio ut pretium ligula quam Vestibulum consequat convallis fringilla Vestibulum nulla. Accumsan morbi tristique auctor. At risus pretium urna tortor metus fringilla interdum mauris tempor congue</p>', '', 'joomla,prestashop,leotheme,pavothemes', '', 'sub-category-1'),
	(5, 1, 'Sub Category 2', '<p>Odio ut pretium ligula quam Vestibulum consequat convallis fringilla Vestibulum nulla. Accumsan morbi tristique auctor. At risus pretium urna tortor metus fringilla interdum mauris tempor congue</p>', '', 'haha,joomla,magento', 'gogogoel', 'sub-category-2');
/*!40000 ALTER TABLE `ps_leoblogcat_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_leoblogcat_shop
DROP TABLE IF EXISTS `ps_leoblogcat_shop`;
CREATE TABLE IF NOT EXISTS `ps_leoblogcat_shop` (
  `id_leoblogcat` int(11) NOT NULL DEFAULT '0',
  `id_shop` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_leoblogcat`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_leoblogcat_shop: ~4 rows (approximately)
/*!40000 ALTER TABLE `ps_leoblogcat_shop` DISABLE KEYS */;
INSERT INTO `ps_leoblogcat_shop` (`id_leoblogcat`, `id_shop`) VALUES
	(1, 1),
	(3, 1),
	(4, 1),
	(5, 1);
/*!40000 ALTER TABLE `ps_leoblogcat_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_leoblog_blog
DROP TABLE IF EXISTS `ps_leoblog_blog`;
CREATE TABLE IF NOT EXISTS `ps_leoblog_blog` (
  `id_leoblog_blog` int(11) NOT NULL AUTO_INCREMENT,
  `id_leoblogcat` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `date_add` date NOT NULL,
  `active` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `hits` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `date_upd` datetime NOT NULL,
  `video_code` text,
  `params` text,
  `featured` tinyint(1) NOT NULL,
  `indexation` int(11) NOT NULL,
  `id_employee` int(11) NOT NULL,
  `product_ids` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_leoblog_blog`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_leoblog_blog: ~7 rows (approximately)
/*!40000 ALTER TABLE `ps_leoblog_blog` DISABLE KEYS */;
INSERT INTO `ps_leoblog_blog` (`id_leoblog_blog`, `id_leoblogcat`, `position`, `date_add`, `active`, `user_id`, `hits`, `image`, `date_upd`, `video_code`, `params`, `featured`, `indexation`, `id_employee`, `product_ids`) VALUES
	(3, 4, 0, '2013-11-28', 1, 0, 40, 'b-blog-1.jpg', '2013-12-20 09:55:38', '<iframe width="560" height="315" src="//www.youtube.com/embed/lzY4lkT8ElU" frameborder="0" allowfullscreen></iframe>', '', 0, 1, 1, ''),
	(4, 4, 2, '2013-12-04', 1, 0, 105, 'b-blog-2.jpg', '2013-12-18 06:31:14', '', '', 0, 1, 1, ''),
	(5, 4, 3, '2013-12-16', 1, 0, 9, 'b-blog-3.jpg', '2013-12-19 01:21:28', '', '', 0, 0, 1, ''),
	(6, 4, 4, '2013-12-18', 1, 0, 121, 'b-blog-4.jpg', '2013-12-20 09:54:03', '', '', 0, 0, 1, ''),
	(7, 4, 5, '2013-12-18', 1, 0, 71, 'b-blog-5.jpg', '2013-12-20 01:04:46', '', '', 0, 0, 1, ''),
	(8, 4, 1, '2013-12-18', 1, 0, 3, 'b-blog-6.jpg', '2013-12-19 22:50:10', '', '', 0, 0, 1, ''),
	(9, 4, 6, '2013-12-18', 1, 0, 0, 'b-blog-7.jpg', '2013-12-18 03:32:42', '', '', 0, 1, 1, '');
/*!40000 ALTER TABLE `ps_leoblog_blog` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_leoblog_blog_lang
DROP TABLE IF EXISTS `ps_leoblog_blog_lang`;
CREATE TABLE IF NOT EXISTS `ps_leoblog_blog_lang` (
  `id_leoblog_blog` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(250) NOT NULL,
  `meta_title` varchar(250) NOT NULL,
  `link_rewrite` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `description` text NOT NULL,
  `tags` varchar(255) NOT NULL,
  PRIMARY KEY (`id_leoblog_blog`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_leoblog_blog_lang: ~7 rows (approximately)
/*!40000 ALTER TABLE `ps_leoblog_blog_lang` DISABLE KEYS */;
INSERT INTO `ps_leoblog_blog_lang` (`id_leoblog_blog`, `id_lang`, `meta_description`, `meta_keywords`, `meta_title`, `link_rewrite`, `content`, `description`, `tags`) VALUES
	(3, 1, '', '', 'At risus pretium urna tortor metus fringilla', 'at-risus-pretium-urna-tortor-metus-fringilla', '<p>Odio ut pretium ligula quam Vestibulum consequat convallis fringilla Vestibulum nulla. Accumsan morbi tristique auctor. At risus pretium urna tortor metus fringilla interdum mauris tempor congue Odio ut pretium ligula quam Vestibulum consequat convallis fringilla Vestibulum nulla. Accumsan morbi tristique auctor. At risus pretium urna tortor metus fringilla interdum mauris tempor congue Odio ut pretium ligula quam Vestibulum consequat convallis fringilla Vestibulum nulla. Accumsan morbi tristique auctor. At risus pretium urna tortor metus fringilla interdum mauris tempor congue</p>\r\n<p> </p>\r\n<p>Odio ut pretium ligula quam Vestibulum consequat convallis fringilla Vestibulum nulla. Accumsan morbi tristique auctor. At risus pretium urna tortor metus fringilla interdum mauris tempor congue</p>\r\n<p> </p>\r\n<p>Odio ut pretium ligula quam Vestibulum consequat convallis fringilla Vestibulum nulla. Accumsan morbi tristique auctor. At risus pretium urna tortor metus fringilla interdum mauris tempor congue Odio ut pretium ligula quam Vestibulum consequat convallis fringilla Vestibulum nulla. Accumsan morbi tristique auctor. At risus pretium urna tortor metus fringilla interdum mauris tempor congue</p>\r\n<p> </p>\r\n<p>Odio ut pretium ligula quam Vestibulum consequat convallis fringilla Vestibulum nulla. Accumsan morbi tristique auctor. At risus pretium urna tortor metus fringilla interdum mauris tempor congue Odio ut pretium ligula quam Vestibulum consequat convallis fringilla Vestibulum nulla. Accumsan morbi tristique auctor. At risus pretium urna tortor metus fringilla interdum mauris tempor congue</p>', '<p>Odio ut pretium ligula quam Vestibulum consequat convallis fringilla Vestibulum nulla. Accumsan morbi tristique auctor. At risus pretium urna tortor metus fringilla interdum mauris tempor congue</p>', 'joomla,wordpress'),
	(4, 1, '', '', 'Ipsum cursus vestibulum at interdum Vivamus', 'ipsum-cursus-vestibulum-at-interdum-vivamus', '<p>Donec tellus Nulla lorem Nullam elit id ut elit feugiat lacus. Congue eget dapibus congue tincidunt senectus nibh risus Phasellus tristique justo. Justo Pellentesque Donec lobortis faucibus Vestibulum Praesent mauris volutpat vitae metus. Ipsum cursus vestibulum at interdum Vivamus nunc fringilla Curabitur ac quis. Nam lacinia wisi tortor orci quis vitae. Donec tellus Nulla lorem Nullam elit id ut elit feugiat lacus. Congue eget dapibus congue tincidunt senectus nibh risus Phasellus tristique justo. Justo Pellentesque Donec lobortis faucibus Vestibulum Praesent mauris volutpat vitae metus. Ipsum cursus vestibulum at interdum Vivamus nunc fringilla Curabitur ac quis. Nam lacinia wisi tortor orci quis vitae. Donec tellus Nulla lorem Nullam elit id ut elit feugiat lacus. Congue eget dapibus congue tincidunt senectus nibh risus Phasellus tristique justo. Justo Pellentesque Donec lobortis faucibus Vestibulum Praesent mauris volutpat vitae metus. Ipsum cursus vestibulum at interdum Vivamus nunc fringilla Curabitur ac quis. Nam lacinia wisi tortor orci quis vitae.</p>\r\n<p>Donec tellus Nulla lorem Nullam elit id ut elit feugiat lacus. Congue eget dapibus congue tincidunt senectus nibh risus Phasellus tristique justo. Justo Pellentesque Donec lobortis faucibus Vestibulum Praesent mauris volutpat vitae metus. Ipsum cursus vestibulum at interdum Vivamus nunc fringilla Curabitur ac quis. Nam lacinia wisi tortor orci quis vitae.Donec tellus Nulla lorem Nullam elit id ut elit feugiat lacus. Congue eget dapibus congue tincidunt senectus nibh risus Phasellus tristique justo. Justo Pellentesque Donec lobortis faucibus Vestibulum Praesent mauris volutpat vitae metus. Ipsum cursus vestibulum at interdum Vivamus nunc fringilla Curabitur ac quis. Nam lacinia wisi tortor orci quis vitae.Donec tellus Nulla lorem Nullam elit id ut elit feugiat lacus. Congue eget dapibus congue tincidunt senectus nibh risus Phasellus tristique justo. Justo Pellentesque Donec lobortis faucibus Vestibulum Praesent mauris volutpat vitae metus. Ipsum cursus vestibulum at interdum Vivamus nunc fringilla Curabitur ac quis. Nam lacinia wisi tortor orci quis vitae.</p>', '<p>Donec tellus Nulla lorem Nullam elit id ut elit feugiat lacus. Congue eget dapibus congue tincidunt senectus nibh risus Phasellus tristique justo. Justo Pellentesque Donec lobortis faucibus Vestibulum Praesent mauris volutpat vitae metus. Ipsum cursus vestibulum at interdum Vivamus nunc fringilla Curabitur ac quis. Nam lacinia wisi tortor orci quis vitae.</p>', 'joomla,prestashop,leotheme'),
	(5, 1, '', 'joomla,prestashop,leotheme,prestashop theme', 'Urna pretium elit mauris cursus Curabitur at elit Vestibulum', 'urna-pretium-elit-mauris-cursus-curabitur-at-elit-vestibulum', '<p>Mi vitae magnis Fusce laoreet nibh felis porttitor laoreet Vestibulum faucibus. At Nulla id tincidunt ut sed semper vel Lorem condimentum ornare. Laoreet Vestibulum lacinia massa a commodo habitasse velit Vestibulum tincidunt In. Turpis at eleifend leo mi elit Aenean porta ac sed faucibus. Nunc urna Morbi fringilla vitae orci convallis condimentum auctor sit dui. Urna pretium elit mauris cursus Curabitur at elit Vestibulum.</p>', '<p>Mi vitae magnis Fusce laoreet nibh felis porttitor laoreet Vestibulum faucibus. At Nulla id tincidunt ut sed semper vel Lorem condimentum ornare. Laoreet Vestibulum lacinia massa a commodo habitasse velit Vestibulum tincidunt In. Turpis at eleifend leo mi elit Aenean porta ac sed faucibus. </p>', 'Joomla'),
	(6, 1, '', '', 'Urna pretium elit mauris cursus Curabitur at elit Vestibulum', 'urna-pretium-elit-mauris-cursus-curabitur-at-elit-vestibulum', '<p>Mi vitae magnis Fusce laoreet nibh felis porttitor laoreet Vestibulum faucibus. At Nulla id tincidunt ut sed semper vel Lorem condimentum ornare. Laoreet Vestibulum lacinia massa a commodo habitasse velit Vestibulum tincidunt In. Turpis at eleifend leo mi elit Aenean porta ac sed faucibus. Nunc urna Morbi fringilla vitae orci convallis condimentum auctor sit dui. Urna pretium elit mauris cursus Curabitur at elit Vestibulum. Mi vitae magnis Fusce laoreet nibh felis porttitor laoreet Vestibulum faucibus. At Nulla id tincidunt ut sed semper vel Lorem condimentum ornare. Laoreet Vestibulum lacinia massa a commodo habitasse velit Vestibulum tincidunt In. Turpis at eleifend leo mi elit Aenean porta ac sed faucibus. Nunc urna Morbi fringilla vitae orci convallis condimentum auctor sit dui. Urna pretium elit mauris cursus Curabitur at elit Vestibulum.</p>', '<p>Mi vitae magnis Fusce laoreet nibh felis porttitor laoreet Vestibulum faucibus. At Nulla id tincidunt ut sed semper vel Lorem condimentum ornare. Laoreet Vestibulum lacinia massa a commodo habitasse velit Vestibulum tincidunt In. Turpis at eleifend leo mi elit Aenean porta ac sed faucibus. Nunc urna Morbi fringilla vitae orci convallis condimentum auctor sit dui. Urna pretium elit mauris cursus Curabitur at elit Vestibulum.</p>', 'leotheme,prestashop,theme'),
	(7, 1, '', '', 'Morbi condimentum molestie Nam enim odio sodales', 'morbi-condimentum-molestie-nam-enim-odio-sodales', '<div class="description clearfix"><p>Odio ut pretium ligula quam Vestibulum consequat convallis fringilla Vestibulum nulla. Accumsan morbi tristique auctor. At risus pretium urna tortor metus fringilla interdum mauris tempor congue.</p></div><div class="blog-content clearfix"><div class="content-wrap clearfix"><div class="itemFullText"><p>Commodo laoreet semper tincidunt lorem Vestibulum nunc at In Curabitur magna. Euismod euismod Suspendisse tortor ante adipiscing risus Aenean Lorem vitae id. Odio ut pretium ligula quam Vestibulum consequat convallis fringilla Vestibulum nulla. Accumsan morbi tristique auctor Aenean nulla lacinia Nullam elit vel vel. At risus pretium urna tortor metus fringilla interdum mauris tempor congue.</p><p>Donec tellus Nulla lorem Nullam elit id ut elit feugiat lacus. Congue eget dapibus congue tincidunt senectus nibh risus Phasellus tristique justo. Justo Pellentesque Donec lobortis faucibus Vestibulum Praesent mauris volutpat vitae metus. Ipsum cursus vestibulum at interdum Vivamus nunc fringilla Curabitur ac quis. Nam lacinia wisi tortor orci quis vitae.</p><p>Sed mauris Pellentesque elit Aliquam at lacus interdum nascetur elit ipsum. Enim ipsum hendrerit Suspendisse turpis laoreet fames tempus ligula pede ac. Et Lorem penatibus orci eu ultrices egestas Nam quam Vivamus nibh. Morbi condimentum molestie Nam enim odio sodales pretium eros sem pellentesque. Sit tellus Integer elit egestas lacus turpis id auctor nascetur ut. Ac elit vitae.</p><p>Mi vitae magnis Fusce laoreet nibh felis porttitor laoreet Vestibulum faucibus. At Nulla id tincidunt ut sed semper vel Lorem condimentum ornare. Laoreet Vestibulum lacinia massa a commodo habitasse velit Vestibulum tincidunt In. Turpis at eleifend leo mi elit Aenean porta ac sed faucibus. Nunc urna Morbi fringilla vitae orci convallis condimentum auctor sit dui. Urna pretium elit mauris cursus Curabitur at elit Vestibulum.</p></div></div></div>', '<p>Sed mauris Pellentesque elit Aliquam at lacus interdum nascetur elit ipsum. Enim ipsum hendrerit Suspendisse turpis laoreet fames tempus ligula pede ac. Et Lorem penatibus orci eu ultrices egestas Nam quam Vivamus nibh.</p>', 'leotheme,prestashop,magento,opencart'),
	(8, 1, '', '', 'Turpis at eleifend leo mi elit Aenean porta ac sed faucibus', 'turpis-at-eleifend-leo-mi-elit-aenean-porta-ac-sed-faucibus', '<div class="description clearfix">\r\n<p>Odio ut pretium ligula quam Vestibulum consequat convallis fringilla Vestibulum nulla. Accumsan morbi tristique auctor. At risus pretium urna tortor metus fringilla interdum mauris tempor congue.</p>\r\n</div>\r\n<div class="blog-content clearfix">\r\n<div class="content-wrap clearfix">\r\n<div class="itemFullText">\r\n<p>Commodo laoreet semper tincidunt lorem Vestibulum nunc at In Curabitur magna. Euismod euismod Suspendisse tortor ante adipiscing risus Aenean Lorem vitae id. Odio ut pretium ligula quam Vestibulum consequat convallis fringilla Vestibulum nulla. Accumsan morbi tristique auctor Aenean nulla lacinia Nullam elit vel vel. At risus pretium urna tortor metus fringilla interdum mauris tempor congue.</p>\r\n<p>Donec tellus Nulla lorem Nullam elit id ut elit feugiat lacus. Congue eget dapibus congue tincidunt senectus nibh risus Phasellus tristique justo. Justo Pellentesque Donec lobortis faucibus Vestibulum Praesent mauris volutpat vitae metus. Ipsum cursus vestibulum at interdum Vivamus nunc fringilla Curabitur ac quis. Nam lacinia wisi tortor orci quis vitae.</p>\r\n<p>Sed mauris Pellentesque elit Aliquam at lacus interdum nascetur elit ipsum. Enim ipsum hendrerit Suspendisse turpis laoreet fames tempus ligula pede ac. Et Lorem penatibus orci eu ultrices egestas Nam quam Vivamus nibh. Morbi condimentum molestie Nam enim odio sodales pretium eros sem pellentesque. Sit tellus Integer elit egestas lacus turpis id auctor nascetur ut. Ac elit vitae.</p>\r\n<p>Mi vitae magnis Fusce laoreet nibh felis porttitor laoreet Vestibulum faucibus. At Nulla id tincidunt ut sed semper vel Lorem condimentum ornare. Laoreet Vestibulum lacinia massa a commodo habitasse velit Vestibulum tincidunt In. Turpis at eleifend leo mi elit Aenean porta ac sed faucibus. Nunc urna Morbi fringilla vitae orci convallis condimentum auctor sit dui. Urna pretium elit mauris cursus Curabitur at elit Vestibulum.</p>\r\n</div>\r\n</div>\r\n</div>', '<p>Turpis at eleifend leo mi elit Aenean porta ac sed faucibus. Nunc urna Morbi fringilla vitae orci convallis condimentum auctor sit dui. Urna pretium elit mauris cursus Curabitur at elit Vestibulum</p>', 'magento,opencart,template'),
	(9, 1, '', '', 'Nullam ullamcorper nisl quis ornare molestie', 'nullam-ullamcorper-nisl-quis-ornare-molestie', '<div class="description clearfix">\r\n<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quas.</p>\r\n</div>\r\n<div class="blog-content clearfix">\r\n<div class="content-wrap clearfix">\r\n<p>Suspendisse posuere, diam in bibendum lobortis, turpis ipsum aliquam risus, sit amet dictum ligula lorem non nisl. Ut vitae nibh id massa vulputate euismod ut quis justo. Ut bibendum sem at massa lacinia, eget elementum ante consectetur. Nulla id pharetra dui, at rhoncus urna. Maecenas non porttitor purus. Nullam ullamcorper nisl quis ornare molestie.</p>\r\n<p>Etiam eget erat est. Phasellus elit justo, mattis non lorem non, aliquam aliquam leo. Sed fermentum consectetur magna, eget semper ante. Aliquam scelerisque justo velit. Fusce cursus blandit dolor, in sodales urna vulputate lobortis. Nulla ut tellus turpis. Nullam lacus sem, volutpat id odio sed, cursus tristique eros. Duis at pellentesque magna. Donec magna nisi, vulputate ac nulla eu, ultricies tincidunt tellus. Nunc tincidunt sem urna, nec venenatis libero vehicula ut.</p>\r\n<p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur faucibus aliquam pulvinar. Vivamus mattis volutpat erat, et congue nisi semper quis. Cras vehicula dignissim libero in elementum. Mauris sit amet dolor justo. Morbi consequat velit vel est fermentum euismod. Curabitur in magna augue.</p>\r\n</div>\r\n</div>', '<p>Suspendisse posuere, diam in bibendum lobortis, turpis ipsum aliquam risus, sit amet dictum ligula lorem non nisl. Ut vitae nibh id massa vulputate euismod ut quis justo. Ut bibendum sem at massa lacinia, eget elementum ante consectetur. Nulla id pharetra dui, at rhoncus urna. Maecenas non porttitor purus. Nullam ullamcorper nisl quis ornare molestie</p>', 'opencart,theme');
/*!40000 ALTER TABLE `ps_leoblog_blog_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_leoblog_blog_shop
DROP TABLE IF EXISTS `ps_leoblog_blog_shop`;
CREATE TABLE IF NOT EXISTS `ps_leoblog_blog_shop` (
  `id_leoblog_blog` int(11) NOT NULL DEFAULT '0',
  `id_shop` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_leoblog_blog`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_leoblog_blog_shop: ~7 rows (approximately)
/*!40000 ALTER TABLE `ps_leoblog_blog_shop` DISABLE KEYS */;
INSERT INTO `ps_leoblog_blog_shop` (`id_leoblog_blog`, `id_shop`) VALUES
	(3, 1),
	(4, 1),
	(5, 1),
	(6, 1),
	(7, 1),
	(8, 1),
	(9, 1);
/*!40000 ALTER TABLE `ps_leoblog_blog_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_leoblog_comment
DROP TABLE IF EXISTS `ps_leoblog_comment`;
CREATE TABLE IF NOT EXISTS `ps_leoblog_comment` (
  `id_comment` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(11) NOT NULL DEFAULT '0',
  `id_leoblog_blog` int(11) unsigned NOT NULL,
  `comment` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `date_add` datetime DEFAULT NULL,
  `user` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id_comment`,`id_shop`),
  KEY `FK_blog_comment` (`id_leoblog_blog`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_leoblog_comment: ~2 rows (approximately)
/*!40000 ALTER TABLE `ps_leoblog_comment` DISABLE KEYS */;
INSERT INTO `ps_leoblog_comment` (`id_comment`, `id_shop`, `id_leoblog_blog`, `comment`, `active`, `date_add`, `user`, `email`) VALUES
	(3, 1, 3, ' At Nulla id tincidunt ut sed semper vel Lorem condimentum ornare. Laoreet Vestibulum lacinia massa a commodo habitasse velit Vestibulum tincidunt In', 1, '2013-12-11 22:18:13', 'ha cong tien', 'tienhc@brainos.vn'),
	(4, 1, 3, ' At Nulla id tincidunt ut sed semper vel Lorem condimentum ornare. Laoreet Vestibulum lacinia massa a commodo habitasse velit Vestibulum tincidunt In', 1, '2013-12-11 22:18:33', 'ha cong tien', 'tienhc@brainos.vn');
/*!40000 ALTER TABLE `ps_leoblog_comment` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_leosliderlayer_groups
DROP TABLE IF EXISTS `ps_leosliderlayer_groups`;
CREATE TABLE IF NOT EXISTS `ps_leosliderlayer_groups` (
  `id_leosliderlayer_groups` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `hook` varchar(64) NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `params` text NOT NULL,
  PRIMARY KEY (`id_leosliderlayer_groups`,`id_shop`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_leosliderlayer_groups: ~5 rows (approximately)
/*!40000 ALTER TABLE `ps_leosliderlayer_groups` DISABLE KEYS */;
INSERT INTO `ps_leosliderlayer_groups` (`id_leosliderlayer_groups`, `title`, `id_shop`, `hook`, `active`, `params`) VALUES
	(1, 'Slideshow default', 1, 'displaySlideshow', 1, 'eyJhdXRvX3BsYXkiOiIwIiwiZGVsYXkiOiI5MDAwIiwiZnVsbHdpZHRoIjoiZnVsbHdpZHRoIiwibWRfd2lkdGgiOiIxMiIsInNtX3dpZHRoIjoiMTIiLCJ4c193aWR0aCI6IjEyIiwidG91Y2hfbW9iaWxlIjoiMSIsInN0b3Bfb25faG92ZXIiOiIxIiwic2h1ZmZsZV9tb2RlIjoiMSIsInNoYWRvd190eXBlIjoiMCIsInNob3dfdGltZV9saW5lIjoiMSIsInRpbWVfbGluZV9wb3NpdGlvbiI6InRvcCIsIm1hcmdpbiI6IjBweCIsInBhZGRpbmciOiIwcHgiLCJiYWNrZ3JvdW5kX2NvbG9yIjoiI2ZmZmZlZSIsImJhY2tncm91bmRfaW1hZ2UiOiIwIiwiYmFja2dyb3VuZF91cmwiOiIiLCJncm91cF9jbGFzcyI6InNsaWRlLWRlZmF1bHQgaGlkZGVuLXhzIiwibmF2aWdhdG9yX3R5cGUiOiJub25lIiwibmF2aWdhdG9yX2Fycm93cyI6InZlcnRpY2FsY2VudGVyZWQiLCJuYXZpZ2F0aW9uX3N0eWxlIjoicm91bmQiLCJzaG93X25hdmlnYXRvciI6IjAiLCJoaWRlX25hdmlnYXRvcl9hZnRlciI6IjIwMCIsImltYWdlX2Nyb3BwaW5nIjoiMSIsIndpZHRoIjoiODczIiwiaGVpZ2h0IjoiNDkwIiwidGh1bWJuYWlsX3dpZHRoIjoiMTAwIiwidGh1bWJuYWlsX2hlaWdodCI6IjUwIiwidGh1bWJuYWlsX2Ftb3VudCI6IjUifQ=='),
	(2, 'Slideshow cd', 1, 'displaySlideshow', 1, 'eyJhdXRvX3BsYXkiOiIwIiwiZGVsYXkiOiI5MDAwIiwiZnVsbHdpZHRoIjoiZnVsbHNjcmVlbiIsIm1kX3dpZHRoIjoiMTIiLCJzbV93aWR0aCI6IjEyIiwieHNfd2lkdGgiOiIxMiIsInRvdWNoX21vYmlsZSI6IjEiLCJzdG9wX29uX2hvdmVyIjoiMSIsInNodWZmbGVfbW9kZSI6IjEiLCJzaGFkb3dfdHlwZSI6IjAiLCJzaG93X3RpbWVfbGluZSI6IjEiLCJ0aW1lX2xpbmVfcG9zaXRpb24iOiJ0b3AiLCJtYXJnaW4iOiIwcHgiLCJwYWRkaW5nIjoiMHB4IiwiYmFja2dyb3VuZF9jb2xvciI6IiNmZmZmZWUiLCJiYWNrZ3JvdW5kX2ltYWdlIjoiMCIsImJhY2tncm91bmRfdXJsIjoiIiwiZ3JvdXBfY2xhc3MiOiJoaWRkZW4teHMiLCJuYXZpZ2F0b3JfdHlwZSI6Im5vbmUiLCJuYXZpZ2F0b3JfYXJyb3dzIjoidmVydGljYWxjZW50ZXJlZCIsIm5hdmlnYXRpb25fc3R5bGUiOiJyb3VuZCIsInNob3dfbmF2aWdhdG9yIjoiMCIsImhpZGVfbmF2aWdhdG9yX2FmdGVyIjoiMjAwIiwiaW1hZ2VfY3JvcHBpbmciOiIxIiwid2lkdGgiOiIxMTcwIiwiaGVpZ2h0IjoiNjAwIiwidGh1bWJuYWlsX3dpZHRoIjoiMTAwIiwidGh1bWJuYWlsX2hlaWdodCI6IjUwIiwidGh1bWJuYWlsX2Ftb3VudCI6IjUifQ=='),
	(3, 'Slideshow sport', 1, 'displaySlideshow', 1, 'eyJhdXRvX3BsYXkiOiIwIiwiZGVsYXkiOiI5MDAwIiwiZnVsbHdpZHRoIjoiZnVsbHNjcmVlbiIsIm1kX3dpZHRoIjoiMTIiLCJzbV93aWR0aCI6IjEyIiwieHNfd2lkdGgiOiIxMiIsInRvdWNoX21vYmlsZSI6IjEiLCJzdG9wX29uX2hvdmVyIjoiMSIsInNodWZmbGVfbW9kZSI6IjEiLCJzaGFkb3dfdHlwZSI6IjAiLCJzaG93X3RpbWVfbGluZSI6IjEiLCJ0aW1lX2xpbmVfcG9zaXRpb24iOiJ0b3AiLCJtYXJnaW4iOiIwcHgiLCJwYWRkaW5nIjoiMHB4IiwiYmFja2dyb3VuZF9jb2xvciI6IiNmZmZmZWUiLCJiYWNrZ3JvdW5kX2ltYWdlIjoiMCIsImJhY2tncm91bmRfdXJsIjoiIiwiZ3JvdXBfY2xhc3MiOiJoaWRkZW4teHMiLCJuYXZpZ2F0b3JfdHlwZSI6Im5vbmUiLCJuYXZpZ2F0b3JfYXJyb3dzIjoidmVydGljYWxjZW50ZXJlZCIsIm5hdmlnYXRpb25fc3R5bGUiOiJyb3VuZCIsInNob3dfbmF2aWdhdG9yIjoiMCIsImhpZGVfbmF2aWdhdG9yX2FmdGVyIjoiMjAwIiwiaW1hZ2VfY3JvcHBpbmciOiIxIiwid2lkdGgiOiIxMTcwIiwiaGVpZ2h0IjoiNjQwIiwidGh1bWJuYWlsX3dpZHRoIjoiMTAwIiwidGh1bWJuYWlsX2hlaWdodCI6IjUwIiwidGh1bWJuYWlsX2Ftb3VudCI6IjUifQ=='),
	(4, 'Slideshow fashion', 1, 'displaySlideshow', 1, 'eyJhdXRvX3BsYXkiOiIxIiwiZGVsYXkiOiI5MDAwIiwiZnVsbHdpZHRoIjoiZnVsbHdpZHRoIiwibWRfd2lkdGgiOiIxMiIsInNtX3dpZHRoIjoiMTIiLCJ4c193aWR0aCI6IjEyIiwidG91Y2hfbW9iaWxlIjoiMSIsInN0b3Bfb25faG92ZXIiOiIxIiwic2h1ZmZsZV9tb2RlIjoiMSIsInNoYWRvd190eXBlIjoiMCIsInNob3dfdGltZV9saW5lIjoiMSIsInRpbWVfbGluZV9wb3NpdGlvbiI6InRvcCIsIm1hcmdpbiI6IjBweCIsInBhZGRpbmciOiIwcHgiLCJiYWNrZ3JvdW5kX2NvbG9yIjoiI2ZmZmZlZSIsImJhY2tncm91bmRfaW1hZ2UiOiIwIiwiYmFja2dyb3VuZF91cmwiOiIiLCJncm91cF9jbGFzcyI6ImhpZGRlbi14cyIsIm5hdmlnYXRvcl90eXBlIjoibm9uZSIsIm5hdmlnYXRvcl9hcnJvd3MiOiJ2ZXJ0aWNhbGNlbnRlcmVkIiwibmF2aWdhdGlvbl9zdHlsZSI6InJvdW5kIiwic2hvd19uYXZpZ2F0b3IiOiIwIiwiaGlkZV9uYXZpZ2F0b3JfYWZ0ZXIiOiIyMDAiLCJpbWFnZV9jcm9wcGluZyI6IjEiLCJ3aWR0aCI6Ijg3MyIsImhlaWdodCI6IjU0MCIsInRodW1ibmFpbF93aWR0aCI6IjEwMCIsInRodW1ibmFpbF9oZWlnaHQiOiI1MCIsInRodW1ibmFpbF9hbW91bnQiOiI1In0='),
	(5, 'Slideshow gifts', 1, 'displaySlideshow', 1, 'eyJhdXRvX3BsYXkiOiIwIiwiZGVsYXkiOiI5MDAwIiwiZnVsbHdpZHRoIjoiZnVsbHdpZHRoIiwibWRfd2lkdGgiOiIxMiIsInNtX3dpZHRoIjoiMTIiLCJ4c193aWR0aCI6IjEyIiwidG91Y2hfbW9iaWxlIjoiMSIsInN0b3Bfb25faG92ZXIiOiIxIiwic2h1ZmZsZV9tb2RlIjoiMSIsInNoYWRvd190eXBlIjoiMCIsInNob3dfdGltZV9saW5lIjoiMSIsInRpbWVfbGluZV9wb3NpdGlvbiI6InRvcCIsIm1hcmdpbiI6IjBweCIsInBhZGRpbmciOiIwcHgiLCJiYWNrZ3JvdW5kX2NvbG9yIjoiI2ZmZmZlZSIsImJhY2tncm91bmRfaW1hZ2UiOiIwIiwiYmFja2dyb3VuZF91cmwiOiIiLCJncm91cF9jbGFzcyI6ImhpZGRlbi14cyIsIm5hdmlnYXRvcl90eXBlIjoibm9uZSIsIm5hdmlnYXRvcl9hcnJvd3MiOiJ2ZXJ0aWNhbGNlbnRlcmVkIiwibmF2aWdhdGlvbl9zdHlsZSI6InJvdW5kIiwic2hvd19uYXZpZ2F0b3IiOiIwIiwiaGlkZV9uYXZpZ2F0b3JfYWZ0ZXIiOiIyMDAiLCJpbWFnZV9jcm9wcGluZyI6IjEiLCJ3aWR0aCI6IjExNzAiLCJoZWlnaHQiOiI1NjAiLCJ0aHVtYm5haWxfd2lkdGgiOiIxMDAiLCJ0aHVtYm5haWxfaGVpZ2h0IjoiNTAiLCJ0aHVtYm5haWxfYW1vdW50IjoiNSJ9');
/*!40000 ALTER TABLE `ps_leosliderlayer_groups` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_leosliderlayer_slides
DROP TABLE IF EXISTS `ps_leosliderlayer_slides`;
CREATE TABLE IF NOT EXISTS `ps_leosliderlayer_slides` (
  `id_leosliderlayer_slides` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_group` int(11) NOT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `parent_id` int(11) NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`id_leosliderlayer_slides`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_leosliderlayer_slides: ~18 rows (approximately)
/*!40000 ALTER TABLE `ps_leosliderlayer_slides` DISABLE KEYS */;
INSERT INTO `ps_leosliderlayer_slides` (`id_leosliderlayer_slides`, `id_group`, `position`, `active`, `parent_id`, `params`) VALUES
	(1, 1, 0, 1, 0, 'eyJ0cmFuc2l0aW9uIjoiZmx5aW4iLCJzbG90IjoiNyIsInJvdGF0aW9uIjoiMCIsImR1cmF0aW9uIjoiMzAwIiwiZGVsYXkiOiIwIiwiZW5hYmxlX2xpbmsiOiIxIn0='),
	(3, 0, 0, 0, 0, 'ZmFsc2U='),
	(4, 0, 0, 0, 0, 'ZmFsc2U='),
	(16, 5, 0, 1, 0, 'eyJ0cmFuc2l0aW9uIjoicmFuZG9tIiwic2xvdCI6IjciLCJyb3RhdGlvbiI6IjAiLCJkdXJhdGlvbiI6IjMwMCIsImRlbGF5IjoiMCIsImVuYWJsZV9saW5rIjoiMSJ9'),
	(19, 3, 0, 1, 0, 'eyJ0cmFuc2l0aW9uIjoicmFuZG9tIiwic2xvdCI6IjciLCJyb3RhdGlvbiI6IjAiLCJkdXJhdGlvbiI6IjMwMCIsImRlbGF5IjoiMCIsImVuYWJsZV9saW5rIjoiMSJ9'),
	(23, 4, 0, 1, 0, 'eyJ0cmFuc2l0aW9uIjoicmFuZG9tIiwic2xvdCI6IjciLCJyb3RhdGlvbiI6IjAiLCJkdXJhdGlvbiI6IjMwMCIsImRlbGF5IjoiMCIsImVuYWJsZV9saW5rIjoiMSJ9'),
	(28, 1, 0, 1, 0, 'eyJ0cmFuc2l0aW9uIjoicmFuZG9tIiwic2xvdCI6IjciLCJyb3RhdGlvbiI6IjAiLCJkdXJhdGlvbiI6IjMwMCIsImRlbGF5IjoiMCIsImVuYWJsZV9saW5rIjoiMSJ9'),
	(29, 1, 0, 1, 0, 'eyJ0cmFuc2l0aW9uIjoicmFuZG9tIiwic2xvdCI6IjciLCJyb3RhdGlvbiI6IjAiLCJkdXJhdGlvbiI6IjMwMCIsImRlbGF5IjoiMCIsImVuYWJsZV9saW5rIjoiMSJ9'),
	(30, 5, 0, 1, 0, 'eyJ0cmFuc2l0aW9uIjoicmFuZG9tIiwic2xvdCI6IjciLCJyb3RhdGlvbiI6IjAiLCJkdXJhdGlvbiI6IjMwMCIsImRlbGF5IjoiMCIsImVuYWJsZV9saW5rIjoiMSJ9'),
	(31, 5, 0, 1, 0, 'eyJ0cmFuc2l0aW9uIjoicmFuZG9tIiwic2xvdCI6IjciLCJyb3RhdGlvbiI6IjAiLCJkdXJhdGlvbiI6IjMwMCIsImRlbGF5IjoiMCIsImVuYWJsZV9saW5rIjoiMSJ9'),
	(32, 5, 0, 1, 0, 'eyJ0cmFuc2l0aW9uIjoicmFuZG9tIiwic2xvdCI6IjciLCJyb3RhdGlvbiI6IjAiLCJkdXJhdGlvbiI6IjMwMCIsImRlbGF5IjoiMCIsImVuYWJsZV9saW5rIjoiMSJ9'),
	(33, 3, 0, 1, 0, 'eyJ0cmFuc2l0aW9uIjoicmFuZG9tIiwic2xvdCI6IjciLCJyb3RhdGlvbiI6IjAiLCJkdXJhdGlvbiI6IjMwMCIsImRlbGF5IjoiMCIsImVuYWJsZV9saW5rIjoiMSJ9'),
	(34, 3, 0, 1, 0, 'eyJ0cmFuc2l0aW9uIjoicmFuZG9tIiwic2xvdCI6IjciLCJyb3RhdGlvbiI6IjAiLCJkdXJhdGlvbiI6IjMwMCIsImRlbGF5IjoiMCIsImVuYWJsZV9saW5rIjoiMSJ9'),
	(35, 4, 0, 1, 0, 'eyJ0cmFuc2l0aW9uIjoicmFuZG9tIiwic2xvdCI6IjciLCJyb3RhdGlvbiI6IjAiLCJkdXJhdGlvbiI6IjMwMCIsImRlbGF5IjoiMCIsImVuYWJsZV9saW5rIjoiMSJ9'),
	(36, 4, 0, 1, 0, 'eyJ0cmFuc2l0aW9uIjoicmFuZG9tIiwic2xvdCI6IjciLCJyb3RhdGlvbiI6IjAiLCJkdXJhdGlvbiI6IjMwMCIsImRlbGF5IjoiMCIsImVuYWJsZV9saW5rIjoiMSJ9'),
	(37, 2, 0, 1, 0, 'eyJ0cmFuc2l0aW9uIjoicmFuZG9tIiwic2xvdCI6IjciLCJyb3RhdGlvbiI6IjAiLCJkdXJhdGlvbiI6IjMwMCIsImRlbGF5IjoiMCIsImVuYWJsZV9saW5rIjoiMSJ9'),
	(38, 2, 0, 1, 0, 'eyJ0cmFuc2l0aW9uIjoicmFuZG9tIiwic2xvdCI6IjciLCJyb3RhdGlvbiI6IjAiLCJkdXJhdGlvbiI6IjMwMCIsImRlbGF5IjoiMCIsImVuYWJsZV9saW5rIjoiMSJ9'),
	(39, 2, 0, 1, 0, 'eyJ0cmFuc2l0aW9uIjoicmFuZG9tIiwic2xvdCI6IjciLCJyb3RhdGlvbiI6IjAiLCJkdXJhdGlvbiI6IjMwMCIsImRlbGF5IjoiMCIsImVuYWJsZV9saW5rIjoiMSJ9');
/*!40000 ALTER TABLE `ps_leosliderlayer_slides` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_leosliderlayer_slides_lang
DROP TABLE IF EXISTS `ps_leosliderlayer_slides_lang`;
CREATE TABLE IF NOT EXISTS `ps_leosliderlayer_slides_lang` (
  `id_leosliderlayer_slides` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `video` text,
  `layersparams` text,
  PRIMARY KEY (`id_leosliderlayer_slides`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_leosliderlayer_slides_lang: ~18 rows (approximately)
/*!40000 ALTER TABLE `ps_leosliderlayer_slides_lang` DISABLE KEYS */;
INSERT INTO `ps_leosliderlayer_slides_lang` (`id_leosliderlayer_slides`, `id_lang`, `title`, `link`, `image`, `thumbnail`, `video`, `layersparams`) VALUES
	(1, 1, 'slider 1', 'http://prestashop.com', 'bg-sl-mb.jpg', '', 'eyJ1c2V2aWRlbyI6IjAiLCJ2aWRlb2lkIjoiIiwidmlkZW9hdXRvIjoiMSIsImJhY2tncm91bmRfY29sb3IiOiIjMjkyOTJiIn0=', 'W3sibGF5ZXJfdmlkZW9fdHlwZSI6InlvdXR1YmUiLCJsYXllcl92aWRlb19pZCI6IiIsImxheWVyX3ZpZGVvX2hlaWdodCI6IjIwMCIsImxheWVyX3ZpZGVvX3dpZHRoIjoiMzAwIiwibGF5ZXJfdmlkZW9fdGh1bWIiOiIiLCJsYXllcl9pZCI6IjFfMSIsImxheWVyX2NvbnRlbnQiOiJsYXllcjIucG5nIiwibGF5ZXJfdHlwZSI6InRleHQiLCJsYXllcl9jbGFzcyI6IiIsImxheWVyX2NhcHRpb24iOiI8aDIgc3R5bGU9XCJsZXR0ZXItc3BhY2luZzotM3B4OyAgICBmb250LXdlaWdodDogMzAwO1wiPk1hZWNlbjxzdHJvbmcgc3R5bGU9XCJ0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlOyBmb250LXdlaWdodDogOTAwO1wiPiBzYWdpdCA8XC9zdHJvbmc+PFwvaDI+IiwibGF5ZXJfZm9udF9zaXplIjoiNjBweCIsImxheWVyX2JhY2tncm91bmRfY29sb3IiOiIiLCJsYXllcl9jb2xvciI6IiNmZmZmZmYiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoic2Z0IiwibGF5ZXJfZWFzaW5nIjoiZWFzZUluRXhwbyIsImxheWVyX3NwZWVkIjoiMzUwIiwibGF5ZXJfdG9wIjoiMTE2IiwibGF5ZXJfbGVmdCI6IjYwIiwibGF5ZXJfZW5kdGltZSI6IjAiLCJsYXllcl9lbmRzcGVlZCI6IjMwMCIsImxheWVyX2VuZGFuaW1hdGlvbiI6ImF1dG8iLCJsYXllcl9lbmRlYXNpbmciOiJub3RoaW5nIiwidGltZV9zdGFydCI6IjEyMDAifSx7ImxheWVyX3ZpZGVvX3R5cGUiOiJ5b3V0dWJlIiwibGF5ZXJfdmlkZW9faWQiOiIiLCJsYXllcl92aWRlb19oZWlnaHQiOiIyMDAiLCJsYXllcl92aWRlb193aWR0aCI6IjMwMCIsImxheWVyX3ZpZGVvX3RodW1iIjoiIiwibGF5ZXJfaWQiOiIxXzIiLCJsYXllcl9jb250ZW50IjoibGF5ZXIyLnBuZyIsImxheWVyX3R5cGUiOiJ0ZXh0IiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiTWFlY2VuYXMgc2FnaXR0aXMgY29uZ3VlIGFyY3UsIHV0IG1ldHVzIHV0IHNhZ2l0dGlzPGJyPiB1bHRyaWNlcyBudW5jIHJob25jdXMgc29kYWxlcyIsImxheWVyX2ZvbnRfc2l6ZSI6IjE0cHgiLCJsYXllcl9iYWNrZ3JvdW5kX2NvbG9yIjoiIiwibGF5ZXJfY29sb3IiOiIiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoiZmFkZSIsImxheWVyX2Vhc2luZyI6ImVhc2VJbkNpcmMiLCJsYXllcl9zcGVlZCI6IjM1MCIsImxheWVyX3RvcCI6IjE5NCIsImxheWVyX2xlZnQiOiI2MCIsImxheWVyX2VuZHRpbWUiOiIwIiwibGF5ZXJfZW5kc3BlZWQiOiIzMDAiLCJsYXllcl9lbmRhbmltYXRpb24iOiJhdXRvIiwibGF5ZXJfZW5kZWFzaW5nIjoibm90aGluZyIsInRpbWVfc3RhcnQiOiIyMDAwIn0seyJsYXllcl92aWRlb190eXBlIjoieW91dHViZSIsImxheWVyX3ZpZGVvX2lkIjoiIiwibGF5ZXJfdmlkZW9faGVpZ2h0IjoiMjAwIiwibGF5ZXJfdmlkZW9fd2lkdGgiOiIzMDAiLCJsYXllcl92aWRlb190aHVtYiI6IiIsImxheWVyX2lkIjoiMV8zIiwibGF5ZXJfY29udGVudCI6ImxheWVyMi5wbmciLCJsYXllcl90eXBlIjoidGV4dCIsImxheWVyX2NsYXNzIjoiIiwibGF5ZXJfY2FwdGlvbiI6IjxhIGhyZWY9XCIjXCIgY2xhc3M9XCJidG5cIiAgc3R5bGU9XCJjb2xvcjogI2ZmZjtib3JkZXItY29sb3I6IzU0NTQ1NTsgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XCI+dmlldyBjb2xsZWN0aW9uPFwvYT4iLCJsYXllcl9mb250X3NpemUiOiIiLCJsYXllcl9iYWNrZ3JvdW5kX2NvbG9yIjoiIiwibGF5ZXJfY29sb3IiOiIiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoiZmFkZSIsImxheWVyX2Vhc2luZyI6ImVhc2VJbkN1YmljIiwibGF5ZXJfc3BlZWQiOiIzNTAiLCJsYXllcl90b3AiOiIzMTAiLCJsYXllcl9sZWZ0IjoiNjAiLCJsYXllcl9lbmR0aW1lIjoiMCIsImxheWVyX2VuZHNwZWVkIjoiMzAwIiwibGF5ZXJfZW5kYW5pbWF0aW9uIjoiYXV0byIsImxheWVyX2VuZGVhc2luZyI6Im5vdGhpbmciLCJ0aW1lX3N0YXJ0IjoiMjgwMCJ9LHsibGF5ZXJfdmlkZW9fdHlwZSI6InlvdXR1YmUiLCJsYXllcl92aWRlb19pZCI6IiIsImxheWVyX3ZpZGVvX2hlaWdodCI6IjIwMCIsImxheWVyX3ZpZGVvX3dpZHRoIjoiMzAwIiwibGF5ZXJfdmlkZW9fdGh1bWIiOiIiLCJsYXllcl9pZCI6IjFfNCIsImxheWVyX2NvbnRlbnQiOiJzbC1tYi0xLnBuZyIsImxheWVyX3R5cGUiOiJpbWFnZSIsImxheWVyX2NsYXNzIjoiIiwibGF5ZXJfY2FwdGlvbiI6IllvdXIgSW1hZ2UgSGVyZSA0IiwibGF5ZXJfZm9udF9zaXplIjoiIiwibGF5ZXJfYmFja2dyb3VuZF9jb2xvciI6IiIsImxheWVyX2NvbG9yIjoiIiwibGF5ZXJfbGluayI6IiIsImxheWVyX2FuaW1hdGlvbiI6ImxmciIsImxheWVyX2Vhc2luZyI6ImVhc2VPdXRFeHBvIiwibGF5ZXJfc3BlZWQiOiIzNTAiLCJsYXllcl90b3AiOiI2NSIsImxheWVyX2xlZnQiOiI0MzgiLCJsYXllcl9lbmR0aW1lIjoiMCIsImxheWVyX2VuZHNwZWVkIjoiMzAwIiwibGF5ZXJfZW5kYW5pbWF0aW9uIjoiYXV0byIsImxheWVyX2VuZGVhc2luZyI6Im5vdGhpbmciLCJ0aW1lX3N0YXJ0IjoiMjAwIn0seyJsYXllcl92aWRlb190eXBlIjoieW91dHViZSIsImxheWVyX3ZpZGVvX2lkIjoiIiwibGF5ZXJfdmlkZW9faGVpZ2h0IjoiMjAwIiwibGF5ZXJfdmlkZW9fd2lkdGgiOiIzMDAiLCJsYXllcl92aWRlb190aHVtYiI6IiIsImxheWVyX2lkIjoiMV81IiwibGF5ZXJfY29udGVudCI6ImxheWVyMi5wbmciLCJsYXllcl90eXBlIjoidGV4dCIsImxheWVyX2NsYXNzIjoiIiwibGF5ZXJfY2FwdGlvbiI6IjxzcGFuIHN0eWxlPVwidGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZVwiPmZyb206ICQ3NTAuMDA8XC9zcGFuPiIsImxheWVyX2ZvbnRfc2l6ZSI6IjE4cHgiLCJsYXllcl9iYWNrZ3JvdW5kX2NvbG9yIjoiIiwibGF5ZXJfY29sb3IiOiIjNDljYWU2IiwibGF5ZXJfbGluayI6IiIsImxheWVyX2FuaW1hdGlvbiI6ImZhZGUiLCJsYXllcl9lYXNpbmciOiJlYXNlSW5RdWFkIiwibGF5ZXJfc3BlZWQiOiIzNTAiLCJsYXllcl90b3AiOiIyNTciLCJsYXllcl9sZWZ0IjoiNjAiLCJsYXllcl9lbmR0aW1lIjoiMCIsImxheWVyX2VuZHNwZWVkIjoiMzAwIiwibGF5ZXJfZW5kYW5pbWF0aW9uIjoiYXV0byIsImxheWVyX2VuZGVhc2luZyI6Im5vdGhpbmciLCJ0aW1lX3N0YXJ0IjoiMjQwMCJ9XQ=='),
	(3, 1, '', '', '', '', 'eyJ1c2V2aWRlbyI6ZmFsc2UsInZpZGVvaWQiOmZhbHNlLCJ2aWRlb2F1dG8iOmZhbHNlLCJiYWNrZ3JvdW5kX2NvbG9yIjoiIn0=', ''),
	(4, 1, '', '', '', '', 'eyJ1c2V2aWRlbyI6ZmFsc2UsInZpZGVvaWQiOmZhbHNlLCJ2aWRlb2F1dG8iOmZhbHNlLCJiYWNrZ3JvdW5kX2NvbG9yIjoiIn0=', ''),
	(16, 1, 'slidershow-gift1', 'http://prestashop.com', 'bg-gift.jpg', '', 'eyJ1c2V2aWRlbyI6IjAiLCJ2aWRlb2lkIjoiIiwidmlkZW9hdXRvIjoiMSIsImJhY2tncm91bmRfY29sb3IiOiIjZmZmZmZmIn0=', 'W3sibGF5ZXJfdmlkZW9fdHlwZSI6InlvdXR1YmUiLCJsYXllcl92aWRlb19pZCI6IiIsImxheWVyX3ZpZGVvX2hlaWdodCI6IjIwMCIsImxheWVyX3ZpZGVvX3dpZHRoIjoiMzAwIiwibGF5ZXJfdmlkZW9fdGh1bWIiOiIiLCJsYXllcl9pZCI6IjFfMSIsImxheWVyX2NvbnRlbnQiOiJzbC1nZjEuanBnIiwibGF5ZXJfdHlwZSI6ImltYWdlIiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiWW91ciBJbWFnZSBIZXJlIDYiLCJsYXllcl9mb250X3NpemUiOiIiLCJsYXllcl9iYWNrZ3JvdW5kX2NvbG9yIjoiIiwibGF5ZXJfY29sb3IiOiIiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoicmFuZG9tcm90YXRlIiwibGF5ZXJfZWFzaW5nIjoiZWFzZU91dEJhY2siLCJsYXllcl9zcGVlZCI6IjM1MCIsImxheWVyX3RvcCI6IjAiLCJsYXllcl9sZWZ0IjoiMCIsImxheWVyX2VuZHRpbWUiOiIwIiwibGF5ZXJfZW5kc3BlZWQiOiIzMDAiLCJsYXllcl9lbmRhbmltYXRpb24iOiJhdXRvIiwibGF5ZXJfZW5kZWFzaW5nIjoibm90aGluZyIsInRpbWVfc3RhcnQiOiI0MDAifSx7ImxheWVyX3ZpZGVvX3R5cGUiOiJ5b3V0dWJlIiwibGF5ZXJfdmlkZW9faWQiOiIiLCJsYXllcl92aWRlb19oZWlnaHQiOiIyMDAiLCJsYXllcl92aWRlb193aWR0aCI6IjMwMCIsImxheWVyX3ZpZGVvX3RodW1iIjoiIiwibGF5ZXJfaWQiOiIxXzIiLCJsYXllcl9jb250ZW50IjoibGF5ZXIyLnBuZyIsImxheWVyX3R5cGUiOiJ0ZXh0IiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiPHNwYW4gc3R5bGU9XCIgIGxldHRlci1zcGFjaW5nOi0zcHg7Zm9udC13ZWlnaHQ6IDcwMDsgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcIj5iaWcgc2FsZTxcL3NwYW4+IiwibGF5ZXJfZm9udF9zaXplIjoiOTBweCIsImxheWVyX2JhY2tncm91bmRfY29sb3IiOiIiLCJsYXllcl9jb2xvciI6IiNmOGMyNTUiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoic2ZyIiwibGF5ZXJfZWFzaW5nIjoiZWFzZU91dEN1YmljIiwibGF5ZXJfc3BlZWQiOiIzNTAiLCJsYXllcl90b3AiOiIxMjQiLCJsYXllcl9sZWZ0IjoiNzM1IiwibGF5ZXJfZW5kdGltZSI6IjAiLCJsYXllcl9lbmRzcGVlZCI6IjMwMCIsImxheWVyX2VuZGFuaW1hdGlvbiI6ImF1dG8iLCJsYXllcl9lbmRlYXNpbmciOiJub3RoaW5nIiwidGltZV9zdGFydCI6IjE4MDAifSx7ImxheWVyX3ZpZGVvX3R5cGUiOiJ5b3V0dWJlIiwibGF5ZXJfdmlkZW9faWQiOiIiLCJsYXllcl92aWRlb19oZWlnaHQiOiIyMDAiLCJsYXllcl92aWRlb193aWR0aCI6IjMwMCIsImxheWVyX3ZpZGVvX3RodW1iIjoiIiwibGF5ZXJfaWQiOiIxXzMiLCJsYXllcl9jb250ZW50IjoibGF5ZXIyLnBuZyIsImxheWVyX3R5cGUiOiJ0ZXh0IiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiPHNwYW4gc3R5bGU9XCIgZm9udC13ZWlnaHQ6IDcwMDsgIGZvbnQtc3R5bGU6IGl0YWxpYzsgbGV0dGVyLXNwYWNpbmc6LTFweFwiPkNocmlzdG1hczxcL3NwYW4+IiwibGF5ZXJfZm9udF9zaXplIjoiMzBweCIsImxheWVyX2JhY2tncm91bmRfY29sb3IiOiIiLCJsYXllcl9jb2xvciI6IiNmZjliNmQiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoic2ZyIiwibGF5ZXJfZWFzaW5nIjoiZWFzZU91dFF1YWQiLCJsYXllcl9zcGVlZCI6IjM1MCIsImxheWVyX3RvcCI6IjExNyIsImxheWVyX2xlZnQiOiI3MzUiLCJsYXllcl9lbmR0aW1lIjoiMCIsImxheWVyX2VuZHNwZWVkIjoiMzAwIiwibGF5ZXJfZW5kYW5pbWF0aW9uIjoiYXV0byIsImxheWVyX2VuZGVhc2luZyI6Im5vdGhpbmciLCJ0aW1lX3N0YXJ0IjoiMTIwMCJ9LHsibGF5ZXJfdmlkZW9fdHlwZSI6InlvdXR1YmUiLCJsYXllcl92aWRlb19pZCI6IiIsImxheWVyX3ZpZGVvX2hlaWdodCI6IjIwMCIsImxheWVyX3ZpZGVvX3dpZHRoIjoiMzAwIiwibGF5ZXJfdmlkZW9fdGh1bWIiOiIiLCJsYXllcl9pZCI6IjFfNCIsImxheWVyX2NvbnRlbnQiOiJsYXllcjIucG5nIiwibGF5ZXJfdHlwZSI6InRleHQiLCJsYXllcl9jbGFzcyI6IiIsImxheWVyX2NhcHRpb24iOiI8c3BhbiBzdHlsZT1cIiAgbGV0dGVyLXNwYWNpbmc6LTNweDsgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcIj51cCB0byA8c3Ryb25nPjUwJSA8XC9zdHJvbmc+b2ZmPFwvc3Bhbj4iLCJsYXllcl9mb250X3NpemUiOiI1MHB4IiwibGF5ZXJfYmFja2dyb3VuZF9jb2xvciI6IiIsImxheWVyX2NvbG9yIjoiIzAwMDAwMCIsImxheWVyX2xpbmsiOiIiLCJsYXllcl9hbmltYXRpb24iOiJzZnIiLCJsYXllcl9lYXNpbmciOiJlYXNlSW5PdXRRdWFydCIsImxheWVyX3NwZWVkIjoiMzUwIiwibGF5ZXJfdG9wIjoiMjIxIiwibGF5ZXJfbGVmdCI6IjczNSIsImxheWVyX2VuZHRpbWUiOiIwIiwibGF5ZXJfZW5kc3BlZWQiOiIzMDAiLCJsYXllcl9lbmRhbmltYXRpb24iOiJhdXRvIiwibGF5ZXJfZW5kZWFzaW5nIjoibm90aGluZyIsInRpbWVfc3RhcnQiOiIyNjAwIn0seyJsYXllcl92aWRlb190eXBlIjoieW91dHViZSIsImxheWVyX3ZpZGVvX2lkIjoiIiwibGF5ZXJfdmlkZW9faGVpZ2h0IjoiMjAwIiwibGF5ZXJfdmlkZW9fd2lkdGgiOiIzMDAiLCJsYXllcl92aWRlb190aHVtYiI6IiIsImxheWVyX2lkIjoiMV81IiwibGF5ZXJfY29udGVudCI6ImxheWVyMi5wbmciLCJsYXllcl90eXBlIjoidGV4dCIsImxheWVyX2NsYXNzIjoiIiwibGF5ZXJfY2FwdGlvbiI6IjxzcGFuIHN0eWxlPVwidGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcIj4gdGhpcyBvZmZmZXIgaXMgdmFsaWQgb24gYWxsIG91ciBzdG9yZSBpdGVtczxcL3NwYW4+IiwibGF5ZXJfZm9udF9zaXplIjoiMTJweCIsImxheWVyX2JhY2tncm91bmRfY29sb3IiOiIiLCJsYXllcl9jb2xvciI6IiMwMDAwMDAiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoic2ZyIiwibGF5ZXJfZWFzaW5nIjoiZWFzZUluT3V0U2luZSIsImxheWVyX3NwZWVkIjoiMzUwIiwibGF5ZXJfdG9wIjoiMjg4IiwibGF5ZXJfbGVmdCI6IjczNSIsImxheWVyX2VuZHRpbWUiOiIwIiwibGF5ZXJfZW5kc3BlZWQiOiIzMDAiLCJsYXllcl9lbmRhbmltYXRpb24iOiJhdXRvIiwibGF5ZXJfZW5kZWFzaW5nIjoibm90aGluZyIsInRpbWVfc3RhcnQiOiIzMDAwIn0seyJsYXllcl92aWRlb190eXBlIjoieW91dHViZSIsImxheWVyX3ZpZGVvX2lkIjoiIiwibGF5ZXJfdmlkZW9faGVpZ2h0IjoiMjAwIiwibGF5ZXJfdmlkZW9fd2lkdGgiOiIzMDAiLCJsYXllcl92aWRlb190aHVtYiI6IiIsImxheWVyX2lkIjoiMV82IiwibGF5ZXJfY29udGVudCI6ImxheWVyMi5wbmciLCJsYXllcl90eXBlIjoidGV4dCIsImxheWVyX2NsYXNzIjoiIiwibGF5ZXJfY2FwdGlvbiI6IjxhIGNsYXNzPVwiYnRuIGJ0bi1vdXRsaW5lXCI+dmlldyBjb2xsZWN0aW9uPFwvYT4iLCJsYXllcl9mb250X3NpemUiOiIiLCJsYXllcl9iYWNrZ3JvdW5kX2NvbG9yIjoiIiwibGF5ZXJfY29sb3IiOiIiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoic2ZyIiwibGF5ZXJfZWFzaW5nIjoiZWFzZUluT3V0RWxhc3RpYyIsImxheWVyX3NwZWVkIjoiMzUwIiwibGF5ZXJfdG9wIjoiMzQ2IiwibGF5ZXJfbGVmdCI6IjczNSIsImxheWVyX2VuZHRpbWUiOiIwIiwibGF5ZXJfZW5kc3BlZWQiOiIzMDAiLCJsYXllcl9lbmRhbmltYXRpb24iOiJhdXRvIiwibGF5ZXJfZW5kZWFzaW5nIjoibm90aGluZyIsInRpbWVfc3RhcnQiOiIzNjAwIn1d'),
	(19, 1, 'Slide-sport1', '', 'slideshow-sport3.jpg', '', 'eyJ1c2V2aWRlbyI6IjAiLCJ2aWRlb2lkIjoiIiwidmlkZW9hdXRvIjoiMCIsImJhY2tncm91bmRfY29sb3IiOiIifQ==', 'W3sibGF5ZXJfdmlkZW9fdHlwZSI6InlvdXR1YmUiLCJsYXllcl92aWRlb19pZCI6IiIsImxheWVyX3ZpZGVvX2hlaWdodCI6IjIwMCIsImxheWVyX3ZpZGVvX3dpZHRoIjoiMzAwIiwibGF5ZXJfdmlkZW9fdGh1bWIiOiIiLCJsYXllcl9pZCI6IjFfMSIsImxheWVyX2NvbnRlbnQiOiIiLCJsYXllcl90eXBlIjoidGV4dCIsImxheWVyX2NsYXNzIjoiIiwibGF5ZXJfY2FwdGlvbiI6IjxzcGFuIHN0eWxlPVwiIGZvbnQtd2VpZ2h0OiAxMDA7IGxldHRlci1zcGFjaW5nOiAtN3B4O3RleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XCI+U2hvcCBtZW5zPFwvc3Bhbj4iLCJsYXllcl9mb250X3NpemUiOiI4MHB4IiwibGF5ZXJfYmFja2dyb3VuZF9jb2xvciI6IiIsImxheWVyX2NvbG9yIjoiI2ZmZmZmZiIsImxheWVyX2xpbmsiOiIiLCJsYXllcl9hbmltYXRpb24iOiJmYWRlIiwibGF5ZXJfZWFzaW5nIjoiZWFzZU91dEV4cG8iLCJsYXllcl9zcGVlZCI6IjM1MCIsImxheWVyX3RvcCI6IjE0MCIsImxheWVyX2xlZnQiOiI3MzAiLCJsYXllcl9lbmR0aW1lIjoiMCIsImxheWVyX2VuZHNwZWVkIjoiMzAwIiwibGF5ZXJfZW5kYW5pbWF0aW9uIjoiYXV0byIsImxheWVyX2VuZGVhc2luZyI6Im5vdGhpbmciLCJ0aW1lX3N0YXJ0IjoiNDAwIn0seyJsYXllcl92aWRlb190eXBlIjoieW91dHViZSIsImxheWVyX3ZpZGVvX2lkIjoiIiwibGF5ZXJfdmlkZW9faGVpZ2h0IjoiMjAwIiwibGF5ZXJfdmlkZW9fd2lkdGgiOiIzMDAiLCJsYXllcl92aWRlb190aHVtYiI6IiIsImxheWVyX2lkIjoiMV8yIiwibGF5ZXJfY29udGVudCI6IiIsImxheWVyX3R5cGUiOiJ0ZXh0IiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiPHNwYW4gc3R5bGU9XCIgZm9udC13ZWlnaHQ6IDkwMDsgbGV0dGVyLXNwYWNpbmc6IC03cHg7dGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcIj5vY3RvYmVyPFwvc3Bhbj4iLCJsYXllcl9mb250X3NpemUiOiIxMDBweCIsImxheWVyX2JhY2tncm91bmRfY29sb3IiOiIiLCJsYXllcl9jb2xvciI6IiNmZmZmZmYiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoiZmFkZSIsImxheWVyX2Vhc2luZyI6ImVhc2VPdXRFeHBvIiwibGF5ZXJfc3BlZWQiOiIzNTAiLCJsYXllcl90b3AiOiIyMjAiLCJsYXllcl9sZWZ0IjoiNzMwIiwibGF5ZXJfZW5kdGltZSI6IjAiLCJsYXllcl9lbmRzcGVlZCI6IjMwMCIsImxheWVyX2VuZGFuaW1hdGlvbiI6ImF1dG8iLCJsYXllcl9lbmRlYXNpbmciOiJub3RoaW5nIiwidGltZV9zdGFydCI6IjgwMCJ9LHsibGF5ZXJfdmlkZW9fdHlwZSI6InlvdXR1YmUiLCJsYXllcl92aWRlb19pZCI6IiIsImxheWVyX3ZpZGVvX2hlaWdodCI6IjIwMCIsImxheWVyX3ZpZGVvX3dpZHRoIjoiMzAwIiwibGF5ZXJfdmlkZW9fdGh1bWIiOiIiLCJsYXllcl9pZCI6IjFfMyIsImxheWVyX2NvbnRlbnQiOiIiLCJsYXllcl90eXBlIjoidGV4dCIsImxheWVyX2NsYXNzIjoiIiwibGF5ZXJfY2FwdGlvbiI6IjxzcGFuIHN0eWxlPVwiIGZvbnQtd2VpZ2h0OiAxMDA7IGxldHRlci1zcGFjaW5nOiAtN3B4O3RleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2VcIj5DYXRhbG9nPFwvc3Bhbj4iLCJsYXllcl9mb250X3NpemUiOiI4MHB4IiwibGF5ZXJfYmFja2dyb3VuZF9jb2xvciI6IiIsImxheWVyX2NvbG9yIjoiI2ZmZmZmZiIsImxheWVyX2xpbmsiOiIiLCJsYXllcl9hbmltYXRpb24iOiJmYWRlIiwibGF5ZXJfZWFzaW5nIjoiZWFzZU91dEV4cG8iLCJsYXllcl9zcGVlZCI6IjM1MCIsImxheWVyX3RvcCI6IjMzNSIsImxheWVyX2xlZnQiOiI3MzAiLCJsYXllcl9lbmR0aW1lIjoiMCIsImxheWVyX2VuZHNwZWVkIjoiMzAwIiwibGF5ZXJfZW5kYW5pbWF0aW9uIjoiYXV0byIsImxheWVyX2VuZGVhc2luZyI6Im5vdGhpbmciLCJ0aW1lX3N0YXJ0IjoiMTIwMCJ9XQ=='),
	(23, 1, 'slide fashion1', '', 'Image_home.png', '', 'eyJ1c2V2aWRlbyI6IjAiLCJ2aWRlb2lkIjoiIiwidmlkZW9hdXRvIjoiMCIsImJhY2tncm91bmRfY29sb3IiOiIifQ==', 'W3sibGF5ZXJfdmlkZW9fdHlwZSI6InlvdXR1YmUiLCJsYXllcl92aWRlb19pZCI6IiIsImxheWVyX3ZpZGVvX2hlaWdodCI6IjIwMCIsImxheWVyX3ZpZGVvX3dpZHRoIjoiMzAwIiwibGF5ZXJfdmlkZW9fdGh1bWIiOiIiLCJsYXllcl9pZCI6IjFfMSIsImxheWVyX2NvbnRlbnQiOiJCb3Rvbi5wbmciLCJsYXllcl90eXBlIjoiaW1hZ2UiLCJsYXllcl9jbGFzcyI6IiIsImxheWVyX2NhcHRpb24iOiJZb3VyIEltYWdlIEhlcmUgNyIsImxheWVyX2ZvbnRfc2l6ZSI6IiIsImxheWVyX2JhY2tncm91bmRfY29sb3IiOiIiLCJsYXllcl9jb2xvciI6IiIsImxheWVyX2xpbmsiOiIiLCJsYXllcl9hbmltYXRpb24iOiJsZnIiLCJsYXllcl9lYXNpbmciOiJlYXNlT3V0RXhwbyIsImxheWVyX3NwZWVkIjoiMzUwIiwibGF5ZXJfdG9wIjoiLTk3OSIsImxheWVyX2xlZnQiOiIwIiwibGF5ZXJfZW5kdGltZSI6IjAiLCJsYXllcl9lbmRzcGVlZCI6IjMwMCIsImxheWVyX2VuZGFuaW1hdGlvbiI6ImF1dG8iLCJsYXllcl9lbmRlYXNpbmciOiJub3RoaW5nIiwidGltZV9zdGFydCI6IjI4MDAifV0='),
	(28, 1, 'Slider 2', 'http://prestashop.com', 'bg-sl-mb.jpg', '', 'eyJ1c2V2aWRlbyI6IjAiLCJ2aWRlb2lkIjoiIiwidmlkZW9hdXRvIjoiMSIsImJhY2tncm91bmRfY29sb3IiOiIjMjkyOTJiIn0=', 'W3sibGF5ZXJfdmlkZW9fdHlwZSI6InlvdXR1YmUiLCJsYXllcl92aWRlb19pZCI6IiIsImxheWVyX3ZpZGVvX2hlaWdodCI6IjIwMCIsImxheWVyX3ZpZGVvX3dpZHRoIjoiMzAwIiwibGF5ZXJfdmlkZW9fdGh1bWIiOiIiLCJsYXllcl9pZCI6IjFfMSIsImxheWVyX2NvbnRlbnQiOiJsYXllcjIucG5nIiwibGF5ZXJfdHlwZSI6InRleHQiLCJsYXllcl9jbGFzcyI6IiIsImxheWVyX2NhcHRpb24iOiI8aDIgc3R5bGU9XCJsZXR0ZXItc3BhY2luZzotM3B4OyAgICBmb250LXdlaWdodDogMzAwO1wiPk1hZWNlbjxzdHJvbmcgc3R5bGU9XCJ0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlOyBmb250LXdlaWdodDogOTAwO1wiPiBzYWdpdCA8XC9zdHJvbmc+PFwvaDI+IiwibGF5ZXJfZm9udF9zaXplIjoiNjBweCIsImxheWVyX2JhY2tncm91bmRfY29sb3IiOiIiLCJsYXllcl9jb2xvciI6IiNmZmZmZmYiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoic2ZsIiwibGF5ZXJfZWFzaW5nIjoiZWFzZUluRXhwbyIsImxheWVyX3NwZWVkIjoiMzUwIiwibGF5ZXJfdG9wIjoiMTE2IiwibGF5ZXJfbGVmdCI6IjYwIiwibGF5ZXJfZW5kdGltZSI6IjAiLCJsYXllcl9lbmRzcGVlZCI6IjMwMCIsImxheWVyX2VuZGFuaW1hdGlvbiI6ImF1dG8iLCJsYXllcl9lbmRlYXNpbmciOiJub3RoaW5nIiwidGltZV9zdGFydCI6IjEyMDAifSx7ImxheWVyX3ZpZGVvX3R5cGUiOiJ5b3V0dWJlIiwibGF5ZXJfdmlkZW9faWQiOiIiLCJsYXllcl92aWRlb19oZWlnaHQiOiIyMDAiLCJsYXllcl92aWRlb193aWR0aCI6IjMwMCIsImxheWVyX3ZpZGVvX3RodW1iIjoiIiwibGF5ZXJfaWQiOiIxXzIiLCJsYXllcl9jb250ZW50IjoibGF5ZXIyLnBuZyIsImxheWVyX3R5cGUiOiJ0ZXh0IiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiTWFlY2VuYXMgc2FnaXR0aXMgY29uZ3VlIGFyY3UsIHV0IG1ldHVzIHV0IHNhZ2l0dGlzPGJyPiB1bHRyaWNlcyBudW5jIHJob25jdXMgc29kYWxlcyIsImxheWVyX2ZvbnRfc2l6ZSI6IjE0cHgiLCJsYXllcl9iYWNrZ3JvdW5kX2NvbG9yIjoiIiwibGF5ZXJfY29sb3IiOiIiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoic2ZsIiwibGF5ZXJfZWFzaW5nIjoiZWFzZUluQ2lyYyIsImxheWVyX3NwZWVkIjoiMzUwIiwibGF5ZXJfdG9wIjoiMTk0IiwibGF5ZXJfbGVmdCI6IjYwIiwibGF5ZXJfZW5kdGltZSI6IjAiLCJsYXllcl9lbmRzcGVlZCI6IjMwMCIsImxheWVyX2VuZGFuaW1hdGlvbiI6ImF1dG8iLCJsYXllcl9lbmRlYXNpbmciOiJub3RoaW5nIiwidGltZV9zdGFydCI6IjIwMDAifSx7ImxheWVyX3ZpZGVvX3R5cGUiOiJ5b3V0dWJlIiwibGF5ZXJfdmlkZW9faWQiOiIiLCJsYXllcl92aWRlb19oZWlnaHQiOiIyMDAiLCJsYXllcl92aWRlb193aWR0aCI6IjMwMCIsImxheWVyX3ZpZGVvX3RodW1iIjoiIiwibGF5ZXJfaWQiOiIxXzMiLCJsYXllcl9jb250ZW50IjoibGF5ZXIyLnBuZyIsImxheWVyX3R5cGUiOiJ0ZXh0IiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiPGEgaHJlZj1cIiNcIiBjbGFzcz1cImJ0blwiICBzdHlsZT1cImNvbG9yOiAjZmZmO2JvcmRlci1jb2xvcjojNTQ1NDU1OyAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcIj52aWV3IGNvbGxlY3Rpb248XC9hPiIsImxheWVyX2ZvbnRfc2l6ZSI6IiIsImxheWVyX2JhY2tncm91bmRfY29sb3IiOiIiLCJsYXllcl9jb2xvciI6IiIsImxheWVyX2xpbmsiOiIiLCJsYXllcl9hbmltYXRpb24iOiJzZmwiLCJsYXllcl9lYXNpbmciOiJlYXNlSW5DdWJpYyIsImxheWVyX3NwZWVkIjoiMzUwIiwibGF5ZXJfdG9wIjoiMzEwIiwibGF5ZXJfbGVmdCI6IjYwIiwibGF5ZXJfZW5kdGltZSI6IjAiLCJsYXllcl9lbmRzcGVlZCI6IjMwMCIsImxheWVyX2VuZGFuaW1hdGlvbiI6ImF1dG8iLCJsYXllcl9lbmRlYXNpbmciOiJlYXNlSW5TaW5lIiwidGltZV9zdGFydCI6IjI4MDAifSx7ImxheWVyX3ZpZGVvX3R5cGUiOiJ5b3V0dWJlIiwibGF5ZXJfdmlkZW9faWQiOiIiLCJsYXllcl92aWRlb19oZWlnaHQiOiIyMDAiLCJsYXllcl92aWRlb193aWR0aCI6IjMwMCIsImxheWVyX3ZpZGVvX3RodW1iIjoiIiwibGF5ZXJfaWQiOiIxXzQiLCJsYXllcl9jb250ZW50Ijoic2wtbWItMi5wbmciLCJsYXllcl90eXBlIjoiaW1hZ2UiLCJsYXllcl9jbGFzcyI6IiIsImxheWVyX2NhcHRpb24iOiJZb3VyIEltYWdlIEhlcmUgNCIsImxheWVyX2ZvbnRfc2l6ZSI6IiIsImxheWVyX2JhY2tncm91bmRfY29sb3IiOiIiLCJsYXllcl9jb2xvciI6IiIsImxheWVyX2xpbmsiOiIiLCJsYXllcl9hbmltYXRpb24iOiJyYW5kb21yb3RhdGUiLCJsYXllcl9lYXNpbmciOiJlYXNlT3V0RXhwbyIsImxheWVyX3NwZWVkIjoiMzUwIiwibGF5ZXJfdG9wIjoiNjUiLCJsYXllcl9sZWZ0IjoiNDM4IiwibGF5ZXJfZW5kdGltZSI6IjAiLCJsYXllcl9lbmRzcGVlZCI6IjMwMCIsImxheWVyX2VuZGFuaW1hdGlvbiI6ImF1dG8iLCJsYXllcl9lbmRlYXNpbmciOiJub3RoaW5nIiwidGltZV9zdGFydCI6IjIwMCJ9LHsibGF5ZXJfdmlkZW9fdHlwZSI6InlvdXR1YmUiLCJsYXllcl92aWRlb19pZCI6IiIsImxheWVyX3ZpZGVvX2hlaWdodCI6IjIwMCIsImxheWVyX3ZpZGVvX3dpZHRoIjoiMzAwIiwibGF5ZXJfdmlkZW9fdGh1bWIiOiIiLCJsYXllcl9pZCI6IjFfNSIsImxheWVyX2NvbnRlbnQiOiJsYXllcjIucG5nIiwibGF5ZXJfdHlwZSI6InRleHQiLCJsYXllcl9jbGFzcyI6IiIsImxheWVyX2NhcHRpb24iOiI8c3BhbiBzdHlsZT1cInRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2VcIj5mcm9tOiAkNzAwLjAwPFwvc3Bhbj4iLCJsYXllcl9mb250X3NpemUiOiIxOHB4IiwibGF5ZXJfYmFja2dyb3VuZF9jb2xvciI6IiIsImxheWVyX2NvbG9yIjoiIzQ5Y2FlNiIsImxheWVyX2xpbmsiOiIiLCJsYXllcl9hbmltYXRpb24iOiJzZmwiLCJsYXllcl9lYXNpbmciOiJlYXNlSW5RdWFkIiwibGF5ZXJfc3BlZWQiOiIzNTAiLCJsYXllcl90b3AiOiIyNTciLCJsYXllcl9sZWZ0IjoiNjAiLCJsYXllcl9lbmR0aW1lIjoiMCIsImxheWVyX2VuZHNwZWVkIjoiMzAwIiwibGF5ZXJfZW5kYW5pbWF0aW9uIjoiYXV0byIsImxheWVyX2VuZGVhc2luZyI6Im5vdGhpbmciLCJ0aW1lX3N0YXJ0IjoiMjQwMCJ9XQ=='),
	(29, 1, 'Slider 3', 'http://prestashop.com', 'bg-sl-mb.jpg', '', 'eyJ1c2V2aWRlbyI6IjAiLCJ2aWRlb2lkIjoiIiwidmlkZW9hdXRvIjoiMSIsImJhY2tncm91bmRfY29sb3IiOiIjMjkyOTJiIn0=', 'W3sibGF5ZXJfdmlkZW9fdHlwZSI6InlvdXR1YmUiLCJsYXllcl92aWRlb19pZCI6IiIsImxheWVyX3ZpZGVvX2hlaWdodCI6IjIwMCIsImxheWVyX3ZpZGVvX3dpZHRoIjoiMzAwIiwibGF5ZXJfdmlkZW9fdGh1bWIiOiIiLCJsYXllcl9pZCI6IjFfMSIsImxheWVyX2NvbnRlbnQiOiJsYXllcjIucG5nIiwibGF5ZXJfdHlwZSI6InRleHQiLCJsYXllcl9jbGFzcyI6IiIsImxheWVyX2NhcHRpb24iOiI8aDIgc3R5bGU9XCJsZXR0ZXItc3BhY2luZzotM3B4OyAgICBmb250LXdlaWdodDogMzAwO1wiPk1hZWNlbjxzdHJvbmcgc3R5bGU9XCJ0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlOyBmb250LXdlaWdodDogOTAwO1wiPiBzYWdpdCA8XC9zdHJvbmc+PFwvaDI+IiwibGF5ZXJfZm9udF9zaXplIjoiNjBweCIsImxheWVyX2JhY2tncm91bmRfY29sb3IiOiIiLCJsYXllcl9jb2xvciI6IiNmZmZmZmYiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoiZmFkZSIsImxheWVyX2Vhc2luZyI6ImVhc2VJbkV4cG8iLCJsYXllcl9zcGVlZCI6IjM1MCIsImxheWVyX3RvcCI6IjExNiIsImxheWVyX2xlZnQiOiI2MCIsImxheWVyX2VuZHRpbWUiOiIwIiwibGF5ZXJfZW5kc3BlZWQiOiIzMDAiLCJsYXllcl9lbmRhbmltYXRpb24iOiJhdXRvIiwibGF5ZXJfZW5kZWFzaW5nIjoibm90aGluZyIsInRpbWVfc3RhcnQiOiIxMjAwIn0seyJsYXllcl92aWRlb190eXBlIjoieW91dHViZSIsImxheWVyX3ZpZGVvX2lkIjoiIiwibGF5ZXJfdmlkZW9faGVpZ2h0IjoiMjAwIiwibGF5ZXJfdmlkZW9fd2lkdGgiOiIzMDAiLCJsYXllcl92aWRlb190aHVtYiI6IiIsImxheWVyX2lkIjoiMV8yIiwibGF5ZXJfY29udGVudCI6ImxheWVyMi5wbmciLCJsYXllcl90eXBlIjoidGV4dCIsImxheWVyX2NsYXNzIjoiIiwibGF5ZXJfY2FwdGlvbiI6Ik1hZWNlbmFzIHNhZ2l0dGlzIGNvbmd1ZSBhcmN1LCB1dCBtZXR1cyB1dCBzYWdpdHRpczxicj4gdWx0cmljZXMgbnVuYyByaG9uY3VzIHNvZGFsZXMiLCJsYXllcl9mb250X3NpemUiOiIxNHB4IiwibGF5ZXJfYmFja2dyb3VuZF9jb2xvciI6IiIsImxheWVyX2NvbG9yIjoiIiwibGF5ZXJfbGluayI6IiIsImxheWVyX2FuaW1hdGlvbiI6InNmciIsImxheWVyX2Vhc2luZyI6ImVhc2VJbkNpcmMiLCJsYXllcl9zcGVlZCI6IjM1MCIsImxheWVyX3RvcCI6IjE5NCIsImxheWVyX2xlZnQiOiI2MCIsImxheWVyX2VuZHRpbWUiOiIwIiwibGF5ZXJfZW5kc3BlZWQiOiIzMDAiLCJsYXllcl9lbmRhbmltYXRpb24iOiJhdXRvIiwibGF5ZXJfZW5kZWFzaW5nIjoibm90aGluZyIsInRpbWVfc3RhcnQiOiIyMDAwIn0seyJsYXllcl92aWRlb190eXBlIjoieW91dHViZSIsImxheWVyX3ZpZGVvX2lkIjoiIiwibGF5ZXJfdmlkZW9faGVpZ2h0IjoiMjAwIiwibGF5ZXJfdmlkZW9fd2lkdGgiOiIzMDAiLCJsYXllcl92aWRlb190aHVtYiI6IiIsImxheWVyX2lkIjoiMV8zIiwibGF5ZXJfY29udGVudCI6ImxheWVyMi5wbmciLCJsYXllcl90eXBlIjoidGV4dCIsImxheWVyX2NsYXNzIjoiIiwibGF5ZXJfY2FwdGlvbiI6IjxhIGhyZWY9XCIjXCIgY2xhc3M9XCJidG5cIiAgc3R5bGU9XCJjb2xvcjogI2ZmZjtib3JkZXItY29sb3I6IzU0NTQ1NTsgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XCI+dmlldyBjb2xsZWN0aW9uPFwvYT4iLCJsYXllcl9mb250X3NpemUiOiIiLCJsYXllcl9iYWNrZ3JvdW5kX2NvbG9yIjoiIiwibGF5ZXJfY29sb3IiOiIiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoic2ZiIiwibGF5ZXJfZWFzaW5nIjoiZWFzZUluQ3ViaWMiLCJsYXllcl9zcGVlZCI6IjM1MCIsImxheWVyX3RvcCI6IjMxMCIsImxheWVyX2xlZnQiOiI2MCIsImxheWVyX2VuZHRpbWUiOiIwIiwibGF5ZXJfZW5kc3BlZWQiOiIzMDAiLCJsYXllcl9lbmRhbmltYXRpb24iOiJhdXRvIiwibGF5ZXJfZW5kZWFzaW5nIjoiZWFzZUluU2luZSIsInRpbWVfc3RhcnQiOiIyODAwIn0seyJsYXllcl92aWRlb190eXBlIjoieW91dHViZSIsImxheWVyX3ZpZGVvX2lkIjoiIiwibGF5ZXJfdmlkZW9faGVpZ2h0IjoiMjAwIiwibGF5ZXJfdmlkZW9fd2lkdGgiOiIzMDAiLCJsYXllcl92aWRlb190aHVtYiI6IiIsImxheWVyX2lkIjoiMV80IiwibGF5ZXJfY29udGVudCI6InNsLW1iLTMucG5nIiwibGF5ZXJfdHlwZSI6ImltYWdlIiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiWW91ciBJbWFnZSBIZXJlIDQiLCJsYXllcl9mb250X3NpemUiOiIiLCJsYXllcl9iYWNrZ3JvdW5kX2NvbG9yIjoiIiwibGF5ZXJfY29sb3IiOiIiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoiZmFkZSIsImxheWVyX2Vhc2luZyI6ImVhc2VPdXRFeHBvIiwibGF5ZXJfc3BlZWQiOiIzNTAiLCJsYXllcl90b3AiOiI2NSIsImxheWVyX2xlZnQiOiI0MzgiLCJsYXllcl9lbmR0aW1lIjoiMCIsImxheWVyX2VuZHNwZWVkIjoiMzAwIiwibGF5ZXJfZW5kYW5pbWF0aW9uIjoiYXV0byIsImxheWVyX2VuZGVhc2luZyI6Im5vdGhpbmciLCJ0aW1lX3N0YXJ0IjoiMjAwIn0seyJsYXllcl92aWRlb190eXBlIjoieW91dHViZSIsImxheWVyX3ZpZGVvX2lkIjoiIiwibGF5ZXJfdmlkZW9faGVpZ2h0IjoiMjAwIiwibGF5ZXJfdmlkZW9fd2lkdGgiOiIzMDAiLCJsYXllcl92aWRlb190aHVtYiI6IiIsImxheWVyX2lkIjoiMV81IiwibGF5ZXJfY29udGVudCI6ImxheWVyMi5wbmciLCJsYXllcl90eXBlIjoidGV4dCIsImxheWVyX2NsYXNzIjoiIiwibGF5ZXJfY2FwdGlvbiI6IjxzcGFuIHN0eWxlPVwidGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZVwiPmZyb206ICQ3MDAuMDA8XC9zcGFuPiIsImxheWVyX2ZvbnRfc2l6ZSI6IjE4cHgiLCJsYXllcl9iYWNrZ3JvdW5kX2NvbG9yIjoiIiwibGF5ZXJfY29sb3IiOiIjNDljYWU2IiwibGF5ZXJfbGluayI6IiIsImxheWVyX2FuaW1hdGlvbiI6InNmbCIsImxheWVyX2Vhc2luZyI6ImVhc2VJblF1YWQiLCJsYXllcl9zcGVlZCI6IjM1MCIsImxheWVyX3RvcCI6IjI1NyIsImxheWVyX2xlZnQiOiI2MCIsImxheWVyX2VuZHRpbWUiOiIwIiwibGF5ZXJfZW5kc3BlZWQiOiIzMDAiLCJsYXllcl9lbmRhbmltYXRpb24iOiJhdXRvIiwibGF5ZXJfZW5kZWFzaW5nIjoibm90aGluZyIsInRpbWVfc3RhcnQiOiIyNDAwIn1d'),
	(30, 1, 'slidershow-gift2', 'http://prestashop.com', 'bg-gift.jpg', '', 'eyJ1c2V2aWRlbyI6IjAiLCJ2aWRlb2lkIjoiIiwidmlkZW9hdXRvIjoiMSIsImJhY2tncm91bmRfY29sb3IiOiIjZmZmZmZmIn0=', 'W3sibGF5ZXJfdmlkZW9fdHlwZSI6InlvdXR1YmUiLCJsYXllcl92aWRlb19pZCI6IiIsImxheWVyX3ZpZGVvX2hlaWdodCI6IjIwMCIsImxheWVyX3ZpZGVvX3dpZHRoIjoiMzAwIiwibGF5ZXJfdmlkZW9fdGh1bWIiOiIiLCJsYXllcl9pZCI6IjFfMSIsImxheWVyX2NvbnRlbnQiOiJzbC1nZjIuanBnIiwibGF5ZXJfdHlwZSI6ImltYWdlIiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiWW91ciBJbWFnZSBIZXJlIDYiLCJsYXllcl9mb250X3NpemUiOiIiLCJsYXllcl9iYWNrZ3JvdW5kX2NvbG9yIjoiIiwibGF5ZXJfY29sb3IiOiIiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoicmFuZG9tcm90YXRlIiwibGF5ZXJfZWFzaW5nIjoiZWFzZUluQm91bmNlIiwibGF5ZXJfc3BlZWQiOiIzNTAiLCJsYXllcl90b3AiOiIwIiwibGF5ZXJfbGVmdCI6IjQ2NSIsImxheWVyX2VuZHRpbWUiOiIwIiwibGF5ZXJfZW5kc3BlZWQiOiIzMDAiLCJsYXllcl9lbmRhbmltYXRpb24iOiJhdXRvIiwibGF5ZXJfZW5kZWFzaW5nIjoibm90aGluZyIsInRpbWVfc3RhcnQiOiI0MDAifSx7ImxheWVyX3ZpZGVvX3R5cGUiOiJ5b3V0dWJlIiwibGF5ZXJfdmlkZW9faWQiOiIiLCJsYXllcl92aWRlb19oZWlnaHQiOiIyMDAiLCJsYXllcl92aWRlb193aWR0aCI6IjMwMCIsImxheWVyX3ZpZGVvX3RodW1iIjoiIiwibGF5ZXJfaWQiOiIxXzIiLCJsYXllcl9jb250ZW50IjoibGF5ZXIyLnBuZyIsImxheWVyX3R5cGUiOiJ0ZXh0IiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiPHNwYW4gc3R5bGU9XCIgIGxldHRlci1zcGFjaW5nOi0zcHg7Zm9udC13ZWlnaHQ6IDcwMDsgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcIj5iaWcgc2FsZTxcL3NwYW4+IiwibGF5ZXJfZm9udF9zaXplIjoiOTBweCIsImxheWVyX2JhY2tncm91bmRfY29sb3IiOiIiLCJsYXllcl9jb2xvciI6IiNmOGMyNTUiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoic2ZsIiwibGF5ZXJfZWFzaW5nIjoiZWFzZUluRWxhc3RpYyIsImxheWVyX3NwZWVkIjoiMzUwIiwibGF5ZXJfdG9wIjoiMTI0IiwibGF5ZXJfbGVmdCI6IjUwIiwibGF5ZXJfZW5kdGltZSI6IjAiLCJsYXllcl9lbmRzcGVlZCI6IjMwMCIsImxheWVyX2VuZGFuaW1hdGlvbiI6ImF1dG8iLCJsYXllcl9lbmRlYXNpbmciOiJub3RoaW5nIiwidGltZV9zdGFydCI6IjE4MDAifSx7ImxheWVyX3ZpZGVvX3R5cGUiOiJ5b3V0dWJlIiwibGF5ZXJfdmlkZW9faWQiOiIiLCJsYXllcl92aWRlb19oZWlnaHQiOiIyMDAiLCJsYXllcl92aWRlb193aWR0aCI6IjMwMCIsImxheWVyX3ZpZGVvX3RodW1iIjoiIiwibGF5ZXJfaWQiOiIxXzMiLCJsYXllcl9jb250ZW50IjoibGF5ZXIyLnBuZyIsImxheWVyX3R5cGUiOiJ0ZXh0IiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiPHNwYW4gc3R5bGU9XCIgZm9udC13ZWlnaHQ6IDcwMDsgIGZvbnQtc3R5bGU6IGl0YWxpYzsgbGV0dGVyLXNwYWNpbmc6LTFweFwiPkNocmlzdG1hczxcL3NwYW4+IiwibGF5ZXJfZm9udF9zaXplIjoiMzBweCIsImxheWVyX2JhY2tncm91bmRfY29sb3IiOiIiLCJsYXllcl9jb2xvciI6IiNmZjliNmQiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoic2ZsIiwibGF5ZXJfZWFzaW5nIjoiZWFzZUluQmFjayIsImxheWVyX3NwZWVkIjoiMzUwIiwibGF5ZXJfdG9wIjoiMTE3IiwibGF5ZXJfbGVmdCI6IjUwIiwibGF5ZXJfZW5kdGltZSI6IjAiLCJsYXllcl9lbmRzcGVlZCI6IjMwMCIsImxheWVyX2VuZGFuaW1hdGlvbiI6ImF1dG8iLCJsYXllcl9lbmRlYXNpbmciOiJub3RoaW5nIiwidGltZV9zdGFydCI6IjEyMDAifSx7ImxheWVyX3ZpZGVvX3R5cGUiOiJ5b3V0dWJlIiwibGF5ZXJfdmlkZW9faWQiOiIiLCJsYXllcl92aWRlb19oZWlnaHQiOiIyMDAiLCJsYXllcl92aWRlb193aWR0aCI6IjMwMCIsImxheWVyX3ZpZGVvX3RodW1iIjoiIiwibGF5ZXJfaWQiOiIxXzQiLCJsYXllcl9jb250ZW50IjoibGF5ZXIyLnBuZyIsImxheWVyX3R5cGUiOiJ0ZXh0IiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiPHNwYW4gc3R5bGU9XCIgIGxldHRlci1zcGFjaW5nOi0zcHg7IHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XCI+dXAgdG8gPHN0cm9uZz41MCUgPFwvc3Ryb25nPm9mZjxcL3NwYW4+IiwibGF5ZXJfZm9udF9zaXplIjoiNTBweCIsImxheWVyX2JhY2tncm91bmRfY29sb3IiOiIiLCJsYXllcl9jb2xvciI6IiMwMDAwMDAiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoic2ZsIiwibGF5ZXJfZWFzaW5nIjoiZWFzZUluQ2lyYyIsImxheWVyX3NwZWVkIjoiMzUwIiwibGF5ZXJfdG9wIjoiMjIxIiwibGF5ZXJfbGVmdCI6IjUwIiwibGF5ZXJfZW5kdGltZSI6IjAiLCJsYXllcl9lbmRzcGVlZCI6IjMwMCIsImxheWVyX2VuZGFuaW1hdGlvbiI6ImF1dG8iLCJsYXllcl9lbmRlYXNpbmciOiJub3RoaW5nIiwidGltZV9zdGFydCI6IjI0MDAifSx7ImxheWVyX3ZpZGVvX3R5cGUiOiJ5b3V0dWJlIiwibGF5ZXJfdmlkZW9faWQiOiIiLCJsYXllcl92aWRlb19oZWlnaHQiOiIyMDAiLCJsYXllcl92aWRlb193aWR0aCI6IjMwMCIsImxheWVyX3ZpZGVvX3RodW1iIjoiIiwibGF5ZXJfaWQiOiIxXzUiLCJsYXllcl9jb250ZW50IjoibGF5ZXIyLnBuZyIsImxheWVyX3R5cGUiOiJ0ZXh0IiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiPHNwYW4gc3R5bGU9XCJ0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1wiPiB0aGlzIG9mZmZlciBpcyB2YWxpZCBvbiBhbGwgb3VyIHN0b3JlIGl0ZW1zPFwvc3Bhbj4iLCJsYXllcl9mb250X3NpemUiOiIxMnB4IiwibGF5ZXJfYmFja2dyb3VuZF9jb2xvciI6IiIsImxheWVyX2NvbG9yIjoiIzAwMDAwMCIsImxheWVyX2xpbmsiOiIiLCJsYXllcl9hbmltYXRpb24iOiJzZmwiLCJsYXllcl9lYXNpbmciOiJlYXNlSW5FeHBvIiwibGF5ZXJfc3BlZWQiOiIzNTAiLCJsYXllcl90b3AiOiIyODgiLCJsYXllcl9sZWZ0IjoiNTAiLCJsYXllcl9lbmR0aW1lIjoiMCIsImxheWVyX2VuZHNwZWVkIjoiMzAwIiwibGF5ZXJfZW5kYW5pbWF0aW9uIjoiYXV0byIsImxheWVyX2VuZGVhc2luZyI6Im5vdGhpbmciLCJ0aW1lX3N0YXJ0IjoiMzAwMCJ9LHsibGF5ZXJfdmlkZW9fdHlwZSI6InlvdXR1YmUiLCJsYXllcl92aWRlb19pZCI6IiIsImxheWVyX3ZpZGVvX2hlaWdodCI6IjIwMCIsImxheWVyX3ZpZGVvX3dpZHRoIjoiMzAwIiwibGF5ZXJfdmlkZW9fdGh1bWIiOiIiLCJsYXllcl9pZCI6IjFfNiIsImxheWVyX2NvbnRlbnQiOiJsYXllcjIucG5nIiwibGF5ZXJfdHlwZSI6InRleHQiLCJsYXllcl9jbGFzcyI6IiIsImxheWVyX2NhcHRpb24iOiI8c3BhbiBjbGFzcz1cImJ0biBidG4tb3V0bGluZVwiPnZpZXcgY29sbGVjdGlvbjxcL3NwYW4+IiwibGF5ZXJfZm9udF9zaXplIjoiIiwibGF5ZXJfYmFja2dyb3VuZF9jb2xvciI6IiIsImxheWVyX2NvbG9yIjoiIiwibGF5ZXJfbGluayI6IiIsImxheWVyX2FuaW1hdGlvbiI6InNmbCIsImxheWVyX2Vhc2luZyI6ImVhc2VJblNpbmUiLCJsYXllcl9zcGVlZCI6IjM1MCIsImxheWVyX3RvcCI6IjM0NiIsImxheWVyX2xlZnQiOiI1MCIsImxheWVyX2VuZHRpbWUiOiIwIiwibGF5ZXJfZW5kc3BlZWQiOiIzMDAiLCJsYXllcl9lbmRhbmltYXRpb24iOiJhdXRvIiwibGF5ZXJfZW5kZWFzaW5nIjoibm90aGluZyIsInRpbWVfc3RhcnQiOiIzNjAwIn1d'),
	(31, 1, 'Slidershow-gift3', 'http://prestashop.com', 'bg-gift.jpg', '', 'eyJ1c2V2aWRlbyI6IjAiLCJ2aWRlb2lkIjoiIiwidmlkZW9hdXRvIjoiMSIsImJhY2tncm91bmRfY29sb3IiOiIjZmZmZmZmIn0=', 'W3sibGF5ZXJfdmlkZW9fdHlwZSI6InlvdXR1YmUiLCJsYXllcl92aWRlb19pZCI6IiIsImxheWVyX3ZpZGVvX2hlaWdodCI6IjIwMCIsImxheWVyX3ZpZGVvX3dpZHRoIjoiMzAwIiwibGF5ZXJfdmlkZW9fdGh1bWIiOiIiLCJsYXllcl9pZCI6IjFfMSIsImxheWVyX2NvbnRlbnQiOiJzbC1nZjMuanBnIiwibGF5ZXJfdHlwZSI6ImltYWdlIiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiWW91ciBJbWFnZSBIZXJlIDYiLCJsYXllcl9mb250X3NpemUiOiIiLCJsYXllcl9iYWNrZ3JvdW5kX2NvbG9yIjoiIiwibGF5ZXJfY29sb3IiOiIiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoicmFuZG9tcm90YXRlIiwibGF5ZXJfZWFzaW5nIjoiZWFzZU91dEJhY2siLCJsYXllcl9zcGVlZCI6IjM1MCIsImxheWVyX3RvcCI6IjAiLCJsYXllcl9sZWZ0IjoiMCIsImxheWVyX2VuZHRpbWUiOiIwIiwibGF5ZXJfZW5kc3BlZWQiOiIzMDAiLCJsYXllcl9lbmRhbmltYXRpb24iOiJhdXRvIiwibGF5ZXJfZW5kZWFzaW5nIjoibm90aGluZyIsInRpbWVfc3RhcnQiOiI0MDAifSx7ImxheWVyX3ZpZGVvX3R5cGUiOiJ5b3V0dWJlIiwibGF5ZXJfdmlkZW9faWQiOiIiLCJsYXllcl92aWRlb19oZWlnaHQiOiIyMDAiLCJsYXllcl92aWRlb193aWR0aCI6IjMwMCIsImxheWVyX3ZpZGVvX3RodW1iIjoiIiwibGF5ZXJfaWQiOiIxXzIiLCJsYXllcl9jb250ZW50IjoibGF5ZXIyLnBuZyIsImxheWVyX3R5cGUiOiJ0ZXh0IiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiPHNwYW4gc3R5bGU9XCIgIGxldHRlci1zcGFjaW5nOi0zcHg7Zm9udC13ZWlnaHQ6IDcwMDsgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcIj5iaWcgc2FsZTxcL3NwYW4+IiwibGF5ZXJfZm9udF9zaXplIjoiOTBweCIsImxheWVyX2JhY2tncm91bmRfY29sb3IiOiIiLCJsYXllcl9jb2xvciI6IiNmOGMyNTUiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoic2ZyIiwibGF5ZXJfZWFzaW5nIjoiZWFzZU91dEN1YmljIiwibGF5ZXJfc3BlZWQiOiIzNTAiLCJsYXllcl90b3AiOiIxMjQiLCJsYXllcl9sZWZ0IjoiNzM1IiwibGF5ZXJfZW5kdGltZSI6IjAiLCJsYXllcl9lbmRzcGVlZCI6IjMwMCIsImxheWVyX2VuZGFuaW1hdGlvbiI6ImF1dG8iLCJsYXllcl9lbmRlYXNpbmciOiJub3RoaW5nIiwidGltZV9zdGFydCI6IjE4MDAifSx7ImxheWVyX3ZpZGVvX3R5cGUiOiJ5b3V0dWJlIiwibGF5ZXJfdmlkZW9faWQiOiIiLCJsYXllcl92aWRlb19oZWlnaHQiOiIyMDAiLCJsYXllcl92aWRlb193aWR0aCI6IjMwMCIsImxheWVyX3ZpZGVvX3RodW1iIjoiIiwibGF5ZXJfaWQiOiIxXzMiLCJsYXllcl9jb250ZW50IjoibGF5ZXIyLnBuZyIsImxheWVyX3R5cGUiOiJ0ZXh0IiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiPHNwYW4gc3R5bGU9XCIgZm9udC13ZWlnaHQ6IDcwMDsgIGZvbnQtc3R5bGU6IGl0YWxpYzsgbGV0dGVyLXNwYWNpbmc6LTFweFwiPkNocmlzdG1hczxcL3NwYW4+IiwibGF5ZXJfZm9udF9zaXplIjoiMzBweCIsImxheWVyX2JhY2tncm91bmRfY29sb3IiOiIiLCJsYXllcl9jb2xvciI6IiNmZjliNmQiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoic2Z0IiwibGF5ZXJfZWFzaW5nIjoiZWFzZU91dFF1YWQiLCJsYXllcl9zcGVlZCI6IjM1MCIsImxheWVyX3RvcCI6IjExNyIsImxheWVyX2xlZnQiOiI3MzUiLCJsYXllcl9lbmR0aW1lIjoiMCIsImxheWVyX2VuZHNwZWVkIjoiMzAwIiwibGF5ZXJfZW5kYW5pbWF0aW9uIjoiYXV0byIsImxheWVyX2VuZGVhc2luZyI6Im5vdGhpbmciLCJ0aW1lX3N0YXJ0IjoiMTIwMCJ9LHsibGF5ZXJfdmlkZW9fdHlwZSI6InlvdXR1YmUiLCJsYXllcl92aWRlb19pZCI6IiIsImxheWVyX3ZpZGVvX2hlaWdodCI6IjIwMCIsImxheWVyX3ZpZGVvX3dpZHRoIjoiMzAwIiwibGF5ZXJfdmlkZW9fdGh1bWIiOiIiLCJsYXllcl9pZCI6IjFfNCIsImxheWVyX2NvbnRlbnQiOiJsYXllcjIucG5nIiwibGF5ZXJfdHlwZSI6InRleHQiLCJsYXllcl9jbGFzcyI6IiIsImxheWVyX2NhcHRpb24iOiI8c3BhbiBzdHlsZT1cIiAgbGV0dGVyLXNwYWNpbmc6LTNweDsgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcIj51cCB0byA8c3Ryb25nPjUwJSA8XC9zdHJvbmc+b2ZmPFwvc3Bhbj4iLCJsYXllcl9mb250X3NpemUiOiI1MHB4IiwibGF5ZXJfYmFja2dyb3VuZF9jb2xvciI6IiIsImxheWVyX2NvbG9yIjoiIzAwMDAwMCIsImxheWVyX2xpbmsiOiIiLCJsYXllcl9hbmltYXRpb24iOiJzZmwiLCJsYXllcl9lYXNpbmciOiJlYXNlSW5PdXRRdWFydCIsImxheWVyX3NwZWVkIjoiMzUwIiwibGF5ZXJfdG9wIjoiMjIxIiwibGF5ZXJfbGVmdCI6IjczNSIsImxheWVyX2VuZHRpbWUiOiIwIiwibGF5ZXJfZW5kc3BlZWQiOiIzMDAiLCJsYXllcl9lbmRhbmltYXRpb24iOiJhdXRvIiwibGF5ZXJfZW5kZWFzaW5nIjoibm90aGluZyIsInRpbWVfc3RhcnQiOiIyNDAwIn0seyJsYXllcl92aWRlb190eXBlIjoieW91dHViZSIsImxheWVyX3ZpZGVvX2lkIjoiIiwibGF5ZXJfdmlkZW9faGVpZ2h0IjoiMjAwIiwibGF5ZXJfdmlkZW9fd2lkdGgiOiIzMDAiLCJsYXllcl92aWRlb190aHVtYiI6IiIsImxheWVyX2lkIjoiMV81IiwibGF5ZXJfY29udGVudCI6ImxheWVyMi5wbmciLCJsYXllcl90eXBlIjoidGV4dCIsImxheWVyX2NsYXNzIjoiIiwibGF5ZXJfY2FwdGlvbiI6IjxzcGFuIHN0eWxlPVwidGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcIj4gdGhpcyBvZmZmZXIgaXMgdmFsaWQgb24gYWxsIG91ciBzdG9yZSBpdGVtczxcL3NwYW4+IiwibGF5ZXJfZm9udF9zaXplIjoiMTJweCIsImxheWVyX2JhY2tncm91bmRfY29sb3IiOiIiLCJsYXllcl9jb2xvciI6IiMwMDAwMDAiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoic2ZyIiwibGF5ZXJfZWFzaW5nIjoiZWFzZUluT3V0U2luZSIsImxheWVyX3NwZWVkIjoiMzUwIiwibGF5ZXJfdG9wIjoiMjg4IiwibGF5ZXJfbGVmdCI6IjczNSIsImxheWVyX2VuZHRpbWUiOiIwIiwibGF5ZXJfZW5kc3BlZWQiOiIzMDAiLCJsYXllcl9lbmRhbmltYXRpb24iOiJhdXRvIiwibGF5ZXJfZW5kZWFzaW5nIjoibm90aGluZyIsInRpbWVfc3RhcnQiOiIzMDAwIn0seyJsYXllcl92aWRlb190eXBlIjoieW91dHViZSIsImxheWVyX3ZpZGVvX2lkIjoiIiwibGF5ZXJfdmlkZW9faGVpZ2h0IjoiMjAwIiwibGF5ZXJfdmlkZW9fd2lkdGgiOiIzMDAiLCJsYXllcl92aWRlb190aHVtYiI6IiIsImxheWVyX2lkIjoiMV82IiwibGF5ZXJfY29udGVudCI6ImxheWVyMi5wbmciLCJsYXllcl90eXBlIjoidGV4dCIsImxheWVyX2NsYXNzIjoiIiwibGF5ZXJfY2FwdGlvbiI6IjxhIGNsYXNzPVwiYnRuIGJ0bi1vdXRsaW5lXCI+dmlldyBjb2xsZWN0aW9uPFwvYT4iLCJsYXllcl9mb250X3NpemUiOiIiLCJsYXllcl9iYWNrZ3JvdW5kX2NvbG9yIjoiIiwibGF5ZXJfY29sb3IiOiIiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoic2ZiIiwibGF5ZXJfZWFzaW5nIjoiZWFzZUluT3V0RWxhc3RpYyIsImxheWVyX3NwZWVkIjoiMzUwIiwibGF5ZXJfdG9wIjoiMzQ2IiwibGF5ZXJfbGVmdCI6IjczNSIsImxheWVyX2VuZHRpbWUiOiIwIiwibGF5ZXJfZW5kc3BlZWQiOiIzMDAiLCJsYXllcl9lbmRhbmltYXRpb24iOiJhdXRvIiwibGF5ZXJfZW5kZWFzaW5nIjoibm90aGluZyIsInRpbWVfc3RhcnQiOiIzNjAwIn1d'),
	(32, 1, 'Slidershow-gift4', 'http://prestashop.com', 'bg-gift.jpg', '', 'eyJ1c2V2aWRlbyI6IjAiLCJ2aWRlb2lkIjoiIiwidmlkZW9hdXRvIjoiMSIsImJhY2tncm91bmRfY29sb3IiOiIjZmZmZmZmIn0=', 'W3sibGF5ZXJfdmlkZW9fdHlwZSI6InlvdXR1YmUiLCJsYXllcl92aWRlb19pZCI6IiIsImxheWVyX3ZpZGVvX2hlaWdodCI6IjIwMCIsImxheWVyX3ZpZGVvX3dpZHRoIjoiMzAwIiwibGF5ZXJfdmlkZW9fdGh1bWIiOiIiLCJsYXllcl9pZCI6IjFfMSIsImxheWVyX2NvbnRlbnQiOiJzbC1nZjMuanBnIiwibGF5ZXJfdHlwZSI6ImltYWdlIiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiWW91ciBJbWFnZSBIZXJlIDYiLCJsYXllcl9mb250X3NpemUiOiIiLCJsYXllcl9iYWNrZ3JvdW5kX2NvbG9yIjoiIiwibGF5ZXJfY29sb3IiOiIiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoicmFuZG9tcm90YXRlIiwibGF5ZXJfZWFzaW5nIjoiZWFzZUluQm91bmNlIiwibGF5ZXJfc3BlZWQiOiIzNTAiLCJsYXllcl90b3AiOiIwIiwibGF5ZXJfbGVmdCI6IjQ5NSIsImxheWVyX2VuZHRpbWUiOiIwIiwibGF5ZXJfZW5kc3BlZWQiOiIzMDAiLCJsYXllcl9lbmRhbmltYXRpb24iOiJhdXRvIiwibGF5ZXJfZW5kZWFzaW5nIjoibm90aGluZyIsInRpbWVfc3RhcnQiOiI0MDAifSx7ImxheWVyX3ZpZGVvX3R5cGUiOiJ5b3V0dWJlIiwibGF5ZXJfdmlkZW9faWQiOiIiLCJsYXllcl92aWRlb19oZWlnaHQiOiIyMDAiLCJsYXllcl92aWRlb193aWR0aCI6IjMwMCIsImxheWVyX3ZpZGVvX3RodW1iIjoiIiwibGF5ZXJfaWQiOiIxXzIiLCJsYXllcl9jb250ZW50IjoibGF5ZXIyLnBuZyIsImxheWVyX3R5cGUiOiJ0ZXh0IiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiPHNwYW4gc3R5bGU9XCIgIGxldHRlci1zcGFjaW5nOi0zcHg7Zm9udC13ZWlnaHQ6IDcwMDsgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcIj5iaWcgc2FsZTxcL3NwYW4+IiwibGF5ZXJfZm9udF9zaXplIjoiOTBweCIsImxheWVyX2JhY2tncm91bmRfY29sb3IiOiIiLCJsYXllcl9jb2xvciI6IiNmOGMyNTUiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoic2ZsIiwibGF5ZXJfZWFzaW5nIjoiZWFzZUluRWxhc3RpYyIsImxheWVyX3NwZWVkIjoiMzUwIiwibGF5ZXJfdG9wIjoiMTI0IiwibGF5ZXJfbGVmdCI6IjUwIiwibGF5ZXJfZW5kdGltZSI6IjAiLCJsYXllcl9lbmRzcGVlZCI6IjMwMCIsImxheWVyX2VuZGFuaW1hdGlvbiI6ImF1dG8iLCJsYXllcl9lbmRlYXNpbmciOiJub3RoaW5nIiwidGltZV9zdGFydCI6IjE4MDAifSx7ImxheWVyX3ZpZGVvX3R5cGUiOiJ5b3V0dWJlIiwibGF5ZXJfdmlkZW9faWQiOiIiLCJsYXllcl92aWRlb19oZWlnaHQiOiIyMDAiLCJsYXllcl92aWRlb193aWR0aCI6IjMwMCIsImxheWVyX3ZpZGVvX3RodW1iIjoiIiwibGF5ZXJfaWQiOiIxXzMiLCJsYXllcl9jb250ZW50IjoibGF5ZXIyLnBuZyIsImxheWVyX3R5cGUiOiJ0ZXh0IiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiPHNwYW4gc3R5bGU9XCIgZm9udC13ZWlnaHQ6IDcwMDsgIGZvbnQtc3R5bGU6IGl0YWxpYzsgbGV0dGVyLXNwYWNpbmc6LTFweFwiPkNocmlzdG1hczxcL3NwYW4+IiwibGF5ZXJfZm9udF9zaXplIjoiMzBweCIsImxheWVyX2JhY2tncm91bmRfY29sb3IiOiIiLCJsYXllcl9jb2xvciI6IiNmZjliNmQiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoic2ZsIiwibGF5ZXJfZWFzaW5nIjoiZWFzZUluQmFjayIsImxheWVyX3NwZWVkIjoiMzUwIiwibGF5ZXJfdG9wIjoiMTE3IiwibGF5ZXJfbGVmdCI6IjUwIiwibGF5ZXJfZW5kdGltZSI6IjAiLCJsYXllcl9lbmRzcGVlZCI6IjMwMCIsImxheWVyX2VuZGFuaW1hdGlvbiI6ImF1dG8iLCJsYXllcl9lbmRlYXNpbmciOiJub3RoaW5nIiwidGltZV9zdGFydCI6IjEyMDAifSx7ImxheWVyX3ZpZGVvX3R5cGUiOiJ5b3V0dWJlIiwibGF5ZXJfdmlkZW9faWQiOiIiLCJsYXllcl92aWRlb19oZWlnaHQiOiIyMDAiLCJsYXllcl92aWRlb193aWR0aCI6IjMwMCIsImxheWVyX3ZpZGVvX3RodW1iIjoiIiwibGF5ZXJfaWQiOiIxXzQiLCJsYXllcl9jb250ZW50IjoibGF5ZXIyLnBuZyIsImxheWVyX3R5cGUiOiJ0ZXh0IiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiPHNwYW4gc3R5bGU9XCIgIGxldHRlci1zcGFjaW5nOi0zcHg7IHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XCI+dXAgdG8gPHN0cm9uZz41MCUgPFwvc3Ryb25nPm9mZjxcL3NwYW4+IiwibGF5ZXJfZm9udF9zaXplIjoiNTBweCIsImxheWVyX2JhY2tncm91bmRfY29sb3IiOiIiLCJsYXllcl9jb2xvciI6IiMwMDAwMDAiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoic2ZyIiwibGF5ZXJfZWFzaW5nIjoiZWFzZUluQ2lyYyIsImxheWVyX3NwZWVkIjoiMzUwIiwibGF5ZXJfdG9wIjoiMjIxIiwibGF5ZXJfbGVmdCI6IjUwIiwibGF5ZXJfZW5kdGltZSI6IjAiLCJsYXllcl9lbmRzcGVlZCI6IjMwMCIsImxheWVyX2VuZGFuaW1hdGlvbiI6ImF1dG8iLCJsYXllcl9lbmRlYXNpbmciOiJub3RoaW5nIiwidGltZV9zdGFydCI6IjI0MDAifSx7ImxheWVyX3ZpZGVvX3R5cGUiOiJ5b3V0dWJlIiwibGF5ZXJfdmlkZW9faWQiOiIiLCJsYXllcl92aWRlb19oZWlnaHQiOiIyMDAiLCJsYXllcl92aWRlb193aWR0aCI6IjMwMCIsImxheWVyX3ZpZGVvX3RodW1iIjoiIiwibGF5ZXJfaWQiOiIxXzUiLCJsYXllcl9jb250ZW50IjoibGF5ZXIyLnBuZyIsImxheWVyX3R5cGUiOiJ0ZXh0IiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiPHNwYW4gc3R5bGU9XCJ0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1wiPiB0aGlzIG9mZmZlciBpcyB2YWxpZCBvbiBhbGwgb3VyIHN0b3JlIGl0ZW1zPFwvc3Bhbj4iLCJsYXllcl9mb250X3NpemUiOiIxMnB4IiwibGF5ZXJfYmFja2dyb3VuZF9jb2xvciI6IiIsImxheWVyX2NvbG9yIjoiIzAwMDAwMCIsImxheWVyX2xpbmsiOiIiLCJsYXllcl9hbmltYXRpb24iOiJzZnIiLCJsYXllcl9lYXNpbmciOiJlYXNlSW5FeHBvIiwibGF5ZXJfc3BlZWQiOiIzNTAiLCJsYXllcl90b3AiOiIyODgiLCJsYXllcl9sZWZ0IjoiNTAiLCJsYXllcl9lbmR0aW1lIjoiMCIsImxheWVyX2VuZHNwZWVkIjoiMzAwIiwibGF5ZXJfZW5kYW5pbWF0aW9uIjoiYXV0byIsImxheWVyX2VuZGVhc2luZyI6Im5vdGhpbmciLCJ0aW1lX3N0YXJ0IjoiMzAwMCJ9LHsibGF5ZXJfdmlkZW9fdHlwZSI6InlvdXR1YmUiLCJsYXllcl92aWRlb19pZCI6IiIsImxheWVyX3ZpZGVvX2hlaWdodCI6IjIwMCIsImxheWVyX3ZpZGVvX3dpZHRoIjoiMzAwIiwibGF5ZXJfdmlkZW9fdGh1bWIiOiIiLCJsYXllcl9pZCI6IjFfNiIsImxheWVyX2NvbnRlbnQiOiJsYXllcjIucG5nIiwibGF5ZXJfdHlwZSI6InRleHQiLCJsYXllcl9jbGFzcyI6IiIsImxheWVyX2NhcHRpb24iOiI8c3BhbiBjbGFzcz1cImJ0biBidG4tb3V0bGluZVwiPnZpZXcgY29sbGVjdGlvbjxcL3NwYW4+IiwibGF5ZXJfZm9udF9zaXplIjoiIiwibGF5ZXJfYmFja2dyb3VuZF9jb2xvciI6IiIsImxheWVyX2NvbG9yIjoiIiwibGF5ZXJfbGluayI6IiIsImxheWVyX2FuaW1hdGlvbiI6InNmbCIsImxheWVyX2Vhc2luZyI6ImVhc2VJblNpbmUiLCJsYXllcl9zcGVlZCI6IjM1MCIsImxheWVyX3RvcCI6IjM0NiIsImxheWVyX2xlZnQiOiI1MCIsImxheWVyX2VuZHRpbWUiOiIwIiwibGF5ZXJfZW5kc3BlZWQiOiIzMDAiLCJsYXllcl9lbmRhbmltYXRpb24iOiJhdXRvIiwibGF5ZXJfZW5kZWFzaW5nIjoibm90aGluZyIsInRpbWVfc3RhcnQiOiIzNjAwIn1d'),
	(33, 1, 'Slide-sport2', '', 'sl-sport2.jpg', '', 'eyJ1c2V2aWRlbyI6IjAiLCJ2aWRlb2lkIjoiIiwidmlkZW9hdXRvIjoiMCIsImJhY2tncm91bmRfY29sb3IiOiIifQ==', 'W3sibGF5ZXJfdmlkZW9fdHlwZSI6InlvdXR1YmUiLCJsYXllcl92aWRlb19pZCI6IiIsImxheWVyX3ZpZGVvX2hlaWdodCI6IjIwMCIsImxheWVyX3ZpZGVvX3dpZHRoIjoiMzAwIiwibGF5ZXJfdmlkZW9fdGh1bWIiOiIiLCJsYXllcl9pZCI6IjFfMSIsImxheWVyX2NvbnRlbnQiOiIiLCJsYXllcl90eXBlIjoidGV4dCIsImxheWVyX2NsYXNzIjoiIiwibGF5ZXJfY2FwdGlvbiI6IjxzcGFuIHN0eWxlPVwiIGZvbnQtd2VpZ2h0OiAxMDA7IGxldHRlci1zcGFjaW5nOiAtN3B4O3RleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XCI+U3VyZmluZyBzZWFzb248XC9zcGFuPiIsImxheWVyX2ZvbnRfc2l6ZSI6IjgwcHgiLCJsYXllcl9iYWNrZ3JvdW5kX2NvbG9yIjoiIiwibGF5ZXJfY29sb3IiOiIjZmZmZmZmIiwibGF5ZXJfbGluayI6IiIsImxheWVyX2FuaW1hdGlvbiI6ImZhZGUiLCJsYXllcl9lYXNpbmciOiJlYXNlT3V0RXhwbyIsImxheWVyX3NwZWVkIjoiMzUwIiwibGF5ZXJfdG9wIjoiMTQwIiwibGF5ZXJfbGVmdCI6IjEwMCIsImxheWVyX2VuZHRpbWUiOiIwIiwibGF5ZXJfZW5kc3BlZWQiOiIzMDAiLCJsYXllcl9lbmRhbmltYXRpb24iOiJhdXRvIiwibGF5ZXJfZW5kZWFzaW5nIjoibm90aGluZyIsInRpbWVfc3RhcnQiOiI0MDAifSx7ImxheWVyX3ZpZGVvX3R5cGUiOiJ5b3V0dWJlIiwibGF5ZXJfdmlkZW9faWQiOiIiLCJsYXllcl92aWRlb19oZWlnaHQiOiIyMDAiLCJsYXllcl92aWRlb193aWR0aCI6IjMwMCIsImxheWVyX3ZpZGVvX3RodW1iIjoiIiwibGF5ZXJfaWQiOiIxXzIiLCJsYXllcl9jb250ZW50IjoiIiwibGF5ZXJfdHlwZSI6InRleHQiLCJsYXllcl9jbGFzcyI6IiIsImxheWVyX2NhcHRpb24iOiI8c3BhbiBzdHlsZT1cIiBmb250LXdlaWdodDogOTAwOyBsZXR0ZXItc3BhY2luZzogLTdweDt0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1wiPmlzIGNvbWluZzxcL3NwYW4+IiwibGF5ZXJfZm9udF9zaXplIjoiMTAwcHgiLCJsYXllcl9iYWNrZ3JvdW5kX2NvbG9yIjoiIiwibGF5ZXJfY29sb3IiOiIjZmZmZmZmIiwibGF5ZXJfbGluayI6IiIsImxheWVyX2FuaW1hdGlvbiI6ImZhZGUiLCJsYXllcl9lYXNpbmciOiJlYXNlT3V0RXhwbyIsImxheWVyX3NwZWVkIjoiMzUwIiwibGF5ZXJfdG9wIjoiMjIwIiwibGF5ZXJfbGVmdCI6IjEwMCIsImxheWVyX2VuZHRpbWUiOiIwIiwibGF5ZXJfZW5kc3BlZWQiOiIzMDAiLCJsYXllcl9lbmRhbmltYXRpb24iOiJhdXRvIiwibGF5ZXJfZW5kZWFzaW5nIjoibm90aGluZyIsInRpbWVfc3RhcnQiOiI4MDAifSx7ImxheWVyX3ZpZGVvX3R5cGUiOiJ5b3V0dWJlIiwibGF5ZXJfdmlkZW9faWQiOiIiLCJsYXllcl92aWRlb19oZWlnaHQiOiIyMDAiLCJsYXllcl92aWRlb193aWR0aCI6IjMwMCIsImxheWVyX3ZpZGVvX3RodW1iIjoiIiwibGF5ZXJfaWQiOiIxXzMiLCJsYXllcl9jb250ZW50IjoiIiwibGF5ZXJfdHlwZSI6InRleHQiLCJsYXllcl9jbGFzcyI6IiIsImxheWVyX2NhcHRpb24iOiI8c3BhbiBzdHlsZT1cIiBmb250LXdlaWdodDogMTAwOyBsZXR0ZXItc3BhY2luZzogLTdweDt0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlXCI+QXJlIHlvdSByZWFkeT88XC9zcGFuPiIsImxheWVyX2ZvbnRfc2l6ZSI6IjgwcHgiLCJsYXllcl9iYWNrZ3JvdW5kX2NvbG9yIjoiIiwibGF5ZXJfY29sb3IiOiIjZmZmZmZmIiwibGF5ZXJfbGluayI6IiIsImxheWVyX2FuaW1hdGlvbiI6ImZhZGUiLCJsYXllcl9lYXNpbmciOiJlYXNlT3V0RXhwbyIsImxheWVyX3NwZWVkIjoiMzUwIiwibGF5ZXJfdG9wIjoiMzM1IiwibGF5ZXJfbGVmdCI6IjEwMCIsImxheWVyX2VuZHRpbWUiOiIwIiwibGF5ZXJfZW5kc3BlZWQiOiIzMDAiLCJsYXllcl9lbmRhbmltYXRpb24iOiJhdXRvIiwibGF5ZXJfZW5kZWFzaW5nIjoibm90aGluZyIsInRpbWVfc3RhcnQiOiIxMjAwIn1d'),
	(34, 1, 'Slide-sport3', '', 'sl-sport3.jpg', '', 'eyJ1c2V2aWRlbyI6IjAiLCJ2aWRlb2lkIjoiIiwidmlkZW9hdXRvIjoiMCIsImJhY2tncm91bmRfY29sb3IiOiIifQ==', 'W3sibGF5ZXJfdmlkZW9fdHlwZSI6InlvdXR1YmUiLCJsYXllcl92aWRlb19pZCI6IiIsImxheWVyX3ZpZGVvX2hlaWdodCI6IjIwMCIsImxheWVyX3ZpZGVvX3dpZHRoIjoiMzAwIiwibGF5ZXJfdmlkZW9fdGh1bWIiOiIiLCJsYXllcl9pZCI6IjFfMSIsImxheWVyX2NvbnRlbnQiOiIiLCJsYXllcl90eXBlIjoidGV4dCIsImxheWVyX2NsYXNzIjoiIiwibGF5ZXJfY2FwdGlvbiI6IjxzcGFuIHN0eWxlPVwiIGZvbnQtd2VpZ2h0OiAxMDA7IGxldHRlci1zcGFjaW5nOiAtN3B4O3RleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XCI+U2hvcCBtZW5zPFwvc3Bhbj4iLCJsYXllcl9mb250X3NpemUiOiI4MHB4IiwibGF5ZXJfYmFja2dyb3VuZF9jb2xvciI6IiIsImxheWVyX2NvbG9yIjoiI2ZmZmZmZiIsImxheWVyX2xpbmsiOiIiLCJsYXllcl9hbmltYXRpb24iOiJmYWRlIiwibGF5ZXJfZWFzaW5nIjoiZWFzZU91dEV4cG8iLCJsYXllcl9zcGVlZCI6IjM1MCIsImxheWVyX3RvcCI6IjE0MCIsImxheWVyX2xlZnQiOiI3MzAiLCJsYXllcl9lbmR0aW1lIjoiMCIsImxheWVyX2VuZHNwZWVkIjoiMzAwIiwibGF5ZXJfZW5kYW5pbWF0aW9uIjoiYXV0byIsImxheWVyX2VuZGVhc2luZyI6Im5vdGhpbmciLCJ0aW1lX3N0YXJ0IjoiNDAwIn0seyJsYXllcl92aWRlb190eXBlIjoieW91dHViZSIsImxheWVyX3ZpZGVvX2lkIjoiIiwibGF5ZXJfdmlkZW9faGVpZ2h0IjoiMjAwIiwibGF5ZXJfdmlkZW9fd2lkdGgiOiIzMDAiLCJsYXllcl92aWRlb190aHVtYiI6IiIsImxheWVyX2lkIjoiMV8yIiwibGF5ZXJfY29udGVudCI6IiIsImxheWVyX3R5cGUiOiJ0ZXh0IiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiPHNwYW4gc3R5bGU9XCIgZm9udC13ZWlnaHQ6IDkwMDsgbGV0dGVyLXNwYWNpbmc6IC03cHg7dGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcIj5vY3RvYmVyPFwvc3Bhbj4iLCJsYXllcl9mb250X3NpemUiOiIxMDBweCIsImxheWVyX2JhY2tncm91bmRfY29sb3IiOiIiLCJsYXllcl9jb2xvciI6IiNmZmZmZmYiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoiZmFkZSIsImxheWVyX2Vhc2luZyI6ImVhc2VPdXRFeHBvIiwibGF5ZXJfc3BlZWQiOiIzNTAiLCJsYXllcl90b3AiOiIyMjAiLCJsYXllcl9sZWZ0IjoiNzMwIiwibGF5ZXJfZW5kdGltZSI6IjAiLCJsYXllcl9lbmRzcGVlZCI6IjMwMCIsImxheWVyX2VuZGFuaW1hdGlvbiI6ImF1dG8iLCJsYXllcl9lbmRlYXNpbmciOiJub3RoaW5nIiwidGltZV9zdGFydCI6IjgwMCJ9LHsibGF5ZXJfdmlkZW9fdHlwZSI6InlvdXR1YmUiLCJsYXllcl92aWRlb19pZCI6IiIsImxheWVyX3ZpZGVvX2hlaWdodCI6IjIwMCIsImxheWVyX3ZpZGVvX3dpZHRoIjoiMzAwIiwibGF5ZXJfdmlkZW9fdGh1bWIiOiIiLCJsYXllcl9pZCI6IjFfMyIsImxheWVyX2NvbnRlbnQiOiIiLCJsYXllcl90eXBlIjoidGV4dCIsImxheWVyX2NsYXNzIjoiIiwibGF5ZXJfY2FwdGlvbiI6IjxzcGFuIHN0eWxlPVwiIGZvbnQtd2VpZ2h0OiAxMDA7IGxldHRlci1zcGFjaW5nOiAtN3B4O3RleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2VcIj5DYXRhbG9nPFwvc3Bhbj4iLCJsYXllcl9mb250X3NpemUiOiI4MHB4IiwibGF5ZXJfYmFja2dyb3VuZF9jb2xvciI6IiIsImxheWVyX2NvbG9yIjoiI2ZmZmZmZiIsImxheWVyX2xpbmsiOiIiLCJsYXllcl9hbmltYXRpb24iOiJmYWRlIiwibGF5ZXJfZWFzaW5nIjoiZWFzZU91dEV4cG8iLCJsYXllcl9zcGVlZCI6IjM1MCIsImxheWVyX3RvcCI6IjMzNSIsImxheWVyX2xlZnQiOiI3MzAiLCJsYXllcl9lbmR0aW1lIjoiMCIsImxheWVyX2VuZHNwZWVkIjoiMzAwIiwibGF5ZXJfZW5kYW5pbWF0aW9uIjoiYXV0byIsImxheWVyX2VuZGVhc2luZyI6Im5vdGhpbmciLCJ0aW1lX3N0YXJ0IjoiMTIwMCJ9XQ=='),
	(35, 1, 'Slide fashion2', '', 'Image_home.png', '', 'eyJ1c2V2aWRlbyI6IjAiLCJ2aWRlb2lkIjoiIiwidmlkZW9hdXRvIjoiMCIsImJhY2tncm91bmRfY29sb3IiOiIifQ==', ''),
	(36, 1, 'slide fashion3', '', 'Image_home.png', '', 'eyJ1c2V2aWRlbyI6IjAiLCJ2aWRlb2lkIjoiIiwidmlkZW9hdXRvIjoiMCIsImJhY2tncm91bmRfY29sb3IiOiIifQ==', ''),
	(37, 1, 'Slider Cd1', '', 'bg-sl-cd1.jpg', '', 'eyJ1c2V2aWRlbyI6IjAiLCJ2aWRlb2lkIjoiIiwidmlkZW9hdXRvIjoiMCIsImJhY2tncm91bmRfY29sb3IiOiIifQ==', 'W3sibGF5ZXJfdmlkZW9fdHlwZSI6InlvdXR1YmUiLCJsYXllcl92aWRlb19pZCI6IiIsImxheWVyX3ZpZGVvX2hlaWdodCI6IjIwMCIsImxheWVyX3ZpZGVvX3dpZHRoIjoiMzAwIiwibGF5ZXJfdmlkZW9fdGh1bWIiOiIiLCJsYXllcl9pZCI6IjFfMSIsImxheWVyX2NvbnRlbnQiOiJzbC1jZDEucG5nIiwibGF5ZXJfdHlwZSI6ImltYWdlIiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiWW91ciBJbWFnZSBIZXJlIDEiLCJsYXllcl9mb250X3NpemUiOiIiLCJsYXllcl9iYWNrZ3JvdW5kX2NvbG9yIjoiIiwibGF5ZXJfY29sb3IiOiIiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoibGZyIiwibGF5ZXJfZWFzaW5nIjoiZWFzZUluT3V0Q2lyYyIsImxheWVyX3NwZWVkIjoiMzUwIiwibGF5ZXJfdG9wIjoiMTI2IiwibGF5ZXJfbGVmdCI6IjI3MCIsImxheWVyX2VuZHRpbWUiOiIwIiwibGF5ZXJfZW5kc3BlZWQiOiIzMDAiLCJsYXllcl9lbmRhbmltYXRpb24iOiJhdXRvIiwibGF5ZXJfZW5kZWFzaW5nIjoibm90aGluZyIsInRpbWVfc3RhcnQiOiI0MDAifSx7ImxheWVyX3ZpZGVvX3R5cGUiOiJ5b3V0dWJlIiwibGF5ZXJfdmlkZW9faWQiOiIiLCJsYXllcl92aWRlb19oZWlnaHQiOiIyMDAiLCJsYXllcl92aWRlb193aWR0aCI6IjMwMCIsImxheWVyX3ZpZGVvX3RodW1iIjoiIiwibGF5ZXJfaWQiOiIxXzIiLCJsYXllcl9jb250ZW50Ijoic2wtY2QxLnBuZyIsImxheWVyX3R5cGUiOiJ0ZXh0IiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiPHNwYW4gc3R5bGU9XCJsZXR0ZXItc3BhY2luZzogMnB4O3RleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XCI+cGhpbGxpcCBwaGlsbGlwczxcL3NwYW4+IiwibGF5ZXJfZm9udF9zaXplIjoiMTJweCIsImxheWVyX2JhY2tncm91bmRfY29sb3IiOiIiLCJsYXllcl9jb2xvciI6IiNmZmZmZmYiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoiZmFkZSIsImxheWVyX2Vhc2luZyI6ImVhc2VPdXRFeHBvIiwibGF5ZXJfc3BlZWQiOiIzNTAiLCJsYXllcl90b3AiOiIxOTMiLCJsYXllcl9sZWZ0IjoiMCIsImxheWVyX2VuZHRpbWUiOiIwIiwibGF5ZXJfZW5kc3BlZWQiOiIzMDAiLCJsYXllcl9lbmRhbmltYXRpb24iOiJhdXRvIiwibGF5ZXJfZW5kZWFzaW5nIjoibm90aGluZyIsInRpbWVfc3RhcnQiOiI4MDAifSx7ImxheWVyX3ZpZGVvX3R5cGUiOiJ5b3V0dWJlIiwibGF5ZXJfdmlkZW9faWQiOiIiLCJsYXllcl92aWRlb19oZWlnaHQiOiIyMDAiLCJsYXllcl92aWRlb193aWR0aCI6IjMwMCIsImxheWVyX3ZpZGVvX3RodW1iIjoiIiwibGF5ZXJfaWQiOiIxXzMiLCJsYXllcl9jb250ZW50Ijoic2wtY2QxLnBuZyIsImxheWVyX3R5cGUiOiJ0ZXh0IiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiPHNwYW4gc3R5bGU9XCJmb250LXdlaWdodDogYm9sZDtsZXR0ZXItc3BhY2luZzogLTJweDt0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1wiPmJlaGluZCB0aGUgbGlnaHQ8XC9zcGFuPiIsImxheWVyX2ZvbnRfc2l6ZSI6IjYwcHgiLCJsYXllcl9iYWNrZ3JvdW5kX2NvbG9yIjoiIiwibGF5ZXJfY29sb3IiOiIjZmZmIiwibGF5ZXJfbGluayI6IiIsImxheWVyX2FuaW1hdGlvbiI6ImZhZGUiLCJsYXllcl9lYXNpbmciOiJlYXNlT3V0RXhwbyIsImxheWVyX3NwZWVkIjoiMzUwIiwibGF5ZXJfdG9wIjoiMTkzIiwibGF5ZXJfbGVmdCI6IjAiLCJsYXllcl9lbmR0aW1lIjoiMCIsImxheWVyX2VuZHNwZWVkIjoiMzAwIiwibGF5ZXJfZW5kYW5pbWF0aW9uIjoiYXV0byIsImxheWVyX2VuZGVhc2luZyI6Im5vdGhpbmciLCJ0aW1lX3N0YXJ0IjoiMTIwMCJ9LHsibGF5ZXJfdmlkZW9fdHlwZSI6InlvdXR1YmUiLCJsYXllcl92aWRlb19pZCI6IiIsImxheWVyX3ZpZGVvX2hlaWdodCI6IjIwMCIsImxheWVyX3ZpZGVvX3dpZHRoIjoiMzAwIiwibGF5ZXJfdmlkZW9fdGh1bWIiOiIiLCJsYXllcl9pZCI6IjFfNCIsImxheWVyX2NvbnRlbnQiOiJzbC1jZDEucG5nIiwibGF5ZXJfdHlwZSI6InRleHQiLCJsYXllcl9jbGFzcyI6IiIsImxheWVyX2NhcHRpb24iOiI8ZGl2IHN0eWxlPVwiYm9yZGVyLXRvcDogM3B4IHNvbGlkICNmZmY7ICB3aWR0aDogMzBweFwiPjxcL2Rpdj4iLCJsYXllcl9mb250X3NpemUiOiIiLCJsYXllcl9iYWNrZ3JvdW5kX2NvbG9yIjoiIiwibGF5ZXJfY29sb3IiOiIiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoiZmFkZSIsImxheWVyX2Vhc2luZyI6ImVhc2VPdXRFeHBvIiwibGF5ZXJfc3BlZWQiOiIzNTAiLCJsYXllcl90b3AiOiIyNzYiLCJsYXllcl9sZWZ0IjoiMCIsImxheWVyX2VuZHRpbWUiOiIwIiwibGF5ZXJfZW5kc3BlZWQiOiIzMDAiLCJsYXllcl9lbmRhbmltYXRpb24iOiJhdXRvIiwibGF5ZXJfZW5kZWFzaW5nIjoibm90aGluZyIsInRpbWVfc3RhcnQiOiIxNjAwIn0seyJsYXllcl92aWRlb190eXBlIjoieW91dHViZSIsImxheWVyX3ZpZGVvX2lkIjoiIiwibGF5ZXJfdmlkZW9faGVpZ2h0IjoiMjAwIiwibGF5ZXJfdmlkZW9fd2lkdGgiOiIzMDAiLCJsYXllcl92aWRlb190aHVtYiI6IiIsImxheWVyX2lkIjoiMV81IiwibGF5ZXJfY29udGVudCI6InNsLWNkMS5wbmciLCJsYXllcl90eXBlIjoidGV4dCIsImxheWVyX2NsYXNzIjoiIiwibGF5ZXJfY2FwdGlvbiI6IjxzcGFuIHN0eWxlPVwibGV0dGVyLXNwYWNpbmc6IDJweDt0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1wiPk11c2ljIEZyb20gVGhlIE1vdGlvbiBQaWN0dXJlPFwvc3Bhbj4iLCJsYXllcl9mb250X3NpemUiOiIxMnB4IiwibGF5ZXJfYmFja2dyb3VuZF9jb2xvciI6IiIsImxheWVyX2NvbG9yIjoiI2ZmZmZmZiIsImxheWVyX2xpbmsiOiIiLCJsYXllcl9hbmltYXRpb24iOiJmYWRlIiwibGF5ZXJfZWFzaW5nIjoiZWFzZU91dEV4cG8iLCJsYXllcl9zcGVlZCI6IjM1MCIsImxheWVyX3RvcCI6IjI5NSIsImxheWVyX2xlZnQiOiIwIiwibGF5ZXJfZW5kdGltZSI6IjAiLCJsYXllcl9lbmRzcGVlZCI6IjMwMCIsImxheWVyX2VuZGFuaW1hdGlvbiI6ImF1dG8iLCJsYXllcl9lbmRlYXNpbmciOiJub3RoaW5nIiwidGltZV9zdGFydCI6IjIwMDAifSx7ImxheWVyX3ZpZGVvX3R5cGUiOiJ5b3V0dWJlIiwibGF5ZXJfdmlkZW9faWQiOiIiLCJsYXllcl92aWRlb19oZWlnaHQiOiIyMDAiLCJsYXllcl92aWRlb193aWR0aCI6IjMwMCIsImxheWVyX3ZpZGVvX3RodW1iIjoiIiwibGF5ZXJfaWQiOiIxXzYiLCJsYXllcl9jb250ZW50Ijoic2wtY2QxLnBuZyIsImxheWVyX3R5cGUiOiJ0ZXh0IiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiPHNwYW4gY2xhc3M9XCJidG4gYnRuLW91dGxpbmVcIj52aWV3IGNvbGxlY3Rpb248XC9zcGFuPiIsImxheWVyX2ZvbnRfc2l6ZSI6IiIsImxheWVyX2JhY2tncm91bmRfY29sb3IiOiIiLCJsYXllcl9jb2xvciI6IiIsImxheWVyX2xpbmsiOiIiLCJsYXllcl9hbmltYXRpb24iOiJmYWRlIiwibGF5ZXJfZWFzaW5nIjoiZWFzZU91dEV4cG8iLCJsYXllcl9zcGVlZCI6IjM1MCIsImxheWVyX3RvcCI6IjM1MyIsImxheWVyX2xlZnQiOiIwIiwibGF5ZXJfZW5kdGltZSI6IjAiLCJsYXllcl9lbmRzcGVlZCI6IjMwMCIsImxheWVyX2VuZGFuaW1hdGlvbiI6ImF1dG8iLCJsYXllcl9lbmRlYXNpbmciOiJub3RoaW5nIiwidGltZV9zdGFydCI6IjI0MDAifV0='),
	(38, 1, 'Slider Cd2', '', 'bg-sl-cd2.jpg', '', 'eyJ1c2V2aWRlbyI6IjAiLCJ2aWRlb2lkIjoiIiwidmlkZW9hdXRvIjoiMCIsImJhY2tncm91bmRfY29sb3IiOiIifQ==', 'W3sibGF5ZXJfdmlkZW9fdHlwZSI6InlvdXR1YmUiLCJsYXllcl92aWRlb19pZCI6IiIsImxheWVyX3ZpZGVvX2hlaWdodCI6IjIwMCIsImxheWVyX3ZpZGVvX3dpZHRoIjoiMzAwIiwibGF5ZXJfdmlkZW9fdGh1bWIiOiIiLCJsYXllcl9pZCI6IjFfMSIsImxheWVyX2NvbnRlbnQiOiJzbC1jZDIucG5nIiwibGF5ZXJfdHlwZSI6ImltYWdlIiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiWW91ciBJbWFnZSBIZXJlIDEiLCJsYXllcl9mb250X3NpemUiOiIiLCJsYXllcl9iYWNrZ3JvdW5kX2NvbG9yIjoiIiwibGF5ZXJfY29sb3IiOiIiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoibGZyIiwibGF5ZXJfZWFzaW5nIjoiZWFzZUluT3V0Q2lyYyIsImxheWVyX3NwZWVkIjoiMzUwIiwibGF5ZXJfdG9wIjoiMTI2IiwibGF5ZXJfbGVmdCI6IjI3MCIsImxheWVyX2VuZHRpbWUiOiIwIiwibGF5ZXJfZW5kc3BlZWQiOiIzMDAiLCJsYXllcl9lbmRhbmltYXRpb24iOiJhdXRvIiwibGF5ZXJfZW5kZWFzaW5nIjoibm90aGluZyIsInRpbWVfc3RhcnQiOiI0MDAifSx7ImxheWVyX3ZpZGVvX3R5cGUiOiJ5b3V0dWJlIiwibGF5ZXJfdmlkZW9faWQiOiIiLCJsYXllcl92aWRlb19oZWlnaHQiOiIyMDAiLCJsYXllcl92aWRlb193aWR0aCI6IjMwMCIsImxheWVyX3ZpZGVvX3RodW1iIjoiIiwibGF5ZXJfaWQiOiIxXzIiLCJsYXllcl9jb250ZW50Ijoic2wtY2QxLnBuZyIsImxheWVyX3R5cGUiOiJ0ZXh0IiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiPHNwYW4gc3R5bGU9XCJmb250LXdlaWdodDogYm9sZDtsZXR0ZXItc3BhY2luZzogLTJweDt0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1wiPnBhc3Npb25lPFwvc3Bhbj4iLCJsYXllcl9mb250X3NpemUiOiI2MHB4IiwibGF5ZXJfYmFja2dyb3VuZF9jb2xvciI6IiIsImxheWVyX2NvbG9yIjoiI2ZmZiIsImxheWVyX2xpbmsiOiIiLCJsYXllcl9hbmltYXRpb24iOiJmYWRlIiwibGF5ZXJfZWFzaW5nIjoiZWFzZU91dEV4cG8iLCJsYXllcl9zcGVlZCI6IjM1MCIsImxheWVyX3RvcCI6IjE5MyIsImxheWVyX2xlZnQiOiIwIiwibGF5ZXJfZW5kdGltZSI6IjAiLCJsYXllcl9lbmRzcGVlZCI6IjMwMCIsImxheWVyX2VuZGFuaW1hdGlvbiI6ImF1dG8iLCJsYXllcl9lbmRlYXNpbmciOiJub3RoaW5nIiwidGltZV9zdGFydCI6IjEyMDAifSx7ImxheWVyX3ZpZGVvX3R5cGUiOiJ5b3V0dWJlIiwibGF5ZXJfdmlkZW9faWQiOiIiLCJsYXllcl92aWRlb19oZWlnaHQiOiIyMDAiLCJsYXllcl92aWRlb193aWR0aCI6IjMwMCIsImxheWVyX3ZpZGVvX3RodW1iIjoiIiwibGF5ZXJfaWQiOiIxXzMiLCJsYXllcl9jb250ZW50Ijoic2wtY2QxLnBuZyIsImxheWVyX3R5cGUiOiJ0ZXh0IiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiPHNwYW4gc3R5bGU9XCJsZXR0ZXItc3BhY2luZzogMnB4O3RleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XCI+YW5kcmVhIGJvY2VsbGk8XC9zcGFuPiIsImxheWVyX2ZvbnRfc2l6ZSI6IjEycHgiLCJsYXllcl9iYWNrZ3JvdW5kX2NvbG9yIjoiIiwibGF5ZXJfY29sb3IiOiIjZmZmZmZmIiwibGF5ZXJfbGluayI6IiIsImxheWVyX2FuaW1hdGlvbiI6ImZhZGUiLCJsYXllcl9lYXNpbmciOiJlYXNlT3V0RXhwbyIsImxheWVyX3NwZWVkIjoiMzUwIiwibGF5ZXJfdG9wIjoiMTg1IiwibGF5ZXJfbGVmdCI6IjMiLCJsYXllcl9lbmR0aW1lIjoiMCIsImxheWVyX2VuZHNwZWVkIjoiMzAwIiwibGF5ZXJfZW5kYW5pbWF0aW9uIjoiYXV0byIsImxheWVyX2VuZGVhc2luZyI6Im5vdGhpbmciLCJ0aW1lX3N0YXJ0IjoiODAwIn0seyJsYXllcl92aWRlb190eXBlIjoieW91dHViZSIsImxheWVyX3ZpZGVvX2lkIjoiIiwibGF5ZXJfdmlkZW9faGVpZ2h0IjoiMjAwIiwibGF5ZXJfdmlkZW9fd2lkdGgiOiIzMDAiLCJsYXllcl92aWRlb190aHVtYiI6IiIsImxheWVyX2lkIjoiMV80IiwibGF5ZXJfY29udGVudCI6InNsLWNkMS5wbmciLCJsYXllcl90eXBlIjoidGV4dCIsImxheWVyX2NsYXNzIjoiIiwibGF5ZXJfY2FwdGlvbiI6IjxkaXYgc3R5bGU9XCJib3JkZXItdG9wOiAzcHggc29saWQgI2ZmZjsgIHdpZHRoOiAzMHB4XCI+PFwvZGl2PiIsImxheWVyX2ZvbnRfc2l6ZSI6IiIsImxheWVyX2JhY2tncm91bmRfY29sb3IiOiIiLCJsYXllcl9jb2xvciI6IiIsImxheWVyX2xpbmsiOiIiLCJsYXllcl9hbmltYXRpb24iOiJmYWRlIiwibGF5ZXJfZWFzaW5nIjoiZWFzZU91dEV4cG8iLCJsYXllcl9zcGVlZCI6IjM1MCIsImxheWVyX3RvcCI6IjI3NiIsImxheWVyX2xlZnQiOiIwIiwibGF5ZXJfZW5kdGltZSI6IjAiLCJsYXllcl9lbmRzcGVlZCI6IjMwMCIsImxheWVyX2VuZGFuaW1hdGlvbiI6ImF1dG8iLCJsYXllcl9lbmRlYXNpbmciOiJub3RoaW5nIiwidGltZV9zdGFydCI6IjE2MDAifSx7ImxheWVyX3ZpZGVvX3R5cGUiOiJ5b3V0dWJlIiwibGF5ZXJfdmlkZW9faWQiOiIiLCJsYXllcl92aWRlb19oZWlnaHQiOiIyMDAiLCJsYXllcl92aWRlb193aWR0aCI6IjMwMCIsImxheWVyX3ZpZGVvX3RodW1iIjoiIiwibGF5ZXJfaWQiOiIxXzUiLCJsYXllcl9jb250ZW50Ijoic2wtY2QxLnBuZyIsImxheWVyX3R5cGUiOiJ0ZXh0IiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiPHNwYW4gc3R5bGU9XCJsZXR0ZXItc3BhY2luZzogMnB4O3RleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XCI+TXVzaWMgRnJvbSBUaGUgTW90aW9uIFBpY3R1cmU8XC9zcGFuPiIsImxheWVyX2ZvbnRfc2l6ZSI6IjEycHgiLCJsYXllcl9iYWNrZ3JvdW5kX2NvbG9yIjoiIiwibGF5ZXJfY29sb3IiOiIjZmZmZmZmIiwibGF5ZXJfbGluayI6IiIsImxheWVyX2FuaW1hdGlvbiI6ImZhZGUiLCJsYXllcl9lYXNpbmciOiJlYXNlT3V0RXhwbyIsImxheWVyX3NwZWVkIjoiMzUwIiwibGF5ZXJfdG9wIjoiMjk1IiwibGF5ZXJfbGVmdCI6IjAiLCJsYXllcl9lbmR0aW1lIjoiMCIsImxheWVyX2VuZHNwZWVkIjoiMzAwIiwibGF5ZXJfZW5kYW5pbWF0aW9uIjoiYXV0byIsImxheWVyX2VuZGVhc2luZyI6Im5vdGhpbmciLCJ0aW1lX3N0YXJ0IjoiMjAwMCJ9LHsibGF5ZXJfdmlkZW9fdHlwZSI6InlvdXR1YmUiLCJsYXllcl92aWRlb19pZCI6IiIsImxheWVyX3ZpZGVvX2hlaWdodCI6IjIwMCIsImxheWVyX3ZpZGVvX3dpZHRoIjoiMzAwIiwibGF5ZXJfdmlkZW9fdGh1bWIiOiIiLCJsYXllcl9pZCI6IjFfNiIsImxheWVyX2NvbnRlbnQiOiJzbC1jZDEucG5nIiwibGF5ZXJfdHlwZSI6InRleHQiLCJsYXllcl9jbGFzcyI6IiIsImxheWVyX2NhcHRpb24iOiI8c3BhbiBjbGFzcz1cImJ0biBidG4tb3V0bGluZVwiPnZpZXcgY29sbGVjdGlvbjxcL3NwYW4+IiwibGF5ZXJfZm9udF9zaXplIjoiIiwibGF5ZXJfYmFja2dyb3VuZF9jb2xvciI6IiIsImxheWVyX2NvbG9yIjoiIiwibGF5ZXJfbGluayI6IiIsImxheWVyX2FuaW1hdGlvbiI6ImZhZGUiLCJsYXllcl9lYXNpbmciOiJlYXNlT3V0RXhwbyIsImxheWVyX3NwZWVkIjoiMzUwIiwibGF5ZXJfdG9wIjoiMzUzIiwibGF5ZXJfbGVmdCI6IjAiLCJsYXllcl9lbmR0aW1lIjoiMCIsImxheWVyX2VuZHNwZWVkIjoiMzAwIiwibGF5ZXJfZW5kYW5pbWF0aW9uIjoiYXV0byIsImxheWVyX2VuZGVhc2luZyI6Im5vdGhpbmciLCJ0aW1lX3N0YXJ0IjoiMjQwMCJ9XQ=='),
	(39, 1, 'Slider Cd3', '', 'bg-sl-cd3.jpg', '', 'eyJ1c2V2aWRlbyI6IjAiLCJ2aWRlb2lkIjoiIiwidmlkZW9hdXRvIjoiMCIsImJhY2tncm91bmRfY29sb3IiOiIifQ==', 'W3sibGF5ZXJfdmlkZW9fdHlwZSI6InlvdXR1YmUiLCJsYXllcl92aWRlb19pZCI6IiIsImxheWVyX3ZpZGVvX2hlaWdodCI6IjIwMCIsImxheWVyX3ZpZGVvX3dpZHRoIjoiMzAwIiwibGF5ZXJfdmlkZW9fdGh1bWIiOiIiLCJsYXllcl9pZCI6IjFfMSIsImxheWVyX2NvbnRlbnQiOiJzbC1jZDMucG5nIiwibGF5ZXJfdHlwZSI6ImltYWdlIiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiWW91ciBJbWFnZSBIZXJlIDEiLCJsYXllcl9mb250X3NpemUiOiIiLCJsYXllcl9iYWNrZ3JvdW5kX2NvbG9yIjoiIiwibGF5ZXJfY29sb3IiOiIiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoibGZyIiwibGF5ZXJfZWFzaW5nIjoiZWFzZUluT3V0Q2lyYyIsImxheWVyX3NwZWVkIjoiMzUwIiwibGF5ZXJfdG9wIjoiMTI2IiwibGF5ZXJfbGVmdCI6IjI3MCIsImxheWVyX2VuZHRpbWUiOiIwIiwibGF5ZXJfZW5kc3BlZWQiOiIzMDAiLCJsYXllcl9lbmRhbmltYXRpb24iOiJhdXRvIiwibGF5ZXJfZW5kZWFzaW5nIjoibm90aGluZyIsInRpbWVfc3RhcnQiOiI0MDAifSx7ImxheWVyX3ZpZGVvX3R5cGUiOiJ5b3V0dWJlIiwibGF5ZXJfdmlkZW9faWQiOiIiLCJsYXllcl92aWRlb19oZWlnaHQiOiIyMDAiLCJsYXllcl92aWRlb193aWR0aCI6IjMwMCIsImxheWVyX3ZpZGVvX3RodW1iIjoiIiwibGF5ZXJfaWQiOiIxXzIiLCJsYXllcl9jb250ZW50Ijoic2wtY2QxLnBuZyIsImxheWVyX3R5cGUiOiJ0ZXh0IiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiPHNwYW4gc3R5bGU9XCJmb250LXdlaWdodDogYm9sZDtsZXR0ZXItc3BhY2luZzogLTJweDt0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1wiPnJpZGUgb3V0PFwvc3Bhbj4iLCJsYXllcl9mb250X3NpemUiOiI2MHB4IiwibGF5ZXJfYmFja2dyb3VuZF9jb2xvciI6IiIsImxheWVyX2NvbG9yIjoiI2ZmZiIsImxheWVyX2xpbmsiOiIiLCJsYXllcl9hbmltYXRpb24iOiJmYWRlIiwibGF5ZXJfZWFzaW5nIjoiZWFzZU91dEV4cG8iLCJsYXllcl9zcGVlZCI6IjM1MCIsImxheWVyX3RvcCI6IjE5MyIsImxheWVyX2xlZnQiOiIwIiwibGF5ZXJfZW5kdGltZSI6IjAiLCJsYXllcl9lbmRzcGVlZCI6IjMwMCIsImxheWVyX2VuZGFuaW1hdGlvbiI6ImF1dG8iLCJsYXllcl9lbmRlYXNpbmciOiJub3RoaW5nIiwidGltZV9zdGFydCI6IjEyMDAifSx7ImxheWVyX3ZpZGVvX3R5cGUiOiJ5b3V0dWJlIiwibGF5ZXJfdmlkZW9faWQiOiIiLCJsYXllcl92aWRlb19oZWlnaHQiOiIyMDAiLCJsYXllcl92aWRlb193aWR0aCI6IjMwMCIsImxheWVyX3ZpZGVvX3RodW1iIjoiIiwibGF5ZXJfaWQiOiIxXzMiLCJsYXllcl9jb250ZW50Ijoic2wtY2QxLnBuZyIsImxheWVyX3R5cGUiOiJ0ZXh0IiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiPHNwYW4gc3R5bGU9XCJsZXR0ZXItc3BhY2luZzogMnB4O3RleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XCI+Ym9iIHNlZ2VyPFwvc3Bhbj4iLCJsYXllcl9mb250X3NpemUiOiIxMnB4IiwibGF5ZXJfYmFja2dyb3VuZF9jb2xvciI6IiIsImxheWVyX2NvbG9yIjoiI2ZmZmZmZiIsImxheWVyX2xpbmsiOiIiLCJsYXllcl9hbmltYXRpb24iOiJmYWRlIiwibGF5ZXJfZWFzaW5nIjoiZWFzZU91dEV4cG8iLCJsYXllcl9zcGVlZCI6IjM1MCIsImxheWVyX3RvcCI6IjE4NSIsImxheWVyX2xlZnQiOiIzIiwibGF5ZXJfZW5kdGltZSI6IjAiLCJsYXllcl9lbmRzcGVlZCI6IjMwMCIsImxheWVyX2VuZGFuaW1hdGlvbiI6ImF1dG8iLCJsYXllcl9lbmRlYXNpbmciOiJub3RoaW5nIiwidGltZV9zdGFydCI6IjgwMCJ9LHsibGF5ZXJfdmlkZW9fdHlwZSI6InlvdXR1YmUiLCJsYXllcl92aWRlb19pZCI6IiIsImxheWVyX3ZpZGVvX2hlaWdodCI6IjIwMCIsImxheWVyX3ZpZGVvX3dpZHRoIjoiMzAwIiwibGF5ZXJfdmlkZW9fdGh1bWIiOiIiLCJsYXllcl9pZCI6IjFfNCIsImxheWVyX2NvbnRlbnQiOiJzbC1jZDEucG5nIiwibGF5ZXJfdHlwZSI6InRleHQiLCJsYXllcl9jbGFzcyI6IiIsImxheWVyX2NhcHRpb24iOiI8ZGl2IHN0eWxlPVwiYm9yZGVyLXRvcDogM3B4IHNvbGlkICNmZmY7ICB3aWR0aDogMzBweFwiPjxcL2Rpdj4iLCJsYXllcl9mb250X3NpemUiOiIiLCJsYXllcl9iYWNrZ3JvdW5kX2NvbG9yIjoiIiwibGF5ZXJfY29sb3IiOiIiLCJsYXllcl9saW5rIjoiIiwibGF5ZXJfYW5pbWF0aW9uIjoiZmFkZSIsImxheWVyX2Vhc2luZyI6ImVhc2VPdXRFeHBvIiwibGF5ZXJfc3BlZWQiOiIzNTAiLCJsYXllcl90b3AiOiIyNzYiLCJsYXllcl9sZWZ0IjoiMCIsImxheWVyX2VuZHRpbWUiOiIwIiwibGF5ZXJfZW5kc3BlZWQiOiIzMDAiLCJsYXllcl9lbmRhbmltYXRpb24iOiJhdXRvIiwibGF5ZXJfZW5kZWFzaW5nIjoibm90aGluZyIsInRpbWVfc3RhcnQiOiIxNjAwIn0seyJsYXllcl92aWRlb190eXBlIjoieW91dHViZSIsImxheWVyX3ZpZGVvX2lkIjoiIiwibGF5ZXJfdmlkZW9faGVpZ2h0IjoiMjAwIiwibGF5ZXJfdmlkZW9fd2lkdGgiOiIzMDAiLCJsYXllcl92aWRlb190aHVtYiI6IiIsImxheWVyX2lkIjoiMV81IiwibGF5ZXJfY29udGVudCI6InNsLWNkMS5wbmciLCJsYXllcl90eXBlIjoidGV4dCIsImxheWVyX2NsYXNzIjoiIiwibGF5ZXJfY2FwdGlvbiI6IjxzcGFuIHN0eWxlPVwibGV0dGVyLXNwYWNpbmc6IDJweDt0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1wiPk11c2ljIEZyb20gVGhlIE1vdGlvbiBQaWN0dXJlPFwvc3Bhbj4iLCJsYXllcl9mb250X3NpemUiOiIxMnB4IiwibGF5ZXJfYmFja2dyb3VuZF9jb2xvciI6IiIsImxheWVyX2NvbG9yIjoiI2ZmZmZmZiIsImxheWVyX2xpbmsiOiIiLCJsYXllcl9hbmltYXRpb24iOiJmYWRlIiwibGF5ZXJfZWFzaW5nIjoiZWFzZU91dEV4cG8iLCJsYXllcl9zcGVlZCI6IjM1MCIsImxheWVyX3RvcCI6IjI5NSIsImxheWVyX2xlZnQiOiIwIiwibGF5ZXJfZW5kdGltZSI6IjAiLCJsYXllcl9lbmRzcGVlZCI6IjMwMCIsImxheWVyX2VuZGFuaW1hdGlvbiI6ImF1dG8iLCJsYXllcl9lbmRlYXNpbmciOiJub3RoaW5nIiwidGltZV9zdGFydCI6IjIwMDAifSx7ImxheWVyX3ZpZGVvX3R5cGUiOiJ5b3V0dWJlIiwibGF5ZXJfdmlkZW9faWQiOiIiLCJsYXllcl92aWRlb19oZWlnaHQiOiIyMDAiLCJsYXllcl92aWRlb193aWR0aCI6IjMwMCIsImxheWVyX3ZpZGVvX3RodW1iIjoiIiwibGF5ZXJfaWQiOiIxXzYiLCJsYXllcl9jb250ZW50Ijoic2wtY2QxLnBuZyIsImxheWVyX3R5cGUiOiJ0ZXh0IiwibGF5ZXJfY2xhc3MiOiIiLCJsYXllcl9jYXB0aW9uIjoiPHNwYW4gY2xhc3M9XCJidG4gYnRuLW91dGxpbmVcIj52aWV3IGNvbGxlY3Rpb248XC9zcGFuPiIsImxheWVyX2ZvbnRfc2l6ZSI6IiIsImxheWVyX2JhY2tncm91bmRfY29sb3IiOiIiLCJsYXllcl9jb2xvciI6IiIsImxheWVyX2xpbmsiOiIiLCJsYXllcl9hbmltYXRpb24iOiJmYWRlIiwibGF5ZXJfZWFzaW5nIjoiZWFzZU91dEV4cG8iLCJsYXllcl9zcGVlZCI6IjM1MCIsImxheWVyX3RvcCI6IjM1MyIsImxheWVyX2xlZnQiOiIwIiwibGF5ZXJfZW5kdGltZSI6IjAiLCJsYXllcl9lbmRzcGVlZCI6IjMwMCIsImxheWVyX2VuZGFuaW1hdGlvbiI6ImF1dG8iLCJsYXllcl9lbmRlYXNpbmciOiJub3RoaW5nIiwidGltZV9zdGFydCI6IjI0MDAifV0=');
/*!40000 ALTER TABLE `ps_leosliderlayer_slides_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_linksmenutop
DROP TABLE IF EXISTS `ps_linksmenutop`;
CREATE TABLE IF NOT EXISTS `ps_linksmenutop` (
  `id_linksmenutop` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(11) unsigned NOT NULL,
  `new_window` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_linksmenutop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_linksmenutop: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_linksmenutop` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_linksmenutop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_linksmenutop_lang
DROP TABLE IF EXISTS `ps_linksmenutop_lang`;
CREATE TABLE IF NOT EXISTS `ps_linksmenutop_lang` (
  `id_linksmenutop` int(11) unsigned NOT NULL,
  `id_lang` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  `label` varchar(128) NOT NULL,
  `link` varchar(128) NOT NULL,
  KEY `id_linksmenutop` (`id_linksmenutop`,`id_lang`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_linksmenutop_lang: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_linksmenutop_lang` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_linksmenutop_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_log
DROP TABLE IF EXISTS `ps_log`;
CREATE TABLE IF NOT EXISTS `ps_log` (
  `id_log` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `severity` tinyint(1) NOT NULL,
  `error_code` int(11) DEFAULT NULL,
  `message` text NOT NULL,
  `object_type` varchar(32) DEFAULT NULL,
  `object_id` int(10) unsigned DEFAULT NULL,
  `id_employee` int(10) unsigned DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_log`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_log: ~59 rows (approximately)
/*!40000 ALTER TABLE `ps_log` DISABLE KEYS */;
INSERT INTO `ps_log` (`id_log`, `severity`, `error_code`, `message`, `object_type`, `object_id`, `id_employee`, `date_add`, `date_upd`) VALUES
	(1, 1, 0, 'Conexión con el Backoffice desde 127.0.0.1', '', 0, 1, '2015-05-08 14:02:07', '2015-05-08 14:02:07'),
	(2, 1, 0, 'Conexión con el Backoffice desde 127.0.0.1', '', 0, 1, '2015-05-08 13:40:35', '2015-05-08 13:40:35'),
	(3, 1, 0, 'Employee añadido', 'Employee', 2, 1, '2015-05-08 13:51:52', '2015-05-08 13:51:52'),
	(4, 1, 0, 'Conexión con el Backoffice desde 127.0.0.1', '', 0, 2, '2015-05-08 13:52:07', '2015-05-08 13:52:07'),
	(5, 1, 0, 'Conexión con el Backoffice desde 127.0.0.1', '', 0, 2, '2015-05-11 09:05:21', '2015-05-11 09:05:21'),
	(6, 1, 0, 'Conexión con el Backoffice desde 127.0.0.1', '', 0, 2, '2015-05-11 11:09:01', '2015-05-11 11:09:01'),
	(7, 1, 0, 'Conexión con el Backoffice desde 127.0.0.1', '', 0, 2, '2015-05-11 12:31:02', '2015-05-11 12:31:02'),
	(8, 1, 0, 'Conexión con el Backoffice desde 127.0.0.1', '', 0, 2, '2015-05-11 13:02:09', '2015-05-11 13:02:09'),
	(9, 1, 0, 'Conexión con el Backoffice desde 127.0.0.1', '', 0, 2, '2015-05-11 13:42:33', '2015-05-11 13:42:33'),
	(10, 1, 0, 'Conexión con el Backoffice desde 127.0.0.1', '', 0, 2, '2015-05-11 14:07:49', '2015-05-11 14:07:49'),
	(11, 1, 0, 'Conexión con el Backoffice desde 127.0.0.1', '', 0, 2, '2015-05-11 14:31:44', '2015-05-11 14:31:44'),
	(12, 1, 0, 'Conexión con el Backoffice desde 127.0.0.1', '', 0, 2, '2015-05-11 15:23:37', '2015-05-11 15:23:37'),
	(13, 1, 0, 'Conexión con el Backoffice desde 127.0.0.1', '', 0, 2, '2015-05-11 16:22:20', '2015-05-11 16:22:20'),
	(14, 1, 0, 'Conexión con el Backoffice desde 127.0.0.1', '', 0, 2, '2015-05-11 17:25:30', '2015-05-11 17:25:30'),
	(15, 1, 0, 'Conexión con el Backoffice desde 127.0.0.1', '', 0, 2, '2015-05-11 17:48:33', '2015-05-11 17:48:33'),
	(16, 1, 0, 'Conexión con el Backoffice desde 127.0.0.1', '', 0, 2, '2015-05-12 09:04:14', '2015-05-12 09:04:14'),
	(17, 1, 0, 'Category modificación', 'Category', 3, 2, '2015-05-12 09:07:34', '2015-05-12 09:07:34'),
	(18, 1, 0, 'Category modificación', 'Category', 3, 2, '2015-05-12 09:08:11', '2015-05-12 09:08:11'),
	(19, 1, 0, 'Category añadido', 'Category', 12, 2, '2015-05-12 09:31:44', '2015-05-12 09:31:44'),
	(20, 1, 0, 'Category añadido', 'Category', 13, 2, '2015-05-12 09:32:21', '2015-05-12 09:32:21'),
	(21, 1, 0, 'Category añadido', 'Category', 14, 2, '2015-05-12 09:32:38', '2015-05-12 09:32:38'),
	(22, 1, 0, 'Category añadido', 'Category', 15, 2, '2015-05-12 09:33:10', '2015-05-12 09:33:10'),
	(23, 1, 0, 'Category borrado', 'Category', 12, 2, '2015-05-12 09:35:37', '2015-05-12 09:35:37'),
	(24, 1, 0, 'Category modificación', 'Category', 4, 2, '2015-05-12 09:36:31', '2015-05-12 09:36:31'),
	(25, 1, 0, 'Category modificación', 'Category', 4, 2, '2015-05-12 09:36:47', '2015-05-12 09:36:47'),
	(26, 1, 0, 'Category modificación', 'Category', 5, 2, '2015-05-12 09:40:27', '2015-05-12 09:40:27'),
	(27, 1, 0, 'Category modificación', 'Category', 5, 2, '2015-05-12 09:40:38', '2015-05-12 09:40:38'),
	(28, 1, 0, 'Category modificación', 'Category', 7, 2, '2015-05-12 09:41:21', '2015-05-12 09:41:21'),
	(29, 1, 0, 'Category modificación', 'Category', 7, 2, '2015-05-12 09:41:38', '2015-05-12 09:41:38'),
	(30, 1, 0, 'Category modificación', 'Category', 8, 2, '2015-05-12 09:42:29', '2015-05-12 09:42:29'),
	(31, 1, 0, 'Category modificación', 'Category', 8, 2, '2015-05-12 09:42:46', '2015-05-12 09:42:46'),
	(32, 1, 0, 'Category modificación', 'Category', 8, 2, '2015-05-12 09:44:22', '2015-05-12 09:44:22'),
	(33, 1, 0, 'Category modificación', 'Category', 9, 2, '2015-05-12 09:45:09', '2015-05-12 09:45:09'),
	(34, 1, 0, 'Category modificación', 'Category', 9, 2, '2015-05-12 09:45:25', '2015-05-12 09:45:25'),
	(35, 1, 0, 'Category modificación', 'Category', 9, 2, '2015-05-12 09:45:43', '2015-05-12 09:45:43'),
	(36, 1, 0, 'Category modificación', 'Category', 10, 2, '2015-05-12 09:46:23', '2015-05-12 09:46:23'),
	(37, 1, 0, 'Feature modificación', 'Feature', 4, 2, '2015-05-12 10:04:32', '2015-05-12 10:04:32'),
	(38, 1, 0, 'FeatureValue añadido', 'FeatureValue', 34, 2, '2015-05-12 10:05:25', '2015-05-12 10:05:25'),
	(39, 1, 0, 'FeatureValue añadido', 'FeatureValue', 35, 2, '2015-05-12 10:05:37', '2015-05-12 10:05:37'),
	(40, 1, 0, 'FeatureValue añadido', 'FeatureValue', 36, 2, '2015-05-12 10:05:46', '2015-05-12 10:05:46'),
	(41, 1, 0, 'FeatureValue añadido', 'FeatureValue', 37, 2, '2015-05-12 10:06:01', '2015-05-12 10:06:01'),
	(42, 1, 0, 'Feature modificación', 'Feature', 4, 2, '2015-05-12 10:07:01', '2015-05-12 10:07:01'),
	(43, 1, 0, 'Feature modificación', 'Feature', 4, 2, '2015-05-12 10:11:21', '2015-05-12 10:11:21'),
	(44, 1, 0, 'Category añadido', 'Category', 16, 2, '2015-05-12 10:16:55', '2015-05-12 10:16:55'),
	(45, 1, 0, 'Category añadido', 'Category', 17, 2, '2015-05-12 10:17:32', '2015-05-12 10:17:32'),
	(46, 1, 0, 'Category añadido', 'Category', 18, 2, '2015-05-12 10:18:02', '2015-05-12 10:18:02'),
	(47, 1, 0, 'Category añadido', 'Category', 19, 2, '2015-05-12 10:19:12', '2015-05-12 10:19:12'),
	(48, 1, 0, 'Category añadido', 'Category', 20, 2, '2015-05-12 10:19:35', '2015-05-12 10:19:35'),
	(49, 1, 0, 'Category añadido', 'Category', 21, 2, '2015-05-12 10:20:02', '2015-05-12 10:20:02'),
	(50, 1, 0, 'Category añadido', 'Category', 22, 2, '2015-05-12 10:20:23', '2015-05-12 10:20:23'),
	(51, 1, 0, 'Product modificación', 'Product', 1, 2, '2015-05-12 10:23:26', '2015-05-12 10:23:26'),
	(52, 1, 0, 'Conexión con el Backoffice desde 127.0.0.1', '', 0, 2, '2015-05-12 11:26:38', '2015-05-12 11:26:38'),
	(53, 1, 0, 'Category modificación', 'Category', 3, 2, '2015-05-12 11:27:19', '2015-05-12 11:27:19'),
	(54, 1, 0, 'Conexión con el Backoffice desde 127.0.0.1', '', 0, 2, '2015-05-12 11:52:17', '2015-05-12 11:52:17'),
	(55, 1, 0, 'Conexión con el Backoffice desde 127.0.0.1', '', 0, 2, '2015-05-12 13:12:50', '2015-05-12 13:12:50'),
	(56, 1, 0, 'Conexión con el Backoffice desde 127.0.0.1', '', 0, 2, '2015-05-12 14:01:11', '2015-05-12 14:01:11'),
	(57, 1, 0, 'Conexión con el Backoffice desde 127.0.0.1', '', 0, 2, '2015-05-12 14:23:06', '2015-05-12 14:23:06'),
	(58, 1, 0, 'Conexión con el Backoffice desde 127.0.0.1', '', 0, 2, '2015-05-12 15:38:30', '2015-05-12 15:38:30'),
	(59, 1, 0, 'Conexión con el Backoffice desde 127.0.0.1', '', 0, 2, '2015-05-12 17:43:35', '2015-05-12 17:43:35');
/*!40000 ALTER TABLE `ps_log` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_mail
DROP TABLE IF EXISTS `ps_mail`;
CREATE TABLE IF NOT EXISTS `ps_mail` (
  `id_mail` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `recipient` varchar(126) NOT NULL,
  `template` varchar(62) NOT NULL,
  `subject` varchar(254) NOT NULL,
  `id_lang` int(11) unsigned NOT NULL,
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_mail`),
  KEY `recipient` (`recipient`(10))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_mail: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_mail` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_mail` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_manufacturer
DROP TABLE IF EXISTS `ps_manufacturer`;
CREATE TABLE IF NOT EXISTS `ps_manufacturer` (
  `id_manufacturer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_manufacturer`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_manufacturer: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_manufacturer` DISABLE KEYS */;
INSERT INTO `ps_manufacturer` (`id_manufacturer`, `name`, `date_add`, `date_upd`, `active`) VALUES
	(1, 'Fashion Manufacturer', '2015-05-08 14:00:20', '2015-05-08 14:00:20', 1);
/*!40000 ALTER TABLE `ps_manufacturer` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_manufacturer_lang
DROP TABLE IF EXISTS `ps_manufacturer_lang`;
CREATE TABLE IF NOT EXISTS `ps_manufacturer_lang` (
  `id_manufacturer` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `description` text,
  `short_description` text,
  `meta_title` varchar(128) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_manufacturer`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_manufacturer_lang: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_manufacturer_lang` DISABLE KEYS */;
INSERT INTO `ps_manufacturer_lang` (`id_manufacturer`, `id_lang`, `description`, `short_description`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
	(1, 1, '', '', '', '', '');
/*!40000 ALTER TABLE `ps_manufacturer_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_manufacturer_shop
DROP TABLE IF EXISTS `ps_manufacturer_shop`;
CREATE TABLE IF NOT EXISTS `ps_manufacturer_shop` (
  `id_manufacturer` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_manufacturer`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_manufacturer_shop: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_manufacturer_shop` DISABLE KEYS */;
INSERT INTO `ps_manufacturer_shop` (`id_manufacturer`, `id_shop`) VALUES
	(1, 1);
/*!40000 ALTER TABLE `ps_manufacturer_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_memcached_servers
DROP TABLE IF EXISTS `ps_memcached_servers`;
CREATE TABLE IF NOT EXISTS `ps_memcached_servers` (
  `id_memcached_server` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(254) NOT NULL,
  `port` int(11) unsigned NOT NULL,
  `weight` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_memcached_server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_memcached_servers: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_memcached_servers` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_memcached_servers` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_message
DROP TABLE IF EXISTS `ps_message`;
CREATE TABLE IF NOT EXISTS `ps_message` (
  `id_message` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cart` int(10) unsigned DEFAULT NULL,
  `id_customer` int(10) unsigned NOT NULL,
  `id_employee` int(10) unsigned DEFAULT NULL,
  `id_order` int(10) unsigned NOT NULL,
  `message` text NOT NULL,
  `private` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_message`),
  KEY `message_order` (`id_order`),
  KEY `id_cart` (`id_cart`),
  KEY `id_customer` (`id_customer`),
  KEY `id_employee` (`id_employee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_message: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_message` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_message_readed
DROP TABLE IF EXISTS `ps_message_readed`;
CREATE TABLE IF NOT EXISTS `ps_message_readed` (
  `id_message` int(10) unsigned NOT NULL,
  `id_employee` int(10) unsigned NOT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_message`,`id_employee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_message_readed: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_message_readed` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_message_readed` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_meta
DROP TABLE IF EXISTS `ps_meta`;
CREATE TABLE IF NOT EXISTS `ps_meta` (
  `id_meta` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page` varchar(64) NOT NULL,
  `configurable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_meta`),
  UNIQUE KEY `page` (`page`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_meta: ~38 rows (approximately)
/*!40000 ALTER TABLE `ps_meta` DISABLE KEYS */;
INSERT INTO `ps_meta` (`id_meta`, `page`, `configurable`) VALUES
	(1, 'pagenotfound', 1),
	(2, 'best-sales', 1),
	(3, 'contact', 1),
	(4, 'index', 1),
	(5, 'manufacturer', 1),
	(6, 'new-products', 1),
	(7, 'password', 1),
	(8, 'prices-drop', 1),
	(9, 'sitemap', 1),
	(10, 'supplier', 1),
	(11, 'address', 1),
	(12, 'addresses', 1),
	(13, 'authentication', 1),
	(14, 'cart', 1),
	(15, 'discount', 1),
	(16, 'history', 1),
	(17, 'identity', 1),
	(18, 'my-account', 1),
	(19, 'order-follow', 1),
	(20, 'order-slip', 1),
	(21, 'order', 1),
	(22, 'search', 1),
	(23, 'stores', 1),
	(24, 'order-opc', 1),
	(25, 'guest-tracking', 1),
	(26, 'order-confirmation', 1),
	(27, 'product', 0),
	(28, 'category', 0),
	(29, 'cms', 0),
	(30, 'module-cheque-payment', 0),
	(31, 'module-cheque-validation', 0),
	(32, 'module-bankwire-validation', 0),
	(33, 'module-bankwire-payment', 0),
	(34, 'products-comparison', 1),
	(35, 'module-blocknewsletter-verification', 1),
	(36, 'module-blockwishlist-mywishlist', 1),
	(37, 'module-blockwishlist-view', 1),
	(38, 'module-cronjobs-callback', 1);
/*!40000 ALTER TABLE `ps_meta` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_meta_lang
DROP TABLE IF EXISTS `ps_meta_lang`;
CREATE TABLE IF NOT EXISTS `ps_meta_lang` (
  `id_meta` int(10) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_lang` int(10) unsigned NOT NULL,
  `title` varchar(128) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `url_rewrite` varchar(254) NOT NULL,
  PRIMARY KEY (`id_meta`,`id_shop`,`id_lang`),
  KEY `id_shop` (`id_shop`),
  KEY `id_lang` (`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_meta_lang: ~31 rows (approximately)
/*!40000 ALTER TABLE `ps_meta_lang` DISABLE KEYS */;
INSERT INTO `ps_meta_lang` (`id_meta`, `id_shop`, `id_lang`, `title`, `description`, `keywords`, `url_rewrite`) VALUES
	(1, 1, 1, 'Error 404', 'Página no encontrada', 'error, 404, No se ha encontrado', 'pagina-no-ecnontrada'),
	(2, 1, 1, 'Lo más vendido', 'Nuestros productos estrella', 'los más vendidos', 'mas-vendido'),
	(3, 1, 1, 'Contáctanos', 'Utiliza nuestro formulario para ponerte en contacto con nosotros', 'formulario de contacto, e-mail', 'contactanos'),
	(4, 1, 1, '', 'Tienda creada con PrestaShop', 'tienda, prestashop', ''),
	(5, 1, 1, 'Fabricantes', 'Lista de fabricantes', 'fabricantes', 'fabricantes'),
	(6, 1, 1, 'Productos nuevos', 'Nuestros productos nuevos', 'nuevo, productos', 'nuevos-productos'),
	(7, 1, 1, '¿Has olvidado tu contraseña?', 'Introduce la dirección de correo electrónico que utilices para acceder para recibir un mensaje de correo con una nueva contraseña', 'contraseña, has olvidado, e-mail, nuevo, regeneración', 'recuperacion-contraseña'),
	(8, 1, 1, 'Bajamos los precios', 'Nuestros productos especiales', 'promoción, reducción', 'bajamos-precios'),
	(9, 1, 1, 'Mapa del sitio web', '¿Estás perdido? Encuentra lo que buscas', 'plan, sitio', 'mapa-web'),
	(10, 1, 1, 'Proveedores', 'Lista de proveedores', 'proveedores', 'proveedor'),
	(11, 1, 1, 'Dirección', '', '', 'direccion'),
	(12, 1, 1, 'Direcciones', '', '', 'direcciones'),
	(13, 1, 1, 'Iniciar sesión', '', '', 'inicio-sesion'),
	(14, 1, 1, 'Carrito', '', '', 'carrito'),
	(15, 1, 1, 'Descuento', '', '', 'descuento'),
	(16, 1, 1, 'Historial de compra', '', '', 'historial-compra'),
	(17, 1, 1, 'Datos personales', '', '', 'datos-personales'),
	(18, 1, 1, 'Mi cuenta', '', '', 'mi-cuenta'),
	(19, 1, 1, 'Seguimiento del pedido', '', '', 'seguimiento-pedido'),
	(20, 1, 1, 'Albarán', '', '', 'albaran'),
	(21, 1, 1, 'Pedido', '', '', 'pedido'),
	(22, 1, 1, 'Buscar', '', '', 'buscar'),
	(23, 1, 1, 'Tiendas', '', '', 'tiendas'),
	(24, 1, 1, 'Pedido', '', '', 'pedido-rapido'),
	(25, 1, 1, 'Seguimiento para clientes no registrados', '', '', 'seguimiento-cliente-no-registrado'),
	(26, 1, 1, 'Confirmación de pedido', '', '', 'confirmacion-pedido'),
	(34, 1, 1, 'Comparativa de productos', '', '', 'comparativa-productos'),
	(35, 1, 1, '', '', '', ''),
	(36, 1, 1, '', '', '', ''),
	(37, 1, 1, '', '', '', ''),
	(38, 1, 1, '', '', '', '');
/*!40000 ALTER TABLE `ps_meta_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_module
DROP TABLE IF EXISTS `ps_module`;
CREATE TABLE IF NOT EXISTS `ps_module` (
  `id_module` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `version` varchar(8) NOT NULL,
  PRIMARY KEY (`id_module`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_module: ~86 rows (approximately)
/*!40000 ALTER TABLE `ps_module` DISABLE KEYS */;
INSERT INTO `ps_module` (`id_module`, `name`, `active`, `version`) VALUES
	(1, 'socialsharing', 1, '1.3.0'),
	(2, 'blockbanner', 1, '1.3.4'),
	(3, 'bankwire', 1, '1.0.5'),
	(4, 'blockbestsellers', 1, '1.6.2'),
	(5, 'blockcart', 1, '1.5.7'),
	(6, 'blocksocial', 1, '1.1.5'),
	(7, 'blockcategories', 1, '2.8.9'),
	(8, 'blockcurrencies', 1, '0.3.2'),
	(9, 'blockfacebook', 1, '1.3.4'),
	(10, 'blocklanguages', 1, '1.3.3'),
	(11, 'blocklayered', 1, '2.0.7'),
	(12, 'blockcms', 1, '2.0.4'),
	(13, 'blockcmsinfo', 1, '1.5.2'),
	(14, 'blockcontact', 1, '1.3.3'),
	(15, 'blockcontactinfos', 1, '1.1.2'),
	(16, 'blockmanufacturer', 1, '1.2.2'),
	(18, 'blockmyaccountfooter', 1, '1.5.1'),
	(19, 'blocknewproducts', 1, '1.9.6'),
	(20, 'blocknewsletter', 1, '2.1.6'),
	(21, 'blockpaymentlogo', 1, '0.3.3'),
	(22, 'blocksearch', 1, '1.5.3'),
	(23, 'blockspecials', 1, '1.1.6'),
	(24, 'blockstore', 1, '1.2.1'),
	(25, 'blocksupplier', 1, '1.1.1'),
	(26, 'blocktags', 1, '1.2.5'),
	(27, 'blocktopmenu', 1, '2.1.1'),
	(28, 'blockuserinfo', 1, '0.3.1'),
	(29, 'blockviewed', 1, '1.2.3'),
	(30, 'cheque', 1, '2.5.7'),
	(31, 'dashactivity', 1, '0.4.6'),
	(32, 'dashtrends', 1, '0.7.3'),
	(33, 'dashgoals', 1, '0.6.4'),
	(34, 'dashproducts', 1, '0.3.2'),
	(35, 'graphnvd3', 1, '1.4'),
	(36, 'gridhtml', 1, '1.2.2'),
	(37, 'homeslider', 1, '1.4.5'),
	(38, 'homefeatured', 1, '1.6.2'),
	(39, 'productpaymentlogos', 1, '1.3.6'),
	(40, 'pagesnotfound', 1, '1.3.4'),
	(41, 'sekeywords', 1, '1.2.4'),
	(42, 'statsbestcategories', 1, '1.4.1'),
	(43, 'statsbestcustomers', 1, '1.4.1'),
	(44, 'statsbestproducts', 1, '1.4.1'),
	(45, 'statsbestsuppliers', 1, '1.3.1'),
	(46, 'statsbestvouchers', 1, '1.4.1'),
	(47, 'statscarrier', 1, '1.3.1'),
	(48, 'statscatalog', 1, '1.2.3'),
	(49, 'statscheckup', 1, '1.3.1'),
	(50, 'statsdata', 1, '1.4.1'),
	(51, 'statsequipment', 1, '1.2.3'),
	(52, 'statsforecast', 1, '1.3.5'),
	(53, 'statslive', 1, '1.2.2'),
	(54, 'statsnewsletter', 1, '1.3.1'),
	(55, 'statsorigin', 1, '1.3.1'),
	(56, 'statspersonalinfos', 1, '1.3.1'),
	(57, 'statsproduct', 1, '1.3.1'),
	(58, 'statsregistrations', 1, '1.3.1'),
	(59, 'statssales', 1, '1.2.3'),
	(60, 'statssearch', 1, '1.3.2'),
	(61, 'statsstock', 1, '1.4.2'),
	(62, 'statsvisits', 1, '1.5.1'),
	(63, 'themeconfigurator', 1, '1.2.1'),
	(64, 'gamification', 1, '1.10.3'),
	(65, 'blockwishlist', 1, '1.2.3'),
	(66, 'productcomments', 1, '3.4.1'),
	(67, 'sendtoafriend', 1, '1.7.3'),
	(68, 'cronjobs', 1, '1.2.6'),
	(69, 'onboarding', 1, '0.2.0'),
	(70, 'ptsblockmanufacturer', 1, '1.1'),
	(71, 'ptsmegamenu', 1, '2.5'),
	(72, 'ptsstaticcontent', 1, '2.1'),
	(73, 'ptsverticalmenu', 1, '1.4'),
	(74, 'ptsblockproducttabs', 1, '2.0'),
	(75, 'leoblog', 1, '2.0'),
	(76, 'blockleoblogs', 1, '0.9'),
	(77, 'leosliderlayer', 1, '1.4'),
	(78, 'ptsmaplocator', 1, '1.0'),
	(79, 'ptsblockrelatedproducts', 1, '1.1'),
	(80, 'ptsblocksearch', 1, '2.0'),
	(81, 'ptspagebuilder', 1, '5.0'),
	(82, 'ptsthemepanel', 1, '1.6'),
	(83, 'ptsutilproductcarousel', 1, '2.0'),
	(84, 'ptsbttestimonials', 1, '1.0'),
	(85, 'ptsblockreinsurance', 1, '2.1'),
	(86, 'blocklink', 1, '1.5.7'),
	(87, 'blockpermanentlinks', 1, '0.2.1'),
	(88, 'blockmyaccount', 1, '1.3.1');
/*!40000 ALTER TABLE `ps_module` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_module_access
DROP TABLE IF EXISTS `ps_module_access`;
CREATE TABLE IF NOT EXISTS `ps_module_access` (
  `id_profile` int(10) unsigned NOT NULL,
  `id_module` int(10) unsigned NOT NULL,
  `view` tinyint(1) NOT NULL DEFAULT '0',
  `configure` tinyint(1) NOT NULL DEFAULT '0',
  `uninstall` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_profile`,`id_module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_module_access: ~258 rows (approximately)
/*!40000 ALTER TABLE `ps_module_access` DISABLE KEYS */;
INSERT INTO `ps_module_access` (`id_profile`, `id_module`, `view`, `configure`, `uninstall`) VALUES
	(2, 1, 1, 1, 1),
	(2, 2, 1, 1, 1),
	(2, 3, 1, 1, 1),
	(2, 4, 1, 1, 1),
	(2, 5, 1, 1, 1),
	(2, 6, 1, 1, 1),
	(2, 7, 1, 1, 1),
	(2, 8, 1, 1, 1),
	(2, 9, 1, 1, 1),
	(2, 10, 1, 1, 1),
	(2, 11, 1, 1, 1),
	(2, 12, 1, 1, 1),
	(2, 13, 1, 1, 1),
	(2, 14, 1, 1, 1),
	(2, 15, 1, 1, 1),
	(2, 16, 1, 1, 1),
	(2, 18, 1, 1, 1),
	(2, 19, 1, 1, 1),
	(2, 20, 1, 1, 1),
	(2, 21, 1, 1, 1),
	(2, 22, 1, 1, 1),
	(2, 23, 1, 1, 1),
	(2, 24, 1, 1, 1),
	(2, 25, 1, 1, 1),
	(2, 26, 1, 1, 1),
	(2, 27, 1, 1, 1),
	(2, 28, 1, 1, 1),
	(2, 29, 1, 1, 1),
	(2, 30, 1, 1, 1),
	(2, 31, 1, 1, 1),
	(2, 32, 1, 1, 1),
	(2, 33, 1, 1, 1),
	(2, 34, 1, 1, 1),
	(2, 35, 1, 1, 1),
	(2, 36, 1, 1, 1),
	(2, 37, 1, 1, 1),
	(2, 38, 1, 1, 1),
	(2, 39, 1, 1, 1),
	(2, 40, 1, 1, 1),
	(2, 41, 1, 1, 1),
	(2, 42, 1, 1, 1),
	(2, 43, 1, 1, 1),
	(2, 44, 1, 1, 1),
	(2, 45, 1, 1, 1),
	(2, 46, 1, 1, 1),
	(2, 47, 1, 1, 1),
	(2, 48, 1, 1, 1),
	(2, 49, 1, 1, 1),
	(2, 50, 1, 1, 1),
	(2, 51, 1, 1, 1),
	(2, 52, 1, 1, 1),
	(2, 53, 1, 1, 1),
	(2, 54, 1, 1, 1),
	(2, 55, 1, 1, 1),
	(2, 56, 1, 1, 1),
	(2, 57, 1, 1, 1),
	(2, 58, 1, 1, 1),
	(2, 59, 1, 1, 1),
	(2, 60, 1, 1, 1),
	(2, 61, 1, 1, 1),
	(2, 62, 1, 1, 1),
	(2, 63, 1, 1, 1),
	(2, 64, 1, 1, 1),
	(2, 65, 1, 1, 1),
	(2, 66, 1, 1, 1),
	(2, 67, 1, 1, 1),
	(2, 68, 1, 1, 1),
	(2, 69, 1, 1, 1),
	(2, 70, 1, 1, 1),
	(2, 71, 1, 1, 1),
	(2, 72, 1, 1, 1),
	(2, 73, 1, 1, 1),
	(2, 74, 1, 1, 1),
	(2, 75, 1, 1, 1),
	(2, 76, 1, 1, 1),
	(2, 77, 1, 1, 1),
	(2, 78, 1, 1, 1),
	(2, 79, 1, 1, 1),
	(2, 80, 1, 1, 1),
	(2, 81, 1, 1, 1),
	(2, 82, 1, 1, 1),
	(2, 83, 1, 1, 1),
	(2, 84, 1, 1, 1),
	(2, 85, 1, 1, 1),
	(2, 86, 1, 1, 1),
	(2, 87, 1, 1, 1),
	(2, 88, 1, 1, 1),
	(3, 1, 1, 0, 0),
	(3, 2, 1, 0, 0),
	(3, 3, 1, 0, 0),
	(3, 4, 1, 0, 0),
	(3, 5, 1, 0, 0),
	(3, 6, 1, 0, 0),
	(3, 7, 1, 0, 0),
	(3, 8, 1, 0, 0),
	(3, 9, 1, 0, 0),
	(3, 10, 1, 0, 0),
	(3, 11, 1, 0, 0),
	(3, 12, 1, 0, 0),
	(3, 13, 1, 0, 0),
	(3, 14, 1, 0, 0),
	(3, 15, 1, 0, 0),
	(3, 16, 1, 0, 0),
	(3, 18, 1, 0, 0),
	(3, 19, 1, 0, 0),
	(3, 20, 1, 0, 0),
	(3, 21, 1, 0, 0),
	(3, 22, 1, 0, 0),
	(3, 23, 1, 0, 0),
	(3, 24, 1, 0, 0),
	(3, 25, 1, 0, 0),
	(3, 26, 1, 0, 0),
	(3, 27, 1, 0, 0),
	(3, 28, 1, 0, 0),
	(3, 29, 1, 0, 0),
	(3, 30, 1, 0, 0),
	(3, 31, 1, 0, 0),
	(3, 32, 1, 0, 0),
	(3, 33, 1, 0, 0),
	(3, 34, 1, 0, 0),
	(3, 35, 1, 0, 0),
	(3, 36, 1, 0, 0),
	(3, 37, 1, 0, 0),
	(3, 38, 1, 0, 0),
	(3, 39, 1, 0, 0),
	(3, 40, 1, 0, 0),
	(3, 41, 1, 0, 0),
	(3, 42, 1, 0, 0),
	(3, 43, 1, 0, 0),
	(3, 44, 1, 0, 0),
	(3, 45, 1, 0, 0),
	(3, 46, 1, 0, 0),
	(3, 47, 1, 0, 0),
	(3, 48, 1, 0, 0),
	(3, 49, 1, 0, 0),
	(3, 50, 1, 0, 0),
	(3, 51, 1, 0, 0),
	(3, 52, 1, 0, 0),
	(3, 53, 1, 0, 0),
	(3, 54, 1, 0, 0),
	(3, 55, 1, 0, 0),
	(3, 56, 1, 0, 0),
	(3, 57, 1, 0, 0),
	(3, 58, 1, 0, 0),
	(3, 59, 1, 0, 0),
	(3, 60, 1, 0, 0),
	(3, 61, 1, 0, 0),
	(3, 62, 1, 0, 0),
	(3, 63, 1, 0, 0),
	(3, 64, 1, 0, 0),
	(3, 65, 1, 0, 0),
	(3, 66, 1, 0, 0),
	(3, 67, 1, 0, 0),
	(3, 68, 1, 0, 0),
	(3, 69, 1, 0, 0),
	(3, 70, 1, 0, 0),
	(3, 71, 1, 0, 0),
	(3, 72, 1, 0, 0),
	(3, 73, 1, 0, 0),
	(3, 74, 1, 0, 0),
	(3, 75, 1, 0, 0),
	(3, 76, 1, 0, 0),
	(3, 77, 1, 0, 0),
	(3, 78, 1, 0, 0),
	(3, 79, 1, 0, 0),
	(3, 80, 1, 0, 0),
	(3, 81, 1, 0, 0),
	(3, 82, 1, 0, 0),
	(3, 83, 1, 0, 0),
	(3, 84, 1, 0, 0),
	(3, 85, 1, 0, 0),
	(3, 86, 1, 0, 0),
	(3, 87, 1, 0, 0),
	(3, 88, 1, 0, 0),
	(4, 1, 1, 1, 1),
	(4, 2, 1, 1, 1),
	(4, 3, 1, 1, 1),
	(4, 4, 1, 1, 1),
	(4, 5, 1, 1, 1),
	(4, 6, 1, 1, 1),
	(4, 7, 1, 1, 1),
	(4, 8, 1, 1, 1),
	(4, 9, 1, 1, 1),
	(4, 10, 1, 1, 1),
	(4, 11, 1, 1, 1),
	(4, 12, 1, 1, 1),
	(4, 13, 1, 1, 1),
	(4, 14, 1, 1, 1),
	(4, 15, 1, 1, 1),
	(4, 16, 1, 1, 1),
	(4, 18, 1, 1, 1),
	(4, 19, 1, 1, 1),
	(4, 20, 1, 1, 1),
	(4, 21, 1, 1, 1),
	(4, 22, 1, 1, 1),
	(4, 23, 1, 1, 1),
	(4, 24, 1, 1, 1),
	(4, 25, 1, 1, 1),
	(4, 26, 1, 1, 1),
	(4, 27, 1, 1, 1),
	(4, 28, 1, 1, 1),
	(4, 29, 1, 1, 1),
	(4, 30, 1, 1, 1),
	(4, 31, 1, 1, 1),
	(4, 32, 1, 1, 1),
	(4, 33, 1, 1, 1),
	(4, 34, 1, 1, 1),
	(4, 35, 1, 1, 1),
	(4, 36, 1, 1, 1),
	(4, 37, 1, 1, 1),
	(4, 38, 1, 1, 1),
	(4, 39, 1, 1, 1),
	(4, 40, 1, 1, 1),
	(4, 41, 1, 1, 1),
	(4, 42, 1, 1, 1),
	(4, 43, 1, 1, 1),
	(4, 44, 1, 1, 1),
	(4, 45, 1, 1, 1),
	(4, 46, 1, 1, 1),
	(4, 47, 1, 1, 1),
	(4, 48, 1, 1, 1),
	(4, 49, 1, 1, 1),
	(4, 50, 1, 1, 1),
	(4, 51, 1, 1, 1),
	(4, 52, 1, 1, 1),
	(4, 53, 1, 1, 1),
	(4, 54, 1, 1, 1),
	(4, 55, 1, 1, 1),
	(4, 56, 1, 1, 1),
	(4, 57, 1, 1, 1),
	(4, 58, 1, 1, 1),
	(4, 59, 1, 1, 1),
	(4, 60, 1, 1, 1),
	(4, 61, 1, 1, 1),
	(4, 62, 1, 1, 1),
	(4, 63, 1, 1, 1),
	(4, 64, 1, 1, 1),
	(4, 65, 1, 1, 1),
	(4, 66, 1, 1, 1),
	(4, 67, 1, 1, 1),
	(4, 68, 1, 1, 1),
	(4, 69, 1, 1, 1),
	(4, 70, 1, 1, 1),
	(4, 71, 1, 1, 1),
	(4, 72, 1, 1, 1),
	(4, 73, 1, 1, 1),
	(4, 74, 1, 1, 1),
	(4, 75, 1, 1, 1),
	(4, 76, 1, 1, 1),
	(4, 77, 1, 1, 1),
	(4, 78, 1, 1, 1),
	(4, 79, 1, 1, 1),
	(4, 80, 1, 1, 1),
	(4, 81, 1, 1, 1),
	(4, 82, 1, 1, 1),
	(4, 83, 1, 1, 1),
	(4, 84, 1, 1, 1),
	(4, 85, 1, 1, 1),
	(4, 86, 1, 1, 1),
	(4, 87, 1, 1, 1),
	(4, 88, 1, 1, 1);
/*!40000 ALTER TABLE `ps_module_access` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_module_country
DROP TABLE IF EXISTS `ps_module_country`;
CREATE TABLE IF NOT EXISTS `ps_module_country` (
  `id_module` int(10) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_country` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_module`,`id_shop`,`id_country`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_module_country: ~2 rows (approximately)
/*!40000 ALTER TABLE `ps_module_country` DISABLE KEYS */;
INSERT INTO `ps_module_country` (`id_module`, `id_shop`, `id_country`) VALUES
	(3, 1, 68),
	(30, 1, 68);
/*!40000 ALTER TABLE `ps_module_country` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_module_currency
DROP TABLE IF EXISTS `ps_module_currency`;
CREATE TABLE IF NOT EXISTS `ps_module_currency` (
  `id_module` int(10) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_currency` int(11) NOT NULL,
  PRIMARY KEY (`id_module`,`id_shop`,`id_currency`),
  KEY `id_module` (`id_module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_module_currency: ~2 rows (approximately)
/*!40000 ALTER TABLE `ps_module_currency` DISABLE KEYS */;
INSERT INTO `ps_module_currency` (`id_module`, `id_shop`, `id_currency`) VALUES
	(3, 1, 1),
	(30, 1, 1);
/*!40000 ALTER TABLE `ps_module_currency` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_module_group
DROP TABLE IF EXISTS `ps_module_group`;
CREATE TABLE IF NOT EXISTS `ps_module_group` (
  `id_module` int(10) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_group` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_module`,`id_shop`,`id_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_module_group: ~258 rows (approximately)
/*!40000 ALTER TABLE `ps_module_group` DISABLE KEYS */;
INSERT INTO `ps_module_group` (`id_module`, `id_shop`, `id_group`) VALUES
	(1, 1, 1),
	(1, 1, 2),
	(1, 1, 3),
	(2, 1, 1),
	(2, 1, 2),
	(2, 1, 3),
	(3, 1, 1),
	(3, 1, 2),
	(3, 1, 3),
	(4, 1, 1),
	(4, 1, 2),
	(4, 1, 3),
	(5, 1, 1),
	(5, 1, 2),
	(5, 1, 3),
	(6, 1, 1),
	(6, 1, 2),
	(6, 1, 3),
	(7, 1, 1),
	(7, 1, 2),
	(7, 1, 3),
	(8, 1, 1),
	(8, 1, 2),
	(8, 1, 3),
	(9, 1, 1),
	(9, 1, 2),
	(9, 1, 3),
	(10, 1, 1),
	(10, 1, 2),
	(10, 1, 3),
	(11, 1, 1),
	(11, 1, 2),
	(11, 1, 3),
	(12, 1, 1),
	(12, 1, 2),
	(12, 1, 3),
	(13, 1, 1),
	(13, 1, 2),
	(13, 1, 3),
	(14, 1, 1),
	(14, 1, 2),
	(14, 1, 3),
	(15, 1, 1),
	(15, 1, 2),
	(15, 1, 3),
	(16, 1, 1),
	(16, 1, 2),
	(16, 1, 3),
	(18, 1, 1),
	(18, 1, 2),
	(18, 1, 3),
	(19, 1, 1),
	(19, 1, 2),
	(19, 1, 3),
	(20, 1, 1),
	(20, 1, 2),
	(20, 1, 3),
	(21, 1, 1),
	(21, 1, 2),
	(21, 1, 3),
	(22, 1, 1),
	(22, 1, 2),
	(22, 1, 3),
	(23, 1, 1),
	(23, 1, 2),
	(23, 1, 3),
	(24, 1, 1),
	(24, 1, 2),
	(24, 1, 3),
	(25, 1, 1),
	(25, 1, 2),
	(25, 1, 3),
	(26, 1, 1),
	(26, 1, 2),
	(26, 1, 3),
	(27, 1, 1),
	(27, 1, 2),
	(27, 1, 3),
	(28, 1, 1),
	(28, 1, 2),
	(28, 1, 3),
	(29, 1, 1),
	(29, 1, 2),
	(29, 1, 3),
	(30, 1, 1),
	(30, 1, 2),
	(30, 1, 3),
	(31, 1, 1),
	(31, 1, 2),
	(31, 1, 3),
	(32, 1, 1),
	(32, 1, 2),
	(32, 1, 3),
	(33, 1, 1),
	(33, 1, 2),
	(33, 1, 3),
	(34, 1, 1),
	(34, 1, 2),
	(34, 1, 3),
	(35, 1, 1),
	(35, 1, 2),
	(35, 1, 3),
	(36, 1, 1),
	(36, 1, 2),
	(36, 1, 3),
	(37, 1, 1),
	(37, 1, 2),
	(37, 1, 3),
	(38, 1, 1),
	(38, 1, 2),
	(38, 1, 3),
	(39, 1, 1),
	(39, 1, 2),
	(39, 1, 3),
	(40, 1, 1),
	(40, 1, 2),
	(40, 1, 3),
	(41, 1, 1),
	(41, 1, 2),
	(41, 1, 3),
	(42, 1, 1),
	(42, 1, 2),
	(42, 1, 3),
	(43, 1, 1),
	(43, 1, 2),
	(43, 1, 3),
	(44, 1, 1),
	(44, 1, 2),
	(44, 1, 3),
	(45, 1, 1),
	(45, 1, 2),
	(45, 1, 3),
	(46, 1, 1),
	(46, 1, 2),
	(46, 1, 3),
	(47, 1, 1),
	(47, 1, 2),
	(47, 1, 3),
	(48, 1, 1),
	(48, 1, 2),
	(48, 1, 3),
	(49, 1, 1),
	(49, 1, 2),
	(49, 1, 3),
	(50, 1, 1),
	(50, 1, 2),
	(50, 1, 3),
	(51, 1, 1),
	(51, 1, 2),
	(51, 1, 3),
	(52, 1, 1),
	(52, 1, 2),
	(52, 1, 3),
	(53, 1, 1),
	(53, 1, 2),
	(53, 1, 3),
	(54, 1, 1),
	(54, 1, 2),
	(54, 1, 3),
	(55, 1, 1),
	(55, 1, 2),
	(55, 1, 3),
	(56, 1, 1),
	(56, 1, 2),
	(56, 1, 3),
	(57, 1, 1),
	(57, 1, 2),
	(57, 1, 3),
	(58, 1, 1),
	(58, 1, 2),
	(58, 1, 3),
	(59, 1, 1),
	(59, 1, 2),
	(59, 1, 3),
	(60, 1, 1),
	(60, 1, 2),
	(60, 1, 3),
	(61, 1, 1),
	(61, 1, 2),
	(61, 1, 3),
	(62, 1, 1),
	(62, 1, 2),
	(62, 1, 3),
	(63, 1, 1),
	(63, 1, 2),
	(63, 1, 3),
	(64, 1, 1),
	(64, 1, 2),
	(64, 1, 3),
	(65, 1, 1),
	(65, 1, 2),
	(65, 1, 3),
	(66, 1, 1),
	(66, 1, 2),
	(66, 1, 3),
	(67, 1, 1),
	(67, 1, 2),
	(67, 1, 3),
	(68, 1, 1),
	(68, 1, 2),
	(68, 1, 3),
	(69, 1, 1),
	(69, 1, 2),
	(69, 1, 3),
	(70, 1, 1),
	(70, 1, 2),
	(70, 1, 3),
	(71, 1, 1),
	(71, 1, 2),
	(71, 1, 3),
	(72, 1, 1),
	(72, 1, 2),
	(72, 1, 3),
	(73, 1, 1),
	(73, 1, 2),
	(73, 1, 3),
	(74, 1, 1),
	(74, 1, 2),
	(74, 1, 3),
	(75, 1, 1),
	(75, 1, 2),
	(75, 1, 3),
	(76, 1, 1),
	(76, 1, 2),
	(76, 1, 3),
	(77, 1, 1),
	(77, 1, 2),
	(77, 1, 3),
	(78, 1, 1),
	(78, 1, 2),
	(78, 1, 3),
	(79, 1, 1),
	(79, 1, 2),
	(79, 1, 3),
	(80, 1, 1),
	(80, 1, 2),
	(80, 1, 3),
	(81, 1, 1),
	(81, 1, 2),
	(81, 1, 3),
	(82, 1, 1),
	(82, 1, 2),
	(82, 1, 3),
	(83, 1, 1),
	(83, 1, 2),
	(83, 1, 3),
	(84, 1, 1),
	(84, 1, 2),
	(84, 1, 3),
	(85, 1, 1),
	(85, 1, 2),
	(85, 1, 3),
	(86, 1, 1),
	(86, 1, 2),
	(86, 1, 3),
	(87, 1, 1),
	(87, 1, 2),
	(87, 1, 3),
	(88, 1, 1),
	(88, 1, 2),
	(88, 1, 3);
/*!40000 ALTER TABLE `ps_module_group` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_module_preference
DROP TABLE IF EXISTS `ps_module_preference`;
CREATE TABLE IF NOT EXISTS `ps_module_preference` (
  `id_module_preference` int(11) NOT NULL AUTO_INCREMENT,
  `id_employee` int(11) NOT NULL,
  `module` varchar(255) NOT NULL,
  `interest` tinyint(1) DEFAULT NULL,
  `favorite` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_module_preference`),
  UNIQUE KEY `employee_module` (`id_employee`,`module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_module_preference: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_module_preference` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_module_preference` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_module_shop
DROP TABLE IF EXISTS `ps_module_shop`;
CREATE TABLE IF NOT EXISTS `ps_module_shop` (
  `id_module` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  `enable_device` tinyint(1) NOT NULL DEFAULT '7',
  PRIMARY KEY (`id_module`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_module_shop: ~83 rows (approximately)
/*!40000 ALTER TABLE `ps_module_shop` DISABLE KEYS */;
INSERT INTO `ps_module_shop` (`id_module`, `id_shop`, `enable_device`) VALUES
	(1, 1, 7),
	(3, 1, 7),
	(4, 1, 7),
	(5, 1, 7),
	(6, 1, 7),
	(7, 1, 7),
	(8, 1, 7),
	(9, 1, 7),
	(10, 1, 7),
	(11, 1, 7),
	(12, 1, 7),
	(14, 1, 7),
	(15, 1, 7),
	(16, 1, 7),
	(18, 1, 7),
	(19, 1, 7),
	(20, 1, 7),
	(21, 1, 7),
	(22, 1, 7),
	(23, 1, 7),
	(24, 1, 7),
	(25, 1, 7),
	(27, 1, 7),
	(28, 1, 7),
	(29, 1, 7),
	(30, 1, 7),
	(31, 1, 7),
	(32, 1, 7),
	(33, 1, 7),
	(34, 1, 7),
	(35, 1, 7),
	(36, 1, 7),
	(37, 1, 3),
	(38, 1, 7),
	(40, 1, 7),
	(41, 1, 7),
	(42, 1, 7),
	(43, 1, 7),
	(44, 1, 7),
	(45, 1, 7),
	(46, 1, 7),
	(47, 1, 7),
	(48, 1, 7),
	(49, 1, 7),
	(50, 1, 7),
	(51, 1, 7),
	(52, 1, 7),
	(53, 1, 7),
	(54, 1, 7),
	(55, 1, 7),
	(56, 1, 7),
	(57, 1, 7),
	(58, 1, 7),
	(59, 1, 7),
	(60, 1, 7),
	(61, 1, 7),
	(62, 1, 7),
	(63, 1, 7),
	(64, 1, 7),
	(65, 1, 7),
	(66, 1, 7),
	(67, 1, 7),
	(68, 1, 7),
	(69, 1, 7),
	(70, 1, 7),
	(71, 1, 7),
	(72, 1, 7),
	(73, 1, 7),
	(74, 1, 7),
	(75, 1, 7),
	(76, 1, 7),
	(77, 1, 7),
	(78, 1, 7),
	(79, 1, 7),
	(80, 1, 7),
	(81, 1, 7),
	(82, 1, 7),
	(83, 1, 7),
	(84, 1, 7),
	(85, 1, 1),
	(86, 1, 7),
	(87, 1, 7),
	(88, 1, 7);
/*!40000 ALTER TABLE `ps_module_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_newsletter
DROP TABLE IF EXISTS `ps_newsletter`;
CREATE TABLE IF NOT EXISTS `ps_newsletter` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL DEFAULT '1',
  `id_shop_group` int(10) unsigned NOT NULL DEFAULT '1',
  `email` varchar(255) NOT NULL,
  `newsletter_date_add` datetime DEFAULT NULL,
  `ip_registration_newsletter` varchar(15) NOT NULL,
  `http_referer` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_newsletter: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_newsletter` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_newsletter` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_operating_system
DROP TABLE IF EXISTS `ps_operating_system`;
CREATE TABLE IF NOT EXISTS `ps_operating_system` (
  `id_operating_system` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id_operating_system`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_operating_system: ~7 rows (approximately)
/*!40000 ALTER TABLE `ps_operating_system` DISABLE KEYS */;
INSERT INTO `ps_operating_system` (`id_operating_system`, `name`) VALUES
	(1, 'Windows XP'),
	(2, 'Windows Vista'),
	(3, 'Windows 7'),
	(4, 'Windows 8'),
	(5, 'MacOsX'),
	(6, 'Linux'),
	(7, 'Android');
/*!40000 ALTER TABLE `ps_operating_system` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_orders
DROP TABLE IF EXISTS `ps_orders`;
CREATE TABLE IF NOT EXISTS `ps_orders` (
  `id_order` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reference` varchar(9) DEFAULT NULL,
  `id_shop_group` int(11) unsigned NOT NULL DEFAULT '1',
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_carrier` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `id_customer` int(10) unsigned NOT NULL,
  `id_cart` int(10) unsigned NOT NULL,
  `id_currency` int(10) unsigned NOT NULL,
  `id_address_delivery` int(10) unsigned NOT NULL,
  `id_address_invoice` int(10) unsigned NOT NULL,
  `current_state` int(10) unsigned NOT NULL,
  `secure_key` varchar(32) NOT NULL DEFAULT '-1',
  `payment` varchar(255) NOT NULL,
  `conversion_rate` decimal(13,6) NOT NULL DEFAULT '1.000000',
  `module` varchar(255) DEFAULT NULL,
  `recyclable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `gift` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `gift_message` text,
  `mobile_theme` tinyint(1) NOT NULL DEFAULT '0',
  `shipping_number` varchar(64) DEFAULT NULL,
  `total_discounts` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_discounts_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_discounts_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_paid` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_paid_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_paid_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_paid_real` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_products` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_products_wt` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_shipping` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_shipping_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_shipping_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `carrier_tax_rate` decimal(10,3) NOT NULL DEFAULT '0.000',
  `total_wrapping` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_wrapping_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_wrapping_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `round_mode` tinyint(1) NOT NULL DEFAULT '2',
  `invoice_number` int(10) unsigned NOT NULL DEFAULT '0',
  `delivery_number` int(10) unsigned NOT NULL DEFAULT '0',
  `invoice_date` datetime NOT NULL,
  `delivery_date` datetime NOT NULL,
  `valid` int(1) unsigned NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_order`),
  KEY `reference` (`reference`),
  KEY `id_customer` (`id_customer`),
  KEY `id_cart` (`id_cart`),
  KEY `invoice_number` (`invoice_number`),
  KEY `id_carrier` (`id_carrier`),
  KEY `id_lang` (`id_lang`),
  KEY `id_currency` (`id_currency`),
  KEY `id_address_delivery` (`id_address_delivery`),
  KEY `id_address_invoice` (`id_address_invoice`),
  KEY `id_shop_group` (`id_shop_group`),
  KEY `current_state` (`current_state`),
  KEY `id_shop` (`id_shop`),
  KEY `date_add` (`date_add`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_orders: ~5 rows (approximately)
/*!40000 ALTER TABLE `ps_orders` DISABLE KEYS */;
INSERT INTO `ps_orders` (`id_order`, `reference`, `id_shop_group`, `id_shop`, `id_carrier`, `id_lang`, `id_customer`, `id_cart`, `id_currency`, `id_address_delivery`, `id_address_invoice`, `current_state`, `secure_key`, `payment`, `conversion_rate`, `module`, `recyclable`, `gift`, `gift_message`, `mobile_theme`, `shipping_number`, `total_discounts`, `total_discounts_tax_incl`, `total_discounts_tax_excl`, `total_paid`, `total_paid_tax_incl`, `total_paid_tax_excl`, `total_paid_real`, `total_products`, `total_products_wt`, `total_shipping`, `total_shipping_tax_incl`, `total_shipping_tax_excl`, `carrier_tax_rate`, `total_wrapping`, `total_wrapping_tax_incl`, `total_wrapping_tax_excl`, `round_mode`, `invoice_number`, `delivery_number`, `invoice_date`, `delivery_date`, `valid`, `date_add`, `date_upd`) VALUES
	(1, 'XKBKNABJK', 1, 1, 2, 1, 1, 1, 1, 4, 4, 6, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 'Payment by check', 1.000000, 'cheque', 0, 0, '', 0, '', 0.000000, 0.000000, 0.000000, 55.000000, 55.000000, 55.000000, 0.000000, 53.000000, 53.000000, 2.000000, 2.000000, 2.000000, 0.000, 0.000000, 0.000000, 0.000000, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2015-05-08 14:00:23', '2015-05-08 14:00:23'),
	(2, 'OHSATSERP', 1, 1, 2, 1, 1, 2, 1, 4, 4, 1, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 'Payment by check', 1.000000, 'cheque', 0, 0, '', 0, '', 0.000000, 0.000000, 0.000000, 75.900000, 75.900000, 75.900000, 0.000000, 73.900000, 73.900000, 2.000000, 2.000000, 2.000000, 0.000, 0.000000, 0.000000, 0.000000, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2015-05-08 14:00:23', '2015-05-08 14:00:23'),
	(3, 'UOYEVOLI', 1, 1, 2, 1, 1, 3, 1, 4, 4, 8, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 'Payment by check', 1.000000, 'cheque', 0, 0, '', 0, '', 0.000000, 0.000000, 0.000000, 76.010000, 76.010000, 76.010000, 0.000000, 74.010000, 74.010000, 2.000000, 2.000000, 2.000000, 0.000, 0.000000, 0.000000, 0.000000, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2015-05-08 14:00:23', '2015-05-08 14:00:23'),
	(4, 'FFATNOMMJ', 1, 1, 2, 1, 1, 4, 1, 4, 4, 1, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 'Payment by check', 1.000000, 'cheque', 0, 0, '', 0, '', 0.000000, 0.000000, 0.000000, 89.890000, 89.890000, 89.890000, 0.000000, 87.890000, 87.890000, 2.000000, 2.000000, 2.000000, 0.000, 0.000000, 0.000000, 0.000000, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2015-05-08 14:00:23', '2015-05-08 14:00:23'),
	(5, 'KHWLILZLL', 1, 1, 2, 1, 1, 5, 1, 4, 4, 10, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 'Bank wire', 1.000000, 'bankwire', 0, 0, '', 0, '', 0.000000, 0.000000, 0.000000, 71.510000, 71.510000, 71.510000, 0.000000, 69.510000, 69.510000, 2.000000, 2.000000, 2.000000, 0.000, 0.000000, 0.000000, 0.000000, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2015-05-08 14:00:23', '2015-05-08 14:00:23');
/*!40000 ALTER TABLE `ps_orders` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_order_carrier
DROP TABLE IF EXISTS `ps_order_carrier`;
CREATE TABLE IF NOT EXISTS `ps_order_carrier` (
  `id_order_carrier` int(11) NOT NULL AUTO_INCREMENT,
  `id_order` int(11) unsigned NOT NULL,
  `id_carrier` int(11) unsigned NOT NULL,
  `id_order_invoice` int(11) unsigned DEFAULT NULL,
  `weight` decimal(20,6) DEFAULT NULL,
  `shipping_cost_tax_excl` decimal(20,6) DEFAULT NULL,
  `shipping_cost_tax_incl` decimal(20,6) DEFAULT NULL,
  `tracking_number` varchar(64) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_order_carrier`),
  KEY `id_order` (`id_order`),
  KEY `id_carrier` (`id_carrier`),
  KEY `id_order_invoice` (`id_order_invoice`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_order_carrier: ~5 rows (approximately)
/*!40000 ALTER TABLE `ps_order_carrier` DISABLE KEYS */;
INSERT INTO `ps_order_carrier` (`id_order_carrier`, `id_order`, `id_carrier`, `id_order_invoice`, `weight`, `shipping_cost_tax_excl`, `shipping_cost_tax_incl`, `tracking_number`, `date_add`) VALUES
	(1, 1, 2, 0, 0.000000, 2.000000, 2.000000, '', '2015-05-08 14:00:23'),
	(2, 2, 2, 0, 0.000000, 2.000000, 2.000000, '', '2015-05-08 14:00:23'),
	(3, 3, 2, 0, 0.000000, 2.000000, 2.000000, '', '2015-05-08 14:00:23'),
	(4, 4, 2, 0, 0.000000, 2.000000, 2.000000, '', '2015-05-08 14:00:23'),
	(5, 5, 2, 0, 0.000000, 2.000000, 2.000000, '', '2015-05-08 14:00:23');
/*!40000 ALTER TABLE `ps_order_carrier` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_order_cart_rule
DROP TABLE IF EXISTS `ps_order_cart_rule`;
CREATE TABLE IF NOT EXISTS `ps_order_cart_rule` (
  `id_order_cart_rule` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_order` int(10) unsigned NOT NULL,
  `id_cart_rule` int(10) unsigned NOT NULL,
  `id_order_invoice` int(10) unsigned DEFAULT '0',
  `name` varchar(254) NOT NULL,
  `value` decimal(17,2) NOT NULL DEFAULT '0.00',
  `value_tax_excl` decimal(17,2) NOT NULL DEFAULT '0.00',
  `free_shipping` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_order_cart_rule`),
  KEY `id_order` (`id_order`),
  KEY `id_cart_rule` (`id_cart_rule`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_order_cart_rule: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_order_cart_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_order_cart_rule` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_order_detail
DROP TABLE IF EXISTS `ps_order_detail`;
CREATE TABLE IF NOT EXISTS `ps_order_detail` (
  `id_order_detail` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_order` int(10) unsigned NOT NULL,
  `id_order_invoice` int(11) DEFAULT NULL,
  `id_warehouse` int(10) unsigned DEFAULT '0',
  `id_shop` int(11) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `product_attribute_id` int(10) unsigned DEFAULT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_quantity` int(10) unsigned NOT NULL DEFAULT '0',
  `product_quantity_in_stock` int(10) NOT NULL DEFAULT '0',
  `product_quantity_refunded` int(10) unsigned NOT NULL DEFAULT '0',
  `product_quantity_return` int(10) unsigned NOT NULL DEFAULT '0',
  `product_quantity_reinjected` int(10) unsigned NOT NULL DEFAULT '0',
  `product_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `reduction_percent` decimal(10,2) NOT NULL DEFAULT '0.00',
  `reduction_amount` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `reduction_amount_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `reduction_amount_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `group_reduction` decimal(10,2) NOT NULL DEFAULT '0.00',
  `product_quantity_discount` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `product_ean13` varchar(13) DEFAULT NULL,
  `product_upc` varchar(12) DEFAULT NULL,
  `product_reference` varchar(32) DEFAULT NULL,
  `product_supplier_reference` varchar(32) DEFAULT NULL,
  `product_weight` decimal(20,6) NOT NULL,
  `id_tax_rules_group` int(11) unsigned DEFAULT '0',
  `tax_computation_method` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `tax_name` varchar(16) NOT NULL,
  `tax_rate` decimal(10,3) NOT NULL DEFAULT '0.000',
  `ecotax` decimal(21,6) NOT NULL DEFAULT '0.000000',
  `ecotax_tax_rate` decimal(5,3) NOT NULL DEFAULT '0.000',
  `discount_quantity_applied` tinyint(1) NOT NULL DEFAULT '0',
  `download_hash` varchar(255) DEFAULT NULL,
  `download_nb` int(10) unsigned DEFAULT '0',
  `download_deadline` datetime DEFAULT NULL,
  `total_price_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_price_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `unit_price_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `unit_price_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_shipping_price_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_shipping_price_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `purchase_supplier_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `original_product_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  PRIMARY KEY (`id_order_detail`),
  KEY `order_detail_order` (`id_order`),
  KEY `product_id` (`product_id`),
  KEY `product_attribute_id` (`product_attribute_id`),
  KEY `id_tax_rules_group` (`id_tax_rules_group`),
  KEY `id_order_id_order_detail` (`id_order`,`id_order_detail`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_order_detail: ~15 rows (approximately)
/*!40000 ALTER TABLE `ps_order_detail` DISABLE KEYS */;
INSERT INTO `ps_order_detail` (`id_order_detail`, `id_order`, `id_order_invoice`, `id_warehouse`, `id_shop`, `product_id`, `product_attribute_id`, `product_name`, `product_quantity`, `product_quantity_in_stock`, `product_quantity_refunded`, `product_quantity_return`, `product_quantity_reinjected`, `product_price`, `reduction_percent`, `reduction_amount`, `reduction_amount_tax_incl`, `reduction_amount_tax_excl`, `group_reduction`, `product_quantity_discount`, `product_ean13`, `product_upc`, `product_reference`, `product_supplier_reference`, `product_weight`, `id_tax_rules_group`, `tax_computation_method`, `tax_name`, `tax_rate`, `ecotax`, `ecotax_tax_rate`, `discount_quantity_applied`, `download_hash`, `download_nb`, `download_deadline`, `total_price_tax_incl`, `total_price_tax_excl`, `unit_price_tax_incl`, `unit_price_tax_excl`, `total_shipping_price_tax_incl`, `total_shipping_price_tax_excl`, `purchase_supplier_price`, `original_product_price`) VALUES
	(1, 1, 0, 0, 1, 2, 10, 'Blouse - Color : White, Size : M', 1, 1, 0, 0, 0, 26.999852, 0.00, 0.000000, 0.000000, 0.000000, 0.00, 0.000000, '', '', 'demo_2', '', 0.000000, 0, 0, '', 0.000, 0.000000, 0.000, 0, '', 0, '0000-00-00 00:00:00', 27.000000, 27.000000, 27.000000, 27.000000, 0.000000, 0.000000, 0.000000, 26.999852),
	(2, 1, 0, 0, 1, 3, 13, 'Printed Dress - Color : Orange, Size : S', 1, 1, 0, 0, 0, 25.999852, 0.00, 0.000000, 0.000000, 0.000000, 0.00, 0.000000, '', '', 'demo_3', '', 0.000000, 0, 0, '', 0.000, 0.000000, 0.000, 0, '', 0, '0000-00-00 00:00:00', 26.000000, 26.000000, 26.000000, 26.000000, 0.000000, 0.000000, 0.000000, 25.999852),
	(3, 2, 0, 0, 1, 2, 10, 'Blouse - Color : White, Size : M', 1, 1, 0, 0, 0, 26.999852, 0.00, 0.000000, 0.000000, 0.000000, 0.00, 0.000000, '', '', 'demo_2', '', 0.000000, 0, 0, '', 0.000, 0.000000, 0.000, 0, '', 0, '0000-00-00 00:00:00', 27.000000, 27.000000, 27.000000, 27.000000, 0.000000, 0.000000, 0.000000, 26.999852),
	(4, 2, 0, 0, 1, 6, 32, 'Printed Summer Dress - Color : Yellow, Size : M', 1, 1, 0, 0, 0, 30.502569, 0.00, 0.000000, 0.000000, 0.000000, 0.00, 0.000000, '', '', 'demo_6', '', 0.000000, 0, 0, '', 0.000, 0.000000, 0.000, 0, '', 0, '0000-00-00 00:00:00', 30.500000, 30.500000, 30.500000, 30.500000, 0.000000, 0.000000, 0.000000, 30.502569),
	(5, 2, 0, 0, 1, 7, 34, 'Printed Chiffon Dress - Color : Yellow, Size : S', 1, 1, 0, 0, 0, 20.501236, 20.00, 0.000000, 0.000000, 0.000000, 0.00, 17.400000, '', '', 'demo_7', '', 0.000000, 0, 0, '', 0.000, 0.000000, 0.000, 0, '', 0, '0000-00-00 00:00:00', 16.400000, 16.400000, 16.400000, 16.400000, 0.000000, 0.000000, 0.000000, 20.501236),
	(6, 3, 0, 0, 1, 1, 1, 'Faded Short Sleeve T-shirts - Color : Orange, Size : S', 1, 1, 0, 0, 0, 16.510000, 0.00, 0.000000, 0.000000, 0.000000, 0.00, 0.000000, '', '', 'demo_1', '', 0.000000, 0, 0, '', 0.000, 0.000000, 0.000, 0, '', 0, '0000-00-00 00:00:00', 16.510000, 16.510000, 16.510000, 16.510000, 0.000000, 0.000000, 0.000000, 16.510000),
	(7, 3, 0, 0, 1, 2, 10, 'Blouse - Color : White, Size : M', 1, 1, 0, 0, 0, 26.999852, 0.00, 0.000000, 0.000000, 0.000000, 0.00, 0.000000, '', '', 'demo_2', '', 0.000000, 0, 0, '', 0.000, 0.000000, 0.000, 0, '', 0, '0000-00-00 00:00:00', 27.000000, 27.000000, 27.000000, 27.000000, 0.000000, 0.000000, 0.000000, 26.999852),
	(8, 3, 0, 0, 1, 6, 32, 'Printed Summer Dress - Color : Yellow, Size : M', 1, 1, 0, 0, 0, 30.502569, 0.00, 0.000000, 0.000000, 0.000000, 0.00, 0.000000, '', '', 'demo_6', '', 0.000000, 0, 0, '', 0.000, 0.000000, 0.000, 0, '', 0, '0000-00-00 00:00:00', 30.500000, 30.500000, 30.500000, 30.500000, 0.000000, 0.000000, 0.000000, 30.502569),
	(9, 4, 0, 0, 1, 1, 1, 'Faded Short Sleeve T-shirts - Color : Orange, Size : S', 1, 1, 0, 0, 0, 16.510000, 0.00, 0.000000, 0.000000, 0.000000, 0.00, 0.000000, '', '', 'demo_1', '', 0.000000, 0, 0, '', 0.000, 0.000000, 0.000, 0, '', 0, '0000-00-00 00:00:00', 16.510000, 16.510000, 16.510000, 16.510000, 0.000000, 0.000000, 0.000000, 16.510000),
	(10, 4, 0, 0, 1, 3, 13, 'Printed Dress - Color : Orange, Size : S', 1, 1, 0, 0, 0, 25.999852, 0.00, 0.000000, 0.000000, 0.000000, 0.00, 0.000000, '', '', 'demo_3', '', 0.000000, 0, 0, '', 0.000, 0.000000, 0.000, 0, '', 0, '0000-00-00 00:00:00', 26.000000, 26.000000, 26.000000, 26.000000, 0.000000, 0.000000, 0.000000, 25.999852),
	(11, 4, 0, 0, 1, 5, 19, 'Printed Summer Dress - Color : Yellow, Size : S', 1, 1, 0, 0, 0, 30.506321, 5.00, 0.000000, 0.000000, 0.000000, 0.00, 29.980000, '', '', 'demo_5', '', 0.000000, 0, 0, '', 0.000, 0.000000, 0.000, 0, '', 0, '0000-00-00 00:00:00', 28.980000, 28.980000, 28.980000, 28.980000, 0.000000, 0.000000, 0.000000, 30.506321),
	(12, 4, 0, 0, 1, 7, 34, 'Printed Chiffon Dress - Color : Yellow, Size : S', 1, 1, 0, 0, 0, 20.501236, 20.00, 0.000000, 0.000000, 0.000000, 0.00, 17.400000, '', '', 'demo_7', '', 0.000000, 0, 0, '', 0.000, 0.000000, 0.000, 0, '', 0, '0000-00-00 00:00:00', 16.400000, 16.400000, 16.400000, 16.400000, 0.000000, 0.000000, 0.000000, 20.501236),
	(13, 5, 0, 0, 1, 1, 1, 'Faded Short Sleeve T-shirts - Color : Orange, Size : S', 1, 1, 0, 0, 0, 16.510000, 0.00, 0.000000, 0.000000, 0.000000, 0.00, 0.000000, '', '', 'demo_1', '', 0.000000, 0, 0, '', 0.000, 0.000000, 0.000, 0, '', 0, '0000-00-00 00:00:00', 16.510000, 16.510000, 16.510000, 16.510000, 0.000000, 0.000000, 0.000000, 16.510000),
	(14, 5, 0, 0, 1, 2, 7, 'Blouse - Color : Black, Size : S', 1, 1, 0, 0, 0, 26.999852, 0.00, 0.000000, 0.000000, 0.000000, 0.00, 0.000000, '', '', 'demo_2', '', 0.000000, 0, 0, '', 0.000, 0.000000, 0.000, 0, '', 0, '0000-00-00 00:00:00', 27.000000, 27.000000, 27.000000, 27.000000, 0.000000, 0.000000, 0.000000, 26.999852),
	(15, 5, 0, 0, 1, 3, 13, 'Printed Dress - Color : Orange, Size : S', 1, 1, 0, 0, 0, 25.999852, 0.00, 0.000000, 0.000000, 0.000000, 0.00, 0.000000, '', '', 'demo_3', '', 0.000000, 0, 0, '', 0.000, 0.000000, 0.000, 0, '', 0, '0000-00-00 00:00:00', 26.000000, 26.000000, 26.000000, 26.000000, 0.000000, 0.000000, 0.000000, 25.999852);
/*!40000 ALTER TABLE `ps_order_detail` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_order_detail_tax
DROP TABLE IF EXISTS `ps_order_detail_tax`;
CREATE TABLE IF NOT EXISTS `ps_order_detail_tax` (
  `id_order_detail` int(11) NOT NULL,
  `id_tax` int(11) NOT NULL,
  `unit_amount` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `total_amount` decimal(16,6) NOT NULL DEFAULT '0.000000',
  KEY `id_order_detail` (`id_order_detail`),
  KEY `id_tax` (`id_tax`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_order_detail_tax: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_order_detail_tax` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_order_detail_tax` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_order_history
DROP TABLE IF EXISTS `ps_order_history`;
CREATE TABLE IF NOT EXISTS `ps_order_history` (
  `id_order_history` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_employee` int(10) unsigned NOT NULL,
  `id_order` int(10) unsigned NOT NULL,
  `id_order_state` int(10) unsigned NOT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_order_history`),
  KEY `order_history_order` (`id_order`),
  KEY `id_employee` (`id_employee`),
  KEY `id_order_state` (`id_order_state`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_order_history: ~7 rows (approximately)
/*!40000 ALTER TABLE `ps_order_history` DISABLE KEYS */;
INSERT INTO `ps_order_history` (`id_order_history`, `id_employee`, `id_order`, `id_order_state`, `date_add`) VALUES
	(1, 0, 1, 1, '2015-05-08 14:00:23'),
	(2, 0, 2, 1, '2015-05-08 14:00:23'),
	(3, 0, 3, 1, '2015-05-08 14:00:23'),
	(4, 0, 4, 1, '2015-05-08 14:00:23'),
	(5, 0, 5, 10, '2015-05-08 14:00:23'),
	(6, 1, 1, 6, '2015-05-08 14:00:23'),
	(7, 1, 3, 8, '2015-05-08 14:00:23');
/*!40000 ALTER TABLE `ps_order_history` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_order_invoice
DROP TABLE IF EXISTS `ps_order_invoice`;
CREATE TABLE IF NOT EXISTS `ps_order_invoice` (
  `id_order_invoice` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_order` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `delivery_number` int(11) NOT NULL,
  `delivery_date` datetime DEFAULT NULL,
  `total_discount_tax_excl` decimal(17,2) NOT NULL DEFAULT '0.00',
  `total_discount_tax_incl` decimal(17,2) NOT NULL DEFAULT '0.00',
  `total_paid_tax_excl` decimal(17,2) NOT NULL DEFAULT '0.00',
  `total_paid_tax_incl` decimal(17,2) NOT NULL DEFAULT '0.00',
  `total_products` decimal(17,2) NOT NULL DEFAULT '0.00',
  `total_products_wt` decimal(17,2) NOT NULL DEFAULT '0.00',
  `total_shipping_tax_excl` decimal(17,2) NOT NULL DEFAULT '0.00',
  `total_shipping_tax_incl` decimal(17,2) NOT NULL DEFAULT '0.00',
  `shipping_tax_computation_method` int(10) unsigned NOT NULL,
  `total_wrapping_tax_excl` decimal(17,2) NOT NULL DEFAULT '0.00',
  `total_wrapping_tax_incl` decimal(17,2) NOT NULL DEFAULT '0.00',
  `note` text,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_order_invoice`),
  KEY `id_order` (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_order_invoice: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_order_invoice` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_order_invoice` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_order_invoice_payment
DROP TABLE IF EXISTS `ps_order_invoice_payment`;
CREATE TABLE IF NOT EXISTS `ps_order_invoice_payment` (
  `id_order_invoice` int(11) unsigned NOT NULL,
  `id_order_payment` int(11) unsigned NOT NULL,
  `id_order` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_order_invoice`,`id_order_payment`),
  KEY `order_payment` (`id_order_payment`),
  KEY `id_order` (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_order_invoice_payment: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_order_invoice_payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_order_invoice_payment` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_order_invoice_tax
DROP TABLE IF EXISTS `ps_order_invoice_tax`;
CREATE TABLE IF NOT EXISTS `ps_order_invoice_tax` (
  `id_order_invoice` int(11) NOT NULL,
  `type` varchar(15) NOT NULL,
  `id_tax` int(11) NOT NULL,
  `amount` decimal(10,6) NOT NULL DEFAULT '0.000000',
  KEY `id_tax` (`id_tax`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_order_invoice_tax: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_order_invoice_tax` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_order_invoice_tax` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_order_message
DROP TABLE IF EXISTS `ps_order_message`;
CREATE TABLE IF NOT EXISTS `ps_order_message` (
  `id_order_message` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_order_message`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_order_message: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_order_message` DISABLE KEYS */;
INSERT INTO `ps_order_message` (`id_order_message`, `date_add`) VALUES
	(1, '2015-05-08 14:00:24');
/*!40000 ALTER TABLE `ps_order_message` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_order_message_lang
DROP TABLE IF EXISTS `ps_order_message_lang`;
CREATE TABLE IF NOT EXISTS `ps_order_message_lang` (
  `id_order_message` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(128) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`id_order_message`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_order_message_lang: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_order_message_lang` DISABLE KEYS */;
INSERT INTO `ps_order_message_lang` (`id_order_message`, `id_lang`, `name`, `message`) VALUES
	(1, 1, 'Retraso', 'Hola.\n\nLamentablemente, uno de los artículos que has pedido está agotado. Esto podría causar un ligero retraso en el envío.\nPor favor, disculpa las molestias ocasionadas. Estamos trabajando duro para solucionarlo, no te preocupes.\n\nUn saludo,');
/*!40000 ALTER TABLE `ps_order_message_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_order_payment
DROP TABLE IF EXISTS `ps_order_payment`;
CREATE TABLE IF NOT EXISTS `ps_order_payment` (
  `id_order_payment` int(11) NOT NULL AUTO_INCREMENT,
  `order_reference` varchar(9) DEFAULT NULL,
  `id_currency` int(10) unsigned NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `conversion_rate` decimal(13,6) NOT NULL DEFAULT '1.000000',
  `transaction_id` varchar(254) DEFAULT NULL,
  `card_number` varchar(254) DEFAULT NULL,
  `card_brand` varchar(254) DEFAULT NULL,
  `card_expiration` char(7) DEFAULT NULL,
  `card_holder` varchar(254) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_order_payment`),
  KEY `order_reference` (`order_reference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_order_payment: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_order_payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_order_payment` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_order_return
DROP TABLE IF EXISTS `ps_order_return`;
CREATE TABLE IF NOT EXISTS `ps_order_return` (
  `id_order_return` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_customer` int(10) unsigned NOT NULL,
  `id_order` int(10) unsigned NOT NULL,
  `state` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `question` text NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_order_return`),
  KEY `order_return_customer` (`id_customer`),
  KEY `id_order` (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_order_return: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_order_return` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_order_return` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_order_return_detail
DROP TABLE IF EXISTS `ps_order_return_detail`;
CREATE TABLE IF NOT EXISTS `ps_order_return_detail` (
  `id_order_return` int(10) unsigned NOT NULL,
  `id_order_detail` int(10) unsigned NOT NULL,
  `id_customization` int(10) unsigned NOT NULL DEFAULT '0',
  `product_quantity` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_order_return`,`id_order_detail`,`id_customization`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_order_return_detail: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_order_return_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_order_return_detail` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_order_return_state
DROP TABLE IF EXISTS `ps_order_return_state`;
CREATE TABLE IF NOT EXISTS `ps_order_return_state` (
  `id_order_return_state` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `color` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id_order_return_state`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_order_return_state: ~5 rows (approximately)
/*!40000 ALTER TABLE `ps_order_return_state` DISABLE KEYS */;
INSERT INTO `ps_order_return_state` (`id_order_return_state`, `color`) VALUES
	(1, '#4169E1'),
	(2, '#8A2BE2'),
	(3, '#32CD32'),
	(4, '#DC143C'),
	(5, '#108510');
/*!40000 ALTER TABLE `ps_order_return_state` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_order_return_state_lang
DROP TABLE IF EXISTS `ps_order_return_state_lang`;
CREATE TABLE IF NOT EXISTS `ps_order_return_state_lang` (
  `id_order_return_state` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id_order_return_state`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_order_return_state_lang: ~5 rows (approximately)
/*!40000 ALTER TABLE `ps_order_return_state_lang` DISABLE KEYS */;
INSERT INTO `ps_order_return_state_lang` (`id_order_return_state`, `id_lang`, `name`) VALUES
	(1, 1, 'Pendiente de confirmación'),
	(2, 1, 'Pendiente del paquete'),
	(3, 1, 'Paquete recibido'),
	(4, 1, 'Devolución denegada'),
	(5, 1, 'Devolución completada');
/*!40000 ALTER TABLE `ps_order_return_state_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_order_slip
DROP TABLE IF EXISTS `ps_order_slip`;
CREATE TABLE IF NOT EXISTS `ps_order_slip` (
  `id_order_slip` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `conversion_rate` decimal(13,6) NOT NULL DEFAULT '1.000000',
  `id_customer` int(10) unsigned NOT NULL,
  `id_order` int(10) unsigned NOT NULL,
  `total_products_tax_excl` decimal(20,6) DEFAULT NULL,
  `total_products_tax_incl` decimal(20,6) DEFAULT NULL,
  `total_shipping_tax_excl` decimal(20,6) DEFAULT NULL,
  `total_shipping_tax_incl` decimal(20,6) DEFAULT NULL,
  `shipping_cost` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `amount` decimal(10,2) NOT NULL,
  `shipping_cost_amount` decimal(10,2) NOT NULL,
  `partial` tinyint(1) NOT NULL,
  `order_slip_type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_order_slip`),
  KEY `order_slip_customer` (`id_customer`),
  KEY `id_order` (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_order_slip: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_order_slip` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_order_slip` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_order_slip_detail
DROP TABLE IF EXISTS `ps_order_slip_detail`;
CREATE TABLE IF NOT EXISTS `ps_order_slip_detail` (
  `id_order_slip` int(10) unsigned NOT NULL,
  `id_order_detail` int(10) unsigned NOT NULL,
  `product_quantity` int(10) unsigned NOT NULL DEFAULT '0',
  `unit_price_tax_excl` decimal(20,6) DEFAULT NULL,
  `unit_price_tax_incl` decimal(20,6) DEFAULT NULL,
  `total_price_tax_excl` decimal(20,6) DEFAULT NULL,
  `total_price_tax_incl` decimal(20,6) DEFAULT NULL,
  `amount_tax_excl` decimal(20,6) DEFAULT NULL,
  `amount_tax_incl` decimal(20,6) DEFAULT NULL,
  PRIMARY KEY (`id_order_slip`,`id_order_detail`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_order_slip_detail: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_order_slip_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_order_slip_detail` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_order_slip_detail_tax
DROP TABLE IF EXISTS `ps_order_slip_detail_tax`;
CREATE TABLE IF NOT EXISTS `ps_order_slip_detail_tax` (
  `id_order_slip_detail` int(11) unsigned NOT NULL,
  `id_tax` int(11) unsigned NOT NULL,
  `unit_amount` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `total_amount` decimal(16,6) NOT NULL DEFAULT '0.000000',
  KEY `id_order_slip_detail` (`id_order_slip_detail`),
  KEY `id_tax` (`id_tax`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_order_slip_detail_tax: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_order_slip_detail_tax` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_order_slip_detail_tax` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_order_state
DROP TABLE IF EXISTS `ps_order_state`;
CREATE TABLE IF NOT EXISTS `ps_order_state` (
  `id_order_state` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoice` tinyint(1) unsigned DEFAULT '0',
  `send_email` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `module_name` varchar(255) DEFAULT NULL,
  `color` varchar(32) DEFAULT NULL,
  `unremovable` tinyint(1) unsigned NOT NULL,
  `hidden` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `logable` tinyint(1) NOT NULL DEFAULT '0',
  `delivery` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `shipped` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `paid` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pdf_invoice` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pdf_delivery` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_order_state`),
  KEY `module_name` (`module_name`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_order_state: ~14 rows (approximately)
/*!40000 ALTER TABLE `ps_order_state` DISABLE KEYS */;
INSERT INTO `ps_order_state` (`id_order_state`, `invoice`, `send_email`, `module_name`, `color`, `unremovable`, `hidden`, `logable`, `delivery`, `shipped`, `paid`, `pdf_invoice`, `pdf_delivery`, `deleted`) VALUES
	(1, 0, 1, 'cheque', '#4169E1', 1, 0, 0, 0, 0, 0, 0, 0, 0),
	(2, 1, 1, '', '#32CD32', 1, 0, 1, 0, 0, 1, 1, 0, 0),
	(3, 1, 1, '', '#FF8C00', 1, 0, 1, 1, 0, 1, 0, 0, 0),
	(4, 1, 1, '', '#8A2BE2', 1, 0, 1, 1, 1, 1, 0, 0, 0),
	(5, 1, 0, '', '#108510', 1, 0, 1, 1, 1, 1, 0, 0, 0),
	(6, 0, 1, '', '#DC143C', 1, 0, 0, 0, 0, 0, 0, 0, 0),
	(7, 1, 1, '', '#ec2e15', 1, 0, 0, 0, 0, 0, 0, 0, 0),
	(8, 0, 1, '', '#8f0621', 1, 0, 0, 0, 0, 0, 0, 0, 0),
	(9, 1, 1, '', '#FF69B4', 1, 0, 0, 0, 0, 1, 0, 0, 0),
	(10, 0, 1, 'bankwire', '#4169E1', 1, 0, 0, 0, 0, 0, 0, 0, 0),
	(11, 0, 0, '', '#4169E1', 1, 0, 0, 0, 0, 0, 0, 0, 0),
	(12, 1, 1, '', '#32CD32', 1, 0, 1, 0, 0, 1, 0, 0, 0),
	(13, 0, 1, '', '#FF69B4', 1, 0, 0, 0, 0, 0, 0, 0, 0),
	(14, 0, 0, 'cashondelivery', '#4169E1', 1, 0, 0, 0, 0, 0, 0, 0, 0);
/*!40000 ALTER TABLE `ps_order_state` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_order_state_lang
DROP TABLE IF EXISTS `ps_order_state_lang`;
CREATE TABLE IF NOT EXISTS `ps_order_state_lang` (
  `id_order_state` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(64) NOT NULL,
  `template` varchar(64) NOT NULL,
  PRIMARY KEY (`id_order_state`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_order_state_lang: ~14 rows (approximately)
/*!40000 ALTER TABLE `ps_order_state_lang` DISABLE KEYS */;
INSERT INTO `ps_order_state_lang` (`id_order_state`, `id_lang`, `name`, `template`) VALUES
	(1, 1, 'Pago mediante cheque pendiente', 'cheque'),
	(2, 1, 'Pago aceptado', 'payment'),
	(3, 1, 'Preparación en proceso', 'preparation'),
	(4, 1, 'Enviado', 'shipped'),
	(5, 1, 'Entregado', ''),
	(6, 1, 'Cancelado', 'order_canceled'),
	(7, 1, 'Reembolso', 'refund'),
	(8, 1, 'Error en el pago', 'payment_error'),
	(9, 1, 'Productos fuera de línea', 'outofstock'),
	(10, 1, 'Pago por transferencia bancaria pendiente', 'bankwire'),
	(11, 1, 'Pago mediante PayPal pendiente', ''),
	(12, 1, 'Pago a distancia aceptado', 'payment'),
	(13, 1, 'Productos fuera de línea', 'outofstock'),
	(14, 1, 'Awaiting cod validation', 'cashondelivery');
/*!40000 ALTER TABLE `ps_order_state_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_pack
DROP TABLE IF EXISTS `ps_pack`;
CREATE TABLE IF NOT EXISTS `ps_pack` (
  `id_product_pack` int(10) unsigned NOT NULL,
  `id_product_item` int(10) unsigned NOT NULL,
  `id_product_attribute_item` int(10) unsigned NOT NULL,
  `quantity` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_product_pack`,`id_product_item`,`id_product_attribute_item`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_pack: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_pack` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_pack` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_page
DROP TABLE IF EXISTS `ps_page`;
CREATE TABLE IF NOT EXISTS `ps_page` (
  `id_page` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_page_type` int(10) unsigned NOT NULL,
  `id_object` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_page`),
  KEY `id_page_type` (`id_page_type`),
  KEY `id_object` (`id_object`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_page: ~3 rows (approximately)
/*!40000 ALTER TABLE `ps_page` DISABLE KEYS */;
INSERT INTO `ps_page` (`id_page`, `id_page_type`, `id_object`) VALUES
	(1, 1, NULL),
	(2, 2, NULL),
	(3, 3, 1);
/*!40000 ALTER TABLE `ps_page` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_pagebuilderprofile
DROP TABLE IF EXISTS `ps_pagebuilderprofile`;
CREATE TABLE IF NOT EXISTS `ps_pagebuilderprofile` (
  `name` varchar(255) NOT NULL,
  `isdefault` tinyint(1) NOT NULL,
  `pkey` varchar(50) NOT NULL,
  `layout` text NOT NULL,
  `widget` text NOT NULL,
  `id_pagebuilderprofile` int(11) NOT NULL AUTO_INCREMENT,
  `isdelete` tinyint(1) NOT NULL,
  `is_footer` tinyint(1) NOT NULL,
  `key` int(11) NOT NULL,
  PRIMARY KEY (`id_pagebuilderprofile`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_pagebuilderprofile: ~10 rows (approximately)
/*!40000 ALTER TABLE `ps_pagebuilderprofile` DISABLE KEYS */;
INSERT INTO `ps_pagebuilderprofile` (`name`, `isdefault`, `pkey`, `layout`, `widget`, `id_pagebuilderprofile`, `isdelete`, `is_footer`, `key`) VALUES
	('FooterMobile', 0, '', '[{"index":0,"cls":"pts-footer-top","bgcolor":"#323d4c","bgimage":"","fullwidth":"0","parallax":"0","sfxcls":null,"cols":[{"index":0,"cls":"hidden-xs","sfxcls":null,"bgcolor":"","bgimage":"","inrow":0,"lgcol":9,"mdcol":8,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831113755","name":"Logo Footer"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":3,"mdcol":4,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831222604","name":"Social footer"}],"rows":[]}]},{"index":0,"cls":"pts-footer-center","bgcolor":"","bgimage":"","fullwidth":"0","parallax":"0","sfxcls":null,"cols":[{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":3,"mdcol":3,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831295069","name":"Contact info"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":2,"mdcol":2,"smcol":4,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831349733","name":"Customer Service"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":2,"mdcol":2,"smcol":4,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831415734","name":"Information"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":2,"mdcol":2,"smcol":4,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831468547","name":"My account"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":3,"mdcol":3,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"newsletter","wkey":"key_1414831529453","name":"Sign Up For Newsletter"}],"rows":[]}]},{"index":0,"cls":"hidden-xs","bgcolor":"","bgimage":"bg-footerbottom.jpg","fullwidth":"0","parallax":"1","sfxcls":null,"cols":[{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":12,"mdcol":12,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831611780","name":"Html Footer"}],"rows":[]}]}]', 'a:8:{s:17:"key_1414831113755";a:1:{s:6:"config";s:2547:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzExMTM3NTU=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:16:"TG9nbyBGb290ZXI=";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:16:"TG9nbyBGb290ZXI=";s:14:"widget_title_2";s:16:"TG9nbyBGb290ZXI=";s:14:"widget_title_3";s:16:"TG9nbyBGb290ZXI=";s:14:"widget_title_4";s:16:"TG9nbyBGb290ZXI=";s:14:"widget_title_5";s:16:"TG9nbyBGb290ZXI=";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:328:"PGRpdiBjbGFzcz0iaW1hZ2UtYmFubmVyICBwdWxsLWxlZnQiPjxpbWcgY2xhc3M9Iml0ZW0taW1nIiBzcmM9Im1vZHVsZXMvcHRzcGFnZWJ1aWxkZXIvaW1hZ2VzL2xvZ28tZm9vdGVyLnBuZyIgYWx0PSIiIC8+PC9kaXY+DQo8ZGl2IGNsYXNzPSJpdGVtLWh0bWwiPg0KPHA+V2XigJlyZSBjb25maWRlbnQgd2XigJl2ZSBwcm92aWRlZCBhbGwgdGhlIGJlc3QgZm9yIHlvdS4gU3RheSBjb25uZWVjdCB3aXRoIHVzPC9wPg0KPC9kaXY+";s:13:"htmlcontent_2";s:388:"PGRpdiBjbGFzcz0iaW1hZ2UtYmFubmVyICBwdWxsLWxlZnQiPjxpbWcgY2xhc3M9Iml0ZW0taW1nIiBzcmM9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW1nL2Ntcy9sb2dvLWZvb3Rlci5wbmciIGFsdD0iIiB3aWR0aD0iMTMxIiBoZWlnaHQ9IjQwIiAvPjwvZGl2Pg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxwPldl4oCZcmUgY29uZmlkZW50IHdl4oCZdmUgcHJvdmlkZWQgYWxsIHRoZSBiZXN0IGZvciB5b3UuIFN0YXkgY29ubmVlY3Qgd2l0aCB1czwvcD4NCjwvZGl2Pg==";s:13:"htmlcontent_3";s:388:"PGRpdiBjbGFzcz0iaW1hZ2UtYmFubmVyICBwdWxsLWxlZnQiPjxpbWcgY2xhc3M9Iml0ZW0taW1nIiBzcmM9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW1nL2Ntcy9sb2dvLWZvb3Rlci5wbmciIGFsdD0iIiB3aWR0aD0iMTMxIiBoZWlnaHQ9IjQwIiAvPjwvZGl2Pg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxwPldl4oCZcmUgY29uZmlkZW50IHdl4oCZdmUgcHJvdmlkZWQgYWxsIHRoZSBiZXN0IGZvciB5b3UuIFN0YXkgY29ubmVlY3Qgd2l0aCB1czwvcD4NCjwvZGl2Pg==";s:13:"htmlcontent_4";s:388:"PGRpdiBjbGFzcz0iaW1hZ2UtYmFubmVyICBwdWxsLWxlZnQiPjxpbWcgY2xhc3M9Iml0ZW0taW1nIiBzcmM9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW1nL2Ntcy9sb2dvLWZvb3Rlci5wbmciIGFsdD0iIiB3aWR0aD0iMTMxIiBoZWlnaHQ9IjQwIiAvPjwvZGl2Pg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxwPldl4oCZcmUgY29uZmlkZW50IHdl4oCZdmUgcHJvdmlkZWQgYWxsIHRoZSBiZXN0IGZvciB5b3UuIFN0YXkgY29ubmVlY3Qgd2l0aCB1czwvcD4NCjwvZGl2Pg==";s:13:"htmlcontent_5";s:388:"PGRpdiBjbGFzcz0iaW1hZ2UtYmFubmVyICBwdWxsLWxlZnQiPjxpbWcgY2xhc3M9Iml0ZW0taW1nIiBzcmM9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW1nL2Ntcy9sb2dvLWZvb3Rlci5wbmciIGFsdD0iIiB3aWR0aD0iMTMxIiBoZWlnaHQ9IjQwIiAvPjwvZGl2Pg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxwPldl4oCZcmUgY29uZmlkZW50IHdl4oCZdmUgcHJvdmlkZWQgYWxsIHRoZSBiZXN0IGZvciB5b3UuIFN0YXkgY29ubmVlY3Qgd2l0aCB1czwvcD4NCjwvZGl2Pg==";}s:4:"wkey";s:17:"key_1414831113755";}";}s:17:"key_1414831222604";a:1:{s:6:"config";s:6557:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzEyMjI2MDQ=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:20:"U29jaWFsIGZvb3Rlcg==";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:20:"U29jaWFsIGZvb3Rlcg==";s:14:"widget_title_2";s:20:"U29jaWFsIGZvb3Rlcg==";s:14:"widget_title_3";s:20:"U29jaWFsIGZvb3Rlcg==";s:14:"widget_title_4";s:20:"U29jaWFsIGZvb3Rlcg==";s:14:"widget_title_5";s:20:"U29jaWFsIGZvb3Rlcg==";s:12:"addition_cls";s:20:"c29jaWFsLWZvb3Rlcg==";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:1168:"PGRpdiBpZD0ic29jaWFsX2Jsb2NrIj4NCjx1bCBjbGFzcz0ic29jaWFsIj4NCjxsaSBjbGFzcz0iZmFjZWJvb2siPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy5mYWNlYm9vay5jb20vcHJlc3Rhc2hvcCIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IkZhY2Vib29rIj4gRmFjZWJvb2sgPC9hPjwvbGk+DQo8bGkgY2xhc3M9InR3aXR0ZXIiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy50d2l0dGVyLmNvbS9wcmVzdGFzaG9wIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iVHdpdHRlciI+VHdpdHRlciA8L2E+PC9saT4NCjxsaSBjbGFzcz0icnNzIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9Imh0dHA6Ly93d3cucHJlc3Rhc2hvcC5jb20vYmxvZy9lbi9mZWVkLyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlJTUyI+IFJTUzwvYT48L2xpPg0KPGxpIGNsYXNzPSJ5b3V0dWJlIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9IiMiIHRhcmdldD0iX2JsYW5rIiBkYXRhLW9yaWdpbmFsLXRpdGxlPSJZb3V0dWJlIj4gWW91dHViZTwvYT48L2xpPg0KPGxpIGNsYXNzPSJnb29nbGUtcGx1cyI+PGEgY2xhc3M9ImJ0bi10b29sdGlwIiBocmVmPSIjIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iR29vZ2xlIFBsdXMiPkdvb2dsZSBQbHVzIDwvYT48L2xpPg0KPGxpIGNsYXNzPSJwaW50ZXJlc3QiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iIyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlBpbnRlcmVzdCI+IFBpbnRlcmVzdDwvYT48L2xpPg0KPC91bD4NCjwvZGl2Pg==";s:13:"htmlcontent_2";s:1168:"PGRpdiBpZD0ic29jaWFsX2Jsb2NrIj4NCjx1bCBjbGFzcz0ic29jaWFsIj4NCjxsaSBjbGFzcz0iZmFjZWJvb2siPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy5mYWNlYm9vay5jb20vcHJlc3Rhc2hvcCIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IkZhY2Vib29rIj4gRmFjZWJvb2sgPC9hPjwvbGk+DQo8bGkgY2xhc3M9InR3aXR0ZXIiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy50d2l0dGVyLmNvbS9wcmVzdGFzaG9wIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iVHdpdHRlciI+VHdpdHRlciA8L2E+PC9saT4NCjxsaSBjbGFzcz0icnNzIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9Imh0dHA6Ly93d3cucHJlc3Rhc2hvcC5jb20vYmxvZy9lbi9mZWVkLyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlJTUyI+IFJTUzwvYT48L2xpPg0KPGxpIGNsYXNzPSJ5b3V0dWJlIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9IiMiIHRhcmdldD0iX2JsYW5rIiBkYXRhLW9yaWdpbmFsLXRpdGxlPSJZb3V0dWJlIj4gWW91dHViZTwvYT48L2xpPg0KPGxpIGNsYXNzPSJnb29nbGUtcGx1cyI+PGEgY2xhc3M9ImJ0bi10b29sdGlwIiBocmVmPSIjIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iR29vZ2xlIFBsdXMiPkdvb2dsZSBQbHVzIDwvYT48L2xpPg0KPGxpIGNsYXNzPSJwaW50ZXJlc3QiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iIyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlBpbnRlcmVzdCI+IFBpbnRlcmVzdDwvYT48L2xpPg0KPC91bD4NCjwvZGl2Pg==";s:13:"htmlcontent_3";s:1168:"PGRpdiBpZD0ic29jaWFsX2Jsb2NrIj4NCjx1bCBjbGFzcz0ic29jaWFsIj4NCjxsaSBjbGFzcz0iZmFjZWJvb2siPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy5mYWNlYm9vay5jb20vcHJlc3Rhc2hvcCIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IkZhY2Vib29rIj4gRmFjZWJvb2sgPC9hPjwvbGk+DQo8bGkgY2xhc3M9InR3aXR0ZXIiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy50d2l0dGVyLmNvbS9wcmVzdGFzaG9wIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iVHdpdHRlciI+VHdpdHRlciA8L2E+PC9saT4NCjxsaSBjbGFzcz0icnNzIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9Imh0dHA6Ly93d3cucHJlc3Rhc2hvcC5jb20vYmxvZy9lbi9mZWVkLyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlJTUyI+IFJTUzwvYT48L2xpPg0KPGxpIGNsYXNzPSJ5b3V0dWJlIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9IiMiIHRhcmdldD0iX2JsYW5rIiBkYXRhLW9yaWdpbmFsLXRpdGxlPSJZb3V0dWJlIj4gWW91dHViZTwvYT48L2xpPg0KPGxpIGNsYXNzPSJnb29nbGUtcGx1cyI+PGEgY2xhc3M9ImJ0bi10b29sdGlwIiBocmVmPSIjIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iR29vZ2xlIFBsdXMiPkdvb2dsZSBQbHVzIDwvYT48L2xpPg0KPGxpIGNsYXNzPSJwaW50ZXJlc3QiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iIyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlBpbnRlcmVzdCI+IFBpbnRlcmVzdDwvYT48L2xpPg0KPC91bD4NCjwvZGl2Pg==";s:13:"htmlcontent_4";s:1168:"PGRpdiBpZD0ic29jaWFsX2Jsb2NrIj4NCjx1bCBjbGFzcz0ic29jaWFsIj4NCjxsaSBjbGFzcz0iZmFjZWJvb2siPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy5mYWNlYm9vay5jb20vcHJlc3Rhc2hvcCIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IkZhY2Vib29rIj4gRmFjZWJvb2sgPC9hPjwvbGk+DQo8bGkgY2xhc3M9InR3aXR0ZXIiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy50d2l0dGVyLmNvbS9wcmVzdGFzaG9wIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iVHdpdHRlciI+VHdpdHRlciA8L2E+PC9saT4NCjxsaSBjbGFzcz0icnNzIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9Imh0dHA6Ly93d3cucHJlc3Rhc2hvcC5jb20vYmxvZy9lbi9mZWVkLyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlJTUyI+IFJTUzwvYT48L2xpPg0KPGxpIGNsYXNzPSJ5b3V0dWJlIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9IiMiIHRhcmdldD0iX2JsYW5rIiBkYXRhLW9yaWdpbmFsLXRpdGxlPSJZb3V0dWJlIj4gWW91dHViZTwvYT48L2xpPg0KPGxpIGNsYXNzPSJnb29nbGUtcGx1cyI+PGEgY2xhc3M9ImJ0bi10b29sdGlwIiBocmVmPSIjIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iR29vZ2xlIFBsdXMiPkdvb2dsZSBQbHVzIDwvYT48L2xpPg0KPGxpIGNsYXNzPSJwaW50ZXJlc3QiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iIyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlBpbnRlcmVzdCI+IFBpbnRlcmVzdDwvYT48L2xpPg0KPC91bD4NCjwvZGl2Pg==";s:13:"htmlcontent_5";s:1168:"PGRpdiBpZD0ic29jaWFsX2Jsb2NrIj4NCjx1bCBjbGFzcz0ic29jaWFsIj4NCjxsaSBjbGFzcz0iZmFjZWJvb2siPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy5mYWNlYm9vay5jb20vcHJlc3Rhc2hvcCIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IkZhY2Vib29rIj4gRmFjZWJvb2sgPC9hPjwvbGk+DQo8bGkgY2xhc3M9InR3aXR0ZXIiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy50d2l0dGVyLmNvbS9wcmVzdGFzaG9wIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iVHdpdHRlciI+VHdpdHRlciA8L2E+PC9saT4NCjxsaSBjbGFzcz0icnNzIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9Imh0dHA6Ly93d3cucHJlc3Rhc2hvcC5jb20vYmxvZy9lbi9mZWVkLyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlJTUyI+IFJTUzwvYT48L2xpPg0KPGxpIGNsYXNzPSJ5b3V0dWJlIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9IiMiIHRhcmdldD0iX2JsYW5rIiBkYXRhLW9yaWdpbmFsLXRpdGxlPSJZb3V0dWJlIj4gWW91dHViZTwvYT48L2xpPg0KPGxpIGNsYXNzPSJnb29nbGUtcGx1cyI+PGEgY2xhc3M9ImJ0bi10b29sdGlwIiBocmVmPSIjIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iR29vZ2xlIFBsdXMiPkdvb2dsZSBQbHVzIDwvYT48L2xpPg0KPGxpIGNsYXNzPSJwaW50ZXJlc3QiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iIyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlBpbnRlcmVzdCI+IFBpbnRlcmVzdDwvYT48L2xpPg0KPC91bD4NCjwvZGl2Pg==";}s:4:"wkey";s:17:"key_1414831222604";}";}s:17:"key_1414831295069";a:1:{s:6:"config";s:4247:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzEyOTUwNjk=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:16:"Q29udGFjdCBpbmZv";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:16:"Q29udGFjdCBpbmZv";s:14:"widget_title_2";s:16:"Q29udGFjdCBpbmZv";s:14:"widget_title_3";s:16:"Q29udGFjdCBpbmZv";s:14:"widget_title_4";s:16:"Q29udGFjdCBpbmZv";s:14:"widget_title_5";s:16:"Q29udGFjdCBpbmZv";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:716:"PGRpdiBjbGFzcz0ibGlzdC1ncm91cCI+U2VkIG5vbiBtYXVyaXMgdml0YWUgZXJhdCBjb25zZXF1YXQgYXVjdG9yIGV1IGluIGVsaXQuIENsYXNzIGFwdGVudCB0YWNpdGkgc29zcXUgYWQgbGl0b3JhIHRvcnF1ZW50IHBlciBjb251YmlhPC9kaXY+DQo8dWwgY2xhc3M9Imxpc3QtZ3JvdXAiPg0KPGxpPk5vIDExMDQgU2t5IFRvd2VyLCBOZXd5b3JrLCBVU0E8L2xpPg0KPGxpPkNhbGwgdXMgbm93OiAwMTIzLTQ1Ni03ODk8L2xpPg0KPGxpPkVtYWlsOiA8YSBocmVmPSJtYWlsdG86JTczJTYxJTZjJTY1JTczQCU3OSU2ZiU3NSU3MiU2MyU2ZiU2ZCU3MCU2MSU2ZSU3OS4lNjMlNmYlNmQiPnNhbGVzQHlvdXJjb21wYW55LmNvbTwvYT48L2xpPg0KPC91bD4NCjxkaXYgY2xhc3M9Im1hcCI+PGEgaWQ9ImZhbmN5Ym94LW1hcCIgdGl0bGU9IlZpZXdNYXAiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9Y29udGFjdCZjb250ZW50X29ubHk9MSI+Vmlld01hcCA8L2E+PC9kaXY+";s:13:"htmlcontent_2";s:716:"PGRpdiBjbGFzcz0ibGlzdC1ncm91cCI+U2VkIG5vbiBtYXVyaXMgdml0YWUgZXJhdCBjb25zZXF1YXQgYXVjdG9yIGV1IGluIGVsaXQuIENsYXNzIGFwdGVudCB0YWNpdGkgc29zcXUgYWQgbGl0b3JhIHRvcnF1ZW50IHBlciBjb251YmlhPC9kaXY+DQo8dWwgY2xhc3M9Imxpc3QtZ3JvdXAiPg0KPGxpPk5vIDExMDQgU2t5IFRvd2VyLCBOZXd5b3JrLCBVU0E8L2xpPg0KPGxpPkNhbGwgdXMgbm93OiAwMTIzLTQ1Ni03ODk8L2xpPg0KPGxpPkVtYWlsOiA8YSBocmVmPSJtYWlsdG86JTczJTYxJTZjJTY1JTczQCU3OSU2ZiU3NSU3MiU2MyU2ZiU2ZCU3MCU2MSU2ZSU3OS4lNjMlNmYlNmQiPnNhbGVzQHlvdXJjb21wYW55LmNvbTwvYT48L2xpPg0KPC91bD4NCjxkaXYgY2xhc3M9Im1hcCI+PGEgaWQ9ImZhbmN5Ym94LW1hcCIgdGl0bGU9IlZpZXdNYXAiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9Y29udGFjdCZjb250ZW50X29ubHk9MSI+Vmlld01hcCA8L2E+PC9kaXY+";s:13:"htmlcontent_3";s:716:"PGRpdiBjbGFzcz0ibGlzdC1ncm91cCI+U2VkIG5vbiBtYXVyaXMgdml0YWUgZXJhdCBjb25zZXF1YXQgYXVjdG9yIGV1IGluIGVsaXQuIENsYXNzIGFwdGVudCB0YWNpdGkgc29zcXUgYWQgbGl0b3JhIHRvcnF1ZW50IHBlciBjb251YmlhPC9kaXY+DQo8dWwgY2xhc3M9Imxpc3QtZ3JvdXAiPg0KPGxpPk5vIDExMDQgU2t5IFRvd2VyLCBOZXd5b3JrLCBVU0E8L2xpPg0KPGxpPkNhbGwgdXMgbm93OiAwMTIzLTQ1Ni03ODk8L2xpPg0KPGxpPkVtYWlsOiA8YSBocmVmPSJtYWlsdG86JTczJTYxJTZjJTY1JTczQCU3OSU2ZiU3NSU3MiU2MyU2ZiU2ZCU3MCU2MSU2ZSU3OS4lNjMlNmYlNmQiPnNhbGVzQHlvdXJjb21wYW55LmNvbTwvYT48L2xpPg0KPC91bD4NCjxkaXYgY2xhc3M9Im1hcCI+PGEgaWQ9ImZhbmN5Ym94LW1hcCIgdGl0bGU9IlZpZXdNYXAiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9Y29udGFjdCZjb250ZW50X29ubHk9MSI+Vmlld01hcCA8L2E+PC9kaXY+";s:13:"htmlcontent_4";s:716:"PGRpdiBjbGFzcz0ibGlzdC1ncm91cCI+U2VkIG5vbiBtYXVyaXMgdml0YWUgZXJhdCBjb25zZXF1YXQgYXVjdG9yIGV1IGluIGVsaXQuIENsYXNzIGFwdGVudCB0YWNpdGkgc29zcXUgYWQgbGl0b3JhIHRvcnF1ZW50IHBlciBjb251YmlhPC9kaXY+DQo8dWwgY2xhc3M9Imxpc3QtZ3JvdXAiPg0KPGxpPk5vIDExMDQgU2t5IFRvd2VyLCBOZXd5b3JrLCBVU0E8L2xpPg0KPGxpPkNhbGwgdXMgbm93OiAwMTIzLTQ1Ni03ODk8L2xpPg0KPGxpPkVtYWlsOiA8YSBocmVmPSJtYWlsdG86JTczJTYxJTZjJTY1JTczQCU3OSU2ZiU3NSU3MiU2MyU2ZiU2ZCU3MCU2MSU2ZSU3OS4lNjMlNmYlNmQiPnNhbGVzQHlvdXJjb21wYW55LmNvbTwvYT48L2xpPg0KPC91bD4NCjxkaXYgY2xhc3M9Im1hcCI+PGEgaWQ9ImZhbmN5Ym94LW1hcCIgdGl0bGU9IlZpZXdNYXAiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9Y29udGFjdCZjb250ZW50X29ubHk9MSI+Vmlld01hcCA8L2E+PC9kaXY+";s:13:"htmlcontent_5";s:716:"PGRpdiBjbGFzcz0ibGlzdC1ncm91cCI+U2VkIG5vbiBtYXVyaXMgdml0YWUgZXJhdCBjb25zZXF1YXQgYXVjdG9yIGV1IGluIGVsaXQuIENsYXNzIGFwdGVudCB0YWNpdGkgc29zcXUgYWQgbGl0b3JhIHRvcnF1ZW50IHBlciBjb251YmlhPC9kaXY+DQo8dWwgY2xhc3M9Imxpc3QtZ3JvdXAiPg0KPGxpPk5vIDExMDQgU2t5IFRvd2VyLCBOZXd5b3JrLCBVU0E8L2xpPg0KPGxpPkNhbGwgdXMgbm93OiAwMTIzLTQ1Ni03ODk8L2xpPg0KPGxpPkVtYWlsOiA8YSBocmVmPSJtYWlsdG86JTczJTYxJTZjJTY1JTczQCU3OSU2ZiU3NSU3MiU2MyU2ZiU2ZCU3MCU2MSU2ZSU3OS4lNjMlNmYlNmQiPnNhbGVzQHlvdXJjb21wYW55LmNvbTwvYT48L2xpPg0KPC91bD4NCjxkaXYgY2xhc3M9Im1hcCI+PGEgaWQ9ImZhbmN5Ym94LW1hcCIgdGl0bGU9IlZpZXdNYXAiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9Y29udGFjdCZjb250ZW50X29ubHk9MSI+Vmlld01hcCA8L2E+PC9kaXY+";}s:4:"wkey";s:17:"key_1414831295069";}";}s:17:"key_1414831349733";a:1:{s:6:"config";s:3535:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzEzNDk3MzM=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:24:"Q3VzdG9tZXIgU2VydmljZQ==";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:24:"Q3VzdG9tZXIgU2VydmljZQ==";s:14:"widget_title_2";s:24:"Q3VzdG9tZXIgU2VydmljZQ==";s:14:"widget_title_3";s:24:"Q3VzdG9tZXIgU2VydmljZQ==";s:14:"widget_title_4";s:24:"Q3VzdG9tZXIgU2VydmljZQ==";s:14:"widget_title_5";s:24:"Q3VzdG9tZXIgU2VydmljZQ==";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:564:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iT3VyIHN0b3JlcyIgaHJlZj0iaW5kZXgucGhwP2NvbnRyb2xsZXI9c3RvcmVzIj4gT3VyIHN0b3JlcyA8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IkNvbnRhY3QgdXMiIGhyZWY9ImluZGV4LnBocD9jb250cm9sbGVyPWNvbnRhY3QiPiBDb250YWN0IHVzIDwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJTaXRlbWFwIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zaXRlbWFwIj4gU2l0ZW1hcCA8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iVmlldyBhIGxpc3Qgb2Ygc3VwcGxpZXJzIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zdXBwbGllciI+IFN1cHBsaWVycyA8L2E+PC9saT4NCjwvdWw+";s:13:"htmlcontent_2";s:564:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iT3VyIHN0b3JlcyIgaHJlZj0iaW5kZXgucGhwP2NvbnRyb2xsZXI9c3RvcmVzIj4gT3VyIHN0b3JlcyA8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IkNvbnRhY3QgdXMiIGhyZWY9ImluZGV4LnBocD9jb250cm9sbGVyPWNvbnRhY3QiPiBDb250YWN0IHVzIDwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJTaXRlbWFwIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zaXRlbWFwIj4gU2l0ZW1hcCA8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iVmlldyBhIGxpc3Qgb2Ygc3VwcGxpZXJzIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zdXBwbGllciI+IFN1cHBsaWVycyA8L2E+PC9saT4NCjwvdWw+";s:13:"htmlcontent_3";s:564:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iT3VyIHN0b3JlcyIgaHJlZj0iaW5kZXgucGhwP2NvbnRyb2xsZXI9c3RvcmVzIj4gT3VyIHN0b3JlcyA8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IkNvbnRhY3QgdXMiIGhyZWY9ImluZGV4LnBocD9jb250cm9sbGVyPWNvbnRhY3QiPiBDb250YWN0IHVzIDwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJTaXRlbWFwIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zaXRlbWFwIj4gU2l0ZW1hcCA8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iVmlldyBhIGxpc3Qgb2Ygc3VwcGxpZXJzIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zdXBwbGllciI+IFN1cHBsaWVycyA8L2E+PC9saT4NCjwvdWw+";s:13:"htmlcontent_4";s:564:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iT3VyIHN0b3JlcyIgaHJlZj0iaW5kZXgucGhwP2NvbnRyb2xsZXI9c3RvcmVzIj4gT3VyIHN0b3JlcyA8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IkNvbnRhY3QgdXMiIGhyZWY9ImluZGV4LnBocD9jb250cm9sbGVyPWNvbnRhY3QiPiBDb250YWN0IHVzIDwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJTaXRlbWFwIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zaXRlbWFwIj4gU2l0ZW1hcCA8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iVmlldyBhIGxpc3Qgb2Ygc3VwcGxpZXJzIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zdXBwbGllciI+IFN1cHBsaWVycyA8L2E+PC9saT4NCjwvdWw+";s:13:"htmlcontent_5";s:564:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iT3VyIHN0b3JlcyIgaHJlZj0iaW5kZXgucGhwP2NvbnRyb2xsZXI9c3RvcmVzIj4gT3VyIHN0b3JlcyA8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IkNvbnRhY3QgdXMiIGhyZWY9ImluZGV4LnBocD9jb250cm9sbGVyPWNvbnRhY3QiPiBDb250YWN0IHVzIDwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJTaXRlbWFwIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zaXRlbWFwIj4gU2l0ZW1hcCA8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iVmlldyBhIGxpc3Qgb2Ygc3VwcGxpZXJzIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zdXBwbGllciI+IFN1cHBsaWVycyA8L2E+PC9saT4NCjwvdWw+";}s:4:"wkey";s:17:"key_1414831349733";}";}s:17:"key_1414831415734";a:1:{s:6:"config";s:3187:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzE0MTU3MzQ=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:16:"SW5mb3JtYXRpb24=";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:16:"SW5mb3JtYXRpb24=";s:14:"widget_title_2";s:16:"SW5mb3JtYXRpb24=";s:14:"widget_title_3";s:16:"SW5mb3JtYXRpb24=";s:14:"widget_title_4";s:16:"SW5mb3JtYXRpb24=";s:14:"widget_title_5";s:16:"SW5mb3JtYXRpb24=";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:504:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj5NaSB2aXRhZSBtYWduaXM8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+IEZ1c2NlIGxhb3JlZXQ8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+VmVzdGlidSBmYXVjaWJ1czwvYT48L2xpPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj4gUG9ydHRpdG9yIGxhb3JlZXQgPC9hPjwvbGk+DQo8bGkgY2xhc3M9Iml0ZW0iPjxhIHRpdGxlPSIjIiBocmVmPSIjIj4gRmVsaXMgcG9ydHRpdG9yIDwvYT48L2xpPg0KPC91bD4=";s:13:"htmlcontent_2";s:504:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj5NaSB2aXRhZSBtYWduaXM8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+IEZ1c2NlIGxhb3JlZXQ8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+VmVzdGlidSBmYXVjaWJ1czwvYT48L2xpPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj4gUG9ydHRpdG9yIGxhb3JlZXQgPC9hPjwvbGk+DQo8bGkgY2xhc3M9Iml0ZW0iPjxhIHRpdGxlPSIjIiBocmVmPSIjIj4gRmVsaXMgcG9ydHRpdG9yIDwvYT48L2xpPg0KPC91bD4=";s:13:"htmlcontent_3";s:504:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj5NaSB2aXRhZSBtYWduaXM8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+IEZ1c2NlIGxhb3JlZXQ8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+VmVzdGlidSBmYXVjaWJ1czwvYT48L2xpPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj4gUG9ydHRpdG9yIGxhb3JlZXQgPC9hPjwvbGk+DQo8bGkgY2xhc3M9Iml0ZW0iPjxhIHRpdGxlPSIjIiBocmVmPSIjIj4gRmVsaXMgcG9ydHRpdG9yIDwvYT48L2xpPg0KPC91bD4=";s:13:"htmlcontent_4";s:504:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj5NaSB2aXRhZSBtYWduaXM8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+IEZ1c2NlIGxhb3JlZXQ8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+VmVzdGlidSBmYXVjaWJ1czwvYT48L2xpPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj4gUG9ydHRpdG9yIGxhb3JlZXQgPC9hPjwvbGk+DQo8bGkgY2xhc3M9Iml0ZW0iPjxhIHRpdGxlPSIjIiBocmVmPSIjIj4gRmVsaXMgcG9ydHRpdG9yIDwvYT48L2xpPg0KPC91bD4=";s:13:"htmlcontent_5";s:504:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj5NaSB2aXRhZSBtYWduaXM8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+IEZ1c2NlIGxhb3JlZXQ8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+VmVzdGlidSBmYXVjaWJ1czwvYT48L2xpPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj4gUG9ydHRpdG9yIGxhb3JlZXQgPC9hPjwvbGk+DQo8bGkgY2xhc3M9Iml0ZW0iPjxhIHRpdGxlPSIjIiBocmVmPSIjIj4gRmVsaXMgcG9ydHRpdG9yIDwvYT48L2xpPg0KPC91bD4=";}s:4:"wkey";s:17:"key_1414831415734";}";}s:17:"key_1414831468547";a:1:{s:6:"config";s:5007:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzE0Njg1NDc=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:16:"TXkgYWNjb3VudA==";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:16:"TXkgYWNjb3VudA==";s:14:"widget_title_2";s:16:"TXkgYWNjb3VudA==";s:14:"widget_title_3";s:16:"TXkgYWNjb3VudA==";s:14:"widget_title_4";s:16:"TXkgYWNjb3VudA==";s:14:"widget_title_5";s:16:"TXkgYWNjb3VudA==";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:868:"PHVsIGNsYXNzPSJsaXN0LWJsb2NrIGxpc3QtZ3JvdXAgbm9zdHlsZSI+DQo8bGk+PGEgdGl0bGU9Ik15IG9yZGVycyIgaHJlZj0iaHR0cDovL2xvY2FsaG9zdC9wcmVzdGFzaG9wL3BmX21peG1vYmlsZS9pbmRleC5waHA/Y29udHJvbGxlcj1oaXN0b3J5IiByZWw9Im5vZm9sbG93Ij5NeSBvcmRlcnM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTXkgY3JlZGl0IHNsaXBzIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPW9yZGVyLXNsaXAiIHJlbD0ibm9mb2xsb3ciPk15IGNyZWRpdCBzbGlwczwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJNeSBhZGRyZXNzZXMiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9YWRkcmVzc2VzIiByZWw9Im5vZm9sbG93Ij5NeSBhZGRyZXNzZXM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTWFuYWdlIG15IHBlcnNvbmFsIGluZm9ybWF0aW9uIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPWlkZW50aXR5IiByZWw9Im5vZm9sbG93Ij5NeSBwZXJzb25hbCBpbmZvPC9hPjwvbGk+DQo8L3VsPg==";s:13:"htmlcontent_2";s:868:"PHVsIGNsYXNzPSJsaXN0LWJsb2NrIGxpc3QtZ3JvdXAgbm9zdHlsZSI+DQo8bGk+PGEgdGl0bGU9Ik15IG9yZGVycyIgaHJlZj0iaHR0cDovL2xvY2FsaG9zdC9wcmVzdGFzaG9wL3BmX21peG1vYmlsZS9pbmRleC5waHA/Y29udHJvbGxlcj1oaXN0b3J5IiByZWw9Im5vZm9sbG93Ij5NeSBvcmRlcnM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTXkgY3JlZGl0IHNsaXBzIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPW9yZGVyLXNsaXAiIHJlbD0ibm9mb2xsb3ciPk15IGNyZWRpdCBzbGlwczwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJNeSBhZGRyZXNzZXMiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9YWRkcmVzc2VzIiByZWw9Im5vZm9sbG93Ij5NeSBhZGRyZXNzZXM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTWFuYWdlIG15IHBlcnNvbmFsIGluZm9ybWF0aW9uIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPWlkZW50aXR5IiByZWw9Im5vZm9sbG93Ij5NeSBwZXJzb25hbCBpbmZvPC9hPjwvbGk+DQo8L3VsPg==";s:13:"htmlcontent_3";s:868:"PHVsIGNsYXNzPSJsaXN0LWJsb2NrIGxpc3QtZ3JvdXAgbm9zdHlsZSI+DQo8bGk+PGEgdGl0bGU9Ik15IG9yZGVycyIgaHJlZj0iaHR0cDovL2xvY2FsaG9zdC9wcmVzdGFzaG9wL3BmX21peG1vYmlsZS9pbmRleC5waHA/Y29udHJvbGxlcj1oaXN0b3J5IiByZWw9Im5vZm9sbG93Ij5NeSBvcmRlcnM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTXkgY3JlZGl0IHNsaXBzIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPW9yZGVyLXNsaXAiIHJlbD0ibm9mb2xsb3ciPk15IGNyZWRpdCBzbGlwczwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJNeSBhZGRyZXNzZXMiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9YWRkcmVzc2VzIiByZWw9Im5vZm9sbG93Ij5NeSBhZGRyZXNzZXM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTWFuYWdlIG15IHBlcnNvbmFsIGluZm9ybWF0aW9uIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPWlkZW50aXR5IiByZWw9Im5vZm9sbG93Ij5NeSBwZXJzb25hbCBpbmZvPC9hPjwvbGk+DQo8L3VsPg==";s:13:"htmlcontent_4";s:868:"PHVsIGNsYXNzPSJsaXN0LWJsb2NrIGxpc3QtZ3JvdXAgbm9zdHlsZSI+DQo8bGk+PGEgdGl0bGU9Ik15IG9yZGVycyIgaHJlZj0iaHR0cDovL2xvY2FsaG9zdC9wcmVzdGFzaG9wL3BmX21peG1vYmlsZS9pbmRleC5waHA/Y29udHJvbGxlcj1oaXN0b3J5IiByZWw9Im5vZm9sbG93Ij5NeSBvcmRlcnM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTXkgY3JlZGl0IHNsaXBzIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPW9yZGVyLXNsaXAiIHJlbD0ibm9mb2xsb3ciPk15IGNyZWRpdCBzbGlwczwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJNeSBhZGRyZXNzZXMiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9YWRkcmVzc2VzIiByZWw9Im5vZm9sbG93Ij5NeSBhZGRyZXNzZXM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTWFuYWdlIG15IHBlcnNvbmFsIGluZm9ybWF0aW9uIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPWlkZW50aXR5IiByZWw9Im5vZm9sbG93Ij5NeSBwZXJzb25hbCBpbmZvPC9hPjwvbGk+DQo8L3VsPg==";s:13:"htmlcontent_5";s:868:"PHVsIGNsYXNzPSJsaXN0LWJsb2NrIGxpc3QtZ3JvdXAgbm9zdHlsZSI+DQo8bGk+PGEgdGl0bGU9Ik15IG9yZGVycyIgaHJlZj0iaHR0cDovL2xvY2FsaG9zdC9wcmVzdGFzaG9wL3BmX21peG1vYmlsZS9pbmRleC5waHA/Y29udHJvbGxlcj1oaXN0b3J5IiByZWw9Im5vZm9sbG93Ij5NeSBvcmRlcnM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTXkgY3JlZGl0IHNsaXBzIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPW9yZGVyLXNsaXAiIHJlbD0ibm9mb2xsb3ciPk15IGNyZWRpdCBzbGlwczwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJNeSBhZGRyZXNzZXMiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9YWRkcmVzc2VzIiByZWw9Im5vZm9sbG93Ij5NeSBhZGRyZXNzZXM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTWFuYWdlIG15IHBlcnNvbmFsIGluZm9ybWF0aW9uIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPWlkZW50aXR5IiByZWw9Im5vZm9sbG93Ij5NeSBwZXJzb25hbCBpbmZvPC9hPjwvbGk+DQo8L3VsPg==";}s:4:"wkey";s:17:"key_1414831468547";}";}s:17:"key_1414831529453";a:1:{s:6:"config";s:1594:"a:2:{s:6:"widget";a:20:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzE1Mjk0NTM=";s:5:"wtype";s:16:"bmV3c2xldHRlcg==";s:11:"widget_name";s:32:"U2lnbiBVcCBGb3IgTmV3c2xldHRlcg==";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:32:"U2lnbiBVcCBGb3IgTmV3c2xldHRlcg==";s:14:"widget_title_2";s:32:"U2lnbiBVcCBGb3IgTmV3c2xldHRlcg==";s:14:"widget_title_3";s:32:"U2lnbiBVcCBGb3IgTmV3c2xldHRlcg==";s:14:"widget_title_4";s:32:"U2lnbiBVcCBGb3IgTmV3c2xldHRlcg==";s:14:"widget_title_5";s:32:"U2lnbiBVcCBGb3IgTmV3c2xldHRlcg==";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:5:"class";s:20:"cHRzLW5ld3NsZXR0ZXI=";s:9:"imagefile";s:0:"";s:13:"information_1";s:144:"PHA+U2lnbiB1cCB0byBvdXIgbmV3c2xldHRlciBhbmQgZ2V0IGV4Y2x1c2l2ZSBkZWFscyB5b3Ugd29udCBmaW5kIGFueXdoZXJlIGVsc2Ugc3RyYWlnaHQgdG8geW91ciBpbmJveCE8L3A+";s:13:"information_2";s:144:"PHA+U2lnbiB1cCB0byBvdXIgbmV3c2xldHRlciBhbmQgZ2V0IGV4Y2x1c2l2ZSBkZWFscyB5b3Ugd29udCBmaW5kIGFueXdoZXJlIGVsc2Ugc3RyYWlnaHQgdG8geW91ciBpbmJveCE8L3A+";s:13:"information_3";s:144:"PHA+U2lnbiB1cCB0byBvdXIgbmV3c2xldHRlciBhbmQgZ2V0IGV4Y2x1c2l2ZSBkZWFscyB5b3Ugd29udCBmaW5kIGFueXdoZXJlIGVsc2Ugc3RyYWlnaHQgdG8geW91ciBpbmJveCE8L3A+";s:13:"information_4";s:144:"PHA+U2lnbiB1cCB0byBvdXIgbmV3c2xldHRlciBhbmQgZ2V0IGV4Y2x1c2l2ZSBkZWFscyB5b3Ugd29udCBmaW5kIGFueXdoZXJlIGVsc2Ugc3RyYWlnaHQgdG8geW91ciBpbmJveCE8L3A+";s:13:"information_5";s:144:"PHA+U2lnbiB1cCB0byBvdXIgbmV3c2xldHRlciBhbmQgZ2V0IGV4Y2x1c2l2ZSBkZWFscyB5b3Ugd29udCBmaW5kIGFueXdoZXJlIGVsc2Ugc3RyYWlnaHQgdG8geW91ciBpbmJveCE8L3A+";s:16:"newsletter_style";s:8:"c3R5bGUx";}s:4:"wkey";s:17:"key_1414831529453";}";}s:17:"key_1414831611780";a:1:{s:6:"config";s:5467:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzE2MTE3ODA=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:16:"SHRtbCBGb290ZXI=";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:16:"SHRtbCBGb290ZXI=";s:14:"widget_title_2";s:16:"SHRtbCBGb290ZXI=";s:14:"widget_title_3";s:16:"SHRtbCBGb290ZXI=";s:14:"widget_title_4";s:16:"SHRtbCBGb290ZXI=";s:14:"widget_title_5";s:16:"SHRtbCBGb290ZXI=";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:960:"PHVsIGNsYXNzPSJyb3cgIj4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14cy0xMiAiPg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxkaXYgY2xhc3M9ImN1c3RvbSBpbmZvLXNlcnZpY2VzIj4NCjxkaXYgY2xhc3M9ImluZm8tdGl0bGUgYm9yZGVyIHNtYWxsIj5jYWxsIHRoZSBjdXN0b21lciBzZXJ2aWNlczwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC1oaWdobGlnaHQiPjxzdHJvbmc+MTgwMCAtIDE1NzAgLSAwMDA8L3N0cm9uZz48L2Rpdj4NCjxkaXYgY2xhc3M9IiBib3JkZXIgc21hbGwiPkZyZWUgMjQvNyBzdXBwb3J0IGZvciBvdXIgY3VzdG9tZXJzPC9kaXY+DQo8L2Rpdj4NCjwvZGl2Pg0KPC9saT4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14czEyIj4NCjxkaXYgY2xhc3M9Iml0ZW0taHRtbCI+DQo8ZGl2IGNsYXNzPSJjdXN0b20gaW5mby1xdWVzdGlvbiI+DQo8ZGl2IGNsYXNzPSJpbmZvLXRpdGxlIGJvcmRlciBzbWFsbCI+ZW1haWwgdXMgeW91ciBxdWVzdGlvbjwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC13YXJuaW5nIj48c3Ryb25nPnN1cHBvcnRAbWl4c3RvcmUuY29tPC9zdHJvbmc+PC9kaXY+DQo8ZGl2IGNsYXNzPSIgYm9yZGVyIHNtYWxsIj5XZSB3aWxsIHJlc3BvbmQgd2l0aGluIDMgYnVzaW5lc3MgZGF5czwvZGl2Pg0KPC9kaXY+DQo8L2Rpdj4NCjwvbGk+DQo8L3VsPg==";s:13:"htmlcontent_2";s:960:"PHVsIGNsYXNzPSJyb3cgIj4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14cy0xMiAiPg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxkaXYgY2xhc3M9ImN1c3RvbSBpbmZvLXNlcnZpY2VzIj4NCjxkaXYgY2xhc3M9ImluZm8tdGl0bGUgYm9yZGVyIHNtYWxsIj5jYWxsIHRoZSBjdXN0b21lciBzZXJ2aWNlczwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC1oaWdobGlnaHQiPjxzdHJvbmc+MTgwMCAtIDE1NzAgLSAwMDA8L3N0cm9uZz48L2Rpdj4NCjxkaXYgY2xhc3M9IiBib3JkZXIgc21hbGwiPkZyZWUgMjQvNyBzdXBwb3J0IGZvciBvdXIgY3VzdG9tZXJzPC9kaXY+DQo8L2Rpdj4NCjwvZGl2Pg0KPC9saT4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14czEyIj4NCjxkaXYgY2xhc3M9Iml0ZW0taHRtbCI+DQo8ZGl2IGNsYXNzPSJjdXN0b20gaW5mby1xdWVzdGlvbiI+DQo8ZGl2IGNsYXNzPSJpbmZvLXRpdGxlIGJvcmRlciBzbWFsbCI+ZW1haWwgdXMgeW91ciBxdWVzdGlvbjwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC13YXJuaW5nIj48c3Ryb25nPnN1cHBvcnRAbWl4c3RvcmUuY29tPC9zdHJvbmc+PC9kaXY+DQo8ZGl2IGNsYXNzPSIgYm9yZGVyIHNtYWxsIj5XZSB3aWxsIHJlc3BvbmQgd2l0aGluIDMgYnVzaW5lc3MgZGF5czwvZGl2Pg0KPC9kaXY+DQo8L2Rpdj4NCjwvbGk+DQo8L3VsPg==";s:13:"htmlcontent_3";s:960:"PHVsIGNsYXNzPSJyb3cgIj4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14cy0xMiAiPg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxkaXYgY2xhc3M9ImN1c3RvbSBpbmZvLXNlcnZpY2VzIj4NCjxkaXYgY2xhc3M9ImluZm8tdGl0bGUgYm9yZGVyIHNtYWxsIj5jYWxsIHRoZSBjdXN0b21lciBzZXJ2aWNlczwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC1oaWdobGlnaHQiPjxzdHJvbmc+MTgwMCAtIDE1NzAgLSAwMDA8L3N0cm9uZz48L2Rpdj4NCjxkaXYgY2xhc3M9IiBib3JkZXIgc21hbGwiPkZyZWUgMjQvNyBzdXBwb3J0IGZvciBvdXIgY3VzdG9tZXJzPC9kaXY+DQo8L2Rpdj4NCjwvZGl2Pg0KPC9saT4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14czEyIj4NCjxkaXYgY2xhc3M9Iml0ZW0taHRtbCI+DQo8ZGl2IGNsYXNzPSJjdXN0b20gaW5mby1xdWVzdGlvbiI+DQo8ZGl2IGNsYXNzPSJpbmZvLXRpdGxlIGJvcmRlciBzbWFsbCI+ZW1haWwgdXMgeW91ciBxdWVzdGlvbjwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC13YXJuaW5nIj48c3Ryb25nPnN1cHBvcnRAbWl4c3RvcmUuY29tPC9zdHJvbmc+PC9kaXY+DQo8ZGl2IGNsYXNzPSIgYm9yZGVyIHNtYWxsIj5XZSB3aWxsIHJlc3BvbmQgd2l0aGluIDMgYnVzaW5lc3MgZGF5czwvZGl2Pg0KPC9kaXY+DQo8L2Rpdj4NCjwvbGk+DQo8L3VsPg==";s:13:"htmlcontent_4";s:960:"PHVsIGNsYXNzPSJyb3cgIj4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14cy0xMiAiPg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxkaXYgY2xhc3M9ImN1c3RvbSBpbmZvLXNlcnZpY2VzIj4NCjxkaXYgY2xhc3M9ImluZm8tdGl0bGUgYm9yZGVyIHNtYWxsIj5jYWxsIHRoZSBjdXN0b21lciBzZXJ2aWNlczwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC1oaWdobGlnaHQiPjxzdHJvbmc+MTgwMCAtIDE1NzAgLSAwMDA8L3N0cm9uZz48L2Rpdj4NCjxkaXYgY2xhc3M9IiBib3JkZXIgc21hbGwiPkZyZWUgMjQvNyBzdXBwb3J0IGZvciBvdXIgY3VzdG9tZXJzPC9kaXY+DQo8L2Rpdj4NCjwvZGl2Pg0KPC9saT4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14czEyIj4NCjxkaXYgY2xhc3M9Iml0ZW0taHRtbCI+DQo8ZGl2IGNsYXNzPSJjdXN0b20gaW5mby1xdWVzdGlvbiI+DQo8ZGl2IGNsYXNzPSJpbmZvLXRpdGxlIGJvcmRlciBzbWFsbCI+ZW1haWwgdXMgeW91ciBxdWVzdGlvbjwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC13YXJuaW5nIj48c3Ryb25nPnN1cHBvcnRAbWl4c3RvcmUuY29tPC9zdHJvbmc+PC9kaXY+DQo8ZGl2IGNsYXNzPSIgYm9yZGVyIHNtYWxsIj5XZSB3aWxsIHJlc3BvbmQgd2l0aGluIDMgYnVzaW5lc3MgZGF5czwvZGl2Pg0KPC9kaXY+DQo8L2Rpdj4NCjwvbGk+DQo8L3VsPg==";s:13:"htmlcontent_5";s:960:"PHVsIGNsYXNzPSJyb3cgIj4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14cy0xMiAiPg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxkaXYgY2xhc3M9ImN1c3RvbSBpbmZvLXNlcnZpY2VzIj4NCjxkaXYgY2xhc3M9ImluZm8tdGl0bGUgYm9yZGVyIHNtYWxsIj5jYWxsIHRoZSBjdXN0b21lciBzZXJ2aWNlczwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC1oaWdobGlnaHQiPjxzdHJvbmc+MTgwMCAtIDE1NzAgLSAwMDA8L3N0cm9uZz48L2Rpdj4NCjxkaXYgY2xhc3M9IiBib3JkZXIgc21hbGwiPkZyZWUgMjQvNyBzdXBwb3J0IGZvciBvdXIgY3VzdG9tZXJzPC9kaXY+DQo8L2Rpdj4NCjwvZGl2Pg0KPC9saT4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14czEyIj4NCjxkaXYgY2xhc3M9Iml0ZW0taHRtbCI+DQo8ZGl2IGNsYXNzPSJjdXN0b20gaW5mby1xdWVzdGlvbiI+DQo8ZGl2IGNsYXNzPSJpbmZvLXRpdGxlIGJvcmRlciBzbWFsbCI+ZW1haWwgdXMgeW91ciBxdWVzdGlvbjwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC13YXJuaW5nIj48c3Ryb25nPnN1cHBvcnRAbWl4c3RvcmUuY29tPC9zdHJvbmc+PC9kaXY+DQo8ZGl2IGNsYXNzPSIgYm9yZGVyIHNtYWxsIj5XZSB3aWxsIHJlc3BvbmQgd2l0aGluIDMgYnVzaW5lc3MgZGF5czwvZGl2Pg0KPC9kaXY+DQo8L2Rpdj4NCjwvbGk+DQo8L3VsPg==";}s:4:"wkey";s:17:"key_1414831611780";}";}}', 17, 0, 1, 1414831728),
	('FooterGifts', 0, '', '[{"index":0,"cls":"pts-footer-top","bgcolor":"#f3f3f3","bgimage":"","fullwidth":"0","parallax":"0","sfxcls":null,"cols":[{"index":0,"cls":"hidden-xs","sfxcls":null,"bgcolor":"","bgimage":"","inrow":0,"lgcol":9,"mdcol":8,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831113755","name":"Logo Footer"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":3,"mdcol":4,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831222604","name":"Social footer"}],"rows":[]}]},{"index":0,"cls":"pts-footer-center","bgcolor":"#333333","bgimage":"","fullwidth":"0","parallax":"0","sfxcls":null,"cols":[{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":3,"mdcol":3,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831295069","name":"Contact info"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":2,"mdcol":2,"smcol":4,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831349733","name":"Customer Service"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":2,"mdcol":2,"smcol":4,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831415734","name":"Information"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":2,"mdcol":2,"smcol":4,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831468547","name":"My account"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":3,"mdcol":3,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"newsletter","wkey":"key_1414831529453","name":"Sign Up For Newsletter"}],"rows":[]}]},{"index":0,"cls":"hidden-xs","bgcolor":"","bgimage":"bg-footerbottom-cd.jpg","fullwidth":"0","parallax":"1","sfxcls":null,"cols":[{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":12,"mdcol":12,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831611780","name":"Html Footer"}],"rows":[]}]}]', 'a:8:{s:17:"key_1414831113755";a:1:{s:6:"config";s:2547:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzExMTM3NTU=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:16:"TG9nbyBGb290ZXI=";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:16:"TG9nbyBGb290ZXI=";s:14:"widget_title_2";s:16:"TG9nbyBGb290ZXI=";s:14:"widget_title_3";s:16:"TG9nbyBGb290ZXI=";s:14:"widget_title_4";s:16:"TG9nbyBGb290ZXI=";s:14:"widget_title_5";s:16:"TG9nbyBGb290ZXI=";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:328:"PGRpdiBjbGFzcz0iaW1hZ2UtYmFubmVyICBwdWxsLWxlZnQiPjxpbWcgY2xhc3M9Iml0ZW0taW1nIiBzcmM9Im1vZHVsZXMvcHRzcGFnZWJ1aWxkZXIvaW1hZ2VzL2xvZ28tZm9vdGVyLnBuZyIgYWx0PSIiIC8+PC9kaXY+DQo8ZGl2IGNsYXNzPSJpdGVtLWh0bWwiPg0KPHA+V2XigJlyZSBjb25maWRlbnQgd2XigJl2ZSBwcm92aWRlZCBhbGwgdGhlIGJlc3QgZm9yIHlvdS4gU3RheSBjb25uZWVjdCB3aXRoIHVzPC9wPg0KPC9kaXY+";s:13:"htmlcontent_2";s:388:"PGRpdiBjbGFzcz0iaW1hZ2UtYmFubmVyICBwdWxsLWxlZnQiPjxpbWcgY2xhc3M9Iml0ZW0taW1nIiBzcmM9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW1nL2Ntcy9sb2dvLWZvb3Rlci5wbmciIGFsdD0iIiB3aWR0aD0iMTMxIiBoZWlnaHQ9IjQwIiAvPjwvZGl2Pg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxwPldl4oCZcmUgY29uZmlkZW50IHdl4oCZdmUgcHJvdmlkZWQgYWxsIHRoZSBiZXN0IGZvciB5b3UuIFN0YXkgY29ubmVlY3Qgd2l0aCB1czwvcD4NCjwvZGl2Pg==";s:13:"htmlcontent_3";s:388:"PGRpdiBjbGFzcz0iaW1hZ2UtYmFubmVyICBwdWxsLWxlZnQiPjxpbWcgY2xhc3M9Iml0ZW0taW1nIiBzcmM9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW1nL2Ntcy9sb2dvLWZvb3Rlci5wbmciIGFsdD0iIiB3aWR0aD0iMTMxIiBoZWlnaHQ9IjQwIiAvPjwvZGl2Pg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxwPldl4oCZcmUgY29uZmlkZW50IHdl4oCZdmUgcHJvdmlkZWQgYWxsIHRoZSBiZXN0IGZvciB5b3UuIFN0YXkgY29ubmVlY3Qgd2l0aCB1czwvcD4NCjwvZGl2Pg==";s:13:"htmlcontent_4";s:388:"PGRpdiBjbGFzcz0iaW1hZ2UtYmFubmVyICBwdWxsLWxlZnQiPjxpbWcgY2xhc3M9Iml0ZW0taW1nIiBzcmM9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW1nL2Ntcy9sb2dvLWZvb3Rlci5wbmciIGFsdD0iIiB3aWR0aD0iMTMxIiBoZWlnaHQ9IjQwIiAvPjwvZGl2Pg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxwPldl4oCZcmUgY29uZmlkZW50IHdl4oCZdmUgcHJvdmlkZWQgYWxsIHRoZSBiZXN0IGZvciB5b3UuIFN0YXkgY29ubmVlY3Qgd2l0aCB1czwvcD4NCjwvZGl2Pg==";s:13:"htmlcontent_5";s:388:"PGRpdiBjbGFzcz0iaW1hZ2UtYmFubmVyICBwdWxsLWxlZnQiPjxpbWcgY2xhc3M9Iml0ZW0taW1nIiBzcmM9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW1nL2Ntcy9sb2dvLWZvb3Rlci5wbmciIGFsdD0iIiB3aWR0aD0iMTMxIiBoZWlnaHQ9IjQwIiAvPjwvZGl2Pg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxwPldl4oCZcmUgY29uZmlkZW50IHdl4oCZdmUgcHJvdmlkZWQgYWxsIHRoZSBiZXN0IGZvciB5b3UuIFN0YXkgY29ubmVlY3Qgd2l0aCB1czwvcD4NCjwvZGl2Pg==";}s:4:"wkey";s:17:"key_1414831113755";}";}s:17:"key_1414831222604";a:1:{s:6:"config";s:6557:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzEyMjI2MDQ=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:20:"U29jaWFsIGZvb3Rlcg==";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:20:"U29jaWFsIGZvb3Rlcg==";s:14:"widget_title_2";s:20:"U29jaWFsIGZvb3Rlcg==";s:14:"widget_title_3";s:20:"U29jaWFsIGZvb3Rlcg==";s:14:"widget_title_4";s:20:"U29jaWFsIGZvb3Rlcg==";s:14:"widget_title_5";s:20:"U29jaWFsIGZvb3Rlcg==";s:12:"addition_cls";s:20:"c29jaWFsLWZvb3Rlcg==";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:1168:"PGRpdiBpZD0ic29jaWFsX2Jsb2NrIj4NCjx1bCBjbGFzcz0ic29jaWFsIj4NCjxsaSBjbGFzcz0iZmFjZWJvb2siPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy5mYWNlYm9vay5jb20vcHJlc3Rhc2hvcCIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IkZhY2Vib29rIj4gRmFjZWJvb2sgPC9hPjwvbGk+DQo8bGkgY2xhc3M9InR3aXR0ZXIiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy50d2l0dGVyLmNvbS9wcmVzdGFzaG9wIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iVHdpdHRlciI+VHdpdHRlciA8L2E+PC9saT4NCjxsaSBjbGFzcz0icnNzIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9Imh0dHA6Ly93d3cucHJlc3Rhc2hvcC5jb20vYmxvZy9lbi9mZWVkLyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlJTUyI+IFJTUzwvYT48L2xpPg0KPGxpIGNsYXNzPSJ5b3V0dWJlIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9IiMiIHRhcmdldD0iX2JsYW5rIiBkYXRhLW9yaWdpbmFsLXRpdGxlPSJZb3V0dWJlIj4gWW91dHViZTwvYT48L2xpPg0KPGxpIGNsYXNzPSJnb29nbGUtcGx1cyI+PGEgY2xhc3M9ImJ0bi10b29sdGlwIiBocmVmPSIjIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iR29vZ2xlIFBsdXMiPkdvb2dsZSBQbHVzIDwvYT48L2xpPg0KPGxpIGNsYXNzPSJwaW50ZXJlc3QiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iIyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlBpbnRlcmVzdCI+IFBpbnRlcmVzdDwvYT48L2xpPg0KPC91bD4NCjwvZGl2Pg==";s:13:"htmlcontent_2";s:1168:"PGRpdiBpZD0ic29jaWFsX2Jsb2NrIj4NCjx1bCBjbGFzcz0ic29jaWFsIj4NCjxsaSBjbGFzcz0iZmFjZWJvb2siPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy5mYWNlYm9vay5jb20vcHJlc3Rhc2hvcCIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IkZhY2Vib29rIj4gRmFjZWJvb2sgPC9hPjwvbGk+DQo8bGkgY2xhc3M9InR3aXR0ZXIiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy50d2l0dGVyLmNvbS9wcmVzdGFzaG9wIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iVHdpdHRlciI+VHdpdHRlciA8L2E+PC9saT4NCjxsaSBjbGFzcz0icnNzIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9Imh0dHA6Ly93d3cucHJlc3Rhc2hvcC5jb20vYmxvZy9lbi9mZWVkLyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlJTUyI+IFJTUzwvYT48L2xpPg0KPGxpIGNsYXNzPSJ5b3V0dWJlIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9IiMiIHRhcmdldD0iX2JsYW5rIiBkYXRhLW9yaWdpbmFsLXRpdGxlPSJZb3V0dWJlIj4gWW91dHViZTwvYT48L2xpPg0KPGxpIGNsYXNzPSJnb29nbGUtcGx1cyI+PGEgY2xhc3M9ImJ0bi10b29sdGlwIiBocmVmPSIjIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iR29vZ2xlIFBsdXMiPkdvb2dsZSBQbHVzIDwvYT48L2xpPg0KPGxpIGNsYXNzPSJwaW50ZXJlc3QiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iIyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlBpbnRlcmVzdCI+IFBpbnRlcmVzdDwvYT48L2xpPg0KPC91bD4NCjwvZGl2Pg==";s:13:"htmlcontent_3";s:1168:"PGRpdiBpZD0ic29jaWFsX2Jsb2NrIj4NCjx1bCBjbGFzcz0ic29jaWFsIj4NCjxsaSBjbGFzcz0iZmFjZWJvb2siPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy5mYWNlYm9vay5jb20vcHJlc3Rhc2hvcCIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IkZhY2Vib29rIj4gRmFjZWJvb2sgPC9hPjwvbGk+DQo8bGkgY2xhc3M9InR3aXR0ZXIiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy50d2l0dGVyLmNvbS9wcmVzdGFzaG9wIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iVHdpdHRlciI+VHdpdHRlciA8L2E+PC9saT4NCjxsaSBjbGFzcz0icnNzIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9Imh0dHA6Ly93d3cucHJlc3Rhc2hvcC5jb20vYmxvZy9lbi9mZWVkLyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlJTUyI+IFJTUzwvYT48L2xpPg0KPGxpIGNsYXNzPSJ5b3V0dWJlIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9IiMiIHRhcmdldD0iX2JsYW5rIiBkYXRhLW9yaWdpbmFsLXRpdGxlPSJZb3V0dWJlIj4gWW91dHViZTwvYT48L2xpPg0KPGxpIGNsYXNzPSJnb29nbGUtcGx1cyI+PGEgY2xhc3M9ImJ0bi10b29sdGlwIiBocmVmPSIjIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iR29vZ2xlIFBsdXMiPkdvb2dsZSBQbHVzIDwvYT48L2xpPg0KPGxpIGNsYXNzPSJwaW50ZXJlc3QiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iIyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlBpbnRlcmVzdCI+IFBpbnRlcmVzdDwvYT48L2xpPg0KPC91bD4NCjwvZGl2Pg==";s:13:"htmlcontent_4";s:1168:"PGRpdiBpZD0ic29jaWFsX2Jsb2NrIj4NCjx1bCBjbGFzcz0ic29jaWFsIj4NCjxsaSBjbGFzcz0iZmFjZWJvb2siPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy5mYWNlYm9vay5jb20vcHJlc3Rhc2hvcCIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IkZhY2Vib29rIj4gRmFjZWJvb2sgPC9hPjwvbGk+DQo8bGkgY2xhc3M9InR3aXR0ZXIiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy50d2l0dGVyLmNvbS9wcmVzdGFzaG9wIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iVHdpdHRlciI+VHdpdHRlciA8L2E+PC9saT4NCjxsaSBjbGFzcz0icnNzIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9Imh0dHA6Ly93d3cucHJlc3Rhc2hvcC5jb20vYmxvZy9lbi9mZWVkLyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlJTUyI+IFJTUzwvYT48L2xpPg0KPGxpIGNsYXNzPSJ5b3V0dWJlIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9IiMiIHRhcmdldD0iX2JsYW5rIiBkYXRhLW9yaWdpbmFsLXRpdGxlPSJZb3V0dWJlIj4gWW91dHViZTwvYT48L2xpPg0KPGxpIGNsYXNzPSJnb29nbGUtcGx1cyI+PGEgY2xhc3M9ImJ0bi10b29sdGlwIiBocmVmPSIjIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iR29vZ2xlIFBsdXMiPkdvb2dsZSBQbHVzIDwvYT48L2xpPg0KPGxpIGNsYXNzPSJwaW50ZXJlc3QiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iIyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlBpbnRlcmVzdCI+IFBpbnRlcmVzdDwvYT48L2xpPg0KPC91bD4NCjwvZGl2Pg==";s:13:"htmlcontent_5";s:1168:"PGRpdiBpZD0ic29jaWFsX2Jsb2NrIj4NCjx1bCBjbGFzcz0ic29jaWFsIj4NCjxsaSBjbGFzcz0iZmFjZWJvb2siPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy5mYWNlYm9vay5jb20vcHJlc3Rhc2hvcCIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IkZhY2Vib29rIj4gRmFjZWJvb2sgPC9hPjwvbGk+DQo8bGkgY2xhc3M9InR3aXR0ZXIiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy50d2l0dGVyLmNvbS9wcmVzdGFzaG9wIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iVHdpdHRlciI+VHdpdHRlciA8L2E+PC9saT4NCjxsaSBjbGFzcz0icnNzIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9Imh0dHA6Ly93d3cucHJlc3Rhc2hvcC5jb20vYmxvZy9lbi9mZWVkLyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlJTUyI+IFJTUzwvYT48L2xpPg0KPGxpIGNsYXNzPSJ5b3V0dWJlIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9IiMiIHRhcmdldD0iX2JsYW5rIiBkYXRhLW9yaWdpbmFsLXRpdGxlPSJZb3V0dWJlIj4gWW91dHViZTwvYT48L2xpPg0KPGxpIGNsYXNzPSJnb29nbGUtcGx1cyI+PGEgY2xhc3M9ImJ0bi10b29sdGlwIiBocmVmPSIjIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iR29vZ2xlIFBsdXMiPkdvb2dsZSBQbHVzIDwvYT48L2xpPg0KPGxpIGNsYXNzPSJwaW50ZXJlc3QiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iIyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlBpbnRlcmVzdCI+IFBpbnRlcmVzdDwvYT48L2xpPg0KPC91bD4NCjwvZGl2Pg==";}s:4:"wkey";s:17:"key_1414831222604";}";}s:17:"key_1414831295069";a:1:{s:6:"config";s:4247:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzEyOTUwNjk=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:16:"Q29udGFjdCBpbmZv";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:16:"Q29udGFjdCBpbmZv";s:14:"widget_title_2";s:16:"Q29udGFjdCBpbmZv";s:14:"widget_title_3";s:16:"Q29udGFjdCBpbmZv";s:14:"widget_title_4";s:16:"Q29udGFjdCBpbmZv";s:14:"widget_title_5";s:16:"Q29udGFjdCBpbmZv";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:716:"PGRpdiBjbGFzcz0ibGlzdC1ncm91cCI+U2VkIG5vbiBtYXVyaXMgdml0YWUgZXJhdCBjb25zZXF1YXQgYXVjdG9yIGV1IGluIGVsaXQuIENsYXNzIGFwdGVudCB0YWNpdGkgc29zcXUgYWQgbGl0b3JhIHRvcnF1ZW50IHBlciBjb251YmlhPC9kaXY+DQo8dWwgY2xhc3M9Imxpc3QtZ3JvdXAiPg0KPGxpPk5vIDExMDQgU2t5IFRvd2VyLCBOZXd5b3JrLCBVU0E8L2xpPg0KPGxpPkNhbGwgdXMgbm93OiAwMTIzLTQ1Ni03ODk8L2xpPg0KPGxpPkVtYWlsOiA8YSBocmVmPSJtYWlsdG86JTczJTYxJTZjJTY1JTczQCU3OSU2ZiU3NSU3MiU2MyU2ZiU2ZCU3MCU2MSU2ZSU3OS4lNjMlNmYlNmQiPnNhbGVzQHlvdXJjb21wYW55LmNvbTwvYT48L2xpPg0KPC91bD4NCjxkaXYgY2xhc3M9Im1hcCI+PGEgaWQ9ImZhbmN5Ym94LW1hcCIgdGl0bGU9IlZpZXdNYXAiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9Y29udGFjdCZjb250ZW50X29ubHk9MSI+Vmlld01hcCA8L2E+PC9kaXY+";s:13:"htmlcontent_2";s:716:"PGRpdiBjbGFzcz0ibGlzdC1ncm91cCI+U2VkIG5vbiBtYXVyaXMgdml0YWUgZXJhdCBjb25zZXF1YXQgYXVjdG9yIGV1IGluIGVsaXQuIENsYXNzIGFwdGVudCB0YWNpdGkgc29zcXUgYWQgbGl0b3JhIHRvcnF1ZW50IHBlciBjb251YmlhPC9kaXY+DQo8dWwgY2xhc3M9Imxpc3QtZ3JvdXAiPg0KPGxpPk5vIDExMDQgU2t5IFRvd2VyLCBOZXd5b3JrLCBVU0E8L2xpPg0KPGxpPkNhbGwgdXMgbm93OiAwMTIzLTQ1Ni03ODk8L2xpPg0KPGxpPkVtYWlsOiA8YSBocmVmPSJtYWlsdG86JTczJTYxJTZjJTY1JTczQCU3OSU2ZiU3NSU3MiU2MyU2ZiU2ZCU3MCU2MSU2ZSU3OS4lNjMlNmYlNmQiPnNhbGVzQHlvdXJjb21wYW55LmNvbTwvYT48L2xpPg0KPC91bD4NCjxkaXYgY2xhc3M9Im1hcCI+PGEgaWQ9ImZhbmN5Ym94LW1hcCIgdGl0bGU9IlZpZXdNYXAiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9Y29udGFjdCZjb250ZW50X29ubHk9MSI+Vmlld01hcCA8L2E+PC9kaXY+";s:13:"htmlcontent_3";s:716:"PGRpdiBjbGFzcz0ibGlzdC1ncm91cCI+U2VkIG5vbiBtYXVyaXMgdml0YWUgZXJhdCBjb25zZXF1YXQgYXVjdG9yIGV1IGluIGVsaXQuIENsYXNzIGFwdGVudCB0YWNpdGkgc29zcXUgYWQgbGl0b3JhIHRvcnF1ZW50IHBlciBjb251YmlhPC9kaXY+DQo8dWwgY2xhc3M9Imxpc3QtZ3JvdXAiPg0KPGxpPk5vIDExMDQgU2t5IFRvd2VyLCBOZXd5b3JrLCBVU0E8L2xpPg0KPGxpPkNhbGwgdXMgbm93OiAwMTIzLTQ1Ni03ODk8L2xpPg0KPGxpPkVtYWlsOiA8YSBocmVmPSJtYWlsdG86JTczJTYxJTZjJTY1JTczQCU3OSU2ZiU3NSU3MiU2MyU2ZiU2ZCU3MCU2MSU2ZSU3OS4lNjMlNmYlNmQiPnNhbGVzQHlvdXJjb21wYW55LmNvbTwvYT48L2xpPg0KPC91bD4NCjxkaXYgY2xhc3M9Im1hcCI+PGEgaWQ9ImZhbmN5Ym94LW1hcCIgdGl0bGU9IlZpZXdNYXAiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9Y29udGFjdCZjb250ZW50X29ubHk9MSI+Vmlld01hcCA8L2E+PC9kaXY+";s:13:"htmlcontent_4";s:716:"PGRpdiBjbGFzcz0ibGlzdC1ncm91cCI+U2VkIG5vbiBtYXVyaXMgdml0YWUgZXJhdCBjb25zZXF1YXQgYXVjdG9yIGV1IGluIGVsaXQuIENsYXNzIGFwdGVudCB0YWNpdGkgc29zcXUgYWQgbGl0b3JhIHRvcnF1ZW50IHBlciBjb251YmlhPC9kaXY+DQo8dWwgY2xhc3M9Imxpc3QtZ3JvdXAiPg0KPGxpPk5vIDExMDQgU2t5IFRvd2VyLCBOZXd5b3JrLCBVU0E8L2xpPg0KPGxpPkNhbGwgdXMgbm93OiAwMTIzLTQ1Ni03ODk8L2xpPg0KPGxpPkVtYWlsOiA8YSBocmVmPSJtYWlsdG86JTczJTYxJTZjJTY1JTczQCU3OSU2ZiU3NSU3MiU2MyU2ZiU2ZCU3MCU2MSU2ZSU3OS4lNjMlNmYlNmQiPnNhbGVzQHlvdXJjb21wYW55LmNvbTwvYT48L2xpPg0KPC91bD4NCjxkaXYgY2xhc3M9Im1hcCI+PGEgaWQ9ImZhbmN5Ym94LW1hcCIgdGl0bGU9IlZpZXdNYXAiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9Y29udGFjdCZjb250ZW50X29ubHk9MSI+Vmlld01hcCA8L2E+PC9kaXY+";s:13:"htmlcontent_5";s:716:"PGRpdiBjbGFzcz0ibGlzdC1ncm91cCI+U2VkIG5vbiBtYXVyaXMgdml0YWUgZXJhdCBjb25zZXF1YXQgYXVjdG9yIGV1IGluIGVsaXQuIENsYXNzIGFwdGVudCB0YWNpdGkgc29zcXUgYWQgbGl0b3JhIHRvcnF1ZW50IHBlciBjb251YmlhPC9kaXY+DQo8dWwgY2xhc3M9Imxpc3QtZ3JvdXAiPg0KPGxpPk5vIDExMDQgU2t5IFRvd2VyLCBOZXd5b3JrLCBVU0E8L2xpPg0KPGxpPkNhbGwgdXMgbm93OiAwMTIzLTQ1Ni03ODk8L2xpPg0KPGxpPkVtYWlsOiA8YSBocmVmPSJtYWlsdG86JTczJTYxJTZjJTY1JTczQCU3OSU2ZiU3NSU3MiU2MyU2ZiU2ZCU3MCU2MSU2ZSU3OS4lNjMlNmYlNmQiPnNhbGVzQHlvdXJjb21wYW55LmNvbTwvYT48L2xpPg0KPC91bD4NCjxkaXYgY2xhc3M9Im1hcCI+PGEgaWQ9ImZhbmN5Ym94LW1hcCIgdGl0bGU9IlZpZXdNYXAiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9Y29udGFjdCZjb250ZW50X29ubHk9MSI+Vmlld01hcCA8L2E+PC9kaXY+";}s:4:"wkey";s:17:"key_1414831295069";}";}s:17:"key_1414831349733";a:1:{s:6:"config";s:3535:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzEzNDk3MzM=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:24:"Q3VzdG9tZXIgU2VydmljZQ==";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:24:"Q3VzdG9tZXIgU2VydmljZQ==";s:14:"widget_title_2";s:24:"Q3VzdG9tZXIgU2VydmljZQ==";s:14:"widget_title_3";s:24:"Q3VzdG9tZXIgU2VydmljZQ==";s:14:"widget_title_4";s:24:"Q3VzdG9tZXIgU2VydmljZQ==";s:14:"widget_title_5";s:24:"Q3VzdG9tZXIgU2VydmljZQ==";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:564:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iT3VyIHN0b3JlcyIgaHJlZj0iaW5kZXgucGhwP2NvbnRyb2xsZXI9c3RvcmVzIj4gT3VyIHN0b3JlcyA8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IkNvbnRhY3QgdXMiIGhyZWY9ImluZGV4LnBocD9jb250cm9sbGVyPWNvbnRhY3QiPiBDb250YWN0IHVzIDwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJTaXRlbWFwIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zaXRlbWFwIj4gU2l0ZW1hcCA8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iVmlldyBhIGxpc3Qgb2Ygc3VwcGxpZXJzIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zdXBwbGllciI+IFN1cHBsaWVycyA8L2E+PC9saT4NCjwvdWw+";s:13:"htmlcontent_2";s:564:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iT3VyIHN0b3JlcyIgaHJlZj0iaW5kZXgucGhwP2NvbnRyb2xsZXI9c3RvcmVzIj4gT3VyIHN0b3JlcyA8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IkNvbnRhY3QgdXMiIGhyZWY9ImluZGV4LnBocD9jb250cm9sbGVyPWNvbnRhY3QiPiBDb250YWN0IHVzIDwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJTaXRlbWFwIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zaXRlbWFwIj4gU2l0ZW1hcCA8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iVmlldyBhIGxpc3Qgb2Ygc3VwcGxpZXJzIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zdXBwbGllciI+IFN1cHBsaWVycyA8L2E+PC9saT4NCjwvdWw+";s:13:"htmlcontent_3";s:564:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iT3VyIHN0b3JlcyIgaHJlZj0iaW5kZXgucGhwP2NvbnRyb2xsZXI9c3RvcmVzIj4gT3VyIHN0b3JlcyA8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IkNvbnRhY3QgdXMiIGhyZWY9ImluZGV4LnBocD9jb250cm9sbGVyPWNvbnRhY3QiPiBDb250YWN0IHVzIDwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJTaXRlbWFwIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zaXRlbWFwIj4gU2l0ZW1hcCA8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iVmlldyBhIGxpc3Qgb2Ygc3VwcGxpZXJzIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zdXBwbGllciI+IFN1cHBsaWVycyA8L2E+PC9saT4NCjwvdWw+";s:13:"htmlcontent_4";s:564:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iT3VyIHN0b3JlcyIgaHJlZj0iaW5kZXgucGhwP2NvbnRyb2xsZXI9c3RvcmVzIj4gT3VyIHN0b3JlcyA8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IkNvbnRhY3QgdXMiIGhyZWY9ImluZGV4LnBocD9jb250cm9sbGVyPWNvbnRhY3QiPiBDb250YWN0IHVzIDwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJTaXRlbWFwIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zaXRlbWFwIj4gU2l0ZW1hcCA8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iVmlldyBhIGxpc3Qgb2Ygc3VwcGxpZXJzIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zdXBwbGllciI+IFN1cHBsaWVycyA8L2E+PC9saT4NCjwvdWw+";s:13:"htmlcontent_5";s:564:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iT3VyIHN0b3JlcyIgaHJlZj0iaW5kZXgucGhwP2NvbnRyb2xsZXI9c3RvcmVzIj4gT3VyIHN0b3JlcyA8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IkNvbnRhY3QgdXMiIGhyZWY9ImluZGV4LnBocD9jb250cm9sbGVyPWNvbnRhY3QiPiBDb250YWN0IHVzIDwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJTaXRlbWFwIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zaXRlbWFwIj4gU2l0ZW1hcCA8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iVmlldyBhIGxpc3Qgb2Ygc3VwcGxpZXJzIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zdXBwbGllciI+IFN1cHBsaWVycyA8L2E+PC9saT4NCjwvdWw+";}s:4:"wkey";s:17:"key_1414831349733";}";}s:17:"key_1414831415734";a:1:{s:6:"config";s:3187:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzE0MTU3MzQ=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:16:"SW5mb3JtYXRpb24=";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:16:"SW5mb3JtYXRpb24=";s:14:"widget_title_2";s:16:"SW5mb3JtYXRpb24=";s:14:"widget_title_3";s:16:"SW5mb3JtYXRpb24=";s:14:"widget_title_4";s:16:"SW5mb3JtYXRpb24=";s:14:"widget_title_5";s:16:"SW5mb3JtYXRpb24=";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:504:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj5NaSB2aXRhZSBtYWduaXM8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+IEZ1c2NlIGxhb3JlZXQ8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+VmVzdGlidSBmYXVjaWJ1czwvYT48L2xpPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj4gUG9ydHRpdG9yIGxhb3JlZXQgPC9hPjwvbGk+DQo8bGkgY2xhc3M9Iml0ZW0iPjxhIHRpdGxlPSIjIiBocmVmPSIjIj4gRmVsaXMgcG9ydHRpdG9yIDwvYT48L2xpPg0KPC91bD4=";s:13:"htmlcontent_2";s:504:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj5NaSB2aXRhZSBtYWduaXM8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+IEZ1c2NlIGxhb3JlZXQ8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+VmVzdGlidSBmYXVjaWJ1czwvYT48L2xpPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj4gUG9ydHRpdG9yIGxhb3JlZXQgPC9hPjwvbGk+DQo8bGkgY2xhc3M9Iml0ZW0iPjxhIHRpdGxlPSIjIiBocmVmPSIjIj4gRmVsaXMgcG9ydHRpdG9yIDwvYT48L2xpPg0KPC91bD4=";s:13:"htmlcontent_3";s:504:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj5NaSB2aXRhZSBtYWduaXM8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+IEZ1c2NlIGxhb3JlZXQ8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+VmVzdGlidSBmYXVjaWJ1czwvYT48L2xpPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj4gUG9ydHRpdG9yIGxhb3JlZXQgPC9hPjwvbGk+DQo8bGkgY2xhc3M9Iml0ZW0iPjxhIHRpdGxlPSIjIiBocmVmPSIjIj4gRmVsaXMgcG9ydHRpdG9yIDwvYT48L2xpPg0KPC91bD4=";s:13:"htmlcontent_4";s:504:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj5NaSB2aXRhZSBtYWduaXM8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+IEZ1c2NlIGxhb3JlZXQ8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+VmVzdGlidSBmYXVjaWJ1czwvYT48L2xpPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj4gUG9ydHRpdG9yIGxhb3JlZXQgPC9hPjwvbGk+DQo8bGkgY2xhc3M9Iml0ZW0iPjxhIHRpdGxlPSIjIiBocmVmPSIjIj4gRmVsaXMgcG9ydHRpdG9yIDwvYT48L2xpPg0KPC91bD4=";s:13:"htmlcontent_5";s:504:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj5NaSB2aXRhZSBtYWduaXM8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+IEZ1c2NlIGxhb3JlZXQ8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+VmVzdGlidSBmYXVjaWJ1czwvYT48L2xpPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj4gUG9ydHRpdG9yIGxhb3JlZXQgPC9hPjwvbGk+DQo8bGkgY2xhc3M9Iml0ZW0iPjxhIHRpdGxlPSIjIiBocmVmPSIjIj4gRmVsaXMgcG9ydHRpdG9yIDwvYT48L2xpPg0KPC91bD4=";}s:4:"wkey";s:17:"key_1414831415734";}";}s:17:"key_1414831468547";a:1:{s:6:"config";s:5007:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzE0Njg1NDc=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:16:"TXkgYWNjb3VudA==";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:16:"TXkgYWNjb3VudA==";s:14:"widget_title_2";s:16:"TXkgYWNjb3VudA==";s:14:"widget_title_3";s:16:"TXkgYWNjb3VudA==";s:14:"widget_title_4";s:16:"TXkgYWNjb3VudA==";s:14:"widget_title_5";s:16:"TXkgYWNjb3VudA==";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:868:"PHVsIGNsYXNzPSJsaXN0LWJsb2NrIGxpc3QtZ3JvdXAgbm9zdHlsZSI+DQo8bGk+PGEgdGl0bGU9Ik15IG9yZGVycyIgaHJlZj0iaHR0cDovL2xvY2FsaG9zdC9wcmVzdGFzaG9wL3BmX21peG1vYmlsZS9pbmRleC5waHA/Y29udHJvbGxlcj1oaXN0b3J5IiByZWw9Im5vZm9sbG93Ij5NeSBvcmRlcnM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTXkgY3JlZGl0IHNsaXBzIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPW9yZGVyLXNsaXAiIHJlbD0ibm9mb2xsb3ciPk15IGNyZWRpdCBzbGlwczwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJNeSBhZGRyZXNzZXMiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9YWRkcmVzc2VzIiByZWw9Im5vZm9sbG93Ij5NeSBhZGRyZXNzZXM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTWFuYWdlIG15IHBlcnNvbmFsIGluZm9ybWF0aW9uIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPWlkZW50aXR5IiByZWw9Im5vZm9sbG93Ij5NeSBwZXJzb25hbCBpbmZvPC9hPjwvbGk+DQo8L3VsPg==";s:13:"htmlcontent_2";s:868:"PHVsIGNsYXNzPSJsaXN0LWJsb2NrIGxpc3QtZ3JvdXAgbm9zdHlsZSI+DQo8bGk+PGEgdGl0bGU9Ik15IG9yZGVycyIgaHJlZj0iaHR0cDovL2xvY2FsaG9zdC9wcmVzdGFzaG9wL3BmX21peG1vYmlsZS9pbmRleC5waHA/Y29udHJvbGxlcj1oaXN0b3J5IiByZWw9Im5vZm9sbG93Ij5NeSBvcmRlcnM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTXkgY3JlZGl0IHNsaXBzIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPW9yZGVyLXNsaXAiIHJlbD0ibm9mb2xsb3ciPk15IGNyZWRpdCBzbGlwczwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJNeSBhZGRyZXNzZXMiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9YWRkcmVzc2VzIiByZWw9Im5vZm9sbG93Ij5NeSBhZGRyZXNzZXM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTWFuYWdlIG15IHBlcnNvbmFsIGluZm9ybWF0aW9uIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPWlkZW50aXR5IiByZWw9Im5vZm9sbG93Ij5NeSBwZXJzb25hbCBpbmZvPC9hPjwvbGk+DQo8L3VsPg==";s:13:"htmlcontent_3";s:868:"PHVsIGNsYXNzPSJsaXN0LWJsb2NrIGxpc3QtZ3JvdXAgbm9zdHlsZSI+DQo8bGk+PGEgdGl0bGU9Ik15IG9yZGVycyIgaHJlZj0iaHR0cDovL2xvY2FsaG9zdC9wcmVzdGFzaG9wL3BmX21peG1vYmlsZS9pbmRleC5waHA/Y29udHJvbGxlcj1oaXN0b3J5IiByZWw9Im5vZm9sbG93Ij5NeSBvcmRlcnM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTXkgY3JlZGl0IHNsaXBzIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPW9yZGVyLXNsaXAiIHJlbD0ibm9mb2xsb3ciPk15IGNyZWRpdCBzbGlwczwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJNeSBhZGRyZXNzZXMiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9YWRkcmVzc2VzIiByZWw9Im5vZm9sbG93Ij5NeSBhZGRyZXNzZXM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTWFuYWdlIG15IHBlcnNvbmFsIGluZm9ybWF0aW9uIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPWlkZW50aXR5IiByZWw9Im5vZm9sbG93Ij5NeSBwZXJzb25hbCBpbmZvPC9hPjwvbGk+DQo8L3VsPg==";s:13:"htmlcontent_4";s:868:"PHVsIGNsYXNzPSJsaXN0LWJsb2NrIGxpc3QtZ3JvdXAgbm9zdHlsZSI+DQo8bGk+PGEgdGl0bGU9Ik15IG9yZGVycyIgaHJlZj0iaHR0cDovL2xvY2FsaG9zdC9wcmVzdGFzaG9wL3BmX21peG1vYmlsZS9pbmRleC5waHA/Y29udHJvbGxlcj1oaXN0b3J5IiByZWw9Im5vZm9sbG93Ij5NeSBvcmRlcnM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTXkgY3JlZGl0IHNsaXBzIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPW9yZGVyLXNsaXAiIHJlbD0ibm9mb2xsb3ciPk15IGNyZWRpdCBzbGlwczwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJNeSBhZGRyZXNzZXMiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9YWRkcmVzc2VzIiByZWw9Im5vZm9sbG93Ij5NeSBhZGRyZXNzZXM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTWFuYWdlIG15IHBlcnNvbmFsIGluZm9ybWF0aW9uIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPWlkZW50aXR5IiByZWw9Im5vZm9sbG93Ij5NeSBwZXJzb25hbCBpbmZvPC9hPjwvbGk+DQo8L3VsPg==";s:13:"htmlcontent_5";s:868:"PHVsIGNsYXNzPSJsaXN0LWJsb2NrIGxpc3QtZ3JvdXAgbm9zdHlsZSI+DQo8bGk+PGEgdGl0bGU9Ik15IG9yZGVycyIgaHJlZj0iaHR0cDovL2xvY2FsaG9zdC9wcmVzdGFzaG9wL3BmX21peG1vYmlsZS9pbmRleC5waHA/Y29udHJvbGxlcj1oaXN0b3J5IiByZWw9Im5vZm9sbG93Ij5NeSBvcmRlcnM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTXkgY3JlZGl0IHNsaXBzIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPW9yZGVyLXNsaXAiIHJlbD0ibm9mb2xsb3ciPk15IGNyZWRpdCBzbGlwczwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJNeSBhZGRyZXNzZXMiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9YWRkcmVzc2VzIiByZWw9Im5vZm9sbG93Ij5NeSBhZGRyZXNzZXM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTWFuYWdlIG15IHBlcnNvbmFsIGluZm9ybWF0aW9uIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPWlkZW50aXR5IiByZWw9Im5vZm9sbG93Ij5NeSBwZXJzb25hbCBpbmZvPC9hPjwvbGk+DQo8L3VsPg==";}s:4:"wkey";s:17:"key_1414831468547";}";}s:17:"key_1414831529453";a:1:{s:6:"config";s:1594:"a:2:{s:6:"widget";a:20:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzE1Mjk0NTM=";s:5:"wtype";s:16:"bmV3c2xldHRlcg==";s:11:"widget_name";s:32:"U2lnbiBVcCBGb3IgTmV3c2xldHRlcg==";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:32:"U2lnbiBVcCBGb3IgTmV3c2xldHRlcg==";s:14:"widget_title_2";s:32:"U2lnbiBVcCBGb3IgTmV3c2xldHRlcg==";s:14:"widget_title_3";s:32:"U2lnbiBVcCBGb3IgTmV3c2xldHRlcg==";s:14:"widget_title_4";s:32:"U2lnbiBVcCBGb3IgTmV3c2xldHRlcg==";s:14:"widget_title_5";s:32:"U2lnbiBVcCBGb3IgTmV3c2xldHRlcg==";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:5:"class";s:20:"cHRzLW5ld3NsZXR0ZXI=";s:9:"imagefile";s:0:"";s:13:"information_1";s:144:"PHA+U2lnbiB1cCB0byBvdXIgbmV3c2xldHRlciBhbmQgZ2V0IGV4Y2x1c2l2ZSBkZWFscyB5b3Ugd29udCBmaW5kIGFueXdoZXJlIGVsc2Ugc3RyYWlnaHQgdG8geW91ciBpbmJveCE8L3A+";s:13:"information_2";s:144:"PHA+U2lnbiB1cCB0byBvdXIgbmV3c2xldHRlciBhbmQgZ2V0IGV4Y2x1c2l2ZSBkZWFscyB5b3Ugd29udCBmaW5kIGFueXdoZXJlIGVsc2Ugc3RyYWlnaHQgdG8geW91ciBpbmJveCE8L3A+";s:13:"information_3";s:144:"PHA+U2lnbiB1cCB0byBvdXIgbmV3c2xldHRlciBhbmQgZ2V0IGV4Y2x1c2l2ZSBkZWFscyB5b3Ugd29udCBmaW5kIGFueXdoZXJlIGVsc2Ugc3RyYWlnaHQgdG8geW91ciBpbmJveCE8L3A+";s:13:"information_4";s:144:"PHA+U2lnbiB1cCB0byBvdXIgbmV3c2xldHRlciBhbmQgZ2V0IGV4Y2x1c2l2ZSBkZWFscyB5b3Ugd29udCBmaW5kIGFueXdoZXJlIGVsc2Ugc3RyYWlnaHQgdG8geW91ciBpbmJveCE8L3A+";s:13:"information_5";s:144:"PHA+U2lnbiB1cCB0byBvdXIgbmV3c2xldHRlciBhbmQgZ2V0IGV4Y2x1c2l2ZSBkZWFscyB5b3Ugd29udCBmaW5kIGFueXdoZXJlIGVsc2Ugc3RyYWlnaHQgdG8geW91ciBpbmJveCE8L3A+";s:16:"newsletter_style";s:8:"c3R5bGUx";}s:4:"wkey";s:17:"key_1414831529453";}";}s:17:"key_1414831611780";a:1:{s:6:"config";s:5467:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzE2MTE3ODA=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:16:"SHRtbCBGb290ZXI=";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:16:"SHRtbCBGb290ZXI=";s:14:"widget_title_2";s:16:"SHRtbCBGb290ZXI=";s:14:"widget_title_3";s:16:"SHRtbCBGb290ZXI=";s:14:"widget_title_4";s:16:"SHRtbCBGb290ZXI=";s:14:"widget_title_5";s:16:"SHRtbCBGb290ZXI=";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:960:"PHVsIGNsYXNzPSJyb3cgIj4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14cy0xMiAiPg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxkaXYgY2xhc3M9ImN1c3RvbSBpbmZvLXNlcnZpY2VzIj4NCjxkaXYgY2xhc3M9ImluZm8tdGl0bGUgYm9yZGVyIHNtYWxsIj5jYWxsIHRoZSBjdXN0b21lciBzZXJ2aWNlczwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC1oaWdobGlnaHQiPjxzdHJvbmc+MTgwMCAtIDE1NzAgLSAwMDA8L3N0cm9uZz48L2Rpdj4NCjxkaXYgY2xhc3M9IiBib3JkZXIgc21hbGwiPkZyZWUgMjQvNyBzdXBwb3J0IGZvciBvdXIgY3VzdG9tZXJzPC9kaXY+DQo8L2Rpdj4NCjwvZGl2Pg0KPC9saT4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14czEyIj4NCjxkaXYgY2xhc3M9Iml0ZW0taHRtbCI+DQo8ZGl2IGNsYXNzPSJjdXN0b20gaW5mby1xdWVzdGlvbiI+DQo8ZGl2IGNsYXNzPSJpbmZvLXRpdGxlIGJvcmRlciBzbWFsbCI+ZW1haWwgdXMgeW91ciBxdWVzdGlvbjwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC13YXJuaW5nIj48c3Ryb25nPnN1cHBvcnRAbWl4c3RvcmUuY29tPC9zdHJvbmc+PC9kaXY+DQo8ZGl2IGNsYXNzPSIgYm9yZGVyIHNtYWxsIj5XZSB3aWxsIHJlc3BvbmQgd2l0aGluIDMgYnVzaW5lc3MgZGF5czwvZGl2Pg0KPC9kaXY+DQo8L2Rpdj4NCjwvbGk+DQo8L3VsPg==";s:13:"htmlcontent_2";s:960:"PHVsIGNsYXNzPSJyb3cgIj4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14cy0xMiAiPg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxkaXYgY2xhc3M9ImN1c3RvbSBpbmZvLXNlcnZpY2VzIj4NCjxkaXYgY2xhc3M9ImluZm8tdGl0bGUgYm9yZGVyIHNtYWxsIj5jYWxsIHRoZSBjdXN0b21lciBzZXJ2aWNlczwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC1oaWdobGlnaHQiPjxzdHJvbmc+MTgwMCAtIDE1NzAgLSAwMDA8L3N0cm9uZz48L2Rpdj4NCjxkaXYgY2xhc3M9IiBib3JkZXIgc21hbGwiPkZyZWUgMjQvNyBzdXBwb3J0IGZvciBvdXIgY3VzdG9tZXJzPC9kaXY+DQo8L2Rpdj4NCjwvZGl2Pg0KPC9saT4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14czEyIj4NCjxkaXYgY2xhc3M9Iml0ZW0taHRtbCI+DQo8ZGl2IGNsYXNzPSJjdXN0b20gaW5mby1xdWVzdGlvbiI+DQo8ZGl2IGNsYXNzPSJpbmZvLXRpdGxlIGJvcmRlciBzbWFsbCI+ZW1haWwgdXMgeW91ciBxdWVzdGlvbjwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC13YXJuaW5nIj48c3Ryb25nPnN1cHBvcnRAbWl4c3RvcmUuY29tPC9zdHJvbmc+PC9kaXY+DQo8ZGl2IGNsYXNzPSIgYm9yZGVyIHNtYWxsIj5XZSB3aWxsIHJlc3BvbmQgd2l0aGluIDMgYnVzaW5lc3MgZGF5czwvZGl2Pg0KPC9kaXY+DQo8L2Rpdj4NCjwvbGk+DQo8L3VsPg==";s:13:"htmlcontent_3";s:960:"PHVsIGNsYXNzPSJyb3cgIj4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14cy0xMiAiPg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxkaXYgY2xhc3M9ImN1c3RvbSBpbmZvLXNlcnZpY2VzIj4NCjxkaXYgY2xhc3M9ImluZm8tdGl0bGUgYm9yZGVyIHNtYWxsIj5jYWxsIHRoZSBjdXN0b21lciBzZXJ2aWNlczwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC1oaWdobGlnaHQiPjxzdHJvbmc+MTgwMCAtIDE1NzAgLSAwMDA8L3N0cm9uZz48L2Rpdj4NCjxkaXYgY2xhc3M9IiBib3JkZXIgc21hbGwiPkZyZWUgMjQvNyBzdXBwb3J0IGZvciBvdXIgY3VzdG9tZXJzPC9kaXY+DQo8L2Rpdj4NCjwvZGl2Pg0KPC9saT4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14czEyIj4NCjxkaXYgY2xhc3M9Iml0ZW0taHRtbCI+DQo8ZGl2IGNsYXNzPSJjdXN0b20gaW5mby1xdWVzdGlvbiI+DQo8ZGl2IGNsYXNzPSJpbmZvLXRpdGxlIGJvcmRlciBzbWFsbCI+ZW1haWwgdXMgeW91ciBxdWVzdGlvbjwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC13YXJuaW5nIj48c3Ryb25nPnN1cHBvcnRAbWl4c3RvcmUuY29tPC9zdHJvbmc+PC9kaXY+DQo8ZGl2IGNsYXNzPSIgYm9yZGVyIHNtYWxsIj5XZSB3aWxsIHJlc3BvbmQgd2l0aGluIDMgYnVzaW5lc3MgZGF5czwvZGl2Pg0KPC9kaXY+DQo8L2Rpdj4NCjwvbGk+DQo8L3VsPg==";s:13:"htmlcontent_4";s:960:"PHVsIGNsYXNzPSJyb3cgIj4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14cy0xMiAiPg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxkaXYgY2xhc3M9ImN1c3RvbSBpbmZvLXNlcnZpY2VzIj4NCjxkaXYgY2xhc3M9ImluZm8tdGl0bGUgYm9yZGVyIHNtYWxsIj5jYWxsIHRoZSBjdXN0b21lciBzZXJ2aWNlczwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC1oaWdobGlnaHQiPjxzdHJvbmc+MTgwMCAtIDE1NzAgLSAwMDA8L3N0cm9uZz48L2Rpdj4NCjxkaXYgY2xhc3M9IiBib3JkZXIgc21hbGwiPkZyZWUgMjQvNyBzdXBwb3J0IGZvciBvdXIgY3VzdG9tZXJzPC9kaXY+DQo8L2Rpdj4NCjwvZGl2Pg0KPC9saT4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14czEyIj4NCjxkaXYgY2xhc3M9Iml0ZW0taHRtbCI+DQo8ZGl2IGNsYXNzPSJjdXN0b20gaW5mby1xdWVzdGlvbiI+DQo8ZGl2IGNsYXNzPSJpbmZvLXRpdGxlIGJvcmRlciBzbWFsbCI+ZW1haWwgdXMgeW91ciBxdWVzdGlvbjwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC13YXJuaW5nIj48c3Ryb25nPnN1cHBvcnRAbWl4c3RvcmUuY29tPC9zdHJvbmc+PC9kaXY+DQo8ZGl2IGNsYXNzPSIgYm9yZGVyIHNtYWxsIj5XZSB3aWxsIHJlc3BvbmQgd2l0aGluIDMgYnVzaW5lc3MgZGF5czwvZGl2Pg0KPC9kaXY+DQo8L2Rpdj4NCjwvbGk+DQo8L3VsPg==";s:13:"htmlcontent_5";s:960:"PHVsIGNsYXNzPSJyb3cgIj4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14cy0xMiAiPg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxkaXYgY2xhc3M9ImN1c3RvbSBpbmZvLXNlcnZpY2VzIj4NCjxkaXYgY2xhc3M9ImluZm8tdGl0bGUgYm9yZGVyIHNtYWxsIj5jYWxsIHRoZSBjdXN0b21lciBzZXJ2aWNlczwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC1oaWdobGlnaHQiPjxzdHJvbmc+MTgwMCAtIDE1NzAgLSAwMDA8L3N0cm9uZz48L2Rpdj4NCjxkaXYgY2xhc3M9IiBib3JkZXIgc21hbGwiPkZyZWUgMjQvNyBzdXBwb3J0IGZvciBvdXIgY3VzdG9tZXJzPC9kaXY+DQo8L2Rpdj4NCjwvZGl2Pg0KPC9saT4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14czEyIj4NCjxkaXYgY2xhc3M9Iml0ZW0taHRtbCI+DQo8ZGl2IGNsYXNzPSJjdXN0b20gaW5mby1xdWVzdGlvbiI+DQo8ZGl2IGNsYXNzPSJpbmZvLXRpdGxlIGJvcmRlciBzbWFsbCI+ZW1haWwgdXMgeW91ciBxdWVzdGlvbjwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC13YXJuaW5nIj48c3Ryb25nPnN1cHBvcnRAbWl4c3RvcmUuY29tPC9zdHJvbmc+PC9kaXY+DQo8ZGl2IGNsYXNzPSIgYm9yZGVyIHNtYWxsIj5XZSB3aWxsIHJlc3BvbmQgd2l0aGluIDMgYnVzaW5lc3MgZGF5czwvZGl2Pg0KPC9kaXY+DQo8L2Rpdj4NCjwvbGk+DQo8L3VsPg==";}s:4:"wkey";s:17:"key_1414831611780";}";}}', 24, 0, 1, 1414832874),
	('FooterCd', 0, '', '[{"index":0,"cls":"pts-footer-top","bgcolor":"#ffffff","bgimage":"","fullwidth":"0","parallax":"0","sfxcls":null,"cols":[{"index":0,"cls":"hidden-xs","sfxcls":null,"bgcolor":"","bgimage":"","inrow":0,"lgcol":9,"mdcol":8,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831113755","name":"Logo Footer"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":3,"mdcol":4,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831222604","name":"Social footer"}],"rows":[]}]},{"index":0,"cls":"pts-footer-center","bgcolor":"#283240","bgimage":"","fullwidth":"0","parallax":"0","sfxcls":null,"cols":[{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":3,"mdcol":3,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831295069","name":"Contact info"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":2,"mdcol":2,"smcol":4,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831349733","name":"Customer Service"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":2,"mdcol":2,"smcol":4,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831415734","name":"Information"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":2,"mdcol":2,"smcol":4,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831468547","name":"My account"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":3,"mdcol":3,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"newsletter","wkey":"key_1414831529453","name":"Sign Up For Newsletter"}],"rows":[]}]},{"index":0,"cls":"hidden-xs","bgcolor":"","bgimage":"bg-footerbottom-cd.jpg","fullwidth":"0","parallax":"1","sfxcls":null,"cols":[{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":12,"mdcol":12,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831611780","name":"Html Footer"}],"rows":[]}]}]', 'a:8:{s:17:"key_1414831113755";a:1:{s:6:"config";s:2547:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzExMTM3NTU=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:16:"TG9nbyBGb290ZXI=";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:16:"TG9nbyBGb290ZXI=";s:14:"widget_title_2";s:16:"TG9nbyBGb290ZXI=";s:14:"widget_title_3";s:16:"TG9nbyBGb290ZXI=";s:14:"widget_title_4";s:16:"TG9nbyBGb290ZXI=";s:14:"widget_title_5";s:16:"TG9nbyBGb290ZXI=";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:328:"PGRpdiBjbGFzcz0iaW1hZ2UtYmFubmVyICBwdWxsLWxlZnQiPjxpbWcgY2xhc3M9Iml0ZW0taW1nIiBzcmM9Im1vZHVsZXMvcHRzcGFnZWJ1aWxkZXIvaW1hZ2VzL2xvZ28tZm9vdGVyLnBuZyIgYWx0PSIiIC8+PC9kaXY+DQo8ZGl2IGNsYXNzPSJpdGVtLWh0bWwiPg0KPHA+V2XigJlyZSBjb25maWRlbnQgd2XigJl2ZSBwcm92aWRlZCBhbGwgdGhlIGJlc3QgZm9yIHlvdS4gU3RheSBjb25uZWVjdCB3aXRoIHVzPC9wPg0KPC9kaXY+";s:13:"htmlcontent_2";s:388:"PGRpdiBjbGFzcz0iaW1hZ2UtYmFubmVyICBwdWxsLWxlZnQiPjxpbWcgY2xhc3M9Iml0ZW0taW1nIiBzcmM9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW1nL2Ntcy9sb2dvLWZvb3Rlci5wbmciIGFsdD0iIiB3aWR0aD0iMTMxIiBoZWlnaHQ9IjQwIiAvPjwvZGl2Pg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxwPldl4oCZcmUgY29uZmlkZW50IHdl4oCZdmUgcHJvdmlkZWQgYWxsIHRoZSBiZXN0IGZvciB5b3UuIFN0YXkgY29ubmVlY3Qgd2l0aCB1czwvcD4NCjwvZGl2Pg==";s:13:"htmlcontent_3";s:388:"PGRpdiBjbGFzcz0iaW1hZ2UtYmFubmVyICBwdWxsLWxlZnQiPjxpbWcgY2xhc3M9Iml0ZW0taW1nIiBzcmM9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW1nL2Ntcy9sb2dvLWZvb3Rlci5wbmciIGFsdD0iIiB3aWR0aD0iMTMxIiBoZWlnaHQ9IjQwIiAvPjwvZGl2Pg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxwPldl4oCZcmUgY29uZmlkZW50IHdl4oCZdmUgcHJvdmlkZWQgYWxsIHRoZSBiZXN0IGZvciB5b3UuIFN0YXkgY29ubmVlY3Qgd2l0aCB1czwvcD4NCjwvZGl2Pg==";s:13:"htmlcontent_4";s:388:"PGRpdiBjbGFzcz0iaW1hZ2UtYmFubmVyICBwdWxsLWxlZnQiPjxpbWcgY2xhc3M9Iml0ZW0taW1nIiBzcmM9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW1nL2Ntcy9sb2dvLWZvb3Rlci5wbmciIGFsdD0iIiB3aWR0aD0iMTMxIiBoZWlnaHQ9IjQwIiAvPjwvZGl2Pg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxwPldl4oCZcmUgY29uZmlkZW50IHdl4oCZdmUgcHJvdmlkZWQgYWxsIHRoZSBiZXN0IGZvciB5b3UuIFN0YXkgY29ubmVlY3Qgd2l0aCB1czwvcD4NCjwvZGl2Pg==";s:13:"htmlcontent_5";s:388:"PGRpdiBjbGFzcz0iaW1hZ2UtYmFubmVyICBwdWxsLWxlZnQiPjxpbWcgY2xhc3M9Iml0ZW0taW1nIiBzcmM9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW1nL2Ntcy9sb2dvLWZvb3Rlci5wbmciIGFsdD0iIiB3aWR0aD0iMTMxIiBoZWlnaHQ9IjQwIiAvPjwvZGl2Pg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxwPldl4oCZcmUgY29uZmlkZW50IHdl4oCZdmUgcHJvdmlkZWQgYWxsIHRoZSBiZXN0IGZvciB5b3UuIFN0YXkgY29ubmVlY3Qgd2l0aCB1czwvcD4NCjwvZGl2Pg==";}s:4:"wkey";s:17:"key_1414831113755";}";}s:17:"key_1414831222604";a:1:{s:6:"config";s:6557:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzEyMjI2MDQ=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:20:"U29jaWFsIGZvb3Rlcg==";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:20:"U29jaWFsIGZvb3Rlcg==";s:14:"widget_title_2";s:20:"U29jaWFsIGZvb3Rlcg==";s:14:"widget_title_3";s:20:"U29jaWFsIGZvb3Rlcg==";s:14:"widget_title_4";s:20:"U29jaWFsIGZvb3Rlcg==";s:14:"widget_title_5";s:20:"U29jaWFsIGZvb3Rlcg==";s:12:"addition_cls";s:20:"c29jaWFsLWZvb3Rlcg==";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:1168:"PGRpdiBpZD0ic29jaWFsX2Jsb2NrIj4NCjx1bCBjbGFzcz0ic29jaWFsIj4NCjxsaSBjbGFzcz0iZmFjZWJvb2siPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy5mYWNlYm9vay5jb20vcHJlc3Rhc2hvcCIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IkZhY2Vib29rIj4gRmFjZWJvb2sgPC9hPjwvbGk+DQo8bGkgY2xhc3M9InR3aXR0ZXIiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy50d2l0dGVyLmNvbS9wcmVzdGFzaG9wIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iVHdpdHRlciI+VHdpdHRlciA8L2E+PC9saT4NCjxsaSBjbGFzcz0icnNzIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9Imh0dHA6Ly93d3cucHJlc3Rhc2hvcC5jb20vYmxvZy9lbi9mZWVkLyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlJTUyI+IFJTUzwvYT48L2xpPg0KPGxpIGNsYXNzPSJ5b3V0dWJlIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9IiMiIHRhcmdldD0iX2JsYW5rIiBkYXRhLW9yaWdpbmFsLXRpdGxlPSJZb3V0dWJlIj4gWW91dHViZTwvYT48L2xpPg0KPGxpIGNsYXNzPSJnb29nbGUtcGx1cyI+PGEgY2xhc3M9ImJ0bi10b29sdGlwIiBocmVmPSIjIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iR29vZ2xlIFBsdXMiPkdvb2dsZSBQbHVzIDwvYT48L2xpPg0KPGxpIGNsYXNzPSJwaW50ZXJlc3QiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iIyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlBpbnRlcmVzdCI+IFBpbnRlcmVzdDwvYT48L2xpPg0KPC91bD4NCjwvZGl2Pg==";s:13:"htmlcontent_2";s:1168:"PGRpdiBpZD0ic29jaWFsX2Jsb2NrIj4NCjx1bCBjbGFzcz0ic29jaWFsIj4NCjxsaSBjbGFzcz0iZmFjZWJvb2siPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy5mYWNlYm9vay5jb20vcHJlc3Rhc2hvcCIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IkZhY2Vib29rIj4gRmFjZWJvb2sgPC9hPjwvbGk+DQo8bGkgY2xhc3M9InR3aXR0ZXIiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy50d2l0dGVyLmNvbS9wcmVzdGFzaG9wIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iVHdpdHRlciI+VHdpdHRlciA8L2E+PC9saT4NCjxsaSBjbGFzcz0icnNzIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9Imh0dHA6Ly93d3cucHJlc3Rhc2hvcC5jb20vYmxvZy9lbi9mZWVkLyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlJTUyI+IFJTUzwvYT48L2xpPg0KPGxpIGNsYXNzPSJ5b3V0dWJlIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9IiMiIHRhcmdldD0iX2JsYW5rIiBkYXRhLW9yaWdpbmFsLXRpdGxlPSJZb3V0dWJlIj4gWW91dHViZTwvYT48L2xpPg0KPGxpIGNsYXNzPSJnb29nbGUtcGx1cyI+PGEgY2xhc3M9ImJ0bi10b29sdGlwIiBocmVmPSIjIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iR29vZ2xlIFBsdXMiPkdvb2dsZSBQbHVzIDwvYT48L2xpPg0KPGxpIGNsYXNzPSJwaW50ZXJlc3QiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iIyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlBpbnRlcmVzdCI+IFBpbnRlcmVzdDwvYT48L2xpPg0KPC91bD4NCjwvZGl2Pg==";s:13:"htmlcontent_3";s:1168:"PGRpdiBpZD0ic29jaWFsX2Jsb2NrIj4NCjx1bCBjbGFzcz0ic29jaWFsIj4NCjxsaSBjbGFzcz0iZmFjZWJvb2siPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy5mYWNlYm9vay5jb20vcHJlc3Rhc2hvcCIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IkZhY2Vib29rIj4gRmFjZWJvb2sgPC9hPjwvbGk+DQo8bGkgY2xhc3M9InR3aXR0ZXIiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy50d2l0dGVyLmNvbS9wcmVzdGFzaG9wIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iVHdpdHRlciI+VHdpdHRlciA8L2E+PC9saT4NCjxsaSBjbGFzcz0icnNzIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9Imh0dHA6Ly93d3cucHJlc3Rhc2hvcC5jb20vYmxvZy9lbi9mZWVkLyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlJTUyI+IFJTUzwvYT48L2xpPg0KPGxpIGNsYXNzPSJ5b3V0dWJlIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9IiMiIHRhcmdldD0iX2JsYW5rIiBkYXRhLW9yaWdpbmFsLXRpdGxlPSJZb3V0dWJlIj4gWW91dHViZTwvYT48L2xpPg0KPGxpIGNsYXNzPSJnb29nbGUtcGx1cyI+PGEgY2xhc3M9ImJ0bi10b29sdGlwIiBocmVmPSIjIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iR29vZ2xlIFBsdXMiPkdvb2dsZSBQbHVzIDwvYT48L2xpPg0KPGxpIGNsYXNzPSJwaW50ZXJlc3QiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iIyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlBpbnRlcmVzdCI+IFBpbnRlcmVzdDwvYT48L2xpPg0KPC91bD4NCjwvZGl2Pg==";s:13:"htmlcontent_4";s:1168:"PGRpdiBpZD0ic29jaWFsX2Jsb2NrIj4NCjx1bCBjbGFzcz0ic29jaWFsIj4NCjxsaSBjbGFzcz0iZmFjZWJvb2siPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy5mYWNlYm9vay5jb20vcHJlc3Rhc2hvcCIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IkZhY2Vib29rIj4gRmFjZWJvb2sgPC9hPjwvbGk+DQo8bGkgY2xhc3M9InR3aXR0ZXIiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy50d2l0dGVyLmNvbS9wcmVzdGFzaG9wIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iVHdpdHRlciI+VHdpdHRlciA8L2E+PC9saT4NCjxsaSBjbGFzcz0icnNzIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9Imh0dHA6Ly93d3cucHJlc3Rhc2hvcC5jb20vYmxvZy9lbi9mZWVkLyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlJTUyI+IFJTUzwvYT48L2xpPg0KPGxpIGNsYXNzPSJ5b3V0dWJlIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9IiMiIHRhcmdldD0iX2JsYW5rIiBkYXRhLW9yaWdpbmFsLXRpdGxlPSJZb3V0dWJlIj4gWW91dHViZTwvYT48L2xpPg0KPGxpIGNsYXNzPSJnb29nbGUtcGx1cyI+PGEgY2xhc3M9ImJ0bi10b29sdGlwIiBocmVmPSIjIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iR29vZ2xlIFBsdXMiPkdvb2dsZSBQbHVzIDwvYT48L2xpPg0KPGxpIGNsYXNzPSJwaW50ZXJlc3QiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iIyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlBpbnRlcmVzdCI+IFBpbnRlcmVzdDwvYT48L2xpPg0KPC91bD4NCjwvZGl2Pg==";s:13:"htmlcontent_5";s:1168:"PGRpdiBpZD0ic29jaWFsX2Jsb2NrIj4NCjx1bCBjbGFzcz0ic29jaWFsIj4NCjxsaSBjbGFzcz0iZmFjZWJvb2siPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy5mYWNlYm9vay5jb20vcHJlc3Rhc2hvcCIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IkZhY2Vib29rIj4gRmFjZWJvb2sgPC9hPjwvbGk+DQo8bGkgY2xhc3M9InR3aXR0ZXIiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy50d2l0dGVyLmNvbS9wcmVzdGFzaG9wIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iVHdpdHRlciI+VHdpdHRlciA8L2E+PC9saT4NCjxsaSBjbGFzcz0icnNzIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9Imh0dHA6Ly93d3cucHJlc3Rhc2hvcC5jb20vYmxvZy9lbi9mZWVkLyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlJTUyI+IFJTUzwvYT48L2xpPg0KPGxpIGNsYXNzPSJ5b3V0dWJlIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9IiMiIHRhcmdldD0iX2JsYW5rIiBkYXRhLW9yaWdpbmFsLXRpdGxlPSJZb3V0dWJlIj4gWW91dHViZTwvYT48L2xpPg0KPGxpIGNsYXNzPSJnb29nbGUtcGx1cyI+PGEgY2xhc3M9ImJ0bi10b29sdGlwIiBocmVmPSIjIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iR29vZ2xlIFBsdXMiPkdvb2dsZSBQbHVzIDwvYT48L2xpPg0KPGxpIGNsYXNzPSJwaW50ZXJlc3QiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iIyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlBpbnRlcmVzdCI+IFBpbnRlcmVzdDwvYT48L2xpPg0KPC91bD4NCjwvZGl2Pg==";}s:4:"wkey";s:17:"key_1414831222604";}";}s:17:"key_1414831295069";a:1:{s:6:"config";s:4247:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzEyOTUwNjk=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:16:"Q29udGFjdCBpbmZv";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:16:"Q29udGFjdCBpbmZv";s:14:"widget_title_2";s:16:"Q29udGFjdCBpbmZv";s:14:"widget_title_3";s:16:"Q29udGFjdCBpbmZv";s:14:"widget_title_4";s:16:"Q29udGFjdCBpbmZv";s:14:"widget_title_5";s:16:"Q29udGFjdCBpbmZv";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:716:"PGRpdiBjbGFzcz0ibGlzdC1ncm91cCI+U2VkIG5vbiBtYXVyaXMgdml0YWUgZXJhdCBjb25zZXF1YXQgYXVjdG9yIGV1IGluIGVsaXQuIENsYXNzIGFwdGVudCB0YWNpdGkgc29zcXUgYWQgbGl0b3JhIHRvcnF1ZW50IHBlciBjb251YmlhPC9kaXY+DQo8dWwgY2xhc3M9Imxpc3QtZ3JvdXAiPg0KPGxpPk5vIDExMDQgU2t5IFRvd2VyLCBOZXd5b3JrLCBVU0E8L2xpPg0KPGxpPkNhbGwgdXMgbm93OiAwMTIzLTQ1Ni03ODk8L2xpPg0KPGxpPkVtYWlsOiA8YSBocmVmPSJtYWlsdG86JTczJTYxJTZjJTY1JTczQCU3OSU2ZiU3NSU3MiU2MyU2ZiU2ZCU3MCU2MSU2ZSU3OS4lNjMlNmYlNmQiPnNhbGVzQHlvdXJjb21wYW55LmNvbTwvYT48L2xpPg0KPC91bD4NCjxkaXYgY2xhc3M9Im1hcCI+PGEgaWQ9ImZhbmN5Ym94LW1hcCIgdGl0bGU9IlZpZXdNYXAiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9Y29udGFjdCZjb250ZW50X29ubHk9MSI+Vmlld01hcCA8L2E+PC9kaXY+";s:13:"htmlcontent_2";s:716:"PGRpdiBjbGFzcz0ibGlzdC1ncm91cCI+U2VkIG5vbiBtYXVyaXMgdml0YWUgZXJhdCBjb25zZXF1YXQgYXVjdG9yIGV1IGluIGVsaXQuIENsYXNzIGFwdGVudCB0YWNpdGkgc29zcXUgYWQgbGl0b3JhIHRvcnF1ZW50IHBlciBjb251YmlhPC9kaXY+DQo8dWwgY2xhc3M9Imxpc3QtZ3JvdXAiPg0KPGxpPk5vIDExMDQgU2t5IFRvd2VyLCBOZXd5b3JrLCBVU0E8L2xpPg0KPGxpPkNhbGwgdXMgbm93OiAwMTIzLTQ1Ni03ODk8L2xpPg0KPGxpPkVtYWlsOiA8YSBocmVmPSJtYWlsdG86JTczJTYxJTZjJTY1JTczQCU3OSU2ZiU3NSU3MiU2MyU2ZiU2ZCU3MCU2MSU2ZSU3OS4lNjMlNmYlNmQiPnNhbGVzQHlvdXJjb21wYW55LmNvbTwvYT48L2xpPg0KPC91bD4NCjxkaXYgY2xhc3M9Im1hcCI+PGEgaWQ9ImZhbmN5Ym94LW1hcCIgdGl0bGU9IlZpZXdNYXAiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9Y29udGFjdCZjb250ZW50X29ubHk9MSI+Vmlld01hcCA8L2E+PC9kaXY+";s:13:"htmlcontent_3";s:716:"PGRpdiBjbGFzcz0ibGlzdC1ncm91cCI+U2VkIG5vbiBtYXVyaXMgdml0YWUgZXJhdCBjb25zZXF1YXQgYXVjdG9yIGV1IGluIGVsaXQuIENsYXNzIGFwdGVudCB0YWNpdGkgc29zcXUgYWQgbGl0b3JhIHRvcnF1ZW50IHBlciBjb251YmlhPC9kaXY+DQo8dWwgY2xhc3M9Imxpc3QtZ3JvdXAiPg0KPGxpPk5vIDExMDQgU2t5IFRvd2VyLCBOZXd5b3JrLCBVU0E8L2xpPg0KPGxpPkNhbGwgdXMgbm93OiAwMTIzLTQ1Ni03ODk8L2xpPg0KPGxpPkVtYWlsOiA8YSBocmVmPSJtYWlsdG86JTczJTYxJTZjJTY1JTczQCU3OSU2ZiU3NSU3MiU2MyU2ZiU2ZCU3MCU2MSU2ZSU3OS4lNjMlNmYlNmQiPnNhbGVzQHlvdXJjb21wYW55LmNvbTwvYT48L2xpPg0KPC91bD4NCjxkaXYgY2xhc3M9Im1hcCI+PGEgaWQ9ImZhbmN5Ym94LW1hcCIgdGl0bGU9IlZpZXdNYXAiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9Y29udGFjdCZjb250ZW50X29ubHk9MSI+Vmlld01hcCA8L2E+PC9kaXY+";s:13:"htmlcontent_4";s:716:"PGRpdiBjbGFzcz0ibGlzdC1ncm91cCI+U2VkIG5vbiBtYXVyaXMgdml0YWUgZXJhdCBjb25zZXF1YXQgYXVjdG9yIGV1IGluIGVsaXQuIENsYXNzIGFwdGVudCB0YWNpdGkgc29zcXUgYWQgbGl0b3JhIHRvcnF1ZW50IHBlciBjb251YmlhPC9kaXY+DQo8dWwgY2xhc3M9Imxpc3QtZ3JvdXAiPg0KPGxpPk5vIDExMDQgU2t5IFRvd2VyLCBOZXd5b3JrLCBVU0E8L2xpPg0KPGxpPkNhbGwgdXMgbm93OiAwMTIzLTQ1Ni03ODk8L2xpPg0KPGxpPkVtYWlsOiA8YSBocmVmPSJtYWlsdG86JTczJTYxJTZjJTY1JTczQCU3OSU2ZiU3NSU3MiU2MyU2ZiU2ZCU3MCU2MSU2ZSU3OS4lNjMlNmYlNmQiPnNhbGVzQHlvdXJjb21wYW55LmNvbTwvYT48L2xpPg0KPC91bD4NCjxkaXYgY2xhc3M9Im1hcCI+PGEgaWQ9ImZhbmN5Ym94LW1hcCIgdGl0bGU9IlZpZXdNYXAiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9Y29udGFjdCZjb250ZW50X29ubHk9MSI+Vmlld01hcCA8L2E+PC9kaXY+";s:13:"htmlcontent_5";s:716:"PGRpdiBjbGFzcz0ibGlzdC1ncm91cCI+U2VkIG5vbiBtYXVyaXMgdml0YWUgZXJhdCBjb25zZXF1YXQgYXVjdG9yIGV1IGluIGVsaXQuIENsYXNzIGFwdGVudCB0YWNpdGkgc29zcXUgYWQgbGl0b3JhIHRvcnF1ZW50IHBlciBjb251YmlhPC9kaXY+DQo8dWwgY2xhc3M9Imxpc3QtZ3JvdXAiPg0KPGxpPk5vIDExMDQgU2t5IFRvd2VyLCBOZXd5b3JrLCBVU0E8L2xpPg0KPGxpPkNhbGwgdXMgbm93OiAwMTIzLTQ1Ni03ODk8L2xpPg0KPGxpPkVtYWlsOiA8YSBocmVmPSJtYWlsdG86JTczJTYxJTZjJTY1JTczQCU3OSU2ZiU3NSU3MiU2MyU2ZiU2ZCU3MCU2MSU2ZSU3OS4lNjMlNmYlNmQiPnNhbGVzQHlvdXJjb21wYW55LmNvbTwvYT48L2xpPg0KPC91bD4NCjxkaXYgY2xhc3M9Im1hcCI+PGEgaWQ9ImZhbmN5Ym94LW1hcCIgdGl0bGU9IlZpZXdNYXAiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9Y29udGFjdCZjb250ZW50X29ubHk9MSI+Vmlld01hcCA8L2E+PC9kaXY+";}s:4:"wkey";s:17:"key_1414831295069";}";}s:17:"key_1414831349733";a:1:{s:6:"config";s:3535:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzEzNDk3MzM=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:24:"Q3VzdG9tZXIgU2VydmljZQ==";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:24:"Q3VzdG9tZXIgU2VydmljZQ==";s:14:"widget_title_2";s:24:"Q3VzdG9tZXIgU2VydmljZQ==";s:14:"widget_title_3";s:24:"Q3VzdG9tZXIgU2VydmljZQ==";s:14:"widget_title_4";s:24:"Q3VzdG9tZXIgU2VydmljZQ==";s:14:"widget_title_5";s:24:"Q3VzdG9tZXIgU2VydmljZQ==";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:564:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iT3VyIHN0b3JlcyIgaHJlZj0iaW5kZXgucGhwP2NvbnRyb2xsZXI9c3RvcmVzIj4gT3VyIHN0b3JlcyA8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IkNvbnRhY3QgdXMiIGhyZWY9ImluZGV4LnBocD9jb250cm9sbGVyPWNvbnRhY3QiPiBDb250YWN0IHVzIDwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJTaXRlbWFwIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zaXRlbWFwIj4gU2l0ZW1hcCA8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iVmlldyBhIGxpc3Qgb2Ygc3VwcGxpZXJzIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zdXBwbGllciI+IFN1cHBsaWVycyA8L2E+PC9saT4NCjwvdWw+";s:13:"htmlcontent_2";s:564:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iT3VyIHN0b3JlcyIgaHJlZj0iaW5kZXgucGhwP2NvbnRyb2xsZXI9c3RvcmVzIj4gT3VyIHN0b3JlcyA8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IkNvbnRhY3QgdXMiIGhyZWY9ImluZGV4LnBocD9jb250cm9sbGVyPWNvbnRhY3QiPiBDb250YWN0IHVzIDwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJTaXRlbWFwIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zaXRlbWFwIj4gU2l0ZW1hcCA8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iVmlldyBhIGxpc3Qgb2Ygc3VwcGxpZXJzIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zdXBwbGllciI+IFN1cHBsaWVycyA8L2E+PC9saT4NCjwvdWw+";s:13:"htmlcontent_3";s:564:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iT3VyIHN0b3JlcyIgaHJlZj0iaW5kZXgucGhwP2NvbnRyb2xsZXI9c3RvcmVzIj4gT3VyIHN0b3JlcyA8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IkNvbnRhY3QgdXMiIGhyZWY9ImluZGV4LnBocD9jb250cm9sbGVyPWNvbnRhY3QiPiBDb250YWN0IHVzIDwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJTaXRlbWFwIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zaXRlbWFwIj4gU2l0ZW1hcCA8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iVmlldyBhIGxpc3Qgb2Ygc3VwcGxpZXJzIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zdXBwbGllciI+IFN1cHBsaWVycyA8L2E+PC9saT4NCjwvdWw+";s:13:"htmlcontent_4";s:564:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iT3VyIHN0b3JlcyIgaHJlZj0iaW5kZXgucGhwP2NvbnRyb2xsZXI9c3RvcmVzIj4gT3VyIHN0b3JlcyA8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IkNvbnRhY3QgdXMiIGhyZWY9ImluZGV4LnBocD9jb250cm9sbGVyPWNvbnRhY3QiPiBDb250YWN0IHVzIDwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJTaXRlbWFwIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zaXRlbWFwIj4gU2l0ZW1hcCA8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iVmlldyBhIGxpc3Qgb2Ygc3VwcGxpZXJzIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zdXBwbGllciI+IFN1cHBsaWVycyA8L2E+PC9saT4NCjwvdWw+";s:13:"htmlcontent_5";s:564:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iT3VyIHN0b3JlcyIgaHJlZj0iaW5kZXgucGhwP2NvbnRyb2xsZXI9c3RvcmVzIj4gT3VyIHN0b3JlcyA8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IkNvbnRhY3QgdXMiIGhyZWY9ImluZGV4LnBocD9jb250cm9sbGVyPWNvbnRhY3QiPiBDb250YWN0IHVzIDwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJTaXRlbWFwIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zaXRlbWFwIj4gU2l0ZW1hcCA8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iVmlldyBhIGxpc3Qgb2Ygc3VwcGxpZXJzIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zdXBwbGllciI+IFN1cHBsaWVycyA8L2E+PC9saT4NCjwvdWw+";}s:4:"wkey";s:17:"key_1414831349733";}";}s:17:"key_1414831415734";a:1:{s:6:"config";s:3187:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzE0MTU3MzQ=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:16:"SW5mb3JtYXRpb24=";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:16:"SW5mb3JtYXRpb24=";s:14:"widget_title_2";s:16:"SW5mb3JtYXRpb24=";s:14:"widget_title_3";s:16:"SW5mb3JtYXRpb24=";s:14:"widget_title_4";s:16:"SW5mb3JtYXRpb24=";s:14:"widget_title_5";s:16:"SW5mb3JtYXRpb24=";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:504:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj5NaSB2aXRhZSBtYWduaXM8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+IEZ1c2NlIGxhb3JlZXQ8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+VmVzdGlidSBmYXVjaWJ1czwvYT48L2xpPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj4gUG9ydHRpdG9yIGxhb3JlZXQgPC9hPjwvbGk+DQo8bGkgY2xhc3M9Iml0ZW0iPjxhIHRpdGxlPSIjIiBocmVmPSIjIj4gRmVsaXMgcG9ydHRpdG9yIDwvYT48L2xpPg0KPC91bD4=";s:13:"htmlcontent_2";s:504:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj5NaSB2aXRhZSBtYWduaXM8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+IEZ1c2NlIGxhb3JlZXQ8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+VmVzdGlidSBmYXVjaWJ1czwvYT48L2xpPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj4gUG9ydHRpdG9yIGxhb3JlZXQgPC9hPjwvbGk+DQo8bGkgY2xhc3M9Iml0ZW0iPjxhIHRpdGxlPSIjIiBocmVmPSIjIj4gRmVsaXMgcG9ydHRpdG9yIDwvYT48L2xpPg0KPC91bD4=";s:13:"htmlcontent_3";s:504:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj5NaSB2aXRhZSBtYWduaXM8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+IEZ1c2NlIGxhb3JlZXQ8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+VmVzdGlidSBmYXVjaWJ1czwvYT48L2xpPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj4gUG9ydHRpdG9yIGxhb3JlZXQgPC9hPjwvbGk+DQo8bGkgY2xhc3M9Iml0ZW0iPjxhIHRpdGxlPSIjIiBocmVmPSIjIj4gRmVsaXMgcG9ydHRpdG9yIDwvYT48L2xpPg0KPC91bD4=";s:13:"htmlcontent_4";s:504:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj5NaSB2aXRhZSBtYWduaXM8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+IEZ1c2NlIGxhb3JlZXQ8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+VmVzdGlidSBmYXVjaWJ1czwvYT48L2xpPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj4gUG9ydHRpdG9yIGxhb3JlZXQgPC9hPjwvbGk+DQo8bGkgY2xhc3M9Iml0ZW0iPjxhIHRpdGxlPSIjIiBocmVmPSIjIj4gRmVsaXMgcG9ydHRpdG9yIDwvYT48L2xpPg0KPC91bD4=";s:13:"htmlcontent_5";s:504:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj5NaSB2aXRhZSBtYWduaXM8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+IEZ1c2NlIGxhb3JlZXQ8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+VmVzdGlidSBmYXVjaWJ1czwvYT48L2xpPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj4gUG9ydHRpdG9yIGxhb3JlZXQgPC9hPjwvbGk+DQo8bGkgY2xhc3M9Iml0ZW0iPjxhIHRpdGxlPSIjIiBocmVmPSIjIj4gRmVsaXMgcG9ydHRpdG9yIDwvYT48L2xpPg0KPC91bD4=";}s:4:"wkey";s:17:"key_1414831415734";}";}s:17:"key_1414831468547";a:1:{s:6:"config";s:5007:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzE0Njg1NDc=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:16:"TXkgYWNjb3VudA==";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:16:"TXkgYWNjb3VudA==";s:14:"widget_title_2";s:16:"TXkgYWNjb3VudA==";s:14:"widget_title_3";s:16:"TXkgYWNjb3VudA==";s:14:"widget_title_4";s:16:"TXkgYWNjb3VudA==";s:14:"widget_title_5";s:16:"TXkgYWNjb3VudA==";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:868:"PHVsIGNsYXNzPSJsaXN0LWJsb2NrIGxpc3QtZ3JvdXAgbm9zdHlsZSI+DQo8bGk+PGEgdGl0bGU9Ik15IG9yZGVycyIgaHJlZj0iaHR0cDovL2xvY2FsaG9zdC9wcmVzdGFzaG9wL3BmX21peG1vYmlsZS9pbmRleC5waHA/Y29udHJvbGxlcj1oaXN0b3J5IiByZWw9Im5vZm9sbG93Ij5NeSBvcmRlcnM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTXkgY3JlZGl0IHNsaXBzIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPW9yZGVyLXNsaXAiIHJlbD0ibm9mb2xsb3ciPk15IGNyZWRpdCBzbGlwczwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJNeSBhZGRyZXNzZXMiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9YWRkcmVzc2VzIiByZWw9Im5vZm9sbG93Ij5NeSBhZGRyZXNzZXM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTWFuYWdlIG15IHBlcnNvbmFsIGluZm9ybWF0aW9uIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPWlkZW50aXR5IiByZWw9Im5vZm9sbG93Ij5NeSBwZXJzb25hbCBpbmZvPC9hPjwvbGk+DQo8L3VsPg==";s:13:"htmlcontent_2";s:868:"PHVsIGNsYXNzPSJsaXN0LWJsb2NrIGxpc3QtZ3JvdXAgbm9zdHlsZSI+DQo8bGk+PGEgdGl0bGU9Ik15IG9yZGVycyIgaHJlZj0iaHR0cDovL2xvY2FsaG9zdC9wcmVzdGFzaG9wL3BmX21peG1vYmlsZS9pbmRleC5waHA/Y29udHJvbGxlcj1oaXN0b3J5IiByZWw9Im5vZm9sbG93Ij5NeSBvcmRlcnM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTXkgY3JlZGl0IHNsaXBzIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPW9yZGVyLXNsaXAiIHJlbD0ibm9mb2xsb3ciPk15IGNyZWRpdCBzbGlwczwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJNeSBhZGRyZXNzZXMiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9YWRkcmVzc2VzIiByZWw9Im5vZm9sbG93Ij5NeSBhZGRyZXNzZXM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTWFuYWdlIG15IHBlcnNvbmFsIGluZm9ybWF0aW9uIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPWlkZW50aXR5IiByZWw9Im5vZm9sbG93Ij5NeSBwZXJzb25hbCBpbmZvPC9hPjwvbGk+DQo8L3VsPg==";s:13:"htmlcontent_3";s:868:"PHVsIGNsYXNzPSJsaXN0LWJsb2NrIGxpc3QtZ3JvdXAgbm9zdHlsZSI+DQo8bGk+PGEgdGl0bGU9Ik15IG9yZGVycyIgaHJlZj0iaHR0cDovL2xvY2FsaG9zdC9wcmVzdGFzaG9wL3BmX21peG1vYmlsZS9pbmRleC5waHA/Y29udHJvbGxlcj1oaXN0b3J5IiByZWw9Im5vZm9sbG93Ij5NeSBvcmRlcnM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTXkgY3JlZGl0IHNsaXBzIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPW9yZGVyLXNsaXAiIHJlbD0ibm9mb2xsb3ciPk15IGNyZWRpdCBzbGlwczwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJNeSBhZGRyZXNzZXMiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9YWRkcmVzc2VzIiByZWw9Im5vZm9sbG93Ij5NeSBhZGRyZXNzZXM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTWFuYWdlIG15IHBlcnNvbmFsIGluZm9ybWF0aW9uIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPWlkZW50aXR5IiByZWw9Im5vZm9sbG93Ij5NeSBwZXJzb25hbCBpbmZvPC9hPjwvbGk+DQo8L3VsPg==";s:13:"htmlcontent_4";s:868:"PHVsIGNsYXNzPSJsaXN0LWJsb2NrIGxpc3QtZ3JvdXAgbm9zdHlsZSI+DQo8bGk+PGEgdGl0bGU9Ik15IG9yZGVycyIgaHJlZj0iaHR0cDovL2xvY2FsaG9zdC9wcmVzdGFzaG9wL3BmX21peG1vYmlsZS9pbmRleC5waHA/Y29udHJvbGxlcj1oaXN0b3J5IiByZWw9Im5vZm9sbG93Ij5NeSBvcmRlcnM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTXkgY3JlZGl0IHNsaXBzIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPW9yZGVyLXNsaXAiIHJlbD0ibm9mb2xsb3ciPk15IGNyZWRpdCBzbGlwczwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJNeSBhZGRyZXNzZXMiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9YWRkcmVzc2VzIiByZWw9Im5vZm9sbG93Ij5NeSBhZGRyZXNzZXM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTWFuYWdlIG15IHBlcnNvbmFsIGluZm9ybWF0aW9uIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPWlkZW50aXR5IiByZWw9Im5vZm9sbG93Ij5NeSBwZXJzb25hbCBpbmZvPC9hPjwvbGk+DQo8L3VsPg==";s:13:"htmlcontent_5";s:868:"PHVsIGNsYXNzPSJsaXN0LWJsb2NrIGxpc3QtZ3JvdXAgbm9zdHlsZSI+DQo8bGk+PGEgdGl0bGU9Ik15IG9yZGVycyIgaHJlZj0iaHR0cDovL2xvY2FsaG9zdC9wcmVzdGFzaG9wL3BmX21peG1vYmlsZS9pbmRleC5waHA/Y29udHJvbGxlcj1oaXN0b3J5IiByZWw9Im5vZm9sbG93Ij5NeSBvcmRlcnM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTXkgY3JlZGl0IHNsaXBzIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPW9yZGVyLXNsaXAiIHJlbD0ibm9mb2xsb3ciPk15IGNyZWRpdCBzbGlwczwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJNeSBhZGRyZXNzZXMiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9YWRkcmVzc2VzIiByZWw9Im5vZm9sbG93Ij5NeSBhZGRyZXNzZXM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTWFuYWdlIG15IHBlcnNvbmFsIGluZm9ybWF0aW9uIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPWlkZW50aXR5IiByZWw9Im5vZm9sbG93Ij5NeSBwZXJzb25hbCBpbmZvPC9hPjwvbGk+DQo8L3VsPg==";}s:4:"wkey";s:17:"key_1414831468547";}";}s:17:"key_1414831529453";a:1:{s:6:"config";s:1594:"a:2:{s:6:"widget";a:20:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzE1Mjk0NTM=";s:5:"wtype";s:16:"bmV3c2xldHRlcg==";s:11:"widget_name";s:32:"U2lnbiBVcCBGb3IgTmV3c2xldHRlcg==";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:32:"U2lnbiBVcCBGb3IgTmV3c2xldHRlcg==";s:14:"widget_title_2";s:32:"U2lnbiBVcCBGb3IgTmV3c2xldHRlcg==";s:14:"widget_title_3";s:32:"U2lnbiBVcCBGb3IgTmV3c2xldHRlcg==";s:14:"widget_title_4";s:32:"U2lnbiBVcCBGb3IgTmV3c2xldHRlcg==";s:14:"widget_title_5";s:32:"U2lnbiBVcCBGb3IgTmV3c2xldHRlcg==";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:5:"class";s:20:"cHRzLW5ld3NsZXR0ZXI=";s:9:"imagefile";s:0:"";s:13:"information_1";s:144:"PHA+U2lnbiB1cCB0byBvdXIgbmV3c2xldHRlciBhbmQgZ2V0IGV4Y2x1c2l2ZSBkZWFscyB5b3Ugd29udCBmaW5kIGFueXdoZXJlIGVsc2Ugc3RyYWlnaHQgdG8geW91ciBpbmJveCE8L3A+";s:13:"information_2";s:144:"PHA+U2lnbiB1cCB0byBvdXIgbmV3c2xldHRlciBhbmQgZ2V0IGV4Y2x1c2l2ZSBkZWFscyB5b3Ugd29udCBmaW5kIGFueXdoZXJlIGVsc2Ugc3RyYWlnaHQgdG8geW91ciBpbmJveCE8L3A+";s:13:"information_3";s:144:"PHA+U2lnbiB1cCB0byBvdXIgbmV3c2xldHRlciBhbmQgZ2V0IGV4Y2x1c2l2ZSBkZWFscyB5b3Ugd29udCBmaW5kIGFueXdoZXJlIGVsc2Ugc3RyYWlnaHQgdG8geW91ciBpbmJveCE8L3A+";s:13:"information_4";s:144:"PHA+U2lnbiB1cCB0byBvdXIgbmV3c2xldHRlciBhbmQgZ2V0IGV4Y2x1c2l2ZSBkZWFscyB5b3Ugd29udCBmaW5kIGFueXdoZXJlIGVsc2Ugc3RyYWlnaHQgdG8geW91ciBpbmJveCE8L3A+";s:13:"information_5";s:144:"PHA+U2lnbiB1cCB0byBvdXIgbmV3c2xldHRlciBhbmQgZ2V0IGV4Y2x1c2l2ZSBkZWFscyB5b3Ugd29udCBmaW5kIGFueXdoZXJlIGVsc2Ugc3RyYWlnaHQgdG8geW91ciBpbmJveCE8L3A+";s:16:"newsletter_style";s:8:"c3R5bGUx";}s:4:"wkey";s:17:"key_1414831529453";}";}s:17:"key_1414831611780";a:1:{s:6:"config";s:5467:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzE2MTE3ODA=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:16:"SHRtbCBGb290ZXI=";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:16:"SHRtbCBGb290ZXI=";s:14:"widget_title_2";s:16:"SHRtbCBGb290ZXI=";s:14:"widget_title_3";s:16:"SHRtbCBGb290ZXI=";s:14:"widget_title_4";s:16:"SHRtbCBGb290ZXI=";s:14:"widget_title_5";s:16:"SHRtbCBGb290ZXI=";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:960:"PHVsIGNsYXNzPSJyb3cgIj4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14cy0xMiAiPg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxkaXYgY2xhc3M9ImN1c3RvbSBpbmZvLXNlcnZpY2VzIj4NCjxkaXYgY2xhc3M9ImluZm8tdGl0bGUgYm9yZGVyIHNtYWxsIj5jYWxsIHRoZSBjdXN0b21lciBzZXJ2aWNlczwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC1oaWdobGlnaHQiPjxzdHJvbmc+MTgwMCAtIDE1NzAgLSAwMDA8L3N0cm9uZz48L2Rpdj4NCjxkaXYgY2xhc3M9IiBib3JkZXIgc21hbGwiPkZyZWUgMjQvNyBzdXBwb3J0IGZvciBvdXIgY3VzdG9tZXJzPC9kaXY+DQo8L2Rpdj4NCjwvZGl2Pg0KPC9saT4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14czEyIj4NCjxkaXYgY2xhc3M9Iml0ZW0taHRtbCI+DQo8ZGl2IGNsYXNzPSJjdXN0b20gaW5mby1xdWVzdGlvbiI+DQo8ZGl2IGNsYXNzPSJpbmZvLXRpdGxlIGJvcmRlciBzbWFsbCI+ZW1haWwgdXMgeW91ciBxdWVzdGlvbjwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC13YXJuaW5nIj48c3Ryb25nPnN1cHBvcnRAbWl4c3RvcmUuY29tPC9zdHJvbmc+PC9kaXY+DQo8ZGl2IGNsYXNzPSIgYm9yZGVyIHNtYWxsIj5XZSB3aWxsIHJlc3BvbmQgd2l0aGluIDMgYnVzaW5lc3MgZGF5czwvZGl2Pg0KPC9kaXY+DQo8L2Rpdj4NCjwvbGk+DQo8L3VsPg==";s:13:"htmlcontent_2";s:960:"PHVsIGNsYXNzPSJyb3cgIj4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14cy0xMiAiPg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxkaXYgY2xhc3M9ImN1c3RvbSBpbmZvLXNlcnZpY2VzIj4NCjxkaXYgY2xhc3M9ImluZm8tdGl0bGUgYm9yZGVyIHNtYWxsIj5jYWxsIHRoZSBjdXN0b21lciBzZXJ2aWNlczwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC1oaWdobGlnaHQiPjxzdHJvbmc+MTgwMCAtIDE1NzAgLSAwMDA8L3N0cm9uZz48L2Rpdj4NCjxkaXYgY2xhc3M9IiBib3JkZXIgc21hbGwiPkZyZWUgMjQvNyBzdXBwb3J0IGZvciBvdXIgY3VzdG9tZXJzPC9kaXY+DQo8L2Rpdj4NCjwvZGl2Pg0KPC9saT4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14czEyIj4NCjxkaXYgY2xhc3M9Iml0ZW0taHRtbCI+DQo8ZGl2IGNsYXNzPSJjdXN0b20gaW5mby1xdWVzdGlvbiI+DQo8ZGl2IGNsYXNzPSJpbmZvLXRpdGxlIGJvcmRlciBzbWFsbCI+ZW1haWwgdXMgeW91ciBxdWVzdGlvbjwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC13YXJuaW5nIj48c3Ryb25nPnN1cHBvcnRAbWl4c3RvcmUuY29tPC9zdHJvbmc+PC9kaXY+DQo8ZGl2IGNsYXNzPSIgYm9yZGVyIHNtYWxsIj5XZSB3aWxsIHJlc3BvbmQgd2l0aGluIDMgYnVzaW5lc3MgZGF5czwvZGl2Pg0KPC9kaXY+DQo8L2Rpdj4NCjwvbGk+DQo8L3VsPg==";s:13:"htmlcontent_3";s:960:"PHVsIGNsYXNzPSJyb3cgIj4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14cy0xMiAiPg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxkaXYgY2xhc3M9ImN1c3RvbSBpbmZvLXNlcnZpY2VzIj4NCjxkaXYgY2xhc3M9ImluZm8tdGl0bGUgYm9yZGVyIHNtYWxsIj5jYWxsIHRoZSBjdXN0b21lciBzZXJ2aWNlczwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC1oaWdobGlnaHQiPjxzdHJvbmc+MTgwMCAtIDE1NzAgLSAwMDA8L3N0cm9uZz48L2Rpdj4NCjxkaXYgY2xhc3M9IiBib3JkZXIgc21hbGwiPkZyZWUgMjQvNyBzdXBwb3J0IGZvciBvdXIgY3VzdG9tZXJzPC9kaXY+DQo8L2Rpdj4NCjwvZGl2Pg0KPC9saT4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14czEyIj4NCjxkaXYgY2xhc3M9Iml0ZW0taHRtbCI+DQo8ZGl2IGNsYXNzPSJjdXN0b20gaW5mby1xdWVzdGlvbiI+DQo8ZGl2IGNsYXNzPSJpbmZvLXRpdGxlIGJvcmRlciBzbWFsbCI+ZW1haWwgdXMgeW91ciBxdWVzdGlvbjwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC13YXJuaW5nIj48c3Ryb25nPnN1cHBvcnRAbWl4c3RvcmUuY29tPC9zdHJvbmc+PC9kaXY+DQo8ZGl2IGNsYXNzPSIgYm9yZGVyIHNtYWxsIj5XZSB3aWxsIHJlc3BvbmQgd2l0aGluIDMgYnVzaW5lc3MgZGF5czwvZGl2Pg0KPC9kaXY+DQo8L2Rpdj4NCjwvbGk+DQo8L3VsPg==";s:13:"htmlcontent_4";s:960:"PHVsIGNsYXNzPSJyb3cgIj4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14cy0xMiAiPg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxkaXYgY2xhc3M9ImN1c3RvbSBpbmZvLXNlcnZpY2VzIj4NCjxkaXYgY2xhc3M9ImluZm8tdGl0bGUgYm9yZGVyIHNtYWxsIj5jYWxsIHRoZSBjdXN0b21lciBzZXJ2aWNlczwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC1oaWdobGlnaHQiPjxzdHJvbmc+MTgwMCAtIDE1NzAgLSAwMDA8L3N0cm9uZz48L2Rpdj4NCjxkaXYgY2xhc3M9IiBib3JkZXIgc21hbGwiPkZyZWUgMjQvNyBzdXBwb3J0IGZvciBvdXIgY3VzdG9tZXJzPC9kaXY+DQo8L2Rpdj4NCjwvZGl2Pg0KPC9saT4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14czEyIj4NCjxkaXYgY2xhc3M9Iml0ZW0taHRtbCI+DQo8ZGl2IGNsYXNzPSJjdXN0b20gaW5mby1xdWVzdGlvbiI+DQo8ZGl2IGNsYXNzPSJpbmZvLXRpdGxlIGJvcmRlciBzbWFsbCI+ZW1haWwgdXMgeW91ciBxdWVzdGlvbjwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC13YXJuaW5nIj48c3Ryb25nPnN1cHBvcnRAbWl4c3RvcmUuY29tPC9zdHJvbmc+PC9kaXY+DQo8ZGl2IGNsYXNzPSIgYm9yZGVyIHNtYWxsIj5XZSB3aWxsIHJlc3BvbmQgd2l0aGluIDMgYnVzaW5lc3MgZGF5czwvZGl2Pg0KPC9kaXY+DQo8L2Rpdj4NCjwvbGk+DQo8L3VsPg==";s:13:"htmlcontent_5";s:960:"PHVsIGNsYXNzPSJyb3cgIj4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14cy0xMiAiPg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxkaXYgY2xhc3M9ImN1c3RvbSBpbmZvLXNlcnZpY2VzIj4NCjxkaXYgY2xhc3M9ImluZm8tdGl0bGUgYm9yZGVyIHNtYWxsIj5jYWxsIHRoZSBjdXN0b21lciBzZXJ2aWNlczwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC1oaWdobGlnaHQiPjxzdHJvbmc+MTgwMCAtIDE1NzAgLSAwMDA8L3N0cm9uZz48L2Rpdj4NCjxkaXYgY2xhc3M9IiBib3JkZXIgc21hbGwiPkZyZWUgMjQvNyBzdXBwb3J0IGZvciBvdXIgY3VzdG9tZXJzPC9kaXY+DQo8L2Rpdj4NCjwvZGl2Pg0KPC9saT4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14czEyIj4NCjxkaXYgY2xhc3M9Iml0ZW0taHRtbCI+DQo8ZGl2IGNsYXNzPSJjdXN0b20gaW5mby1xdWVzdGlvbiI+DQo8ZGl2IGNsYXNzPSJpbmZvLXRpdGxlIGJvcmRlciBzbWFsbCI+ZW1haWwgdXMgeW91ciBxdWVzdGlvbjwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC13YXJuaW5nIj48c3Ryb25nPnN1cHBvcnRAbWl4c3RvcmUuY29tPC9zdHJvbmc+PC9kaXY+DQo8ZGl2IGNsYXNzPSIgYm9yZGVyIHNtYWxsIj5XZSB3aWxsIHJlc3BvbmQgd2l0aGluIDMgYnVzaW5lc3MgZGF5czwvZGl2Pg0KPC9kaXY+DQo8L2Rpdj4NCjwvbGk+DQo8L3VsPg==";}s:4:"wkey";s:17:"key_1414831611780";}";}}', 26, 0, 1, 1414833478),
	('FooterSport', 0, '', '[{"index":0,"cls":"pts-footer-top","bgcolor":"#eb5454","bgimage":"","fullwidth":"0","parallax":"0","sfxcls":null,"cols":[{"index":0,"cls":"hidden-xs","sfxcls":null,"bgcolor":"","bgimage":"","inrow":0,"lgcol":9,"mdcol":8,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831113755","name":"Logo Footer"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":3,"mdcol":4,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831222604","name":"Social footer"}],"rows":[]}]},{"index":0,"cls":"pts-footer-center","bgcolor":"","bgimage":"","fullwidth":"0","parallax":"0","sfxcls":null,"cols":[{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":3,"mdcol":3,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831295069","name":"Contact info"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":2,"mdcol":2,"smcol":4,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831349733","name":"Customer Service"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":2,"mdcol":2,"smcol":4,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831415734","name":"Information"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":2,"mdcol":2,"smcol":4,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831468547","name":"My account"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":3,"mdcol":3,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"twitter","wkey":"key_1415605119427","name":"Last Tweet"}],"rows":[]}]},{"index":0,"cls":"hidden-xs","bgcolor":"","bgimage":"bg-footerbottom-sport.jpg","fullwidth":"0","parallax":"1","sfxcls":null,"cols":[{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":12,"mdcol":12,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831611780","name":"Html Footer"}],"rows":[]}]}]', 'a:8:{s:17:"key_1414831113755";a:1:{s:6:"config";s:2547:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzExMTM3NTU=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:16:"TG9nbyBGb290ZXI=";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:16:"TG9nbyBGb290ZXI=";s:14:"widget_title_2";s:16:"TG9nbyBGb290ZXI=";s:14:"widget_title_3";s:16:"TG9nbyBGb290ZXI=";s:14:"widget_title_4";s:16:"TG9nbyBGb290ZXI=";s:14:"widget_title_5";s:16:"TG9nbyBGb290ZXI=";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:328:"PGRpdiBjbGFzcz0iaW1hZ2UtYmFubmVyICBwdWxsLWxlZnQiPjxpbWcgY2xhc3M9Iml0ZW0taW1nIiBzcmM9Im1vZHVsZXMvcHRzcGFnZWJ1aWxkZXIvaW1hZ2VzL2xvZ29zcG9ydC5wbmciIGFsdD0iIiAvPjwvZGl2Pg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxwPldl4oCZcmUgY29uZmlkZW50IHdl4oCZdmUgcHJvdmlkZWQgYWxsIHRoZSBiZXN0IGZvciB5b3UuIFN0YXkgY29ubmVlY3Qgd2l0aCB1czwvcD4NCjwvZGl2Pg==";s:13:"htmlcontent_2";s:388:"PGRpdiBjbGFzcz0iaW1hZ2UtYmFubmVyICBwdWxsLWxlZnQiPjxpbWcgY2xhc3M9Iml0ZW0taW1nIiBzcmM9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW1nL2Ntcy9sb2dvLWZvb3Rlci5wbmciIGFsdD0iIiB3aWR0aD0iMTMxIiBoZWlnaHQ9IjQwIiAvPjwvZGl2Pg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxwPldl4oCZcmUgY29uZmlkZW50IHdl4oCZdmUgcHJvdmlkZWQgYWxsIHRoZSBiZXN0IGZvciB5b3UuIFN0YXkgY29ubmVlY3Qgd2l0aCB1czwvcD4NCjwvZGl2Pg==";s:13:"htmlcontent_3";s:388:"PGRpdiBjbGFzcz0iaW1hZ2UtYmFubmVyICBwdWxsLWxlZnQiPjxpbWcgY2xhc3M9Iml0ZW0taW1nIiBzcmM9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW1nL2Ntcy9sb2dvLWZvb3Rlci5wbmciIGFsdD0iIiB3aWR0aD0iMTMxIiBoZWlnaHQ9IjQwIiAvPjwvZGl2Pg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxwPldl4oCZcmUgY29uZmlkZW50IHdl4oCZdmUgcHJvdmlkZWQgYWxsIHRoZSBiZXN0IGZvciB5b3UuIFN0YXkgY29ubmVlY3Qgd2l0aCB1czwvcD4NCjwvZGl2Pg==";s:13:"htmlcontent_4";s:388:"PGRpdiBjbGFzcz0iaW1hZ2UtYmFubmVyICBwdWxsLWxlZnQiPjxpbWcgY2xhc3M9Iml0ZW0taW1nIiBzcmM9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW1nL2Ntcy9sb2dvLWZvb3Rlci5wbmciIGFsdD0iIiB3aWR0aD0iMTMxIiBoZWlnaHQ9IjQwIiAvPjwvZGl2Pg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxwPldl4oCZcmUgY29uZmlkZW50IHdl4oCZdmUgcHJvdmlkZWQgYWxsIHRoZSBiZXN0IGZvciB5b3UuIFN0YXkgY29ubmVlY3Qgd2l0aCB1czwvcD4NCjwvZGl2Pg==";s:13:"htmlcontent_5";s:388:"PGRpdiBjbGFzcz0iaW1hZ2UtYmFubmVyICBwdWxsLWxlZnQiPjxpbWcgY2xhc3M9Iml0ZW0taW1nIiBzcmM9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW1nL2Ntcy9sb2dvLWZvb3Rlci5wbmciIGFsdD0iIiB3aWR0aD0iMTMxIiBoZWlnaHQ9IjQwIiAvPjwvZGl2Pg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxwPldl4oCZcmUgY29uZmlkZW50IHdl4oCZdmUgcHJvdmlkZWQgYWxsIHRoZSBiZXN0IGZvciB5b3UuIFN0YXkgY29ubmVlY3Qgd2l0aCB1czwvcD4NCjwvZGl2Pg==";}s:4:"wkey";s:17:"key_1414831113755";}";}s:17:"key_1414831222604";a:1:{s:6:"config";s:6557:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzEyMjI2MDQ=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:20:"U29jaWFsIGZvb3Rlcg==";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:20:"U29jaWFsIGZvb3Rlcg==";s:14:"widget_title_2";s:20:"U29jaWFsIGZvb3Rlcg==";s:14:"widget_title_3";s:20:"U29jaWFsIGZvb3Rlcg==";s:14:"widget_title_4";s:20:"U29jaWFsIGZvb3Rlcg==";s:14:"widget_title_5";s:20:"U29jaWFsIGZvb3Rlcg==";s:12:"addition_cls";s:20:"c29jaWFsLWZvb3Rlcg==";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:1168:"PGRpdiBpZD0ic29jaWFsX2Jsb2NrIj4NCjx1bCBjbGFzcz0ic29jaWFsIj4NCjxsaSBjbGFzcz0iZmFjZWJvb2siPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy5mYWNlYm9vay5jb20vcHJlc3Rhc2hvcCIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IkZhY2Vib29rIj4gRmFjZWJvb2sgPC9hPjwvbGk+DQo8bGkgY2xhc3M9InR3aXR0ZXIiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy50d2l0dGVyLmNvbS9wcmVzdGFzaG9wIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iVHdpdHRlciI+VHdpdHRlciA8L2E+PC9saT4NCjxsaSBjbGFzcz0icnNzIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9Imh0dHA6Ly93d3cucHJlc3Rhc2hvcC5jb20vYmxvZy9lbi9mZWVkLyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlJTUyI+IFJTUzwvYT48L2xpPg0KPGxpIGNsYXNzPSJ5b3V0dWJlIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9IiMiIHRhcmdldD0iX2JsYW5rIiBkYXRhLW9yaWdpbmFsLXRpdGxlPSJZb3V0dWJlIj4gWW91dHViZTwvYT48L2xpPg0KPGxpIGNsYXNzPSJnb29nbGUtcGx1cyI+PGEgY2xhc3M9ImJ0bi10b29sdGlwIiBocmVmPSIjIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iR29vZ2xlIFBsdXMiPkdvb2dsZSBQbHVzIDwvYT48L2xpPg0KPGxpIGNsYXNzPSJwaW50ZXJlc3QiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iIyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlBpbnRlcmVzdCI+IFBpbnRlcmVzdDwvYT48L2xpPg0KPC91bD4NCjwvZGl2Pg==";s:13:"htmlcontent_2";s:1168:"PGRpdiBpZD0ic29jaWFsX2Jsb2NrIj4NCjx1bCBjbGFzcz0ic29jaWFsIj4NCjxsaSBjbGFzcz0iZmFjZWJvb2siPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy5mYWNlYm9vay5jb20vcHJlc3Rhc2hvcCIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IkZhY2Vib29rIj4gRmFjZWJvb2sgPC9hPjwvbGk+DQo8bGkgY2xhc3M9InR3aXR0ZXIiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy50d2l0dGVyLmNvbS9wcmVzdGFzaG9wIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iVHdpdHRlciI+VHdpdHRlciA8L2E+PC9saT4NCjxsaSBjbGFzcz0icnNzIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9Imh0dHA6Ly93d3cucHJlc3Rhc2hvcC5jb20vYmxvZy9lbi9mZWVkLyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlJTUyI+IFJTUzwvYT48L2xpPg0KPGxpIGNsYXNzPSJ5b3V0dWJlIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9IiMiIHRhcmdldD0iX2JsYW5rIiBkYXRhLW9yaWdpbmFsLXRpdGxlPSJZb3V0dWJlIj4gWW91dHViZTwvYT48L2xpPg0KPGxpIGNsYXNzPSJnb29nbGUtcGx1cyI+PGEgY2xhc3M9ImJ0bi10b29sdGlwIiBocmVmPSIjIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iR29vZ2xlIFBsdXMiPkdvb2dsZSBQbHVzIDwvYT48L2xpPg0KPGxpIGNsYXNzPSJwaW50ZXJlc3QiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iIyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlBpbnRlcmVzdCI+IFBpbnRlcmVzdDwvYT48L2xpPg0KPC91bD4NCjwvZGl2Pg==";s:13:"htmlcontent_3";s:1168:"PGRpdiBpZD0ic29jaWFsX2Jsb2NrIj4NCjx1bCBjbGFzcz0ic29jaWFsIj4NCjxsaSBjbGFzcz0iZmFjZWJvb2siPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy5mYWNlYm9vay5jb20vcHJlc3Rhc2hvcCIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IkZhY2Vib29rIj4gRmFjZWJvb2sgPC9hPjwvbGk+DQo8bGkgY2xhc3M9InR3aXR0ZXIiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy50d2l0dGVyLmNvbS9wcmVzdGFzaG9wIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iVHdpdHRlciI+VHdpdHRlciA8L2E+PC9saT4NCjxsaSBjbGFzcz0icnNzIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9Imh0dHA6Ly93d3cucHJlc3Rhc2hvcC5jb20vYmxvZy9lbi9mZWVkLyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlJTUyI+IFJTUzwvYT48L2xpPg0KPGxpIGNsYXNzPSJ5b3V0dWJlIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9IiMiIHRhcmdldD0iX2JsYW5rIiBkYXRhLW9yaWdpbmFsLXRpdGxlPSJZb3V0dWJlIj4gWW91dHViZTwvYT48L2xpPg0KPGxpIGNsYXNzPSJnb29nbGUtcGx1cyI+PGEgY2xhc3M9ImJ0bi10b29sdGlwIiBocmVmPSIjIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iR29vZ2xlIFBsdXMiPkdvb2dsZSBQbHVzIDwvYT48L2xpPg0KPGxpIGNsYXNzPSJwaW50ZXJlc3QiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iIyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlBpbnRlcmVzdCI+IFBpbnRlcmVzdDwvYT48L2xpPg0KPC91bD4NCjwvZGl2Pg==";s:13:"htmlcontent_4";s:1168:"PGRpdiBpZD0ic29jaWFsX2Jsb2NrIj4NCjx1bCBjbGFzcz0ic29jaWFsIj4NCjxsaSBjbGFzcz0iZmFjZWJvb2siPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy5mYWNlYm9vay5jb20vcHJlc3Rhc2hvcCIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IkZhY2Vib29rIj4gRmFjZWJvb2sgPC9hPjwvbGk+DQo8bGkgY2xhc3M9InR3aXR0ZXIiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy50d2l0dGVyLmNvbS9wcmVzdGFzaG9wIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iVHdpdHRlciI+VHdpdHRlciA8L2E+PC9saT4NCjxsaSBjbGFzcz0icnNzIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9Imh0dHA6Ly93d3cucHJlc3Rhc2hvcC5jb20vYmxvZy9lbi9mZWVkLyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlJTUyI+IFJTUzwvYT48L2xpPg0KPGxpIGNsYXNzPSJ5b3V0dWJlIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9IiMiIHRhcmdldD0iX2JsYW5rIiBkYXRhLW9yaWdpbmFsLXRpdGxlPSJZb3V0dWJlIj4gWW91dHViZTwvYT48L2xpPg0KPGxpIGNsYXNzPSJnb29nbGUtcGx1cyI+PGEgY2xhc3M9ImJ0bi10b29sdGlwIiBocmVmPSIjIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iR29vZ2xlIFBsdXMiPkdvb2dsZSBQbHVzIDwvYT48L2xpPg0KPGxpIGNsYXNzPSJwaW50ZXJlc3QiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iIyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlBpbnRlcmVzdCI+IFBpbnRlcmVzdDwvYT48L2xpPg0KPC91bD4NCjwvZGl2Pg==";s:13:"htmlcontent_5";s:1168:"PGRpdiBpZD0ic29jaWFsX2Jsb2NrIj4NCjx1bCBjbGFzcz0ic29jaWFsIj4NCjxsaSBjbGFzcz0iZmFjZWJvb2siPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy5mYWNlYm9vay5jb20vcHJlc3Rhc2hvcCIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IkZhY2Vib29rIj4gRmFjZWJvb2sgPC9hPjwvbGk+DQo8bGkgY2xhc3M9InR3aXR0ZXIiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy50d2l0dGVyLmNvbS9wcmVzdGFzaG9wIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iVHdpdHRlciI+VHdpdHRlciA8L2E+PC9saT4NCjxsaSBjbGFzcz0icnNzIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9Imh0dHA6Ly93d3cucHJlc3Rhc2hvcC5jb20vYmxvZy9lbi9mZWVkLyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlJTUyI+IFJTUzwvYT48L2xpPg0KPGxpIGNsYXNzPSJ5b3V0dWJlIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9IiMiIHRhcmdldD0iX2JsYW5rIiBkYXRhLW9yaWdpbmFsLXRpdGxlPSJZb3V0dWJlIj4gWW91dHViZTwvYT48L2xpPg0KPGxpIGNsYXNzPSJnb29nbGUtcGx1cyI+PGEgY2xhc3M9ImJ0bi10b29sdGlwIiBocmVmPSIjIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iR29vZ2xlIFBsdXMiPkdvb2dsZSBQbHVzIDwvYT48L2xpPg0KPGxpIGNsYXNzPSJwaW50ZXJlc3QiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iIyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlBpbnRlcmVzdCI+IFBpbnRlcmVzdDwvYT48L2xpPg0KPC91bD4NCjwvZGl2Pg==";}s:4:"wkey";s:17:"key_1414831222604";}";}s:17:"key_1414831295069";a:1:{s:6:"config";s:4247:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzEyOTUwNjk=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:16:"Q29udGFjdCBpbmZv";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:16:"Q29udGFjdCBpbmZv";s:14:"widget_title_2";s:16:"Q29udGFjdCBpbmZv";s:14:"widget_title_3";s:16:"Q29udGFjdCBpbmZv";s:14:"widget_title_4";s:16:"Q29udGFjdCBpbmZv";s:14:"widget_title_5";s:16:"Q29udGFjdCBpbmZv";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:716:"PGRpdiBjbGFzcz0ibGlzdC1ncm91cCI+U2VkIG5vbiBtYXVyaXMgdml0YWUgZXJhdCBjb25zZXF1YXQgYXVjdG9yIGV1IGluIGVsaXQuIENsYXNzIGFwdGVudCB0YWNpdGkgc29zcXUgYWQgbGl0b3JhIHRvcnF1ZW50IHBlciBjb251YmlhPC9kaXY+DQo8dWwgY2xhc3M9Imxpc3QtZ3JvdXAiPg0KPGxpPk5vIDExMDQgU2t5IFRvd2VyLCBOZXd5b3JrLCBVU0E8L2xpPg0KPGxpPkNhbGwgdXMgbm93OiAwMTIzLTQ1Ni03ODk8L2xpPg0KPGxpPkVtYWlsOiA8YSBocmVmPSJtYWlsdG86JTczJTYxJTZjJTY1JTczQCU3OSU2ZiU3NSU3MiU2MyU2ZiU2ZCU3MCU2MSU2ZSU3OS4lNjMlNmYlNmQiPnNhbGVzQHlvdXJjb21wYW55LmNvbTwvYT48L2xpPg0KPC91bD4NCjxkaXYgY2xhc3M9Im1hcCI+PGEgaWQ9ImZhbmN5Ym94LW1hcCIgdGl0bGU9IlZpZXdNYXAiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9Y29udGFjdCZjb250ZW50X29ubHk9MSI+Vmlld01hcCA8L2E+PC9kaXY+";s:13:"htmlcontent_2";s:716:"PGRpdiBjbGFzcz0ibGlzdC1ncm91cCI+U2VkIG5vbiBtYXVyaXMgdml0YWUgZXJhdCBjb25zZXF1YXQgYXVjdG9yIGV1IGluIGVsaXQuIENsYXNzIGFwdGVudCB0YWNpdGkgc29zcXUgYWQgbGl0b3JhIHRvcnF1ZW50IHBlciBjb251YmlhPC9kaXY+DQo8dWwgY2xhc3M9Imxpc3QtZ3JvdXAiPg0KPGxpPk5vIDExMDQgU2t5IFRvd2VyLCBOZXd5b3JrLCBVU0E8L2xpPg0KPGxpPkNhbGwgdXMgbm93OiAwMTIzLTQ1Ni03ODk8L2xpPg0KPGxpPkVtYWlsOiA8YSBocmVmPSJtYWlsdG86JTczJTYxJTZjJTY1JTczQCU3OSU2ZiU3NSU3MiU2MyU2ZiU2ZCU3MCU2MSU2ZSU3OS4lNjMlNmYlNmQiPnNhbGVzQHlvdXJjb21wYW55LmNvbTwvYT48L2xpPg0KPC91bD4NCjxkaXYgY2xhc3M9Im1hcCI+PGEgaWQ9ImZhbmN5Ym94LW1hcCIgdGl0bGU9IlZpZXdNYXAiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9Y29udGFjdCZjb250ZW50X29ubHk9MSI+Vmlld01hcCA8L2E+PC9kaXY+";s:13:"htmlcontent_3";s:716:"PGRpdiBjbGFzcz0ibGlzdC1ncm91cCI+U2VkIG5vbiBtYXVyaXMgdml0YWUgZXJhdCBjb25zZXF1YXQgYXVjdG9yIGV1IGluIGVsaXQuIENsYXNzIGFwdGVudCB0YWNpdGkgc29zcXUgYWQgbGl0b3JhIHRvcnF1ZW50IHBlciBjb251YmlhPC9kaXY+DQo8dWwgY2xhc3M9Imxpc3QtZ3JvdXAiPg0KPGxpPk5vIDExMDQgU2t5IFRvd2VyLCBOZXd5b3JrLCBVU0E8L2xpPg0KPGxpPkNhbGwgdXMgbm93OiAwMTIzLTQ1Ni03ODk8L2xpPg0KPGxpPkVtYWlsOiA8YSBocmVmPSJtYWlsdG86JTczJTYxJTZjJTY1JTczQCU3OSU2ZiU3NSU3MiU2MyU2ZiU2ZCU3MCU2MSU2ZSU3OS4lNjMlNmYlNmQiPnNhbGVzQHlvdXJjb21wYW55LmNvbTwvYT48L2xpPg0KPC91bD4NCjxkaXYgY2xhc3M9Im1hcCI+PGEgaWQ9ImZhbmN5Ym94LW1hcCIgdGl0bGU9IlZpZXdNYXAiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9Y29udGFjdCZjb250ZW50X29ubHk9MSI+Vmlld01hcCA8L2E+PC9kaXY+";s:13:"htmlcontent_4";s:716:"PGRpdiBjbGFzcz0ibGlzdC1ncm91cCI+U2VkIG5vbiBtYXVyaXMgdml0YWUgZXJhdCBjb25zZXF1YXQgYXVjdG9yIGV1IGluIGVsaXQuIENsYXNzIGFwdGVudCB0YWNpdGkgc29zcXUgYWQgbGl0b3JhIHRvcnF1ZW50IHBlciBjb251YmlhPC9kaXY+DQo8dWwgY2xhc3M9Imxpc3QtZ3JvdXAiPg0KPGxpPk5vIDExMDQgU2t5IFRvd2VyLCBOZXd5b3JrLCBVU0E8L2xpPg0KPGxpPkNhbGwgdXMgbm93OiAwMTIzLTQ1Ni03ODk8L2xpPg0KPGxpPkVtYWlsOiA8YSBocmVmPSJtYWlsdG86JTczJTYxJTZjJTY1JTczQCU3OSU2ZiU3NSU3MiU2MyU2ZiU2ZCU3MCU2MSU2ZSU3OS4lNjMlNmYlNmQiPnNhbGVzQHlvdXJjb21wYW55LmNvbTwvYT48L2xpPg0KPC91bD4NCjxkaXYgY2xhc3M9Im1hcCI+PGEgaWQ9ImZhbmN5Ym94LW1hcCIgdGl0bGU9IlZpZXdNYXAiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9Y29udGFjdCZjb250ZW50X29ubHk9MSI+Vmlld01hcCA8L2E+PC9kaXY+";s:13:"htmlcontent_5";s:716:"PGRpdiBjbGFzcz0ibGlzdC1ncm91cCI+U2VkIG5vbiBtYXVyaXMgdml0YWUgZXJhdCBjb25zZXF1YXQgYXVjdG9yIGV1IGluIGVsaXQuIENsYXNzIGFwdGVudCB0YWNpdGkgc29zcXUgYWQgbGl0b3JhIHRvcnF1ZW50IHBlciBjb251YmlhPC9kaXY+DQo8dWwgY2xhc3M9Imxpc3QtZ3JvdXAiPg0KPGxpPk5vIDExMDQgU2t5IFRvd2VyLCBOZXd5b3JrLCBVU0E8L2xpPg0KPGxpPkNhbGwgdXMgbm93OiAwMTIzLTQ1Ni03ODk8L2xpPg0KPGxpPkVtYWlsOiA8YSBocmVmPSJtYWlsdG86JTczJTYxJTZjJTY1JTczQCU3OSU2ZiU3NSU3MiU2MyU2ZiU2ZCU3MCU2MSU2ZSU3OS4lNjMlNmYlNmQiPnNhbGVzQHlvdXJjb21wYW55LmNvbTwvYT48L2xpPg0KPC91bD4NCjxkaXYgY2xhc3M9Im1hcCI+PGEgaWQ9ImZhbmN5Ym94LW1hcCIgdGl0bGU9IlZpZXdNYXAiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9Y29udGFjdCZjb250ZW50X29ubHk9MSI+Vmlld01hcCA8L2E+PC9kaXY+";}s:4:"wkey";s:17:"key_1414831295069";}";}s:17:"key_1414831349733";a:1:{s:6:"config";s:3535:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzEzNDk3MzM=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:24:"Q3VzdG9tZXIgU2VydmljZQ==";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:24:"Q3VzdG9tZXIgU2VydmljZQ==";s:14:"widget_title_2";s:24:"Q3VzdG9tZXIgU2VydmljZQ==";s:14:"widget_title_3";s:24:"Q3VzdG9tZXIgU2VydmljZQ==";s:14:"widget_title_4";s:24:"Q3VzdG9tZXIgU2VydmljZQ==";s:14:"widget_title_5";s:24:"Q3VzdG9tZXIgU2VydmljZQ==";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:564:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iT3VyIHN0b3JlcyIgaHJlZj0iaW5kZXgucGhwP2NvbnRyb2xsZXI9c3RvcmVzIj4gT3VyIHN0b3JlcyA8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IkNvbnRhY3QgdXMiIGhyZWY9ImluZGV4LnBocD9jb250cm9sbGVyPWNvbnRhY3QiPiBDb250YWN0IHVzIDwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJTaXRlbWFwIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zaXRlbWFwIj4gU2l0ZW1hcCA8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iVmlldyBhIGxpc3Qgb2Ygc3VwcGxpZXJzIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zdXBwbGllciI+IFN1cHBsaWVycyA8L2E+PC9saT4NCjwvdWw+";s:13:"htmlcontent_2";s:564:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iT3VyIHN0b3JlcyIgaHJlZj0iaW5kZXgucGhwP2NvbnRyb2xsZXI9c3RvcmVzIj4gT3VyIHN0b3JlcyA8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IkNvbnRhY3QgdXMiIGhyZWY9ImluZGV4LnBocD9jb250cm9sbGVyPWNvbnRhY3QiPiBDb250YWN0IHVzIDwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJTaXRlbWFwIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zaXRlbWFwIj4gU2l0ZW1hcCA8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iVmlldyBhIGxpc3Qgb2Ygc3VwcGxpZXJzIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zdXBwbGllciI+IFN1cHBsaWVycyA8L2E+PC9saT4NCjwvdWw+";s:13:"htmlcontent_3";s:564:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iT3VyIHN0b3JlcyIgaHJlZj0iaW5kZXgucGhwP2NvbnRyb2xsZXI9c3RvcmVzIj4gT3VyIHN0b3JlcyA8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IkNvbnRhY3QgdXMiIGhyZWY9ImluZGV4LnBocD9jb250cm9sbGVyPWNvbnRhY3QiPiBDb250YWN0IHVzIDwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJTaXRlbWFwIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zaXRlbWFwIj4gU2l0ZW1hcCA8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iVmlldyBhIGxpc3Qgb2Ygc3VwcGxpZXJzIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zdXBwbGllciI+IFN1cHBsaWVycyA8L2E+PC9saT4NCjwvdWw+";s:13:"htmlcontent_4";s:564:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iT3VyIHN0b3JlcyIgaHJlZj0iaW5kZXgucGhwP2NvbnRyb2xsZXI9c3RvcmVzIj4gT3VyIHN0b3JlcyA8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IkNvbnRhY3QgdXMiIGhyZWY9ImluZGV4LnBocD9jb250cm9sbGVyPWNvbnRhY3QiPiBDb250YWN0IHVzIDwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJTaXRlbWFwIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zaXRlbWFwIj4gU2l0ZW1hcCA8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iVmlldyBhIGxpc3Qgb2Ygc3VwcGxpZXJzIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zdXBwbGllciI+IFN1cHBsaWVycyA8L2E+PC9saT4NCjwvdWw+";s:13:"htmlcontent_5";s:564:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iT3VyIHN0b3JlcyIgaHJlZj0iaW5kZXgucGhwP2NvbnRyb2xsZXI9c3RvcmVzIj4gT3VyIHN0b3JlcyA8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IkNvbnRhY3QgdXMiIGhyZWY9ImluZGV4LnBocD9jb250cm9sbGVyPWNvbnRhY3QiPiBDb250YWN0IHVzIDwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJTaXRlbWFwIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zaXRlbWFwIj4gU2l0ZW1hcCA8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iVmlldyBhIGxpc3Qgb2Ygc3VwcGxpZXJzIiBocmVmPSJpbmRleC5waHA/Y29udHJvbGxlcj1zdXBwbGllciI+IFN1cHBsaWVycyA8L2E+PC9saT4NCjwvdWw+";}s:4:"wkey";s:17:"key_1414831349733";}";}s:17:"key_1414831415734";a:1:{s:6:"config";s:3187:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzE0MTU3MzQ=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:16:"SW5mb3JtYXRpb24=";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:16:"SW5mb3JtYXRpb24=";s:14:"widget_title_2";s:16:"SW5mb3JtYXRpb24=";s:14:"widget_title_3";s:16:"SW5mb3JtYXRpb24=";s:14:"widget_title_4";s:16:"SW5mb3JtYXRpb24=";s:14:"widget_title_5";s:16:"SW5mb3JtYXRpb24=";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:504:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj5NaSB2aXRhZSBtYWduaXM8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+IEZ1c2NlIGxhb3JlZXQ8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+VmVzdGlidSBmYXVjaWJ1czwvYT48L2xpPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj4gUG9ydHRpdG9yIGxhb3JlZXQgPC9hPjwvbGk+DQo8bGkgY2xhc3M9Iml0ZW0iPjxhIHRpdGxlPSIjIiBocmVmPSIjIj4gRmVsaXMgcG9ydHRpdG9yIDwvYT48L2xpPg0KPC91bD4=";s:13:"htmlcontent_2";s:504:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj5NaSB2aXRhZSBtYWduaXM8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+IEZ1c2NlIGxhb3JlZXQ8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+VmVzdGlidSBmYXVjaWJ1czwvYT48L2xpPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj4gUG9ydHRpdG9yIGxhb3JlZXQgPC9hPjwvbGk+DQo8bGkgY2xhc3M9Iml0ZW0iPjxhIHRpdGxlPSIjIiBocmVmPSIjIj4gRmVsaXMgcG9ydHRpdG9yIDwvYT48L2xpPg0KPC91bD4=";s:13:"htmlcontent_3";s:504:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj5NaSB2aXRhZSBtYWduaXM8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+IEZ1c2NlIGxhb3JlZXQ8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+VmVzdGlidSBmYXVjaWJ1czwvYT48L2xpPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj4gUG9ydHRpdG9yIGxhb3JlZXQgPC9hPjwvbGk+DQo8bGkgY2xhc3M9Iml0ZW0iPjxhIHRpdGxlPSIjIiBocmVmPSIjIj4gRmVsaXMgcG9ydHRpdG9yIDwvYT48L2xpPg0KPC91bD4=";s:13:"htmlcontent_4";s:504:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj5NaSB2aXRhZSBtYWduaXM8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+IEZ1c2NlIGxhb3JlZXQ8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+VmVzdGlidSBmYXVjaWJ1czwvYT48L2xpPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj4gUG9ydHRpdG9yIGxhb3JlZXQgPC9hPjwvbGk+DQo8bGkgY2xhc3M9Iml0ZW0iPjxhIHRpdGxlPSIjIiBocmVmPSIjIj4gRmVsaXMgcG9ydHRpdG9yIDwvYT48L2xpPg0KPC91bD4=";s:13:"htmlcontent_5";s:504:"PHVsIGNsYXNzPSIgbGlzdC1ncm91cCBsaXN0LWJsb2NrIG5vc3R5bGUiPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj5NaSB2aXRhZSBtYWduaXM8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+IEZ1c2NlIGxhb3JlZXQ8L2E+PC9saT4NCjxsaSBjbGFzcz0iaXRlbSI+PGEgdGl0bGU9IiIgaHJlZj0iIyI+VmVzdGlidSBmYXVjaWJ1czwvYT48L2xpPg0KPGxpIGNsYXNzPSJpdGVtIj48YSB0aXRsZT0iIiBocmVmPSIjIj4gUG9ydHRpdG9yIGxhb3JlZXQgPC9hPjwvbGk+DQo8bGkgY2xhc3M9Iml0ZW0iPjxhIHRpdGxlPSIjIiBocmVmPSIjIj4gRmVsaXMgcG9ydHRpdG9yIDwvYT48L2xpPg0KPC91bD4=";}s:4:"wkey";s:17:"key_1414831415734";}";}s:17:"key_1414831468547";a:1:{s:6:"config";s:5007:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzE0Njg1NDc=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:16:"TXkgYWNjb3VudA==";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:16:"TXkgYWNjb3VudA==";s:14:"widget_title_2";s:16:"TXkgYWNjb3VudA==";s:14:"widget_title_3";s:16:"TXkgYWNjb3VudA==";s:14:"widget_title_4";s:16:"TXkgYWNjb3VudA==";s:14:"widget_title_5";s:16:"TXkgYWNjb3VudA==";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:868:"PHVsIGNsYXNzPSJsaXN0LWJsb2NrIGxpc3QtZ3JvdXAgbm9zdHlsZSI+DQo8bGk+PGEgdGl0bGU9Ik15IG9yZGVycyIgaHJlZj0iaHR0cDovL2xvY2FsaG9zdC9wcmVzdGFzaG9wL3BmX21peG1vYmlsZS9pbmRleC5waHA/Y29udHJvbGxlcj1oaXN0b3J5IiByZWw9Im5vZm9sbG93Ij5NeSBvcmRlcnM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTXkgY3JlZGl0IHNsaXBzIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPW9yZGVyLXNsaXAiIHJlbD0ibm9mb2xsb3ciPk15IGNyZWRpdCBzbGlwczwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJNeSBhZGRyZXNzZXMiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9YWRkcmVzc2VzIiByZWw9Im5vZm9sbG93Ij5NeSBhZGRyZXNzZXM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTWFuYWdlIG15IHBlcnNvbmFsIGluZm9ybWF0aW9uIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPWlkZW50aXR5IiByZWw9Im5vZm9sbG93Ij5NeSBwZXJzb25hbCBpbmZvPC9hPjwvbGk+DQo8L3VsPg==";s:13:"htmlcontent_2";s:868:"PHVsIGNsYXNzPSJsaXN0LWJsb2NrIGxpc3QtZ3JvdXAgbm9zdHlsZSI+DQo8bGk+PGEgdGl0bGU9Ik15IG9yZGVycyIgaHJlZj0iaHR0cDovL2xvY2FsaG9zdC9wcmVzdGFzaG9wL3BmX21peG1vYmlsZS9pbmRleC5waHA/Y29udHJvbGxlcj1oaXN0b3J5IiByZWw9Im5vZm9sbG93Ij5NeSBvcmRlcnM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTXkgY3JlZGl0IHNsaXBzIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPW9yZGVyLXNsaXAiIHJlbD0ibm9mb2xsb3ciPk15IGNyZWRpdCBzbGlwczwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJNeSBhZGRyZXNzZXMiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9YWRkcmVzc2VzIiByZWw9Im5vZm9sbG93Ij5NeSBhZGRyZXNzZXM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTWFuYWdlIG15IHBlcnNvbmFsIGluZm9ybWF0aW9uIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPWlkZW50aXR5IiByZWw9Im5vZm9sbG93Ij5NeSBwZXJzb25hbCBpbmZvPC9hPjwvbGk+DQo8L3VsPg==";s:13:"htmlcontent_3";s:868:"PHVsIGNsYXNzPSJsaXN0LWJsb2NrIGxpc3QtZ3JvdXAgbm9zdHlsZSI+DQo8bGk+PGEgdGl0bGU9Ik15IG9yZGVycyIgaHJlZj0iaHR0cDovL2xvY2FsaG9zdC9wcmVzdGFzaG9wL3BmX21peG1vYmlsZS9pbmRleC5waHA/Y29udHJvbGxlcj1oaXN0b3J5IiByZWw9Im5vZm9sbG93Ij5NeSBvcmRlcnM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTXkgY3JlZGl0IHNsaXBzIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPW9yZGVyLXNsaXAiIHJlbD0ibm9mb2xsb3ciPk15IGNyZWRpdCBzbGlwczwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJNeSBhZGRyZXNzZXMiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9YWRkcmVzc2VzIiByZWw9Im5vZm9sbG93Ij5NeSBhZGRyZXNzZXM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTWFuYWdlIG15IHBlcnNvbmFsIGluZm9ybWF0aW9uIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPWlkZW50aXR5IiByZWw9Im5vZm9sbG93Ij5NeSBwZXJzb25hbCBpbmZvPC9hPjwvbGk+DQo8L3VsPg==";s:13:"htmlcontent_4";s:868:"PHVsIGNsYXNzPSJsaXN0LWJsb2NrIGxpc3QtZ3JvdXAgbm9zdHlsZSI+DQo8bGk+PGEgdGl0bGU9Ik15IG9yZGVycyIgaHJlZj0iaHR0cDovL2xvY2FsaG9zdC9wcmVzdGFzaG9wL3BmX21peG1vYmlsZS9pbmRleC5waHA/Y29udHJvbGxlcj1oaXN0b3J5IiByZWw9Im5vZm9sbG93Ij5NeSBvcmRlcnM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTXkgY3JlZGl0IHNsaXBzIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPW9yZGVyLXNsaXAiIHJlbD0ibm9mb2xsb3ciPk15IGNyZWRpdCBzbGlwczwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJNeSBhZGRyZXNzZXMiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9YWRkcmVzc2VzIiByZWw9Im5vZm9sbG93Ij5NeSBhZGRyZXNzZXM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTWFuYWdlIG15IHBlcnNvbmFsIGluZm9ybWF0aW9uIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPWlkZW50aXR5IiByZWw9Im5vZm9sbG93Ij5NeSBwZXJzb25hbCBpbmZvPC9hPjwvbGk+DQo8L3VsPg==";s:13:"htmlcontent_5";s:868:"PHVsIGNsYXNzPSJsaXN0LWJsb2NrIGxpc3QtZ3JvdXAgbm9zdHlsZSI+DQo8bGk+PGEgdGl0bGU9Ik15IG9yZGVycyIgaHJlZj0iaHR0cDovL2xvY2FsaG9zdC9wcmVzdGFzaG9wL3BmX21peG1vYmlsZS9pbmRleC5waHA/Y29udHJvbGxlcj1oaXN0b3J5IiByZWw9Im5vZm9sbG93Ij5NeSBvcmRlcnM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTXkgY3JlZGl0IHNsaXBzIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPW9yZGVyLXNsaXAiIHJlbD0ibm9mb2xsb3ciPk15IGNyZWRpdCBzbGlwczwvYT48L2xpPg0KPGxpPjxhIHRpdGxlPSJNeSBhZGRyZXNzZXMiIGhyZWY9Imh0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9taXhtb2JpbGUvaW5kZXgucGhwP2NvbnRyb2xsZXI9YWRkcmVzc2VzIiByZWw9Im5vZm9sbG93Ij5NeSBhZGRyZXNzZXM8L2E+PC9saT4NCjxsaT48YSB0aXRsZT0iTWFuYWdlIG15IHBlcnNvbmFsIGluZm9ybWF0aW9uIiBocmVmPSJodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWl4bW9iaWxlL2luZGV4LnBocD9jb250cm9sbGVyPWlkZW50aXR5IiByZWw9Im5vZm9sbG93Ij5NeSBwZXJzb25hbCBpbmZvPC9hPjwvbGk+DQo8L3VsPg==";}s:4:"wkey";s:17:"key_1414831468547";}";}s:17:"key_1415605119427";a:1:{s:6:"config";s:956:"a:2:{s:6:"widget";a:27:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTU2MDUxMTk0Mjc=";s:5:"wtype";s:12:"dHdpdHRlcg==";s:11:"widget_name";s:16:"TGFzdCBUd2VldA==";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:16:"TGFzdCBUd2VldA==";s:14:"widget_title_2";s:0:"";s:14:"widget_title_3";s:0:"";s:14:"widget_title_4";s:0:"";s:14:"widget_title_5";s:0:"";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:10:"twidget_id";s:24:"MzY2NzY2ODk2OTg2NTkxMjMy";s:5:"count";s:4:"Mg==";s:8:"username";s:16:"cGF2b3RoZW1lcw==";s:12:"border_color";s:12:"I2RmZWZmZg==";s:14:"nickname_color";s:12:"I2Q5ZWNmZg==";s:10:"name_color";s:12:"I2RmZWZmZg==";s:11:"title_color";s:12:"I2RkZWVmZg==";s:10:"link_color";s:12:"IzAwMDAwMA==";s:5:"width";s:0:"";s:6:"height";s:0:"";s:12:"show_replies";s:4:"MQ==";s:11:"show_header";s:4:"MA==";s:11:"show_footer";s:4:"MA==";s:11:"show_border";s:4:"MA==";s:14:"show_scrollbar";s:4:"MQ==";}s:4:"wkey";s:17:"key_1415605119427";}";}s:17:"key_1414831611780";a:1:{s:6:"config";s:5467:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzE2MTE3ODA=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:16:"SHRtbCBGb290ZXI=";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:16:"SHRtbCBGb290ZXI=";s:14:"widget_title_2";s:16:"SHRtbCBGb290ZXI=";s:14:"widget_title_3";s:16:"SHRtbCBGb290ZXI=";s:14:"widget_title_4";s:16:"SHRtbCBGb290ZXI=";s:14:"widget_title_5";s:16:"SHRtbCBGb290ZXI=";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:960:"PHVsIGNsYXNzPSJyb3cgIj4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14cy0xMiAiPg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxkaXYgY2xhc3M9ImN1c3RvbSBpbmZvLXNlcnZpY2VzIj4NCjxkaXYgY2xhc3M9ImluZm8tdGl0bGUgYm9yZGVyIHNtYWxsIj5jYWxsIHRoZSBjdXN0b21lciBzZXJ2aWNlczwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC1oaWdobGlnaHQiPjxzdHJvbmc+MTgwMCAtIDE1NzAgLSAwMDA8L3N0cm9uZz48L2Rpdj4NCjxkaXYgY2xhc3M9IiBib3JkZXIgc21hbGwiPkZyZWUgMjQvNyBzdXBwb3J0IGZvciBvdXIgY3VzdG9tZXJzPC9kaXY+DQo8L2Rpdj4NCjwvZGl2Pg0KPC9saT4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14czEyIj4NCjxkaXYgY2xhc3M9Iml0ZW0taHRtbCI+DQo8ZGl2IGNsYXNzPSJjdXN0b20gaW5mby1xdWVzdGlvbiI+DQo8ZGl2IGNsYXNzPSJpbmZvLXRpdGxlIGJvcmRlciBzbWFsbCI+ZW1haWwgdXMgeW91ciBxdWVzdGlvbjwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC13YXJuaW5nIj48c3Ryb25nPnN1cHBvcnRAbWl4c3RvcmUuY29tPC9zdHJvbmc+PC9kaXY+DQo8ZGl2IGNsYXNzPSIgYm9yZGVyIHNtYWxsIj5XZSB3aWxsIHJlc3BvbmQgd2l0aGluIDMgYnVzaW5lc3MgZGF5czwvZGl2Pg0KPC9kaXY+DQo8L2Rpdj4NCjwvbGk+DQo8L3VsPg==";s:13:"htmlcontent_2";s:960:"PHVsIGNsYXNzPSJyb3cgIj4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14cy0xMiAiPg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxkaXYgY2xhc3M9ImN1c3RvbSBpbmZvLXNlcnZpY2VzIj4NCjxkaXYgY2xhc3M9ImluZm8tdGl0bGUgYm9yZGVyIHNtYWxsIj5jYWxsIHRoZSBjdXN0b21lciBzZXJ2aWNlczwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC1oaWdobGlnaHQiPjxzdHJvbmc+MTgwMCAtIDE1NzAgLSAwMDA8L3N0cm9uZz48L2Rpdj4NCjxkaXYgY2xhc3M9IiBib3JkZXIgc21hbGwiPkZyZWUgMjQvNyBzdXBwb3J0IGZvciBvdXIgY3VzdG9tZXJzPC9kaXY+DQo8L2Rpdj4NCjwvZGl2Pg0KPC9saT4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14czEyIj4NCjxkaXYgY2xhc3M9Iml0ZW0taHRtbCI+DQo8ZGl2IGNsYXNzPSJjdXN0b20gaW5mby1xdWVzdGlvbiI+DQo8ZGl2IGNsYXNzPSJpbmZvLXRpdGxlIGJvcmRlciBzbWFsbCI+ZW1haWwgdXMgeW91ciBxdWVzdGlvbjwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC13YXJuaW5nIj48c3Ryb25nPnN1cHBvcnRAbWl4c3RvcmUuY29tPC9zdHJvbmc+PC9kaXY+DQo8ZGl2IGNsYXNzPSIgYm9yZGVyIHNtYWxsIj5XZSB3aWxsIHJlc3BvbmQgd2l0aGluIDMgYnVzaW5lc3MgZGF5czwvZGl2Pg0KPC9kaXY+DQo8L2Rpdj4NCjwvbGk+DQo8L3VsPg==";s:13:"htmlcontent_3";s:960:"PHVsIGNsYXNzPSJyb3cgIj4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14cy0xMiAiPg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxkaXYgY2xhc3M9ImN1c3RvbSBpbmZvLXNlcnZpY2VzIj4NCjxkaXYgY2xhc3M9ImluZm8tdGl0bGUgYm9yZGVyIHNtYWxsIj5jYWxsIHRoZSBjdXN0b21lciBzZXJ2aWNlczwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC1oaWdobGlnaHQiPjxzdHJvbmc+MTgwMCAtIDE1NzAgLSAwMDA8L3N0cm9uZz48L2Rpdj4NCjxkaXYgY2xhc3M9IiBib3JkZXIgc21hbGwiPkZyZWUgMjQvNyBzdXBwb3J0IGZvciBvdXIgY3VzdG9tZXJzPC9kaXY+DQo8L2Rpdj4NCjwvZGl2Pg0KPC9saT4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14czEyIj4NCjxkaXYgY2xhc3M9Iml0ZW0taHRtbCI+DQo8ZGl2IGNsYXNzPSJjdXN0b20gaW5mby1xdWVzdGlvbiI+DQo8ZGl2IGNsYXNzPSJpbmZvLXRpdGxlIGJvcmRlciBzbWFsbCI+ZW1haWwgdXMgeW91ciBxdWVzdGlvbjwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC13YXJuaW5nIj48c3Ryb25nPnN1cHBvcnRAbWl4c3RvcmUuY29tPC9zdHJvbmc+PC9kaXY+DQo8ZGl2IGNsYXNzPSIgYm9yZGVyIHNtYWxsIj5XZSB3aWxsIHJlc3BvbmQgd2l0aGluIDMgYnVzaW5lc3MgZGF5czwvZGl2Pg0KPC9kaXY+DQo8L2Rpdj4NCjwvbGk+DQo8L3VsPg==";s:13:"htmlcontent_4";s:960:"PHVsIGNsYXNzPSJyb3cgIj4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14cy0xMiAiPg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxkaXYgY2xhc3M9ImN1c3RvbSBpbmZvLXNlcnZpY2VzIj4NCjxkaXYgY2xhc3M9ImluZm8tdGl0bGUgYm9yZGVyIHNtYWxsIj5jYWxsIHRoZSBjdXN0b21lciBzZXJ2aWNlczwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC1oaWdobGlnaHQiPjxzdHJvbmc+MTgwMCAtIDE1NzAgLSAwMDA8L3N0cm9uZz48L2Rpdj4NCjxkaXYgY2xhc3M9IiBib3JkZXIgc21hbGwiPkZyZWUgMjQvNyBzdXBwb3J0IGZvciBvdXIgY3VzdG9tZXJzPC9kaXY+DQo8L2Rpdj4NCjwvZGl2Pg0KPC9saT4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14czEyIj4NCjxkaXYgY2xhc3M9Iml0ZW0taHRtbCI+DQo8ZGl2IGNsYXNzPSJjdXN0b20gaW5mby1xdWVzdGlvbiI+DQo8ZGl2IGNsYXNzPSJpbmZvLXRpdGxlIGJvcmRlciBzbWFsbCI+ZW1haWwgdXMgeW91ciBxdWVzdGlvbjwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC13YXJuaW5nIj48c3Ryb25nPnN1cHBvcnRAbWl4c3RvcmUuY29tPC9zdHJvbmc+PC9kaXY+DQo8ZGl2IGNsYXNzPSIgYm9yZGVyIHNtYWxsIj5XZSB3aWxsIHJlc3BvbmQgd2l0aGluIDMgYnVzaW5lc3MgZGF5czwvZGl2Pg0KPC9kaXY+DQo8L2Rpdj4NCjwvbGk+DQo8L3VsPg==";s:13:"htmlcontent_5";s:960:"PHVsIGNsYXNzPSJyb3cgIj4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14cy0xMiAiPg0KPGRpdiBjbGFzcz0iaXRlbS1odG1sIj4NCjxkaXYgY2xhc3M9ImN1c3RvbSBpbmZvLXNlcnZpY2VzIj4NCjxkaXYgY2xhc3M9ImluZm8tdGl0bGUgYm9yZGVyIHNtYWxsIj5jYWxsIHRoZSBjdXN0b21lciBzZXJ2aWNlczwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC1oaWdobGlnaHQiPjxzdHJvbmc+MTgwMCAtIDE1NzAgLSAwMDA8L3N0cm9uZz48L2Rpdj4NCjxkaXYgY2xhc3M9IiBib3JkZXIgc21hbGwiPkZyZWUgMjQvNyBzdXBwb3J0IGZvciBvdXIgY3VzdG9tZXJzPC9kaXY+DQo8L2Rpdj4NCjwvZGl2Pg0KPC9saT4NCjxsaSBjbGFzcz0iY29sLWxnLTYgY29sLW1kLTYgY29sLXNtLTEyIGNvbC14czEyIj4NCjxkaXYgY2xhc3M9Iml0ZW0taHRtbCI+DQo8ZGl2IGNsYXNzPSJjdXN0b20gaW5mby1xdWVzdGlvbiI+DQo8ZGl2IGNsYXNzPSJpbmZvLXRpdGxlIGJvcmRlciBzbWFsbCI+ZW1haWwgdXMgeW91ciBxdWVzdGlvbjwvZGl2Pg0KPGRpdiBjbGFzcz0idGV4dC13YXJuaW5nIj48c3Ryb25nPnN1cHBvcnRAbWl4c3RvcmUuY29tPC9zdHJvbmc+PC9kaXY+DQo8ZGl2IGNsYXNzPSIgYm9yZGVyIHNtYWxsIj5XZSB3aWxsIHJlc3BvbmQgd2l0aGluIDMgYnVzaW5lc3MgZGF5czwvZGl2Pg0KPC9kaXY+DQo8L2Rpdj4NCjwvbGk+DQo8L3VsPg==";}s:4:"wkey";s:17:"key_1414831611780";}";}}', 27, 0, 1, 1414833517),
	('HomeMobile', 0, '', '[{"index":0,"cls":"hidden-xs","bgcolor":"","bgimage":"","fullwidth":"0","parallax":"0","sfxcls":null,"cols":[{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":12,"mdcol":12,"smcol":12,"xscol":12,"widgets":[],"rows":[{"index":0,"cls":"","bgcolor":"","bgimage":"","fullwidth":0,"parallax":0,"sfxcls":"","cols":[{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":9,"mdcol":9,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"sliderlayer","wkey":"key_1414833847653","name":"Slideshow mobile"}],"rows":[]},{"index":0,"cls":"hidden-sm","sfxcls":null,"bgcolor":"","bgimage":"","inrow":0,"lgcol":3,"mdcol":3,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"image","wkey":"key_1414833958636","name":"Bannerslideshow1"},{"cls":"","wtype":"image","wkey":"key_1414834182579","name":"Bannerslideshow2"}],"rows":[]}]},{"index":0,"cls":"","bgcolor":"","bgimage":"","fullwidth":0,"parallax":0,"sfxcls":"","cols":[{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":4,"mdcol":4,"smcol":4,"xscol":12,"widgets":[{"cls":"","wtype":"image","wkey":"key_1414834277258","name":"Banner Top1"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":4,"mdcol":4,"smcol":4,"xscol":12,"widgets":[{"cls":"","wtype":"image","wkey":"key_1414834310290","name":"Banner Top2"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":4,"mdcol":4,"smcol":4,"xscol":12,"widgets":[{"cls":"","wtype":"image","wkey":"key_1414834347125","name":"Banner Top3"}],"rows":[]}]}]}]},{"index":0,"cls":"","bgcolor":"","bgimage":"","fullwidth":0,"parallax":0,"sfxcls":"","cols":[{"index":0,"cls":"sidebar","sfxcls":null,"bgcolor":"","bgimage":"","inrow":0,"lgcol":3,"mdcol":3,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"categories_block","wkey":"key_1414834429133","name":"Shop By Categories"},{"cls":"","wtype":"image","wkey":"key_1414834506482","name":"Banner Left"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":9,"mdcol":9,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"producttabs","wkey":"key_1414834565283","name":"Product Tab"}],"rows":[]}]},{"index":0,"cls":"hidden-xs","bgcolor":"","bgimage":"","fullwidth":"0","parallax":"0","sfxcls":null,"cols":[{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":12,"mdcol":12,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"image","wkey":"key_1414834608996","name":"Banner"}],"rows":[]}]},{"index":0,"cls":"","bgcolor":"","bgimage":"","fullwidth":0,"parallax":0,"sfxcls":"","cols":[{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":12,"mdcol":12,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"utilproduct_list","wkey":"key_1414834742537","name":"New Products"}],"rows":[]}]},{"index":0,"cls":"","bgcolor":"","bgimage":"","fullwidth":0,"parallax":0,"sfxcls":"","cols":[{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":12,"mdcol":12,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"bloglatest","wkey":"key_1414834852938","name":"Latest From Blog"}],"rows":[]}]},{"index":0,"cls":"hidden-xs","bgcolor":"","bgimage":"","fullwidth":"0","parallax":"0","sfxcls":null,"cols":[{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":12,"mdcol":12,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"manufacture","wkey":"key_1414834899016","name":"Manufacture Logo"}],"rows":[]}]}]', 'a:13:{s:17:"key_1414833847653";a:1:{s:6:"config";s:600:"a:2:{s:6:"widget";a:13:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzM4NDc2NTM=";s:5:"wtype";s:16:"c2xpZGVybGF5ZXI=";s:11:"widget_name";s:24:"U2xpZGVzaG93IG1vYmlsZQ==";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:24:"U2xpZGVzaG93IG1vYmlsZQ==";s:14:"widget_title_2";s:24:"U2xpZGVzaG93IG1vYmlsZQ==";s:14:"widget_title_3";s:24:"U2xpZGVzaG93IG1vYmlsZQ==";s:14:"widget_title_4";s:24:"U2xpZGVzaG93IG1vYmlsZQ==";s:14:"widget_title_5";s:24:"U2xpZGVzaG93IG1vYmlsZQ==";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:8:"id_group";s:4:"MQ==";}s:4:"wkey";s:17:"key_1414833847653";}";}s:17:"key_1414833958636";a:1:{s:6:"config";s:736:"a:2:{s:6:"widget";a:18:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzM5NTg2MzY=";s:5:"wtype";s:8:"aW1hZ2U=";s:11:"widget_name";s:24:"QmFubmVyc2xpZGVzaG93MQ==";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:24:"QmFubmVyc2xpZGVzaG93MQ==";s:14:"widget_title_2";s:24:"QmFubmVyc2xpZGVzaG93MQ==";s:14:"widget_title_3";s:24:"QmFubmVyc2xpZGVzaG93MQ==";s:14:"widget_title_4";s:24:"QmFubmVyc2xpZGVzaG93MQ==";s:14:"widget_title_5";s:24:"QmFubmVyc2xpZGVzaG93MQ==";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:9:"imagefile";s:24:"YmFubmVyX3NsaWRlcjEuanBn";s:4:"size";s:0:"";s:9:"animation";s:4:"NA==";s:9:"alignment";s:8:"bGVmdA==";s:7:"ispopup";s:4:"MA==";s:8:"link_url";s:0:"";}s:4:"wkey";s:17:"key_1414833958636";}";}s:17:"key_1414834182579";a:1:{s:6:"config";s:736:"a:2:{s:6:"widget";a:18:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzQxODI1Nzk=";s:5:"wtype";s:8:"aW1hZ2U=";s:11:"widget_name";s:24:"QmFubmVyc2xpZGVzaG93Mg==";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:24:"QmFubmVyc2xpZGVzaG93Mg==";s:14:"widget_title_2";s:24:"QmFubmVyc2xpZGVzaG93Mg==";s:14:"widget_title_3";s:24:"QmFubmVyc2xpZGVzaG93Mg==";s:14:"widget_title_4";s:24:"QmFubmVyc2xpZGVzaG93Mg==";s:14:"widget_title_5";s:24:"QmFubmVyc2xpZGVzaG93Mg==";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:9:"imagefile";s:24:"YmFubmVyX3NsaWRlcjIuanBn";s:4:"size";s:0:"";s:9:"animation";s:4:"NA==";s:9:"alignment";s:8:"bGVmdA==";s:7:"ispopup";s:4:"MA==";s:8:"link_url";s:0:"";}s:4:"wkey";s:17:"key_1414834182579";}";}s:17:"key_1414834277258";a:1:{s:6:"config";s:616:"a:2:{s:6:"widget";a:18:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzQyNzcyNTg=";s:5:"wtype";s:8:"aW1hZ2U=";s:11:"widget_name";s:16:"QmFubmVyIFRvcDE=";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:0:"";s:14:"widget_title_2";s:0:"";s:14:"widget_title_3";s:0:"";s:14:"widget_title_4";s:0:"";s:14:"widget_title_5";s:0:"";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:9:"imagefile";s:20:"YmFubmVyX3RvcDEuanBn";s:4:"size";s:16:"QmFubmVyIFRvcDE=";s:9:"animation";s:4:"NA==";s:9:"alignment";s:8:"bGVmdA==";s:7:"ispopup";s:4:"MA==";s:8:"link_url";s:0:"";}s:4:"wkey";s:17:"key_1414834277258";}";}s:17:"key_1414834310290";a:1:{s:6:"config";s:684:"a:2:{s:6:"widget";a:18:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzQzMTAyOTA=";s:5:"wtype";s:8:"aW1hZ2U=";s:11:"widget_name";s:16:"QmFubmVyIFRvcDI=";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:16:"QmFubmVyIFRvcDI=";s:14:"widget_title_2";s:16:"QmFubmVyIFRvcDI=";s:14:"widget_title_3";s:16:"QmFubmVyIFRvcDI=";s:14:"widget_title_4";s:16:"QmFubmVyIFRvcDI=";s:14:"widget_title_5";s:16:"QmFubmVyIFRvcDI=";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:9:"imagefile";s:20:"YmFubmVyX3RvcDIuanBn";s:4:"size";s:0:"";s:9:"animation";s:4:"NA==";s:9:"alignment";s:8:"bGVmdA==";s:7:"ispopup";s:4:"MA==";s:8:"link_url";s:0:"";}s:4:"wkey";s:17:"key_1414834310290";}";}s:17:"key_1414834347125";a:1:{s:6:"config";s:684:"a:2:{s:6:"widget";a:18:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzQzNDcxMjU=";s:5:"wtype";s:8:"aW1hZ2U=";s:11:"widget_name";s:16:"QmFubmVyIFRvcDM=";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:16:"QmFubmVyIFRvcDM=";s:14:"widget_title_2";s:16:"QmFubmVyIFRvcDM=";s:14:"widget_title_3";s:16:"QmFubmVyIFRvcDM=";s:14:"widget_title_4";s:16:"QmFubmVyIFRvcDM=";s:14:"widget_title_5";s:16:"QmFubmVyIFRvcDM=";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:9:"imagefile";s:20:"YmFubmVyX3RvcDMuanBn";s:4:"size";s:0:"";s:9:"animation";s:4:"NA==";s:9:"alignment";s:8:"bGVmdA==";s:7:"ispopup";s:4:"MA==";s:8:"link_url";s:0:"";}s:4:"wkey";s:17:"key_1414834347125";}";}s:17:"key_1414834429133";a:1:{s:6:"config";s:738:"a:2:{s:6:"widget";a:16:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzQ0MjkxMzM=";s:5:"wtype";s:24:"Y2F0ZWdvcmllc19ibG9jaw==";s:11:"widget_name";s:24:"U2hvcCBCeSBDYXRlZ29yaWVz";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:24:"U2hvcCBCeSBDYXRlZ29yaWVz";s:14:"widget_title_2";s:24:"U2hvcCBCeSBDYXRlZ29yaWVz";s:14:"widget_title_3";s:24:"U2hvcCBCeSBDYXRlZ29yaWVz";s:14:"widget_title_4";s:24:"U2hvcCBCeSBDYXRlZ29yaWVz";s:14:"widget_title_5";s:24:"U2hvcCBCeSBDYXRlZ29yaWVz";s:12:"addition_cls";s:12:"bm9wYWRkaW5n";s:8:"stylecls";s:16:"aGlnaGxpZ2h0ZWQ=";s:15:"categ_max_depth";s:4:"NA==";s:11:"categ_dhtml";s:4:"MQ==";s:10:"categ_sort";s:4:"MA==";s:14:"categ_sort_way";s:4:"MA==";}s:4:"wkey";s:17:"key_1414834429133";}";}s:17:"key_1414834506482";a:1:{s:6:"config";s:710:"a:2:{s:6:"widget";a:18:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzQ1MDY0ODI=";s:5:"wtype";s:8:"aW1hZ2U=";s:11:"widget_name";s:16:"QmFubmVyIExlZnQ=";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:16:"QmFubmVyIExlZnQ=";s:14:"widget_title_2";s:16:"QmFubmVyIExlZnQ=";s:14:"widget_title_3";s:16:"QmFubmVyIExlZnQ=";s:14:"widget_title_4";s:16:"QmFubmVyIExlZnQ=";s:14:"widget_title_5";s:16:"QmFubmVyIExlZnQ=";s:12:"addition_cls";s:12:"aGlkZGVuLXhz";s:8:"stylecls";s:12:"bm9wYWRkaW5n";s:9:"imagefile";s:20:"YmFubmVyX2xlZnQuanBn";s:4:"size";s:0:"";s:9:"animation";s:4:"NA==";s:9:"alignment";s:8:"bGVmdA==";s:7:"ispopup";s:4:"MA==";s:8:"link_url";s:0:"";}s:4:"wkey";s:17:"key_1414834506482";}";}s:17:"key_1414834565283";a:1:{s:6:"config";s:739:"a:2:{s:6:"widget";a:19:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzQ1NjUyODM=";s:5:"wtype";s:16:"cHJvZHVjdHRhYnM=";s:11:"widget_name";s:16:"UHJvZHVjdCBUYWI=";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:16:"UHJvZHVjdCBUYWI=";s:14:"widget_title_2";s:16:"UHJvZHVjdCBUYWI=";s:14:"widget_title_3";s:16:"UHJvZHVjdCBUYWI=";s:14:"widget_title_4";s:16:"UHJvZHVjdCBUYWI=";s:14:"widget_title_5";s:16:"UHJvZHVjdCBUYWI=";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:5:"limit";s:4:"MTg=";s:6:"column";s:4:"Mw==";s:12:"itemsperpage";s:4:"Ng==";s:13:"enable_newest";s:4:"MQ==";s:15:"enable_featured";s:4:"MQ==";s:17:"enable_bestseller";s:4:"MQ==";s:14:"enable_special";s:4:"MQ==";}s:4:"wkey";s:17:"key_1414834565283";}";}s:17:"key_1414834608996";a:1:{s:6:"config";s:647:"a:2:{s:6:"widget";a:18:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzQ2MDg5OTY=";s:5:"wtype";s:8:"aW1hZ2U=";s:11:"widget_name";s:8:"QmFubmVy";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:8:"QmFubmVy";s:14:"widget_title_2";s:8:"QmFubmVy";s:14:"widget_title_3";s:8:"QmFubmVy";s:14:"widget_title_4";s:8:"QmFubmVy";s:14:"widget_title_5";s:8:"QmFubmVy";s:12:"addition_cls";s:12:"cGFkZGluZw==";s:8:"stylecls";s:0:"";s:9:"imagefile";s:24:"YmFubmVyLWJvdHRvbS5qcGc=";s:4:"size";s:0:"";s:9:"animation";s:4:"NA==";s:9:"alignment";s:8:"bGVmdA==";s:7:"ispopup";s:4:"MA==";s:8:"link_url";s:0:"";}s:4:"wkey";s:17:"key_1414834608996";}";}s:17:"key_1414834742537";a:1:{s:6:"config";s:683:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzQ3NDI1Mzc=";s:5:"wtype";s:24:"dXRpbHByb2R1Y3RfbGlzdA==";s:11:"widget_name";s:16:"TmV3IFByb2R1Y3Rz";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:16:"TmV3IFByb2R1Y3Rz";s:14:"widget_title_2";s:16:"TmV3IFByb2R1Y3Rz";s:14:"widget_title_3";s:16:"TmV3IFByb2R1Y3Rz";s:14:"widget_title_4";s:16:"TmV3IFByb2R1Y3Rz";s:14:"widget_title_5";s:16:"TmV3IFByb2R1Y3Rz";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:5:"limit";s:4:"MTU=";s:13:"utilmin_width";s:4:"MjA1";s:13:"utilmax_width";s:4:"MjE1";s:9:"list_type";s:8:"bmV3ZXN0";s:12:"utilinterval";s:4:"MA==";}s:4:"wkey";s:17:"key_1414834742537";}";}s:17:"key_1414834852938";a:1:{s:6:"config";s:954:"a:2:{s:6:"widget";a:26:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzQ4NTI5Mzg=";s:5:"wtype";s:16:"YmxvZ2xhdGVzdA==";s:11:"widget_name";s:24:"TGF0ZXN0IEZyb20gQmxvZw==";s:14:"widget_title_1";s:24:"TGF0ZXN0IEZyb20gQmxvZw==";s:14:"widget_title_2";s:24:"TGF0ZXN0IEZyb20gQmxvZw==";s:14:"widget_title_3";s:24:"TGF0ZXN0IEZyb20gQmxvZw==";s:14:"widget_title_4";s:24:"TGF0ZXN0IEZyb20gQmxvZw==";s:14:"widget_title_5";s:24:"TGF0ZXN0IEZyb20gQmxvZw==";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:3:"nbr";s:4:"OQ==";s:5:"width";s:4:"MzM4";s:6:"height";s:4:"MTIw";s:4:"page";s:4:"Mw==";s:3:"col";s:4:"Mw==";s:4:"intv";s:8:"ODAwMA==";s:4:"show";s:4:"MA==";s:10:"show_image";s:4:"MQ==";s:10:"show_title";s:4:"MQ==";s:13:"show_category";s:4:"MA==";s:12:"show_dateadd";s:4:"MQ==";s:12:"show_comment";s:4:"MA==";s:16:"show_description";s:4:"MQ==";s:13:"show_readmore";s:4:"MQ==";s:9:"list_mode";s:8:"Z3JpZA==";}s:4:"wkey";s:17:"key_1414834852938";}";}s:17:"key_1414834899016";a:1:{s:6:"config";s:652:"a:2:{s:6:"widget";a:15:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzQ4OTkwMTY=";s:5:"wtype";s:16:"bWFudWZhY3R1cmU=";s:11:"widget_name";s:24:"TWFudWZhY3R1cmUgTG9nbw==";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:24:"TWFudWZhY3R1cmUgTG9nbw==";s:14:"widget_title_2";s:24:"TWFudWZhY3R1cmUgTG9nbw==";s:14:"widget_title_3";s:24:"TWFudWZhY3R1cmUgTG9nbw==";s:14:"widget_title_4";s:24:"TWFudWZhY3R1cmUgTG9nbw==";s:14:"widget_title_5";s:24:"TWFudWZhY3R1cmUgTG9nbw==";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:5:"limit";s:4:"MTI=";s:6:"column";s:4:"Ng==";s:12:"itemsperpage";s:4:"Ng==";}s:4:"wkey";s:17:"key_1414834899016";}";}}', 28, 0, 0, 1414834909),
	('HomeFashion', 0, '', '[{"index":0,"cls":"hidden-xs","bgcolor":"","bgimage":"","fullwidth":"0","parallax":"0","sfxcls":null,"cols":[{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":12,"mdcol":9,"smcol":12,"xscol":12,"widgets":[],"rows":[{"index":0,"cls":"","bgcolor":"","bgimage":"","fullwidth":0,"parallax":0,"sfxcls":"","cols":[{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":12,"mdcol":12,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"sliderlayer","wkey":"key_1414835464242","name":"Slideshoe Fashion"}],"rows":[]}]}]}]},{"index":0,"cls":"","bgcolor":"","bgimage":"","fullwidth":0,"parallax":0,"sfxcls":"","cols":[{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":12,"mdcol":12,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"producttabs","wkey":"key_1414835778185","name":"Products Tabs"}],"rows":[]}]},{"index":0,"cls":"","bgcolor":"","bgimage":"","fullwidth":0,"parallax":0,"sfxcls":"","cols":[{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":3,"mdcol":4,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"ourservice","wkey":"key_1414835898817","name":"envío gratis"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":6,"mdcol":4,"smcol":12,"xscol":12,"widgets":[],"rows":[]},{"index":0,"cls":"","sfxcls":null,"bgcolor":"#eb5454","bgimage":"","inrow":0,"lgcol":3,"mdcol":4,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"ourservice","wkey":"key_1414836040982","name":"Customer Support"}],"rows":[]}]}]', 'a:4:{s:17:"key_1414835464242";a:1:{s:6:"config";s:600:"a:2:{s:6:"widget";a:13:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzU0NjQyNDI=";s:5:"wtype";s:16:"c2xpZGVybGF5ZXI=";s:11:"widget_name";s:24:"U2xpZGVzaG9lIEZhc2hpb24=";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:24:"U2xpZGVzaG9lIEZhc2hpb24=";s:14:"widget_title_2";s:24:"U2xpZGVzaG9lIEZhc2hpb24=";s:14:"widget_title_3";s:24:"U2xpZGVzaG9lIEZhc2hpb24=";s:14:"widget_title_4";s:24:"U2xpZGVzaG9lIEZhc2hpb24=";s:14:"widget_title_5";s:24:"U2xpZGVzaG9lIEZhc2hpb24=";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:8:"id_group";s:4:"NA==";}s:4:"wkey";s:17:"key_1414835464242";}";}s:17:"key_1414835778185";a:1:{s:6:"config";s:635:"a:2:{s:6:"widget";a:17:{s:18:"saveptspagebuilder";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzU3NzgxODU=";s:5:"wtype";s:16:"cHJvZHVjdHRhYnM=";s:11:"widget_name";s:20:"UHJvZHVjdHMgVGFicw==";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:20:"UHJvZHVjdHMgVGFicw==";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:5:"limit";s:4:"MTY=";s:6:"column";s:4:"NA==";s:12:"itemsperpage";s:4:"NA==";s:13:"enable_newest";s:4:"MQ==";s:15:"enable_featured";s:4:"MQ==";s:17:"enable_bestseller";s:4:"MQ==";s:14:"enable_special";s:4:"MQ==";s:16:"enable_toprating";s:4:"MQ==";s:15:"enable_mostview";s:4:"MQ==";}s:4:"wkey";s:17:"key_1414835778185";}";}s:17:"key_1414835898817";a:1:{s:6:"config";s:640:"a:2:{s:6:"widget";a:13:{s:18:"saveptspagebuilder";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzU4OTg4MTc=";s:5:"wtype";s:16:"b3Vyc2VydmljZQ==";s:11:"widget_name";s:20:"ZW52w61vIGdyYXRpcw==";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:20:"RW52w61vIGdyYXRpcw==";s:12:"addition_cls";s:16:"dGV4dC13aGl0ZQ==";s:8:"stylecls";s:8:"ZGFuZ2Vy";s:9:"imagefile";s:32:"Q2FtaW9uX2VudmlvX2dyYXRpcy5wbmc=";s:4:"icon";s:0:"";s:9:"content_1";s:100:"PGg1IGNsYXNzPSJ0ZXh0LXdoaXRlIj5FbnbDrW8gZ3JhdGlzPC9oNT4NCjxwPnBvciBjb21wcmFzIHNvYnJlICQ1MC4wMDA8L3A+";s:9:"imagesize";s:8:"NTV4NTU=";s:13:"icon_position";s:4:"dG9w";}s:4:"wkey";s:17:"key_1414835898817";}";}s:17:"key_1414836040982";a:1:{s:6:"config";s:611:"a:2:{s:6:"widget";a:13:{s:18:"saveptspagebuilder";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzYwNDA5ODI=";s:5:"wtype";s:16:"b3Vyc2VydmljZQ==";s:11:"widget_name";s:24:"Q3VzdG9tZXIgU3VwcG9ydA==";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:28:"QXRlbmNpw7NuIGFsIGNsaWVudGU=";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:12:"d2FybmluZw==";s:9:"imagefile";s:32:"QXRlbmNpb25fYWxfY2xpZW50ZS5wbmc=";s:4:"icon";s:0:"";s:9:"content_1";s:72:"PGg1PnJlc3BvbmRlcmVtb3MgYSBsYSBicmV2ZWRhZCB0b2RhcyB0dXMgZHVkYXM8L2g1Pg==";s:9:"imagesize";s:8:"NTV4NTU=";s:13:"icon_position";s:4:"dG9w";}s:4:"wkey";s:17:"key_1414836040982";}";}}', 29, 0, 0, 1414836318),
	('HomeGifts', 0, '', '[{"index":0,"cls":"hidden-xs","bgcolor":"","bgimage":"","fullwidth":"0","parallax":"0","sfxcls":null,"cols":[{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":12,"mdcol":12,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"sliderlayer","wkey":"key_1414838218636","name":"Slideshow Gifts"}],"rows":[]}]},{"index":0,"cls":"hidden-xs","bgcolor":"","bgimage":"","fullwidth":"0","parallax":"0","sfxcls":null,"cols":[{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":4,"mdcol":4,"smcol":4,"xscol":12,"widgets":[{"cls":"","wtype":"image","wkey":"key_1414838275449","name":"Banner Top1"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":4,"mdcol":4,"smcol":4,"xscol":12,"widgets":[{"cls":"","wtype":"image","wkey":"key_1414838308697","name":"Banner Top2"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":4,"mdcol":4,"smcol":4,"xscol":12,"widgets":[{"cls":"","wtype":"image","wkey":"key_1414838342864","name":"Banner Top3"}],"rows":[]}]},{"index":0,"cls":"","bgcolor":"","bgimage":"","fullwidth":0,"parallax":0,"sfxcls":"","cols":[{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":12,"mdcol":12,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"producttabs","wkey":"key_1414838378050","name":"Products tabs"}],"rows":[]}]},{"index":0,"cls":"","bgcolor":"#ffffff","bgimage":"","fullwidth":"0","parallax":"0","sfxcls":null,"cols":[{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":4,"mdcol":4,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"twitter","wkey":"key_1414838490327","name":"Twitter"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":4,"mdcol":4,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"module","wkey":"key_1414838539072","name":"what client says"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":4,"mdcol":4,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"bloglatest","wkey":"key_1414838604205","name":"From our blog"}],"rows":[]}]}]', 'a:8:{s:17:"key_1414838218636";a:1:{s:6:"config";s:576:"a:2:{s:6:"widget";a:13:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzgyMTg2MzY=";s:5:"wtype";s:16:"c2xpZGVybGF5ZXI=";s:11:"widget_name";s:20:"U2xpZGVzaG93IEdpZnRz";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:20:"U2xpZGVzaG93IEdpZnRz";s:14:"widget_title_2";s:20:"U2xpZGVzaG93IEdpZnRz";s:14:"widget_title_3";s:20:"U2xpZGVzaG93IEdpZnRz";s:14:"widget_title_4";s:20:"U2xpZGVzaG93IEdpZnRz";s:14:"widget_title_5";s:20:"U2xpZGVzaG93IEdpZnRz";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:8:"id_group";s:4:"NQ==";}s:4:"wkey";s:17:"key_1414838218636";}";}s:17:"key_1414838275449";a:1:{s:6:"config";s:688:"a:2:{s:6:"widget";a:18:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzgyNzU0NDk=";s:5:"wtype";s:8:"aW1hZ2U=";s:11:"widget_name";s:16:"QmFubmVyIFRvcDE=";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:16:"QmFubmVyIFRvcDE=";s:14:"widget_title_2";s:16:"QmFubmVyIFRvcDE=";s:14:"widget_title_3";s:16:"QmFubmVyIFRvcDE=";s:14:"widget_title_4";s:16:"QmFubmVyIFRvcDE=";s:14:"widget_title_5";s:16:"QmFubmVyIFRvcDE=";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:9:"imagefile";s:24:"YmFubmVyLWdpZnRzMS5qcGc=";s:4:"size";s:0:"";s:9:"animation";s:4:"NA==";s:9:"alignment";s:8:"bGVmdA==";s:7:"ispopup";s:4:"MA==";s:8:"link_url";s:0:"";}s:4:"wkey";s:17:"key_1414838275449";}";}s:17:"key_1414838308697";a:1:{s:6:"config";s:688:"a:2:{s:6:"widget";a:18:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzgzMDg2OTc=";s:5:"wtype";s:8:"aW1hZ2U=";s:11:"widget_name";s:16:"QmFubmVyIFRvcDI=";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:16:"QmFubmVyIFRvcDI=";s:14:"widget_title_2";s:16:"QmFubmVyIFRvcDI=";s:14:"widget_title_3";s:16:"QmFubmVyIFRvcDI=";s:14:"widget_title_4";s:16:"QmFubmVyIFRvcDI=";s:14:"widget_title_5";s:16:"QmFubmVyIFRvcDI=";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:9:"imagefile";s:24:"YmFubmVyLWdpZnRzMi5qcGc=";s:4:"size";s:0:"";s:9:"animation";s:4:"NA==";s:9:"alignment";s:8:"bGVmdA==";s:7:"ispopup";s:4:"MA==";s:8:"link_url";s:0:"";}s:4:"wkey";s:17:"key_1414838308697";}";}s:17:"key_1414838342864";a:1:{s:6:"config";s:688:"a:2:{s:6:"widget";a:18:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzgzNDI4NjQ=";s:5:"wtype";s:8:"aW1hZ2U=";s:11:"widget_name";s:16:"QmFubmVyIFRvcDM=";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:16:"QmFubmVyIFRvcDM=";s:14:"widget_title_2";s:16:"QmFubmVyIFRvcDM=";s:14:"widget_title_3";s:16:"QmFubmVyIFRvcDM=";s:14:"widget_title_4";s:16:"QmFubmVyIFRvcDM=";s:14:"widget_title_5";s:16:"QmFubmVyIFRvcDM=";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:9:"imagefile";s:24:"YmFubmVyLWdpZnRzMy5qcGc=";s:4:"size";s:0:"";s:9:"animation";s:4:"NA==";s:9:"alignment";s:8:"bGVmdA==";s:7:"ispopup";s:4:"MA==";s:8:"link_url";s:0:"";}s:4:"wkey";s:17:"key_1414838342864";}";}s:17:"key_1414838378050";a:1:{s:6:"config";s:763:"a:2:{s:6:"widget";a:19:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzgzNzgwNTA=";s:5:"wtype";s:16:"cHJvZHVjdHRhYnM=";s:11:"widget_name";s:20:"UHJvZHVjdHMgdGFicw==";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:20:"UHJvZHVjdHMgdGFicw==";s:14:"widget_title_2";s:20:"UHJvZHVjdHMgdGFicw==";s:14:"widget_title_3";s:20:"UHJvZHVjdHMgdGFicw==";s:14:"widget_title_4";s:20:"UHJvZHVjdHMgdGFicw==";s:14:"widget_title_5";s:20:"UHJvZHVjdHMgdGFicw==";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:5:"limit";s:4:"MjQ=";s:6:"column";s:4:"NA==";s:12:"itemsperpage";s:4:"MTI=";s:13:"enable_newest";s:4:"MQ==";s:15:"enable_featured";s:4:"MQ==";s:17:"enable_bestseller";s:4:"MQ==";s:14:"enable_special";s:4:"MQ==";}s:4:"wkey";s:17:"key_1414838378050";}";}s:17:"key_1414838490327";a:1:{s:6:"config";s:1012:"a:2:{s:6:"widget";a:27:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4Mzg0OTAzMjc=";s:5:"wtype";s:12:"dHdpdHRlcg==";s:11:"widget_name";s:12:"VHdpdHRlcg==";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:12:"VHdpdHRlcg==";s:14:"widget_title_2";s:12:"VHdpdHRlcg==";s:14:"widget_title_3";s:12:"VHdpdHRlcg==";s:14:"widget_title_4";s:12:"VHdpdHRlcg==";s:14:"widget_title_5";s:12:"VHdpdHRlcg==";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:10:"twidget_id";s:24:"MzY2NzY2ODk2OTg2NTkxMjMy";s:5:"count";s:4:"Mg==";s:8:"username";s:16:"cGF2b3RoZW1lcw==";s:12:"border_color";s:12:"IzAwMDAwMA==";s:14:"nickname_color";s:12:"IzAwMDAwMA==";s:10:"name_color";s:12:"IzAwMDAwMA==";s:11:"title_color";s:12:"IzAwMDAwMA==";s:10:"link_color";s:12:"IzAwYzRjNA==";s:5:"width";s:4:"OTAl";s:6:"height";s:8:"MTAwJQ==";s:12:"show_replies";s:4:"MQ==";s:11:"show_header";s:4:"MA==";s:11:"show_footer";s:4:"MA==";s:11:"show_border";s:4:"MA==";s:14:"show_scrollbar";s:4:"MQ==";}s:4:"wkey";s:17:"key_1414838490327";}";}s:17:"key_1414838539072";a:1:{s:6:"config";s:657:"a:2:{s:6:"widget";a:14:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4Mzg1MzkwNzI=";s:5:"wtype";s:8:"bW9kdWxl";s:11:"widget_name";s:24:"d2hhdCBjbGllbnQgc2F5cw==";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:24:"d2hhdCBjbGllbnQgc2F5cw==";s:14:"widget_title_2";s:24:"d2hhdCBjbGllbnQgc2F5cw==";s:14:"widget_title_3";s:24:"d2hhdCBjbGllbnQgc2F5cw==";s:14:"widget_title_4";s:24:"d2hhdCBjbGllbnQgc2F5cw==";s:14:"widget_title_5";s:24:"d2hhdCBjbGllbnQgc2F5cw==";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:10:"loadmodule";s:24:"cHRzYnR0ZXN0aW1vbmlhbHM=";s:10:"hookmodule";s:16:"ZGlzcGxheUhvbWU=";}s:4:"wkey";s:17:"key_1414838539072";}";}s:17:"key_1414838604205";a:1:{s:6:"config";s:930:"a:2:{s:6:"widget";a:26:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4Mzg2MDQyMDU=";s:5:"wtype";s:16:"YmxvZ2xhdGVzdA==";s:11:"widget_name";s:20:"RnJvbSBvdXIgYmxvZw==";s:14:"widget_title_1";s:20:"RnJvbSBvdXIgYmxvZw==";s:14:"widget_title_2";s:20:"RnJvbSBvdXIgYmxvZw==";s:14:"widget_title_3";s:20:"RnJvbSBvdXIgYmxvZw==";s:14:"widget_title_4";s:20:"RnJvbSBvdXIgYmxvZw==";s:14:"widget_title_5";s:20:"RnJvbSBvdXIgYmxvZw==";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:3:"nbr";s:4:"NA==";s:5:"width";s:4:"Mjgw";s:6:"height";s:4:"MjQw";s:4:"page";s:4:"Mg==";s:3:"col";s:4:"MQ==";s:4:"intv";s:8:"ODAwMA==";s:4:"show";s:4:"MA==";s:10:"show_image";s:4:"MA==";s:10:"show_title";s:4:"MQ==";s:13:"show_category";s:4:"MA==";s:12:"show_dateadd";s:4:"MQ==";s:12:"show_comment";s:4:"MA==";s:16:"show_description";s:4:"MQ==";s:13:"show_readmore";s:4:"MQ==";s:9:"list_mode";s:8:"Z3JpZA==";}s:4:"wkey";s:17:"key_1414838604205";}";}}', 31, 0, 0, 1414838613),
	('HomeSport', 0, '', '[{"index":0,"cls":"","bgcolor":"","bgimage":"","fullwidth":"1","parallax":"0","sfxcls":null,"cols":[{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":12,"mdcol":12,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"sliderlayer","wkey":"key_1414838855557","name":"Slideshow Sport"}],"rows":[]}]},{"index":0,"cls":"pts_position hidden-sm hidden-xs","bgcolor":"","bgimage":"","fullwidth":"0","parallax":"0","sfxcls":null,"cols":[{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":4,"mdcol":4,"smcol":4,"xscol":12,"widgets":[{"cls":"","wtype":"image","wkey":"key_1414838922157","name":"Banner Top1"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":4,"mdcol":4,"smcol":4,"xscol":12,"widgets":[{"cls":"","wtype":"image","wkey":"key_1414838950005","name":"Banner Top2"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":4,"mdcol":4,"smcol":4,"xscol":12,"widgets":[{"cls":"","wtype":"image","wkey":"key_1414838997989","name":"Banner Top3"}],"rows":[]}]},{"index":0,"cls":"","bgcolor":"","bgimage":"","fullwidth":0,"parallax":0,"sfxcls":"","cols":[{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":9,"mdcol":9,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"product_list","wkey":"key_1414839061549","name":"Featured"},{"cls":"","wtype":"product_list","wkey":"key_1414857575303","name":"New Products "},{"cls":"","wtype":"product_list","wkey":"key_1414857574798","name":"SALE PRODUCTS"},{"cls":"","wtype":"manufacture","wkey":"key_1414857822597","name":"Our Brand"}],"rows":[]},{"index":0,"cls":"sidebar","sfxcls":null,"bgcolor":"","bgimage":"","inrow":0,"lgcol":3,"mdcol":3,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"module","wkey":"key_1414982778465","name":"Shop By Categories "},{"cls":"","wtype":"image","wkey":"key_1414856990014","name":"Banner Right1"},{"cls":"","wtype":"product_list","wkey":"key_1414857039149","name":"Best Sellers"},{"cls":"","wtype":"newsletter","wkey":"key_1414857106887","name":"Join our Newsletter"}],"rows":[]}]}]', 'a:12:{s:17:"key_1414838855557";a:1:{s:6:"config";s:597:"a:2:{s:6:"widget";a:13:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4Mzg4NTU1NTc=";s:5:"wtype";s:16:"c2xpZGVybGF5ZXI=";s:11:"widget_name";s:20:"U2xpZGVzaG93IFNwb3J0";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:20:"U2xpZGVzaG93IFNwb3J0";s:14:"widget_title_2";s:20:"U2xpZGVzaG93IFNwb3J0";s:14:"widget_title_3";s:20:"U2xpZGVzaG93IFNwb3J0";s:14:"widget_title_4";s:20:"U2xpZGVzaG93IFNwb3J0";s:14:"widget_title_5";s:20:"U2xpZGVzaG93IFNwb3J0";s:12:"addition_cls";s:20:"cHRzX3NsaWRlc2hvdw==";s:8:"stylecls";s:0:"";s:8:"id_group";s:4:"Mw==";}s:4:"wkey";s:17:"key_1414838855557";}";}s:17:"key_1414838922157";a:1:{s:6:"config";s:688:"a:2:{s:6:"widget";a:18:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4Mzg5MjIxNTc=";s:5:"wtype";s:8:"aW1hZ2U=";s:11:"widget_name";s:16:"QmFubmVyIFRvcDE=";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:16:"QmFubmVyIFRvcDE=";s:14:"widget_title_2";s:16:"QmFubmVyIFRvcDE=";s:14:"widget_title_3";s:16:"QmFubmVyIFRvcDE=";s:14:"widget_title_4";s:16:"QmFubmVyIFRvcDE=";s:14:"widget_title_5";s:16:"QmFubmVyIFRvcDE=";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:9:"imagefile";s:24:"YmFubmVyLXNwb3J0MS5qcGc=";s:4:"size";s:0:"";s:9:"animation";s:4:"NA==";s:9:"alignment";s:8:"bGVmdA==";s:7:"ispopup";s:4:"MA==";s:8:"link_url";s:0:"";}s:4:"wkey";s:17:"key_1414838922157";}";}s:17:"key_1414838950005";a:1:{s:6:"config";s:688:"a:2:{s:6:"widget";a:18:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4Mzg5NTAwMDU=";s:5:"wtype";s:8:"aW1hZ2U=";s:11:"widget_name";s:16:"QmFubmVyIFRvcDI=";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:16:"QmFubmVyIFRvcDI=";s:14:"widget_title_2";s:16:"QmFubmVyIFRvcDI=";s:14:"widget_title_3";s:16:"QmFubmVyIFRvcDI=";s:14:"widget_title_4";s:16:"QmFubmVyIFRvcDI=";s:14:"widget_title_5";s:16:"QmFubmVyIFRvcDI=";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:9:"imagefile";s:24:"YmFubmVyLXNwb3J0Mi5qcGc=";s:4:"size";s:0:"";s:9:"animation";s:4:"NA==";s:9:"alignment";s:8:"bGVmdA==";s:7:"ispopup";s:4:"MA==";s:8:"link_url";s:0:"";}s:4:"wkey";s:17:"key_1414838950005";}";}s:17:"key_1414838997989";a:1:{s:6:"config";s:688:"a:2:{s:6:"widget";a:18:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4Mzg5OTc5ODk=";s:5:"wtype";s:8:"aW1hZ2U=";s:11:"widget_name";s:16:"QmFubmVyIFRvcDM=";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:16:"QmFubmVyIFRvcDM=";s:14:"widget_title_2";s:16:"QmFubmVyIFRvcDM=";s:14:"widget_title_3";s:16:"QmFubmVyIFRvcDM=";s:14:"widget_title_4";s:16:"QmFubmVyIFRvcDM=";s:14:"widget_title_5";s:16:"QmFubmVyIFRvcDM=";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:9:"imagefile";s:24:"YmFubmVyLXNwb3J0My5qcGc=";s:4:"size";s:0:"";s:9:"animation";s:4:"NA==";s:9:"alignment";s:8:"bGVmdA==";s:7:"ispopup";s:4:"MA==";s:8:"link_url";s:0:"";}s:4:"wkey";s:17:"key_1414838997989";}";}s:17:"key_1414839061549";a:1:{s:6:"config";s:647:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzkwNjE1NDk=";s:5:"wtype";s:16:"cHJvZHVjdF9saXN0";s:11:"widget_name";s:12:"RmVhdHVyZWQ=";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:12:"RmVhdHVyZWQ=";s:14:"widget_title_2";s:12:"RmVhdHVyZWQ=";s:14:"widget_title_3";s:12:"RmVhdHVyZWQ=";s:14:"widget_title_4";s:12:"RmVhdHVyZWQ=";s:14:"widget_title_5";s:12:"RmVhdHVyZWQ=";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:5:"limit";s:4:"OA==";s:6:"column";s:4:"Mw==";s:12:"itemsperpage";s:4:"Ng==";s:9:"list_type";s:12:"ZmVhdHVyZWQ=";s:9:"list_mode";s:8:"Z3JpZA==";}s:4:"wkey";s:17:"key_1414839061549";}";}s:17:"key_1414857575303";a:1:{s:6:"config";s:690:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4NTc1NzUzMDM=";s:5:"wtype";s:16:"cHJvZHVjdF9saXN0";s:11:"widget_name";s:20:"TmV3IFByb2R1Y3RzIA==";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:20:"TmV3IFByb2R1Y3RzIA==";s:14:"widget_title_2";s:20:"TmV3IFByb2R1Y3RzIA==";s:14:"widget_title_3";s:20:"TmV3IFByb2R1Y3RzIA==";s:14:"widget_title_4";s:20:"TmV3IFByb2R1Y3RzIA==";s:14:"widget_title_5";s:20:"TmV3IFByb2R1Y3RzIA==";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:5:"limit";s:4:"OA==";s:6:"column";s:4:"Mw==";s:12:"itemsperpage";s:4:"Mw==";s:9:"list_type";s:8:"bmV3ZXN0";s:9:"list_mode";s:8:"Z3JpZA==";}s:4:"wkey";s:17:"key_1414857575303";}";}s:17:"key_1414857574798";a:1:{s:6:"config";s:695:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4NTc1NzQ3OTg=";s:5:"wtype";s:16:"cHJvZHVjdF9saXN0";s:11:"widget_name";s:20:"U0FMRSBQUk9EVUNUUw==";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:20:"U0FMRSBQUk9EVUNUUw==";s:14:"widget_title_2";s:20:"U0FMRSBQUk9EVUNUUw==";s:14:"widget_title_3";s:20:"U0FMRSBQUk9EVUNUUw==";s:14:"widget_title_4";s:20:"U0FMRSBQUk9EVUNUUw==";s:14:"widget_title_5";s:20:"U0FMRSBQUk9EVUNUUw==";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:5:"limit";s:4:"OA==";s:6:"column";s:4:"Mw==";s:12:"itemsperpage";s:4:"Mw==";s:9:"list_type";s:12:"c3BlY2lhbA==";s:9:"list_mode";s:8:"Z3JpZA==";}s:4:"wkey";s:17:"key_1414857574798";}";}s:17:"key_1414857822597";a:1:{s:6:"config";s:580:"a:2:{s:6:"widget";a:15:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4NTc4MjI1OTc=";s:5:"wtype";s:16:"bWFudWZhY3R1cmU=";s:11:"widget_name";s:12:"T3VyIEJyYW5k";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:12:"T3VyIEJyYW5k";s:14:"widget_title_2";s:12:"T3VyIEJyYW5k";s:14:"widget_title_3";s:12:"T3VyIEJyYW5k";s:14:"widget_title_4";s:12:"T3VyIEJyYW5k";s:14:"widget_title_5";s:12:"T3VyIEJyYW5k";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:5:"limit";s:4:"OA==";s:6:"column";s:4:"NA==";s:12:"itemsperpage";s:4:"NA==";}s:4:"wkey";s:17:"key_1414857822597";}";}s:17:"key_1414982778465";a:1:{s:6:"config";s:599:"a:2:{s:6:"widget";a:14:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ5ODI3Nzg0NjU=";s:5:"wtype";s:8:"bW9kdWxl";s:11:"widget_name";s:28:"U2hvcCBCeSBDYXRlZ29yaWVzIA==";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:28:"U2hvcCBCeSBDYXRlZ29yaWVzIA==";s:14:"widget_title_2";s:0:"";s:14:"widget_title_3";s:0:"";s:14:"widget_title_4";s:0:"";s:14:"widget_title_5";s:0:"";s:12:"addition_cls";s:12:"bm9wYWRkaW5n";s:8:"stylecls";s:16:"aGlnaGxpZ2h0ZWQ=";s:10:"loadmodule";s:20:"cHRzdmVydGljYWxtZW51";s:10:"hookmodule";s:24:"ZGlzcGxheVJpZ2h0Q29sdW1u";}s:4:"wkey";s:17:"key_1414982778465";}";}s:17:"key_1414856990014";a:1:{s:6:"config";s:725:"a:2:{s:6:"widget";a:18:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4NTY5OTAwMTQ=";s:5:"wtype";s:8:"aW1hZ2U=";s:11:"widget_name";s:20:"QmFubmVyIFJpZ2h0MQ==";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:20:"QmFubmVyIFJpZ2h0MQ==";s:14:"widget_title_2";s:20:"QmFubmVyIFJpZ2h0MQ==";s:14:"widget_title_3";s:20:"QmFubmVyIFJpZ2h0MQ==";s:14:"widget_title_4";s:20:"QmFubmVyIFJpZ2h0MQ==";s:14:"widget_title_5";s:20:"QmFubmVyIFJpZ2h0MQ==";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:12:"bm9wYWRkaW5n";s:9:"imagefile";s:24:"YmFubmVyLXNwb3J0NC5qcGc=";s:4:"size";s:0:"";s:9:"animation";s:4:"NA==";s:9:"alignment";s:8:"bGVmdA==";s:7:"ispopup";s:4:"MQ==";s:8:"link_url";s:0:"";}s:4:"wkey";s:17:"key_1414856990014";}";}s:17:"key_1414857039149";a:1:{s:6:"config";s:683:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4NTcwMzkxNDk=";s:5:"wtype";s:16:"cHJvZHVjdF9saXN0";s:11:"widget_name";s:16:"QmVzdCBTZWxsZXJz";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:16:"QmVzdCBTZWxsZXJz";s:14:"widget_title_2";s:16:"QmVzdCBTZWxsZXJz";s:14:"widget_title_3";s:16:"QmVzdCBTZWxsZXJz";s:14:"widget_title_4";s:16:"QmVzdCBTZWxsZXJz";s:14:"widget_title_5";s:16:"QmVzdCBTZWxsZXJz";s:12:"addition_cls";s:8:"ZmRocw==";s:8:"stylecls";s:0:"";s:5:"limit";s:4:"OA==";s:6:"column";s:4:"MQ==";s:12:"itemsperpage";s:4:"NA==";s:9:"list_type";s:16:"YmVzdHNlbGxlcg==";s:9:"list_mode";s:8:"bGlzdDE=";}s:4:"wkey";s:17:"key_1414857039149";}";}s:17:"key_1414857106887";a:1:{s:6:"config";s:1608:"a:2:{s:6:"widget";a:20:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4NTcxMDY4ODc=";s:5:"wtype";s:16:"bmV3c2xldHRlcg==";s:11:"widget_name";s:28:"Sm9pbiBvdXIgTmV3c2xldHRlcg==";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:28:"Sm9pbiBvdXIgTmV3c2xldHRlcg==";s:14:"widget_title_2";s:28:"Sm9pbiBvdXIgTmV3c2xldHRlcg==";s:14:"widget_title_3";s:28:"Sm9pbiBvdXIgTmV3c2xldHRlcg==";s:14:"widget_title_4";s:28:"Sm9pbiBvdXIgTmV3c2xldHRlcg==";s:14:"widget_title_5";s:28:"Sm9pbiBvdXIgTmV3c2xldHRlcg==";s:12:"addition_cls";s:12:"bm8tc3R5bGU=";s:8:"stylecls";s:0:"";s:5:"class";s:20:"cHRzLW5ld3NsZXR0ZXI=";s:9:"imagefile";s:24:"YmctbmV3c2xldHRlci5qcGc=";s:13:"information_1";s:144:"PHA+U2lnbiB1cCB0byBvdXIgbmV3c2xldHRlciBhbmQgZ2V0IGV4Y2x1c2l2ZSBkZWFscyB5b3Ugd29udCBmaW5kIGFueXdoZXJlIGVsc2Ugc3RyYWlnaHQgdG8geW91ciBpbmJveCE8L3A+";s:13:"information_2";s:144:"PHA+U2lnbiB1cCB0byBvdXIgbmV3c2xldHRlciBhbmQgZ2V0IGV4Y2x1c2l2ZSBkZWFscyB5b3Ugd29udCBmaW5kIGFueXdoZXJlIGVsc2Ugc3RyYWlnaHQgdG8geW91ciBpbmJveCE8L3A+";s:13:"information_3";s:144:"PHA+U2lnbiB1cCB0byBvdXIgbmV3c2xldHRlciBhbmQgZ2V0IGV4Y2x1c2l2ZSBkZWFscyB5b3Ugd29udCBmaW5kIGFueXdoZXJlIGVsc2Ugc3RyYWlnaHQgdG8geW91ciBpbmJveCE8L3A+";s:13:"information_4";s:144:"PHA+U2lnbiB1cCB0byBvdXIgbmV3c2xldHRlciBhbmQgZ2V0IGV4Y2x1c2l2ZSBkZWFscyB5b3Ugd29udCBmaW5kIGFueXdoZXJlIGVsc2Ugc3RyYWlnaHQgdG8geW91ciBpbmJveCE8L3A+";s:13:"information_5";s:144:"PHA+U2lnbiB1cCB0byBvdXIgbmV3c2xldHRlciBhbmQgZ2V0IGV4Y2x1c2l2ZSBkZWFscyB5b3Ugd29udCBmaW5kIGFueXdoZXJlIGVsc2Ugc3RyYWlnaHQgdG8geW91ciBpbmJveCE8L3A+";s:16:"newsletter_style";s:8:"c3R5bGUy";}s:4:"wkey";s:17:"key_1414857106887";}";}}', 32, 0, 0, 1414839482),
	('FooterFashion', 0, '', '[{"index":0,"cls":"pts-footer-top","bgcolor":"#ffffff","bgimage":"","fullwidth":"0","parallax":"0","sfxcls":null,"cols":[{"index":0,"cls":"hidden-xs","sfxcls":null,"bgcolor":"","bgimage":"","inrow":0,"lgcol":9,"mdcol":8,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831113755","name":"Logo Footer"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":3,"mdcol":4,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831222604","name":"Social footer"}],"rows":[]}]},{"index":0,"cls":"pts-footer-center","bgcolor":"#000000","bgimage":"","fullwidth":"0","parallax":"0","sfxcls":null,"cols":[{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":3,"mdcol":3,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831295069","name":"Contact info"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":3,"mdcol":2,"smcol":4,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831349733","name":"Customer Service"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":3,"mdcol":2,"smcol":4,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1414831415734","name":"Information"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":3,"mdcol":3,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"html","wkey":"key_1431373620317","name":""}],"rows":[]}]}]', 'a:6:{s:17:"key_1414831113755";a:1:{s:6:"config";s:681:"a:2:{s:6:"widget";a:9:{s:18:"saveptspagebuilder";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzExMTM3NTU=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:16:"TG9nbyBGb290ZXI=";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:16:"TG9nbyBGb290ZXI=";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:316:"PGRpdiBjbGFzcz0iaW1hZ2UtYmFubmVyICBwdWxsLWxlZnQiPjxpbWcgc3JjPSJodHRwOi8vY2VsaW9kZXYuY29tL2ltZy9jbXMvTG9nb19mb290ZXIucG5nIiBhbHQ9IiIgd2lkdGg9IjE5MyIgaGVpZ2h0PSI1MiIgLz48L2Rpdj4NCjxkaXYgY2xhc3M9Iml0ZW0taHRtbCI+DQo8cD5zw61ndWVub3MgeSBjb21wYXJ0ZSB0dSBleHBlcmllbmNpYSBlbiBudWVzdHJhcyByZWRlcyBzb2NpYWxlczwvcD4NCjwvZGl2Pg==";}s:4:"wkey";s:17:"key_1414831113755";}";}s:17:"key_1414831222604";a:1:{s:6:"config";s:1563:"a:2:{s:6:"widget";a:9:{s:18:"saveptspagebuilder";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzEyMjI2MDQ=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:20:"U29jaWFsIGZvb3Rlcg==";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:20:"U29jaWFsIGZvb3Rlcg==";s:12:"addition_cls";s:20:"c29jaWFsLWZvb3Rlcg==";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:1168:"PGRpdiBpZD0ic29jaWFsX2Jsb2NrIj4NCjx1bCBjbGFzcz0ic29jaWFsIj4NCjxsaSBjbGFzcz0iZmFjZWJvb2siPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy5mYWNlYm9vay5jb20vcHJlc3Rhc2hvcCIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IkZhY2Vib29rIj4gRmFjZWJvb2sgPC9hPjwvbGk+DQo8bGkgY2xhc3M9InR3aXR0ZXIiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iaHR0cDovL3d3dy50d2l0dGVyLmNvbS9wcmVzdGFzaG9wIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iVHdpdHRlciI+VHdpdHRlciA8L2E+PC9saT4NCjxsaSBjbGFzcz0icnNzIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9Imh0dHA6Ly93d3cucHJlc3Rhc2hvcC5jb20vYmxvZy9lbi9mZWVkLyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlJTUyI+IFJTUzwvYT48L2xpPg0KPGxpIGNsYXNzPSJ5b3V0dWJlIj48YSBjbGFzcz0iYnRuLXRvb2x0aXAiIGhyZWY9IiMiIHRhcmdldD0iX2JsYW5rIiBkYXRhLW9yaWdpbmFsLXRpdGxlPSJZb3V0dWJlIj4gWW91dHViZTwvYT48L2xpPg0KPGxpIGNsYXNzPSJnb29nbGUtcGx1cyI+PGEgY2xhc3M9ImJ0bi10b29sdGlwIiBocmVmPSIjIiB0YXJnZXQ9Il9ibGFuayIgZGF0YS1vcmlnaW5hbC10aXRsZT0iR29vZ2xlIFBsdXMiPkdvb2dsZSBQbHVzIDwvYT48L2xpPg0KPGxpIGNsYXNzPSJwaW50ZXJlc3QiPjxhIGNsYXNzPSJidG4tdG9vbHRpcCIgaHJlZj0iIyIgdGFyZ2V0PSJfYmxhbmsiIGRhdGEtb3JpZ2luYWwtdGl0bGU9IlBpbnRlcmVzdCI+IFBpbnRlcmVzdDwvYT48L2xpPg0KPC91bD4NCjwvZGl2Pg==";}s:4:"wkey";s:17:"key_1414831222604";}";}s:17:"key_1414831295069";a:1:{s:6:"config";s:917:"a:2:{s:6:"widget";a:9:{s:18:"saveptspagebuilder";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzEyOTUwNjk=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:16:"Q29udGFjdCBpbmZv";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:12:"TWkgQ3VlbnRh";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:556:"PGRpdiBjbGFzcz0ibGlzdC1ncm91cCI+DQo8dWw+DQo8bGk+PGEgaHJlZj0iTWlzJTIwcGVkaWRvcyI+TWlzIFBlZGlkb3M8L2E+PC9saT4NCjwvdWw+DQo8cD4gPC9wPg0KPHVsPg0KPGxpPjxhIGhyZWY9Ik1pcyUyMHBlZGlkb3MiPk1pcyBEaXJlY2Npb25lczwvYT48L2xpPg0KPC91bD4NCjxwPiA8L3A+DQo8dWw+DQo8bGk+PGEgaHJlZj0iTWlzJTIwcGVkaWRvcyI+TWlzIERhdG9zIHBlcnNvbmFsZXM8L2E+PC9saT4NCjwvdWw+DQo8cD4gPC9wPg0KPHVsPg0KPGxpPjxhIGhyZWY9Ik1pcyUyMHBlZGlkb3MiPkN1cMOzbiBkZSBjcsOpZGl0bzwvYT48L2xpPg0KPC91bD4NCjxwPiA8L3A+DQo8dWw+DQo8bGk+PGEgaHJlZj0iTWlzJTIwcGVkaWRvcyI+Q3Vww7NuIGRlIGRlc2N1ZW50bzwvYT48L2xpPg0KPC91bD4NCjwvZGl2Pg==";}s:4:"wkey";s:17:"key_1414831295069";}";}s:17:"key_1414831349733";a:1:{s:6:"config";s:789:"a:2:{s:6:"widget";a:9:{s:18:"saveptspagebuilder";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzEzNDk3MzM=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:24:"Q3VzdG9tZXIgU2VydmljZQ==";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:16:"Q2F0ZWdvcsOtYXM=";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:416:"PHVsPg0KPGxpPjxhIGhyZWY9ImRzYSI+TmV3IGFycml2YWxzPC9hPjwvbGk+DQo8L3VsPg0KPHA+IDwvcD4NCjx1bD4NCjxsaT48YSBocmVmPSJkc2EiPlByb21vY2lvbmVzPC9hPjwvbGk+DQo8L3VsPg0KPHA+IDwvcD4NCjx1bD4NCjxsaT48YSBocmVmPSJkc2EiPlZlc3R1YXJpbzwvYT48L2xpPg0KPC91bD4NCjxwPiA8L3A+DQo8dWw+DQo8bGk+PGEgaHJlZj0iZHNhIj5KZWFucyB5IHBhbnRhbG9uZXM8L2E+PC9saT4NCjwvdWw+DQo8cD4gPC9wPg0KPHVsPg0KPGxpPjxhIGhyZWY9ImRzYXNhIj5BY2Nlc29yaW9zPC9hPjwvbGk+DQo8L3VsPg==";}s:4:"wkey";s:17:"key_1414831349733";}";}s:17:"key_1414831415734";a:1:{s:6:"config";s:729:"a:2:{s:6:"widget";a:9:{s:18:"saveptspagebuilder";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzE0MTU3MzQ=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:16:"SW5mb3JtYXRpb24=";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:16:"SW5mb3JtYWNpw7Nu";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:364:"PHVsPg0KPGxpPjxhIGhyZWY9ImRzYWFkcyI+Q2FtYmlvcyB5IGRldm9sdWNpw7NuPC9hPjwvbGk+DQo8L3VsPg0KPHA+IDwvcD4NCjx1bD4NCjxsaT48YSBocmVmPSJkc2FhZHMiPkdhcmFudMOtYXM8L2E+PC9saT4NCjwvdWw+DQo8cD4gPC9wPg0KPHVsPg0KPGxpPjxhIGhyZWY9ImRzYWFkcyI+TWVkaW9zIGRlIHBhZ288L2E+PC9saT4NCjwvdWw+DQo8cD4gPC9wPg0KPHVsPg0KPGxpPjxhIGhyZWY9ImRzYWFkcyI+UHJlZ3VudGFzIGZyZWN1ZW50ZXM8L2E+PC9saT4NCjwvdWw+";}s:4:"wkey";s:17:"key_1414831415734";}";}s:17:"key_1431373620317";a:1:{s:6:"config";s:883:"a:2:{s:6:"widget";a:9:{s:18:"saveptspagebuilder";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MzEzNzM2MjAzMTc=";s:5:"wtype";s:8:"aHRtbA==";s:11:"widget_name";s:0:"";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:0:"";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:13:"htmlcontent_1";s:552:"PHA+IDwvcD4NCjxwIHN0eWxlPSJ0ZXh0LWFsaWduOiBsZWZ0OyI+PGltZyBzcmM9Imh0dHA6Ly9jZWxpb2Rldi5jb20vaW1nL2Ntcy9Mb2dvX2Zvb3Rlcl9fLnBuZyIgYWx0PSIiIHdpZHRoPSIxMjYiIGhlaWdodD0iMzQiIC8+PC9wPg0KPHAgc3R5bGU9InRleHQtYWxpZ246IGxlZnQ7Ij5DZWxpbyBlcyB1bmEgY2FkZW5hIGRlIHRpZW5kYXMgZGUgcm9wYTxiciAvPnBhcmEgaG9tYnJlIGRlIG9yaWdlbiBmcmFuY8OpcywgZnVuZGFkYTxiciAvPnBvciBNYXJjIHkgTGF1cmVudCBHcm9zbWFuIGVuIDE5ODUuPGJyIC8+Q29uIGVsIHBhc28gZGUgbG9zIGHDsW9zLCBzZSBoYTxiciAvPmNvbnZlcnRpZG8gZW4gbGEgbWFyY2EgZXNlbmNpYWwgZGUgcm9wYTxiciAvPm1hc2N1bGluYSB5IGzDrWRlciBlbiBzdSBtZXJjYWRvLjwvcD4=";}s:4:"wkey";s:17:"key_1431373620317";}";}}', 33, 0, 1, 1414840717),
	('HomeCd', 0, '', '[{"index":0,"cls":"pts_slideshow","bgcolor":"","bgimage":"","fullwidth":"1","parallax":"0","sfxcls":null,"cols":[{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":12,"mdcol":12,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"sliderlayer","wkey":"key_1414836690480","name":"Slideshow Cd"}],"rows":[]}]},{"index":0,"cls":"","bgcolor":"","bgimage":"","fullwidth":0,"parallax":0,"sfxcls":"","cols":[{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":9,"mdcol":9,"smcol":12,"xscol":12,"widgets":[],"rows":[{"index":0,"cls":"","bgcolor":"","bgimage":"","fullwidth":0,"parallax":0,"sfxcls":"","cols":[{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":4,"mdcol":4,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"product_deal","wkey":"key_1414836746258","name":"Top deals"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":4,"mdcol":4,"smcol":6,"xscol":12,"widgets":[{"cls":"","wtype":"product_list","wkey":"key_1414836800759","name":"Top Rating"}],"rows":[]},{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":4,"mdcol":4,"smcol":6,"xscol":12,"widgets":[{"cls":"","wtype":"product_list","wkey":"key_1414836926854","name":"Top Sellers"}],"rows":[]}]},{"index":0,"cls":"","bgcolor":"","bgimage":"","fullwidth":0,"parallax":0,"sfxcls":"","cols":[{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":12,"mdcol":12,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"product_list","wkey":"key_1414837033847","name":"New Releasea"},{"cls":"","wtype":"product_list","wkey":"key_1414837103548","name":"Featured Albums"},{"cls":"","wtype":"product_list","wkey":"key_1414850940550","name":"Most Viewed"}],"rows":[]}]}]},{"index":0,"cls":"sidebar","sfxcls":null,"bgcolor":"","bgimage":"","inrow":0,"lgcol":3,"mdcol":3,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"categories_block","wkey":"key_1414850390466","name":"Shop by genre"},{"cls":"","wtype":"image","wkey":"key_1414850578294","name":"Banner Right1"},{"cls":"","wtype":"product_list","wkey":"key_1414850714803","name":"Product New"},{"cls":"","wtype":"image","wkey":"key_1414850769306","name":"Banner Right2"},{"cls":"","wtype":"image","wkey":"key_1414850805939","name":"Banner Right3"}],"rows":[]}]},{"index":0,"cls":"","bgcolor":"","bgimage":"","fullwidth":0,"parallax":0,"sfxcls":"","cols":[{"index":0,"cls":"","sfxcls":"","bgcolor":"","bgimage":"","inrow":0,"lgcol":12,"mdcol":12,"smcol":12,"xscol":12,"widgets":[{"cls":"","wtype":"bloglatest","wkey":"key_1414851084190","name":"From Our Blog"}],"rows":[]}]}]', 'a:13:{s:17:"key_1414836690480";a:1:{s:6:"config";s:552:"a:2:{s:6:"widget";a:13:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzY2OTA0ODA=";s:5:"wtype";s:16:"c2xpZGVybGF5ZXI=";s:11:"widget_name";s:16:"U2xpZGVzaG93IENk";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:16:"U2xpZGVzaG93IENk";s:14:"widget_title_2";s:16:"U2xpZGVzaG93IENk";s:14:"widget_title_3";s:16:"U2xpZGVzaG93IENk";s:14:"widget_title_4";s:16:"U2xpZGVzaG93IENk";s:14:"widget_title_5";s:16:"U2xpZGVzaG93IENk";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:8:"id_group";s:4:"Mg==";}s:4:"wkey";s:17:"key_1414836690480";}";}s:17:"key_1414836746258";a:1:{s:6:"config";s:580:"a:2:{s:6:"widget";a:15:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzY3NDYyNTg=";s:5:"wtype";s:16:"cHJvZHVjdF9kZWFs";s:11:"widget_name";s:12:"VG9wIGRlYWxz";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:12:"VG9wIGRlYWxz";s:14:"widget_title_2";s:12:"VG9wIGRlYWxz";s:14:"widget_title_3";s:12:"VG9wIGRlYWxz";s:14:"widget_title_4";s:12:"VG9wIGRlYWxz";s:14:"widget_title_5";s:12:"VG9wIGRlYWxz";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:5:"limit";s:4:"NA==";s:6:"column";s:4:"MQ==";s:12:"itemsperpage";s:4:"MQ==";}s:4:"wkey";s:17:"key_1414836746258";}";}s:17:"key_1414836800759";a:1:{s:6:"config";s:671:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzY4MDA3NTk=";s:5:"wtype";s:16:"cHJvZHVjdF9saXN0";s:11:"widget_name";s:16:"VG9wIFJhdGluZw==";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:16:"VG9wIFJhdGluZw==";s:14:"widget_title_2";s:16:"VG9wIFJhdGluZw==";s:14:"widget_title_3";s:16:"VG9wIFJhdGluZw==";s:14:"widget_title_4";s:16:"VG9wIFJhdGluZw==";s:14:"widget_title_5";s:16:"VG9wIFJhdGluZw==";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:5:"limit";s:4:"OA==";s:6:"column";s:4:"MQ==";s:12:"itemsperpage";s:4:"NA==";s:9:"list_type";s:12:"dG9wcmF0aW5n";s:9:"list_mode";s:8:"bGlzdDI=";}s:4:"wkey";s:17:"key_1414836800759";}";}s:17:"key_1414836926854";a:1:{s:6:"config";s:675:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzY5MjY4NTQ=";s:5:"wtype";s:16:"cHJvZHVjdF9saXN0";s:11:"widget_name";s:16:"VG9wIFNlbGxlcnM=";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:16:"VG9wIFNlbGxlcnM=";s:14:"widget_title_2";s:16:"VG9wIFNlbGxlcnM=";s:14:"widget_title_3";s:16:"VG9wIFNlbGxlcnM=";s:14:"widget_title_4";s:16:"VG9wIFNlbGxlcnM=";s:14:"widget_title_5";s:16:"VG9wIFNlbGxlcnM=";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:5:"limit";s:4:"OA==";s:6:"column";s:4:"MQ==";s:12:"itemsperpage";s:4:"NA==";s:9:"list_type";s:16:"YmVzdHNlbGxlcg==";s:9:"list_mode";s:8:"bGlzdDI=";}s:4:"wkey";s:17:"key_1414836926854";}";}s:17:"key_1414837033847";a:1:{s:6:"config";s:666:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzcwMzM4NDc=";s:5:"wtype";s:16:"cHJvZHVjdF9saXN0";s:11:"widget_name";s:16:"TmV3IFJlbGVhc2Vh";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:16:"TmV3IFJlbGVhc2Vh";s:14:"widget_title_2";s:16:"TmV3IFJlbGVhc2Vh";s:14:"widget_title_3";s:16:"TmV3IFJlbGVhc2Vh";s:14:"widget_title_4";s:16:"TmV3IFJlbGVhc2Vh";s:14:"widget_title_5";s:16:"TmV3IFJlbGVhc2Vh";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:5:"limit";s:4:"MTI=";s:6:"column";s:4:"NA==";s:12:"itemsperpage";s:4:"NA==";s:9:"list_type";s:8:"bmV3ZXN0";s:9:"list_mode";s:8:"Z3JpZA==";}s:4:"wkey";s:17:"key_1414837033847";}";}s:17:"key_1414837103548";a:1:{s:6:"config";s:695:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4MzcxMDM1NDg=";s:5:"wtype";s:16:"cHJvZHVjdF9saXN0";s:11:"widget_name";s:20:"RmVhdHVyZWQgQWxidW1z";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:20:"RmVhdHVyZWQgQWxidW1z";s:14:"widget_title_2";s:20:"RmVhdHVyZWQgQWxidW1z";s:14:"widget_title_3";s:20:"RmVhdHVyZWQgQWxidW1z";s:14:"widget_title_4";s:20:"RmVhdHVyZWQgQWxidW1z";s:14:"widget_title_5";s:20:"RmVhdHVyZWQgQWxidW1z";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:5:"limit";s:4:"MTI=";s:6:"column";s:4:"NA==";s:12:"itemsperpage";s:4:"NA==";s:9:"list_type";s:12:"ZmVhdHVyZWQ=";s:9:"list_mode";s:8:"Z3JpZA==";}s:4:"wkey";s:17:"key_1414837103548";}";}s:17:"key_1414850940550";a:1:{s:6:"config";s:671:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4NTA5NDA1NTA=";s:5:"wtype";s:16:"cHJvZHVjdF9saXN0";s:11:"widget_name";s:16:"TW9zdCBWaWV3ZWQ=";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:16:"TW9zdCBWaWV3ZWQ=";s:14:"widget_title_2";s:16:"TW9zdCBWaWV3ZWQ=";s:14:"widget_title_3";s:16:"TW9zdCBWaWV3ZWQ=";s:14:"widget_title_4";s:16:"TW9zdCBWaWV3ZWQ=";s:14:"widget_title_5";s:16:"TW9zdCBWaWV3ZWQ=";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:5:"limit";s:4:"MTI=";s:6:"column";s:4:"NA==";s:12:"itemsperpage";s:4:"NA==";s:9:"list_type";s:12:"bW9zdHZpZXc=";s:9:"list_mode";s:8:"Z3JpZA==";}s:4:"wkey";s:17:"key_1414850940550";}";}s:17:"key_1414850390466";a:1:{s:6:"config";s:714:"a:2:{s:6:"widget";a:16:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4NTAzOTA0NjY=";s:5:"wtype";s:24:"Y2F0ZWdvcmllc19ibG9jaw==";s:11:"widget_name";s:20:"U2hvcCBieSBnZW5yZQ==";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:20:"U2hvcCBieSBnZW5yZQ==";s:14:"widget_title_2";s:20:"U2hvcCBieSBnZW5yZQ==";s:14:"widget_title_3";s:20:"U2hvcCBieSBnZW5yZQ==";s:14:"widget_title_4";s:20:"U2hvcCBieSBnZW5yZQ==";s:14:"widget_title_5";s:20:"U2hvcCBieSBnZW5yZQ==";s:12:"addition_cls";s:12:"bm9wYWRkaW5n";s:8:"stylecls";s:16:"aGlnaGxpZ2h0ZWQ=";s:15:"categ_max_depth";s:4:"NA==";s:11:"categ_dhtml";s:4:"MQ==";s:10:"categ_sort";s:4:"MA==";s:14:"categ_sort_way";s:4:"MA==";}s:4:"wkey";s:17:"key_1414850390466";}";}s:17:"key_1414850578294";a:1:{s:6:"config";s:737:"a:2:{s:6:"widget";a:18:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4NTA1NzgyOTQ=";s:5:"wtype";s:8:"aW1hZ2U=";s:11:"widget_name";s:20:"QmFubmVyIFJpZ2h0MQ==";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:20:"QmFubmVyIFJpZ2h0MQ==";s:14:"widget_title_2";s:20:"QmFubmVyIFJpZ2h0MQ==";s:14:"widget_title_3";s:20:"QmFubmVyIFJpZ2h0MQ==";s:14:"widget_title_4";s:20:"QmFubmVyIFJpZ2h0MQ==";s:14:"widget_title_5";s:20:"QmFubmVyIFJpZ2h0MQ==";s:12:"addition_cls";s:24:"bm9wYWRkaW5nIG5vYm9yZGVy";s:8:"stylecls";s:0:"";s:9:"imagefile";s:24:"YmFubmVyLXJpZ2h0MS5qcGc=";s:4:"size";s:0:"";s:9:"animation";s:4:"NA==";s:9:"alignment";s:8:"bGVmdA==";s:7:"ispopup";s:4:"MA==";s:8:"link_url";s:0:"";}s:4:"wkey";s:17:"key_1414850578294";}";}s:17:"key_1414850714803";a:1:{s:6:"config";s:666:"a:2:{s:6:"widget";a:17:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4NTA3MTQ4MDM=";s:5:"wtype";s:16:"cHJvZHVjdF9saXN0";s:11:"widget_name";s:16:"UHJvZHVjdCBOZXc=";s:10:"show_title";s:4:"MQ==";s:14:"widget_title_1";s:16:"UHJvZHVjdCBOZXc=";s:14:"widget_title_2";s:16:"UHJvZHVjdCBOZXc=";s:14:"widget_title_3";s:16:"UHJvZHVjdCBOZXc=";s:14:"widget_title_4";s:16:"UHJvZHVjdCBOZXc=";s:14:"widget_title_5";s:16:"UHJvZHVjdCBOZXc=";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:5:"limit";s:4:"OA==";s:6:"column";s:4:"MQ==";s:12:"itemsperpage";s:4:"NA==";s:9:"list_type";s:8:"bmV3ZXN0";s:9:"list_mode";s:8:"bGlzdDE=";}s:4:"wkey";s:17:"key_1414850714803";}";}s:17:"key_1414850769306";a:1:{s:6:"config";s:737:"a:2:{s:6:"widget";a:18:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4NTA3NjkzMDY=";s:5:"wtype";s:8:"aW1hZ2U=";s:11:"widget_name";s:20:"QmFubmVyIFJpZ2h0Mg==";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:20:"QmFubmVyIFJpZ2h0Mg==";s:14:"widget_title_2";s:20:"QmFubmVyIFJpZ2h0Mg==";s:14:"widget_title_3";s:20:"QmFubmVyIFJpZ2h0Mg==";s:14:"widget_title_4";s:20:"QmFubmVyIFJpZ2h0Mg==";s:14:"widget_title_5";s:20:"QmFubmVyIFJpZ2h0Mg==";s:12:"addition_cls";s:24:"bm9wYWRkaW5nIG5vYm9yZGVy";s:8:"stylecls";s:0:"";s:9:"imagefile";s:24:"YmFubmVyLXJpZ2h0Mi5qcGc=";s:4:"size";s:0:"";s:9:"animation";s:4:"NA==";s:9:"alignment";s:8:"bGVmdA==";s:7:"ispopup";s:4:"MA==";s:8:"link_url";s:0:"";}s:4:"wkey";s:17:"key_1414850769306";}";}s:17:"key_1414850805939";a:1:{s:6:"config";s:737:"a:2:{s:6:"widget";a:18:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4NTA4MDU5Mzk=";s:5:"wtype";s:8:"aW1hZ2U=";s:11:"widget_name";s:20:"QmFubmVyIFJpZ2h0Mw==";s:10:"show_title";s:4:"MA==";s:14:"widget_title_1";s:20:"QmFubmVyIFJpZ2h0Mw==";s:14:"widget_title_2";s:20:"QmFubmVyIFJpZ2h0Mw==";s:14:"widget_title_3";s:20:"QmFubmVyIFJpZ2h0Mw==";s:14:"widget_title_4";s:20:"QmFubmVyIFJpZ2h0Mw==";s:14:"widget_title_5";s:20:"QmFubmVyIFJpZ2h0Mw==";s:12:"addition_cls";s:24:"bm9wYWRkaW5nIG5vYm9yZGVy";s:8:"stylecls";s:0:"";s:9:"imagefile";s:24:"YmFubmVyLXJpZ2h0My5qcGc=";s:4:"size";s:0:"";s:9:"animation";s:4:"NA==";s:9:"alignment";s:8:"bGVmdA==";s:7:"ispopup";s:4:"MA==";s:8:"link_url";s:0:"";}s:4:"wkey";s:17:"key_1414850805939";}";}s:17:"key_1414851084190";a:1:{s:6:"config";s:930:"a:2:{s:6:"widget";a:26:{s:15:"saveptsmegamenu";s:4:"MQ==";s:4:"wkey";s:24:"a2V5XzE0MTQ4NTEwODQxOTA=";s:5:"wtype";s:16:"YmxvZ2xhdGVzdA==";s:11:"widget_name";s:20:"RnJvbSBPdXIgQmxvZw==";s:14:"widget_title_1";s:20:"RnJvbSBPdXIgQmxvZw==";s:14:"widget_title_2";s:20:"RnJvbSBPdXIgQmxvZw==";s:14:"widget_title_3";s:20:"RnJvbSBPdXIgQmxvZw==";s:14:"widget_title_4";s:20:"RnJvbSBPdXIgQmxvZw==";s:14:"widget_title_5";s:20:"RnJvbSBPdXIgQmxvZw==";s:12:"addition_cls";s:0:"";s:8:"stylecls";s:0:"";s:3:"nbr";s:4:"OA==";s:5:"width";s:4:"Mjc5";s:6:"height";s:4:"MTYw";s:4:"page";s:4:"NA==";s:3:"col";s:4:"NA==";s:4:"intv";s:8:"ODAwMA==";s:4:"show";s:4:"MA==";s:10:"show_image";s:4:"MQ==";s:10:"show_title";s:4:"MQ==";s:13:"show_category";s:4:"MA==";s:12:"show_dateadd";s:4:"MQ==";s:12:"show_comment";s:4:"MA==";s:16:"show_description";s:4:"MQ==";s:13:"show_readmore";s:4:"MQ==";s:9:"list_mode";s:8:"Z3JpZA==";}s:4:"wkey";s:17:"key_1414851084190";}";}}', 34, 0, 0, 1414849996);
/*!40000 ALTER TABLE `ps_pagebuilderprofile` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_pagebuilderprofile_shop
DROP TABLE IF EXISTS `ps_pagebuilderprofile_shop`;
CREATE TABLE IF NOT EXISTS `ps_pagebuilderprofile_shop` (
  `id_pagebuilderprofile` int(11) NOT NULL,
  `id_shop` int(11) NOT NULL,
  `default` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_pagebuilderprofile`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_pagebuilderprofile_shop: ~10 rows (approximately)
/*!40000 ALTER TABLE `ps_pagebuilderprofile_shop` DISABLE KEYS */;
INSERT INTO `ps_pagebuilderprofile_shop` (`id_pagebuilderprofile`, `id_shop`, `default`) VALUES
	(17, 1, 0),
	(24, 1, 0),
	(26, 1, 0),
	(27, 1, 0),
	(28, 1, 0),
	(29, 1, 0),
	(31, 1, 0),
	(32, 1, 0),
	(33, 1, 0),
	(34, 1, 0);
/*!40000 ALTER TABLE `ps_pagebuilderprofile_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_pagenotfound
DROP TABLE IF EXISTS `ps_pagenotfound`;
CREATE TABLE IF NOT EXISTS `ps_pagenotfound` (
  `id_pagenotfound` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL DEFAULT '1',
  `id_shop_group` int(10) unsigned NOT NULL DEFAULT '1',
  `request_uri` varchar(256) NOT NULL,
  `http_referer` varchar(256) NOT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_pagenotfound`),
  KEY `date_add` (`date_add`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_pagenotfound: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_pagenotfound` DISABLE KEYS */;
INSERT INTO `ps_pagenotfound` (`id_pagenotfound`, `id_shop`, `id_shop_group`, `request_uri`, `http_referer`, `date_add`) VALUES
	(1, 1, 1, '/index.php?controller=404', 'http://celiodev.com/index.php', '2015-05-08 17:18:42');
/*!40000 ALTER TABLE `ps_pagenotfound` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_page_type
DROP TABLE IF EXISTS `ps_page_type`;
CREATE TABLE IF NOT EXISTS `ps_page_type` (
  `id_page_type` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id_page_type`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_page_type: ~3 rows (approximately)
/*!40000 ALTER TABLE `ps_page_type` DISABLE KEYS */;
INSERT INTO `ps_page_type` (`id_page_type`, `name`) VALUES
	(1, 'index'),
	(2, 'index'),
	(3, 'product');
/*!40000 ALTER TABLE `ps_page_type` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_page_viewed
DROP TABLE IF EXISTS `ps_page_viewed`;
CREATE TABLE IF NOT EXISTS `ps_page_viewed` (
  `id_page` int(10) unsigned NOT NULL,
  `id_shop_group` int(10) unsigned NOT NULL DEFAULT '1',
  `id_shop` int(10) unsigned NOT NULL DEFAULT '1',
  `id_date_range` int(10) unsigned NOT NULL,
  `counter` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_page`,`id_date_range`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_page_viewed: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_page_viewed` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_page_viewed` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_product
DROP TABLE IF EXISTS `ps_product`;
CREATE TABLE IF NOT EXISTS `ps_product` (
  `id_product` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_supplier` int(10) unsigned DEFAULT NULL,
  `id_manufacturer` int(10) unsigned DEFAULT NULL,
  `id_category_default` int(10) unsigned DEFAULT NULL,
  `id_shop_default` int(10) unsigned NOT NULL DEFAULT '1',
  `id_tax_rules_group` int(11) unsigned NOT NULL,
  `on_sale` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `online_only` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ean13` varchar(13) DEFAULT NULL,
  `upc` varchar(12) DEFAULT NULL,
  `ecotax` decimal(17,6) NOT NULL DEFAULT '0.000000',
  `quantity` int(10) NOT NULL DEFAULT '0',
  `minimal_quantity` int(10) unsigned NOT NULL DEFAULT '1',
  `price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `wholesale_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `unity` varchar(255) DEFAULT NULL,
  `unit_price_ratio` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `additional_shipping_cost` decimal(20,2) NOT NULL DEFAULT '0.00',
  `reference` varchar(32) DEFAULT NULL,
  `supplier_reference` varchar(32) DEFAULT NULL,
  `location` varchar(64) DEFAULT NULL,
  `width` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `height` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `depth` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `weight` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `out_of_stock` int(10) unsigned NOT NULL DEFAULT '2',
  `quantity_discount` tinyint(1) DEFAULT '0',
  `customizable` tinyint(2) NOT NULL DEFAULT '0',
  `uploadable_files` tinyint(4) NOT NULL DEFAULT '0',
  `text_fields` tinyint(4) NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `redirect_type` enum('','404','301','302') NOT NULL DEFAULT '',
  `id_product_redirected` int(10) unsigned NOT NULL DEFAULT '0',
  `available_for_order` tinyint(1) NOT NULL DEFAULT '1',
  `available_date` date NOT NULL DEFAULT '0000-00-00',
  `condition` enum('new','used','refurbished') NOT NULL DEFAULT 'new',
  `show_price` tinyint(1) NOT NULL DEFAULT '1',
  `indexed` tinyint(1) NOT NULL DEFAULT '0',
  `visibility` enum('both','catalog','search','none') NOT NULL DEFAULT 'both',
  `cache_is_pack` tinyint(1) NOT NULL DEFAULT '0',
  `cache_has_attachments` tinyint(1) NOT NULL DEFAULT '0',
  `is_virtual` tinyint(1) NOT NULL DEFAULT '0',
  `cache_default_attribute` int(10) unsigned DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `advanced_stock_management` tinyint(1) NOT NULL DEFAULT '0',
  `pack_stock_type` int(11) unsigned NOT NULL DEFAULT '3',
  PRIMARY KEY (`id_product`),
  KEY `product_supplier` (`id_supplier`),
  KEY `product_manufacturer` (`id_manufacturer`),
  KEY `id_category_default` (`id_category_default`),
  KEY `indexed` (`indexed`),
  KEY `date_add` (`date_add`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_product: ~7 rows (approximately)
/*!40000 ALTER TABLE `ps_product` DISABLE KEYS */;
INSERT INTO `ps_product` (`id_product`, `id_supplier`, `id_manufacturer`, `id_category_default`, `id_shop_default`, `id_tax_rules_group`, `on_sale`, `online_only`, `ean13`, `upc`, `ecotax`, `quantity`, `minimal_quantity`, `price`, `wholesale_price`, `unity`, `unit_price_ratio`, `additional_shipping_cost`, `reference`, `supplier_reference`, `location`, `width`, `height`, `depth`, `weight`, `out_of_stock`, `quantity_discount`, `customizable`, `uploadable_files`, `text_fields`, `active`, `redirect_type`, `id_product_redirected`, `available_for_order`, `available_date`, `condition`, `show_price`, `indexed`, `visibility`, `cache_is_pack`, `cache_has_attachments`, `is_virtual`, `cache_default_attribute`, `date_add`, `date_upd`, `advanced_stock_management`, `pack_stock_type`) VALUES
	(1, 1, 1, 5, 1, 1, 0, 0, '0', '', 0.000000, 0, 1, 16.510000, 4.950000, '', 0.000000, 0.00, 'demo_1', '', '', 0.000000, 0.000000, 0.000000, 0.000000, 2, 0, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 1, '2015-05-08 14:00:21', '2015-05-12 10:23:23', 0, 3),
	(2, 1, 1, 7, 1, 1, 0, 0, '0', '', 0.000000, 0, 1, 26.999852, 8.100000, '', 0.000000, 0.00, 'demo_2', '', '', 0.000000, 0.000000, 0.000000, 0.000000, 2, 0, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 7, '2015-05-08 14:00:21', '2015-05-08 14:00:21', 0, 3),
	(3, 1, 1, 9, 1, 1, 0, 0, '0', '', 0.000000, 0, 1, 25.999852, 7.800000, '', 0.000000, 0.00, 'demo_3', '', '', 0.000000, 0.000000, 0.000000, 0.000000, 2, 0, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 13, '2015-05-08 14:00:21', '2015-05-08 14:00:21', 0, 3),
	(4, 1, 1, 10, 1, 1, 0, 0, '0', '', 0.000000, 0, 1, 50.994153, 15.300000, '', 0.000000, 0.00, 'demo_4', '', '', 0.000000, 0.000000, 0.000000, 0.000000, 2, 0, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 16, '2015-05-08 14:00:21', '2015-05-08 14:00:21', 0, 3),
	(5, 1, 1, 11, 1, 1, 0, 0, '0', '', 0.000000, 0, 1, 30.506321, 9.150000, '', 0.000000, 0.00, 'demo_5', '', '', 0.000000, 0.000000, 0.000000, 0.000000, 2, 0, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 19, '2015-05-08 14:00:21', '2015-05-08 14:00:21', 0, 3),
	(6, 1, 1, 11, 1, 1, 0, 0, '0', '', 0.000000, 0, 1, 30.502569, 9.150000, '', 0.000000, 0.00, 'demo_6', '', '', 0.000000, 0.000000, 0.000000, 0.000000, 2, 0, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 31, '2015-05-08 14:00:21', '2015-05-08 14:00:21', 0, 3),
	(7, 1, 1, 11, 1, 1, 0, 0, '0', '', 0.000000, 0, 1, 20.501236, 6.150000, '', 0.000000, 0.00, 'demo_7', '', '', 0.000000, 0.000000, 0.000000, 0.000000, 2, 0, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 34, '2015-05-08 14:00:21', '2015-05-08 14:00:21', 0, 3);
/*!40000 ALTER TABLE `ps_product` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_product_attachment
DROP TABLE IF EXISTS `ps_product_attachment`;
CREATE TABLE IF NOT EXISTS `ps_product_attachment` (
  `id_product` int(10) unsigned NOT NULL,
  `id_attachment` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_product`,`id_attachment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_product_attachment: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_product_attachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_product_attachment` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_product_attribute
DROP TABLE IF EXISTS `ps_product_attribute`;
CREATE TABLE IF NOT EXISTS `ps_product_attribute` (
  `id_product_attribute` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_product` int(10) unsigned NOT NULL,
  `reference` varchar(32) DEFAULT NULL,
  `supplier_reference` varchar(32) DEFAULT NULL,
  `location` varchar(64) DEFAULT NULL,
  `ean13` varchar(13) DEFAULT NULL,
  `upc` varchar(12) DEFAULT NULL,
  `wholesale_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `ecotax` decimal(17,6) NOT NULL DEFAULT '0.000000',
  `quantity` int(10) NOT NULL DEFAULT '0',
  `weight` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `unit_price_impact` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `default_on` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `minimal_quantity` int(10) unsigned NOT NULL DEFAULT '1',
  `available_date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`id_product_attribute`),
  KEY `product_attribute_product` (`id_product`),
  KEY `reference` (`reference`),
  KEY `supplier_reference` (`supplier_reference`),
  KEY `product_default` (`id_product`,`default_on`),
  KEY `id_product_id_product_attribute` (`id_product_attribute`,`id_product`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_product_attribute: ~45 rows (approximately)
/*!40000 ALTER TABLE `ps_product_attribute` DISABLE KEYS */;
INSERT INTO `ps_product_attribute` (`id_product_attribute`, `id_product`, `reference`, `supplier_reference`, `location`, `ean13`, `upc`, `wholesale_price`, `price`, `ecotax`, `quantity`, `weight`, `unit_price_impact`, `default_on`, `minimal_quantity`, `available_date`) VALUES
	(1, 1, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 1, 1, '0000-00-00'),
	(2, 1, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(3, 1, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(4, 1, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(5, 1, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(6, 1, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(7, 2, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 1, 1, '0000-00-00'),
	(8, 2, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(9, 2, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(10, 2, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(11, 2, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(12, 2, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(13, 3, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 1, 1, '0000-00-00'),
	(14, 3, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(15, 3, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(16, 4, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 1, 1, '0000-00-00'),
	(17, 4, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(18, 4, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(19, 5, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 1, 1, '0000-00-00'),
	(20, 5, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(21, 5, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(22, 5, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(23, 5, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(24, 5, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(25, 5, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(26, 5, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(27, 5, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(28, 5, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(29, 5, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(30, 5, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(31, 6, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 1, 1, '0000-00-00'),
	(32, 6, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(33, 6, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(34, 7, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 1, 1, '0000-00-00'),
	(35, 7, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(36, 7, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 100, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(37, 7, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 0, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(38, 7, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 0, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(39, 7, '', '', '', '', '', 6.150000, 0.000000, 0.000000, 0, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(40, 6, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 0, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(41, 6, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 0, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(42, 6, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 0, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(43, 4, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 0, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(44, 4, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 0, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(45, 4, '', '', '', '', '', 0.000000, 0.000000, 0.000000, 0, 0.000000, 0.000000, 0, 1, '0000-00-00');
/*!40000 ALTER TABLE `ps_product_attribute` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_product_attribute_combination
DROP TABLE IF EXISTS `ps_product_attribute_combination`;
CREATE TABLE IF NOT EXISTS `ps_product_attribute_combination` (
  `id_attribute` int(10) unsigned NOT NULL,
  `id_product_attribute` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_attribute`,`id_product_attribute`),
  KEY `id_product_attribute` (`id_product_attribute`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_product_attribute_combination: ~90 rows (approximately)
/*!40000 ALTER TABLE `ps_product_attribute_combination` DISABLE KEYS */;
INSERT INTO `ps_product_attribute_combination` (`id_attribute`, `id_product_attribute`) VALUES
	(1, 1),
	(13, 1),
	(1, 2),
	(14, 2),
	(2, 3),
	(13, 3),
	(2, 4),
	(14, 4),
	(3, 5),
	(13, 5),
	(3, 6),
	(14, 6),
	(1, 7),
	(11, 7),
	(1, 8),
	(8, 8),
	(2, 9),
	(11, 9),
	(2, 10),
	(8, 10),
	(3, 11),
	(11, 11),
	(3, 12),
	(8, 12),
	(1, 13),
	(13, 13),
	(2, 14),
	(13, 14),
	(3, 15),
	(13, 15),
	(1, 16),
	(7, 16),
	(2, 17),
	(7, 17),
	(3, 18),
	(7, 18),
	(1, 19),
	(16, 19),
	(1, 20),
	(14, 20),
	(1, 21),
	(13, 21),
	(1, 22),
	(11, 22),
	(2, 23),
	(16, 23),
	(2, 24),
	(14, 24),
	(2, 25),
	(13, 25),
	(2, 26),
	(11, 26),
	(3, 27),
	(16, 27),
	(3, 28),
	(14, 28),
	(3, 29),
	(13, 29),
	(3, 30),
	(11, 30),
	(1, 31),
	(16, 31),
	(2, 32),
	(16, 32),
	(3, 33),
	(16, 33),
	(1, 34),
	(16, 34),
	(2, 35),
	(16, 35),
	(3, 36),
	(16, 36),
	(1, 37),
	(15, 37),
	(2, 38),
	(15, 38),
	(3, 39),
	(15, 39),
	(1, 40),
	(8, 40),
	(2, 41),
	(8, 41),
	(3, 42),
	(8, 42),
	(1, 43),
	(24, 43),
	(2, 44),
	(24, 44),
	(3, 45),
	(24, 45);
/*!40000 ALTER TABLE `ps_product_attribute_combination` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_product_attribute_image
DROP TABLE IF EXISTS `ps_product_attribute_image`;
CREATE TABLE IF NOT EXISTS `ps_product_attribute_image` (
  `id_product_attribute` int(10) unsigned NOT NULL,
  `id_image` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_product_attribute`,`id_image`),
  KEY `id_image` (`id_image`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_product_attribute_image: ~63 rows (approximately)
/*!40000 ALTER TABLE `ps_product_attribute_image` DISABLE KEYS */;
INSERT INTO `ps_product_attribute_image` (`id_product_attribute`, `id_image`) VALUES
	(1, 1),
	(3, 1),
	(5, 1),
	(1, 2),
	(3, 2),
	(5, 2),
	(2, 3),
	(4, 3),
	(6, 3),
	(2, 4),
	(4, 4),
	(6, 4),
	(8, 5),
	(10, 5),
	(12, 5),
	(8, 6),
	(10, 6),
	(12, 6),
	(7, 7),
	(9, 7),
	(11, 7),
	(16, 10),
	(17, 10),
	(18, 10),
	(43, 11),
	(44, 11),
	(45, 11),
	(19, 12),
	(23, 12),
	(27, 12),
	(20, 13),
	(24, 13),
	(28, 13),
	(21, 14),
	(25, 14),
	(29, 14),
	(22, 15),
	(26, 15),
	(30, 15),
	(31, 16),
	(32, 16),
	(33, 16),
	(31, 17),
	(32, 17),
	(33, 17),
	(40, 18),
	(41, 18),
	(42, 18),
	(40, 19),
	(41, 19),
	(42, 19),
	(34, 20),
	(35, 20),
	(36, 20),
	(34, 21),
	(35, 21),
	(36, 21),
	(37, 22),
	(38, 22),
	(39, 22),
	(37, 23),
	(38, 23),
	(39, 23);
/*!40000 ALTER TABLE `ps_product_attribute_image` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_product_attribute_shop
DROP TABLE IF EXISTS `ps_product_attribute_shop`;
CREATE TABLE IF NOT EXISTS `ps_product_attribute_shop` (
  `id_product_attribute` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `wholesale_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `ecotax` decimal(17,6) NOT NULL DEFAULT '0.000000',
  `weight` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `unit_price_impact` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `default_on` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `minimal_quantity` int(10) unsigned NOT NULL DEFAULT '1',
  `available_date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`id_product_attribute`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_product_attribute_shop: ~45 rows (approximately)
/*!40000 ALTER TABLE `ps_product_attribute_shop` DISABLE KEYS */;
INSERT INTO `ps_product_attribute_shop` (`id_product_attribute`, `id_shop`, `wholesale_price`, `price`, `ecotax`, `weight`, `unit_price_impact`, `default_on`, `minimal_quantity`, `available_date`) VALUES
	(1, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 1, 1, '0000-00-00'),
	(2, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(3, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(4, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(5, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(6, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(7, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 1, 1, '0000-00-00'),
	(8, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(9, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(10, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(11, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(12, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(13, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 1, 1, '0000-00-00'),
	(14, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(15, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(16, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 1, 1, '0000-00-00'),
	(17, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(18, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(19, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 1, 1, '0000-00-00'),
	(20, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(21, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(22, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(23, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(24, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(25, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(26, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(27, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(28, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(29, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(30, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(31, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 1, 1, '0000-00-00'),
	(32, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(33, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(34, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 1, 1, '0000-00-00'),
	(35, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(36, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(37, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(38, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(39, 1, 6.150000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(40, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(41, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(42, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(43, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(44, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00'),
	(45, 1, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0, 1, '0000-00-00');
/*!40000 ALTER TABLE `ps_product_attribute_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_product_carrier
DROP TABLE IF EXISTS `ps_product_carrier`;
CREATE TABLE IF NOT EXISTS `ps_product_carrier` (
  `id_product` int(10) unsigned NOT NULL,
  `id_carrier_reference` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_product`,`id_carrier_reference`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_product_carrier: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_product_carrier` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_product_carrier` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_product_comment
DROP TABLE IF EXISTS `ps_product_comment`;
CREATE TABLE IF NOT EXISTS `ps_product_comment` (
  `id_product_comment` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_product` int(10) unsigned NOT NULL,
  `id_customer` int(10) unsigned NOT NULL,
  `id_guest` int(10) unsigned DEFAULT NULL,
  `title` varchar(64) DEFAULT NULL,
  `content` text NOT NULL,
  `customer_name` varchar(64) DEFAULT NULL,
  `grade` float unsigned NOT NULL,
  `validate` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_product_comment`),
  KEY `id_product` (`id_product`),
  KEY `id_customer` (`id_customer`),
  KEY `id_guest` (`id_guest`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_product_comment: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_product_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_product_comment` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_product_comment_criterion
DROP TABLE IF EXISTS `ps_product_comment_criterion`;
CREATE TABLE IF NOT EXISTS `ps_product_comment_criterion` (
  `id_product_comment_criterion` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_product_comment_criterion_type` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_product_comment_criterion`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_product_comment_criterion: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_product_comment_criterion` DISABLE KEYS */;
INSERT INTO `ps_product_comment_criterion` (`id_product_comment_criterion`, `id_product_comment_criterion_type`, `active`) VALUES
	(1, 1, 1);
/*!40000 ALTER TABLE `ps_product_comment_criterion` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_product_comment_criterion_category
DROP TABLE IF EXISTS `ps_product_comment_criterion_category`;
CREATE TABLE IF NOT EXISTS `ps_product_comment_criterion_category` (
  `id_product_comment_criterion` int(10) unsigned NOT NULL,
  `id_category` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_product_comment_criterion`,`id_category`),
  KEY `id_category` (`id_category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_product_comment_criterion_category: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_product_comment_criterion_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_product_comment_criterion_category` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_product_comment_criterion_lang
DROP TABLE IF EXISTS `ps_product_comment_criterion_lang`;
CREATE TABLE IF NOT EXISTS `ps_product_comment_criterion_lang` (
  `id_product_comment_criterion` int(11) unsigned NOT NULL,
  `id_lang` int(11) unsigned NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id_product_comment_criterion`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_product_comment_criterion_lang: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_product_comment_criterion_lang` DISABLE KEYS */;
INSERT INTO `ps_product_comment_criterion_lang` (`id_product_comment_criterion`, `id_lang`, `name`) VALUES
	(1, 1, 'Quality');
/*!40000 ALTER TABLE `ps_product_comment_criterion_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_product_comment_criterion_product
DROP TABLE IF EXISTS `ps_product_comment_criterion_product`;
CREATE TABLE IF NOT EXISTS `ps_product_comment_criterion_product` (
  `id_product` int(10) unsigned NOT NULL,
  `id_product_comment_criterion` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_product`,`id_product_comment_criterion`),
  KEY `id_product_comment_criterion` (`id_product_comment_criterion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_product_comment_criterion_product: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_product_comment_criterion_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_product_comment_criterion_product` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_product_comment_grade
DROP TABLE IF EXISTS `ps_product_comment_grade`;
CREATE TABLE IF NOT EXISTS `ps_product_comment_grade` (
  `id_product_comment` int(10) unsigned NOT NULL,
  `id_product_comment_criterion` int(10) unsigned NOT NULL,
  `grade` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_product_comment`,`id_product_comment_criterion`),
  KEY `id_product_comment_criterion` (`id_product_comment_criterion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_product_comment_grade: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_product_comment_grade` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_product_comment_grade` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_product_comment_report
DROP TABLE IF EXISTS `ps_product_comment_report`;
CREATE TABLE IF NOT EXISTS `ps_product_comment_report` (
  `id_product_comment` int(10) unsigned NOT NULL,
  `id_customer` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_product_comment`,`id_customer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_product_comment_report: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_product_comment_report` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_product_comment_report` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_product_comment_usefulness
DROP TABLE IF EXISTS `ps_product_comment_usefulness`;
CREATE TABLE IF NOT EXISTS `ps_product_comment_usefulness` (
  `id_product_comment` int(10) unsigned NOT NULL,
  `id_customer` int(10) unsigned NOT NULL,
  `usefulness` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id_product_comment`,`id_customer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_product_comment_usefulness: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_product_comment_usefulness` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_product_comment_usefulness` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_product_country_tax
DROP TABLE IF EXISTS `ps_product_country_tax`;
CREATE TABLE IF NOT EXISTS `ps_product_country_tax` (
  `id_product` int(11) NOT NULL,
  `id_country` int(11) NOT NULL,
  `id_tax` int(11) NOT NULL,
  PRIMARY KEY (`id_product`,`id_country`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_product_country_tax: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_product_country_tax` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_product_country_tax` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_product_download
DROP TABLE IF EXISTS `ps_product_download`;
CREATE TABLE IF NOT EXISTS `ps_product_download` (
  `id_product_download` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_product` int(10) unsigned NOT NULL,
  `display_filename` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_expiration` datetime DEFAULT NULL,
  `nb_days_accessible` int(10) unsigned DEFAULT NULL,
  `nb_downloadable` int(10) unsigned DEFAULT '1',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `is_shareable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_product_download`),
  KEY `product_active` (`id_product`,`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_product_download: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_product_download` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_product_download` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_product_group_reduction_cache
DROP TABLE IF EXISTS `ps_product_group_reduction_cache`;
CREATE TABLE IF NOT EXISTS `ps_product_group_reduction_cache` (
  `id_product` int(10) unsigned NOT NULL,
  `id_group` int(10) unsigned NOT NULL,
  `reduction` decimal(4,3) NOT NULL,
  PRIMARY KEY (`id_product`,`id_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_product_group_reduction_cache: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_product_group_reduction_cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_product_group_reduction_cache` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_product_lang
DROP TABLE IF EXISTS `ps_product_lang`;
CREATE TABLE IF NOT EXISTS `ps_product_lang` (
  `id_product` int(10) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_lang` int(10) unsigned NOT NULL,
  `description` text,
  `description_short` text,
  `link_rewrite` varchar(128) NOT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_title` varchar(128) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `available_now` varchar(255) DEFAULT NULL,
  `available_later` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_product`,`id_shop`,`id_lang`),
  KEY `id_lang` (`id_lang`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_product_lang: ~7 rows (approximately)
/*!40000 ALTER TABLE `ps_product_lang` DISABLE KEYS */;
INSERT INTO `ps_product_lang` (`id_product`, `id_shop`, `id_lang`, `description`, `description_short`, `link_rewrite`, `meta_description`, `meta_keywords`, `meta_title`, `name`, `available_now`, `available_later`) VALUES
	(1, 1, 1, '<p>Fashion lleva diseñando colecciones increíbles desde 2010. La marca ofrece diseños femeninos, con elegantes prendas para combinar y las últimas tendencias en vestidos. Las colecciones han evolucionado hacia una línea prêt-à-porter en la que cada elemento resulta indispensable en el fondo de armario de una mujer. ¿El resultado? Looks frescos, sencillos y muy chic, con una elegancia juvenil y un estilo único e inconfundible. Todas las prendas se confeccionan en Italia, prestando atención hasta al más mínimo detalle. Ahora Fashion ha ampliado su catálogo para incluir todo tipo de complementos: ¡zapatos, sombreros, cinturones y mucho más!</p>', '<p>Camiseta de manga corta con efecto desteñido y escote cerrado. Material suave y elástico para un ajuste cómodo. ¡Combínala con un sombrero de paja y estarás lista para el verano!</p>', 'camiseta-destenida-manga-corta', '', '', '', 'Camiseta efecto desteñido de manga corta', 'En stock', ''),
	(2, 1, 1, '<p>Fashion lleva diseñando colecciones increíbles desde 2010. La marca ofrece diseños femeninos, con elegantes prendas para combinar y las últimas tendencias en vestidos. Las colecciones han evolucionado hacia una línea prêt-à-porter en la que cada elemento resulta indispensable en el fondo de armario de una mujer. ¿El resultado? Looks frescos, sencillos y muy chic, con una elegancia juvenil y un estilo único e inconfundible. Todas las prendas se confeccionan en Italia, prestando atención hasta al más mínimo detalle. Ahora Fashion ha ampliado su catálogo para incluir todo tipo de complementos: ¡zapatos, sombreros, cinturones y mucho más! </ p>', '<p>Blusa de manga corta con detalle drapeado muy femenino en la manga.</p>', 'blusa', '', '', '', 'Blusa', 'En stock', ''),
	(3, 1, 1, '<p>Fashion lleva diseñando colecciones increíbles desde 2010. La marca ofrece diseños femeninos, con elegantes prendas para combinar y las últimas tendencias en vestidos. Las colecciones han evolucionado hacia una línea prêt-à-porter en la que cada elemento resulta indispensable en el fondo de armario de una mujer. ¿El resultado? Looks frescos, sencillos y muy chic, con una elegancia juvenil y un estilo único e inconfundible. Todas las prendas se confeccionan en Italia,prestando atención hasta al más mínimo detalle. Ahora Fashion ha ampliado su catálogo para incluir todo tipo de complementos: ¡zapatos, sombreros, cinturones y mucho más! </ p>', '<p>Vestido doble estampado 100% algodón. Top de rayas negras y blancas y falda skater naranja de cintura alta.</p>', 'vestido-estampado', '', '', '', 'Vestido estampado', 'En stock', ''),
	(4, 1, 1, '<p>Fashion lleva diseñando increíbles colecciones desde 2010. La marca ofrece diseños femeninos, con elegantes prendas para combinar y las últimas tendencias en vestidos. Las colecciones han evolucionado hacia una línea prêt-à-porter en la que cada elemento resulta indispensable en el fondo de armario de una mujer. ¿El resultado? Looks frescos, sencillos y muy chic, con una elegancia juvenil y un estilo único e inconfundible. Todas las prendas se confeccionan en Italia, prestando atención hasta al más mínimo detalle. Ahora Fashion ha ampliado su catálogo para incluir todo tipo de complementos: ¡zapatos, sombreros, cinturones y mucho más! </ p>', '<p>Vestido de noche estampado con mangas rectas, cinturón negro y volantes.</p>', 'vestido-estampado', '', '', '', 'Vestido estampado', 'En stock', ''),
	(5, 1, 1, '<p>Fashion lleva diseñando increíbles colecciones desde 2010. La marca ofrece diseños femeninos, con elegantes prendas para combinar y las últimas tendencias en vestidos. Las colecciones han evolucionado hacia una línea prêt-à-porter en la que cada elemento resulta indispensable en el fondo de armario de una mujer. ¿El resultado? Looks frescos, sencillos y muy chic, con una elegancia juvenil y un estilo único e inconfundible. Todas las prendas se confeccionan en Italia, prestando atención hasta al más mínimo detalle. Ahora Fashion ha ampliado su catálogo para incluir todo tipo de complementos: ¡zapatos, sombreros, cinturones y mucho más! </ p>', '<p>Vestido largo estampado con tirantes finos ajustables. Escote en V, fruncido debajo del pecho y volantes en la parte inferior del vestido.</p>', 'vestido-verano-estampado', '', '', '', 'Vestido de verano estampado', 'En stock', ''),
	(6, 1, 1, '<p>Fashion lleva diseñando increíbles colecciones desde 2010. La marca ofrece diseños femeninos, con elegantes prendas para combinar y las últimas tendencias en vestidos. Las colecciones han evolucionado hacia una línea prêt-à-porter en la que cada elemento resulta indispensable en el fondo de armario de una mujer. ¿El resultado? Looks frescos, sencillos y muy chic, con una elegancia juvenil y un estilo único e inconfundible. Todas las prendas se confeccionan en Italia, prestando atención hasta al más mínimo detalle. Ahora Fashion ha ampliado su catálogo para incluir todo tipo de complementos: ¡zapatos, sombreros, cinturones y mucho más! </ p>', '<p>Vestido sin mangas de gasa hasta la rodilla. Escote en V con elástico debajo del pecho.</p>', 'vestido-verano-estampado', '', '', '', 'Vestido de verano estampado', 'En stock', ''),
	(7, 1, 1, '<p>Fashion lleva diseñando increíbles colecciones desde 2010. La marca ofrece diseños femeninos, con elegantes prendas para combinar y las últimas tendencias en vestidos. Las colecciones han evolucionado hacia una línea prêt-à-porter en la que cada elemento resulta indispensable en el fondo de armario de una mujer. ¿El resultado? Looks frescos, sencillos y muy chic, con una elegancia juvenil y un estilo único e inconfundible. Todas las prendas se confeccionan en Italia, yprestando atención hasta al más mínimo detalle. Ahora Fashion ha ampliado su catálogo para incluir todo tipo de complementos: ¡zapatos, sombreros, cinturones y mucho más! </ p>', '<p>Vestido sin mangas hasta la rodilla, tejido de gasa estampado. Escote pronunciado.</p>', 'vestido-estampado-gasa', '', '', '', 'Vestido de gasa estampado', 'En stock', '');
/*!40000 ALTER TABLE `ps_product_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_product_sale
DROP TABLE IF EXISTS `ps_product_sale`;
CREATE TABLE IF NOT EXISTS `ps_product_sale` (
  `id_product` int(10) unsigned NOT NULL,
  `quantity` int(10) unsigned NOT NULL DEFAULT '0',
  `sale_nbr` int(10) unsigned NOT NULL DEFAULT '0',
  `date_upd` date NOT NULL,
  PRIMARY KEY (`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_product_sale: ~6 rows (approximately)
/*!40000 ALTER TABLE `ps_product_sale` DISABLE KEYS */;
INSERT INTO `ps_product_sale` (`id_product`, `quantity`, `sale_nbr`, `date_upd`) VALUES
	(1, 3, 3, '2015-05-08'),
	(2, 4, 4, '2015-05-08'),
	(3, 3, 3, '2015-05-08'),
	(5, 1, 1, '2015-05-08'),
	(6, 2, 2, '2015-05-08'),
	(7, 2, 2, '2015-05-08');
/*!40000 ALTER TABLE `ps_product_sale` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_product_shop
DROP TABLE IF EXISTS `ps_product_shop`;
CREATE TABLE IF NOT EXISTS `ps_product_shop` (
  `id_product` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_category_default` int(10) unsigned DEFAULT NULL,
  `id_tax_rules_group` int(11) unsigned NOT NULL,
  `on_sale` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `online_only` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ecotax` decimal(17,6) NOT NULL DEFAULT '0.000000',
  `minimal_quantity` int(10) unsigned NOT NULL DEFAULT '1',
  `price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `wholesale_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `unity` varchar(255) DEFAULT NULL,
  `unit_price_ratio` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `additional_shipping_cost` decimal(20,2) NOT NULL DEFAULT '0.00',
  `customizable` tinyint(2) NOT NULL DEFAULT '0',
  `uploadable_files` tinyint(4) NOT NULL DEFAULT '0',
  `text_fields` tinyint(4) NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `redirect_type` enum('','404','301','302') NOT NULL DEFAULT '',
  `id_product_redirected` int(10) unsigned NOT NULL DEFAULT '0',
  `available_for_order` tinyint(1) NOT NULL DEFAULT '1',
  `available_date` date NOT NULL DEFAULT '0000-00-00',
  `condition` enum('new','used','refurbished') NOT NULL DEFAULT 'new',
  `show_price` tinyint(1) NOT NULL DEFAULT '1',
  `indexed` tinyint(1) NOT NULL DEFAULT '0',
  `visibility` enum('both','catalog','search','none') NOT NULL DEFAULT 'both',
  `cache_default_attribute` int(10) unsigned DEFAULT NULL,
  `advanced_stock_management` tinyint(1) NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `pack_stock_type` int(11) unsigned NOT NULL DEFAULT '3',
  PRIMARY KEY (`id_product`,`id_shop`),
  KEY `id_category_default` (`id_category_default`),
  KEY `date_add` (`date_add`,`active`,`visibility`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_product_shop: ~7 rows (approximately)
/*!40000 ALTER TABLE `ps_product_shop` DISABLE KEYS */;
INSERT INTO `ps_product_shop` (`id_product`, `id_shop`, `id_category_default`, `id_tax_rules_group`, `on_sale`, `online_only`, `ecotax`, `minimal_quantity`, `price`, `wholesale_price`, `unity`, `unit_price_ratio`, `additional_shipping_cost`, `customizable`, `uploadable_files`, `text_fields`, `active`, `redirect_type`, `id_product_redirected`, `available_for_order`, `available_date`, `condition`, `show_price`, `indexed`, `visibility`, `cache_default_attribute`, `advanced_stock_management`, `date_add`, `date_upd`, `pack_stock_type`) VALUES
	(1, 1, 5, 1, 0, 0, 0.000000, 1, 16.510000, 4.950000, '', 0.000000, 0.00, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 1, 0, '2015-05-08 14:00:21', '2015-05-12 10:23:23', 3),
	(2, 1, 7, 1, 0, 0, 0.000000, 1, 26.999852, 8.100000, '', 0.000000, 0.00, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 7, 0, '2015-05-08 14:00:21', '2015-05-08 14:00:21', 3),
	(3, 1, 9, 1, 0, 0, 0.000000, 1, 25.999852, 7.800000, '', 0.000000, 0.00, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 13, 0, '2015-05-08 14:00:21', '2015-05-08 14:00:21', 3),
	(4, 1, 10, 1, 0, 0, 0.000000, 1, 50.994153, 15.300000, '', 0.000000, 0.00, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 16, 0, '2015-05-08 14:00:21', '2015-05-08 14:00:21', 3),
	(5, 1, 11, 1, 0, 0, 0.000000, 1, 30.506321, 9.150000, '', 0.000000, 0.00, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 19, 0, '2015-05-08 14:00:21', '2015-05-08 14:00:21', 3),
	(6, 1, 11, 1, 0, 0, 0.000000, 1, 30.502569, 9.150000, '', 0.000000, 0.00, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 31, 0, '2015-05-08 14:00:21', '2015-05-08 14:00:21', 3),
	(7, 1, 11, 1, 0, 0, 0.000000, 1, 20.501236, 6.150000, '', 0.000000, 0.00, 0, 0, 0, 1, '404', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 34, 0, '2015-05-08 14:00:21', '2015-05-08 14:00:21', 3);
/*!40000 ALTER TABLE `ps_product_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_product_supplier
DROP TABLE IF EXISTS `ps_product_supplier`;
CREATE TABLE IF NOT EXISTS `ps_product_supplier` (
  `id_product_supplier` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_product` int(11) unsigned NOT NULL,
  `id_product_attribute` int(11) unsigned NOT NULL DEFAULT '0',
  `id_supplier` int(11) unsigned NOT NULL,
  `product_supplier_reference` varchar(32) DEFAULT NULL,
  `product_supplier_price_te` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `id_currency` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_product_supplier`),
  UNIQUE KEY `id_product` (`id_product`,`id_product_attribute`,`id_supplier`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_product_supplier: ~13 rows (approximately)
/*!40000 ALTER TABLE `ps_product_supplier` DISABLE KEYS */;
INSERT INTO `ps_product_supplier` (`id_product_supplier`, `id_product`, `id_product_attribute`, `id_supplier`, `product_supplier_reference`, `product_supplier_price_te`, `id_currency`) VALUES
	(1, 1, 0, 1, '', 0.000000, 0),
	(2, 2, 0, 1, '', 0.000000, 0),
	(3, 3, 0, 1, '', 0.000000, 0),
	(4, 4, 0, 1, '', 0.000000, 0),
	(5, 5, 0, 1, '', 0.000000, 0),
	(6, 6, 0, 1, '', 0.000000, 0),
	(7, 7, 0, 1, '', 0.000000, 0),
	(8, 1, 1, 1, '', 0.000000, 1),
	(9, 1, 2, 1, '', 0.000000, 1),
	(10, 1, 3, 1, '', 0.000000, 1),
	(11, 1, 4, 1, '', 0.000000, 1),
	(12, 1, 5, 1, '', 0.000000, 1),
	(13, 1, 6, 1, '', 0.000000, 1);
/*!40000 ALTER TABLE `ps_product_supplier` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_product_tag
DROP TABLE IF EXISTS `ps_product_tag`;
CREATE TABLE IF NOT EXISTS `ps_product_tag` (
  `id_product` int(10) unsigned NOT NULL,
  `id_tag` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_product`,`id_tag`),
  KEY `id_tag` (`id_tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_product_tag: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_product_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_product_tag` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_profile
DROP TABLE IF EXISTS `ps_profile`;
CREATE TABLE IF NOT EXISTS `ps_profile` (
  `id_profile` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_profile`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_profile: ~4 rows (approximately)
/*!40000 ALTER TABLE `ps_profile` DISABLE KEYS */;
INSERT INTO `ps_profile` (`id_profile`) VALUES
	(1),
	(2),
	(3),
	(4);
/*!40000 ALTER TABLE `ps_profile` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_profile_lang
DROP TABLE IF EXISTS `ps_profile_lang`;
CREATE TABLE IF NOT EXISTS `ps_profile_lang` (
  `id_lang` int(10) unsigned NOT NULL,
  `id_profile` int(10) unsigned NOT NULL,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`id_profile`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_profile_lang: ~4 rows (approximately)
/*!40000 ALTER TABLE `ps_profile_lang` DISABLE KEYS */;
INSERT INTO `ps_profile_lang` (`id_lang`, `id_profile`, `name`) VALUES
	(1, 1, 'SuperAdmin'),
	(1, 2, 'Especialista en logística'),
	(1, 3, 'Traductor'),
	(1, 4, 'Vendedor');
/*!40000 ALTER TABLE `ps_profile_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_ptsbttestimonials
DROP TABLE IF EXISTS `ps_ptsbttestimonials`;
CREATE TABLE IF NOT EXISTS `ps_ptsbttestimonials` (
  `id_ptsbttestimonials` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `avatar` varchar(255) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `company` varchar(255) DEFAULT NULL,
  `address` varchar(500) NOT NULL,
  `media_id` varchar(20) DEFAULT NULL,
  `media_type` varchar(20) DEFAULT NULL,
  `media_code` varchar(100) DEFAULT NULL,
  `date_add` datetime DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_ptsbttestimonials`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_ptsbttestimonials: ~3 rows (approximately)
/*!40000 ALTER TABLE `ps_ptsbttestimonials` DISABLE KEYS */;
INSERT INTO `ps_ptsbttestimonials` (`id_ptsbttestimonials`, `avatar`, `name`, `email`, `company`, `address`, `media_id`, `media_type`, `media_code`, `date_add`, `position`, `active`) VALUES
	(1, 'sample-1.png', 'John doe 1', 'demo@gmail.com', 'Prestashop', 'Street name, California, USA', '9bdrk8FNWnc', 'Youtube', 'http://www.youtube.com/watch?v=9bdrk8FNWnc', '2015-05-08 14:03:27', 1, 1),
	(2, 'sample-2.png', 'John doe 2', 'demo@gmail.com', 'Prestashop', 'Street name, California, USA', '9bdrk8FNWnc', 'Youtube', 'http://www.youtube.com/watch?v=9bdrk8FNWnc', '2015-05-08 14:03:27', 2, 1),
	(3, 'sample-3.png', 'John doe 3', 'demo@gmail.com', 'Prestashop', 'Street name, California, USA', '9bdrk8FNWnc', 'Youtube', 'http://www.youtube.com/watch?v=9bdrk8FNWnc', '2015-05-08 14:03:27', 3, 1);
/*!40000 ALTER TABLE `ps_ptsbttestimonials` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_ptsbttestimonials_lang
DROP TABLE IF EXISTS `ps_ptsbttestimonials_lang`;
CREATE TABLE IF NOT EXISTS `ps_ptsbttestimonials_lang` (
  `id_ptsbttestimonials` int(10) unsigned NOT NULL,
  `id_lang` int(10) NOT NULL,
  `content` text NOT NULL,
  `note` text NOT NULL,
  PRIMARY KEY (`id_ptsbttestimonials`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_ptsbttestimonials_lang: ~3 rows (approximately)
/*!40000 ALTER TABLE `ps_ptsbttestimonials_lang` DISABLE KEYS */;
INSERT INTO `ps_ptsbttestimonials_lang` (`id_ptsbttestimonials`, `id_lang`, `content`, `note`) VALUES
	(1, 1, 'Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus  sit amet mauris. Morbi accumsan ipsum velit. 1', 'Duis sed odio 1'),
	(2, 1, 'Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus  sit amet mauris. Morbi accumsan ipsum velit. 2', 'Duis sed odio 2'),
	(3, 1, 'Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus  sit amet mauris. Morbi accumsan ipsum velit. 3', 'Duis sed odio 3');
/*!40000 ALTER TABLE `ps_ptsbttestimonials_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_ptsbttestimonials_shop
DROP TABLE IF EXISTS `ps_ptsbttestimonials_shop`;
CREATE TABLE IF NOT EXISTS `ps_ptsbttestimonials_shop` (
  `id_ptsbttestimonials` int(10) unsigned NOT NULL,
  `id_shop` int(10) NOT NULL,
  PRIMARY KEY (`id_ptsbttestimonials`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_ptsbttestimonials_shop: ~3 rows (approximately)
/*!40000 ALTER TABLE `ps_ptsbttestimonials_shop` DISABLE KEYS */;
INSERT INTO `ps_ptsbttestimonials_shop` (`id_ptsbttestimonials`, `id_shop`) VALUES
	(1, 1),
	(2, 1),
	(3, 1);
/*!40000 ALTER TABLE `ps_ptsbttestimonials_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_ptsmegamenu
DROP TABLE IF EXISTS `ps_ptsmegamenu`;
CREATE TABLE IF NOT EXISTS `ps_ptsmegamenu` (
  `id_ptsmegamenu` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `id_parent` int(11) NOT NULL,
  `is_group` tinyint(1) NOT NULL,
  `width` varchar(255) DEFAULT NULL,
  `submenu_width` varchar(255) DEFAULT NULL,
  `colum_width` varchar(255) DEFAULT NULL,
  `submenu_colum_width` varchar(255) DEFAULT NULL,
  `item` varchar(255) DEFAULT NULL,
  `colums` varchar(255) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `is_content` tinyint(1) NOT NULL,
  `show_title` tinyint(1) NOT NULL,
  `type_submenu` varchar(10) NOT NULL,
  `level_depth` smallint(6) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `position` int(11) NOT NULL,
  `submenu_content` text NOT NULL,
  `show_sub` tinyint(1) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `target` varchar(25) DEFAULT NULL,
  `privacy` smallint(6) DEFAULT NULL,
  `position_type` varchar(25) DEFAULT NULL,
  `menu_class` varchar(25) DEFAULT NULL,
  `content` text,
  `icon_class` varchar(255) DEFAULT NULL,
  `level` int(11) NOT NULL,
  `left` int(11) NOT NULL,
  `right` int(11) NOT NULL,
  `submenu_catids` text,
  `is_cattree` tinyint(1) DEFAULT '1',
  `date_add` datetime DEFAULT NULL,
  `date_upd` datetime DEFAULT NULL,
  PRIMARY KEY (`id_ptsmegamenu`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_ptsmegamenu: ~7 rows (approximately)
/*!40000 ALTER TABLE `ps_ptsmegamenu` DISABLE KEYS */;
INSERT INTO `ps_ptsmegamenu` (`id_ptsmegamenu`, `image`, `id_parent`, `is_group`, `width`, `submenu_width`, `colum_width`, `submenu_colum_width`, `item`, `colums`, `type`, `is_content`, `show_title`, `type_submenu`, `level_depth`, `active`, `position`, `submenu_content`, `show_sub`, `url`, `target`, `privacy`, `position_type`, `menu_class`, `content`, `icon_class`, `level`, `left`, `right`, `submenu_catids`, `is_cattree`, `date_add`, `date_upd`) VALUES
	(1, '', 0, 1, '', '', '', '', '', '1', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, '', '', '', '', 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(2, '', 2, 0, '', '', '', '', '', '1', 'url', 0, 1, 'menu', 2, 0, 0, '', 0, 'index.php', '_self', 0, '', '', '', '', 0, 0, 0, '', 1, '2014-03-20 21:54:18', '2015-05-08 14:46:26'),
	(4, '', 1, 0, '', '', '', '', '2', '1', 'category', 0, 1, 'menu', 1, 1, 3, '', 0, '', '_self', 0, '', '', '', '', 0, 0, 0, '', 1, '2014-03-23 22:06:22', '2015-05-08 14:59:42'),
	(5, '', 1, 0, '', '', '', '', '28', '1', 'url', 0, 1, 'menu', 1, 1, 1, '', 0, '', '_self', 0, '', '', '', '', 0, 0, 0, '', 1, '2014-03-23 22:07:34', '2015-05-08 14:53:00'),
	(7, '', 1, 0, '', '', '', '', '2', '1', 'category', 0, 1, 'menu', 1, 1, 4, '', 0, 'contact', '_self', 0, '', '', '', '', 0, 0, 0, '', 1, '2014-03-23 22:11:51', '2015-05-08 15:00:04'),
	(15, '', 1, 0, '', '', '', '', '2', '1', 'category', 0, 1, 'menu', 1, 1, 2, '', 0, '', '_self', 0, '', '', '', '', 0, 0, 0, '', 1, '2014-03-24 00:13:05', '2015-05-08 14:53:40'),
	(26, '', 1, 0, '', '', '', '', '', '1', 'url', 0, 1, 'menu', 1, 1, 0, '', 0, 'http://celiodev.com/index.php', '_self', 0, '', '', '', '', 0, 0, 0, '', 1, '2015-05-08 14:49:07', '2015-05-08 14:52:13');
/*!40000 ALTER TABLE `ps_ptsmegamenu` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_ptsmegamenu_lang
DROP TABLE IF EXISTS `ps_ptsmegamenu_lang`;
CREATE TABLE IF NOT EXISTS `ps_ptsmegamenu_lang` (
  `id_ptsmegamenu` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `description` text,
  `content_text` text,
  `submenu_content_text` text,
  PRIMARY KEY (`id_ptsmegamenu`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_ptsmegamenu_lang: ~7 rows (approximately)
/*!40000 ALTER TABLE `ps_ptsmegamenu_lang` DISABLE KEYS */;
INSERT INTO `ps_ptsmegamenu_lang` (`id_ptsmegamenu`, `id_lang`, `title`, `text`, `description`, `content_text`, `submenu_content_text`) VALUES
	(1, 1, '', '', '', '', ''),
	(2, 1, 'new arrivals', '', '', '', ''),
	(4, 1, 'jeans & pantalones', '', '', '', ''),
	(5, 1, 'promociones', '', '', '', ''),
	(7, 1, 'accesorios', '', '', '', ''),
	(15, 1, 'vestuario', '', '', '', ''),
	(26, 1, 'new arrivals', '', '', '', '');
/*!40000 ALTER TABLE `ps_ptsmegamenu_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_ptsmegamenu_shop
DROP TABLE IF EXISTS `ps_ptsmegamenu_shop`;
CREATE TABLE IF NOT EXISTS `ps_ptsmegamenu_shop` (
  `id_ptsmegamenu` int(11) NOT NULL DEFAULT '0',
  `id_shop` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_ptsmegamenu`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_ptsmegamenu_shop: ~6 rows (approximately)
/*!40000 ALTER TABLE `ps_ptsmegamenu_shop` DISABLE KEYS */;
INSERT INTO `ps_ptsmegamenu_shop` (`id_ptsmegamenu`, `id_shop`) VALUES
	(2, 1),
	(4, 1),
	(5, 1),
	(7, 1),
	(15, 1),
	(26, 1);
/*!40000 ALTER TABLE `ps_ptsmegamenu_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_ptsmegamenu_widgets
DROP TABLE IF EXISTS `ps_ptsmegamenu_widgets`;
CREATE TABLE IF NOT EXISTS `ps_ptsmegamenu_widgets` (
  `id_widget` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `type` varchar(255) NOT NULL,
  `params` text NOT NULL,
  `id_shop` int(11) NOT NULL,
  `key_widget` int(11) NOT NULL,
  PRIMARY KEY (`id_widget`,`id_shop`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_ptsmegamenu_widgets: ~6 rows (approximately)
/*!40000 ALTER TABLE `ps_ptsmegamenu_widgets` DISABLE KEYS */;
INSERT INTO `ps_ptsmegamenu_widgets` (`id_widget`, `name`, `type`, `params`, `id_shop`, `key_widget`) VALUES
	(25, 'Manufactures', 'manufacture', 'YToyNDp7czoxNToic2F2ZXB0c21lZ2FtZW51IjtzOjE6IjEiO3M6OToiaWRfd2lkZ2V0IjtzOjA6IiI7czoxMToid2lkZ2V0X25hbWUiO3M6MTI6Ik1hbnVmYWN0dXJlcyI7czoxNDoid2lkZ2V0X3RpdGxlXzEiO3M6MTI6Ik1hbnVmYWN0dXJlcyI7czoxNDoid2lkZ2V0X3RpdGxlXzIiO3M6MTI6Ik1hbnVmYWN0dXJlcyI7czoxNDoid2lkZ2V0X3RpdGxlXzMiO3M6MTI6Ik1hbnVmYWN0dXJlcyI7czoxNDoid2lkZ2V0X3RpdGxlXzQiO3M6MTI6Ik1hbnVmYWN0dXJlcyI7czoxNDoid2lkZ2V0X3RpdGxlXzUiO3M6MTI6Ik1hbnVmYWN0dXJlcyI7czoxNDoid2lkZ2V0X3RpdGxlXzYiO3M6MTI6Ik1hbnVmYWN0dXJlcyI7czoxNDoid2lkZ2V0X3RpdGxlXzciO3M6MTI6Ik1hbnVmYWN0dXJlcyI7czoxNDoid2lkZ2V0X3RpdGxlXzgiO3M6MTI6Ik1hbnVmYWN0dXJlcyI7czoxNDoid2lkZ2V0X3RpdGxlXzkiO3M6MTI6Ik1hbnVmYWN0dXJlcyI7czoxNDoiYWRkaXRpb25jbHNzXzEiO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfMiI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc18zIjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzQiO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfNSI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc182IjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzciO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfOCI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc185IjtzOjA6IiI7czoxMToid2lkZ2V0X3R5cGUiO3M6MTE6Im1hbnVmYWN0dXJlIjtzOjU6ImxpbWl0IjtzOjI6IjEwIjtzOjM6InRhYiI7czoxMjoiQWRtaW5Nb2R1bGVzIjt9', 1, 1411637567),
	(26, 'Video widget', 'video_code', 'YToyNjp7czoxNToic2F2ZXB0c21lZ2FtZW51IjtzOjE6IjEiO3M6OToiaWRfd2lkZ2V0IjtzOjA6IiI7czoxMToid2lkZ2V0X25hbWUiO3M6MTI6IlZpZGVvIHdpZGdldCI7czoxNDoid2lkZ2V0X3RpdGxlXzEiO3M6NToiVmlkZW8iO3M6MTQ6IndpZGdldF90aXRsZV8yIjtzOjU6IlZpZGVvIjtzOjE0OiJ3aWRnZXRfdGl0bGVfMyI7czo1OiJWaWRlbyI7czoxNDoid2lkZ2V0X3RpdGxlXzQiO3M6NToiVmlkZW8iO3M6MTQ6IndpZGdldF90aXRsZV81IjtzOjU6IlZpZGVvIjtzOjE0OiJ3aWRnZXRfdGl0bGVfNiI7czo1OiJWaWRlbyI7czoxNDoid2lkZ2V0X3RpdGxlXzciO3M6NToiVmlkZW8iO3M6MTQ6IndpZGdldF90aXRsZV84IjtzOjU6IlZpZGVvIjtzOjE0OiJ3aWRnZXRfdGl0bGVfOSI7czo1OiJWaWRlbyI7czoxNDoiYWRkaXRpb25jbHNzXzEiO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfMiI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc18zIjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzQiO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfNSI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc182IjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzciO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfOCI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc185IjtzOjA6IiI7czoxMToid2lkZ2V0X3R5cGUiO3M6MTA6InZpZGVvX2NvZGUiO3M6MTA6InZpZGVvX2xpbmsiO3M6NDI6Imh0dHA6Ly93d3cueW91dHViZS5jb20vd2F0Y2g/dj1selk0bGtUOEVsVSI7czo1OiJ3aWR0aCI7czo0OiIxMDAlIjtzOjY6ImhlaWdodCI7czo1OiIzMDBweCI7czozOiJ0YWIiO3M6MTI6IkFkbWluTW9kdWxlcyI7fQ==', 1, 1411637984),
	(27, 'Link HTML', 'links', 'YTo3NDp7czoxNToic2F2ZXB0c21lZ2FtZW51IjtzOjE6IjEiO3M6OToiaWRfd2lkZ2V0IjtzOjI6IjI3IjtzOjExOiJ3aWRnZXRfbmFtZSI7czo5OiJMaW5rIEhUTUwiO3M6MTQ6IndpZGdldF90aXRsZV8xIjtzOjk6IkxpbmsgSFRNTCI7czoxNDoid2lkZ2V0X3RpdGxlXzIiO3M6OToiTGluayBIVE1MIjtzOjE0OiJ3aWRnZXRfdGl0bGVfMyI7czo5OiJMaW5rIEhUTUwiO3M6MTQ6IndpZGdldF90aXRsZV80IjtzOjk6IkxpbmsgSFRNTCI7czoxNDoid2lkZ2V0X3RpdGxlXzUiO3M6OToiTGluayBIVE1MIjtzOjE0OiJhZGRpdGlvbmNsc3NfMSI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc18yIjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzMiO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfNCI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc181IjtzOjA6IiI7czoxMToid2lkZ2V0X3R5cGUiO3M6NToibGlua3MiO3M6MTM6InRleHRfbGlua18xXzEiO3M6MTU6Iklwc3VtIGRvbG9yIHNpdCI7czoxMzoidGV4dF9saW5rXzFfMiI7czoxNToiSXBzdW0gZG9sb3Igc2l0IjtzOjEzOiJ0ZXh0X2xpbmtfMV8zIjtzOjE1OiJJcHN1bSBkb2xvciBzaXQiO3M6MTM6InRleHRfbGlua18xXzQiO3M6MTU6Iklwc3VtIGRvbG9yIHNpdCI7czoxMzoidGV4dF9saW5rXzFfNSI7czoxNToiSXBzdW0gZG9sb3Igc2l0IjtzOjY6ImxpbmtfMSI7czo1NDoiaW5kZXgucGhwP2lkX2NhdGVnb3J5PTI4JmNvbnRyb2xsZXI9Y2F0ZWdvcnkmaWRfbGFuZz0xIjtzOjEzOiJ0ZXh0X2xpbmtfMl8xIjtzOjE2OiJQbGFjZXJhdCBkYXBpYnVzIjtzOjEzOiJ0ZXh0X2xpbmtfMl8yIjtzOjE2OiJQbGFjZXJhdCBkYXBpYnVzIjtzOjEzOiJ0ZXh0X2xpbmtfMl8zIjtzOjE2OiJQbGFjZXJhdCBkYXBpYnVzIjtzOjEzOiJ0ZXh0X2xpbmtfMl80IjtzOjE2OiJQbGFjZXJhdCBkYXBpYnVzIjtzOjEzOiJ0ZXh0X2xpbmtfMl81IjtzOjE2OiJQbGFjZXJhdCBkYXBpYnVzIjtzOjY6ImxpbmtfMiI7czo1NDoiaW5kZXgucGhwP2lkX2NhdGVnb3J5PTEyJmNvbnRyb2xsZXI9Y2F0ZWdvcnkmaWRfbGFuZz0xIjtzOjEzOiJ0ZXh0X2xpbmtfM18xIjtzOjE2OiJWaXRhZSB2ZXN0aWJ1bHVtIjtzOjEzOiJ0ZXh0X2xpbmtfM18yIjtzOjE2OiJWaXRhZSB2ZXN0aWJ1bHVtIjtzOjEzOiJ0ZXh0X2xpbmtfM18zIjtzOjE2OiJWaXRhZSB2ZXN0aWJ1bHVtIjtzOjEzOiJ0ZXh0X2xpbmtfM180IjtzOjE2OiJWaXRhZSB2ZXN0aWJ1bHVtIjtzOjEzOiJ0ZXh0X2xpbmtfM181IjtzOjE2OiJWaXRhZSB2ZXN0aWJ1bHVtIjtzOjY6ImxpbmtfMyI7czo1NDoiaW5kZXgucGhwP2lkX2NhdGVnb3J5PTEzJmNvbnRyb2xsZXI9Y2F0ZWdvcnkmaWRfbGFuZz0xIjtzOjEzOiJ0ZXh0X2xpbmtfNF8xIjtzOjE0OiJGdXNjZSBsb2JvcnRpcyI7czoxMzoidGV4dF9saW5rXzRfMiI7czoxNDoiRnVzY2UgbG9ib3J0aXMiO3M6MTM6InRleHRfbGlua180XzMiO3M6MTQ6IkZ1c2NlIGxvYm9ydGlzIjtzOjEzOiJ0ZXh0X2xpbmtfNF80IjtzOjE0OiJGdXNjZSBsb2JvcnRpcyI7czoxMzoidGV4dF9saW5rXzRfNSI7czoxNDoiRnVzY2UgbG9ib3J0aXMiO3M6NjoibGlua180IjtzOjU0OiJpbmRleC5waHA/aWRfY2F0ZWdvcnk9MTQmY29udHJvbGxlcj1jYXRlZ29yeSZpZF9sYW5nPTEiO3M6MTM6InRleHRfbGlua181XzEiO3M6MTQ6Ik1hdXJpcyB2aXZlcnJhIjtzOjEzOiJ0ZXh0X2xpbmtfNV8yIjtzOjE0OiJNYXVyaXMgdml2ZXJyYSI7czoxMzoidGV4dF9saW5rXzVfMyI7czoxNDoiTWF1cmlzIHZpdmVycmEiO3M6MTM6InRleHRfbGlua181XzQiO3M6MTQ6Ik1hdXJpcyB2aXZlcnJhIjtzOjEzOiJ0ZXh0X2xpbmtfNV81IjtzOjE0OiJNYXVyaXMgdml2ZXJyYSI7czo2OiJsaW5rXzUiO3M6NTQ6ImluZGV4LnBocD9pZF9jYXRlZ29yeT0xNSZjb250cm9sbGVyPWNhdGVnb3J5JmlkX2xhbmc9MSI7czoxMzoidGV4dF9saW5rXzZfMSI7czoxNjoiVml0YWUgdmVzdGlidWx1bSI7czoxMzoidGV4dF9saW5rXzZfMiI7czoxNjoiVml0YWUgdmVzdGlidWx1bSI7czoxMzoidGV4dF9saW5rXzZfMyI7czoxNjoiVml0YWUgdmVzdGlidWx1bSI7czoxMzoidGV4dF9saW5rXzZfNCI7czoxNjoiVml0YWUgdmVzdGlidWx1bSI7czoxMzoidGV4dF9saW5rXzZfNSI7czoxNjoiVml0YWUgdmVzdGlidWx1bSI7czo2OiJsaW5rXzYiO3M6NTQ6ImluZGV4LnBocD9pZF9jYXRlZ29yeT0xMyZjb250cm9sbGVyPWNhdGVnb3J5JmlkX2xhbmc9MSI7czoxMzoidGV4dF9saW5rXzdfMSI7czoxNDoiRnVzY2UgbG9ib3J0aXMiO3M6MTM6InRleHRfbGlua183XzIiO3M6MTQ6IkZ1c2NlIGxvYm9ydGlzIjtzOjEzOiJ0ZXh0X2xpbmtfN18zIjtzOjE0OiJGdXNjZSBsb2JvcnRpcyI7czoxMzoidGV4dF9saW5rXzdfNCI7czoxNDoiRnVzY2UgbG9ib3J0aXMiO3M6MTM6InRleHRfbGlua183XzUiO3M6MTQ6IkZ1c2NlIGxvYm9ydGlzIjtzOjY6ImxpbmtfNyI7czo1NDoiaW5kZXgucGhwP2lkX2NhdGVnb3J5PTE1JmNvbnRyb2xsZXI9Y2F0ZWdvcnkmaWRfbGFuZz0xIjtzOjEzOiJ0ZXh0X2xpbmtfOF8xIjtzOjA6IiI7czoxMzoidGV4dF9saW5rXzhfMiI7czowOiIiO3M6MTM6InRleHRfbGlua184XzMiO3M6MDoiIjtzOjEzOiJ0ZXh0X2xpbmtfOF80IjtzOjA6IiI7czoxMzoidGV4dF9saW5rXzhfNSI7czowOiIiO3M6NjoibGlua184IjtzOjA6IiI7czoxMzoidGV4dF9saW5rXzlfMSI7czowOiIiO3M6MTM6InRleHRfbGlua185XzIiO3M6MDoiIjtzOjEzOiJ0ZXh0X2xpbmtfOV8zIjtzOjA6IiI7czoxMzoidGV4dF9saW5rXzlfNCI7czowOiIiO3M6MTM6InRleHRfbGlua185XzUiO3M6MDoiIjtzOjY6ImxpbmtfOSI7czowOiIiO3M6MTQ6InRleHRfbGlua18xMF8xIjtzOjA6IiI7czoxNDoidGV4dF9saW5rXzEwXzIiO3M6MDoiIjtzOjE0OiJ0ZXh0X2xpbmtfMTBfMyI7czowOiIiO3M6MTQ6InRleHRfbGlua18xMF80IjtzOjA6IiI7czoxNDoidGV4dF9saW5rXzEwXzUiO3M6MDoiIjtzOjM6InRhYiI7czoxMjoiQWRtaW5Nb2R1bGVzIjt9', 1, 1411638230),
	(28, 'Custome HTML', 'html', 'YTozMjp7czoxNToic2F2ZXB0c21lZ2FtZW51IjtzOjE6IjEiO3M6OToiaWRfd2lkZ2V0IjtzOjA6IiI7czoxMToid2lkZ2V0X25hbWUiO3M6MTI6IkN1c3RvbWUgSFRNTCI7czoxNDoid2lkZ2V0X3RpdGxlXzEiO3M6MTI6IkN1c3RvbWUgSFRNTCI7czoxNDoid2lkZ2V0X3RpdGxlXzIiO3M6MTI6IkN1c3RvbWUgSFRNTCI7czoxNDoid2lkZ2V0X3RpdGxlXzMiO3M6MTI6IkN1c3RvbWUgSFRNTCI7czoxNDoid2lkZ2V0X3RpdGxlXzQiO3M6MTI6IkN1c3RvbWUgSFRNTCI7czoxNDoid2lkZ2V0X3RpdGxlXzUiO3M6MTI6IkN1c3RvbWUgSFRNTCI7czoxNDoid2lkZ2V0X3RpdGxlXzYiO3M6MTI6IkN1c3RvbWUgSFRNTCI7czoxNDoid2lkZ2V0X3RpdGxlXzciO3M6MTI6IkN1c3RvbWUgSFRNTCI7czoxNDoid2lkZ2V0X3RpdGxlXzgiO3M6MTI6IkN1c3RvbWUgSFRNTCI7czoxNDoid2lkZ2V0X3RpdGxlXzkiO3M6MTI6IkN1c3RvbWUgSFRNTCI7czoxNDoiYWRkaXRpb25jbHNzXzEiO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfMiI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc18zIjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzQiO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfNSI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc182IjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzciO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfOCI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc185IjtzOjA6IiI7czoxMToid2lkZ2V0X3R5cGUiO3M6NDoiaHRtbCI7czoxMzoiaHRtbGNvbnRlbnRfMSI7czozNTU6IjxwPkR1aXMgdGluY2lkdW50IGNvbmRpbWVudHVtIGZlbGlzLCBldCB0ZW1wb3IgbmVxdWUgcmhvbmN1cyBhYy4gUHJvaW4gZWxlbWVudHVtLCBmZWxpcyBpZCBwbGFjZXJhdCBkYXBpYnVzLCBwdXJ1cyBpcHN1bSBsb2JvcnRpcyB0ZWxsdXMsIHV0IHZlaGljdWxhIG5pc2wgbWV0dXMgZWdldCBhcmN1LiBGdXNjZSBsb2JvcnRpcyBwb3N1ZXJlIGVsaXQgZXUgZWxlbWVudHVtLiBDdXJhYml0dXIgdHJpc3RpcXVlIGxvcmVtIGFjIG5pc2wgZmV1Z2lhdCwgdml0YWUgdmVzdGlidWx1bSB2ZWxpdCB2dWxwdXRhdGUuIFByb2luIGV0IGxhb3JlZXQgc2FwaWVuLiBNYXVyaXMgdml2ZXJyYSBmZXVnaWF0IGNvbmRpbWVudHVtLjwvcD4iO3M6MTM6Imh0bWxjb250ZW50XzIiO3M6MzU1OiI8cD5EdWlzIHRpbmNpZHVudCBjb25kaW1lbnR1bSBmZWxpcywgZXQgdGVtcG9yIG5lcXVlIHJob25jdXMgYWMuIFByb2luIGVsZW1lbnR1bSwgZmVsaXMgaWQgcGxhY2VyYXQgZGFwaWJ1cywgcHVydXMgaXBzdW0gbG9ib3J0aXMgdGVsbHVzLCB1dCB2ZWhpY3VsYSBuaXNsIG1ldHVzIGVnZXQgYXJjdS4gRnVzY2UgbG9ib3J0aXMgcG9zdWVyZSBlbGl0IGV1IGVsZW1lbnR1bS4gQ3VyYWJpdHVyIHRyaXN0aXF1ZSBsb3JlbSBhYyBuaXNsIGZldWdpYXQsIHZpdGFlIHZlc3RpYnVsdW0gdmVsaXQgdnVscHV0YXRlLiBQcm9pbiBldCBsYW9yZWV0IHNhcGllbi4gTWF1cmlzIHZpdmVycmEgZmV1Z2lhdCBjb25kaW1lbnR1bS48L3A+IjtzOjEzOiJodG1sY29udGVudF8zIjtzOjM1NToiPHA+RHVpcyB0aW5jaWR1bnQgY29uZGltZW50dW0gZmVsaXMsIGV0IHRlbXBvciBuZXF1ZSByaG9uY3VzIGFjLiBQcm9pbiBlbGVtZW50dW0sIGZlbGlzIGlkIHBsYWNlcmF0IGRhcGlidXMsIHB1cnVzIGlwc3VtIGxvYm9ydGlzIHRlbGx1cywgdXQgdmVoaWN1bGEgbmlzbCBtZXR1cyBlZ2V0IGFyY3UuIEZ1c2NlIGxvYm9ydGlzIHBvc3VlcmUgZWxpdCBldSBlbGVtZW50dW0uIEN1cmFiaXR1ciB0cmlzdGlxdWUgbG9yZW0gYWMgbmlzbCBmZXVnaWF0LCB2aXRhZSB2ZXN0aWJ1bHVtIHZlbGl0IHZ1bHB1dGF0ZS4gUHJvaW4gZXQgbGFvcmVldCBzYXBpZW4uIE1hdXJpcyB2aXZlcnJhIGZldWdpYXQgY29uZGltZW50dW0uPC9wPiI7czoxMzoiaHRtbGNvbnRlbnRfNCI7czozNTU6IjxwPkR1aXMgdGluY2lkdW50IGNvbmRpbWVudHVtIGZlbGlzLCBldCB0ZW1wb3IgbmVxdWUgcmhvbmN1cyBhYy4gUHJvaW4gZWxlbWVudHVtLCBmZWxpcyBpZCBwbGFjZXJhdCBkYXBpYnVzLCBwdXJ1cyBpcHN1bSBsb2JvcnRpcyB0ZWxsdXMsIHV0IHZlaGljdWxhIG5pc2wgbWV0dXMgZWdldCBhcmN1LiBGdXNjZSBsb2JvcnRpcyBwb3N1ZXJlIGVsaXQgZXUgZWxlbWVudHVtLiBDdXJhYml0dXIgdHJpc3RpcXVlIGxvcmVtIGFjIG5pc2wgZmV1Z2lhdCwgdml0YWUgdmVzdGlidWx1bSB2ZWxpdCB2dWxwdXRhdGUuIFByb2luIGV0IGxhb3JlZXQgc2FwaWVuLiBNYXVyaXMgdml2ZXJyYSBmZXVnaWF0IGNvbmRpbWVudHVtLjwvcD4iO3M6MTM6Imh0bWxjb250ZW50XzUiO3M6MzU1OiI8cD5EdWlzIHRpbmNpZHVudCBjb25kaW1lbnR1bSBmZWxpcywgZXQgdGVtcG9yIG5lcXVlIHJob25jdXMgYWMuIFByb2luIGVsZW1lbnR1bSwgZmVsaXMgaWQgcGxhY2VyYXQgZGFwaWJ1cywgcHVydXMgaXBzdW0gbG9ib3J0aXMgdGVsbHVzLCB1dCB2ZWhpY3VsYSBuaXNsIG1ldHVzIGVnZXQgYXJjdS4gRnVzY2UgbG9ib3J0aXMgcG9zdWVyZSBlbGl0IGV1IGVsZW1lbnR1bS4gQ3VyYWJpdHVyIHRyaXN0aXF1ZSBsb3JlbSBhYyBuaXNsIGZldWdpYXQsIHZpdGFlIHZlc3RpYnVsdW0gdmVsaXQgdnVscHV0YXRlLiBQcm9pbiBldCBsYW9yZWV0IHNhcGllbi4gTWF1cmlzIHZpdmVycmEgZmV1Z2lhdCBjb25kaW1lbnR1bS48L3A+IjtzOjEzOiJodG1sY29udGVudF82IjtzOjM1NToiPHA+RHVpcyB0aW5jaWR1bnQgY29uZGltZW50dW0gZmVsaXMsIGV0IHRlbXBvciBuZXF1ZSByaG9uY3VzIGFjLiBQcm9pbiBlbGVtZW50dW0sIGZlbGlzIGlkIHBsYWNlcmF0IGRhcGlidXMsIHB1cnVzIGlwc3VtIGxvYm9ydGlzIHRlbGx1cywgdXQgdmVoaWN1bGEgbmlzbCBtZXR1cyBlZ2V0IGFyY3UuIEZ1c2NlIGxvYm9ydGlzIHBvc3VlcmUgZWxpdCBldSBlbGVtZW50dW0uIEN1cmFiaXR1ciB0cmlzdGlxdWUgbG9yZW0gYWMgbmlzbCBmZXVnaWF0LCB2aXRhZSB2ZXN0aWJ1bHVtIHZlbGl0IHZ1bHB1dGF0ZS4gUHJvaW4gZXQgbGFvcmVldCBzYXBpZW4uIE1hdXJpcyB2aXZlcnJhIGZldWdpYXQgY29uZGltZW50dW0uPC9wPiI7czoxMzoiaHRtbGNvbnRlbnRfNyI7czozNTU6IjxwPkR1aXMgdGluY2lkdW50IGNvbmRpbWVudHVtIGZlbGlzLCBldCB0ZW1wb3IgbmVxdWUgcmhvbmN1cyBhYy4gUHJvaW4gZWxlbWVudHVtLCBmZWxpcyBpZCBwbGFjZXJhdCBkYXBpYnVzLCBwdXJ1cyBpcHN1bSBsb2JvcnRpcyB0ZWxsdXMsIHV0IHZlaGljdWxhIG5pc2wgbWV0dXMgZWdldCBhcmN1LiBGdXNjZSBsb2JvcnRpcyBwb3N1ZXJlIGVsaXQgZXUgZWxlbWVudHVtLiBDdXJhYml0dXIgdHJpc3RpcXVlIGxvcmVtIGFjIG5pc2wgZmV1Z2lhdCwgdml0YWUgdmVzdGlidWx1bSB2ZWxpdCB2dWxwdXRhdGUuIFByb2luIGV0IGxhb3JlZXQgc2FwaWVuLiBNYXVyaXMgdml2ZXJyYSBmZXVnaWF0IGNvbmRpbWVudHVtLjwvcD4iO3M6MTM6Imh0bWxjb250ZW50XzgiO3M6MzU1OiI8cD5EdWlzIHRpbmNpZHVudCBjb25kaW1lbnR1bSBmZWxpcywgZXQgdGVtcG9yIG5lcXVlIHJob25jdXMgYWMuIFByb2luIGVsZW1lbnR1bSwgZmVsaXMgaWQgcGxhY2VyYXQgZGFwaWJ1cywgcHVydXMgaXBzdW0gbG9ib3J0aXMgdGVsbHVzLCB1dCB2ZWhpY3VsYSBuaXNsIG1ldHVzIGVnZXQgYXJjdS4gRnVzY2UgbG9ib3J0aXMgcG9zdWVyZSBlbGl0IGV1IGVsZW1lbnR1bS4gQ3VyYWJpdHVyIHRyaXN0aXF1ZSBsb3JlbSBhYyBuaXNsIGZldWdpYXQsIHZpdGFlIHZlc3RpYnVsdW0gdmVsaXQgdnVscHV0YXRlLiBQcm9pbiBldCBsYW9yZWV0IHNhcGllbi4gTWF1cmlzIHZpdmVycmEgZmV1Z2lhdCBjb25kaW1lbnR1bS48L3A+IjtzOjEzOiJodG1sY29udGVudF85IjtzOjM1NToiPHA+RHVpcyB0aW5jaWR1bnQgY29uZGltZW50dW0gZmVsaXMsIGV0IHRlbXBvciBuZXF1ZSByaG9uY3VzIGFjLiBQcm9pbiBlbGVtZW50dW0sIGZlbGlzIGlkIHBsYWNlcmF0IGRhcGlidXMsIHB1cnVzIGlwc3VtIGxvYm9ydGlzIHRlbGx1cywgdXQgdmVoaWN1bGEgbmlzbCBtZXR1cyBlZ2V0IGFyY3UuIEZ1c2NlIGxvYm9ydGlzIHBvc3VlcmUgZWxpdCBldSBlbGVtZW50dW0uIEN1cmFiaXR1ciB0cmlzdGlxdWUgbG9yZW0gYWMgbmlzbCBmZXVnaWF0LCB2aXRhZSB2ZXN0aWJ1bHVtIHZlbGl0IHZ1bHB1dGF0ZS4gUHJvaW4gZXQgbGFvcmVldCBzYXBpZW4uIE1hdXJpcyB2aXZlcnJhIGZldWdpYXQgY29uZGltZW50dW0uPC9wPiI7czozOiJ0YWIiO3M6MTI6IkFkbWluTW9kdWxlcyI7fQ==', 1, 1411639034),
	(29, 'Product New', 'product_list', 'YToyNTp7czoxNToic2F2ZXB0c21lZ2FtZW51IjtzOjE6IjEiO3M6OToiaWRfd2lkZ2V0IjtzOjA6IiI7czoxMToid2lkZ2V0X25hbWUiO3M6MTE6IlByb2R1Y3QgTmV3IjtzOjE0OiJ3aWRnZXRfdGl0bGVfMSI7czoxMToiUHJvZHVjdCBOZXciO3M6MTQ6IndpZGdldF90aXRsZV8yIjtzOjExOiJQcm9kdWN0IE5ldyI7czoxNDoid2lkZ2V0X3RpdGxlXzMiO3M6MTE6IlByb2R1Y3QgTmV3IjtzOjE0OiJ3aWRnZXRfdGl0bGVfNCI7czoxMToiUHJvZHVjdCBOZXciO3M6MTQ6IndpZGdldF90aXRsZV81IjtzOjExOiJQcm9kdWN0IE5ldyI7czoxNDoid2lkZ2V0X3RpdGxlXzYiO3M6MTE6IlByb2R1Y3QgTmV3IjtzOjE0OiJ3aWRnZXRfdGl0bGVfNyI7czoxMToiUHJvZHVjdCBOZXciO3M6MTQ6IndpZGdldF90aXRsZV84IjtzOjExOiJQcm9kdWN0IE5ldyI7czoxNDoid2lkZ2V0X3RpdGxlXzkiO3M6MTE6IlByb2R1Y3QgTmV3IjtzOjE0OiJhZGRpdGlvbmNsc3NfMSI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc18yIjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzMiO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfNCI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc181IjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzYiO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfNyI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc184IjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzkiO3M6MDoiIjtzOjExOiJ3aWRnZXRfdHlwZSI7czoxMjoicHJvZHVjdF9saXN0IjtzOjU6ImxpbWl0IjtzOjE6IjIiO3M6OToibGlzdF90eXBlIjtzOjY6Im5ld2VzdCI7czozOiJ0YWIiO3M6MTI6IkFkbWluTW9kdWxlcyI7fQ==', 1, 1411639385),
	(30, 'Products Special', 'product_list', 'YToyNTp7czoxNToic2F2ZXB0c21lZ2FtZW51IjtzOjE6IjEiO3M6OToiaWRfd2lkZ2V0IjtzOjA6IiI7czoxMToid2lkZ2V0X25hbWUiO3M6MTY6IlByb2R1Y3RzIFNwZWNpYWwiO3M6MTQ6IndpZGdldF90aXRsZV8xIjtzOjE2OiJQcm9kdWN0cyBTcGVjaWFsIjtzOjE0OiJ3aWRnZXRfdGl0bGVfMiI7czoxNjoiUHJvZHVjdHMgU3BlY2lhbCI7czoxNDoid2lkZ2V0X3RpdGxlXzMiO3M6MTY6IlByb2R1Y3RzIFNwZWNpYWwiO3M6MTQ6IndpZGdldF90aXRsZV80IjtzOjE2OiJQcm9kdWN0cyBTcGVjaWFsIjtzOjE0OiJ3aWRnZXRfdGl0bGVfNSI7czoxNjoiUHJvZHVjdHMgU3BlY2lhbCI7czoxNDoid2lkZ2V0X3RpdGxlXzYiO3M6MTY6IlByb2R1Y3RzIFNwZWNpYWwiO3M6MTQ6IndpZGdldF90aXRsZV83IjtzOjE2OiJQcm9kdWN0cyBTcGVjaWFsIjtzOjE0OiJ3aWRnZXRfdGl0bGVfOCI7czoxNjoiUHJvZHVjdHMgU3BlY2lhbCI7czoxNDoid2lkZ2V0X3RpdGxlXzkiO3M6MTY6IlByb2R1Y3RzIFNwZWNpYWwiO3M6MTQ6ImFkZGl0aW9uY2xzc18xIjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzIiO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfMyI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc180IjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzUiO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfNiI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc183IjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzgiO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfOSI7czowOiIiO3M6MTE6IndpZGdldF90eXBlIjtzOjEyOiJwcm9kdWN0X2xpc3QiO3M6NToibGltaXQiO3M6MToiMiI7czo5OiJsaXN0X3R5cGUiO3M6Nzoic3BlY2lhbCI7czozOiJ0YWIiO3M6MTI6IkFkbWluTW9kdWxlcyI7fQ==', 1, 1411639541),
	(31, 'sales', 'product_list', 'YTo5OntzOjE1OiJzYXZlcHRzbWVnYW1lbnUiO3M6MToiMSI7czo5OiJpZF93aWRnZXQiO3M6MDoiIjtzOjExOiJ3aWRnZXRfbmFtZSI7czo1OiJzYWxlcyI7czoxNDoid2lkZ2V0X3RpdGxlXzEiO3M6NToic2FsZXMiO3M6MTQ6ImFkZGl0aW9uY2xzc18xIjtzOjA6IiI7czoxMToid2lkZ2V0X3R5cGUiO3M6MTI6InByb2R1Y3RfbGlzdCI7czo1OiJsaW1pdCI7czoxOiI2IjtzOjk6Imxpc3RfdHlwZSI7czo2OiJuZXdlc3QiO3M6MzoidGFiIjtzOjEyOiJBZG1pbk1vZHVsZXMiO30=', 1, 1431111349);
/*!40000 ALTER TABLE `ps_ptsmegamenu_widgets` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_ptsreinsurance
DROP TABLE IF EXISTS `ps_ptsreinsurance`;
CREATE TABLE IF NOT EXISTS `ps_ptsreinsurance` (
  `id_ptsreinsurance` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `file_name` varchar(100) NOT NULL,
  `addition_class` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_ptsreinsurance`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_ptsreinsurance: ~3 rows (approximately)
/*!40000 ALTER TABLE `ps_ptsreinsurance` DISABLE KEYS */;
INSERT INTO `ps_ptsreinsurance` (`id_ptsreinsurance`, `id_shop`, `file_name`, `addition_class`) VALUES
	(1, 1, 'reinsurance-1-1.jpg', 'col-lg-4 col-md-4 col-sm-4 hidden-xs left-image'),
	(2, 1, 'reinsurance-2-1.jpg', 'col-lg-4 col-md-4 col-sm-4 hidden-xs'),
	(3, 1, 'reinsurance-3-1.jpg', 'col-lg-4 col-md-4 col-sm-4 hidden-xs');
/*!40000 ALTER TABLE `ps_ptsreinsurance` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_ptsreinsurance_lang
DROP TABLE IF EXISTS `ps_ptsreinsurance_lang`;
CREATE TABLE IF NOT EXISTS `ps_ptsreinsurance_lang` (
  `id_ptsreinsurance` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_lang` int(10) unsigned NOT NULL,
  `text` varchar(300) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_ptsreinsurance`,`id_lang`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_ptsreinsurance_lang: ~3 rows (approximately)
/*!40000 ALTER TABLE `ps_ptsreinsurance_lang` DISABLE KEYS */;
INSERT INTO `ps_ptsreinsurance_lang` (`id_ptsreinsurance`, `id_lang`, `text`, `title`) VALUES
	(1, 1, '<p>on order over $100.00</p>', 'Free shipping'),
	(2, 1, '<p>free 90 days return policy</p>', 'Free Return '),
	(3, 1, '<p>free register&nbsp;</p>', 'Member Discount');
/*!40000 ALTER TABLE `ps_ptsreinsurance_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_ptsstaticcontent
DROP TABLE IF EXISTS `ps_ptsstaticcontent`;
CREATE TABLE IF NOT EXISTS `ps_ptsstaticcontent` (
  `id_item` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `item_order` int(10) unsigned NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `title_use` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `hook` varchar(100) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `target` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `image` varchar(100) DEFAULT NULL,
  `image_w` varchar(10) DEFAULT NULL,
  `image_h` varchar(10) DEFAULT NULL,
  `html` text,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `col_lg` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `col_sm` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `class` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_item`)
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_ptsstaticcontent: ~12 rows (approximately)
/*!40000 ALTER TABLE `ps_ptsstaticcontent` DISABLE KEYS */;
INSERT INTO `ps_ptsstaticcontent` (`id_item`, `id_shop`, `id_lang`, `item_order`, `title`, `title_use`, `hook`, `url`, `target`, `image`, `image_w`, `image_h`, `html`, `active`, `col_lg`, `col_sm`, `class`) VALUES
	(3, 1, 1, 1, 'Banner Top1', 0, 'topcolumn', 'http://', 0, 'banner_top1.jpg', '0', '0', '', 1, 4, 0, 'banner-image'),
	(74, 1, 1, 2, 'Banner Top2', 0, 'topcolumn', 'http://', 0, 'banner_top2.jpg', '0', '0', '', 1, 4, 0, 'banner-image'),
	(75, 1, 1, 3, 'Banner Top3', 0, 'topcolumn', 'http://', 0, 'banner_top3.jpg', '0', '0', '', 1, 0, 0, 'banner-image'),
	(76, 1, 1, 1, 'Logo footer', 0, 'footertop', 'http://', 0, 'logo-footer.png', '0', '0', '<p>We&rsquo;re confident we&rsquo;ve provided all the best for you. Stay conneect with us</p>', 1, 12, 0, ''),
	(91, 1, 1, 1, 'Banner Left', 0, 'left', 'http://', 0, 'vestuario_2.png', '0', '0', '', 1, 0, 0, 'nopadding'),
	(92, 1, 1, 1, 'Banner Bottom', 0, 'bottom', 'http://', 0, 'banner-bottom.jpg', '0', '0', '', 1, 0, 0, ''),
	(93, 1, 1, 1, 'Custom Html footerbottom', 0, 'footerbottom', 'http://', 0, '', '0', '0', '<div class="custom info-services">\r\n<div class="info-title border small">call the customer services</div>\r\n<div class="text-highlight"><strong>1800 - 1570 - 000</strong></div>\r\n<div class="border small">Free 24/7 support for our customers</div>\r\n</div>', 1, 6, 0, ''),
	(94, 1, 1, 2, 'Custom Html footerbottom2', 0, 'footerbottom', 'http://', 0, '', '0', '0', '<div class="custom info-question">\r\n<div class="info-title border small">email us your question</div>\r\n<div class="text-warning"><strong>support@mixstore.com</strong></div>\r\n<div class="border small">We will respond within 3 business days</div>\r\n</div>', 1, 6, 0, ''),
	(95, 1, 1, 1, 'Banner header', 0, 'top', '#', 0, 'banner_hearder.jpg', '0', '0', '', 1, 6, 0, ''),
	(97, 1, 1, 2, 'HotLine', 0, 'top', 'http://', 0, '', '0', '0', '<div class="hotline pull-right">\r\n<p>Call the customer services</p>\r\n<p class="number">1800 - 1570 - 000</p>\r\n</div>', 1, 3, 0, 'inner'),
	(98, 1, 1, 1, 'Free Shipping', 0, 'home', 'http://', 0, '', '0', '0', '<h5>free shipping<small>on every order over $ 150</small></h5>', 1, 0, 0, 'free-shipping pull-right'),
	(143, 1, 1, 1, 'paypal', 0, 'footer', 'http://', 0, 'paypal.png', '0', '0', '', 1, 0, 0, 'text-center');
/*!40000 ALTER TABLE `ps_ptsstaticcontent` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_ptsverticalmenu
DROP TABLE IF EXISTS `ps_ptsverticalmenu`;
CREATE TABLE IF NOT EXISTS `ps_ptsverticalmenu` (
  `id_ptsverticalmenu` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `id_parent` int(11) NOT NULL,
  `is_group` tinyint(1) NOT NULL,
  `width` varchar(255) DEFAULT NULL,
  `submenu_width` varchar(255) DEFAULT NULL,
  `colum_width` varchar(255) DEFAULT NULL,
  `submenu_colum_width` varchar(255) DEFAULT NULL,
  `item` varchar(255) DEFAULT NULL,
  `colums` varchar(255) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `is_content` tinyint(1) NOT NULL,
  `show_title` tinyint(1) NOT NULL,
  `type_submenu` varchar(10) NOT NULL,
  `level_depth` smallint(6) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `position` int(11) NOT NULL,
  `submenu_content` text NOT NULL,
  `show_sub` tinyint(1) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `target` varchar(25) DEFAULT NULL,
  `privacy` smallint(6) DEFAULT NULL,
  `position_type` varchar(25) DEFAULT NULL,
  `menu_class` varchar(25) DEFAULT NULL,
  `content` text,
  `icon_class` varchar(255) DEFAULT NULL,
  `level` int(11) NOT NULL,
  `left` int(11) NOT NULL,
  `right` int(11) NOT NULL,
  `submenu_catids` text,
  `is_cattree` tinyint(1) DEFAULT '1',
  `date_add` datetime DEFAULT NULL,
  `date_upd` datetime DEFAULT NULL,
  PRIMARY KEY (`id_ptsverticalmenu`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_ptsverticalmenu: ~9 rows (approximately)
/*!40000 ALTER TABLE `ps_ptsverticalmenu` DISABLE KEYS */;
INSERT INTO `ps_ptsverticalmenu` (`id_ptsverticalmenu`, `image`, `id_parent`, `is_group`, `width`, `submenu_width`, `colum_width`, `submenu_colum_width`, `item`, `colums`, `type`, `is_content`, `show_title`, `type_submenu`, `level_depth`, `active`, `position`, `submenu_content`, `show_sub`, `url`, `target`, `privacy`, `position_type`, `menu_class`, `content`, `icon_class`, `level`, `left`, `right`, `submenu_catids`, `is_cattree`, `date_add`, `date_upd`) VALUES
	(1, '', 0, 0, '', '', '', '', '2', '1', 'category', 0, 1, 'menu', 1, 1, 0, '', 0, '', '_self', 0, '', '', '', '', 0, 0, 0, '', 1, '2014-05-18 22:38:09', '2014-05-18 22:38:09'),
	(2, 'book.png', 1, 0, '', '', '', '', '', '1', 'url', 0, 1, 'menu', 2, 1, 0, '', 0, '', '_self', 0, '', '', '', '', 0, 0, 0, '', 1, '2014-06-25 05:14:46', '2014-06-25 05:14:47'),
	(3, 'game.png', 1, 0, '', '', '', '', '', '1', 'url', 0, 1, 'menu', 2, 1, 1, '', 0, '', '_self', 0, '', '', '', '', 0, 0, 0, '', 1, '2014-06-25 05:15:47', '2014-06-25 05:15:47'),
	(4, 'clothing.png', 1, 0, '', '', '', '', '', '1', 'url', 0, 1, 'menu', 2, 1, 2, '', 0, '', '_self', 0, '', '', '', '', 0, 0, 0, '', 1, '2014-06-25 05:16:29', '2014-06-25 05:16:30'),
	(5, 'beauty.png', 1, 0, '', '', '', '', '', '1', 'url', 0, 1, 'menu', 2, 1, 3, '', 0, '', '_self', 0, '', '', '', '', 0, 0, 0, '', 1, '2014-06-25 05:17:08', '2014-06-25 05:17:08'),
	(6, 'mobile.png', 1, 0, '', '', '', '', '', '1', 'url', 0, 1, 'menu', 2, 1, 4, '', 0, '', '_self', 0, '', '', '', '', 0, 0, 0, '', 1, '2014-06-25 05:19:56', '2014-06-25 05:22:03'),
	(7, 'sport.png', 1, 0, '', '', '', '', '', '1', 'url', 0, 1, 'menu', 2, 1, 5, '', 0, '', '_self', 0, '', '', '', '', 0, 0, 0, '', 1, '2014-06-25 05:29:38', '2014-06-25 05:29:39'),
	(8, 'beauty.png', 1, 0, '', '', '', '', '', '1', 'url', 0, 1, 'menu', 2, 1, 6, '', 0, '', '_self', 0, '', '', '', '', 0, 0, 0, '', 1, '2014-06-25 05:30:24', '2014-06-25 05:30:25'),
	(9, 'mobile.png', 1, 0, '', '', '', '', '', '1', 'url', 0, 1, 'menu', 2, 1, 7, '', 0, '', '_self', 0, '', '', '', '', 0, 0, 0, '', 1, '2014-06-25 05:31:10', '2014-06-25 05:31:10');
/*!40000 ALTER TABLE `ps_ptsverticalmenu` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_ptsverticalmenu_lang
DROP TABLE IF EXISTS `ps_ptsverticalmenu_lang`;
CREATE TABLE IF NOT EXISTS `ps_ptsverticalmenu_lang` (
  `id_ptsverticalmenu` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `description` text,
  `content_text` text,
  `submenu_content_text` text,
  PRIMARY KEY (`id_ptsverticalmenu`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_ptsverticalmenu_lang: ~9 rows (approximately)
/*!40000 ALTER TABLE `ps_ptsverticalmenu_lang` DISABLE KEYS */;
INSERT INTO `ps_ptsverticalmenu_lang` (`id_ptsverticalmenu`, `id_lang`, `title`, `text`, `description`, `content_text`, `submenu_content_text`) VALUES
	(1, 1, 'Home', '', '', '', ''),
	(2, 1, 'Books & Audible', '', '', '', ''),
	(3, 1, 'Movies, Music & Games', '', '', '', ''),
	(4, 1, 'Beauty, Health & Grocery', '', '', '', ''),
	(5, 1, 'Toys, Kids & Baby', '', '', '', ''),
	(6, 1, 'Clothing', '', '', '', ''),
	(7, 1, 'Sports & Outdoors', '', '', '', ''),
	(8, 1, 'Automotive & Industrial', '', '', '', ''),
	(9, 1, 'Appstore', '', '', '', '');
/*!40000 ALTER TABLE `ps_ptsverticalmenu_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_ptsverticalmenu_shop
DROP TABLE IF EXISTS `ps_ptsverticalmenu_shop`;
CREATE TABLE IF NOT EXISTS `ps_ptsverticalmenu_shop` (
  `id_ptsverticalmenu` int(11) NOT NULL DEFAULT '0',
  `id_shop` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_ptsverticalmenu`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_ptsverticalmenu_shop: ~9 rows (approximately)
/*!40000 ALTER TABLE `ps_ptsverticalmenu_shop` DISABLE KEYS */;
INSERT INTO `ps_ptsverticalmenu_shop` (`id_ptsverticalmenu`, `id_shop`) VALUES
	(1, 1),
	(2, 1),
	(3, 1),
	(4, 1),
	(5, 1),
	(6, 1),
	(7, 1),
	(8, 1),
	(9, 1);
/*!40000 ALTER TABLE `ps_ptsverticalmenu_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_ptsverticalmenu_widgets
DROP TABLE IF EXISTS `ps_ptsverticalmenu_widgets`;
CREATE TABLE IF NOT EXISTS `ps_ptsverticalmenu_widgets` (
  `id_widget` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `type` varchar(255) NOT NULL,
  `params` text NOT NULL,
  `id_shop` int(11) NOT NULL,
  `key_widget` int(11) NOT NULL,
  PRIMARY KEY (`id_widget`,`id_shop`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_ptsverticalmenu_widgets: ~8 rows (approximately)
/*!40000 ALTER TABLE `ps_ptsverticalmenu_widgets` DISABLE KEYS */;
INSERT INTO `ps_ptsverticalmenu_widgets` (`id_widget`, `name`, `type`, `params`, `id_shop`, `key_widget`) VALUES
	(11, 'Products Special', 'product_list', 'YToyNTp7czoxOToic2F2ZXB0c3ZlcnRpY2FsbWVudSI7czoxOiIxIjtzOjk6ImlkX3dpZGdldCI7czoyOiIxMSI7czoxMToid2lkZ2V0X25hbWUiO3M6MTY6IlByb2R1Y3RzIFNwZWNpYWwiO3M6MTQ6IndpZGdldF90aXRsZV8xIjtzOjE2OiJQcm9kdWN0cyBTcGVjaWFsIjtzOjE0OiJ3aWRnZXRfdGl0bGVfMiI7czoxNjoiUHJvZHVjdHMgU3BlY2lhbCI7czoxNDoid2lkZ2V0X3RpdGxlXzMiO3M6MTY6IlByb2R1Y3RzIFNwZWNpYWwiO3M6MTQ6IndpZGdldF90aXRsZV80IjtzOjE2OiJQcm9kdWN0cyBTcGVjaWFsIjtzOjE0OiJ3aWRnZXRfdGl0bGVfNSI7czoxNjoiUHJvZHVjdHMgU3BlY2lhbCI7czoxNDoid2lkZ2V0X3RpdGxlXzYiO3M6MTY6IlByb2R1Y3RzIFNwZWNpYWwiO3M6MTQ6IndpZGdldF90aXRsZV83IjtzOjE2OiJQcm9kdWN0cyBTcGVjaWFsIjtzOjE0OiJ3aWRnZXRfdGl0bGVfOCI7czoxNjoiUHJvZHVjdHMgU3BlY2lhbCI7czoxNDoid2lkZ2V0X3RpdGxlXzkiO3M6MTY6IlByb2R1Y3RzIFNwZWNpYWwiO3M6MTQ6ImFkZGl0aW9uY2xzc18xIjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzIiO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfMyI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc180IjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzUiO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfNiI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc183IjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzgiO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfOSI7czowOiIiO3M6MTE6IndpZGdldF90eXBlIjtzOjEyOiJwcm9kdWN0X2xpc3QiO3M6NToibGltaXQiO3M6MToiMiI7czo5OiJsaXN0X3R5cGUiO3M6Nzoic3BlY2lhbCI7czozOiJ0YWIiO3M6MTI6IkFkbWluTW9kdWxlcyI7fQ==', 1, 1411640411),
	(12, 'Product Featured', 'product_list', 'YToyNTp7czoxOToic2F2ZXB0c3ZlcnRpY2FsbWVudSI7czoxOiIxIjtzOjk6ImlkX3dpZGdldCI7czowOiIiO3M6MTE6IndpZGdldF9uYW1lIjtzOjE2OiJQcm9kdWN0IEZlYXR1cmVkIjtzOjE0OiJ3aWRnZXRfdGl0bGVfMSI7czoxNjoiUHJvZHVjdCBGZWF0dXJlZCI7czoxNDoid2lkZ2V0X3RpdGxlXzIiO3M6MTY6IlByb2R1Y3QgRmVhdHVyZWQiO3M6MTQ6IndpZGdldF90aXRsZV8zIjtzOjE2OiJQcm9kdWN0IEZlYXR1cmVkIjtzOjE0OiJ3aWRnZXRfdGl0bGVfNCI7czoxNjoiUHJvZHVjdCBGZWF0dXJlZCI7czoxNDoid2lkZ2V0X3RpdGxlXzUiO3M6MTY6IlByb2R1Y3QgRmVhdHVyZWQiO3M6MTQ6IndpZGdldF90aXRsZV82IjtzOjE2OiJQcm9kdWN0IEZlYXR1cmVkIjtzOjE0OiJ3aWRnZXRfdGl0bGVfNyI7czoxNjoiUHJvZHVjdCBGZWF0dXJlZCI7czoxNDoid2lkZ2V0X3RpdGxlXzgiO3M6MTY6IlByb2R1Y3QgRmVhdHVyZWQiO3M6MTQ6IndpZGdldF90aXRsZV85IjtzOjE2OiJQcm9kdWN0IEZlYXR1cmVkIjtzOjE0OiJhZGRpdGlvbmNsc3NfMSI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc18yIjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzMiO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfNCI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc181IjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzYiO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfNyI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc184IjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzkiO3M6MDoiIjtzOjExOiJ3aWRnZXRfdHlwZSI7czoxMjoicHJvZHVjdF9saXN0IjtzOjU6ImxpbWl0IjtzOjE6IjIiO3M6OToibGlzdF90eXBlIjtzOjg6ImZlYXR1cmVkIjtzOjM6InRhYiI7czoxMjoiQWRtaW5Nb2R1bGVzIjt9', 1, 1411640508),
	(13, 'Products Bestseller', 'product_list', 'YToyNTp7czoxOToic2F2ZXB0c3ZlcnRpY2FsbWVudSI7czoxOiIxIjtzOjk6ImlkX3dpZGdldCI7czowOiIiO3M6MTE6IndpZGdldF9uYW1lIjtzOjE5OiJQcm9kdWN0cyBCZXN0c2VsbGVyIjtzOjE0OiJ3aWRnZXRfdGl0bGVfMSI7czoxOToiUHJvZHVjdHMgQmVzdHNlbGxlciI7czoxNDoid2lkZ2V0X3RpdGxlXzIiO3M6MTk6IlByb2R1Y3RzIEJlc3RzZWxsZXIiO3M6MTQ6IndpZGdldF90aXRsZV8zIjtzOjE5OiJQcm9kdWN0cyBCZXN0c2VsbGVyIjtzOjE0OiJ3aWRnZXRfdGl0bGVfNCI7czoxOToiUHJvZHVjdHMgQmVzdHNlbGxlciI7czoxNDoid2lkZ2V0X3RpdGxlXzUiO3M6MTk6IlByb2R1Y3RzIEJlc3RzZWxsZXIiO3M6MTQ6IndpZGdldF90aXRsZV82IjtzOjE5OiJQcm9kdWN0cyBCZXN0c2VsbGVyIjtzOjE0OiJ3aWRnZXRfdGl0bGVfNyI7czoxOToiUHJvZHVjdHMgQmVzdHNlbGxlciI7czoxNDoid2lkZ2V0X3RpdGxlXzgiO3M6MTk6IlByb2R1Y3RzIEJlc3RzZWxsZXIiO3M6MTQ6IndpZGdldF90aXRsZV85IjtzOjE5OiJQcm9kdWN0cyBCZXN0c2VsbGVyIjtzOjE0OiJhZGRpdGlvbmNsc3NfMSI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc18yIjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzMiO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfNCI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc181IjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzYiO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfNyI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc184IjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzkiO3M6MDoiIjtzOjExOiJ3aWRnZXRfdHlwZSI7czoxMjoicHJvZHVjdF9saXN0IjtzOjU6ImxpbWl0IjtzOjE6IjMiO3M6OToibGlzdF90eXBlIjtzOjEwOiJiZXN0c2VsbGVyIjtzOjM6InRhYiI7czoxMjoiQWRtaW5Nb2R1bGVzIjt9', 1, 1411640595),
	(14, 'Product New', 'product_list', 'YToyNTp7czoxOToic2F2ZXB0c3ZlcnRpY2FsbWVudSI7czoxOiIxIjtzOjk6ImlkX3dpZGdldCI7czowOiIiO3M6MTE6IndpZGdldF9uYW1lIjtzOjExOiJQcm9kdWN0IE5ldyI7czoxNDoid2lkZ2V0X3RpdGxlXzEiO3M6MTE6IlByb2R1Y3QgTmV3IjtzOjE0OiJ3aWRnZXRfdGl0bGVfMiI7czoxMToiUHJvZHVjdCBOZXciO3M6MTQ6IndpZGdldF90aXRsZV8zIjtzOjExOiJQcm9kdWN0IE5ldyI7czoxNDoid2lkZ2V0X3RpdGxlXzQiO3M6MTE6IlByb2R1Y3QgTmV3IjtzOjE0OiJ3aWRnZXRfdGl0bGVfNSI7czoxMToiUHJvZHVjdCBOZXciO3M6MTQ6IndpZGdldF90aXRsZV82IjtzOjExOiJQcm9kdWN0IE5ldyI7czoxNDoid2lkZ2V0X3RpdGxlXzciO3M6MTE6IlByb2R1Y3QgTmV3IjtzOjE0OiJ3aWRnZXRfdGl0bGVfOCI7czoxMToiUHJvZHVjdCBOZXciO3M6MTQ6IndpZGdldF90aXRsZV85IjtzOjExOiJQcm9kdWN0IE5ldyI7czoxNDoiYWRkaXRpb25jbHNzXzEiO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfMiI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc18zIjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzQiO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfNSI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc182IjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzciO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfOCI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc185IjtzOjA6IiI7czoxMToid2lkZ2V0X3R5cGUiO3M6MTI6InByb2R1Y3RfbGlzdCI7czo1OiJsaW1pdCI7czoxOiIzIjtzOjk6Imxpc3RfdHlwZSI7czo2OiJuZXdlc3QiO3M6MzoidGFiIjtzOjEyOiJBZG1pbk1vZHVsZXMiO30=', 1, 1411640692),
	(15, 'Link HTML', 'links', 'YToxMjI6e3M6MTk6InNhdmVwdHN2ZXJ0aWNhbG1lbnUiO3M6MToiMSI7czo5OiJpZF93aWRnZXQiO3M6MDoiIjtzOjExOiJ3aWRnZXRfbmFtZSI7czo5OiJMaW5rIEhUTUwiO3M6MTQ6IndpZGdldF90aXRsZV8xIjtzOjk6IkxpbmsgSFRNTCI7czoxNDoid2lkZ2V0X3RpdGxlXzIiO3M6OToiTGluayBIVE1MIjtzOjE0OiJ3aWRnZXRfdGl0bGVfMyI7czo5OiJMaW5rIEhUTUwiO3M6MTQ6IndpZGdldF90aXRsZV80IjtzOjk6IkxpbmsgSFRNTCI7czoxNDoid2lkZ2V0X3RpdGxlXzUiO3M6OToiTGluayBIVE1MIjtzOjE0OiJ3aWRnZXRfdGl0bGVfNiI7czo5OiJMaW5rIEhUTUwiO3M6MTQ6IndpZGdldF90aXRsZV83IjtzOjk6IkxpbmsgSFRNTCI7czoxNDoid2lkZ2V0X3RpdGxlXzgiO3M6OToiTGluayBIVE1MIjtzOjE0OiJ3aWRnZXRfdGl0bGVfOSI7czo5OiJMaW5rIEhUTUwiO3M6MTQ6ImFkZGl0aW9uY2xzc18xIjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzIiO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfMyI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc180IjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzUiO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfNiI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc183IjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzgiO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfOSI7czowOiIiO3M6MTE6IndpZGdldF90eXBlIjtzOjU6ImxpbmtzIjtzOjEzOiJ0ZXh0X2xpbmtfMV8xIjtzOjE1OiJJcHN1bSBkb2xvciBzaXQiO3M6MTM6InRleHRfbGlua18xXzIiO3M6MTU6Iklwc3VtIGRvbG9yIHNpdCI7czoxMzoidGV4dF9saW5rXzFfMyI7czoxNToiSXBzdW0gZG9sb3Igc2l0IjtzOjEzOiJ0ZXh0X2xpbmtfMV80IjtzOjE1OiJJcHN1bSBkb2xvciBzaXQiO3M6MTM6InRleHRfbGlua18xXzUiO3M6MTU6Iklwc3VtIGRvbG9yIHNpdCI7czoxMzoidGV4dF9saW5rXzFfNiI7czoxNToiSXBzdW0gZG9sb3Igc2l0IjtzOjEzOiJ0ZXh0X2xpbmtfMV83IjtzOjE1OiJJcHN1bSBkb2xvciBzaXQiO3M6MTM6InRleHRfbGlua18xXzgiO3M6MTU6Iklwc3VtIGRvbG9yIHNpdCI7czoxMzoidGV4dF9saW5rXzFfOSI7czoxNToiSXBzdW0gZG9sb3Igc2l0IjtzOjY6ImxpbmtfMSI7czoxOiIjIjtzOjEzOiJ0ZXh0X2xpbmtfMl8xIjtzOjE2OiJQbGFjZXJhdCBkYXBpYnVzIjtzOjEzOiJ0ZXh0X2xpbmtfMl8yIjtzOjE2OiJQbGFjZXJhdCBkYXBpYnVzIjtzOjEzOiJ0ZXh0X2xpbmtfMl8zIjtzOjE2OiJQbGFjZXJhdCBkYXBpYnVzIjtzOjEzOiJ0ZXh0X2xpbmtfMl80IjtzOjE2OiJQbGFjZXJhdCBkYXBpYnVzIjtzOjEzOiJ0ZXh0X2xpbmtfMl81IjtzOjE2OiJQbGFjZXJhdCBkYXBpYnVzIjtzOjEzOiJ0ZXh0X2xpbmtfMl82IjtzOjE2OiJQbGFjZXJhdCBkYXBpYnVzIjtzOjEzOiJ0ZXh0X2xpbmtfMl83IjtzOjE2OiJQbGFjZXJhdCBkYXBpYnVzIjtzOjEzOiJ0ZXh0X2xpbmtfMl84IjtzOjE2OiJQbGFjZXJhdCBkYXBpYnVzIjtzOjEzOiJ0ZXh0X2xpbmtfMl85IjtzOjE2OiJQbGFjZXJhdCBkYXBpYnVzIjtzOjY6ImxpbmtfMiI7czoxOiIjIjtzOjEzOiJ0ZXh0X2xpbmtfM18xIjtzOjE2OiJWaXRhZSB2ZXN0aWJ1bHVtIjtzOjEzOiJ0ZXh0X2xpbmtfM18yIjtzOjE2OiJWaXRhZSB2ZXN0aWJ1bHVtIjtzOjEzOiJ0ZXh0X2xpbmtfM18zIjtzOjE2OiJWaXRhZSB2ZXN0aWJ1bHVtIjtzOjEzOiJ0ZXh0X2xpbmtfM180IjtzOjE2OiJWaXRhZSB2ZXN0aWJ1bHVtIjtzOjEzOiJ0ZXh0X2xpbmtfM181IjtzOjE2OiJWaXRhZSB2ZXN0aWJ1bHVtIjtzOjEzOiJ0ZXh0X2xpbmtfM182IjtzOjE2OiJWaXRhZSB2ZXN0aWJ1bHVtIjtzOjEzOiJ0ZXh0X2xpbmtfM183IjtzOjE2OiJWaXRhZSB2ZXN0aWJ1bHVtIjtzOjEzOiJ0ZXh0X2xpbmtfM184IjtzOjE2OiJWaXRhZSB2ZXN0aWJ1bHVtIjtzOjEzOiJ0ZXh0X2xpbmtfM185IjtzOjE2OiJWaXRhZSB2ZXN0aWJ1bHVtIjtzOjY6ImxpbmtfMyI7czoxOiIjIjtzOjEzOiJ0ZXh0X2xpbmtfNF8xIjtzOjE0OiJGdXNjZSBsb2JvcnRpcyI7czoxMzoidGV4dF9saW5rXzRfMiI7czoxNDoiRnVzY2UgbG9ib3J0aXMiO3M6MTM6InRleHRfbGlua180XzMiO3M6MTQ6IkZ1c2NlIGxvYm9ydGlzIjtzOjEzOiJ0ZXh0X2xpbmtfNF80IjtzOjE0OiJGdXNjZSBsb2JvcnRpcyI7czoxMzoidGV4dF9saW5rXzRfNSI7czoxNDoiRnVzY2UgbG9ib3J0aXMiO3M6MTM6InRleHRfbGlua180XzYiO3M6MTQ6IkZ1c2NlIGxvYm9ydGlzIjtzOjEzOiJ0ZXh0X2xpbmtfNF83IjtzOjE0OiJGdXNjZSBsb2JvcnRpcyI7czoxMzoidGV4dF9saW5rXzRfOCI7czoxNDoiRnVzY2UgbG9ib3J0aXMiO3M6MTM6InRleHRfbGlua180XzkiO3M6MTQ6IkZ1c2NlIGxvYm9ydGlzIjtzOjY6ImxpbmtfNCI7czoxOiIjIjtzOjEzOiJ0ZXh0X2xpbmtfNV8xIjtzOjE0OiJNYXVyaXMgdml2ZXJyYSI7czoxMzoidGV4dF9saW5rXzVfMiI7czoxNDoiTWF1cmlzIHZpdmVycmEiO3M6MTM6InRleHRfbGlua181XzMiO3M6MTQ6Ik1hdXJpcyB2aXZlcnJhIjtzOjEzOiJ0ZXh0X2xpbmtfNV80IjtzOjE0OiJNYXVyaXMgdml2ZXJyYSI7czoxMzoidGV4dF9saW5rXzVfNSI7czoxNDoiTWF1cmlzIHZpdmVycmEiO3M6MTM6InRleHRfbGlua181XzYiO3M6MTQ6Ik1hdXJpcyB2aXZlcnJhIjtzOjEzOiJ0ZXh0X2xpbmtfNV83IjtzOjE0OiJNYXVyaXMgdml2ZXJyYSI7czoxMzoidGV4dF9saW5rXzVfOCI7czoxNDoiTWF1cmlzIHZpdmVycmEiO3M6MTM6InRleHRfbGlua181XzkiO3M6MTQ6Ik1hdXJpcyB2aXZlcnJhIjtzOjY6ImxpbmtfNSI7czoxOiIjIjtzOjEzOiJ0ZXh0X2xpbmtfNl8xIjtzOjA6IiI7czoxMzoidGV4dF9saW5rXzZfMiI7czowOiIiO3M6MTM6InRleHRfbGlua182XzMiO3M6MDoiIjtzOjEzOiJ0ZXh0X2xpbmtfNl80IjtzOjA6IiI7czoxMzoidGV4dF9saW5rXzZfNSI7czowOiIiO3M6MTM6InRleHRfbGlua182XzYiO3M6MDoiIjtzOjEzOiJ0ZXh0X2xpbmtfNl83IjtzOjA6IiI7czoxMzoidGV4dF9saW5rXzZfOCI7czowOiIiO3M6MTM6InRleHRfbGlua182XzkiO3M6MDoiIjtzOjY6ImxpbmtfNiI7czowOiIiO3M6MTM6InRleHRfbGlua183XzEiO3M6MDoiIjtzOjEzOiJ0ZXh0X2xpbmtfN18yIjtzOjA6IiI7czoxMzoidGV4dF9saW5rXzdfMyI7czowOiIiO3M6MTM6InRleHRfbGlua183XzQiO3M6MDoiIjtzOjEzOiJ0ZXh0X2xpbmtfN181IjtzOjA6IiI7czoxMzoidGV4dF9saW5rXzdfNiI7czowOiIiO3M6MTM6InRleHRfbGlua183XzciO3M6MDoiIjtzOjEzOiJ0ZXh0X2xpbmtfN184IjtzOjA6IiI7czoxMzoidGV4dF9saW5rXzdfOSI7czowOiIiO3M6NjoibGlua183IjtzOjA6IiI7czoxMzoidGV4dF9saW5rXzhfMSI7czowOiIiO3M6MTM6InRleHRfbGlua184XzIiO3M6MDoiIjtzOjEzOiJ0ZXh0X2xpbmtfOF8zIjtzOjA6IiI7czoxMzoidGV4dF9saW5rXzhfNCI7czowOiIiO3M6MTM6InRleHRfbGlua184XzUiO3M6MDoiIjtzOjEzOiJ0ZXh0X2xpbmtfOF82IjtzOjA6IiI7czoxMzoidGV4dF9saW5rXzhfNyI7czowOiIiO3M6MTM6InRleHRfbGlua184XzgiO3M6MDoiIjtzOjEzOiJ0ZXh0X2xpbmtfOF85IjtzOjA6IiI7czo2OiJsaW5rXzgiO3M6MDoiIjtzOjEzOiJ0ZXh0X2xpbmtfOV8xIjtzOjA6IiI7czoxMzoidGV4dF9saW5rXzlfMiI7czowOiIiO3M6MTM6InRleHRfbGlua185XzMiO3M6MDoiIjtzOjEzOiJ0ZXh0X2xpbmtfOV80IjtzOjA6IiI7czoxMzoidGV4dF9saW5rXzlfNSI7czowOiIiO3M6MTM6InRleHRfbGlua185XzYiO3M6MDoiIjtzOjEzOiJ0ZXh0X2xpbmtfOV83IjtzOjA6IiI7czoxMzoidGV4dF9saW5rXzlfOCI7czowOiIiO3M6MTM6InRleHRfbGlua185XzkiO3M6MDoiIjtzOjY6ImxpbmtfOSI7czowOiIiO3M6MTQ6InRleHRfbGlua18xMF8xIjtzOjA6IiI7czoxNDoidGV4dF9saW5rXzEwXzIiO3M6MDoiIjtzOjE0OiJ0ZXh0X2xpbmtfMTBfMyI7czowOiIiO3M6MTQ6InRleHRfbGlua18xMF80IjtzOjA6IiI7czoxNDoidGV4dF9saW5rXzEwXzUiO3M6MDoiIjtzOjE0OiJ0ZXh0X2xpbmtfMTBfNiI7czowOiIiO3M6MTQ6InRleHRfbGlua18xMF83IjtzOjA6IiI7czoxNDoidGV4dF9saW5rXzEwXzgiO3M6MDoiIjtzOjE0OiJ0ZXh0X2xpbmtfMTBfOSI7czowOiIiO3M6MzoidGFiIjtzOjEyOiJBZG1pbk1vZHVsZXMiO30=', 1, 1411641320),
	(17, 'Custome Link', 'html', 'YTozMjp7czoxOToic2F2ZXB0c3ZlcnRpY2FsbWVudSI7czoxOiIxIjtzOjk6ImlkX3dpZGdldCI7czowOiIiO3M6MTE6IndpZGdldF9uYW1lIjtzOjEyOiJDdXN0b21lIExpbmsiO3M6MTQ6IndpZGdldF90aXRsZV8xIjtzOjEyOiJDdXN0b21lIExpbmsiO3M6MTQ6IndpZGdldF90aXRsZV8yIjtzOjEyOiJDdXN0b21lIExpbmsiO3M6MTQ6IndpZGdldF90aXRsZV8zIjtzOjEyOiJDdXN0b21lIExpbmsiO3M6MTQ6IndpZGdldF90aXRsZV80IjtzOjEyOiJDdXN0b21lIExpbmsiO3M6MTQ6IndpZGdldF90aXRsZV81IjtzOjEyOiJDdXN0b21lIExpbmsiO3M6MTQ6IndpZGdldF90aXRsZV82IjtzOjEyOiJDdXN0b21lIExpbmsiO3M6MTQ6IndpZGdldF90aXRsZV83IjtzOjEyOiJDdXN0b21lIExpbmsiO3M6MTQ6IndpZGdldF90aXRsZV84IjtzOjEyOiJDdXN0b21lIExpbmsiO3M6MTQ6IndpZGdldF90aXRsZV85IjtzOjEyOiJDdXN0b21lIExpbmsiO3M6MTQ6ImFkZGl0aW9uY2xzc18xIjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzIiO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfMyI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc180IjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzUiO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfNiI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc183IjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzgiO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfOSI7czowOiIiO3M6MTE6IndpZGdldF90eXBlIjtzOjQ6Imh0bWwiO3M6MTM6Imh0bWxjb250ZW50XzEiO3M6MzIzOiI8dWwgY2xhc3M9J25hdi1saW5rcyc+DQo8bGk+PGEgaHJlZj0nIyc+SXBzdW0gZG9sb3Igc2l0PC9hPjwvbGk+DQo8bGk+PGEgaHJlZj0nIyc+UGxhY2VyYXQgZGFwaWJ1czwvYT48L2xpPg0KPGxpPjxhIGhyZWY9JyMnPlZpdGFlIHZlc3RpYnVsdW08L2E+PC9saT4NCjxsaT48YSBocmVmPScjJz5GdXNjZSBsb2JvcnRpczwvYT48L2xpPg0KPGxpPjxhIGhyZWY9JyMnPk1hdXJpcyB2aXZlcnJhPC9hPjwvbGk+DQo8bGk+PGEgaHJlZj0nIyc+Vml0YWUgdmVzdGlidWx1bTwvYT48L2xpPg0KPGxpPjxhIGhyZWY9JyMnPkZ1c2NlIGxvYm9ydGlzPC9hPjwvbGk+DQo8L3VsPiI7czoxMzoiaHRtbGNvbnRlbnRfMiI7czozMjM6Ijx1bCBjbGFzcz0nbmF2LWxpbmtzJz4NCjxsaT48YSBocmVmPScjJz5JcHN1bSBkb2xvciBzaXQ8L2E+PC9saT4NCjxsaT48YSBocmVmPScjJz5QbGFjZXJhdCBkYXBpYnVzPC9hPjwvbGk+DQo8bGk+PGEgaHJlZj0nIyc+Vml0YWUgdmVzdGlidWx1bTwvYT48L2xpPg0KPGxpPjxhIGhyZWY9JyMnPkZ1c2NlIGxvYm9ydGlzPC9hPjwvbGk+DQo8bGk+PGEgaHJlZj0nIyc+TWF1cmlzIHZpdmVycmE8L2E+PC9saT4NCjxsaT48YSBocmVmPScjJz5WaXRhZSB2ZXN0aWJ1bHVtPC9hPjwvbGk+DQo8bGk+PGEgaHJlZj0nIyc+RnVzY2UgbG9ib3J0aXM8L2E+PC9saT4NCjwvdWw+IjtzOjEzOiJodG1sY29udGVudF8zIjtzOjMyMzoiPHVsIGNsYXNzPSduYXYtbGlua3MnPg0KPGxpPjxhIGhyZWY9JyMnPklwc3VtIGRvbG9yIHNpdDwvYT48L2xpPg0KPGxpPjxhIGhyZWY9JyMnPlBsYWNlcmF0IGRhcGlidXM8L2E+PC9saT4NCjxsaT48YSBocmVmPScjJz5WaXRhZSB2ZXN0aWJ1bHVtPC9hPjwvbGk+DQo8bGk+PGEgaHJlZj0nIyc+RnVzY2UgbG9ib3J0aXM8L2E+PC9saT4NCjxsaT48YSBocmVmPScjJz5NYXVyaXMgdml2ZXJyYTwvYT48L2xpPg0KPGxpPjxhIGhyZWY9JyMnPlZpdGFlIHZlc3RpYnVsdW08L2E+PC9saT4NCjxsaT48YSBocmVmPScjJz5GdXNjZSBsb2JvcnRpczwvYT48L2xpPg0KPC91bD4iO3M6MTM6Imh0bWxjb250ZW50XzQiO3M6MzIzOiI8dWwgY2xhc3M9J25hdi1saW5rcyc+DQo8bGk+PGEgaHJlZj0nIyc+SXBzdW0gZG9sb3Igc2l0PC9hPjwvbGk+DQo8bGk+PGEgaHJlZj0nIyc+UGxhY2VyYXQgZGFwaWJ1czwvYT48L2xpPg0KPGxpPjxhIGhyZWY9JyMnPlZpdGFlIHZlc3RpYnVsdW08L2E+PC9saT4NCjxsaT48YSBocmVmPScjJz5GdXNjZSBsb2JvcnRpczwvYT48L2xpPg0KPGxpPjxhIGhyZWY9JyMnPk1hdXJpcyB2aXZlcnJhPC9hPjwvbGk+DQo8bGk+PGEgaHJlZj0nIyc+Vml0YWUgdmVzdGlidWx1bTwvYT48L2xpPg0KPGxpPjxhIGhyZWY9JyMnPkZ1c2NlIGxvYm9ydGlzPC9hPjwvbGk+DQo8L3VsPiI7czoxMzoiaHRtbGNvbnRlbnRfNSI7czozMjM6Ijx1bCBjbGFzcz0nbmF2LWxpbmtzJz4NCjxsaT48YSBocmVmPScjJz5JcHN1bSBkb2xvciBzaXQ8L2E+PC9saT4NCjxsaT48YSBocmVmPScjJz5QbGFjZXJhdCBkYXBpYnVzPC9hPjwvbGk+DQo8bGk+PGEgaHJlZj0nIyc+Vml0YWUgdmVzdGlidWx1bTwvYT48L2xpPg0KPGxpPjxhIGhyZWY9JyMnPkZ1c2NlIGxvYm9ydGlzPC9hPjwvbGk+DQo8bGk+PGEgaHJlZj0nIyc+TWF1cmlzIHZpdmVycmE8L2E+PC9saT4NCjxsaT48YSBocmVmPScjJz5WaXRhZSB2ZXN0aWJ1bHVtPC9hPjwvbGk+DQo8bGk+PGEgaHJlZj0nIyc+RnVzY2UgbG9ib3J0aXM8L2E+PC9saT4NCjwvdWw+IjtzOjEzOiJodG1sY29udGVudF82IjtzOjMyMzoiPHVsIGNsYXNzPSduYXYtbGlua3MnPg0KPGxpPjxhIGhyZWY9JyMnPklwc3VtIGRvbG9yIHNpdDwvYT48L2xpPg0KPGxpPjxhIGhyZWY9JyMnPlBsYWNlcmF0IGRhcGlidXM8L2E+PC9saT4NCjxsaT48YSBocmVmPScjJz5WaXRhZSB2ZXN0aWJ1bHVtPC9hPjwvbGk+DQo8bGk+PGEgaHJlZj0nIyc+RnVzY2UgbG9ib3J0aXM8L2E+PC9saT4NCjxsaT48YSBocmVmPScjJz5NYXVyaXMgdml2ZXJyYTwvYT48L2xpPg0KPGxpPjxhIGhyZWY9JyMnPlZpdGFlIHZlc3RpYnVsdW08L2E+PC9saT4NCjxsaT48YSBocmVmPScjJz5GdXNjZSBsb2JvcnRpczwvYT48L2xpPg0KPC91bD4iO3M6MTM6Imh0bWxjb250ZW50XzciO3M6MzIzOiI8dWwgY2xhc3M9J25hdi1saW5rcyc+DQo8bGk+PGEgaHJlZj0nIyc+SXBzdW0gZG9sb3Igc2l0PC9hPjwvbGk+DQo8bGk+PGEgaHJlZj0nIyc+UGxhY2VyYXQgZGFwaWJ1czwvYT48L2xpPg0KPGxpPjxhIGhyZWY9JyMnPlZpdGFlIHZlc3RpYnVsdW08L2E+PC9saT4NCjxsaT48YSBocmVmPScjJz5GdXNjZSBsb2JvcnRpczwvYT48L2xpPg0KPGxpPjxhIGhyZWY9JyMnPk1hdXJpcyB2aXZlcnJhPC9hPjwvbGk+DQo8bGk+PGEgaHJlZj0nIyc+Vml0YWUgdmVzdGlidWx1bTwvYT48L2xpPg0KPGxpPjxhIGhyZWY9JyMnPkZ1c2NlIGxvYm9ydGlzPC9hPjwvbGk+DQo8L3VsPiI7czoxMzoiaHRtbGNvbnRlbnRfOCI7czozMjM6Ijx1bCBjbGFzcz0nbmF2LWxpbmtzJz4NCjxsaT48YSBocmVmPScjJz5JcHN1bSBkb2xvciBzaXQ8L2E+PC9saT4NCjxsaT48YSBocmVmPScjJz5QbGFjZXJhdCBkYXBpYnVzPC9hPjwvbGk+DQo8bGk+PGEgaHJlZj0nIyc+Vml0YWUgdmVzdGlidWx1bTwvYT48L2xpPg0KPGxpPjxhIGhyZWY9JyMnPkZ1c2NlIGxvYm9ydGlzPC9hPjwvbGk+DQo8bGk+PGEgaHJlZj0nIyc+TWF1cmlzIHZpdmVycmE8L2E+PC9saT4NCjxsaT48YSBocmVmPScjJz5WaXRhZSB2ZXN0aWJ1bHVtPC9hPjwvbGk+DQo8bGk+PGEgaHJlZj0nIyc+RnVzY2UgbG9ib3J0aXM8L2E+PC9saT4NCjwvdWw+IjtzOjEzOiJodG1sY29udGVudF85IjtzOjMyMzoiPHVsIGNsYXNzPSduYXYtbGlua3MnPg0KPGxpPjxhIGhyZWY9JyMnPklwc3VtIGRvbG9yIHNpdDwvYT48L2xpPg0KPGxpPjxhIGhyZWY9JyMnPlBsYWNlcmF0IGRhcGlidXM8L2E+PC9saT4NCjxsaT48YSBocmVmPScjJz5WaXRhZSB2ZXN0aWJ1bHVtPC9hPjwvbGk+DQo8bGk+PGEgaHJlZj0nIyc+RnVzY2UgbG9ib3J0aXM8L2E+PC9saT4NCjxsaT48YSBocmVmPScjJz5NYXVyaXMgdml2ZXJyYTwvYT48L2xpPg0KPGxpPjxhIGhyZWY9JyMnPlZpdGFlIHZlc3RpYnVsdW08L2E+PC9saT4NCjxsaT48YSBocmVmPScjJz5GdXNjZSBsb2JvcnRpczwvYT48L2xpPg0KPC91bD4iO3M6MzoidGFiIjtzOjEyOiJBZG1pbk1vZHVsZXMiO30=', 1, 1411641757),
	(18, 'Video widget', 'video_code', 'YToyNjp7czoxOToic2F2ZXB0c3ZlcnRpY2FsbWVudSI7czoxOiIxIjtzOjk6ImlkX3dpZGdldCI7czowOiIiO3M6MTE6IndpZGdldF9uYW1lIjtzOjEyOiJWaWRlbyB3aWRnZXQiO3M6MTQ6IndpZGdldF90aXRsZV8xIjtzOjU6IlZpZGVvIjtzOjE0OiJ3aWRnZXRfdGl0bGVfMiI7czo1OiJWaWRlbyI7czoxNDoid2lkZ2V0X3RpdGxlXzMiO3M6NToiVmlkZW8iO3M6MTQ6IndpZGdldF90aXRsZV80IjtzOjU6IlZpZGVvIjtzOjE0OiJ3aWRnZXRfdGl0bGVfNSI7czo1OiJWaWRlbyI7czoxNDoid2lkZ2V0X3RpdGxlXzYiO3M6NToiVmlkZW8iO3M6MTQ6IndpZGdldF90aXRsZV83IjtzOjU6IlZpZGVvIjtzOjE0OiJ3aWRnZXRfdGl0bGVfOCI7czo1OiJWaWRlbyI7czoxNDoid2lkZ2V0X3RpdGxlXzkiO3M6NToiVmlkZW8iO3M6MTQ6ImFkZGl0aW9uY2xzc18xIjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzIiO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfMyI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc180IjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzUiO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfNiI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc183IjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzgiO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfOSI7czowOiIiO3M6MTE6IndpZGdldF90eXBlIjtzOjEwOiJ2aWRlb19jb2RlIjtzOjEwOiJ2aWRlb19saW5rIjtzOjQyOiJodHRwOi8vd3d3LnlvdXR1YmUuY29tL3dhdGNoP3Y9bHpZNGxrVDhFbFUiO3M6NToid2lkdGgiO3M6NDoiMTAwJSI7czo2OiJoZWlnaHQiO3M6NToiMzAwcHgiO3M6MzoidGFiIjtzOjEyOiJBZG1pbk1vZHVsZXMiO30=', 1, 1411642153),
	(19, 'Banner', 'html', 'YTozMjp7czoxOToic2F2ZXB0c3ZlcnRpY2FsbWVudSI7czoxOiIxIjtzOjk6ImlkX3dpZGdldCI7czowOiIiO3M6MTE6IndpZGdldF9uYW1lIjtzOjY6IkJhbm5lciI7czoxNDoid2lkZ2V0X3RpdGxlXzEiO3M6NjoiQmFubmVyIjtzOjE0OiJ3aWRnZXRfdGl0bGVfMiI7czo2OiJCYW5uZXIiO3M6MTQ6IndpZGdldF90aXRsZV8zIjtzOjY6IkJhbm5lciI7czoxNDoid2lkZ2V0X3RpdGxlXzQiO3M6NjoiQmFubmVyIjtzOjE0OiJ3aWRnZXRfdGl0bGVfNSI7czo2OiJCYW5uZXIiO3M6MTQ6IndpZGdldF90aXRsZV82IjtzOjY6IkJhbm5lciI7czoxNDoid2lkZ2V0X3RpdGxlXzciO3M6NjoiQmFubmVyIjtzOjE0OiJ3aWRnZXRfdGl0bGVfOCI7czo2OiJCYW5uZXIiO3M6MTQ6IndpZGdldF90aXRsZV85IjtzOjY6IkJhbm5lciI7czoxNDoiYWRkaXRpb25jbHNzXzEiO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfMiI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc18zIjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzQiO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfNSI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc182IjtzOjA6IiI7czoxNDoiYWRkaXRpb25jbHNzXzciO3M6MDoiIjtzOjE0OiJhZGRpdGlvbmNsc3NfOCI7czowOiIiO3M6MTQ6ImFkZGl0aW9uY2xzc185IjtzOjA6IiI7czoxMToid2lkZ2V0X3R5cGUiO3M6NDoiaHRtbCI7czoxMzoiaHRtbGNvbnRlbnRfMSI7czo5NjoiPHA+PGltZyBzcmM9J2h0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9tYXh4X3Nob3AvaW1nL2Ntcy9iYW5uZXItY2F0ZWdvcnkuanBnJyBhbHQ9JycgLz48L3A+IjtzOjEzOiJodG1sY29udGVudF8yIjtzOjk2OiI8cD48aW1nIHNyYz0naHR0cDovL2xvY2FsaG9zdC9wcmVzdGFzaG9wL3BmX21heHhfc2hvcC9pbWcvY21zL2Jhbm5lci1jYXRlZ29yeS5qcGcnIGFsdD0nJyAvPjwvcD4iO3M6MTM6Imh0bWxjb250ZW50XzMiO3M6OTY6IjxwPjxpbWcgc3JjPSdodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWF4eF9zaG9wL2ltZy9jbXMvYmFubmVyLWNhdGVnb3J5LmpwZycgYWx0PScnIC8+PC9wPiI7czoxMzoiaHRtbGNvbnRlbnRfNCI7czo5NjoiPHA+PGltZyBzcmM9J2h0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9tYXh4X3Nob3AvaW1nL2Ntcy9iYW5uZXItY2F0ZWdvcnkuanBnJyBhbHQ9JycgLz48L3A+IjtzOjEzOiJodG1sY29udGVudF81IjtzOjk2OiI8cD48aW1nIHNyYz0naHR0cDovL2xvY2FsaG9zdC9wcmVzdGFzaG9wL3BmX21heHhfc2hvcC9pbWcvY21zL2Jhbm5lci1jYXRlZ29yeS5qcGcnIGFsdD0nJyAvPjwvcD4iO3M6MTM6Imh0bWxjb250ZW50XzYiO3M6OTY6IjxwPjxpbWcgc3JjPSdodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWF4eF9zaG9wL2ltZy9jbXMvYmFubmVyLWNhdGVnb3J5LmpwZycgYWx0PScnIC8+PC9wPiI7czoxMzoiaHRtbGNvbnRlbnRfNyI7czo5NjoiPHA+PGltZyBzcmM9J2h0dHA6Ly9sb2NhbGhvc3QvcHJlc3Rhc2hvcC9wZl9tYXh4X3Nob3AvaW1nL2Ntcy9iYW5uZXItY2F0ZWdvcnkuanBnJyBhbHQ9JycgLz48L3A+IjtzOjEzOiJodG1sY29udGVudF84IjtzOjk2OiI8cD48aW1nIHNyYz0naHR0cDovL2xvY2FsaG9zdC9wcmVzdGFzaG9wL3BmX21heHhfc2hvcC9pbWcvY21zL2Jhbm5lci1jYXRlZ29yeS5qcGcnIGFsdD0nJyAvPjwvcD4iO3M6MTM6Imh0bWxjb250ZW50XzkiO3M6OTY6IjxwPjxpbWcgc3JjPSdodHRwOi8vbG9jYWxob3N0L3ByZXN0YXNob3AvcGZfbWF4eF9zaG9wL2ltZy9jbXMvYmFubmVyLWNhdGVnb3J5LmpwZycgYWx0PScnIC8+PC9wPiI7czozOiJ0YWIiO3M6MTI6IkFkbWluTW9kdWxlcyI7fQ==', 1, 1411643422);
/*!40000 ALTER TABLE `ps_ptsverticalmenu_widgets` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_quick_access
DROP TABLE IF EXISTS `ps_quick_access`;
CREATE TABLE IF NOT EXISTS `ps_quick_access` (
  `id_quick_access` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `new_window` tinyint(1) NOT NULL DEFAULT '0',
  `link` varchar(255) NOT NULL,
  PRIMARY KEY (`id_quick_access`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_quick_access: ~3 rows (approximately)
/*!40000 ALTER TABLE `ps_quick_access` DISABLE KEYS */;
INSERT INTO `ps_quick_access` (`id_quick_access`, `new_window`, `link`) VALUES
	(1, 0, 'index.php?controller=AdminCategories&addcategory'),
	(2, 0, 'index.php?controller=AdminProducts&addproduct'),
	(3, 0, 'index.php?controller=AdminCartRules&addcart_rule');
/*!40000 ALTER TABLE `ps_quick_access` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_quick_access_lang
DROP TABLE IF EXISTS `ps_quick_access_lang`;
CREATE TABLE IF NOT EXISTS `ps_quick_access_lang` (
  `id_quick_access` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`id_quick_access`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_quick_access_lang: ~3 rows (approximately)
/*!40000 ALTER TABLE `ps_quick_access_lang` DISABLE KEYS */;
INSERT INTO `ps_quick_access_lang` (`id_quick_access`, `id_lang`, `name`) VALUES
	(1, 1, 'Nueva categoría'),
	(2, 1, 'Nuevo producto'),
	(3, 1, 'Nuevo vale');
/*!40000 ALTER TABLE `ps_quick_access_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_range_price
DROP TABLE IF EXISTS `ps_range_price`;
CREATE TABLE IF NOT EXISTS `ps_range_price` (
  `id_range_price` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_carrier` int(10) unsigned NOT NULL,
  `delimiter1` decimal(20,6) NOT NULL,
  `delimiter2` decimal(20,6) NOT NULL,
  PRIMARY KEY (`id_range_price`),
  UNIQUE KEY `id_carrier` (`id_carrier`,`delimiter1`,`delimiter2`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_range_price: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_range_price` DISABLE KEYS */;
INSERT INTO `ps_range_price` (`id_range_price`, `id_carrier`, `delimiter1`, `delimiter2`) VALUES
	(1, 2, 0.000000, 10000.000000);
/*!40000 ALTER TABLE `ps_range_price` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_range_weight
DROP TABLE IF EXISTS `ps_range_weight`;
CREATE TABLE IF NOT EXISTS `ps_range_weight` (
  `id_range_weight` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_carrier` int(10) unsigned NOT NULL,
  `delimiter1` decimal(20,6) NOT NULL,
  `delimiter2` decimal(20,6) NOT NULL,
  PRIMARY KEY (`id_range_weight`),
  UNIQUE KEY `id_carrier` (`id_carrier`,`delimiter1`,`delimiter2`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_range_weight: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_range_weight` DISABLE KEYS */;
INSERT INTO `ps_range_weight` (`id_range_weight`, `id_carrier`, `delimiter1`, `delimiter2`) VALUES
	(1, 2, 0.000000, 10000.000000);
/*!40000 ALTER TABLE `ps_range_weight` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_referrer
DROP TABLE IF EXISTS `ps_referrer`;
CREATE TABLE IF NOT EXISTS `ps_referrer` (
  `id_referrer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `passwd` varchar(32) DEFAULT NULL,
  `http_referer_regexp` varchar(64) DEFAULT NULL,
  `http_referer_like` varchar(64) DEFAULT NULL,
  `request_uri_regexp` varchar(64) DEFAULT NULL,
  `request_uri_like` varchar(64) DEFAULT NULL,
  `http_referer_regexp_not` varchar(64) DEFAULT NULL,
  `http_referer_like_not` varchar(64) DEFAULT NULL,
  `request_uri_regexp_not` varchar(64) DEFAULT NULL,
  `request_uri_like_not` varchar(64) DEFAULT NULL,
  `base_fee` decimal(5,2) NOT NULL DEFAULT '0.00',
  `percent_fee` decimal(5,2) NOT NULL DEFAULT '0.00',
  `click_fee` decimal(5,2) NOT NULL DEFAULT '0.00',
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_referrer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_referrer: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_referrer` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_referrer` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_referrer_cache
DROP TABLE IF EXISTS `ps_referrer_cache`;
CREATE TABLE IF NOT EXISTS `ps_referrer_cache` (
  `id_connections_source` int(11) unsigned NOT NULL,
  `id_referrer` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_connections_source`,`id_referrer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_referrer_cache: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_referrer_cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_referrer_cache` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_referrer_shop
DROP TABLE IF EXISTS `ps_referrer_shop`;
CREATE TABLE IF NOT EXISTS `ps_referrer_shop` (
  `id_referrer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL DEFAULT '1',
  `cache_visitors` int(11) DEFAULT NULL,
  `cache_visits` int(11) DEFAULT NULL,
  `cache_pages` int(11) DEFAULT NULL,
  `cache_registrations` int(11) DEFAULT NULL,
  `cache_orders` int(11) DEFAULT NULL,
  `cache_sales` decimal(17,2) DEFAULT NULL,
  `cache_reg_rate` decimal(5,4) DEFAULT NULL,
  `cache_order_rate` decimal(5,4) DEFAULT NULL,
  PRIMARY KEY (`id_referrer`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_referrer_shop: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_referrer_shop` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_referrer_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_request_sql
DROP TABLE IF EXISTS `ps_request_sql`;
CREATE TABLE IF NOT EXISTS `ps_request_sql` (
  `id_request_sql` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `sql` text NOT NULL,
  PRIMARY KEY (`id_request_sql`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_request_sql: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_request_sql` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_request_sql` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_required_field
DROP TABLE IF EXISTS `ps_required_field`;
CREATE TABLE IF NOT EXISTS `ps_required_field` (
  `id_required_field` int(11) NOT NULL AUTO_INCREMENT,
  `object_name` varchar(32) NOT NULL,
  `field_name` varchar(32) NOT NULL,
  PRIMARY KEY (`id_required_field`),
  KEY `object_name` (`object_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_required_field: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_required_field` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_required_field` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_risk
DROP TABLE IF EXISTS `ps_risk`;
CREATE TABLE IF NOT EXISTS `ps_risk` (
  `id_risk` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `percent` tinyint(3) NOT NULL,
  `color` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id_risk`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_risk: ~4 rows (approximately)
/*!40000 ALTER TABLE `ps_risk` DISABLE KEYS */;
INSERT INTO `ps_risk` (`id_risk`, `percent`, `color`) VALUES
	(1, 0, '#32CD32'),
	(2, 35, '#FF8C00'),
	(3, 75, '#DC143C'),
	(4, 100, '#ec2e15');
/*!40000 ALTER TABLE `ps_risk` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_risk_lang
DROP TABLE IF EXISTS `ps_risk_lang`;
CREATE TABLE IF NOT EXISTS `ps_risk_lang` (
  `id_risk` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id_risk`,`id_lang`),
  KEY `id_risk` (`id_risk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_risk_lang: ~4 rows (approximately)
/*!40000 ALTER TABLE `ps_risk_lang` DISABLE KEYS */;
INSERT INTO `ps_risk_lang` (`id_risk`, `id_lang`, `name`) VALUES
	(1, 1, 'None'),
	(2, 1, 'Low'),
	(3, 1, 'Medium'),
	(4, 1, 'High');
/*!40000 ALTER TABLE `ps_risk_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_scene
DROP TABLE IF EXISTS `ps_scene`;
CREATE TABLE IF NOT EXISTS `ps_scene` (
  `id_scene` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_scene`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_scene: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_scene` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_scene` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_scene_category
DROP TABLE IF EXISTS `ps_scene_category`;
CREATE TABLE IF NOT EXISTS `ps_scene_category` (
  `id_scene` int(10) unsigned NOT NULL,
  `id_category` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_scene`,`id_category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_scene_category: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_scene_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_scene_category` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_scene_lang
DROP TABLE IF EXISTS `ps_scene_lang`;
CREATE TABLE IF NOT EXISTS `ps_scene_lang` (
  `id_scene` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id_scene`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_scene_lang: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_scene_lang` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_scene_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_scene_products
DROP TABLE IF EXISTS `ps_scene_products`;
CREATE TABLE IF NOT EXISTS `ps_scene_products` (
  `id_scene` int(10) unsigned NOT NULL,
  `id_product` int(10) unsigned NOT NULL,
  `x_axis` int(4) NOT NULL,
  `y_axis` int(4) NOT NULL,
  `zone_width` int(3) NOT NULL,
  `zone_height` int(3) NOT NULL,
  PRIMARY KEY (`id_scene`,`id_product`,`x_axis`,`y_axis`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_scene_products: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_scene_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_scene_products` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_scene_shop
DROP TABLE IF EXISTS `ps_scene_shop`;
CREATE TABLE IF NOT EXISTS `ps_scene_shop` (
  `id_scene` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_scene`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_scene_shop: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_scene_shop` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_scene_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_search_engine
DROP TABLE IF EXISTS `ps_search_engine`;
CREATE TABLE IF NOT EXISTS `ps_search_engine` (
  `id_search_engine` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(64) NOT NULL,
  `getvar` varchar(16) NOT NULL,
  PRIMARY KEY (`id_search_engine`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_search_engine: ~38 rows (approximately)
/*!40000 ALTER TABLE `ps_search_engine` DISABLE KEYS */;
INSERT INTO `ps_search_engine` (`id_search_engine`, `server`, `getvar`) VALUES
	(1, 'google', 'q'),
	(2, 'aol', 'q'),
	(3, 'yandex', 'text'),
	(4, 'ask.com', 'q'),
	(5, 'nhl.com', 'q'),
	(6, 'yahoo', 'p'),
	(7, 'baidu', 'wd'),
	(8, 'lycos', 'query'),
	(9, 'exalead', 'q'),
	(10, 'search.live', 'q'),
	(11, 'voila', 'rdata'),
	(12, 'altavista', 'q'),
	(13, 'bing', 'q'),
	(14, 'daum', 'q'),
	(15, 'eniro', 'search_word'),
	(16, 'naver', 'query'),
	(17, 'msn', 'q'),
	(18, 'netscape', 'query'),
	(19, 'cnn', 'query'),
	(20, 'about', 'terms'),
	(21, 'mamma', 'query'),
	(22, 'alltheweb', 'q'),
	(23, 'virgilio', 'qs'),
	(24, 'alice', 'qs'),
	(25, 'najdi', 'q'),
	(26, 'mama', 'query'),
	(27, 'seznam', 'q'),
	(28, 'onet', 'qt'),
	(29, 'szukacz', 'q'),
	(30, 'yam', 'k'),
	(31, 'pchome', 'q'),
	(32, 'kvasir', 'q'),
	(33, 'sesam', 'q'),
	(34, 'ozu', 'q'),
	(35, 'terra', 'query'),
	(36, 'mynet', 'q'),
	(37, 'ekolay', 'q'),
	(38, 'rambler', 'words');
/*!40000 ALTER TABLE `ps_search_engine` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_search_index
DROP TABLE IF EXISTS `ps_search_index`;
CREATE TABLE IF NOT EXISTS `ps_search_index` (
  `id_product` int(11) unsigned NOT NULL,
  `id_word` int(11) unsigned NOT NULL,
  `weight` smallint(4) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_word`,`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_search_index: ~534 rows (approximately)
/*!40000 ALTER TABLE `ps_search_index` DISABLE KEYS */;
INSERT INTO `ps_search_index` (`id_product`, `id_word`, `weight`) VALUES
	(1, 1, 42),
	(1, 2, 42),
	(1, 3, 42),
	(1, 4, 72),
	(2, 4, 24),
	(1, 5, 54),
	(2, 5, 18),
	(1, 6, 60),
	(2, 6, 60),
	(3, 6, 30),
	(4, 6, 60),
	(5, 6, 120),
	(6, 6, 60),
	(7, 6, 60),
	(1, 7, 6),
	(5, 7, 12),
	(6, 7, 6),
	(7, 7, 6),
	(1, 8, 6),
	(1, 9, 6),
	(1, 10, 6),
	(1, 11, 6),
	(6, 11, 6),
	(1, 12, 6),
	(1, 13, 6),
	(1, 14, 6),
	(1, 15, 6),
	(1, 16, 6),
	(1, 17, 6),
	(1, 18, 6),
	(1, 19, 6),
	(5, 19, 108),
	(6, 19, 54),
	(7, 19, 18),
	(1, 20, 30),
	(2, 20, 30),
	(3, 20, 15),
	(4, 20, 30),
	(5, 20, 60),
	(6, 20, 30),
	(7, 20, 30),
	(1, 21, 6),
	(2, 21, 6),
	(3, 21, 3),
	(4, 21, 6),
	(5, 21, 12),
	(6, 21, 6),
	(7, 21, 6),
	(1, 22, 6),
	(2, 22, 6),
	(3, 22, 3),
	(4, 22, 6),
	(5, 22, 12),
	(6, 22, 6),
	(7, 22, 6),
	(1, 23, 12),
	(2, 23, 12),
	(3, 23, 6),
	(4, 23, 12),
	(5, 23, 24),
	(6, 23, 12),
	(7, 23, 12),
	(1, 24, 6),
	(2, 24, 6),
	(3, 24, 3),
	(4, 24, 6),
	(5, 24, 12),
	(6, 24, 6),
	(7, 24, 6),
	(1, 25, 6),
	(2, 25, 6),
	(3, 25, 3),
	(4, 25, 6),
	(5, 25, 12),
	(6, 25, 6),
	(7, 25, 6),
	(1, 26, 6),
	(2, 26, 6),
	(3, 26, 3),
	(4, 26, 6),
	(5, 26, 12),
	(6, 26, 6),
	(7, 26, 6),
	(1, 27, 6),
	(2, 27, 6),
	(3, 27, 3),
	(4, 27, 6),
	(5, 27, 12),
	(6, 27, 6),
	(7, 27, 6),
	(1, 28, 6),
	(2, 28, 6),
	(3, 28, 3),
	(4, 28, 6),
	(5, 28, 12),
	(6, 28, 6),
	(7, 28, 6),
	(1, 29, 6),
	(2, 29, 6),
	(3, 29, 3),
	(4, 29, 6),
	(5, 29, 12),
	(6, 29, 6),
	(7, 29, 6),
	(1, 30, 6),
	(2, 30, 6),
	(3, 30, 3),
	(4, 30, 6),
	(5, 30, 12),
	(6, 30, 6),
	(7, 30, 6),
	(1, 31, 12),
	(2, 31, 12),
	(3, 31, 6),
	(4, 31, 12),
	(5, 31, 24),
	(6, 31, 12),
	(7, 31, 12),
	(1, 32, 6),
	(2, 32, 6),
	(3, 32, 3),
	(4, 32, 6),
	(5, 32, 12),
	(6, 32, 6),
	(7, 32, 6),
	(1, 33, 6),
	(2, 33, 6),
	(3, 33, 3),
	(4, 33, 6),
	(5, 33, 12),
	(6, 33, 6),
	(7, 33, 6),
	(1, 34, 6),
	(2, 34, 6),
	(3, 34, 3),
	(4, 34, 6),
	(5, 34, 12),
	(6, 34, 6),
	(7, 34, 6),
	(1, 35, 6),
	(2, 35, 6),
	(3, 35, 12),
	(4, 35, 24),
	(5, 35, 48),
	(6, 35, 24),
	(7, 35, 24),
	(1, 36, 6),
	(2, 36, 6),
	(3, 36, 3),
	(4, 36, 6),
	(5, 36, 12),
	(6, 36, 6),
	(7, 36, 6),
	(1, 37, 6),
	(2, 37, 6),
	(3, 37, 3),
	(4, 37, 6),
	(5, 37, 12),
	(6, 37, 6),
	(7, 37, 6),
	(1, 38, 6),
	(2, 38, 6),
	(3, 38, 3),
	(4, 38, 6),
	(5, 38, 12),
	(6, 38, 6),
	(7, 38, 6),
	(1, 39, 6),
	(2, 39, 6),
	(3, 39, 3),
	(4, 39, 6),
	(5, 39, 12),
	(6, 39, 6),
	(7, 39, 6),
	(1, 40, 6),
	(2, 40, 6),
	(3, 40, 3),
	(4, 40, 6),
	(5, 40, 12),
	(6, 40, 6),
	(7, 40, 6),
	(1, 41, 6),
	(2, 41, 6),
	(3, 41, 3),
	(4, 41, 6),
	(5, 41, 12),
	(6, 41, 6),
	(7, 41, 6),
	(1, 42, 6),
	(2, 42, 6),
	(3, 42, 3),
	(4, 42, 6),
	(5, 42, 12),
	(6, 42, 6),
	(7, 42, 6),
	(1, 43, 6),
	(2, 43, 6),
	(3, 43, 3),
	(4, 43, 6),
	(5, 43, 12),
	(6, 43, 6),
	(7, 43, 6),
	(1, 44, 6),
	(2, 44, 6),
	(3, 44, 3),
	(4, 44, 6),
	(5, 44, 12),
	(6, 44, 6),
	(7, 44, 6),
	(1, 45, 6),
	(2, 45, 6),
	(3, 45, 3),
	(4, 45, 6),
	(5, 45, 12),
	(6, 45, 6),
	(7, 45, 6),
	(1, 46, 6),
	(2, 46, 6),
	(3, 46, 3),
	(4, 46, 6),
	(5, 46, 12),
	(6, 46, 6),
	(7, 46, 6),
	(1, 47, 6),
	(2, 47, 6),
	(3, 47, 3),
	(4, 47, 6),
	(5, 47, 12),
	(6, 47, 6),
	(7, 47, 6),
	(1, 48, 6),
	(2, 48, 6),
	(3, 48, 3),
	(4, 48, 6),
	(5, 48, 12),
	(6, 48, 6),
	(7, 48, 6),
	(1, 49, 6),
	(2, 49, 6),
	(3, 49, 3),
	(4, 49, 6),
	(5, 49, 12),
	(6, 49, 6),
	(7, 49, 6),
	(1, 50, 6),
	(2, 50, 6),
	(3, 50, 3),
	(4, 50, 6),
	(5, 50, 12),
	(6, 50, 6),
	(7, 50, 6),
	(1, 51, 6),
	(2, 51, 6),
	(3, 51, 3),
	(4, 51, 6),
	(5, 51, 12),
	(6, 51, 6),
	(7, 51, 6),
	(1, 52, 6),
	(2, 52, 6),
	(3, 52, 3),
	(4, 52, 6),
	(5, 52, 12),
	(6, 52, 6),
	(7, 52, 6),
	(1, 53, 6),
	(2, 53, 6),
	(3, 53, 3),
	(4, 53, 6),
	(5, 53, 12),
	(6, 53, 6),
	(7, 53, 6),
	(1, 54, 6),
	(2, 54, 6),
	(3, 54, 3),
	(4, 54, 6),
	(5, 54, 12),
	(6, 54, 6),
	(7, 54, 6),
	(1, 55, 6),
	(2, 55, 6),
	(3, 55, 3),
	(4, 55, 6),
	(5, 55, 12),
	(6, 55, 6),
	(7, 55, 6),
	(1, 56, 6),
	(2, 56, 6),
	(3, 56, 3),
	(4, 56, 6),
	(5, 56, 12),
	(6, 56, 6),
	(7, 56, 6),
	(1, 57, 6),
	(2, 57, 6),
	(3, 57, 3),
	(4, 57, 6),
	(5, 57, 12),
	(6, 57, 6),
	(7, 57, 6),
	(1, 58, 6),
	(2, 58, 6),
	(3, 58, 3),
	(4, 58, 6),
	(5, 58, 12),
	(6, 58, 6),
	(7, 58, 6),
	(1, 59, 6),
	(2, 59, 6),
	(3, 59, 3),
	(4, 59, 6),
	(5, 59, 12),
	(6, 59, 6),
	(7, 59, 6),
	(1, 60, 6),
	(2, 60, 6),
	(3, 60, 3),
	(4, 60, 6),
	(5, 60, 12),
	(6, 60, 6),
	(7, 60, 6),
	(1, 61, 6),
	(2, 61, 6),
	(3, 61, 3),
	(4, 61, 6),
	(5, 61, 12),
	(6, 61, 6),
	(7, 61, 6),
	(1, 62, 6),
	(2, 62, 6),
	(3, 62, 3),
	(4, 62, 6),
	(5, 62, 12),
	(6, 62, 6),
	(1, 63, 6),
	(2, 63, 6),
	(3, 63, 3),
	(4, 63, 6),
	(5, 63, 12),
	(6, 63, 6),
	(7, 63, 6),
	(1, 64, 6),
	(2, 64, 6),
	(3, 64, 3),
	(4, 64, 6),
	(5, 64, 12),
	(6, 64, 12),
	(7, 64, 12),
	(1, 65, 12),
	(2, 65, 12),
	(3, 65, 6),
	(4, 65, 12),
	(5, 65, 24),
	(6, 65, 12),
	(7, 65, 12),
	(1, 66, 6),
	(2, 66, 6),
	(3, 66, 3),
	(4, 66, 6),
	(5, 66, 12),
	(6, 66, 6),
	(7, 66, 6),
	(1, 67, 6),
	(2, 67, 12),
	(3, 67, 3),
	(4, 67, 6),
	(5, 67, 12),
	(6, 67, 6),
	(7, 67, 6),
	(1, 68, 6),
	(2, 68, 6),
	(3, 68, 3),
	(4, 68, 6),
	(5, 68, 12),
	(6, 68, 6),
	(7, 68, 6),
	(1, 69, 6),
	(2, 69, 6),
	(3, 69, 3),
	(4, 69, 6),
	(5, 69, 12),
	(6, 69, 6),
	(7, 69, 6),
	(1, 70, 6),
	(2, 70, 6),
	(3, 70, 3),
	(4, 70, 6),
	(5, 70, 12),
	(6, 70, 6),
	(7, 70, 6),
	(1, 71, 6),
	(2, 71, 6),
	(3, 71, 3),
	(4, 71, 6),
	(5, 71, 12),
	(6, 71, 6),
	(7, 71, 6),
	(1, 72, 6),
	(2, 72, 6),
	(3, 72, 3),
	(4, 72, 6),
	(5, 72, 12),
	(6, 72, 6),
	(7, 72, 6),
	(1, 73, 6),
	(2, 73, 6),
	(3, 73, 3),
	(4, 73, 6),
	(5, 73, 12),
	(6, 73, 6),
	(7, 73, 6),
	(1, 74, 6),
	(2, 74, 6),
	(3, 74, 3),
	(4, 74, 6),
	(5, 74, 12),
	(6, 74, 6),
	(7, 74, 6),
	(1, 75, 6),
	(2, 75, 6),
	(3, 75, 3),
	(4, 75, 6),
	(5, 75, 12),
	(6, 75, 6),
	(7, 75, 6),
	(1, 76, 6),
	(2, 76, 6),
	(3, 76, 3),
	(4, 76, 6),
	(5, 76, 12),
	(6, 76, 6),
	(7, 76, 6),
	(1, 77, 6),
	(2, 77, 6),
	(3, 77, 3),
	(4, 77, 6),
	(5, 77, 12),
	(6, 77, 6),
	(7, 77, 6),
	(1, 79, 18),
	(2, 79, 18),
	(3, 79, 9),
	(4, 79, 18),
	(5, 79, 36),
	(6, 79, 18),
	(7, 79, 18),
	(1, 80, 36),
	(3, 80, 21),
	(5, 80, 72),
	(1, 81, 36),
	(5, 81, 72),
	(1, 82, 12),
	(2, 82, 12),
	(3, 82, 9),
	(1, 83, 12),
	(2, 83, 12),
	(5, 83, 24),
	(2, 84, 42),
	(2, 85, 6),
	(2, 86, 6),
	(3, 86, 6),
	(6, 86, 12),
	(7, 86, 12),
	(2, 87, 18),
	(2, 88, 36),
	(4, 88, 6),
	(5, 88, 72),
	(2, 89, 36),
	(6, 89, 36),
	(3, 90, 27),
	(4, 90, 54),
	(5, 90, 120),
	(6, 90, 54),
	(7, 90, 54),
	(3, 91, 21),
	(4, 91, 42),
	(5, 91, 84),
	(6, 91, 36),
	(7, 91, 42),
	(3, 92, 3),
	(3, 93, 3),
	(3, 94, 3),
	(3, 95, 3),
	(3, 96, 3),
	(3, 97, 3),
	(3, 98, 3),
	(3, 99, 3),
	(3, 100, 3),
	(3, 101, 3),
	(3, 102, 9),
	(3, 103, 6),
	(4, 104, 24),
	(4, 105, 6),
	(6, 105, 6),
	(7, 105, 6),
	(4, 106, 6),
	(4, 107, 6),
	(4, 108, 6),
	(5, 108, 12),
	(4, 109, 36),
	(4, 110, 36),
	(4, 111, 12),
	(5, 111, 24),
	(4, 112, 12),
	(4, 113, 12),
	(6, 113, 12),
	(5, 114, 12),
	(5, 115, 12),
	(5, 116, 12),
	(5, 117, 12),
	(5, 118, 12),
	(6, 118, 6),
	(5, 119, 24),
	(6, 119, 6),
	(5, 120, 12),
	(6, 120, 6),
	(5, 121, 12),
	(5, 122, 12),
	(5, 123, 72),
	(6, 123, 36),
	(7, 123, 36),
	(6, 124, 6),
	(7, 124, 42),
	(6, 125, 6),
	(7, 125, 6),
	(6, 126, 12),
	(7, 126, 12),
	(7, 127, 6),
	(7, 128, 6),
	(7, 129, 6),
	(7, 130, 36),
	(7, 131, 12),
	(7, 132, 12),
	(1, 133, 18);
/*!40000 ALTER TABLE `ps_search_index` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_search_word
DROP TABLE IF EXISTS `ps_search_word`;
CREATE TABLE IF NOT EXISTS `ps_search_word` (
  `id_word` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_lang` int(10) unsigned NOT NULL,
  `word` varchar(15) NOT NULL,
  PRIMARY KEY (`id_word`),
  UNIQUE KEY `id_lang` (`id_lang`,`id_shop`,`word`)
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_search_word: ~133 rows (approximately)
/*!40000 ALTER TABLE `ps_search_word` DISABLE KEYS */;
INSERT INTO `ps_search_word` (`id_word`, `id_shop`, `id_lang`, `word`) VALUES
	(93, 1, 1, '100'),
	(25, 1, 1, '2010'),
	(68, 1, 1, 'ahora'),
	(116, 1, 1, 'ajustables'),
	(12, 1, 1, 'ajuste'),
	(82, 1, 1, 'algodon'),
	(101, 1, 1, 'alta'),
	(123, 1, 1, 'amarillo'),
	(69, 1, 1, 'ampliado'),
	(47, 1, 1, 'armario'),
	(63, 1, 1, 'atencion'),
	(81, 1, 1, 'azul'),
	(109, 1, 1, 'beige'),
	(97, 1, 1, 'blancas'),
	(89, 1, 1, 'blanco'),
	(84, 1, 1, 'blusa'),
	(87, 1, 1, 'blusas'),
	(1, 1, 1, 'camiseta'),
	(78, 1, 1, 'camisetas'),
	(70, 1, 1, 'catalogo'),
	(8, 1, 1, 'cerrado'),
	(53, 1, 1, 'chic'),
	(100, 1, 1, 'cintura'),
	(107, 1, 1, 'cinturon'),
	(76, 1, 1, 'cinturones'),
	(23, 1, 1, 'colecciones'),
	(103, 1, 1, 'colorido'),
	(14, 1, 1, 'combinala'),
	(32, 1, 1, 'combinar'),
	(13, 1, 1, 'comodo'),
	(73, 1, 1, 'complementos'),
	(60, 1, 1, 'confeccionan'),
	(5, 1, 1, 'corta'),
	(113, 1, 1, 'corto'),
	(118, 1, 1, 'debajo'),
	(119, 1, 1, 'del'),
	(6, 1, 1, 'demo'),
	(3, 1, 1, 'destenido'),
	(67, 1, 1, 'detalle'),
	(22, 1, 1, 'disenando'),
	(28, 1, 1, 'disenos'),
	(92, 1, 1, 'doble'),
	(85, 1, 1, 'drapeado'),
	(2, 1, 1, 'efecto'),
	(11, 1, 1, 'elastico'),
	(54, 1, 1, 'elegancia'),
	(112, 1, 1, 'elegante'),
	(30, 1, 1, 'elegantes'),
	(43, 1, 1, 'elemento'),
	(7, 1, 1, 'escote'),
	(91, 1, 1, 'estampado'),
	(17, 1, 1, 'estaras'),
	(56, 1, 1, 'estilo'),
	(37, 1, 1, 'evolucionado'),
	(98, 1, 1, 'falda'),
	(20, 1, 1, 'fashion'),
	(86, 1, 1, 'femenino'),
	(29, 1, 1, 'femeninos'),
	(115, 1, 1, 'finos'),
	(46, 1, 1, 'fondo'),
	(51, 1, 1, 'frescos'),
	(117, 1, 1, 'fruncido'),
	(124, 1, 1, 'gasa'),
	(38, 1, 1, 'hacia'),
	(36, 1, 1, 'han'),
	(64, 1, 1, 'hasta'),
	(71, 1, 1, 'incluir'),
	(58, 1, 1, 'inconfundible'),
	(24, 1, 1, 'increibles'),
	(45, 1, 1, 'indispensable'),
	(122, 1, 1, 'inferior'),
	(83, 1, 1, 'informal'),
	(102, 1, 1, 'informales'),
	(61, 1, 1, 'italia'),
	(55, 1, 1, 'juvenil'),
	(133, 1, 1, 'larga'),
	(39, 1, 1, 'linea'),
	(18, 1, 1, 'lista'),
	(21, 1, 1, 'lleva'),
	(50, 1, 1, 'looks'),
	(4, 1, 1, 'manga'),
	(105, 1, 1, 'mangas'),
	(79, 1, 1, 'manufacturer'),
	(26, 1, 1, 'marca'),
	(65, 1, 1, 'mas'),
	(9, 1, 1, 'material'),
	(131, 1, 1, 'media'),
	(66, 1, 1, 'minimo'),
	(77, 1, 1, 'mucho'),
	(48, 1, 1, 'mujer'),
	(80, 1, 1, 'naranja'),
	(96, 1, 1, 'negras'),
	(88, 1, 1, 'negro'),
	(104, 1, 1, 'noche'),
	(27, 1, 1, 'ofrece'),
	(16, 1, 1, 'paja'),
	(121, 1, 1, 'parte'),
	(120, 1, 1, 'pecho'),
	(132, 1, 1, 'pierna'),
	(126, 1, 1, 'poliester'),
	(41, 1, 1, 'porter'),
	(31, 1, 1, 'prendas'),
	(62, 1, 1, 'prestando'),
	(40, 1, 1, 'pret'),
	(128, 1, 1, 'pronunciado'),
	(42, 1, 1, 'que'),
	(95, 1, 1, 'rayas'),
	(106, 1, 1, 'rectas'),
	(44, 1, 1, 'resulta'),
	(49, 1, 1, 'resultado'),
	(125, 1, 1, 'rodilla'),
	(110, 1, 1, 'rosa'),
	(52, 1, 1, 'sencillos'),
	(99, 1, 1, 'skater'),
	(15, 1, 1, 'sombrero'),
	(75, 1, 1, 'sombreros'),
	(10, 1, 1, 'suave'),
	(127, 1, 1, 'tejido'),
	(34, 1, 1, 'tendencias'),
	(72, 1, 1, 'tipo'),
	(114, 1, 1, 'tirantes'),
	(59, 1, 1, 'todas'),
	(94, 1, 1, 'top'),
	(33, 1, 1, 'ultimas'),
	(57, 1, 1, 'unico'),
	(19, 1, 1, 'verano'),
	(130, 1, 1, 'verde'),
	(90, 1, 1, 'vestido'),
	(35, 1, 1, 'vestidos'),
	(111, 1, 1, 'viscosa'),
	(108, 1, 1, 'volantes'),
	(129, 1, 1, 'yprestando'),
	(74, 1, 1, 'zapatos');
/*!40000 ALTER TABLE `ps_search_word` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_sekeyword
DROP TABLE IF EXISTS `ps_sekeyword`;
CREATE TABLE IF NOT EXISTS `ps_sekeyword` (
  `id_sekeyword` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL DEFAULT '1',
  `id_shop_group` int(10) unsigned NOT NULL DEFAULT '1',
  `keyword` varchar(256) NOT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_sekeyword`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_sekeyword: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_sekeyword` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_sekeyword` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_shop
DROP TABLE IF EXISTS `ps_shop`;
CREATE TABLE IF NOT EXISTS `ps_shop` (
  `id_shop` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_group` int(11) unsigned NOT NULL,
  `name` varchar(64) NOT NULL,
  `id_category` int(11) unsigned NOT NULL DEFAULT '1',
  `id_theme` int(1) unsigned NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_shop`),
  KEY `id_shop_group` (`id_shop_group`),
  KEY `id_category` (`id_category`),
  KEY `id_theme` (`id_theme`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_shop: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_shop` DISABLE KEYS */;
INSERT INTO `ps_shop` (`id_shop`, `id_shop_group`, `name`, `id_category`, `id_theme`, `active`, `deleted`) VALUES
	(1, 1, 'Celio', 2, 2, 1, 0);
/*!40000 ALTER TABLE `ps_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_shop_group
DROP TABLE IF EXISTS `ps_shop_group`;
CREATE TABLE IF NOT EXISTS `ps_shop_group` (
  `id_shop_group` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `share_customer` tinyint(1) NOT NULL,
  `share_order` tinyint(1) NOT NULL,
  `share_stock` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_shop_group`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_shop_group: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_shop_group` DISABLE KEYS */;
INSERT INTO `ps_shop_group` (`id_shop_group`, `name`, `share_customer`, `share_order`, `share_stock`, `active`, `deleted`) VALUES
	(1, 'Default', 0, 0, 0, 1, 0);
/*!40000 ALTER TABLE `ps_shop_group` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_shop_url
DROP TABLE IF EXISTS `ps_shop_url`;
CREATE TABLE IF NOT EXISTS `ps_shop_url` (
  `id_shop_url` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(11) unsigned NOT NULL,
  `domain` varchar(150) NOT NULL,
  `domain_ssl` varchar(150) NOT NULL,
  `physical_uri` varchar(64) NOT NULL,
  `virtual_uri` varchar(64) NOT NULL,
  `main` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_shop_url`),
  UNIQUE KEY `full_shop_url` (`domain`,`physical_uri`,`virtual_uri`),
  UNIQUE KEY `full_shop_url_ssl` (`domain_ssl`,`physical_uri`,`virtual_uri`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_shop_url: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_shop_url` DISABLE KEYS */;
INSERT INTO `ps_shop_url` (`id_shop_url`, `id_shop`, `domain`, `domain_ssl`, `physical_uri`, `virtual_uri`, `main`, `active`) VALUES
	(1, 1, 'celiodev.com', 'celiodev.com', '/', '', 1, 1);
/*!40000 ALTER TABLE `ps_shop_url` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_smarty_cache
DROP TABLE IF EXISTS `ps_smarty_cache`;
CREATE TABLE IF NOT EXISTS `ps_smarty_cache` (
  `id_smarty_cache` char(40) NOT NULL,
  `name` char(40) NOT NULL,
  `cache_id` varchar(254) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `content` longtext NOT NULL,
  PRIMARY KEY (`id_smarty_cache`),
  KEY `name` (`name`),
  KEY `cache_id` (`cache_id`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_smarty_cache: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_smarty_cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_smarty_cache` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_specific_price
DROP TABLE IF EXISTS `ps_specific_price`;
CREATE TABLE IF NOT EXISTS `ps_specific_price` (
  `id_specific_price` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_specific_price_rule` int(11) unsigned NOT NULL,
  `id_cart` int(11) unsigned NOT NULL,
  `id_product` int(10) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_shop_group` int(11) unsigned NOT NULL,
  `id_currency` int(10) unsigned NOT NULL,
  `id_country` int(10) unsigned NOT NULL,
  `id_group` int(10) unsigned NOT NULL,
  `id_customer` int(10) unsigned NOT NULL,
  `id_product_attribute` int(10) unsigned NOT NULL,
  `price` decimal(20,6) NOT NULL,
  `from_quantity` mediumint(8) unsigned NOT NULL,
  `reduction` decimal(20,6) NOT NULL,
  `reduction_tax` tinyint(1) NOT NULL DEFAULT '1',
  `reduction_type` enum('amount','percentage') NOT NULL,
  `from` datetime NOT NULL,
  `to` datetime NOT NULL,
  PRIMARY KEY (`id_specific_price`),
  KEY `id_product` (`id_product`,`id_shop`,`id_currency`,`id_country`,`id_group`,`id_customer`,`from_quantity`,`from`,`to`),
  KEY `from_quantity` (`from_quantity`),
  KEY `id_specific_price_rule` (`id_specific_price_rule`),
  KEY `id_cart` (`id_cart`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_specific_price: ~2 rows (approximately)
/*!40000 ALTER TABLE `ps_specific_price` DISABLE KEYS */;
INSERT INTO `ps_specific_price` (`id_specific_price`, `id_specific_price_rule`, `id_cart`, `id_product`, `id_shop`, `id_shop_group`, `id_currency`, `id_country`, `id_group`, `id_customer`, `id_product_attribute`, `price`, `from_quantity`, `reduction`, `reduction_tax`, `reduction_type`, `from`, `to`) VALUES
	(1, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, -1.000000, 1, 0.050000, 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(2, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, -1.000000, 1, 0.200000, 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `ps_specific_price` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_specific_price_priority
DROP TABLE IF EXISTS `ps_specific_price_priority`;
CREATE TABLE IF NOT EXISTS `ps_specific_price_priority` (
  `id_specific_price_priority` int(11) NOT NULL AUTO_INCREMENT,
  `id_product` int(11) NOT NULL,
  `priority` varchar(80) NOT NULL,
  PRIMARY KEY (`id_specific_price_priority`,`id_product`),
  UNIQUE KEY `id_product` (`id_product`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_specific_price_priority: ~1 rows (approximately)
/*!40000 ALTER TABLE `ps_specific_price_priority` DISABLE KEYS */;
INSERT INTO `ps_specific_price_priority` (`id_specific_price_priority`, `id_product`, `priority`) VALUES
	(1, 1, 'id_shop;id_currency;id_country;id_group');
/*!40000 ALTER TABLE `ps_specific_price_priority` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_specific_price_rule
DROP TABLE IF EXISTS `ps_specific_price_rule`;
CREATE TABLE IF NOT EXISTS `ps_specific_price_rule` (
  `id_specific_price_rule` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_currency` int(10) unsigned NOT NULL,
  `id_country` int(10) unsigned NOT NULL,
  `id_group` int(10) unsigned NOT NULL,
  `from_quantity` mediumint(8) unsigned NOT NULL,
  `price` decimal(20,6) DEFAULT NULL,
  `reduction` decimal(20,6) NOT NULL,
  `reduction_tax` tinyint(1) NOT NULL DEFAULT '1',
  `reduction_type` enum('amount','percentage') NOT NULL,
  `from` datetime NOT NULL,
  `to` datetime NOT NULL,
  PRIMARY KEY (`id_specific_price_rule`),
  KEY `id_product` (`id_shop`,`id_currency`,`id_country`,`id_group`,`from_quantity`,`from`,`to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_specific_price_rule: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_specific_price_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_specific_price_rule` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_specific_price_rule_condition
DROP TABLE IF EXISTS `ps_specific_price_rule_condition`;
CREATE TABLE IF NOT EXISTS `ps_specific_price_rule_condition` (
  `id_specific_price_rule_condition` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_specific_price_rule_condition_group` int(11) unsigned NOT NULL,
  `type` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id_specific_price_rule_condition`),
  KEY `id_specific_price_rule_condition_group` (`id_specific_price_rule_condition_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_specific_price_rule_condition: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_specific_price_rule_condition` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_specific_price_rule_condition` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_specific_price_rule_condition_group
DROP TABLE IF EXISTS `ps_specific_price_rule_condition_group`;
CREATE TABLE IF NOT EXISTS `ps_specific_price_rule_condition_group` (
  `id_specific_price_rule_condition_group` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_specific_price_rule` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_specific_price_rule_condition_group`,`id_specific_price_rule`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_specific_price_rule_condition_group: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_specific_price_rule_condition_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_specific_price_rule_condition_group` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_state
DROP TABLE IF EXISTS `ps_state`;
CREATE TABLE IF NOT EXISTS `ps_state` (
  `id_state` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_country` int(11) unsigned NOT NULL,
  `id_zone` int(11) unsigned NOT NULL,
  `name` varchar(64) NOT NULL,
  `iso_code` varchar(7) NOT NULL,
  `tax_behavior` smallint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_state`),
  KEY `id_country` (`id_country`),
  KEY `name` (`name`),
  KEY `id_zone` (`id_zone`)
) ENGINE=InnoDB AUTO_INCREMENT=313 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_state: ~312 rows (approximately)
/*!40000 ALTER TABLE `ps_state` DISABLE KEYS */;
INSERT INTO `ps_state` (`id_state`, `id_country`, `id_zone`, `name`, `iso_code`, `tax_behavior`, `active`) VALUES
	(1, 21, 2, 'Alabama', 'AL', 0, 1),
	(2, 21, 2, 'Alaska', 'AK', 0, 1),
	(3, 21, 2, 'Arizona', 'AZ', 0, 1),
	(4, 21, 2, 'Arkansas', 'AR', 0, 1),
	(5, 21, 2, 'California', 'CA', 0, 1),
	(6, 21, 2, 'Colorado', 'CO', 0, 1),
	(7, 21, 2, 'Connecticut', 'CT', 0, 1),
	(8, 21, 2, 'Delaware', 'DE', 0, 1),
	(9, 21, 2, 'Florida', 'FL', 0, 1),
	(10, 21, 2, 'Georgia', 'GA', 0, 1),
	(11, 21, 2, 'Hawaii', 'HI', 0, 1),
	(12, 21, 2, 'Idaho', 'ID', 0, 1),
	(13, 21, 2, 'Illinois', 'IL', 0, 1),
	(14, 21, 2, 'Indiana', 'IN', 0, 1),
	(15, 21, 2, 'Iowa', 'IA', 0, 1),
	(16, 21, 2, 'Kansas', 'KS', 0, 1),
	(17, 21, 2, 'Kentucky', 'KY', 0, 1),
	(18, 21, 2, 'Louisiana', 'LA', 0, 1),
	(19, 21, 2, 'Maine', 'ME', 0, 1),
	(20, 21, 2, 'Maryland', 'MD', 0, 1),
	(21, 21, 2, 'Massachusetts', 'MA', 0, 1),
	(22, 21, 2, 'Michigan', 'MI', 0, 1),
	(23, 21, 2, 'Minnesota', 'MN', 0, 1),
	(24, 21, 2, 'Mississippi', 'MS', 0, 1),
	(25, 21, 2, 'Missouri', 'MO', 0, 1),
	(26, 21, 2, 'Montana', 'MT', 0, 1),
	(27, 21, 2, 'Nebraska', 'NE', 0, 1),
	(28, 21, 2, 'Nevada', 'NV', 0, 1),
	(29, 21, 2, 'New Hampshire', 'NH', 0, 1),
	(30, 21, 2, 'New Jersey', 'NJ', 0, 1),
	(31, 21, 2, 'New Mexico', 'NM', 0, 1),
	(32, 21, 2, 'New York', 'NY', 0, 1),
	(33, 21, 2, 'North Carolina', 'NC', 0, 1),
	(34, 21, 2, 'North Dakota', 'ND', 0, 1),
	(35, 21, 2, 'Ohio', 'OH', 0, 1),
	(36, 21, 2, 'Oklahoma', 'OK', 0, 1),
	(37, 21, 2, 'Oregon', 'OR', 0, 1),
	(38, 21, 2, 'Pennsylvania', 'PA', 0, 1),
	(39, 21, 2, 'Rhode Island', 'RI', 0, 1),
	(40, 21, 2, 'South Carolina', 'SC', 0, 1),
	(41, 21, 2, 'South Dakota', 'SD', 0, 1),
	(42, 21, 2, 'Tennessee', 'TN', 0, 1),
	(43, 21, 2, 'Texas', 'TX', 0, 1),
	(44, 21, 2, 'Utah', 'UT', 0, 1),
	(45, 21, 2, 'Vermont', 'VT', 0, 1),
	(46, 21, 2, 'Virginia', 'VA', 0, 1),
	(47, 21, 2, 'Washington', 'WA', 0, 1),
	(48, 21, 2, 'West Virginia', 'WV', 0, 1),
	(49, 21, 2, 'Wisconsin', 'WI', 0, 1),
	(50, 21, 2, 'Wyoming', 'WY', 0, 1),
	(51, 21, 2, 'Puerto Rico', 'PR', 0, 1),
	(52, 21, 2, 'US Virgin Islands', 'VI', 0, 1),
	(53, 21, 2, 'District of Columbia', 'DC', 0, 1),
	(54, 145, 2, 'Aguascalientes', 'AGS', 0, 1),
	(55, 145, 2, 'Baja California', 'BCN', 0, 1),
	(56, 145, 2, 'Baja California Sur', 'BCS', 0, 1),
	(57, 145, 2, 'Campeche', 'CAM', 0, 1),
	(58, 145, 2, 'Chiapas', 'CHP', 0, 1),
	(59, 145, 2, 'Chihuahua', 'CHH', 0, 1),
	(60, 145, 2, 'Coahuila', 'COA', 0, 1),
	(61, 145, 2, 'Colima', 'COL', 0, 1),
	(62, 145, 2, 'Distrito Federal', 'DIF', 0, 1),
	(63, 145, 2, 'Durango', 'DUR', 0, 1),
	(64, 145, 2, 'Guanajuato', 'GUA', 0, 1),
	(65, 145, 2, 'Guerrero', 'GRO', 0, 1),
	(66, 145, 2, 'Hidalgo', 'HID', 0, 1),
	(67, 145, 2, 'Jalisco', 'JAL', 0, 1),
	(68, 145, 2, 'Estado de México', 'MEX', 0, 1),
	(69, 145, 2, 'Michoacán', 'MIC', 0, 1),
	(70, 145, 2, 'Morelos', 'MOR', 0, 1),
	(71, 145, 2, 'Nayarit', 'NAY', 0, 1),
	(72, 145, 2, 'Nuevo León', 'NLE', 0, 1),
	(73, 145, 2, 'Oaxaca', 'OAX', 0, 1),
	(74, 145, 2, 'Puebla', 'PUE', 0, 1),
	(75, 145, 2, 'Querétaro', 'QUE', 0, 1),
	(76, 145, 2, 'Quintana Roo', 'ROO', 0, 1),
	(77, 145, 2, 'San Luis Potosí', 'SLP', 0, 1),
	(78, 145, 2, 'Sinaloa', 'SIN', 0, 1),
	(79, 145, 2, 'Sonora', 'SON', 0, 1),
	(80, 145, 2, 'Tabasco', 'TAB', 0, 1),
	(81, 145, 2, 'Tamaulipas', 'TAM', 0, 1),
	(82, 145, 2, 'Tlaxcala', 'TLA', 0, 1),
	(83, 145, 2, 'Veracruz', 'VER', 0, 1),
	(84, 145, 2, 'Yucatán', 'YUC', 0, 1),
	(85, 145, 2, 'Zacatecas', 'ZAC', 0, 1),
	(86, 4, 2, 'Ontario', 'ON', 0, 1),
	(87, 4, 2, 'Quebec', 'QC', 0, 1),
	(88, 4, 2, 'British Columbia', 'BC', 0, 1),
	(89, 4, 2, 'Alberta', 'AB', 0, 1),
	(90, 4, 2, 'Manitoba', 'MB', 0, 1),
	(91, 4, 2, 'Saskatchewan', 'SK', 0, 1),
	(92, 4, 2, 'Nova Scotia', 'NS', 0, 1),
	(93, 4, 2, 'New Brunswick', 'NB', 0, 1),
	(94, 4, 2, 'Newfoundland and Labrador', 'NL', 0, 1),
	(95, 4, 2, 'Prince Edward Island', 'PE', 0, 1),
	(96, 4, 2, 'Northwest Territories', 'NT', 0, 1),
	(97, 4, 2, 'Yukon', 'YT', 0, 1),
	(98, 4, 2, 'Nunavut', 'NU', 0, 1),
	(99, 44, 6, 'Buenos Aires', 'B', 0, 1),
	(100, 44, 6, 'Catamarca', 'K', 0, 1),
	(101, 44, 6, 'Chaco', 'H', 0, 1),
	(102, 44, 6, 'Chubut', 'U', 0, 1),
	(103, 44, 6, 'Ciudad de Buenos Aires', 'C', 0, 1),
	(104, 44, 6, 'Córdoba', 'X', 0, 1),
	(105, 44, 6, 'Corrientes', 'W', 0, 1),
	(106, 44, 6, 'Entre Ríos', 'E', 0, 1),
	(107, 44, 6, 'Formosa', 'P', 0, 1),
	(108, 44, 6, 'Jujuy', 'Y', 0, 1),
	(109, 44, 6, 'La Pampa', 'L', 0, 1),
	(110, 44, 6, 'La Rioja', 'F', 0, 1),
	(111, 44, 6, 'Mendoza', 'M', 0, 1),
	(112, 44, 6, 'Misiones', 'N', 0, 1),
	(113, 44, 6, 'Neuquén', 'Q', 0, 1),
	(114, 44, 6, 'Río Negro', 'R', 0, 1),
	(115, 44, 6, 'Salta', 'A', 0, 1),
	(116, 44, 6, 'San Juan', 'J', 0, 1),
	(117, 44, 6, 'San Luis', 'D', 0, 1),
	(118, 44, 6, 'Santa Cruz', 'Z', 0, 1),
	(119, 44, 6, 'Santa Fe', 'S', 0, 1),
	(120, 44, 6, 'Santiago del Estero', 'G', 0, 1),
	(121, 44, 6, 'Tierra del Fuego', 'V', 0, 1),
	(122, 44, 6, 'Tucumán', 'T', 0, 1),
	(123, 10, 1, 'Agrigento', 'AG', 0, 1),
	(124, 10, 1, 'Alessandria', 'AL', 0, 1),
	(125, 10, 1, 'Ancona', 'AN', 0, 1),
	(126, 10, 1, 'Aosta', 'AO', 0, 1),
	(127, 10, 1, 'Arezzo', 'AR', 0, 1),
	(128, 10, 1, 'Ascoli Piceno', 'AP', 0, 1),
	(129, 10, 1, 'Asti', 'AT', 0, 1),
	(130, 10, 1, 'Avellino', 'AV', 0, 1),
	(131, 10, 1, 'Bari', 'BA', 0, 1),
	(132, 10, 1, 'Barletta-Andria-Trani', 'BT', 0, 1),
	(133, 10, 1, 'Belluno', 'BL', 0, 1),
	(134, 10, 1, 'Benevento', 'BN', 0, 1),
	(135, 10, 1, 'Bergamo', 'BG', 0, 1),
	(136, 10, 1, 'Biella', 'BI', 0, 1),
	(137, 10, 1, 'Bologna', 'BO', 0, 1),
	(138, 10, 1, 'Bolzano', 'BZ', 0, 1),
	(139, 10, 1, 'Brescia', 'BS', 0, 1),
	(140, 10, 1, 'Brindisi', 'BR', 0, 1),
	(141, 10, 1, 'Cagliari', 'CA', 0, 1),
	(142, 10, 1, 'Caltanissetta', 'CL', 0, 1),
	(143, 10, 1, 'Campobasso', 'CB', 0, 1),
	(144, 10, 1, 'Carbonia-Iglesias', 'CI', 0, 1),
	(145, 10, 1, 'Caserta', 'CE', 0, 1),
	(146, 10, 1, 'Catania', 'CT', 0, 1),
	(147, 10, 1, 'Catanzaro', 'CZ', 0, 1),
	(148, 10, 1, 'Chieti', 'CH', 0, 1),
	(149, 10, 1, 'Como', 'CO', 0, 1),
	(150, 10, 1, 'Cosenza', 'CS', 0, 1),
	(151, 10, 1, 'Cremona', 'CR', 0, 1),
	(152, 10, 1, 'Crotone', 'KR', 0, 1),
	(153, 10, 1, 'Cuneo', 'CN', 0, 1),
	(154, 10, 1, 'Enna', 'EN', 0, 1),
	(155, 10, 1, 'Fermo', 'FM', 0, 1),
	(156, 10, 1, 'Ferrara', 'FE', 0, 1),
	(157, 10, 1, 'Firenze', 'FI', 0, 1),
	(158, 10, 1, 'Foggia', 'FG', 0, 1),
	(159, 10, 1, 'Forlì-Cesena', 'FC', 0, 1),
	(160, 10, 1, 'Frosinone', 'FR', 0, 1),
	(161, 10, 1, 'Genova', 'GE', 0, 1),
	(162, 10, 1, 'Gorizia', 'GO', 0, 1),
	(163, 10, 1, 'Grosseto', 'GR', 0, 1),
	(164, 10, 1, 'Imperia', 'IM', 0, 1),
	(165, 10, 1, 'Isernia', 'IS', 0, 1),
	(166, 10, 1, 'L\'Aquila', 'AQ', 0, 1),
	(167, 10, 1, 'La Spezia', 'SP', 0, 1),
	(168, 10, 1, 'Latina', 'LT', 0, 1),
	(169, 10, 1, 'Lecce', 'LE', 0, 1),
	(170, 10, 1, 'Lecco', 'LC', 0, 1),
	(171, 10, 1, 'Livorno', 'LI', 0, 1),
	(172, 10, 1, 'Lodi', 'LO', 0, 1),
	(173, 10, 1, 'Lucca', 'LU', 0, 1),
	(174, 10, 1, 'Macerata', 'MC', 0, 1),
	(175, 10, 1, 'Mantova', 'MN', 0, 1),
	(176, 10, 1, 'Massa', 'MS', 0, 1),
	(177, 10, 1, 'Matera', 'MT', 0, 1),
	(178, 10, 1, 'Medio Campidano', 'VS', 0, 1),
	(179, 10, 1, 'Messina', 'ME', 0, 1),
	(180, 10, 1, 'Milano', 'MI', 0, 1),
	(181, 10, 1, 'Modena', 'MO', 0, 1),
	(182, 10, 1, 'Monza e della Brianza', 'MB', 0, 1),
	(183, 10, 1, 'Napoli', 'NA', 0, 1),
	(184, 10, 1, 'Novara', 'NO', 0, 1),
	(185, 10, 1, 'Nuoro', 'NU', 0, 1),
	(186, 10, 1, 'Ogliastra', 'OG', 0, 1),
	(187, 10, 1, 'Olbia-Tempio', 'OT', 0, 1),
	(188, 10, 1, 'Oristano', 'OR', 0, 1),
	(189, 10, 1, 'Padova', 'PD', 0, 1),
	(190, 10, 1, 'Palermo', 'PA', 0, 1),
	(191, 10, 1, 'Parma', 'PR', 0, 1),
	(192, 10, 1, 'Pavia', 'PV', 0, 1),
	(193, 10, 1, 'Perugia', 'PG', 0, 1),
	(194, 10, 1, 'Pesaro-Urbino', 'PU', 0, 1),
	(195, 10, 1, 'Pescara', 'PE', 0, 1),
	(196, 10, 1, 'Piacenza', 'PC', 0, 1),
	(197, 10, 1, 'Pisa', 'PI', 0, 1),
	(198, 10, 1, 'Pistoia', 'PT', 0, 1),
	(199, 10, 1, 'Pordenone', 'PN', 0, 1),
	(200, 10, 1, 'Potenza', 'PZ', 0, 1),
	(201, 10, 1, 'Prato', 'PO', 0, 1),
	(202, 10, 1, 'Ragusa', 'RG', 0, 1),
	(203, 10, 1, 'Ravenna', 'RA', 0, 1),
	(204, 10, 1, 'Reggio Calabria', 'RC', 0, 1),
	(205, 10, 1, 'Reggio Emilia', 'RE', 0, 1),
	(206, 10, 1, 'Rieti', 'RI', 0, 1),
	(207, 10, 1, 'Rimini', 'RN', 0, 1),
	(208, 10, 1, 'Roma', 'RM', 0, 1),
	(209, 10, 1, 'Rovigo', 'RO', 0, 1),
	(210, 10, 1, 'Salerno', 'SA', 0, 1),
	(211, 10, 1, 'Sassari', 'SS', 0, 1),
	(212, 10, 1, 'Savona', 'SV', 0, 1),
	(213, 10, 1, 'Siena', 'SI', 0, 1),
	(214, 10, 1, 'Siracusa', 'SR', 0, 1),
	(215, 10, 1, 'Sondrio', 'SO', 0, 1),
	(216, 10, 1, 'Taranto', 'TA', 0, 1),
	(217, 10, 1, 'Teramo', 'TE', 0, 1),
	(218, 10, 1, 'Terni', 'TR', 0, 1),
	(219, 10, 1, 'Torino', 'TO', 0, 1),
	(220, 10, 1, 'Trapani', 'TP', 0, 1),
	(221, 10, 1, 'Trento', 'TN', 0, 1),
	(222, 10, 1, 'Treviso', 'TV', 0, 1),
	(223, 10, 1, 'Trieste', 'TS', 0, 1),
	(224, 10, 1, 'Udine', 'UD', 0, 1),
	(225, 10, 1, 'Varese', 'VA', 0, 1),
	(226, 10, 1, 'Venezia', 'VE', 0, 1),
	(227, 10, 1, 'Verbano-Cusio-Ossola', 'VB', 0, 1),
	(228, 10, 1, 'Vercelli', 'VC', 0, 1),
	(229, 10, 1, 'Verona', 'VR', 0, 1),
	(230, 10, 1, 'Vibo Valentia', 'VV', 0, 1),
	(231, 10, 1, 'Vicenza', 'VI', 0, 1),
	(232, 10, 1, 'Viterbo', 'VT', 0, 1),
	(233, 111, 3, 'Aceh', 'AC', 0, 1),
	(234, 111, 3, 'Bali', 'BA', 0, 1),
	(235, 111, 3, 'Bangka', 'BB', 0, 1),
	(236, 111, 3, 'Banten', 'BT', 0, 1),
	(237, 111, 3, 'Bengkulu', 'BE', 0, 1),
	(238, 111, 3, 'Central Java', 'JT', 0, 1),
	(239, 111, 3, 'Central Kalimantan', 'KT', 0, 1),
	(240, 111, 3, 'Central Sulawesi', 'ST', 0, 1),
	(241, 111, 3, 'Coat of arms of East Java', 'JI', 0, 1),
	(242, 111, 3, 'East kalimantan', 'KI', 0, 1),
	(243, 111, 3, 'East Nusa Tenggara', 'NT', 0, 1),
	(244, 111, 3, 'Lambang propinsi', 'GO', 0, 1),
	(245, 111, 3, 'Jakarta', 'JK', 0, 1),
	(246, 111, 3, 'Jambi', 'JA', 0, 1),
	(247, 111, 3, 'Lampung', 'LA', 0, 1),
	(248, 111, 3, 'Maluku', 'MA', 0, 1),
	(249, 111, 3, 'North Maluku', 'MU', 0, 1),
	(250, 111, 3, 'North Sulawesi', 'SA', 0, 1),
	(251, 111, 3, 'North Sumatra', 'SU', 0, 1),
	(252, 111, 3, 'Papua', 'PA', 0, 1),
	(253, 111, 3, 'Riau', 'RI', 0, 1),
	(254, 111, 3, 'Lambang Riau', 'KR', 0, 1),
	(255, 111, 3, 'Southeast Sulawesi', 'SG', 0, 1),
	(256, 111, 3, 'South Kalimantan', 'KS', 0, 1),
	(257, 111, 3, 'South Sulawesi', 'SN', 0, 1),
	(258, 111, 3, 'South Sumatra', 'SS', 0, 1),
	(259, 111, 3, 'West Java', 'JB', 0, 1),
	(260, 111, 3, 'West Kalimantan', 'KB', 0, 1),
	(261, 111, 3, 'West Nusa Tenggara', 'NB', 0, 1),
	(262, 111, 3, 'Lambang Provinsi Papua Barat', 'PB', 0, 1),
	(263, 111, 3, 'West Sulawesi', 'SR', 0, 1),
	(264, 111, 3, 'West Sumatra', 'SB', 0, 1),
	(265, 111, 3, 'Yogyakarta', 'YO', 0, 1),
	(266, 11, 3, 'Aichi', '23', 0, 1),
	(267, 11, 3, 'Akita', '05', 0, 1),
	(268, 11, 3, 'Aomori', '02', 0, 1),
	(269, 11, 3, 'Chiba', '12', 0, 1),
	(270, 11, 3, 'Ehime', '38', 0, 1),
	(271, 11, 3, 'Fukui', '18', 0, 1),
	(272, 11, 3, 'Fukuoka', '40', 0, 1),
	(273, 11, 3, 'Fukushima', '07', 0, 1),
	(274, 11, 3, 'Gifu', '21', 0, 1),
	(275, 11, 3, 'Gunma', '10', 0, 1),
	(276, 11, 3, 'Hiroshima', '34', 0, 1),
	(277, 11, 3, 'Hokkaido', '01', 0, 1),
	(278, 11, 3, 'Hyogo', '28', 0, 1),
	(279, 11, 3, 'Ibaraki', '08', 0, 1),
	(280, 11, 3, 'Ishikawa', '17', 0, 1),
	(281, 11, 3, 'Iwate', '03', 0, 1),
	(282, 11, 3, 'Kagawa', '37', 0, 1),
	(283, 11, 3, 'Kagoshima', '46', 0, 1),
	(284, 11, 3, 'Kanagawa', '14', 0, 1),
	(285, 11, 3, 'Kochi', '39', 0, 1),
	(286, 11, 3, 'Kumamoto', '43', 0, 1),
	(287, 11, 3, 'Kyoto', '26', 0, 1),
	(288, 11, 3, 'Mie', '24', 0, 1),
	(289, 11, 3, 'Miyagi', '04', 0, 1),
	(290, 11, 3, 'Miyazaki', '45', 0, 1),
	(291, 11, 3, 'Nagano', '20', 0, 1),
	(292, 11, 3, 'Nagasaki', '42', 0, 1),
	(293, 11, 3, 'Nara', '29', 0, 1),
	(294, 11, 3, 'Niigata', '15', 0, 1),
	(295, 11, 3, 'Oita', '44', 0, 1),
	(296, 11, 3, 'Okayama', '33', 0, 1),
	(297, 11, 3, 'Okinawa', '47', 0, 1),
	(298, 11, 3, 'Osaka', '27', 0, 1),
	(299, 11, 3, 'Saga', '41', 0, 1),
	(300, 11, 3, 'Saitama', '11', 0, 1),
	(301, 11, 3, 'Shiga', '25', 0, 1),
	(302, 11, 3, 'Shimane', '32', 0, 1),
	(303, 11, 3, 'Shizuoka', '22', 0, 1),
	(304, 11, 3, 'Tochigi', '09', 0, 1),
	(305, 11, 3, 'Tokushima', '36', 0, 1),
	(306, 11, 3, 'Tokyo', '13', 0, 1),
	(307, 11, 3, 'Tottori', '31', 0, 1),
	(308, 11, 3, 'Toyama', '16', 0, 1),
	(309, 11, 3, 'Wakayama', '30', 0, 1),
	(310, 11, 3, 'Yamagata', '06', 0, 1),
	(311, 11, 3, 'Yamaguchi', '35', 0, 1),
	(312, 11, 3, 'Yamanashi', '19', 0, 1);
/*!40000 ALTER TABLE `ps_state` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_statssearch
DROP TABLE IF EXISTS `ps_statssearch`;
CREATE TABLE IF NOT EXISTS `ps_statssearch` (
  `id_statssearch` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL DEFAULT '1',
  `id_shop_group` int(10) unsigned NOT NULL DEFAULT '1',
  `keywords` varchar(255) NOT NULL,
  `results` int(6) NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_statssearch`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_statssearch: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_statssearch` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_statssearch` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_stock
DROP TABLE IF EXISTS `ps_stock`;
CREATE TABLE IF NOT EXISTS `ps_stock` (
  `id_stock` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_warehouse` int(11) unsigned NOT NULL,
  `id_product` int(11) unsigned NOT NULL,
  `id_product_attribute` int(11) unsigned NOT NULL,
  `reference` varchar(32) NOT NULL,
  `ean13` varchar(13) DEFAULT NULL,
  `upc` varchar(12) DEFAULT NULL,
  `physical_quantity` int(11) unsigned NOT NULL,
  `usable_quantity` int(11) unsigned NOT NULL,
  `price_te` decimal(20,6) DEFAULT '0.000000',
  PRIMARY KEY (`id_stock`),
  KEY `id_warehouse` (`id_warehouse`),
  KEY `id_product` (`id_product`),
  KEY `id_product_attribute` (`id_product_attribute`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_stock: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_stock` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_stock` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_stock_available
DROP TABLE IF EXISTS `ps_stock_available`;
CREATE TABLE IF NOT EXISTS `ps_stock_available` (
  `id_stock_available` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_product` int(11) unsigned NOT NULL,
  `id_product_attribute` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  `id_shop_group` int(11) unsigned NOT NULL,
  `quantity` int(10) NOT NULL DEFAULT '0',
  `depends_on_stock` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `out_of_stock` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_stock_available`),
  UNIQUE KEY `product_sqlstock` (`id_product`,`id_product_attribute`,`id_shop`,`id_shop_group`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_group` (`id_shop_group`),
  KEY `id_product` (`id_product`),
  KEY `id_product_attribute` (`id_product_attribute`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_stock_available: ~52 rows (approximately)
/*!40000 ALTER TABLE `ps_stock_available` DISABLE KEYS */;
INSERT INTO `ps_stock_available` (`id_stock_available`, `id_product`, `id_product_attribute`, `id_shop`, `id_shop_group`, `quantity`, `depends_on_stock`, `out_of_stock`) VALUES
	(1, 1, 0, 1, 0, 1799, 0, 2),
	(2, 2, 0, 1, 0, 1799, 0, 2),
	(3, 3, 0, 1, 0, 899, 0, 2),
	(4, 4, 0, 1, 0, 900, 0, 2),
	(5, 5, 0, 1, 0, 3600, 0, 2),
	(6, 6, 0, 1, 0, 900, 0, 2),
	(7, 7, 0, 1, 0, 1800, 0, 2),
	(8, 1, 1, 1, 0, 299, 0, 2),
	(9, 1, 2, 1, 0, 300, 0, 2),
	(10, 1, 3, 1, 0, 300, 0, 2),
	(11, 1, 4, 1, 0, 300, 0, 2),
	(12, 1, 5, 1, 0, 300, 0, 2),
	(13, 1, 6, 1, 0, 300, 0, 2),
	(14, 2, 7, 1, 0, 299, 0, 2),
	(15, 2, 8, 1, 0, 300, 0, 2),
	(16, 2, 9, 1, 0, 300, 0, 2),
	(17, 2, 10, 1, 0, 300, 0, 2),
	(18, 2, 11, 1, 0, 300, 0, 2),
	(19, 2, 12, 1, 0, 300, 0, 2),
	(20, 3, 13, 1, 0, 299, 0, 2),
	(21, 3, 14, 1, 0, 300, 0, 2),
	(22, 3, 15, 1, 0, 300, 0, 2),
	(23, 4, 16, 1, 0, 300, 0, 2),
	(24, 4, 17, 1, 0, 300, 0, 2),
	(25, 4, 18, 1, 0, 300, 0, 2),
	(26, 5, 19, 1, 0, 300, 0, 2),
	(27, 5, 20, 1, 0, 300, 0, 2),
	(28, 5, 21, 1, 0, 300, 0, 2),
	(29, 5, 22, 1, 0, 300, 0, 2),
	(30, 5, 23, 1, 0, 300, 0, 2),
	(31, 5, 24, 1, 0, 300, 0, 2),
	(32, 5, 25, 1, 0, 300, 0, 2),
	(33, 5, 26, 1, 0, 300, 0, 2),
	(34, 5, 27, 1, 0, 300, 0, 2),
	(35, 5, 28, 1, 0, 300, 0, 2),
	(36, 5, 29, 1, 0, 300, 0, 2),
	(37, 5, 30, 1, 0, 300, 0, 2),
	(38, 6, 31, 1, 0, 300, 0, 2),
	(39, 6, 32, 1, 0, 300, 0, 2),
	(40, 6, 33, 1, 0, 300, 0, 2),
	(41, 7, 34, 1, 0, 300, 0, 2),
	(42, 7, 35, 1, 0, 300, 0, 2),
	(43, 7, 36, 1, 0, 300, 0, 2),
	(44, 7, 37, 1, 0, 300, 0, 2),
	(45, 7, 38, 1, 0, 300, 0, 2),
	(46, 7, 39, 1, 0, 300, 0, 2),
	(47, 6, 40, 1, 0, 0, 0, 2),
	(48, 6, 41, 1, 0, 0, 0, 2),
	(49, 6, 42, 1, 0, 0, 0, 2),
	(50, 4, 43, 1, 0, 0, 0, 2),
	(51, 4, 44, 1, 0, 0, 0, 2),
	(52, 4, 45, 1, 0, 0, 0, 2);
/*!40000 ALTER TABLE `ps_stock_available` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_stock_mvt
DROP TABLE IF EXISTS `ps_stock_mvt`;
CREATE TABLE IF NOT EXISTS `ps_stock_mvt` (
  `id_stock_mvt` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_stock` int(11) unsigned NOT NULL,
  `id_order` int(11) unsigned DEFAULT NULL,
  `id_supply_order` int(11) unsigned DEFAULT NULL,
  `id_stock_mvt_reason` int(11) unsigned NOT NULL,
  `id_employee` int(11) unsigned NOT NULL,
  `employee_lastname` varchar(32) DEFAULT '',
  `employee_firstname` varchar(32) DEFAULT '',
  `physical_quantity` int(11) unsigned NOT NULL,
  `date_add` datetime NOT NULL,
  `sign` tinyint(1) NOT NULL DEFAULT '1',
  `price_te` decimal(20,6) DEFAULT '0.000000',
  `last_wa` decimal(20,6) DEFAULT '0.000000',
  `current_wa` decimal(20,6) DEFAULT '0.000000',
  `referer` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_stock_mvt`),
  KEY `id_stock` (`id_stock`),
  KEY `id_stock_mvt_reason` (`id_stock_mvt_reason`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_stock_mvt: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_stock_mvt` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_stock_mvt` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_stock_mvt_reason
DROP TABLE IF EXISTS `ps_stock_mvt_reason`;
CREATE TABLE IF NOT EXISTS `ps_stock_mvt_reason` (
  `id_stock_mvt_reason` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sign` tinyint(1) NOT NULL DEFAULT '1',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_stock_mvt_reason`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_stock_mvt_reason: ~8 rows (approximately)
/*!40000 ALTER TABLE `ps_stock_mvt_reason` DISABLE KEYS */;
INSERT INTO `ps_stock_mvt_reason` (`id_stock_mvt_reason`, `sign`, `date_add`, `date_upd`, `deleted`) VALUES
	(1, 1, '2015-05-08 13:59:53', '2015-05-08 13:59:53', 0),
	(2, -1, '2015-05-08 13:59:53', '2015-05-08 13:59:53', 0),
	(3, -1, '2015-05-08 13:59:53', '2015-05-08 13:59:53', 0),
	(4, -1, '2015-05-08 13:59:53', '2015-05-08 13:59:53', 0),
	(5, 1, '2015-05-08 13:59:53', '2015-05-08 13:59:53', 0),
	(6, -1, '2015-05-08 13:59:54', '2015-05-08 13:59:54', 0),
	(7, 1, '2015-05-08 13:59:54', '2015-05-08 13:59:54', 0),
	(8, 1, '2015-05-08 13:59:54', '2015-05-08 13:59:54', 0);
/*!40000 ALTER TABLE `ps_stock_mvt_reason` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_stock_mvt_reason_lang
DROP TABLE IF EXISTS `ps_stock_mvt_reason_lang`;
CREATE TABLE IF NOT EXISTS `ps_stock_mvt_reason_lang` (
  `id_stock_mvt_reason` int(11) unsigned NOT NULL,
  `id_lang` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id_stock_mvt_reason`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_stock_mvt_reason_lang: ~8 rows (approximately)
/*!40000 ALTER TABLE `ps_stock_mvt_reason_lang` DISABLE KEYS */;
INSERT INTO `ps_stock_mvt_reason_lang` (`id_stock_mvt_reason`, `id_lang`, `name`) VALUES
	(1, 1, 'Aumentar'),
	(2, 1, 'Disminuir'),
	(3, 1, 'Pedido del cliente'),
	(4, 1, 'Regulación tras inventario'),
	(5, 1, 'Regulación tras inventario'),
	(6, 1, 'Transferir a otro almacén'),
	(7, 1, 'Transferir desde otro almacén'),
	(8, 1, 'Pedido de suministros');
/*!40000 ALTER TABLE `ps_stock_mvt_reason_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_store
DROP TABLE IF EXISTS `ps_store`;
CREATE TABLE IF NOT EXISTS `ps_store` (
  `id_store` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_country` int(10) unsigned NOT NULL,
  `id_state` int(10) unsigned DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `address1` varchar(128) NOT NULL,
  `address2` varchar(128) DEFAULT NULL,
  `city` varchar(64) NOT NULL,
  `postcode` varchar(12) NOT NULL,
  `latitude` decimal(13,8) DEFAULT NULL,
  `longitude` decimal(13,8) DEFAULT NULL,
  `hours` varchar(254) DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `fax` varchar(16) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `note` text,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_store`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_store: ~5 rows (approximately)
/*!40000 ALTER TABLE `ps_store` DISABLE KEYS */;
INSERT INTO `ps_store` (`id_store`, `id_country`, `id_state`, `name`, `address1`, `address2`, `city`, `postcode`, `latitude`, `longitude`, `hours`, `phone`, `fax`, `email`, `note`, `active`, `date_add`, `date_upd`) VALUES
	(1, 21, 9, 'Dade County', '3030 SW 8th St Miami', '', 'Miami', ' 33135', 25.76500500, -80.24379700, 'a:7:{i:0;s:13:"09:00 - 19:00";i:1;s:13:"09:00 - 19:00";i:2;s:13:"09:00 - 19:00";i:3;s:13:"09:00 - 19:00";i:4;s:13:"09:00 - 19:00";i:5;s:13:"10:00 - 16:00";i:6;s:13:"10:00 - 16:00";}', '', '', '', '', 1, '2015-05-08 14:00:24', '2015-05-08 14:00:24'),
	(2, 21, 9, 'E Fort Lauderdale', '1000 Northeast 4th Ave Fort Lauderdale', '', 'Miami', ' 33304', 26.13793600, -80.13943500, 'a:7:{i:0;s:13:"09:00 - 19:00";i:1;s:13:"09:00 - 19:00";i:2;s:13:"09:00 - 19:00";i:3;s:13:"09:00 - 19:00";i:4;s:13:"09:00 - 19:00";i:5;s:13:"10:00 - 16:00";i:6;s:13:"10:00 - 16:00";}', '', '', '', '', 1, '2015-05-08 14:00:24', '2015-05-08 14:00:24'),
	(3, 21, 9, 'Pembroke Pines', '11001 Pines Blvd Pembroke Pines', '', 'Miami', '33026', 26.00998700, -80.29447200, 'a:7:{i:0;s:13:"09:00 - 19:00";i:1;s:13:"09:00 - 19:00";i:2;s:13:"09:00 - 19:00";i:3;s:13:"09:00 - 19:00";i:4;s:13:"09:00 - 19:00";i:5;s:13:"10:00 - 16:00";i:6;s:13:"10:00 - 16:00";}', '', '', '', '', 1, '2015-05-08 14:00:24', '2015-05-08 14:00:24'),
	(4, 21, 9, 'Coconut Grove', '2999 SW 32nd Avenue', '', 'Miami', ' 33133', 25.73629600, -80.24479700, 'a:7:{i:0;s:13:"09:00 - 19:00";i:1;s:13:"09:00 - 19:00";i:2;s:13:"09:00 - 19:00";i:3;s:13:"09:00 - 19:00";i:4;s:13:"09:00 - 19:00";i:5;s:13:"10:00 - 16:00";i:6;s:13:"10:00 - 16:00";}', '', '', '', '', 1, '2015-05-08 14:00:24', '2015-05-08 14:00:24'),
	(5, 21, 9, 'N Miami/Biscayne', '12055 Biscayne Blvd', '', 'Miami', '33181', 25.88674000, -80.16329200, 'a:7:{i:0;s:13:"09:00 - 19:00";i:1;s:13:"09:00 - 19:00";i:2;s:13:"09:00 - 19:00";i:3;s:13:"09:00 - 19:00";i:4;s:13:"09:00 - 19:00";i:5;s:13:"10:00 - 16:00";i:6;s:13:"10:00 - 16:00";}', '', '', '', '', 1, '2015-05-08 14:00:24', '2015-05-08 14:00:24');
/*!40000 ALTER TABLE `ps_store` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_store_shop
DROP TABLE IF EXISTS `ps_store_shop`;
CREATE TABLE IF NOT EXISTS `ps_store_shop` (
  `id_store` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_store`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_store_shop: ~5 rows (approximately)
/*!40000 ALTER TABLE `ps_store_shop` DISABLE KEYS */;
INSERT INTO `ps_store_shop` (`id_store`, `id_shop`) VALUES
	(1, 1),
	(2, 1),
	(3, 1),
	(4, 1),
	(5, 1);
/*!40000 ALTER TABLE `ps_store_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_supplier
DROP TABLE IF EXISTS `ps_supplier`;
CREATE TABLE IF NOT EXISTS `ps_supplier` (
  `id_supplier` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_supplier`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_supplier: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_supplier` DISABLE KEYS */;
INSERT INTO `ps_supplier` (`id_supplier`, `name`, `date_add`, `date_upd`, `active`) VALUES
	(1, 'Fashion Supplier', '2015-05-08 14:00:20', '2015-05-08 14:00:20', 1);
/*!40000 ALTER TABLE `ps_supplier` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_supplier_lang
DROP TABLE IF EXISTS `ps_supplier_lang`;
CREATE TABLE IF NOT EXISTS `ps_supplier_lang` (
  `id_supplier` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `description` text,
  `meta_title` varchar(128) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_supplier`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_supplier_lang: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_supplier_lang` DISABLE KEYS */;
INSERT INTO `ps_supplier_lang` (`id_supplier`, `id_lang`, `description`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
	(1, 1, '', '', '', '');
/*!40000 ALTER TABLE `ps_supplier_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_supplier_shop
DROP TABLE IF EXISTS `ps_supplier_shop`;
CREATE TABLE IF NOT EXISTS `ps_supplier_shop` (
  `id_supplier` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_supplier`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_supplier_shop: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_supplier_shop` DISABLE KEYS */;
INSERT INTO `ps_supplier_shop` (`id_supplier`, `id_shop`) VALUES
	(1, 1);
/*!40000 ALTER TABLE `ps_supplier_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_supply_order
DROP TABLE IF EXISTS `ps_supply_order`;
CREATE TABLE IF NOT EXISTS `ps_supply_order` (
  `id_supply_order` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_supplier` int(11) unsigned NOT NULL,
  `supplier_name` varchar(64) NOT NULL,
  `id_lang` int(11) unsigned NOT NULL,
  `id_warehouse` int(11) unsigned NOT NULL,
  `id_supply_order_state` int(11) unsigned NOT NULL,
  `id_currency` int(11) unsigned NOT NULL,
  `id_ref_currency` int(11) unsigned NOT NULL,
  `reference` varchar(64) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `date_delivery_expected` datetime DEFAULT NULL,
  `total_te` decimal(20,6) DEFAULT '0.000000',
  `total_with_discount_te` decimal(20,6) DEFAULT '0.000000',
  `total_tax` decimal(20,6) DEFAULT '0.000000',
  `total_ti` decimal(20,6) DEFAULT '0.000000',
  `discount_rate` decimal(20,6) DEFAULT '0.000000',
  `discount_value_te` decimal(20,6) DEFAULT '0.000000',
  `is_template` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id_supply_order`),
  KEY `id_supplier` (`id_supplier`),
  KEY `id_warehouse` (`id_warehouse`),
  KEY `reference` (`reference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_supply_order: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_supply_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_supply_order` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_supply_order_detail
DROP TABLE IF EXISTS `ps_supply_order_detail`;
CREATE TABLE IF NOT EXISTS `ps_supply_order_detail` (
  `id_supply_order_detail` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_supply_order` int(11) unsigned NOT NULL,
  `id_currency` int(11) unsigned NOT NULL,
  `id_product` int(11) unsigned NOT NULL,
  `id_product_attribute` int(11) unsigned NOT NULL,
  `reference` varchar(32) NOT NULL,
  `supplier_reference` varchar(32) NOT NULL,
  `name` varchar(128) NOT NULL,
  `ean13` varchar(13) DEFAULT NULL,
  `upc` varchar(12) DEFAULT NULL,
  `exchange_rate` decimal(20,6) DEFAULT '0.000000',
  `unit_price_te` decimal(20,6) DEFAULT '0.000000',
  `quantity_expected` int(11) unsigned NOT NULL,
  `quantity_received` int(11) unsigned NOT NULL,
  `price_te` decimal(20,6) DEFAULT '0.000000',
  `discount_rate` decimal(20,6) DEFAULT '0.000000',
  `discount_value_te` decimal(20,6) DEFAULT '0.000000',
  `price_with_discount_te` decimal(20,6) DEFAULT '0.000000',
  `tax_rate` decimal(20,6) DEFAULT '0.000000',
  `tax_value` decimal(20,6) DEFAULT '0.000000',
  `price_ti` decimal(20,6) DEFAULT '0.000000',
  `tax_value_with_order_discount` decimal(20,6) DEFAULT '0.000000',
  `price_with_order_discount_te` decimal(20,6) DEFAULT '0.000000',
  PRIMARY KEY (`id_supply_order_detail`),
  KEY `id_supply_order` (`id_supply_order`),
  KEY `id_product` (`id_product`),
  KEY `id_product_attribute` (`id_product_attribute`),
  KEY `id_product_product_attribute` (`id_product`,`id_product_attribute`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_supply_order_detail: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_supply_order_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_supply_order_detail` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_supply_order_history
DROP TABLE IF EXISTS `ps_supply_order_history`;
CREATE TABLE IF NOT EXISTS `ps_supply_order_history` (
  `id_supply_order_history` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_supply_order` int(11) unsigned NOT NULL,
  `id_employee` int(11) unsigned NOT NULL,
  `employee_lastname` varchar(32) DEFAULT '',
  `employee_firstname` varchar(32) DEFAULT '',
  `id_state` int(11) unsigned NOT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_supply_order_history`),
  KEY `id_supply_order` (`id_supply_order`),
  KEY `id_employee` (`id_employee`),
  KEY `id_state` (`id_state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_supply_order_history: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_supply_order_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_supply_order_history` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_supply_order_receipt_history
DROP TABLE IF EXISTS `ps_supply_order_receipt_history`;
CREATE TABLE IF NOT EXISTS `ps_supply_order_receipt_history` (
  `id_supply_order_receipt_history` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_supply_order_detail` int(11) unsigned NOT NULL,
  `id_employee` int(11) unsigned NOT NULL,
  `employee_lastname` varchar(32) DEFAULT '',
  `employee_firstname` varchar(32) DEFAULT '',
  `id_supply_order_state` int(11) unsigned NOT NULL,
  `quantity` int(11) unsigned NOT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_supply_order_receipt_history`),
  KEY `id_supply_order_detail` (`id_supply_order_detail`),
  KEY `id_supply_order_state` (`id_supply_order_state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_supply_order_receipt_history: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_supply_order_receipt_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_supply_order_receipt_history` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_supply_order_state
DROP TABLE IF EXISTS `ps_supply_order_state`;
CREATE TABLE IF NOT EXISTS `ps_supply_order_state` (
  `id_supply_order_state` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `delivery_note` tinyint(1) NOT NULL DEFAULT '0',
  `editable` tinyint(1) NOT NULL DEFAULT '0',
  `receipt_state` tinyint(1) NOT NULL DEFAULT '0',
  `pending_receipt` tinyint(1) NOT NULL DEFAULT '0',
  `enclosed` tinyint(1) NOT NULL DEFAULT '0',
  `color` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id_supply_order_state`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_supply_order_state: ~6 rows (approximately)
/*!40000 ALTER TABLE `ps_supply_order_state` DISABLE KEYS */;
INSERT INTO `ps_supply_order_state` (`id_supply_order_state`, `delivery_note`, `editable`, `receipt_state`, `pending_receipt`, `enclosed`, `color`) VALUES
	(1, 0, 1, 0, 0, 0, '#faab00'),
	(2, 1, 0, 0, 0, 0, '#273cff'),
	(3, 0, 0, 0, 1, 0, '#ff37f5'),
	(4, 0, 0, 1, 1, 0, '#ff3e33'),
	(5, 0, 0, 1, 0, 1, '#00d60c'),
	(6, 0, 0, 0, 0, 1, '#666666');
/*!40000 ALTER TABLE `ps_supply_order_state` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_supply_order_state_lang
DROP TABLE IF EXISTS `ps_supply_order_state_lang`;
CREATE TABLE IF NOT EXISTS `ps_supply_order_state_lang` (
  `id_supply_order_state` int(11) unsigned NOT NULL,
  `id_lang` int(11) unsigned NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_supply_order_state`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_supply_order_state_lang: ~6 rows (approximately)
/*!40000 ALTER TABLE `ps_supply_order_state_lang` DISABLE KEYS */;
INSERT INTO `ps_supply_order_state_lang` (`id_supply_order_state`, `id_lang`, `name`) VALUES
	(1, 1, '1 - Creación en curso'),
	(2, 1, '2 - Pedido validado'),
	(3, 1, '3 - Pendiente de recepción'),
	(4, 1, '4 - Pedido recibido parcialmente'),
	(5, 1, '5 - Pedido recibido completamente'),
	(6, 1, '6 - Pedido cancelado');
/*!40000 ALTER TABLE `ps_supply_order_state_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_tab
DROP TABLE IF EXISTS `ps_tab`;
CREATE TABLE IF NOT EXISTS `ps_tab` (
  `id_tab` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) NOT NULL,
  `class_name` varchar(64) NOT NULL,
  `module` varchar(64) DEFAULT NULL,
  `position` int(10) unsigned NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `hide_host_mode` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_tab`),
  KEY `class_name` (`class_name`),
  KEY `id_parent` (`id_parent`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_tab: ~115 rows (approximately)
/*!40000 ALTER TABLE `ps_tab` DISABLE KEYS */;
INSERT INTO `ps_tab` (`id_tab`, `id_parent`, `class_name`, `module`, `position`, `active`, `hide_host_mode`) VALUES
	(1, 0, 'AdminDashboard', '', 0, 1, 0),
	(2, -1, 'AdminCms', '', 0, 1, 0),
	(3, -1, 'AdminCmsCategories', '', 1, 1, 0),
	(4, -1, 'AdminAttributeGenerator', '', 2, 1, 0),
	(5, -1, 'AdminSearch', '', 3, 1, 0),
	(6, -1, 'AdminLogin', '', 4, 1, 0),
	(7, -1, 'AdminShop', '', 5, 1, 0),
	(8, -1, 'AdminShopUrl', '', 6, 1, 0),
	(9, 0, 'AdminCatalog', '', 1, 1, 0),
	(10, 0, 'AdminParentOrders', '', 2, 1, 0),
	(11, 0, 'AdminParentCustomer', '', 3, 1, 0),
	(12, 0, 'AdminPriceRule', '', 4, 1, 0),
	(13, 0, 'AdminParentModules', '', 5, 1, 0),
	(14, 0, 'AdminParentShipping', '', 6, 1, 0),
	(15, 0, 'AdminParentLocalization', '', 7, 1, 0),
	(16, 0, 'AdminParentPreferences', '', 8, 1, 0),
	(17, 0, 'AdminTools', '', 9, 1, 0),
	(18, 0, 'AdminAdmin', '', 10, 1, 0),
	(19, 0, 'AdminParentStats', '', 11, 1, 0),
	(20, 0, 'AdminStock', '', 12, 1, 0),
	(21, 9, 'AdminProducts', '', 0, 1, 0),
	(22, 9, 'AdminCategories', '', 1, 1, 0),
	(23, 9, 'AdminTracking', '', 2, 1, 0),
	(24, 9, 'AdminAttributesGroups', '', 3, 1, 0),
	(25, 9, 'AdminFeatures', '', 4, 1, 0),
	(26, 9, 'AdminManufacturers', '', 5, 1, 0),
	(27, 9, 'AdminSuppliers', '', 6, 1, 0),
	(28, 9, 'AdminTags', '', 7, 1, 0),
	(29, 9, 'AdminAttachments', '', 8, 1, 0),
	(30, 10, 'AdminOrders', '', 0, 1, 0),
	(31, 10, 'AdminInvoices', '', 1, 1, 0),
	(32, 10, 'AdminReturn', '', 2, 1, 0),
	(33, 10, 'AdminDeliverySlip', '', 3, 1, 0),
	(34, 10, 'AdminSlip', '', 4, 1, 0),
	(35, 10, 'AdminStatuses', '', 5, 1, 0),
	(36, 10, 'AdminOrderMessage', '', 6, 1, 0),
	(37, 11, 'AdminCustomers', '', 0, 1, 0),
	(38, 11, 'AdminAddresses', '', 1, 1, 0),
	(39, 11, 'AdminGroups', '', 2, 1, 0),
	(40, 11, 'AdminCarts', '', 3, 1, 0),
	(41, 11, 'AdminCustomerThreads', '', 4, 1, 0),
	(42, 11, 'AdminContacts', '', 5, 1, 0),
	(43, 11, 'AdminGenders', '', 6, 1, 0),
	(44, 11, 'AdminOutstanding', '', 7, 0, 0),
	(45, 12, 'AdminCartRules', '', 0, 1, 0),
	(46, 12, 'AdminSpecificPriceRule', '', 1, 1, 0),
	(47, 12, 'AdminMarketing', '', 2, 1, 0),
	(48, 14, 'AdminCarriers', '', 0, 1, 0),
	(49, 14, 'AdminShipping', '', 1, 1, 0),
	(50, 14, 'AdminCarrierWizard', '', 2, 1, 0),
	(51, 15, 'AdminLocalization', '', 0, 1, 0),
	(52, 15, 'AdminLanguages', '', 1, 1, 0),
	(53, 15, 'AdminZones', '', 2, 1, 0),
	(54, 15, 'AdminCountries', '', 3, 1, 0),
	(55, 15, 'AdminStates', '', 4, 1, 0),
	(56, 15, 'AdminCurrencies', '', 5, 1, 0),
	(57, 15, 'AdminTaxes', '', 6, 1, 0),
	(58, 15, 'AdminTaxRulesGroup', '', 7, 1, 0),
	(59, 15, 'AdminTranslations', '', 8, 1, 0),
	(60, 13, 'AdminModules', '', 0, 1, 0),
	(61, 13, 'AdminAddonsCatalog', '', 1, 1, 0),
	(62, 13, 'AdminModulesPositions', '', 2, 1, 0),
	(63, 13, 'AdminPayment', '', 3, 1, 0),
	(64, 16, 'AdminPreferences', '', 0, 1, 0),
	(65, 16, 'AdminOrderPreferences', '', 1, 1, 0),
	(66, 16, 'AdminPPreferences', '', 2, 1, 0),
	(67, 16, 'AdminCustomerPreferences', '', 3, 1, 0),
	(68, 16, 'AdminThemes', '', 4, 1, 0),
	(69, 16, 'AdminMeta', '', 5, 1, 0),
	(70, 16, 'AdminCmsContent', '', 6, 1, 0),
	(71, 16, 'AdminImages', '', 7, 1, 0),
	(72, 16, 'AdminStores', '', 8, 1, 0),
	(73, 16, 'AdminSearchConf', '', 9, 1, 0),
	(74, 16, 'AdminMaintenance', '', 10, 1, 0),
	(75, 16, 'AdminGeolocation', '', 11, 1, 0),
	(76, 17, 'AdminInformation', '', 0, 1, 0),
	(77, 17, 'AdminPerformance', '', 1, 1, 0),
	(78, 17, 'AdminEmails', '', 2, 1, 0),
	(79, 17, 'AdminShopGroup', '', 3, 0, 0),
	(80, 17, 'AdminImport', '', 4, 1, 0),
	(81, 17, 'AdminBackup', '', 5, 1, 0),
	(82, 17, 'AdminRequestSql', '', 6, 1, 0),
	(83, 17, 'AdminLogs', '', 7, 1, 0),
	(84, 17, 'AdminWebservice', '', 8, 1, 0),
	(85, 18, 'AdminAdminPreferences', '', 0, 1, 0),
	(86, 18, 'AdminQuickAccesses', '', 1, 1, 0),
	(87, 18, 'AdminEmployees', '', 2, 1, 0),
	(88, 18, 'AdminProfiles', '', 3, 1, 0),
	(89, 18, 'AdminAccess', '', 4, 1, 0),
	(90, 18, 'AdminTabs', '', 5, 1, 0),
	(91, 19, 'AdminStats', '', 0, 1, 0),
	(92, 19, 'AdminSearchEngines', '', 1, 1, 0),
	(93, 19, 'AdminReferrers', '', 2, 1, 0),
	(94, 20, 'AdminWarehouses', '', 0, 1, 0),
	(95, 20, 'AdminStockManagement', '', 1, 1, 0),
	(96, 20, 'AdminStockMvt', '', 2, 1, 0),
	(97, 20, 'AdminStockInstantState', '', 3, 1, 0),
	(98, 20, 'AdminStockCover', '', 4, 1, 0),
	(99, 20, 'AdminSupplyOrders', '', 5, 1, 0),
	(100, 20, 'AdminStockConfiguration', '', 6, 1, 0),
	(101, -1, 'AdminBlockCategories', 'blockcategories', 7, 1, 0),
	(102, -1, 'AdminDashgoals', 'dashgoals', 8, 1, 0),
	(103, -1, 'AdminThemeConfigurator', 'themeconfigurator', 9, 1, 0),
	(104, 18, 'AdminGamification', 'gamification', 6, 1, 0),
	(105, -1, 'AdminCronJobs', 'cronjobs', 10, 1, 0),
	(106, 99999, 'AdminOnboarding', 'onboarding', 1, 1, 0),
	(107, 0, 'AdminLeoblogManagement', 'leoblog', 13, 1, 0),
	(108, 107, 'AdminLeoblogDashboard', 'leoblog', 1, 1, 0),
	(109, 107, 'AdminLeoblogCategories', 'leoblog', 2, 1, 0),
	(110, 107, 'AdminLeoblogBlogs', 'leoblog', 3, 1, 0),
	(111, 107, 'AdminLeoblogComments', 'leoblog', 4, 1, 0),
	(112, -1, 'AdminLeoSliderLayer', 'leosliderlayer', 11, 1, 0),
	(113, 13, 'AdminPtspagebuilderProfile', 'ptspagebuilder', 4, 1, 0),
	(114, 13, 'AdminPtspagebuilderFooter', 'ptspagebuilder', 5, 1, 0),
	(115, 13, 'AdminPtspagebuilderImage', 'ptspagebuilder', 6, 1, 0);
/*!40000 ALTER TABLE `ps_tab` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_tab_advice
DROP TABLE IF EXISTS `ps_tab_advice`;
CREATE TABLE IF NOT EXISTS `ps_tab_advice` (
  `id_tab` int(11) NOT NULL,
  `id_advice` int(11) NOT NULL,
  PRIMARY KEY (`id_tab`,`id_advice`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_tab_advice: ~63 rows (approximately)
/*!40000 ALTER TABLE `ps_tab_advice` DISABLE KEYS */;
INSERT INTO `ps_tab_advice` (`id_tab`, `id_advice`) VALUES
	(0, 4),
	(0, 19),
	(1, 3),
	(1, 6),
	(1, 7),
	(1, 11),
	(1, 12),
	(1, 18),
	(1, 22),
	(1, 23),
	(1, 24),
	(1, 30),
	(1, 31),
	(1, 32),
	(1, 33),
	(1, 36),
	(1, 38),
	(9, 13),
	(9, 34),
	(21, 1),
	(21, 5),
	(21, 9),
	(21, 10),
	(21, 16),
	(21, 20),
	(21, 21),
	(21, 26),
	(21, 27),
	(21, 36),
	(22, 1),
	(22, 16),
	(22, 36),
	(30, 9),
	(30, 26),
	(30, 35),
	(31, 1),
	(31, 16),
	(37, 8),
	(37, 13),
	(37, 25),
	(37, 34),
	(37, 35),
	(38, 14),
	(38, 15),
	(39, 13),
	(39, 34),
	(41, 8),
	(41, 14),
	(41, 15),
	(41, 25),
	(41, 37),
	(59, 2),
	(59, 17),
	(60, 36),
	(63, 28),
	(68, 36),
	(70, 36),
	(72, 1),
	(72, 16),
	(78, 29),
	(80, 36),
	(87, 29),
	(91, 37);
/*!40000 ALTER TABLE `ps_tab_advice` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_tab_lang
DROP TABLE IF EXISTS `ps_tab_lang`;
CREATE TABLE IF NOT EXISTS `ps_tab_lang` (
  `id_tab` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id_tab`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_tab_lang: ~115 rows (approximately)
/*!40000 ALTER TABLE `ps_tab_lang` DISABLE KEYS */;
INSERT INTO `ps_tab_lang` (`id_tab`, `id_lang`, `name`) VALUES
	(1, 1, 'Inicio'),
	(2, 1, 'Páginas estáticas/CMS'),
	(3, 1, 'Categorías CMS'),
	(4, 1, 'Generador de Combinaciones'),
	(5, 1, 'Buscar'),
	(6, 1, 'Identifíquese'),
	(7, 1, 'Tiendas'),
	(8, 1, 'URLs/Direcciones de la tienda'),
	(9, 1, 'Catálogo'),
	(10, 1, 'Pedidos'),
	(11, 1, 'Clientes'),
	(12, 1, 'Reglas de Descuentos'),
	(13, 1, 'Módulos'),
	(14, 1, 'Transporte'),
	(15, 1, 'Localización'),
	(16, 1, 'Preferencias'),
	(17, 1, 'Parámetros Avanzados'),
	(18, 1, 'Administración'),
	(19, 1, 'Estadísticas'),
	(20, 1, 'Existencias'),
	(21, 1, 'Productos'),
	(22, 1, 'Categorías'),
	(23, 1, 'Monitoreo'),
	(24, 1, 'Atributos de productos'),
	(25, 1, 'Características de productos'),
	(26, 1, 'Fabricantes'),
	(27, 1, 'Proveedores'),
	(28, 1, 'Etiquetas'),
	(29, 1, 'Adjuntos'),
	(30, 1, 'Pedidos'),
	(31, 1, 'Facturas'),
	(32, 1, 'Devoluciones de mercancía'),
	(33, 1, 'Albaranes de entrega'),
	(34, 1, 'Notas de Crédito'),
	(35, 1, 'Estados'),
	(36, 1, 'Mensajes de Pedidos'),
	(37, 1, 'Clientes'),
	(38, 1, 'Direcciones'),
	(39, 1, 'Grupos'),
	(40, 1, 'Carros de compra'),
	(41, 1, 'Servicio al cliente'),
	(42, 1, 'Contacto'),
	(43, 1, 'Tratamientos'),
	(44, 1, 'Sorprendente'),
	(45, 1, 'Vales descuento'),
	(46, 1, 'Reglas del catálogo'),
	(47, 1, 'Márketing'),
	(48, 1, 'Transportistas'),
	(49, 1, 'Preferencias'),
	(50, 1, 'Transportista'),
	(51, 1, 'Localización'),
	(52, 1, 'Idiomas'),
	(53, 1, 'Zona'),
	(54, 1, 'Países'),
	(55, 1, 'Provincias'),
	(56, 1, 'Monedas'),
	(57, 1, 'Impuestos'),
	(58, 1, 'Reglas de impuestos'),
	(59, 1, 'Traducciones'),
	(60, 1, 'Módulos'),
	(61, 1, 'Catálogo de Módulos y Temas'),
	(62, 1, 'Posiciones de los módulos'),
	(63, 1, 'Pago'),
	(64, 1, 'Configuración'),
	(65, 1, 'Pedidos'),
	(66, 1, 'Productos'),
	(67, 1, 'Clientes'),
	(68, 1, 'Temas'),
	(69, 1, 'SEO + URLs'),
	(70, 1, 'CMS'),
	(71, 1, 'Imágenes'),
	(72, 1, 'Contactos de la tienda'),
	(73, 1, 'Buscar'),
	(74, 1, 'Mantenimiento'),
	(75, 1, 'Geolocalización'),
	(76, 1, 'Información'),
	(77, 1, 'Rendimiento'),
	(78, 1, 'Correo Electrónico'),
	(79, 1, 'Multitienda'),
	(80, 1, 'Importar CSV'),
	(81, 1, 'Respaldar BD'),
	(82, 1, 'Gestor SQL'),
	(83, 1, 'Registros/Logs'),
	(84, 1, 'Webservice'),
	(85, 1, 'Preferencias'),
	(86, 1, 'Acceso rápido'),
	(87, 1, 'Empleados'),
	(88, 1, 'Perfiles'),
	(89, 1, 'Permisos'),
	(90, 1, 'Menús'),
	(91, 1, 'Estadísticas'),
	(92, 1, 'Motores de búsqueda'),
	(93, 1, 'Afiliados'),
	(94, 1, 'Almacenes'),
	(95, 1, 'Gestión de existencias'),
	(96, 1, 'Movimiento de Stock'),
	(97, 1, 'Estado inmediato de existencias'),
	(98, 1, 'Cobertura de stock'),
	(99, 1, 'Pedidos de materiales'),
	(100, 1, 'Configuración'),
	(101, 1, 'BlockCategories'),
	(102, 1, 'Dashgoals'),
	(103, 1, 'themeconfigurator'),
	(104, 1, 'Merchant Expertise'),
	(105, 1, 'Cron Jobs'),
	(106, 1, 'Onboarding'),
	(107, 1, 'Blog Management'),
	(108, 1, 'Blog Dashboard'),
	(109, 1, 'Categories Management'),
	(110, 1, 'Blogs Management'),
	(111, 1, 'Comment Management'),
	(112, 1, 'LeoSliderLayer'),
	(113, 1, 'Page Builder Profiles'),
	(114, 1, 'Footer Builder Profiles'),
	(115, 1, 'Page Images Management');
/*!40000 ALTER TABLE `ps_tab_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_tab_module_preference
DROP TABLE IF EXISTS `ps_tab_module_preference`;
CREATE TABLE IF NOT EXISTS `ps_tab_module_preference` (
  `id_tab_module_preference` int(11) NOT NULL AUTO_INCREMENT,
  `id_employee` int(11) NOT NULL,
  `id_tab` int(11) NOT NULL,
  `module` varchar(255) NOT NULL,
  PRIMARY KEY (`id_tab_module_preference`),
  UNIQUE KEY `employee_module` (`id_employee`,`id_tab`,`module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_tab_module_preference: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_tab_module_preference` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_tab_module_preference` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_tag
DROP TABLE IF EXISTS `ps_tag`;
CREATE TABLE IF NOT EXISTS `ps_tag` (
  `id_tag` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`id_tag`),
  KEY `tag_name` (`name`),
  KEY `id_lang` (`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_tag: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_tag` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_tax
DROP TABLE IF EXISTS `ps_tax`;
CREATE TABLE IF NOT EXISTS `ps_tax` (
  `id_tax` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rate` decimal(10,3) NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_tax`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_tax: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_tax` DISABLE KEYS */;
INSERT INTO `ps_tax` (`id_tax`, `rate`, `active`, `deleted`) VALUES
	(1, 19.000, 1, 0);
/*!40000 ALTER TABLE `ps_tax` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_tax_lang
DROP TABLE IF EXISTS `ps_tax_lang`;
CREATE TABLE IF NOT EXISTS `ps_tax_lang` (
  `id_tax` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`id_tax`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_tax_lang: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_tax_lang` DISABLE KEYS */;
INSERT INTO `ps_tax_lang` (`id_tax`, `id_lang`, `name`) VALUES
	(1, 1, 'IVA CL 19%');
/*!40000 ALTER TABLE `ps_tax_lang` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_tax_rule
DROP TABLE IF EXISTS `ps_tax_rule`;
CREATE TABLE IF NOT EXISTS `ps_tax_rule` (
  `id_tax_rule` int(11) NOT NULL AUTO_INCREMENT,
  `id_tax_rules_group` int(11) NOT NULL,
  `id_country` int(11) NOT NULL,
  `id_state` int(11) NOT NULL,
  `zipcode_from` varchar(12) NOT NULL,
  `zipcode_to` varchar(12) NOT NULL,
  `id_tax` int(11) NOT NULL,
  `behavior` int(11) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id_tax_rule`),
  KEY `id_tax_rules_group` (`id_tax_rules_group`),
  KEY `id_tax` (`id_tax`),
  KEY `category_getproducts` (`id_tax_rules_group`,`id_country`,`id_state`,`zipcode_from`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_tax_rule: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_tax_rule` DISABLE KEYS */;
INSERT INTO `ps_tax_rule` (`id_tax_rule`, `id_tax_rules_group`, `id_country`, `id_state`, `zipcode_from`, `zipcode_to`, `id_tax`, `behavior`, `description`) VALUES
	(1, 1, 68, 0, '0', '0', 1, 0, '');
/*!40000 ALTER TABLE `ps_tax_rule` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_tax_rules_group
DROP TABLE IF EXISTS `ps_tax_rules_group`;
CREATE TABLE IF NOT EXISTS `ps_tax_rules_group` (
  `id_tax_rules_group` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `active` int(11) NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_tax_rules_group`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_tax_rules_group: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_tax_rules_group` DISABLE KEYS */;
INSERT INTO `ps_tax_rules_group` (`id_tax_rules_group`, `name`, `active`, `deleted`, `date_add`, `date_upd`) VALUES
	(1, 'CL Standard Rate (19%)', 1, 0, '2015-05-08 14:00:05', '2015-05-08 14:00:05');
/*!40000 ALTER TABLE `ps_tax_rules_group` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_tax_rules_group_shop
DROP TABLE IF EXISTS `ps_tax_rules_group_shop`;
CREATE TABLE IF NOT EXISTS `ps_tax_rules_group_shop` (
  `id_tax_rules_group` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_tax_rules_group`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_tax_rules_group_shop: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_tax_rules_group_shop` DISABLE KEYS */;
INSERT INTO `ps_tax_rules_group_shop` (`id_tax_rules_group`, `id_shop`) VALUES
	(1, 1);
/*!40000 ALTER TABLE `ps_tax_rules_group_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_theme
DROP TABLE IF EXISTS `ps_theme`;
CREATE TABLE IF NOT EXISTS `ps_theme` (
  `id_theme` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `directory` varchar(64) NOT NULL,
  `responsive` tinyint(1) NOT NULL DEFAULT '0',
  `default_left_column` tinyint(1) NOT NULL DEFAULT '0',
  `default_right_column` tinyint(1) NOT NULL DEFAULT '0',
  `product_per_page` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_theme`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_theme: ~2 rows (approximately)
/*!40000 ALTER TABLE `ps_theme` DISABLE KEYS */;
INSERT INTO `ps_theme` (`id_theme`, `name`, `directory`, `responsive`, `default_left_column`, `default_right_column`, `product_per_page`) VALUES
	(1, 'default-bootstrap', 'default-bootstrap', 1, 1, 0, 12),
	(2, 'pf_mixstore', 'pf_mixstore', 1, 1, 1, 12);
/*!40000 ALTER TABLE `ps_theme` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_themeconfigurator
DROP TABLE IF EXISTS `ps_themeconfigurator`;
CREATE TABLE IF NOT EXISTS `ps_themeconfigurator` (
  `id_item` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `item_order` int(10) unsigned NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `title_use` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `hook` varchar(100) DEFAULT NULL,
  `url` text,
  `target` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `image` varchar(100) DEFAULT NULL,
  `image_w` varchar(10) DEFAULT NULL,
  `image_h` varchar(10) DEFAULT NULL,
  `html` text,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_item`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_themeconfigurator: ~7 rows (approximately)
/*!40000 ALTER TABLE `ps_themeconfigurator` DISABLE KEYS */;
INSERT INTO `ps_themeconfigurator` (`id_item`, `id_shop`, `id_lang`, `item_order`, `title`, `title_use`, `hook`, `url`, `target`, `image`, `image_w`, `image_h`, `html`, `active`) VALUES
	(1, 1, 1, 1, '', 0, 'home', 'http://www.prestashop.com/', 0, 'banner-img1.jpg', '383', '267', '', 1),
	(2, 1, 1, 2, '', 0, 'home', 'http://www.prestashop.com/', 0, 'banner-img2.jpg', '383', '267', '', 1),
	(3, 1, 1, 3, '', 0, 'home', 'http://www.prestashop.com/', 0, 'banner-img3.jpg', '383', '267', '', 1),
	(4, 1, 1, 4, '', 0, 'home', 'http://www.prestashop.com/', 0, 'banner-img4.jpg', '383', '142', '', 1),
	(5, 1, 1, 5, '', 0, 'home', 'http://www.prestashop.com/', 0, 'banner-img5.jpg', '777', '142', '', 1),
	(6, 1, 1, 6, '', 0, 'top', 'http://www.prestashop.com/', 0, 'banner-img6.jpg', '381', '219', '', 1),
	(7, 1, 1, 7, '', 0, 'top', 'http://www.prestashop.com/', 0, 'banner-img7.jpg', '381', '219', '', 1);
/*!40000 ALTER TABLE `ps_themeconfigurator` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_theme_meta
DROP TABLE IF EXISTS `ps_theme_meta`;
CREATE TABLE IF NOT EXISTS `ps_theme_meta` (
  `id_theme_meta` int(11) NOT NULL AUTO_INCREMENT,
  `id_theme` int(11) NOT NULL,
  `id_meta` int(10) unsigned NOT NULL,
  `left_column` tinyint(1) NOT NULL DEFAULT '1',
  `right_column` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_theme_meta`),
  UNIQUE KEY `id_theme_2` (`id_theme`,`id_meta`),
  KEY `id_theme` (`id_theme`),
  KEY `id_meta` (`id_meta`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_theme_meta: ~76 rows (approximately)
/*!40000 ALTER TABLE `ps_theme_meta` DISABLE KEYS */;
INSERT INTO `ps_theme_meta` (`id_theme_meta`, `id_theme`, `id_meta`, `left_column`, `right_column`) VALUES
	(1, 1, 1, 0, 0),
	(2, 1, 2, 1, 0),
	(3, 1, 3, 0, 0),
	(4, 1, 4, 0, 0),
	(5, 1, 5, 1, 0),
	(6, 1, 6, 1, 0),
	(7, 1, 7, 0, 0),
	(8, 1, 8, 1, 0),
	(9, 1, 9, 1, 0),
	(10, 1, 10, 0, 0),
	(11, 1, 11, 0, 0),
	(12, 1, 12, 0, 0),
	(13, 1, 13, 0, 0),
	(14, 1, 14, 0, 0),
	(15, 1, 15, 0, 0),
	(16, 1, 16, 0, 0),
	(17, 1, 17, 0, 0),
	(18, 1, 18, 0, 0),
	(19, 1, 19, 0, 0),
	(20, 1, 20, 0, 0),
	(21, 1, 21, 0, 0),
	(22, 1, 22, 1, 0),
	(23, 1, 23, 0, 0),
	(24, 1, 24, 0, 0),
	(25, 1, 25, 0, 0),
	(26, 1, 26, 0, 0),
	(27, 1, 28, 1, 0),
	(28, 1, 29, 0, 0),
	(29, 1, 27, 0, 0),
	(30, 1, 30, 0, 0),
	(31, 1, 31, 0, 0),
	(32, 1, 32, 0, 0),
	(33, 1, 33, 0, 0),
	(34, 1, 34, 0, 0),
	(35, 1, 35, 1, 0),
	(36, 1, 36, 1, 0),
	(37, 1, 37, 1, 0),
	(38, 1, 38, 1, 0),
	(77, 2, 1, 0, 0),
	(78, 2, 2, 1, 0),
	(79, 2, 3, 0, 0),
	(80, 2, 4, 0, 0),
	(81, 2, 5, 1, 0),
	(82, 2, 6, 1, 0),
	(83, 2, 7, 0, 0),
	(84, 2, 8, 1, 0),
	(85, 2, 9, 1, 0),
	(86, 2, 10, 0, 0),
	(87, 2, 11, 0, 0),
	(88, 2, 12, 0, 0),
	(89, 2, 13, 0, 0),
	(90, 2, 14, 1, 0),
	(91, 2, 15, 0, 0),
	(92, 2, 16, 0, 0),
	(93, 2, 17, 0, 0),
	(94, 2, 18, 0, 0),
	(95, 2, 19, 0, 0),
	(96, 2, 20, 0, 0),
	(97, 2, 21, 0, 0),
	(98, 2, 22, 1, 0),
	(99, 2, 23, 0, 0),
	(100, 2, 24, 0, 0),
	(101, 2, 25, 0, 0),
	(102, 2, 26, 0, 0),
	(103, 2, 27, 0, 0),
	(104, 2, 28, 1, 0),
	(105, 2, 29, 0, 0),
	(106, 2, 30, 0, 0),
	(107, 2, 31, 0, 0),
	(108, 2, 32, 0, 0),
	(109, 2, 33, 0, 0),
	(110, 2, 34, 0, 0),
	(111, 2, 35, 0, 0),
	(112, 2, 36, 0, 0),
	(113, 2, 37, 0, 0),
	(114, 2, 38, 0, 0);
/*!40000 ALTER TABLE `ps_theme_meta` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_theme_specific
DROP TABLE IF EXISTS `ps_theme_specific`;
CREATE TABLE IF NOT EXISTS `ps_theme_specific` (
  `id_theme` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  `entity` int(11) unsigned NOT NULL,
  `id_object` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_theme`,`id_shop`,`entity`,`id_object`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_theme_specific: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_theme_specific` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_theme_specific` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_timezone
DROP TABLE IF EXISTS `ps_timezone`;
CREATE TABLE IF NOT EXISTS `ps_timezone` (
  `id_timezone` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`id_timezone`)
) ENGINE=InnoDB AUTO_INCREMENT=561 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_timezone: ~560 rows (approximately)
/*!40000 ALTER TABLE `ps_timezone` DISABLE KEYS */;
INSERT INTO `ps_timezone` (`id_timezone`, `name`) VALUES
	(1, 'Africa/Abidjan'),
	(2, 'Africa/Accra'),
	(3, 'Africa/Addis_Ababa'),
	(4, 'Africa/Algiers'),
	(5, 'Africa/Asmara'),
	(6, 'Africa/Asmera'),
	(7, 'Africa/Bamako'),
	(8, 'Africa/Bangui'),
	(9, 'Africa/Banjul'),
	(10, 'Africa/Bissau'),
	(11, 'Africa/Blantyre'),
	(12, 'Africa/Brazzaville'),
	(13, 'Africa/Bujumbura'),
	(14, 'Africa/Cairo'),
	(15, 'Africa/Casablanca'),
	(16, 'Africa/Ceuta'),
	(17, 'Africa/Conakry'),
	(18, 'Africa/Dakar'),
	(19, 'Africa/Dar_es_Salaam'),
	(20, 'Africa/Djibouti'),
	(21, 'Africa/Douala'),
	(22, 'Africa/El_Aaiun'),
	(23, 'Africa/Freetown'),
	(24, 'Africa/Gaborone'),
	(25, 'Africa/Harare'),
	(26, 'Africa/Johannesburg'),
	(27, 'Africa/Kampala'),
	(28, 'Africa/Khartoum'),
	(29, 'Africa/Kigali'),
	(30, 'Africa/Kinshasa'),
	(31, 'Africa/Lagos'),
	(32, 'Africa/Libreville'),
	(33, 'Africa/Lome'),
	(34, 'Africa/Luanda'),
	(35, 'Africa/Lubumbashi'),
	(36, 'Africa/Lusaka'),
	(37, 'Africa/Malabo'),
	(38, 'Africa/Maputo'),
	(39, 'Africa/Maseru'),
	(40, 'Africa/Mbabane'),
	(41, 'Africa/Mogadishu'),
	(42, 'Africa/Monrovia'),
	(43, 'Africa/Nairobi'),
	(44, 'Africa/Ndjamena'),
	(45, 'Africa/Niamey'),
	(46, 'Africa/Nouakchott'),
	(47, 'Africa/Ouagadougou'),
	(48, 'Africa/Porto-Novo'),
	(49, 'Africa/Sao_Tome'),
	(50, 'Africa/Timbuktu'),
	(51, 'Africa/Tripoli'),
	(52, 'Africa/Tunis'),
	(53, 'Africa/Windhoek'),
	(54, 'America/Adak'),
	(55, 'America/Anchorage '),
	(56, 'America/Anguilla'),
	(57, 'America/Antigua'),
	(58, 'America/Araguaina'),
	(59, 'America/Argentina/Buenos_Aires'),
	(60, 'America/Argentina/Catamarca'),
	(61, 'America/Argentina/ComodRivadavia'),
	(62, 'America/Argentina/Cordoba'),
	(63, 'America/Argentina/Jujuy'),
	(64, 'America/Argentina/La_Rioja'),
	(65, 'America/Argentina/Mendoza'),
	(66, 'America/Argentina/Rio_Gallegos'),
	(67, 'America/Argentina/Salta'),
	(68, 'America/Argentina/San_Juan'),
	(69, 'America/Argentina/San_Luis'),
	(70, 'America/Argentina/Tucuman'),
	(71, 'America/Argentina/Ushuaia'),
	(72, 'America/Aruba'),
	(73, 'America/Asuncion'),
	(74, 'America/Atikokan'),
	(75, 'America/Atka'),
	(76, 'America/Bahia'),
	(77, 'America/Barbados'),
	(78, 'America/Belem'),
	(79, 'America/Belize'),
	(80, 'America/Blanc-Sablon'),
	(81, 'America/Boa_Vista'),
	(82, 'America/Bogota'),
	(83, 'America/Boise'),
	(84, 'America/Buenos_Aires'),
	(85, 'America/Cambridge_Bay'),
	(86, 'America/Campo_Grande'),
	(87, 'America/Cancun'),
	(88, 'America/Caracas'),
	(89, 'America/Catamarca'),
	(90, 'America/Cayenne'),
	(91, 'America/Cayman'),
	(92, 'America/Chicago'),
	(93, 'America/Chihuahua'),
	(94, 'America/Coral_Harbour'),
	(95, 'America/Cordoba'),
	(96, 'America/Costa_Rica'),
	(97, 'America/Cuiaba'),
	(98, 'America/Curacao'),
	(99, 'America/Danmarkshavn'),
	(100, 'America/Dawson'),
	(101, 'America/Dawson_Creek'),
	(102, 'America/Denver'),
	(103, 'America/Detroit'),
	(104, 'America/Dominica'),
	(105, 'America/Edmonton'),
	(106, 'America/Eirunepe'),
	(107, 'America/El_Salvador'),
	(108, 'America/Ensenada'),
	(109, 'America/Fort_Wayne'),
	(110, 'America/Fortaleza'),
	(111, 'America/Glace_Bay'),
	(112, 'America/Godthab'),
	(113, 'America/Goose_Bay'),
	(114, 'America/Grand_Turk'),
	(115, 'America/Grenada'),
	(116, 'America/Guadeloupe'),
	(117, 'America/Guatemala'),
	(118, 'America/Guayaquil'),
	(119, 'America/Guyana'),
	(120, 'America/Halifax'),
	(121, 'America/Havana'),
	(122, 'America/Hermosillo'),
	(123, 'America/Indiana/Indianapolis'),
	(124, 'America/Indiana/Knox'),
	(125, 'America/Indiana/Marengo'),
	(126, 'America/Indiana/Petersburg'),
	(127, 'America/Indiana/Tell_City'),
	(128, 'America/Indiana/Vevay'),
	(129, 'America/Indiana/Vincennes'),
	(130, 'America/Indiana/Winamac'),
	(131, 'America/Indianapolis'),
	(132, 'America/Inuvik'),
	(133, 'America/Iqaluit'),
	(134, 'America/Jamaica'),
	(135, 'America/Jujuy'),
	(136, 'America/Juneau'),
	(137, 'America/Kentucky/Louisville'),
	(138, 'America/Kentucky/Monticello'),
	(139, 'America/Knox_IN'),
	(140, 'America/La_Paz'),
	(141, 'America/Lima'),
	(142, 'America/Los_Angeles'),
	(143, 'America/Louisville'),
	(144, 'America/Maceio'),
	(145, 'America/Managua'),
	(146, 'America/Manaus'),
	(147, 'America/Marigot'),
	(148, 'America/Martinique'),
	(149, 'America/Mazatlan'),
	(150, 'America/Mendoza'),
	(151, 'America/Menominee'),
	(152, 'America/Merida'),
	(153, 'America/Mexico_City'),
	(154, 'America/Miquelon'),
	(155, 'America/Moncton'),
	(156, 'America/Monterrey'),
	(157, 'America/Montevideo'),
	(158, 'America/Montreal'),
	(159, 'America/Montserrat'),
	(160, 'America/Nassau'),
	(161, 'America/New_York'),
	(162, 'America/Nipigon'),
	(163, 'America/Nome'),
	(164, 'America/Noronha'),
	(165, 'America/North_Dakota/Center'),
	(166, 'America/North_Dakota/New_Salem'),
	(167, 'America/Panama'),
	(168, 'America/Pangnirtung'),
	(169, 'America/Paramaribo'),
	(170, 'America/Phoenix'),
	(171, 'America/Port-au-Prince'),
	(172, 'America/Port_of_Spain'),
	(173, 'America/Porto_Acre'),
	(174, 'America/Porto_Velho'),
	(175, 'America/Puerto_Rico'),
	(176, 'America/Rainy_River'),
	(177, 'America/Rankin_Inlet'),
	(178, 'America/Recife'),
	(179, 'America/Regina'),
	(180, 'America/Resolute'),
	(181, 'America/Rio_Branco'),
	(182, 'America/Rosario'),
	(183, 'America/Santarem'),
	(184, 'America/Santiago'),
	(185, 'America/Santo_Domingo'),
	(186, 'America/Sao_Paulo'),
	(187, 'America/Scoresbysund'),
	(188, 'America/Shiprock'),
	(189, 'America/St_Barthelemy'),
	(190, 'America/St_Johns'),
	(191, 'America/St_Kitts'),
	(192, 'America/St_Lucia'),
	(193, 'America/St_Thomas'),
	(194, 'America/St_Vincent'),
	(195, 'America/Swift_Current'),
	(196, 'America/Tegucigalpa'),
	(197, 'America/Thule'),
	(198, 'America/Thunder_Bay'),
	(199, 'America/Tijuana'),
	(200, 'America/Toronto'),
	(201, 'America/Tortola'),
	(202, 'America/Vancouver'),
	(203, 'America/Virgin'),
	(204, 'America/Whitehorse'),
	(205, 'America/Winnipeg'),
	(206, 'America/Yakutat'),
	(207, 'America/Yellowknife'),
	(208, 'Antarctica/Casey'),
	(209, 'Antarctica/Davis'),
	(210, 'Antarctica/DumontDUrville'),
	(211, 'Antarctica/Mawson'),
	(212, 'Antarctica/McMurdo'),
	(213, 'Antarctica/Palmer'),
	(214, 'Antarctica/Rothera'),
	(215, 'Antarctica/South_Pole'),
	(216, 'Antarctica/Syowa'),
	(217, 'Antarctica/Vostok'),
	(218, 'Arctic/Longyearbyen'),
	(219, 'Asia/Aden'),
	(220, 'Asia/Almaty'),
	(221, 'Asia/Amman'),
	(222, 'Asia/Anadyr'),
	(223, 'Asia/Aqtau'),
	(224, 'Asia/Aqtobe'),
	(225, 'Asia/Ashgabat'),
	(226, 'Asia/Ashkhabad'),
	(227, 'Asia/Baghdad'),
	(228, 'Asia/Bahrain'),
	(229, 'Asia/Baku'),
	(230, 'Asia/Bangkok'),
	(231, 'Asia/Beirut'),
	(232, 'Asia/Bishkek'),
	(233, 'Asia/Brunei'),
	(234, 'Asia/Calcutta'),
	(235, 'Asia/Choibalsan'),
	(236, 'Asia/Chongqing'),
	(237, 'Asia/Chungking'),
	(238, 'Asia/Colombo'),
	(239, 'Asia/Dacca'),
	(240, 'Asia/Damascus'),
	(241, 'Asia/Dhaka'),
	(242, 'Asia/Dili'),
	(243, 'Asia/Dubai'),
	(244, 'Asia/Dushanbe'),
	(245, 'Asia/Gaza'),
	(246, 'Asia/Harbin'),
	(247, 'Asia/Ho_Chi_Minh'),
	(248, 'Asia/Hong_Kong'),
	(249, 'Asia/Hovd'),
	(250, 'Asia/Irkutsk'),
	(251, 'Asia/Istanbul'),
	(252, 'Asia/Jakarta'),
	(253, 'Asia/Jayapura'),
	(254, 'Asia/Jerusalem'),
	(255, 'Asia/Kabul'),
	(256, 'Asia/Kamchatka'),
	(257, 'Asia/Karachi'),
	(258, 'Asia/Kashgar'),
	(259, 'Asia/Kathmandu'),
	(260, 'Asia/Katmandu'),
	(261, 'Asia/Kolkata'),
	(262, 'Asia/Krasnoyarsk'),
	(263, 'Asia/Kuala_Lumpur'),
	(264, 'Asia/Kuching'),
	(265, 'Asia/Kuwait'),
	(266, 'Asia/Macao'),
	(267, 'Asia/Macau'),
	(268, 'Asia/Magadan'),
	(269, 'Asia/Makassar'),
	(270, 'Asia/Manila'),
	(271, 'Asia/Muscat'),
	(272, 'Asia/Nicosia'),
	(273, 'Asia/Novosibirsk'),
	(274, 'Asia/Omsk'),
	(275, 'Asia/Oral'),
	(276, 'Asia/Phnom_Penh'),
	(277, 'Asia/Pontianak'),
	(278, 'Asia/Pyongyang'),
	(279, 'Asia/Qatar'),
	(280, 'Asia/Qyzylorda'),
	(281, 'Asia/Rangoon'),
	(282, 'Asia/Riyadh'),
	(283, 'Asia/Saigon'),
	(284, 'Asia/Sakhalin'),
	(285, 'Asia/Samarkand'),
	(286, 'Asia/Seoul'),
	(287, 'Asia/Shanghai'),
	(288, 'Asia/Singapore'),
	(289, 'Asia/Taipei'),
	(290, 'Asia/Tashkent'),
	(291, 'Asia/Tbilisi'),
	(292, 'Asia/Tehran'),
	(293, 'Asia/Tel_Aviv'),
	(294, 'Asia/Thimbu'),
	(295, 'Asia/Thimphu'),
	(296, 'Asia/Tokyo'),
	(297, 'Asia/Ujung_Pandang'),
	(298, 'Asia/Ulaanbaatar'),
	(299, 'Asia/Ulan_Bator'),
	(300, 'Asia/Urumqi'),
	(301, 'Asia/Vientiane'),
	(302, 'Asia/Vladivostok'),
	(303, 'Asia/Yakutsk'),
	(304, 'Asia/Yekaterinburg'),
	(305, 'Asia/Yerevan'),
	(306, 'Atlantic/Azores'),
	(307, 'Atlantic/Bermuda'),
	(308, 'Atlantic/Canary'),
	(309, 'Atlantic/Cape_Verde'),
	(310, 'Atlantic/Faeroe'),
	(311, 'Atlantic/Faroe'),
	(312, 'Atlantic/Jan_Mayen'),
	(313, 'Atlantic/Madeira'),
	(314, 'Atlantic/Reykjavik'),
	(315, 'Atlantic/South_Georgia'),
	(316, 'Atlantic/St_Helena'),
	(317, 'Atlantic/Stanley'),
	(318, 'Australia/ACT'),
	(319, 'Australia/Adelaide'),
	(320, 'Australia/Brisbane'),
	(321, 'Australia/Broken_Hill'),
	(322, 'Australia/Canberra'),
	(323, 'Australia/Currie'),
	(324, 'Australia/Darwin'),
	(325, 'Australia/Eucla'),
	(326, 'Australia/Hobart'),
	(327, 'Australia/LHI'),
	(328, 'Australia/Lindeman'),
	(329, 'Australia/Lord_Howe'),
	(330, 'Australia/Melbourne'),
	(331, 'Australia/North'),
	(332, 'Australia/NSW'),
	(333, 'Australia/Perth'),
	(334, 'Australia/Queensland'),
	(335, 'Australia/South'),
	(336, 'Australia/Sydney'),
	(337, 'Australia/Tasmania'),
	(338, 'Australia/Victoria'),
	(339, 'Australia/West'),
	(340, 'Australia/Yancowinna'),
	(341, 'Europe/Amsterdam'),
	(342, 'Europe/Andorra'),
	(343, 'Europe/Athens'),
	(344, 'Europe/Belfast'),
	(345, 'Europe/Belgrade'),
	(346, 'Europe/Berlin'),
	(347, 'Europe/Bratislava'),
	(348, 'Europe/Brussels'),
	(349, 'Europe/Bucharest'),
	(350, 'Europe/Budapest'),
	(351, 'Europe/Chisinau'),
	(352, 'Europe/Copenhagen'),
	(353, 'Europe/Dublin'),
	(354, 'Europe/Gibraltar'),
	(355, 'Europe/Guernsey'),
	(356, 'Europe/Helsinki'),
	(357, 'Europe/Isle_of_Man'),
	(358, 'Europe/Istanbul'),
	(359, 'Europe/Jersey'),
	(360, 'Europe/Kaliningrad'),
	(361, 'Europe/Kiev'),
	(362, 'Europe/Lisbon'),
	(363, 'Europe/Ljubljana'),
	(364, 'Europe/London'),
	(365, 'Europe/Luxembourg'),
	(366, 'Europe/Madrid'),
	(367, 'Europe/Malta'),
	(368, 'Europe/Mariehamn'),
	(369, 'Europe/Minsk'),
	(370, 'Europe/Monaco'),
	(371, 'Europe/Moscow'),
	(372, 'Europe/Nicosia'),
	(373, 'Europe/Oslo'),
	(374, 'Europe/Paris'),
	(375, 'Europe/Podgorica'),
	(376, 'Europe/Prague'),
	(377, 'Europe/Riga'),
	(378, 'Europe/Rome'),
	(379, 'Europe/Samara'),
	(380, 'Europe/San_Marino'),
	(381, 'Europe/Sarajevo'),
	(382, 'Europe/Simferopol'),
	(383, 'Europe/Skopje'),
	(384, 'Europe/Sofia'),
	(385, 'Europe/Stockholm'),
	(386, 'Europe/Tallinn'),
	(387, 'Europe/Tirane'),
	(388, 'Europe/Tiraspol'),
	(389, 'Europe/Uzhgorod'),
	(390, 'Europe/Vaduz'),
	(391, 'Europe/Vatican'),
	(392, 'Europe/Vienna'),
	(393, 'Europe/Vilnius'),
	(394, 'Europe/Volgograd'),
	(395, 'Europe/Warsaw'),
	(396, 'Europe/Zagreb'),
	(397, 'Europe/Zaporozhye'),
	(398, 'Europe/Zurich'),
	(399, 'Indian/Antananarivo'),
	(400, 'Indian/Chagos'),
	(401, 'Indian/Christmas'),
	(402, 'Indian/Cocos'),
	(403, 'Indian/Comoro'),
	(404, 'Indian/Kerguelen'),
	(405, 'Indian/Mahe'),
	(406, 'Indian/Maldives'),
	(407, 'Indian/Mauritius'),
	(408, 'Indian/Mayotte'),
	(409, 'Indian/Reunion'),
	(410, 'Pacific/Apia'),
	(411, 'Pacific/Auckland'),
	(412, 'Pacific/Chatham'),
	(413, 'Pacific/Easter'),
	(414, 'Pacific/Efate'),
	(415, 'Pacific/Enderbury'),
	(416, 'Pacific/Fakaofo'),
	(417, 'Pacific/Fiji'),
	(418, 'Pacific/Funafuti'),
	(419, 'Pacific/Galapagos'),
	(420, 'Pacific/Gambier'),
	(421, 'Pacific/Guadalcanal'),
	(422, 'Pacific/Guam'),
	(423, 'Pacific/Honolulu'),
	(424, 'Pacific/Johnston'),
	(425, 'Pacific/Kiritimati'),
	(426, 'Pacific/Kosrae'),
	(427, 'Pacific/Kwajalein'),
	(428, 'Pacific/Majuro'),
	(429, 'Pacific/Marquesas'),
	(430, 'Pacific/Midway'),
	(431, 'Pacific/Nauru'),
	(432, 'Pacific/Niue'),
	(433, 'Pacific/Norfolk'),
	(434, 'Pacific/Noumea'),
	(435, 'Pacific/Pago_Pago'),
	(436, 'Pacific/Palau'),
	(437, 'Pacific/Pitcairn'),
	(438, 'Pacific/Ponape'),
	(439, 'Pacific/Port_Moresby'),
	(440, 'Pacific/Rarotonga'),
	(441, 'Pacific/Saipan'),
	(442, 'Pacific/Samoa'),
	(443, 'Pacific/Tahiti'),
	(444, 'Pacific/Tarawa'),
	(445, 'Pacific/Tongatapu'),
	(446, 'Pacific/Truk'),
	(447, 'Pacific/Wake'),
	(448, 'Pacific/Wallis'),
	(449, 'Pacific/Yap'),
	(450, 'Brazil/Acre'),
	(451, 'Brazil/DeNoronha'),
	(452, 'Brazil/East'),
	(453, 'Brazil/West'),
	(454, 'Canada/Atlantic'),
	(455, 'Canada/Central'),
	(456, 'Canada/East-Saskatchewan'),
	(457, 'Canada/Eastern'),
	(458, 'Canada/Mountain'),
	(459, 'Canada/Newfoundland'),
	(460, 'Canada/Pacific'),
	(461, 'Canada/Saskatchewan'),
	(462, 'Canada/Yukon'),
	(463, 'CET'),
	(464, 'Chile/Continental'),
	(465, 'Chile/EasterIsland'),
	(466, 'CST6CDT'),
	(467, 'Cuba'),
	(468, 'EET'),
	(469, 'Egypt'),
	(470, 'Eire'),
	(471, 'EST'),
	(472, 'EST5EDT'),
	(473, 'Etc/GMT'),
	(474, 'Etc/GMT+0'),
	(475, 'Etc/GMT+1'),
	(476, 'Etc/GMT+10'),
	(477, 'Etc/GMT+11'),
	(478, 'Etc/GMT+12'),
	(479, 'Etc/GMT+2'),
	(480, 'Etc/GMT+3'),
	(481, 'Etc/GMT+4'),
	(482, 'Etc/GMT+5'),
	(483, 'Etc/GMT+6'),
	(484, 'Etc/GMT+7'),
	(485, 'Etc/GMT+8'),
	(486, 'Etc/GMT+9'),
	(487, 'Etc/GMT-0'),
	(488, 'Etc/GMT-1'),
	(489, 'Etc/GMT-10'),
	(490, 'Etc/GMT-11'),
	(491, 'Etc/GMT-12'),
	(492, 'Etc/GMT-13'),
	(493, 'Etc/GMT-14'),
	(494, 'Etc/GMT-2'),
	(495, 'Etc/GMT-3'),
	(496, 'Etc/GMT-4'),
	(497, 'Etc/GMT-5'),
	(498, 'Etc/GMT-6'),
	(499, 'Etc/GMT-7'),
	(500, 'Etc/GMT-8'),
	(501, 'Etc/GMT-9'),
	(502, 'Etc/GMT0'),
	(503, 'Etc/Greenwich'),
	(504, 'Etc/UCT'),
	(505, 'Etc/Universal'),
	(506, 'Etc/UTC'),
	(507, 'Etc/Zulu'),
	(508, 'Factory'),
	(509, 'GB'),
	(510, 'GB-Eire'),
	(511, 'GMT'),
	(512, 'GMT+0'),
	(513, 'GMT-0'),
	(514, 'GMT0'),
	(515, 'Greenwich'),
	(516, 'Hongkong'),
	(517, 'HST'),
	(518, 'Iceland'),
	(519, 'Iran'),
	(520, 'Israel'),
	(521, 'Jamaica'),
	(522, 'Japan'),
	(523, 'Kwajalein'),
	(524, 'Libya'),
	(525, 'MET'),
	(526, 'Mexico/BajaNorte'),
	(527, 'Mexico/BajaSur'),
	(528, 'Mexico/General'),
	(529, 'MST'),
	(530, 'MST7MDT'),
	(531, 'Navajo'),
	(532, 'NZ'),
	(533, 'NZ-CHAT'),
	(534, 'Poland'),
	(535, 'Portugal'),
	(536, 'PRC'),
	(537, 'PST8PDT'),
	(538, 'ROC'),
	(539, 'ROK'),
	(540, 'Singapore'),
	(541, 'Turkey'),
	(542, 'UCT'),
	(543, 'Universal'),
	(544, 'US/Alaska'),
	(545, 'US/Aleutian'),
	(546, 'US/Arizona'),
	(547, 'US/Central'),
	(548, 'US/East-Indiana'),
	(549, 'US/Eastern'),
	(550, 'US/Hawaii'),
	(551, 'US/Indiana-Starke'),
	(552, 'US/Michigan'),
	(553, 'US/Mountain'),
	(554, 'US/Pacific'),
	(555, 'US/Pacific-New'),
	(556, 'US/Samoa'),
	(557, 'UTC'),
	(558, 'W-SU'),
	(559, 'WET'),
	(560, 'Zulu');
/*!40000 ALTER TABLE `ps_timezone` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_warehouse
DROP TABLE IF EXISTS `ps_warehouse`;
CREATE TABLE IF NOT EXISTS `ps_warehouse` (
  `id_warehouse` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_currency` int(11) unsigned NOT NULL,
  `id_address` int(11) unsigned NOT NULL,
  `id_employee` int(11) unsigned NOT NULL,
  `reference` varchar(32) DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  `management_type` enum('WA','FIFO','LIFO') NOT NULL DEFAULT 'WA',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_warehouse`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_warehouse: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_warehouse` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_warehouse` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_warehouse_carrier
DROP TABLE IF EXISTS `ps_warehouse_carrier`;
CREATE TABLE IF NOT EXISTS `ps_warehouse_carrier` (
  `id_carrier` int(11) unsigned NOT NULL,
  `id_warehouse` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_warehouse`,`id_carrier`),
  KEY `id_warehouse` (`id_warehouse`),
  KEY `id_carrier` (`id_carrier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_warehouse_carrier: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_warehouse_carrier` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_warehouse_carrier` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_warehouse_product_location
DROP TABLE IF EXISTS `ps_warehouse_product_location`;
CREATE TABLE IF NOT EXISTS `ps_warehouse_product_location` (
  `id_warehouse_product_location` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_product` int(11) unsigned NOT NULL,
  `id_product_attribute` int(11) unsigned NOT NULL,
  `id_warehouse` int(11) unsigned NOT NULL,
  `location` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id_warehouse_product_location`),
  UNIQUE KEY `id_product` (`id_product`,`id_product_attribute`,`id_warehouse`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_warehouse_product_location: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_warehouse_product_location` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_warehouse_product_location` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_warehouse_shop
DROP TABLE IF EXISTS `ps_warehouse_shop`;
CREATE TABLE IF NOT EXISTS `ps_warehouse_shop` (
  `id_shop` int(11) unsigned NOT NULL,
  `id_warehouse` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_warehouse`,`id_shop`),
  KEY `id_warehouse` (`id_warehouse`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_warehouse_shop: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_warehouse_shop` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_warehouse_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_webservice_account
DROP TABLE IF EXISTS `ps_webservice_account`;
CREATE TABLE IF NOT EXISTS `ps_webservice_account` (
  `id_webservice_account` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(32) NOT NULL,
  `description` text,
  `class_name` varchar(50) NOT NULL DEFAULT 'WebserviceRequest',
  `is_module` tinyint(2) NOT NULL DEFAULT '0',
  `module_name` varchar(50) DEFAULT NULL,
  `active` tinyint(2) NOT NULL,
  PRIMARY KEY (`id_webservice_account`),
  KEY `key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_webservice_account: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_webservice_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_webservice_account` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_webservice_account_shop
DROP TABLE IF EXISTS `ps_webservice_account_shop`;
CREATE TABLE IF NOT EXISTS `ps_webservice_account_shop` (
  `id_webservice_account` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_webservice_account`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_webservice_account_shop: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_webservice_account_shop` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_webservice_account_shop` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_webservice_permission
DROP TABLE IF EXISTS `ps_webservice_permission`;
CREATE TABLE IF NOT EXISTS `ps_webservice_permission` (
  `id_webservice_permission` int(11) NOT NULL AUTO_INCREMENT,
  `resource` varchar(50) NOT NULL,
  `method` enum('GET','POST','PUT','DELETE','HEAD') NOT NULL,
  `id_webservice_account` int(11) NOT NULL,
  PRIMARY KEY (`id_webservice_permission`),
  UNIQUE KEY `resource_2` (`resource`,`method`,`id_webservice_account`),
  KEY `resource` (`resource`),
  KEY `method` (`method`),
  KEY `id_webservice_account` (`id_webservice_account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_webservice_permission: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_webservice_permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_webservice_permission` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_web_browser
DROP TABLE IF EXISTS `ps_web_browser`;
CREATE TABLE IF NOT EXISTS `ps_web_browser` (
  `id_web_browser` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id_web_browser`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_web_browser: ~11 rows (approximately)
/*!40000 ALTER TABLE `ps_web_browser` DISABLE KEYS */;
INSERT INTO `ps_web_browser` (`id_web_browser`, `name`) VALUES
	(1, 'Safari'),
	(2, 'Safari iPad'),
	(3, 'Firefox'),
	(4, 'Opera'),
	(5, 'IE 6'),
	(6, 'IE 7'),
	(7, 'IE 8'),
	(8, 'IE 9'),
	(9, 'IE 10'),
	(10, 'IE 11'),
	(11, 'Chrome');
/*!40000 ALTER TABLE `ps_web_browser` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_wishlist
DROP TABLE IF EXISTS `ps_wishlist`;
CREATE TABLE IF NOT EXISTS `ps_wishlist` (
  `id_wishlist` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_customer` int(10) unsigned NOT NULL,
  `token` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `counter` int(10) unsigned DEFAULT NULL,
  `id_shop` int(10) unsigned DEFAULT '1',
  `id_shop_group` int(10) unsigned DEFAULT '1',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `default` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`id_wishlist`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_wishlist: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_wishlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_wishlist` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_wishlist_email
DROP TABLE IF EXISTS `ps_wishlist_email`;
CREATE TABLE IF NOT EXISTS `ps_wishlist_email` (
  `id_wishlist` int(10) unsigned NOT NULL,
  `email` varchar(128) NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_wishlist_email: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_wishlist_email` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_wishlist_email` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_wishlist_product
DROP TABLE IF EXISTS `ps_wishlist_product`;
CREATE TABLE IF NOT EXISTS `ps_wishlist_product` (
  `id_wishlist_product` int(10) NOT NULL AUTO_INCREMENT,
  `id_wishlist` int(10) unsigned NOT NULL,
  `id_product` int(10) unsigned NOT NULL,
  `id_product_attribute` int(10) unsigned NOT NULL,
  `quantity` int(10) unsigned NOT NULL,
  `priority` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_wishlist_product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_wishlist_product: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_wishlist_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_wishlist_product` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_wishlist_product_cart
DROP TABLE IF EXISTS `ps_wishlist_product_cart`;
CREATE TABLE IF NOT EXISTS `ps_wishlist_product_cart` (
  `id_wishlist_product` int(10) unsigned NOT NULL,
  `id_cart` int(10) unsigned NOT NULL,
  `quantity` int(10) unsigned NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_wishlist_product_cart: ~0 rows (approximately)
/*!40000 ALTER TABLE `ps_wishlist_product_cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_wishlist_product_cart` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_zone
DROP TABLE IF EXISTS `ps_zone`;
CREATE TABLE IF NOT EXISTS `ps_zone` (
  `id_zone` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_zone`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_zone: ~8 rows (approximately)
/*!40000 ALTER TABLE `ps_zone` DISABLE KEYS */;
INSERT INTO `ps_zone` (`id_zone`, `name`, `active`) VALUES
	(1, 'Europe', 1),
	(2, 'North America', 1),
	(3, 'Asia', 1),
	(4, 'Africa', 1),
	(5, 'Oceania', 1),
	(6, 'South America', 1),
	(7, 'Europe (non-EU)', 1),
	(8, 'Central America/Antilla', 1);
/*!40000 ALTER TABLE `ps_zone` ENABLE KEYS */;


-- Dumping structure for table celiodev.ps_zone_shop
DROP TABLE IF EXISTS `ps_zone_shop`;
CREATE TABLE IF NOT EXISTS `ps_zone_shop` (
  `id_zone` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_zone`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table celiodev.ps_zone_shop: ~8 rows (approximately)
/*!40000 ALTER TABLE `ps_zone_shop` DISABLE KEYS */;
INSERT INTO `ps_zone_shop` (`id_zone`, `id_shop`) VALUES
	(1, 1),
	(2, 1),
	(3, 1),
	(4, 1),
	(5, 1),
	(6, 1),
	(7, 1),
	(8, 1);
/*!40000 ALTER TABLE `ps_zone_shop` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
