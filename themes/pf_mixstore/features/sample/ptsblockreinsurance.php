<?php 
$queryTables = array(); $querySQL =array(); 
$queryTables['ptsblockreinsurance']['ptsreinsurance']="CREATE TABLE IF NOT EXISTS  `"._DB_PREFIX_."ptsreinsurance` (
  `id_ptsreinsurance` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `file_name` varchar(100) NOT NULL,
  `addition_class` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_ptsreinsurance`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8";


$queryTables['ptsblockreinsurance']['ptsreinsurance_lang']="CREATE TABLE IF NOT EXISTS  `"._DB_PREFIX_."ptsreinsurance_lang` (
  `id_ptsreinsurance` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_lang` int(10) unsigned NOT NULL,
  `text` varchar(300) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_ptsreinsurance`,`id_lang`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8";


/*DATA FOR TABLE ptsreinsurance*/
 $querySQL['ptsreinsurance'][]="INSERT INTO "._DB_PREFIX_."ptsreinsurance( `id_ptsreinsurance`,`id_shop`,`file_name`,`addition_class` ) VALUES('1', '_SHOPID_', 'reinsurance-1-1.jpg', 'col-lg-4 col-md-4 col-sm-4 hidden-xs left-image')"; 
 $querySQL['ptsreinsurance'][]="INSERT INTO "._DB_PREFIX_."ptsreinsurance( `id_ptsreinsurance`,`id_shop`,`file_name`,`addition_class` ) VALUES('2', '_SHOPID_', 'reinsurance-2-1.jpg', 'col-lg-4 col-md-4 col-sm-4 hidden-xs')"; 
 $querySQL['ptsreinsurance'][]="INSERT INTO "._DB_PREFIX_."ptsreinsurance( `id_ptsreinsurance`,`id_shop`,`file_name`,`addition_class` ) VALUES('3', '_SHOPID_', 'reinsurance-3-1.jpg', 'col-lg-4 col-md-4 col-sm-4 hidden-xs')"; 
/*DATA FOR TABLE ptsreinsurance_lang*/
 $querySQL['ptsreinsurance_lang'][]="INSERT INTO "._DB_PREFIX_."ptsreinsurance_lang( `id_ptsreinsurance`,`id_lang`,`text`,`title` ) VALUES('1', '_LANGUAGEID_', '<p>on order over $100.00</p>', 'Free shipping')"; 
 $querySQL['ptsreinsurance_lang'][]="INSERT INTO "._DB_PREFIX_."ptsreinsurance_lang( `id_ptsreinsurance`,`id_lang`,`text`,`title` ) VALUES('2', '_LANGUAGEID_', '<p>free 90 days return policy</p>', 'Free Return ')"; 
 $querySQL['ptsreinsurance_lang'][]="INSERT INTO "._DB_PREFIX_."ptsreinsurance_lang( `id_ptsreinsurance`,`id_lang`,`text`,`title` ) VALUES('3', '_LANGUAGEID_', '<p>free register&nbsp;</p>', 'Member Discount')"; 

?>