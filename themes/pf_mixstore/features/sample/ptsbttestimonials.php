<?php 
$queryTables = array(); $querySQL =array(); 
$queryTables['ptsbttestimonials']['ptsbttestimonials']="CREATE TABLE IF NOT EXISTS  `"._DB_PREFIX_."ptsbttestimonials` (
  `id_ptsbttestimonials` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `avatar` varchar(255) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `company` varchar(255) DEFAULT NULL,
  `address` varchar(500) NOT NULL,
  `media_id` varchar(20) DEFAULT NULL,
  `media_type` varchar(20) DEFAULT NULL,
  `media_code` varchar(100) DEFAULT NULL,
  `date_add` datetime DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_ptsbttestimonials`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8";


$queryTables['ptsbttestimonials']['ptsbttestimonials_lang']="CREATE TABLE IF NOT EXISTS  `"._DB_PREFIX_."ptsbttestimonials_lang` (
  `id_ptsbttestimonials` int(10) unsigned NOT NULL,
  `id_lang` int(10) NOT NULL,
  `content` text NOT NULL,
  `note` text NOT NULL,
  PRIMARY KEY (`id_ptsbttestimonials`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


$queryTables['ptsbttestimonials']['ptsbttestimonials_shop']="CREATE TABLE IF NOT EXISTS  `"._DB_PREFIX_."ptsbttestimonials_shop` (
  `id_ptsbttestimonials` int(10) unsigned NOT NULL,
  `id_shop` int(10) NOT NULL,
  PRIMARY KEY (`id_ptsbttestimonials`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";


/*DATA FOR TABLE ptsbttestimonials*/
 $querySQL['ptsbttestimonials'][]="INSERT INTO "._DB_PREFIX_."ptsbttestimonials( `id_ptsbttestimonials`,`avatar`,`name`,`email`,`company`,`address`,`media_id`,`media_type`,`media_code`,`date_add`,`position`,`active` ) VALUES('1', 'sample-1.png', 'Person 1', 'leotheme@gmail.com', 'BrainOS', 'Tu Liem dist - Ha Noi city', '9bdrk8FNWnc', 'Youtube', 'http://www.youtube.com/watch?v=9bdrk8FNWnc', '2014-08-20 04:12:14', '1', '1')"; 
 $querySQL['ptsbttestimonials'][]="INSERT INTO "._DB_PREFIX_."ptsbttestimonials( `id_ptsbttestimonials`,`avatar`,`name`,`email`,`company`,`address`,`media_id`,`media_type`,`media_code`,`date_add`,`position`,`active` ) VALUES('2', 'sample-2.png', 'Person 2', 'leotheme@gmail.com', 'BrainOS', 'Tu Liem dist - Ha Noi city', '9bdrk8FNWnc', 'Youtube', 'http://www.youtube.com/watch?v=9bdrk8FNWnc', '2014-08-20 04:12:15', '2', '1')"; 
 $querySQL['ptsbttestimonials'][]="INSERT INTO "._DB_PREFIX_."ptsbttestimonials( `id_ptsbttestimonials`,`avatar`,`name`,`email`,`company`,`address`,`media_id`,`media_type`,`media_code`,`date_add`,`position`,`active` ) VALUES('3', 'sample-3.png', 'Person 3', 'leotheme@gmail.com', 'BrainOS', 'Tu Liem dist - Ha Noi city', '9bdrk8FNWnc', 'Youtube', 'http://www.youtube.com/watch?v=9bdrk8FNWnc', '2014-08-20 04:12:16', '3', '1')"; 
/*DATA FOR TABLE ptsbttestimonials_lang*/
 $querySQL['ptsbttestimonials_lang'][]="INSERT INTO "._DB_PREFIX_."ptsbttestimonials_lang( `id_ptsbttestimonials`,`id_lang`,`content`,`note` ) VALUES('1', '_LANGUAGEID_', 'PrestaShop is a leader in e-commerce with over 100,000 online stores and the perfect partner for La Poste.1', 'I love VietNamese 1')"; 
 $querySQL['ptsbttestimonials_lang'][]="INSERT INTO "._DB_PREFIX_."ptsbttestimonials_lang( `id_ptsbttestimonials`,`id_lang`,`content`,`note` ) VALUES('2', '_LANGUAGEID_', 'PrestaShop is a leader in e-commerce with over 100,000 online stores and the perfect partner for La Poste.2', 'I love VietNamese 2')"; 
 $querySQL['ptsbttestimonials_lang'][]="INSERT INTO "._DB_PREFIX_."ptsbttestimonials_lang( `id_ptsbttestimonials`,`id_lang`,`content`,`note` ) VALUES('3', '_LANGUAGEID_', 'PrestaShop is a leader in e-commerce with over 100,000 online stores and the perfect partner for La Poste.3', 'I love VietNamese 3')"; 
/*DATA FOR TABLE ptsbttestimonials_shop*/
 $querySQL['ptsbttestimonials_shop'][]="INSERT INTO "._DB_PREFIX_."ptsbttestimonials_shop( `id_ptsbttestimonials`,`id_shop` ) VALUES('1', '_SHOPID_')"; 
 $querySQL['ptsbttestimonials_shop'][]="INSERT INTO "._DB_PREFIX_."ptsbttestimonials_shop( `id_ptsbttestimonials`,`id_shop` ) VALUES('2', '_SHOPID_')"; 
 $querySQL['ptsbttestimonials_shop'][]="INSERT INTO "._DB_PREFIX_."ptsbttestimonials_shop( `id_ptsbttestimonials`,`id_shop` ) VALUES('3', '_SHOPID_')"; 

?>