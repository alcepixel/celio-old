<?php 
$queryTables = array(); $querySQL =array(); 
$queryTables['ptsstaticcontent']['ptsstaticcontent']="CREATE TABLE IF NOT EXISTS  `"._DB_PREFIX_."ptsstaticcontent` (
  `id_item` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `item_order` int(10) unsigned NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `title_use` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `hook` varchar(100) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `target` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `image` varchar(100) DEFAULT NULL,
  `image_w` varchar(10) DEFAULT NULL,
  `image_h` varchar(10) DEFAULT NULL,
  `html` text,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `col_lg` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `col_sm` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `class` varchar(50) NOT NULL,
  PRIMARY KEY (`id_item`)
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=utf8";


/*DATA FOR TABLE ptsstaticcontent*/
 $querySQL['ptsstaticcontent'][]="INSERT INTO "._DB_PREFIX_."ptsstaticcontent( `id_item`,`id_shop`,`id_lang`,`item_order`,`title`,`title_use`,`hook`,`url`,`target`,`image`,`image_w`,`image_h`,`html`,`active`,`col_lg`,`col_sm`,`class` ) VALUES('3', '_SHOPID_', '_LANGUAGEID_', '1', 'Banner Top1', '0', 'topcolumn', 'http://', '0', 'banner_top1.jpg', '0', '0', '', '1', '4', '0', 'banner-image')"; 
 $querySQL['ptsstaticcontent'][]="INSERT INTO "._DB_PREFIX_."ptsstaticcontent( `id_item`,`id_shop`,`id_lang`,`item_order`,`title`,`title_use`,`hook`,`url`,`target`,`image`,`image_w`,`image_h`,`html`,`active`,`col_lg`,`col_sm`,`class` ) VALUES('74', '_SHOPID_', '_LANGUAGEID_', '2', 'Banner Top2', '0', 'topcolumn', 'http://', '0', 'banner_top2.jpg', '0', '0', '', '1', '4', '0', 'banner-image')"; 
 $querySQL['ptsstaticcontent'][]="INSERT INTO "._DB_PREFIX_."ptsstaticcontent( `id_item`,`id_shop`,`id_lang`,`item_order`,`title`,`title_use`,`hook`,`url`,`target`,`image`,`image_w`,`image_h`,`html`,`active`,`col_lg`,`col_sm`,`class` ) VALUES('75', '_SHOPID_', '_LANGUAGEID_', '3', 'Banner Top3', '0', 'topcolumn', 'http://', '0', 'banner_top3.jpg', '0', '0', '', '1', '0', '0', 'banner-image')"; 
 $querySQL['ptsstaticcontent'][]="INSERT INTO "._DB_PREFIX_."ptsstaticcontent( `id_item`,`id_shop`,`id_lang`,`item_order`,`title`,`title_use`,`hook`,`url`,`target`,`image`,`image_w`,`image_h`,`html`,`active`,`col_lg`,`col_sm`,`class` ) VALUES('76', '_SHOPID_', '_LANGUAGEID_', '1', 'Logo footer', '0', 'footertop', 'http://', '0', 'logo-footer.png', '0', '0', '<p>We&rsquo;re confident we&rsquo;ve provided all the best for you. Stay conneect with us</p>', '1', '12', '0', '')"; 
 $querySQL['ptsstaticcontent'][]="INSERT INTO "._DB_PREFIX_."ptsstaticcontent( `id_item`,`id_shop`,`id_lang`,`item_order`,`title`,`title_use`,`hook`,`url`,`target`,`image`,`image_w`,`image_h`,`html`,`active`,`col_lg`,`col_sm`,`class` ) VALUES('91', '_SHOPID_', '_LANGUAGEID_', '1', 'Banner Left', '0', 'left', 'http://', '0', 'banner_left.jpg', '0', '0', '', '1', '0', '0', 'nopadding')"; 
 $querySQL['ptsstaticcontent'][]="INSERT INTO "._DB_PREFIX_."ptsstaticcontent( `id_item`,`id_shop`,`id_lang`,`item_order`,`title`,`title_use`,`hook`,`url`,`target`,`image`,`image_w`,`image_h`,`html`,`active`,`col_lg`,`col_sm`,`class` ) VALUES('92', '_SHOPID_', '_LANGUAGEID_', '1', 'Banner Bottom', '0', 'bottom', 'http://', '0', 'banner-bottom.jpg', '0', '0', '', '1', '0', '0', '')"; 
 $querySQL['ptsstaticcontent'][]="INSERT INTO "._DB_PREFIX_."ptsstaticcontent( `id_item`,`id_shop`,`id_lang`,`item_order`,`title`,`title_use`,`hook`,`url`,`target`,`image`,`image_w`,`image_h`,`html`,`active`,`col_lg`,`col_sm`,`class` ) VALUES('93', '_SHOPID_', '_LANGUAGEID_', '1', 'Custom Html footerbottom', '0', 'footerbottom', 'http://', '0', '', '0', '0', '<div class=\"custom info-services\">\r\n<div class=\"info-title border small\">call the customer services</div>\r\n<div class=\"text-highlight\"><strong>1800 - 1570 - 000</strong></div>\r\n<div class=\"border small\">Free 24/7 support for our customers</div>\r\n</div>', '1', '6', '0', '')"; 
 $querySQL['ptsstaticcontent'][]="INSERT INTO "._DB_PREFIX_."ptsstaticcontent( `id_item`,`id_shop`,`id_lang`,`item_order`,`title`,`title_use`,`hook`,`url`,`target`,`image`,`image_w`,`image_h`,`html`,`active`,`col_lg`,`col_sm`,`class` ) VALUES('94', '_SHOPID_', '_LANGUAGEID_', '2', 'Custom Html footerbottom2', '0', 'footerbottom', 'http://', '0', '', '0', '0', '<div class=\"custom info-question\">\r\n<div class=\"info-title border small\">email us your question</div>\r\n<div class=\"text-warning\"><strong>support@mixstore.com</strong></div>\r\n<div class=\"border small\">We will respond within 3 business days</div>\r\n</div>', '1', '6', '0', '')"; 
 $querySQL['ptsstaticcontent'][]="INSERT INTO "._DB_PREFIX_."ptsstaticcontent( `id_item`,`id_shop`,`id_lang`,`item_order`,`title`,`title_use`,`hook`,`url`,`target`,`image`,`image_w`,`image_h`,`html`,`active`,`col_lg`,`col_sm`,`class` ) VALUES('95', '_SHOPID_', '_LANGUAGEID_', '1', 'Banner header', '0', 'top', '#', '0', 'banner_hearder.jpg', '0', '0', '', '1', '6', '0', '')"; 
 $querySQL['ptsstaticcontent'][]="INSERT INTO "._DB_PREFIX_."ptsstaticcontent( `id_item`,`id_shop`,`id_lang`,`item_order`,`title`,`title_use`,`hook`,`url`,`target`,`image`,`image_w`,`image_h`,`html`,`active`,`col_lg`,`col_sm`,`class` ) VALUES('97', '_SHOPID_', '_LANGUAGEID_', '2', 'HotLine', '0', 'top', 'http://', '0', '', '0', '0', '<div class=\"hotline pull-right\">\r\n<p>Call the customer services</p>\r\n<p class=\"number\">1800 - 1570 - 000</p>\r\n</div>', '1', '3', '0', 'inner')"; 
 $querySQL['ptsstaticcontent'][]="INSERT INTO "._DB_PREFIX_."ptsstaticcontent( `id_item`,`id_shop`,`id_lang`,`item_order`,`title`,`title_use`,`hook`,`url`,`target`,`image`,`image_w`,`image_h`,`html`,`active`,`col_lg`,`col_sm`,`class` ) VALUES('98', '_SHOPID_', '_LANGUAGEID_', '1', 'Free Shipping', '0', 'home', 'http://', '0', '', '0', '0', '<h5>free shipping<small>on every order over $ 150</small></h5>', '1', '0', '0', 'free-shipping pull-right')"; 
 $querySQL['ptsstaticcontent'][]="INSERT INTO "._DB_PREFIX_."ptsstaticcontent( `id_item`,`id_shop`,`id_lang`,`item_order`,`title`,`title_use`,`hook`,`url`,`target`,`image`,`image_w`,`image_h`,`html`,`active`,`col_lg`,`col_sm`,`class` ) VALUES('143', '_SHOPID_', '_LANGUAGEID_', '1', 'paypal', '0', 'footer', 'http://', '0', 'paypal.png', '0', '0', '', '1', '0', '0', 'text-center')"; 

?>