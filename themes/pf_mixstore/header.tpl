{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="{$language_code|escape:'html':'UTF-8'}"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7" lang="{$language_code|escape:'html':'UTF-8'}"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie8" lang="{$language_code|escape:'html':'UTF-8'}"><![endif]-->
<!--[if gt IE 8]> <html class="no-js ie9" lang="{$language_code|escape:'html':'UTF-8'}"><![endif]-->
<html lang="{$language_code|escape:'html':'UTF-8'}" dir="{$LANG_DIRECTION}" class="{$LANG_DIRECTION}">
	<head>
		<meta charset="utf-8" />
		<title>{$meta_title|escape:'html':'UTF-8'}</title>
{if isset($meta_description) AND $meta_description}
		<meta name="description" content="{$meta_description|escape:'html':'UTF-8'}" />
{/if}
{if isset($meta_keywords) AND $meta_keywords}
		<meta name="keywords" content="{$meta_keywords|escape:'html':'UTF-8'}" />
{/if}
		<meta name="generator" content="PrestaShop" />
		<meta name="robots" content="{if isset($nobots)}no{/if}index,{if isset($nofollow) && $nofollow}no{/if}follow" />
		<meta name="viewport" content="width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0" /> 
		<meta name="apple-mobile-web-app-capable" content="yes" /> 
		<link rel="icon" type="image/vnd.microsoft.icon" href="{$favicon_url}?{$img_update_time}" />
		<link rel="shortcut icon" type="image/x-icon" href="{$favicon_url}?{$img_update_time}" />
{if isset($css_files)}
	{foreach from=$css_files key=css_uri item=media}
		{if preg_match("#global#",$css_uri)}
		<link rel="stylesheet" href="{$css_uri|escape:'html':'UTF-8'}"  id="global-style" type="text/css" media="{$media|escape:'html':'UTF-8'}" />
		{else}
		<link rel="stylesheet" href="{$css_uri|escape:'html':'UTF-8'}" type="text/css" media="{$media|escape:'html':'UTF-8'}" />
		{/if}
	{/foreach}
{/if}
{if isset($js_defer) && !$js_defer && isset($js_files) && isset($js_def)}
	{$js_def}
	{foreach from=$js_files item=js_uri}
	<script type="text/javascript" src="{$js_uri|escape:'html':'UTF-8'}"></script>
	{/foreach}
{/if}
		{$HOOK_HEADER}
		<link href='http{if Tools::usingSecureMode()}s{/if}://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic,900,900italic,100italic,100' rel='stylesheet' type='text/css' />

		<link href='http{if Tools::usingSecureMode()}s{/if}://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css' />
		
		<!--[if IE 8]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	 
	</head>
	<body{if isset($page_name)} id="{$page_name|escape:'html':'UTF-8'}"{/if} class="{if isset($page_name)}{$page_name|escape:'html':'UTF-8'}{/if}{if isset($body_classes) && $body_classes|@count} {implode value=$body_classes separator=' '}{/if}{if $hide_left_column} hide-left-column{/if}{if $hide_right_column} hide-right-column{/if}{if isset($content_only) && $content_only} content_only{/if} lang_{$lang_iso} layout-{$DEFAUTL_LAYOUT}" itemscope="" itemtype="http://schema.org/WebPage" >

	{if !isset($content_only) || !$content_only}
		{if isset($restricted_country_mode) && $restricted_country_mode}
			<div id="restricted-country">
				<p>{l s='You cannot place a new order from your country.'}{if isset($geolocation_country) && $geolocation_country} <span class="bold">{$geolocation_country|escape:'html':'UTF-8'}</span>{/if}</p>
			</div>
		{/if}
			
		<div id="page">
		<section  class="banner hidden-xs hidden-sm">
				<div class="container">
					{hook h="displayBanner"}
				</div>
		</section>
			{if isset($THEME_HEADER_FILE) && file_exists($THEME_HEADER_FILE)}
 				{include file=$THEME_HEADER_FILE page_name_skin=$page_name}
			{else}
			
			<header id="header">
				<section id="topbar" class="topbar">
					<div class="container">
						<div class="container-inner clearfix">
							{hook h="displayNav"}

							{if class_exists('PtsthemePanel')}
								{plugin module='blockcart' hook='top'}
							{/if}

						</div>
					</div>
				</section>	
				<section  id="header-main" class="header">
					<div class="container">
						<div class="row">
							<div id="header_logo" class="col-xs-12 col-sm-12 col-md-3 col-lg-3 inner">
								<div id="logo-theme" class="logo-store">
								{if Configuration::get('PTS_CP_LOGOTYPE') == 'logo-theme'}
									<div class="logo-theme">
										<a href="{$base_dir}" title="{$shop_name|escape:'html':'UTF-8'}">
											
										</a>
									</div>
								{else}
									<a href="{$base_dir}" title="{$shop_name|escape:'html':'UTF-8'}">
											<img class="logo img-responsive" src="{$logo_url}" alt="{$shop_name|escape:'html':'UTF-8'}"{if isset($logo_image_width) && $logo_image_width} width="{$logo_image_width}"{/if}{if isset($logo_image_height) && $logo_image_height} height="{$logo_image_height}"{/if}/>
									</a>
								{/if}
								</div>
							</div>

								{if isset($HOOK_TOP)}{$HOOK_TOP}{/if}
						</div>
					</div>
				
				</section >

	            <section  id="pts-mainnav" class="main-menu">
	                <div class="container">
						<div class="container-inner clearfix">
		                	<div class="row">
			                	<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
			                    	{hook h="displayMainmenu"}
			                 	</div>
							</div>

								{if class_exists('PtsthemePanel')}
									{plugin module='blocksearch' hook='top'}
								{/if}
						</div>
	                </div>
	            </section>

			</header>
			{/if}

			{if $page_name =='index'}
    		{plugin  module='revsliderprestashop' hook='displayHome'}
    		{/if}
    		
            {if Configuration::get('PTS_CP_ENABLE_PBUILDER')&&isset($PTS_PAGEBUILDER_ACTIVED)&&$PTS_PAGEBUILDER_ACTIVED&&$PTS_PAGEBUILDER_FULL}
            	<section id="columns" class="pagebuilder-content">
				{$PTS_PAGEBUILDER_CONTENT}
			{else}
			{if $page_name =='index'}
           <section  id="pts-slideshow" class="slideshow">
		                {hook h="displayslideshow"}     
            </section>
            {/if}
            
			<section id="columns" class="offcanvas-siderbars">
						{if $page_name !='index' && $page_name !='pagenotfound'}
							{if isset($THEME_SKIN_DIR)&&file_exists("$THEME_SKIN_DIR./page-title.tpl")}
								
							<div id="breadcrumb">
								<div class="container">
									
									{include file="$THEME_SKIN_DIR./page-title.tpl"}
								</div>
							</div>
							{else}
							<div id="breadcrumb">
								<div class="container">
									{include file="$tpl_dir./breadcrumb.tpl"}
								</div>
							</div>
							{/if}
						{/if}
				<div class="container main-content">	
					{if $page_name =='index'}
						<div id="top_column" class="top_column row">{hook h="displayTopColumn"}</div>
					{/if}									
			
						<div class="row">
							{if isset($left_column_size) && !empty($left_column_size)}
								<div id="left_column" class="sidebar column col-xs-12 col-sm-12 col-md-{$left_column_size|intval} col-lg-{$left_column_size|intval} offcanvas-sidebar">
									{$HOOK_LEFT_COLUMN}
								</div>
							{/if}
						{if isset($left_column_size) && isset($right_column_size)}{assign var='cols' value=(12 - $left_column_size - $right_column_size)}{else}{assign var='cols' value=12}{/if}
						<div id="center_column" class="center_column col-xs-12 col-sm-12 col-md-{$cols|intval} col-lg-{$cols|intval}">
									<div id="content">

				{/if}
			{/if}
