<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockbanner}pf_mixstore>blockbanner_4b92fcfe6f0ec26909935aa960b7b81f'] = 'كتلة راية';
$_MODULE['<{blockbanner}pf_mixstore>blockbanner_9ec3d0f07db2d25a37ec4b7a21c788f8'] = 'يعرض لافتة في الجزء العلوي من المحل.';
$_MODULE['<{blockbanner}pf_mixstore>blockbanner_df7859ac16e724c9b1fba0a364503d72'] = 'حدث خطأ في ملف التحميل';
$_MODULE['<{blockbanner}pf_mixstore>blockbanner_efc226b17e0532afff43be870bff0de7'] = 'تم تعديل الاعدادات بنحاج';
$_MODULE['<{blockbanner}pf_mixstore>blockbanner_f4f70727dc34561dfde1a3c529b6205c'] = 'الإعدادات';
$_MODULE['<{blockbanner}pf_mixstore>blockbanner_9edcdbdff24876b0dac92f97397ae497'] = 'أعلى الصورة لافتة';
$_MODULE['<{blockbanner}pf_mixstore>blockbanner_e90797453e35e4017b82e54e2b216290'] = 'تحميل صورة لكبار الشعار الخاص بك. الأبعاد الموصى بها هي 1170 س 65px إذا كنت تستخدم السمة الافتراضية.';
$_MODULE['<{blockbanner}pf_mixstore>blockbanner_46fae48f998058600248a16100acfb7e'] = 'راية صلة';
$_MODULE['<{blockbanner}pf_mixstore>blockbanner_084fa1da897dfe3717efa184616ff91c'] = 'ادخل على الرابط المرتبطة الشعار الخاص بك. عند النقر على لافتة، يفتح الرابط في نفس النافذة. إذا تم إدخال أي صلة، فإنه الموجهات إلى الصفحة الرئيسية.';
$_MODULE['<{blockbanner}pf_mixstore>blockbanner_ff09729bee8a82c374f6b61e14a4af76'] = 'وصف راية';
$_MODULE['<{blockbanner}pf_mixstore>blockbanner_112f6f9a1026d85f440e5ca68d8e2ec5'] = 'أدخل وصفا موجزا ولكن ذات مغزى لافتة.';
$_MODULE['<{blockbanner}pf_mixstore>blockbanner_c9cc8cce247e49bae79f15173ce97354'] = 'حفظ';
