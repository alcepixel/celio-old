<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockbestsellers}pf_mixstore>blockbestsellers-home_3cb29f0ccc5fd220a97df89dafe46290'] = 'الأكثر مبيعاً';
$_MODULE['<{blockbestsellers}pf_mixstore>blockbestsellers-home_d3da97e2d9aee5c8fbe03156ad051c99'] = 'مشاهدة المزيد';
$_MODULE['<{blockbestsellers}pf_mixstore>blockbestsellers-home_4351cfebe4b61d8aa5efa1d020710005'] = 'عرض';
$_MODULE['<{blockbestsellers}pf_mixstore>blockbestsellers-home_eae99cd6a931f3553123420b16383812'] = 'جميع الكتب مبيعا';
$_MODULE['<{blockbestsellers}pf_mixstore>blockbestsellers-home_adc570b472f54d65d3b90b8cee8368a9'] = 'لا يوجد منتجات أكثر مبيعاً';
$_MODULE['<{blockbestsellers}pf_mixstore>blockbestsellers_9862f1949f776f69155b6e6b330c7ee1'] = 'بلوك الإعلانات';
$_MODULE['<{blockbestsellers}pf_mixstore>blockbestsellers_ed6476843a865d9daf92e409082b76e1'] = 'اضافة بلوك يظهر اكثر المنتجات مبيعا.';
$_MODULE['<{blockbestsellers}pf_mixstore>blockbestsellers_b15e7271053fe9dd22d80db100179085'] = 'هذه الحاجة وحدة ومدمن مخدرات في عمود والموضوع الخاص بك لا يقوم واحد';
$_MODULE['<{blockbestsellers}pf_mixstore>blockbestsellers_c888438d14855d7d96a2724ee9c306bd'] = 'تم تحديث الإعدادات بنجاح';
$_MODULE['<{blockbestsellers}pf_mixstore>blockbestsellers_f4f70727dc34561dfde1a3c529b6205c'] = 'الإعدادات';
$_MODULE['<{blockbestsellers}pf_mixstore>blockbestsellers_26986c3388870d4148b1b5375368a83d'] = 'المنتجات المعروضة';
$_MODULE['<{blockbestsellers}pf_mixstore>blockbestsellers_2b21378492166b0e5a855e2da611659c'] = 'تحديد عدد من المنتجات لعرضها في هذه المجموعة';
$_MODULE['<{blockbestsellers}pf_mixstore>blockbestsellers_24ff4e4d39bb7811f6bdf0c189462272'] = 'عرض هذا الصندوق دائماً';
$_MODULE['<{blockbestsellers}pf_mixstore>blockbestsellers_84b0c5fdef19ab8ef61cd809f9250d85'] = 'تظهر كتلة حتى لو كان يوجد أكثر الكتب مبيعا المتاحة.';
$_MODULE['<{blockbestsellers}pf_mixstore>blockbestsellers_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'مفعل';
$_MODULE['<{blockbestsellers}pf_mixstore>blockbestsellers_b9f5c797ebbf55adccdd8539a65a0241'] = 'غير مفعل';
$_MODULE['<{blockbestsellers}pf_mixstore>blockbestsellers_c9cc8cce247e49bae79f15173ce97354'] = 'حفظ';
$_MODULE['<{blockbestsellers}pf_mixstore>blockbestsellers_1d0a2e1f62ccf460d604ccbc9e09da95'] = 'عرض المنتجات مبيعا';
$_MODULE['<{blockbestsellers}pf_mixstore>blockbestsellers_3cb29f0ccc5fd220a97df89dafe46290'] = 'الأكثر مبيعاً';
$_MODULE['<{blockbestsellers}pf_mixstore>blockbestsellers_eae99cd6a931f3553123420b16383812'] = 'جميع الكتب مبيعا';
$_MODULE['<{blockbestsellers}pf_mixstore>blockbestsellers_f7be84d6809317a6eb0ff3823a936800'] = 'لا توجد منتجات أكثر مبيعاً في هذا الوقت';
$_MODULE['<{blockbestsellers}pf_mixstore>tab_d7b2933ba512ada478c97fa43dd7ebe6'] = 'الأكثر مبيعاً';
$_MODULE['<{blockbestsellers}pf_mixstore>blockbestsellers-home_09a5fe24fe0fc9ce90efc4aa507c66e7'] = 'أي أكثر الكتب مبيعا في هذا الوقت.';
