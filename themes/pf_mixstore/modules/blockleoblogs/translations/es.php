<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_9df95e2831993f2026966dff786a9fdf'] = 'Leo Blogs derniers modules';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_016a8a600b41dc794b536ef8f0982560'] = 'Ce module fonctionne avec le module leoblog.';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_2e57399079951d84b435700493b8a8c1'] = 'Vous devez remplir le champ \"produits exposés.';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_73293a024e644165e9bf48f270af63a0'] = 'Nombre non valide.';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_c888438d14855d7d96a2724ee9c306bd'] = 'Paramètres mis à jour';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_f4f70727dc34561dfde1a3c529b6205c'] = 'Paramètres';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_ca02ac0a2542a3615a432a7dd4e6a3f5'] = 'Blogs à afficher.';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_e875456f877772f8ac18840ec2337a3a'] = 'Définir le nombre de blogs affichées dans ce bloc.';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_a40e139e6bc2f497baa304a7d5d15020'] = 'Image blog Largeur';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_c4f350f477de71806a7a1039b72b2c49'] = 'Définir la largeur des images affichées dans ce bloc.';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_6b7ad1f460a47d67495762959938f708'] = 'Image blog Hauteur.';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_a14d42edf7636236e390553609a82fd1'] = 'Définir la hauteur des images affichées dans ce bloc.';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_b95892832bcd28968b8552c549d85296'] = 'Articles par page.';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_74527bda74f015d8170ed2f76a71a10a'] = 'Le nombre maximum d\'éléments affichés dans ce bloc.';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_9f05a6035cce3b39a222c090230d308f'] = 'Colonnes dans le Tab.';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_a4d59882d0e863d29ef75c2d190b7191'] = 'Les éléments de la colonne maximales dans l\'onglet.';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_52bd157590de09e3d18a02da5894cf0d'] = 'Intval.';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_8c553821cf7e1a6bcc6a68b53fd67d3e'] = 'Montrer Voir tous';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Activé';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_b9f5c797ebbf55adccdd8539a65a0241'] = 'Handicapé';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_dfda6017439a3a9141bfdafd85f6ec53'] = 'Afficher le titre:';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_b32bc318930e5e84162df7f820b890b7'] = 'Voir Description:';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_be787f82b86e36309ead3e0a96df01db'] = 'Voir l\'image:';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_1e1f3f33b8e5b363babf5c8669b98899'] = 'Afficher Auteur:';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_bbc759979eef21ac52837e37a5efed1c'] = 'Afficher Catégorie:';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_8ee5610557152adc2b7a20346425f713'] = 'Afficher Date de création:';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_414a32386496bab562b4f7fab2bd06d4'] = 'Afficher Counter:';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_11383e2d875def5a4931cb44c3e1a5f2'] = 'Voir Hits:';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_c9cc8cce247e49bae79f15173ce97354'] = 'Enregistrer';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_9147515e20dbdf08f969c64766815578'] = 'Blogs les plus récents';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_0be8406951cdfda82f00f79328cf4efc'] = 'Commentaire';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_a517747c3d12f99244ae598910d979c5'] = 'Auteur';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_b69df945ae986e6b1882cdc87ad19617'] = 'Visites';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_43340e6cc4e88197d57f8d6d5ea50a46'] = 'En savoir plus';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_d8d7ad6abfd08c27baa8bedb78c72ddb'] = 'Voir tous';
$_MODULE['<{blockleoblogs}pf_mixstore>blockleoblogs_6dd8b039e7cd451a8b4fe54654b05ac0'] = 'voir plus de mises à jour';
