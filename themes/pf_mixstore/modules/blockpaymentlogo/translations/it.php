<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockpaymentlogo}pf_mixstore>blockpaymentlogo_eaaf494f0c90a2707d768a9df605e94b'] = 'Blocco loghi di pagamento';
$_MODULE['<{blockpaymentlogo}pf_mixstore>blockpaymentlogo_3fd11fa0ede1bd0ace9b3fcdbf6a71c9'] = 'Aggiunge un blocco che mostra tutti i tuoi loghi di pagamento.';
$_MODULE['<{blockpaymentlogo}pf_mixstore>blockpaymentlogo_b15e7271053fe9dd22d80db100179085'] = 'Questo modulo ha bisogno di essere ancorato ad una colonna e il tuo tema non ne contiene';
$_MODULE['<{blockpaymentlogo}pf_mixstore>blockpaymentlogo_efc226b17e0532afff43be870bff0de7'] = 'Le impostazioni sono state aggiornate.';
$_MODULE['<{blockpaymentlogo}pf_mixstore>blockpaymentlogo_5c5e5371da7ab2c28d1af066a1a1cc0d'] = 'Nessuna pagina CMS è disponibile.';
$_MODULE['<{blockpaymentlogo}pf_mixstore>blockpaymentlogo_f4f70727dc34561dfde1a3c529b6205c'] = 'Impostazioni';
$_MODULE['<{blockpaymentlogo}pf_mixstore>blockpaymentlogo_829cb250498661a9f98a58dc670a9675'] = 'Pagina di destinazione per il link del blocco';
$_MODULE['<{blockpaymentlogo}pf_mixstore>blockpaymentlogo_c9cc8cce247e49bae79f15173ce97354'] = 'Salva';
$_MODULE['<{blockpaymentlogo}pf_mixstore>blockpaymentlogo_b13fca921e8a4547cf90ca42a8b68b8a'] = 'Accettiamo';
