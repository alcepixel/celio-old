<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockreinsurance}pf_mixstore>blockreinsurance_873330fc1881555fffe2bc471d04bf5d'] = 'كتلة طمأنة العملاء';
$_MODULE['<{blockreinsurance}pf_mixstore>blockreinsurance_7e7f70db3c75e428db8e2d0a1765c4e9'] = 'يضيف كتلة المعلومات التي تهدف إلى تقديم معلومات مفيدة لطمأنة العملاء بأن متجرك هو جدير بالثقة.';
$_MODULE['<{blockreinsurance}pf_mixstore>blockreinsurance_d52eaeff31af37a4a7e0550008aff5df'] = 'حدث خطأ أثناء محاولة حفظ.';
$_MODULE['<{blockreinsurance}pf_mixstore>blockreinsurance_0366c7b2ea1bb228cd44aec7e3e26a3e'] = 'تحديث التكوين';
$_MODULE['<{blockreinsurance}pf_mixstore>blockreinsurance_8363eee01f4da190a4cef9d26a36750c'] = 'الجديد كتلة الطمأنينة';
$_MODULE['<{blockreinsurance}pf_mixstore>blockreinsurance_be53a0541a6d36f6ecb879fa2c584b08'] = 'الصورة';
$_MODULE['<{blockreinsurance}pf_mixstore>blockreinsurance_9dffbf69ffba8bc38bc4e01abf4b1675'] = 'نص';
$_MODULE['<{blockreinsurance}pf_mixstore>blockreinsurance_c9cc8cce247e49bae79f15173ce97354'] = 'حفظ';
$_MODULE['<{blockreinsurance}pf_mixstore>blockreinsurance_630f6dc397fe74e52d5189e2c80f282b'] = 'رجوع الى القائمة';
$_MODULE['<{blockreinsurance}pf_mixstore>blockreinsurance_b718adec73e04ce3ec720dd11a06a308'] = 'الرقم التعريفي';
$_MODULE['<{blockreinsurance}pf_mixstore>blockreinsurance_23498d91b6017e5d7f4ddde70daba286'] = 'رقم تعريف المتجر';
$_MODULE['<{blockreinsurance}pf_mixstore>blockreinsurance_ef61fb324d729c341ea8ab9901e23566'] = 'إضافة';
$_MODULE['<{blockreinsurance}pf_mixstore>blockreinsurance_f3295a93167b56c5a19030e91823f7bf'] = 'ضمان استعادة الاموال.';
$_MODULE['<{blockreinsurance}pf_mixstore>blockreinsurance_56e140ebd6f399c22c8859a694b247f3'] = 'في متجر الصرف.';
$_MODULE['<{blockreinsurance}pf_mixstore>blockreinsurance_597ce11744f9bbf116ec9e4a719ec9d3'] = 'الدفع عند الشحن.';
$_MODULE['<{blockreinsurance}pf_mixstore>blockreinsurance_3aca7ac5cf7e462b658960931946f824'] = 'توصيل المجاني';
$_MODULE['<{blockreinsurance}pf_mixstore>blockreinsurance_d05244e0e410a6b85ed53a014908c657'] = '100٪ آمن عمليات الدفع.';
