<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockspecials}pf_mixstore>blockspecials_c19ed4ea98cbf319735f6d09bde6c757'] = 'كتلة العروض الخاصة';
$_MODULE['<{blockspecials}pf_mixstore>blockspecials_f5bae06098deb9298cb9ceca89171944'] = 'ويضيف كتلة عرض المنتجات المخفضة الحالية.';
$_MODULE['<{blockspecials}pf_mixstore>blockspecials_b15e7271053fe9dd22d80db100179085'] = 'هذه الحاجة وحدة ومدمن مخدرات في عمود والموضوع الخاص بك لا يقوم واحد';
$_MODULE['<{blockspecials}pf_mixstore>blockspecials_c888438d14855d7d96a2724ee9c306bd'] = 'تم تحديث الإعدادات بنجاح';
$_MODULE['<{blockspecials}pf_mixstore>blockspecials_f4f70727dc34561dfde1a3c529b6205c'] = 'الإعدادات';
$_MODULE['<{blockspecials}pf_mixstore>blockspecials_24ff4e4d39bb7811f6bdf0c189462272'] = 'عرض هذا الصندوق دائماً';
$_MODULE['<{blockspecials}pf_mixstore>blockspecials_53d61d1ac0507b1bd8cd99db8d64fb19'] = 'إظهار الصندوق حتى إن لم تكن هناك منتجات متاحة.';
$_MODULE['<{blockspecials}pf_mixstore>blockspecials_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'مفعل';
$_MODULE['<{blockspecials}pf_mixstore>blockspecials_b9f5c797ebbf55adccdd8539a65a0241'] = 'غير مفعل';
$_MODULE['<{blockspecials}pf_mixstore>blockspecials_61465481ac2491b37e4517960bbd4a14'] = 'عدد الملفات المخزنة مؤقتا';
$_MODULE['<{blockspecials}pf_mixstore>blockspecials_e80a11f1704b88ad50f8fc6ce0f43525'] = 'يتم عرض العروض الخاصة عشوائيا على الواجهة الأمامية، ولكن نظرا لأنه يأخذ الكثير من RESSOURCES، فمن الأفضل لتخزين النتائج. تتم إعادة تعيين ذاكرة التخزين المؤقت يوميا. 0 سيتم تعطيل ذاكرة التخزين المؤقت.';
$_MODULE['<{blockspecials}pf_mixstore>blockspecials_c9cc8cce247e49bae79f15173ce97354'] = 'حفظ';
$_MODULE['<{blockspecials}pf_mixstore>blockspecials_d1aa22a3126f04664e0fe3f598994014'] = 'التخفيضات';
$_MODULE['<{blockspecials}pf_mixstore>blockspecials_b4f95c1ea534936cc60c6368c225f480'] = 'مشاهدة المزيد';
$_MODULE['<{blockspecials}pf_mixstore>blockspecials_3c9f5a6dc6585f75042bd4242c020081'] = 'لا خاصة في هذا الوقت.';
