<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{dashtrends}pf_mixstore>dashtrends_ee653ade5f520037ef95e9dc2a42364c'] = 'اتجاهات لوحة القيادة';
$_MODULE['<{dashtrends}pf_mixstore>dashtrends_2d125dc25b158f28a1960bd96a9fa8d1'] = '٪ S نقطة';
$_MODULE['<{dashtrends}pf_mixstore>dashtrends_11ff9f68afb6b8b5b8eda218d7c83a65'] = 'المبيعات';
$_MODULE['<{dashtrends}pf_mixstore>dashtrends_7442e29d7d53e549b78d93c46b8cdcfc'] = 'الطلبات';
$_MODULE['<{dashtrends}pf_mixstore>dashtrends_8c804960da61b637c548c951652b0cac'] = 'Average Cart Value';
$_MODULE['<{dashtrends}pf_mixstore>dashtrends_d7e637a6e9ff116de2fa89551240a94d'] = 'الزيارات';
$_MODULE['<{dashtrends}pf_mixstore>dashtrends_e4c3da18c66c0147144767efeb59198f'] = 'Conversion Rate';
$_MODULE['<{dashtrends}pf_mixstore>dashtrends_43d729c7b81bfa5fc10e756660d877d1'] = 'Net Profit';
$_MODULE['<{dashtrends}pf_mixstore>dashtrends_46418a037045b91e6715c4da91a2a269'] = '%s (previous period)';
$_MODULE['<{dashtrends}pf_mixstore>dashboard_zone_two_2938c7f7e560ed972f8a4f68e80ff834'] = 'لوحة التحكم';
$_MODULE['<{dashtrends}pf_mixstore>dashboard_zone_two_f1206f9fadc5ce41694f69129aecac26'] = 'الإعدادات';
$_MODULE['<{dashtrends}pf_mixstore>dashboard_zone_two_63a6a88c066880c5ac42394a22803ca6'] = 'التحديث';
$_MODULE['<{dashtrends}pf_mixstore>dashboard_zone_two_e537825dd409a90ef70d8c2eb56122a1'] = 'مجموع الإيرادات (باستثناء الضرائب) ولدت داخل نطاق التاريخ بأوامر تعتبر التحقق من صحتها.';
$_MODULE['<{dashtrends}pf_mixstore>dashboard_zone_two_11ff9f68afb6b8b5b8eda218d7c83a65'] = 'المبيعات';
$_MODULE['<{dashtrends}pf_mixstore>dashboard_zone_two_8bc1c5ca521b99b87908db0bcd33ec76'] = 'تلقى إجمالي عدد الطلبات في نطاق التاريخ التي تعتبر التحقق من صحتها.';
$_MODULE['<{dashtrends}pf_mixstore>dashboard_zone_two_7442e29d7d53e549b78d93c46b8cdcfc'] = 'الطلبات';
$_MODULE['<{dashtrends}pf_mixstore>dashboard_zone_two_f15f2a2bf99d3dcad2cba1a2c615b9dc'] = 'متوسط ​​العربة القيمة هي مقياس تمثل قيمة متوسط ​​النظام داخل نطاق التاريخ. ويحسب بقسمة المبيعات بنسبة أوامر.';
$_MODULE['<{dashtrends}pf_mixstore>dashboard_zone_two_791d6355d34dfaf60d68ef04d1ee5767'] = 'Cart Value';
$_MODULE['<{dashtrends}pf_mixstore>dashboard_zone_two_4f631447981c5fa240006a5ae2c4b267'] = 'إجمالي عدد الزيارات ضمن نطاق التاريخ. والزيارة هي الفترة الزمنية التي تشارك مستخدم نشط مع موقع الويب الخاص بك.';
$_MODULE['<{dashtrends}pf_mixstore>dashboard_zone_two_d7e637a6e9ff116de2fa89551240a94d'] = 'الزيارات';
$_MODULE['<{dashtrends}pf_mixstore>dashboard_zone_two_7a6e858f8c7c0b78fb4d43cefcb8c017'] = 'التجارة الإلكترونية معدل التحويل هو نسبة الزيارات التي أدت في أمر التحقق من صحتها.';
$_MODULE['<{dashtrends}pf_mixstore>dashboard_zone_two_e4c3da18c66c0147144767efeb59198f'] = 'Conversion Rate';
$_MODULE['<{dashtrends}pf_mixstore>dashboard_zone_two_8dedc1b3ee3a92212fb5b5acad7f207f'] = 'صافي الربح هو مقياس لربحية المشروع بعد حساب جميع التكاليف التجارة الإلكترونية. يمكنك توفير هذه التكاليف عن طريق النقر على أيقونة التكوين الحق فوق هنا.';
$_MODULE['<{dashtrends}pf_mixstore>dashboard_zone_two_43d729c7b81bfa5fc10e756660d877d1'] = 'Net Profit';
