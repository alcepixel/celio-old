<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{pagesnotfound}pf_mixstore>pagesnotfound_251295238bdf7693252f2804c8d3707e'] = 'صفحات لم يتم العثور';
$_MODULE['<{pagesnotfound}pf_mixstore>pagesnotfound_607cc8b8993662a37cac86032fb071d2'] = 'ويضيف علامة تبويب الإحصاءات إلى لوحة القيادة، والتي تبين من خلال صفحات زوارك المطلوبة التي لم يتم العثور عليها.';
$_MODULE['<{pagesnotfound}pf_mixstore>pagesnotfound_dc3a3db6b98723bf91f924537a630600'] = 'تم إفراغ ذاكرة التخزين المؤقت \"صفحات غير موجودة\".';
$_MODULE['<{pagesnotfound}pf_mixstore>pagesnotfound_b323790d8ee3c43d317d19aea5012626'] = 'لقد تم حذف ذاكرة التخزين المؤقت \"صفحات غير موجودة\".';
$_MODULE['<{pagesnotfound}pf_mixstore>pagesnotfound_6602bbeb2956c035fb4cb5e844a4861b'] = 'المعالج';
$_MODULE['<{pagesnotfound}pf_mixstore>pagesnotfound_3604249130acf7fda296e16edc996e5b'] = '404 أخطاء';
$_MODULE['<{pagesnotfound}pf_mixstore>pagesnotfound_675f1f46497aeeee35832a5d9d095976'] = 'خطأ 404 هو رمز خطأ HTTP وهو ما يعني أن الملف من قبل المستخدم المطلوبة لا يمكن العثور عليه. في حالتك فهذا يعني أن واحدا من زوارك دخلت URL الخطأ في شريط العنوان، أو أنك أو موقع آخر له صلة ميت. عندما يكون ذلك ممكنا، يظهر المرجع حتى تتمكن من العثور على الصفحة / الموقع الذي يحتوي على الارتباط ميت. إذا لم يكن كذلك، فهذا يعني عادة أنه هو وصول مباشر، لذلك شخص قد مرجعية وصلة التي لا وجود لها بعد الآن.';
$_MODULE['<{pagesnotfound}pf_mixstore>pagesnotfound_a90083861c168ef985bf70763980aa60'] = 'كيفية التقاط هذه الأخطاء؟';
$_MODULE['<{pagesnotfound}pf_mixstore>pagesnotfound_4f803d59ee120b11662027e049cba1f3'] = 'إذا الإستضافة يدعم ملفات هتكس، يمكنك إنشاء واحد في الدليل الجذر من بريستاشوب وإدراج السطر التالي داخل: \"٪ S\".';
$_MODULE['<{pagesnotfound}pf_mixstore>pagesnotfound_07e7f83ae625fe216a644d09feab4573'] = 'سيتم إعادة توجيه المستخدم طلب صفحة التي لا وجود لها على الصفحة التالية:٪ S. هذه الوحدة بتسجيل الدخول إلى هذه الصفحة.';
$_MODULE['<{pagesnotfound}pf_mixstore>pagesnotfound_01bd0bf7c5a68ad6ee4423118be3f7b6'] = 'يجب استخدام ملف htaccess لإعادة توجيه 404 الأخطاء إلى صفحة \"404.php\".';
$_MODULE['<{pagesnotfound}pf_mixstore>pagesnotfound_193cfc9be3b995831c6af2fea6650e60'] = 'الصفحة';
$_MODULE['<{pagesnotfound}pf_mixstore>pagesnotfound_b6f05e5ddde1ec63d992d61144452dfa'] = 'المرجع';
$_MODULE['<{pagesnotfound}pf_mixstore>pagesnotfound_64d129224a5377b63e9727479ec987d9'] = 'عداد';
$_MODULE['<{pagesnotfound}pf_mixstore>pagesnotfound_4a7a7e7cda40454cee7ec247660f8017'] = 'لا \"الصفحة غير موجودة\" قضية سجلت حتى الآن.';
$_MODULE['<{pagesnotfound}pf_mixstore>pagesnotfound_d8847bc418fc4f5a3e37c2e8390bb9ed'] = 'قاعدة بيانات فارغة';
$_MODULE['<{pagesnotfound}pf_mixstore>pagesnotfound_b9ae3636d6e672413a163f7cb34beb84'] = 'ALL \"لم يتم العثور على صفحات\" إعلانات فارغة لهذه الفترة';
$_MODULE['<{pagesnotfound}pf_mixstore>pagesnotfound_0cf5c3a279c0e8c57b232d8c6bc3f06a'] = 'تفريغ جميع \"صفحات لم يتم العثور على\" إشعارات';
