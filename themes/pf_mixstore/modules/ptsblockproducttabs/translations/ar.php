<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{ptsblockproducttabs}pf_mixstore>ptsblockproducttabs_d9ef3673f23b00b43bf3a57cd99d1829'] = 'نقطة المنتج علامات بلوك';
$_MODULE['<{ptsblockproducttabs}pf_mixstore>ptsblockproducttabs_f7ff3e4bab5377469ca5cb1116f2905f'] = 'ويضيف لبنة مع عروض خاصة المنتجات الحالية مدعوم من الإطار ليو 2.0.';
$_MODULE['<{ptsblockproducttabs}pf_mixstore>ptsblockproducttabs_8dd2f915acf4ec98006d11c9a4b0945b'] = 'تحديث الإعدادات بنجاح.';
$_MODULE['<{ptsblockproducttabs}pf_mixstore>ptsblockproducttabs_f4f70727dc34561dfde1a3c529b6205c'] = 'الإعدادات';
$_MODULE['<{ptsblockproducttabs}pf_mixstore>ptsblockproducttabs_f44441b45f4e8dc7f75e9470a01952f9'] = 'عدد العناصر في الصفحة';
$_MODULE['<{ptsblockproducttabs}pf_mixstore>ptsblockproducttabs_eec0b0d9e243d2b626d7732f759ce46b'] = 'الحد الأقصى لعدد المنتجات في كل صفحة علامة التبويب (الافتراضي: 3).';
$_MODULE['<{ptsblockproducttabs}pf_mixstore>ptsblockproducttabs_e46be59191719d6eebf547872e26761f'] = 'عدد الأعمدة في الصفحة';
$_MODULE['<{ptsblockproducttabs}pf_mixstore>ptsblockproducttabs_409396efd362a2ee8e04e21b0b384015'] = 'الحد الأقصى لعدد الأعمدة في كل صفحة علامة التبويب (الافتراضي: 3).';
$_MODULE['<{ptsblockproducttabs}pf_mixstore>ptsblockproducttabs_7e29bc2364b066b48ee49bd7248acf82'] = 'عدد من المنتجات المعروضة في تبويب';
$_MODULE['<{ptsblockproducttabs}pf_mixstore>ptsblockproducttabs_66c4a387e5e977685503a5f4bb527b09'] = 'الحد الأقصى لعدد المنتجات في كل صفحة علامة التبويب (الافتراضي: 6).';
$_MODULE['<{ptsblockproducttabs}pf_mixstore>ptsblockproducttabs_33c3382149bb372309e84e6459ffc6b5'] = 'جميع المنتجات تبويب';
$_MODULE['<{ptsblockproducttabs}pf_mixstore>ptsblockproducttabs_05ee618c947f0fa8421a50ac086067fd'] = 'عرض جميع المنتجات من جميع علامات التبويب في هذا التبويب.';
$_MODULE['<{ptsblockproducttabs}pf_mixstore>ptsblockproducttabs_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'تمكين';
$_MODULE['<{ptsblockproducttabs}pf_mixstore>ptsblockproducttabs_b9f5c797ebbf55adccdd8539a65a0241'] = 'معاق';
$_MODULE['<{ptsblockproducttabs}pf_mixstore>ptsblockproducttabs_d34cd7cbbcd8d3296363084859d32597'] = 'تبويب خاص';
$_MODULE['<{ptsblockproducttabs}pf_mixstore>ptsblockproducttabs_a8a670d89a6d2f3fa59942fc591011ef'] = 'تظهر كتلة حتى لو لم يتوفر المنتج.';
$_MODULE['<{ptsblockproducttabs}pf_mixstore>ptsblockproducttabs_2891e6bd1b6e8b4be99a6941ff6f1ff0'] = 'أكثر الكتب مبيعا تبويب';
$_MODULE['<{ptsblockproducttabs}pf_mixstore>ptsblockproducttabs_aaffcd9ede4a83ea0678e1f8acb0f8bb'] = 'تبويب مميز';
$_MODULE['<{ptsblockproducttabs}pf_mixstore>ptsblockproducttabs_a93f0744f60adc61871989817f039d95'] = 'Arrials تبويب جديدة';
$_MODULE['<{ptsblockproducttabs}pf_mixstore>ptsblockproducttabs_7713570d69607fec64be59b39ea08610'] = 'كبار التقييم';
$_MODULE['<{ptsblockproducttabs}pf_mixstore>ptsblockproducttabs_d16dd01adf735ed9b87eebff5fc39ce5'] = 'الفاصلة';
$_MODULE['<{ptsblockproducttabs}pf_mixstore>ptsblockproducttabs_b24a524f3acce7d7d0ef2d3bd954d153'] = 'دخول الوقت (miniseconds) للعب دائري. قيمة 0 لإيقاف.';
$_MODULE['<{ptsblockproducttabs}pf_mixstore>ptsblockproducttabs_c9cc8cce247e49bae79f15173ce97354'] = 'حفظ';
$_MODULE['<{ptsblockproducttabs}pf_mixstore>ptsblockproducttabs_b1c94ca2fbc3e78fc30069c8d0f01680'] = 'جميع';
$_MODULE['<{ptsblockproducttabs}pf_mixstore>ptsblockproducttabs_31d26a6487e08357bd771619e894b0c6'] = 'القادمون الجدد';
$_MODULE['<{ptsblockproducttabs}pf_mixstore>ptsblockproducttabs_b4c2b550635fe54fd29f2b64dfaca55d'] = 'خاص';
$_MODULE['<{ptsblockproducttabs}pf_mixstore>ptsblockproducttabs_22c6735b370cecf293e32dca6c678185'] = 'أفضل البائع';
$_MODULE['<{ptsblockproducttabs}pf_mixstore>ptsblockproducttabs_cf8156f1f57a8603cd6b3a28c9c2c61b'] = 'المنتجات المميزة';
