<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_6154ed8c095e8ecbda9d4e51fc6fac14'] = 'نقطة كتلة طمأنة العملاء';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_7e7f70db3c75e428db8e2d0a1765c4e9'] = 'يضيف كتلة المعلومات التي تهدف إلى تقديم معلومات مفيدة لطمأنة العملاء بأن متجرك هو جدير بالثقة.';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_d52eaeff31af37a4a7e0550008aff5df'] = 'حدث خطأ أثناء محاولة حفظ.';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_0366c7b2ea1bb228cd44aec7e3e26a3e'] = 'تم تحديث تكوين كتلة.';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_8363eee01f4da190a4cef9d26a36750c'] = 'الجديد كتلة الطمأنينة';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_be53a0541a6d36f6ecb879fa2c584b08'] = 'صورة';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_b78a3223503896721cca1303f776159b'] = 'عنوان';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_9dffbf69ffba8bc38bc4e01abf4b1675'] = 'النص';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_fbd8a0acc91e582ddf8b9da85003f347'] = 'بالإضافة الدرجة';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_c9cc8cce247e49bae79f15173ce97354'] = 'حفظ';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_630f6dc397fe74e52d5189e2c80f282b'] = 'العودة إلى قائمة';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_b718adec73e04ce3ec720dd11a06a308'] = 'الهوية';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_23498d91b6017e5d7f4ddde70daba286'] = 'معرف شوب';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_ef61fb324d729c341ea8ab9901e23566'] = 'إضافة جديدة';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_f3295a93167b56c5a19030e91823f7bf'] = 'ضمان استعادة الاموال.';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_56e140ebd6f399c22c8859a694b247f3'] = 'في متجر الصرف.';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_597ce11744f9bbf116ec9e4a719ec9d3'] = 'الدفع عند الشحن.';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_3aca7ac5cf7e462b658960931946f824'] = 'الشحن مجانا.';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_d05244e0e410a6b85ed53a014908c657'] = '100٪ آمن عمليات الدفع.';
