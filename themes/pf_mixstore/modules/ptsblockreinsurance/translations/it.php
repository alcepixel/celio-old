<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_6154ed8c095e8ecbda9d4e51fc6fac14'] = 'Pt blocco rassicurazione del cliente';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_7e7f70db3c75e428db8e2d0a1765c4e9'] = 'Aggiunge un blocco di informazioni volto a offrire informazioni utili a rassicurare i clienti che il negozio è affidabile.';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_d52eaeff31af37a4a7e0550008aff5df'] = 'Si è verificato un errore durante il tentativo di salvare.';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_0366c7b2ea1bb228cd44aec7e3e26a3e'] = 'La configurazione del blocco è stata aggiornata.';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_8363eee01f4da190a4cef9d26a36750c'] = 'Nuovo blocco rassicurazione';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_be53a0541a6d36f6ecb879fa2c584b08'] = 'Immagine';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_b78a3223503896721cca1303f776159b'] = 'Titolo';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_9dffbf69ffba8bc38bc4e01abf4b1675'] = 'Testo';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_fbd8a0acc91e582ddf8b9da85003f347'] = 'Aggiunta di Classe';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_c9cc8cce247e49bae79f15173ce97354'] = 'Salva';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_630f6dc397fe74e52d5189e2c80f282b'] = 'Torna alla lista';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_b718adec73e04ce3ec720dd11a06a308'] = 'ID';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_23498d91b6017e5d7f4ddde70daba286'] = 'ID Negozio';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_ef61fb324d729c341ea8ab9901e23566'] = 'Aggiungi nuovo';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_f3295a93167b56c5a19030e91823f7bf'] = 'Garanzia di rimborso.';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_56e140ebd6f399c22c8859a694b247f3'] = 'In-store cambio.';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_597ce11744f9bbf116ec9e4a719ec9d3'] = 'Pagamento al momento della spedizione.';
$_MODULE['<{ptsblockreinsurance}pf_mixstore>ptsblockreinsurance_3aca7ac5cf7e462b658960931946f824'] = 'Spedizione gratuita.';
