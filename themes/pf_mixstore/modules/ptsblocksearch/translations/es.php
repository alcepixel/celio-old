<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{ptsblocksearch}pf_mixstore>categories_a6a2a55bea8760389dfca77132905b7c'] = 'Toutes les catégories';
$_MODULE['<{ptsblocksearch}pf_mixstore>ptsblocksearch-top_13348442cc6a27032d2b4aa28b75a5d3'] = 'Recherche';
$_MODULE['<{ptsblocksearch}pf_mixstore>ptsblocksearch_3202920c8c08cde64f9cc1447e78a951'] = 'Pts rapide bloc de recherche';
$_MODULE['<{ptsblocksearch}pf_mixstore>ptsblocksearch_be305c865235f417d9b4d22fcdf9f1c5'] = 'Ajoute un champ de recherche rapide vers votre site.';
$_MODULE['<{ptsblocksearch}pf_mixstore>ptsblocksearch_13348442cc6a27032d2b4aa28b75a5d3'] = 'Recherche';
$_MODULE['<{ptsblocksearch}pf_mixstore>ptsblocksearch_ce1b00a24b52e74de46971b174d2aaa6'] = 'Recherche de produits:';
$_MODULE['<{ptsblocksearch}pf_mixstore>ptsblocksearch_5f075ae3e1f9d0382bb8c4632991f96f'] = 'Aller';
$_MODULE['<{ptsblocksearch}pf_mixstore>ptsblocksearch-top_2a37324bb4c331c859044121df3f576b'] = 'Recherche ...';
$_MODULE['<{ptsblocksearch}pf_mixstore>ptsblocksearch-top_54f8356a6b5534f7b922c149df152fa6'] = 'Recherche ....';
