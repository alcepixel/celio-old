<div id="ptsbttestimonials{$ptsbttestimonials_modid}" class="block carousel slide ptsbttestimonials products-rows col-sm-12 col-md-6 col-lg-6">
	<div class="title_block">{l s="What clients say"}</div>	
	<div class="block_content">
	
		<div class="carousel-inner">
			{foreach from=$get_testimonials item=test name=testimonial}
				<div class="{if isset($active) && $active == 1} active{/if} item {if $smarty.foreach.testimonial.first}active{/if}">
					<div class="testimonial-item">
						{if $test.content}
							<div class="test-info">
							<blockquote>	<p class="text">{$test.content}</p></blockquote>
							</div>
						{/if}
						
						<div class="media-list">
							<div class="media">
						{if isset($test.avatar) && $test.avatar neq "" }
							<div class="t-avatar pull-left">
								<img class="img-circle" width="{$config_testimonials.media_width}" height="{$config_testimonials.media_height}" src="{$config_testimonials.img_link}{$test.avatar}" alt="{$test.note}" />
								{if isset($test.media_code) && $test.media_code neq "" }
									 <a class="fancybox-media" href="{$test.media_code|escape:'html':'UTF-8'}"><span class="icon icon-camera btn btn-primary"></span></a>
								{/if}
							</div>
						{/if}
							
						{if $test.name } 
						<div class="media-body">
							<div class="t-profile">	
								<div class="name">{$test.name} </div>
								<div class="job">{$test.address}</div>	
							</div>
						</div>
						{/if}
						</div>	
						</div>		

					</div>
				</div>
			{/foreach}
		</div>
	</div>
<ol class="carousel-linked-nav pagination">
	{foreach from=$get_testimonials item=test name=testimonial}
  		<li {if $smarty.foreach.testimonial.index == 0}class="active"{/if}>
  			<a href="#{$smarty.foreach.testimonial.index+1}" title="{$smarty.foreach.testimonial.index+1}"></a>
  		</li>
 	{/foreach}
</ol>
	<script type="text/javascript">
	$(document).ready(function() {
		$('.fancybox-media').fancybox({
			openEffect  : 'none',
			closeEffect : 'none',
			helpers : {
				media : {}
			}
		});


		$('.carousel-linked-nav > li > a').click(function() {
		    var item = Number($(this).attr('href').substring(1));
		    $('#ptsbttestimonials{$ptsbttestimonials_modid}').carousel(item - 1);
		    $('.carousel-linked-nav .active').removeClass('active');
		    $(this).parent().addClass('active');
		    return false;
		});

		$('#ptsbttestimonials{$ptsbttestimonials_modid}').bind('slid', function() {
		    $('.carousel-linked-nav .active').removeClass('active');
		    var idx = $('#ptsbttestimonials{$ptsbttestimonials_modid} .item.active').index();
		    $('.carousel-linked-nav li:eq(' + idx + ')').addClass('active');
		});


	});


	</script>

</div>