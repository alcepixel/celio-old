{if !empty($products)}

<div class="pts-carousel slide boxcarousel row" id="{$tabname}">
	{if count($products)>$itemsperpage}	 
		<div class="carousel-controls">
		 	<a class="carousel-control left" href="#{$tabname}"   data-slide="prev">&lsaquo;</a>
			<a class="carousel-control right" href="#{$tabname}"  data-slide="next">&rsaquo;</a>
		</div>
	{/if}
	<div class="carousel-inner"> 
	{$mproducts=array_chunk($products,$itemsperpage)}
	{foreach from=$mproducts item=products name=mypLoop}
			<!-- Products list -->
			<ul{if isset($id) && $id} id="{$id}"{/if} class="product_list products-block grid {if isset($class) && $class} {$class}{/if}{if isset($active) && $active == 1} active{/if} item {if $smarty.foreach.mypLoop.first}active{/if}  {if Configuration::get('PTS_CP_PRODUCT_STYLE') == 'style1'}style1{elseif Configuration::get('PTS_CP_PRODUCT_STYLE') == 'style2'}style2 {elseif Configuration::get('PTS_CP_PRODUCT_STYLE') == 'style3'}style3 {elseif Configuration::get('PTS_CP_PRODUCT_STYLE') == 'style4'}style4 {elseif Configuration::get('PTS_CP_PRODUCT_STYLE') == 'style5'}style5{/if}">
			{foreach from=$products item=product name=products}
				<li class="ajax_block_product col-xs-12 col-sm-4 col-md-{$scolumn}{if $smarty.foreach.products.iteration%$column == 0} last-in-line{elseif $smarty.foreach.products.iteration%$column == 1} first-in-line{/if}">
		 			{if Configuration::get('PTS_CP_PRODUCT_STYLE') == 'style1'}
						{include file="$tpl_dir./sub/product/style1.tpl" product=$product class=''}
					{elseif Configuration::get('PTS_CP_PRODUCT_STYLE') == 'style2'}
						{include file="$tpl_dir./sub/product/style2.tpl" product=$product class=''}
					{elseif Configuration::get('PTS_CP_PRODUCT_STYLE') == 'style3'}
						{include file="$tpl_dir./sub/product/style3.tpl" product=$product class=''}
					{elseif Configuration::get('PTS_CP_PRODUCT_STYLE') == 'style4'}
						{include file="$tpl_dir./sub/product/style4.tpl" product=$product class=''}
					{elseif Configuration::get('PTS_CP_PRODUCT_STYLE') == 'style5'}
						{include file="$tpl_dir./sub/product/style5.tpl" product=$product class=''}
					{/if}
				</li>
			{/foreach}
			</ul>	
	{/foreach}
	</div>
</div>
{/if}