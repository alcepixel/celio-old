{if !empty($products)}
<div class="pts-carousel boxcarousel slide" id="{$tabname}">
	{if count($products)>$itemsperpage}	 
		<div class="carousel-controls">
		 	<a class="carousel-control left" href="#{$tabname}"   data-slide="prev">&lsaquo;</a>
			<a class="carousel-control right" href="#{$tabname}"  data-slide="next">&rsaquo;</a>
		</div>
	{/if}
	<div class="carousel-inner ptsproductcountdown">
	{$mproducts=array_chunk($products,$itemsperpage)}
	{foreach from=$mproducts item=products name=mypLoop}
			<!-- Products list -->
			<ul{if isset($id) && $id} id="{$id}"{/if} class="product_list products-block grid  row{if isset($class) && $class} {$class}{/if}{if isset($active) && $active == 1} active{/if} item {if $smarty.foreach.mypLoop.first}active{/if}">
			{foreach from=$products item=product name=products}
				
				<li class="ajax_block_product col-xs-12 col-sm-12 col-md-{$scolumn}">
					 
					<div class="product-block clearfix" itemscope="" itemtype="http://schema.org/Product"><div class="product-container">

						<div class="product-image-container image">
								<a class="img product_img_link"	href="{$product.link|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" itemprop="url">
									<img class="replace-2x img-responsive" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html':'UTF-8'}" alt="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}" title="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}" {if isset($homeSize)} width="{$homeSize.width}" height="{$homeSize.height}"{/if} itemprop="image" />
								</a>
						</div>
						<div class="product-body clearfix">
							<div class="product-meta pull-left">
										<h3 class="name" itemprop="name">
											{if isset($product.pack_quantity) && $product.pack_quantity}{$product.pack_quantity|intval|cat:' x '}{/if}
											<a href="{$product.link|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" itemprop="url" >
												{$product.name|truncate:45:'...'|escape:'html':'UTF-8'}
											</a>
										</h3>
										{hook h='displayProductListReviews' product=$product}
										{if (!$PS_CATALOG_MODE && $PS_STOCK_MANAGEMENT && ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
											{if isset($product.available_for_order) && $product.available_for_order && !isset($restricted_country_mode)}
												<span itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="availability">
													{if ($product.allow_oosp || $product.quantity > 0)}
														<span class="{if $product.quantity <= 0}out-of-stock{else}available-now{/if}">
															<link itemprop="availability" href="http://schema.org/InStock" />
															{if $product.quantity <= 0}
																{if $product.allow_oosp}
																	{$product.available_later}
																{else}
																	{l s='Out of stock'}
																{/if}
															{else}
															{if isset($product.available_now) && $product.available_now}{$product.available_now}{else}{l s='In Stock'}{/if}{/if}
														</span>
													{elseif (isset($product.quantity_all_versions) && $product.quantity_all_versions > 0)}
														<span class="available-dif">
															<link itemprop="availability" href="http://schema.org/LimitedAvailability" />{l s='Product available with different options'}
														</span>
													{else}
														<span class="out-of-stock">
															<link itemprop="availability" href="http://schema.org/OutOfStock" />{l s='Out of stock'}
														</span>
													{/if}
												</span>
											{/if}
										{/if}
								</div>	
												
							{if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
								<div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="content_price price pull-right">

									{if isset($product.show_price) && $product.show_price && !isset($restricted_country_mode)}
										{if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
											<span class="old-price">
												{displayWtPrice p=$product.price_without_reduction}
											</span>
											<br/>
											
										{/if}
										<span itemprop="price" class="product-price">
											{if !$priceDisplay}{convertPrice price=$product.price}{else}{convertPrice price=$product.price_tax_exc}{/if}
										</span><br/>
										<meta itemprop="priceCurrency" content="{$priceDisplay}" />
										
									{/if}
								</div>
							{/if}
							<div class="clearfix"></div>
							 <div class="pts-countdown-{$product.id_product} item-countdown clearfix">
			                                        {if $product.js == 'unlimited'}<div class="lof-labelexpired">{l s='Unlimited' mod='ptsproductcountdown'}</div>{/if}
			                                </div>
			                                {if $product.js != 'unlimited'}	
			                                    <script type="text/javascript">
			                                        {literal}
			                                        jQuery(document).ready(function($){{/literal}
			                                            $(".pts-countdown-{$product.id_product}").lofCountDown({literal}{{/literal}
			                                                TargetDate:"{$product.js.month}/{$product.js.day}/{$product.js.year} {$product.js.hour}:{$product.js.minute}:{$product.js.seconds}",
			                                                DisplayFormat:"<ul><li><div class=\"countdown_num\">%%D%% </div><div>{l s='Days' mod='ptsproductcountdown'}</div></li><li><div class=\"countdown_num\">%%H%% </div><div>{l s='Hours' mod='ptsproductcountdown'}</div></li><li><div class=\"countdown_num\">%%M%%</div> <div>{l s='Minutes' mod='ptsproductcountdown'}</div></li><li><div class=\"countdown_num\">%%S%%</div><div> {l s='Seconds' mod='ptsproductcountdown'}</div></li></ul>",
			                                                FinishMessage: "{$product.finish}"
			                                            {literal}
			                                            });
			                                        });
			                                        {/literal}
			                                    </script>
			                                {/if}
			            </div>                    
					</div><!-- .product-container> --></div>
				</li>
			{/foreach}
			</ul>	
	{/foreach}
	</div>
</div>
{/if}