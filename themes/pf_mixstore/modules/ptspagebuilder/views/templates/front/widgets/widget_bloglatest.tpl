{*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<!-- MODULE Block ptsblockblogstabs --> 
<div id="ptsblockblogstabs{$tabname}" class="widget-latestblog block {$addition_cls} {if isset($stylecls)&&$stylecls}block-{$stylecls}{/if}">
	<h4 class="title_block">
		<span class="blog_title">{l s='Latest Blogs' mod='ptspagebuilder'}</span>
		
	</h4>
	<div class="block_content">	
		{if !empty($blogs )}
	 
			{if !empty($blogs)}
		<div class="slide boxcarousel" id="{$tabname}">
			 {if count($blogs)>$itemsperpage}
			 <div class="carousel-controls">	 
			 	<a class="carousel-control left" href="#{$tabname}"   data-slide="prev">&lsaquo;</a>
				<a class="carousel-control right" href="#{$tabname}"  data-slide="next">&rsaquo;</a>
			</div>
			{/if}
			<div class="carousel-inner">
			{$mblogs=array_chunk($blogs,$itemsperpage)}
			{foreach from=$mblogs item=blogs name=mypLoop}
				<div class="item {if $smarty.foreach.mypLoop.first}active{/if}">
						{foreach from=$blogs item=blog name=blogs}
						{if $blog@iteration%$columnspage==1&&$columnspage>1}
						  <div class="row">
						{/if}
						<div class="col-xs-12 col-sm-{$scolumn} col-md-{$scolumn} col-lg-{$scolumn} blog_block ajax_block_blog {if $smarty.foreach.blogs.first}first_item{elseif $smarty.foreach.blogs.last}last_item{/if}">
							{if $list_mode == 'grid'}
							<div class="blog_container clearfix">

									{if $blog.image && $show_image}
									<div class="blog-image">
										<a href="{$blog.link|escape:'html':'UTF-8'}" title="{$blog.title|escape:'html':'UTF-8'}">
											<img alt="" src="{$blog.preview_url}" title="{$blog.title|escape:'html':'UTF-8'}" class="img-responsive"/>
										</a>
									</div>
									{/if}
									<div class="blog-body">
											{if $show_title}
												<h5 class="blog-title">
													<a href="{$blog.link|escape:'html':'UTF-8'}" title="{$blog.title|escape:'html':'UTF-8'}">{$blog.title}</a>
												</h5>

											{/if}
										{if  $show_category || $show_comment}
										<div class="blog-meta">
											{if $show_category}
												<span class="blog-cat"> <span class="icon-list">{l s='In' module='ptsblockblogs'}</span> 
													<a href="{$blog.category_link|escape:'html':'UTF-8'}" title="{$blog.category_title|escape:'html':'UTF-8'}">{$blog.category_title}</a>
												</span>
											{/if}
											{if $show_comment}
												<span class="blog-ctncomment">
													<span class="icon-comment"> {l s='Comment' mod='ptspagebuilder'}:</span> {$blog.comment_count}
												</span>
											{/if}
										</div>
										{/if}

										{if $show_description}
											<div class="blog-shortinfo">
												{$blog.description|strip_tags|truncate:100}
											</div>
										{/if}

										{if $show_dateadd}
												
											<div class="created pull-left">
												<div class="create-date">
													<span>
														{strtotime($blog.date_add)|date_format:" %B %e, %Y"}
													</span>
												</div>
											</div>
										{/if}
										{if $show_readmore}
											<div class="readmore pull-right">
													<a href="{$blog.link|escape:'html':'UTF-8'}" title="{$blog.title|escape:'html':'UTF-8'}">{l s='Read more' mod='ptspagebuilder'}</a>
											</div>
										{/if}
									</div>									
								</div>
							{else}
								<div class="blog_container media clearfix">

									{if $blog.image && $show_image}
									<div class="blog-image pull-left">
										<a href="{$blog.link|escape:'html':'UTF-8'}" title="{$blog.title|escape:'html':'UTF-8'}" class="link">
											<img alt="" src="{$blog.preview_url}" title="{$blog.title|escape:'html':'UTF-8'}" class="img-responsive"/>
										</a>
									</div>
									{/if}
									<div class="media-body">
											{if $show_title}
												<h5 class="blog-title">
													<a href="{$blog.link|escape:'html':'UTF-8'}" title="{$blog.title|escape:'html':'UTF-8'}">{$blog.title}</a>
												</h5>

											{/if}
										{if  $show_category || $show_comment}
										<div class="blog-meta">
											{if $show_category}
												<span class="blog-cat"> <span class="icon-list">{l s='In' module='ptsblockblogs'}</span> 
													<a href="{$blog.category_link|escape:'html':'UTF-8'}" title="{$blog.category_title|escape:'html':'UTF-8'}">{$blog.category_title}</a>
												</span>
											{/if}
											{if $show_comment}
												<span class="blog-ctncomment">
													<span class="icon-comment"> {l s='Comment' mod='ptspagebuilder'}:</span> {$blog.comment_count}
												</span>
											{/if}
										</div>
										{/if}

										{if $show_description}
											<div class="blog-shortinfo">
												{$blog.description|strip_tags|truncate:100}
											</div>
										{/if}

										{if $show_dateadd}
												
											<div class="created pull-left">
												<div class="create-date">
													<span>
														{strtotime($blog.date_add)|date_format:" %B %e, %Y"}
													</span>
												</div>
											</div>
										{/if}
										{if $show_readmore}
											<div class="readmore pull-right">
													<a href="{$blog.link|escape:'html':'UTF-8'}" title="{$blog.title|escape:'html':'UTF-8'}">{l s='Read more' mod='ptspagebuilder'}</a>
											</div>
										{/if}
									</div>									
								</div>

							{/if}
						</div>
						
						{if ($blog@iteration%$columnspage==0||$smarty.foreach.blogs.last)&&$columnspage>1}
						</div>
						{/if}
							
						{/foreach}
				</div>		
			{/foreach}
			</div>
		</div>
		{/if}
		{/if}
	</div>
	{if $config->get('blockpts_blogs_show',1)}
		<div class="blog-viewmore text-right"><a href="{$view_all_link|escape:'html':'UTF-8'}" title="{l s='View All' mod='ptspagebuilder'}">
				<span>{l s='View All' mod='ptspagebuilder'}</span>
			</a></div>

	{/if}	
</div>
<!-- /MODULE Block ptsblockblogstabs -->
<script type="text/javascript">
$(document).ready(function() {
    $('{$tabname}').each(function(){
        $(this).carousel({
            pause: true,
            interval: {$interval}
        });
    });
});
</script>