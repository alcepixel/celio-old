{if $href}
	<div class="widget-button block {$addition_cls} {if isset($stylecls)&&$stylecls}block-{$stylecls}{/if}">
		{if isset($widget_heading)&&!empty($widget_heading)}
			<h4 class="title_block">
				{$widget_heading}
			</h4>
		{/if}
		<div class="widget-inner block_content">
			<a href="{$href}" class="btn {$color_button}{$el_class} {$size}" alt="{$button_title}">
				<i class="{$icon}"></i>
				{$button_title}
			</a>
		</div>
	</div>
{/if}