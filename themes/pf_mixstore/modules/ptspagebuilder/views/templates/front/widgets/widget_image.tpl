{if isset($thumbnailurl)}
<div class="widget-images block {$addition_cls} {if isset($stylecls)&&$stylecls}block-{$stylecls}{/if}">
	{if isset($widget_heading)&&!empty($widget_heading)}
	<h4 class="title_block">
		{$widget_heading}
	</h4>
	{/if}
	<div class="widget-inner block_content clearfix">
		 <div class="image-item">
		 	{if $ispopup}
		 		<a href="{$imageurl}" class="pts-popup fancybox" title="{l s='Large Image' mod='pspagebuilder'}"><span class="icon icon-search-plus"></span></a>
		 	{/if}
		 	{if $link_url}
		 	<a href="{$link_url}" class="pts-btnlink img-animation" title="{l s='Large Image' mod='pspagebuilder'}">
		 	<img alt="" src="{$thumbnailurl}"/></a>
		 	{else}

		 	<span class="img-animation"><img alt="" src="{$thumbnailurl}"/></span>
			{/if}
		 </div>
	</div>
</div>
{/if} 