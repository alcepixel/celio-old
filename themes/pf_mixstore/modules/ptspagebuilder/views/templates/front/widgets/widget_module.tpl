{if $content}
	<div class="widget-module {$addition_cls} {if isset($stylecls)&&$stylecls}block-{$stylecls}{/if} block">
		{if isset($widget_heading)&&!empty($widget_heading)}
			<h4 class="title_block">
				{$widget_heading}
			</h4>
		{/if}
		<div class="widget-inner block_content">
			 {$content}
		</div>
	</div>
{/if}