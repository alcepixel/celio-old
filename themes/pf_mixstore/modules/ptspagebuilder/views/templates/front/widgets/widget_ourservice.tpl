
<div class="widget-ourservice block {$addition_cls} {if isset($stylecls)&&$stylecls}block-{$stylecls}{/if}">
	{if isset($widget_heading)&&!empty($widget_heading)}
		<h4 class="ourservice-heading">
			{$widget_heading}
		</h4>
	{/if}
	<div class="media {if $icon_position == 'top' || $icon_position == 'bottom'}text-center{/if}">
	{if $icon_position == 'left' || $icon_position == 'right' || $icon_position == 'top' }
		{if $icon || $thumbnailurl}
		<div class="{if $icon_position == 'left'}pull-left {elseif $icon_position == 'right'}pull-right{elseif $icon_position == 'top'}top{/if} ">
			{if $thumbnailurl}
				<img src="{$thumbnailurl}" alt="icon"/>
			{elseif $icon}
				<i class="icon {$icon}"></i>
			{/if}
		</div>
		{/if}
	{/if}
		<div class="media-body">
			<div class="ourservice-content">
				{$content}
			</div>
		</div>
		{if $icon_position == 'bottom'}
			{if $icon || $thumbnailurl}
			<div class="{if $icon_position == 'bottom'}bottom{/if}  ">
				{if $thumbnailurl}
					<img src="{$thumbnailurl}" alt="icon"/>
				{elseif $icon}
					<i class="icon {$icon}"></i>
				{/if}
			</div>
			{/if}
		{/if}
	</div>
</div>