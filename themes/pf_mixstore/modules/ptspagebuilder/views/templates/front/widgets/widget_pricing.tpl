<div class="pricing-table  {$class} text-center">
	<div class="pricing-heading">
		<h4 class="no-margin">{$widget_heading}</h4>
		<div class="plan-price" ><sup class="plan-currency"></sup> <span><?php echo $price; ?></span> / <sup class="plan-period">{$period}</sup> </div>
	</div>
	<div class="pricing-body">
		 <div class="plain-info">
			 {$content}
		</div>
	</div>
	<div class="pricing-footer"><a class="btn btn-lg btn-block btn-outline plan-link" href="<?php echo $link; ?>">{$linktitle}</a></div>
</div>


