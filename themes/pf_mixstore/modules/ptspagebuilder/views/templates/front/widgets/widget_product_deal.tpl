 {if isset($products) && !empty($products)}
<div class="widget widget-product_deal {$addition_cls} block">
	{if isset($widget_heading)&&!empty($widget_heading)}
	<h4 class="title_block">
		{$widget_heading}
	</h4>
	{/if}
	<div class="widget-inner block_content">
		
		{if isset($products) AND $products}
	 		{$tabname=rand()+count($products)}
	 		{$scolumn=floor(12/$column)}
			{include file="{$product_tpl}" products=$products scolumn=$scolumn} 
		{/if}

	</div>
</div>
{/if}