 {if isset($products) && !empty($products)}
<div class="widget widget-products {$addition_cls} {if isset($stylecls)&&$stylecls}block-{$stylecls}{/if} block">
	{if isset($widget_heading)&&!empty($widget_heading)}
	<h4 class="title_block">
		{$widget_heading}
	</h4>
	{/if}
	<div class="widget-inner block_content">
		
		{if isset($products) AND $products}
	 		{$tabname=rand()+count($products)}

			{$scolumn=floor(12/$column)}
			{if $list_mode == "grid"}
				{include file="{$product_tpl}" products=$products scolumn=$scolumn}  
			{elseif $list_mode == "list1"}
				{include file="$tpl_dir./modules/ptspagebuilder/views/templates/front/widgets/sub/products_list1.tpl" products=$products scolumn=$scolumn}
			{else}
				{include file="$tpl_dir./modules/ptspagebuilder/views/templates/front/widgets/sub/products_list2.tpl" products=$products scolumn=$scolumn}
			{/if}
		{/if}
	</div>
</div>
{/if}