{if isset($producttabs)}
<div class="widget-producttabs block {$addition_cls}">
 
	<div class="widget-inner  pts-tab clearfix">
		<div class="tab-nav">
			<ul  class="nav nav-theme clearfix">
			{foreach from=$producttabs item=ptab name=myproducttabs}
				{if !empty($ptab.products)}
				<li{if $smarty.foreach.myproducttabs.index == 0} class="active"{/if}><a data-toggle="tab" href="#tab-{$ptab.key}">{$ptab.title}</a></li>
				{/if}
			{/foreach}
			</ul>
		</div>
		<div id="product_tab_content">
		<div class="tab-content">
			{foreach from=$producttabs item=ptab name=myproducttabs}
				{if !empty($ptab.products)}
				<div id="tab-{$ptab.key}" class="producttab-content tab-pane {if $smarty.foreach.myproducttabs.index == 0}active{/if}">
					{$tabname=$ptab.key}
			 		{$scolumn=floor(12/$column)}
					{include file="{$product_tpl}" products=$ptab.products scolumn=$scolumn} 
				</div>
				{/if}
			{/foreach}
			</div>
		</div>
	</div>
</div>
{/if} 