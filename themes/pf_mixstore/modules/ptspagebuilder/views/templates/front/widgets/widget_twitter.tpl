{if isset($username)}
<div class="widget-twitter block {$addition_cls} {if isset($stylecls)&&$stylecls}block-{$stylecls}{/if}">
	{if isset($widget_heading)&&!empty($widget_heading)}
	<h4 class="title_block">
		{$widget_heading}
	</h4>
	{/if}
	<div class="widget-inner block_content">
		<a class="twitter-timeline" data-dnt="true" data-theme="{$theme}" data-link-color="{$link_color}" style="width:{$width}; height:{$height}" data-chrome="{$chrome} transparent" data-border-color="{$border_color}" lang="EN" data-tweet-limit="{$count}" data-show-replies="{$show_replies}" href="https://twitter.com/{$username}"  data-widget-id="{$twidget_id}">Tweets by @{$username}</a>
		{$js}
	</div>
</div>
{/if} 

<script type="text/javascript">
// Customize twitter feed
var hideTwitterAttempts = 0;
function hideTwitterBoxElements() {
 setTimeout( function() {
  if ( $('.widget-twitter').length ) {
   $('.widget-twitter .widget-inner iframe').each( function(){
    var ibody = $(this).contents().find( 'body' );
    if ( ibody.find( '.timeline .stream .h-feed li.tweet' ).length ) {
     ibody.find( '.header .p-nickname' ).css( 'color', '{$nickname_color}' );
     ibody.find( '.p-name' ).css( 'color', '{$name_color}' );
     ibody.find( '.e-entry-title' ).css( 'color', '{$title_color}' );
    } else {
     $(this).hide();
    }
   });
  }
  hideTwitterAttempts++;
  if ( hideTwitterAttempts < 3 ) {
   hideTwitterBoxElements();
  }
 }, 2500);
}
// somewhere in your code after html page load
hideTwitterBoxElements();
</script>