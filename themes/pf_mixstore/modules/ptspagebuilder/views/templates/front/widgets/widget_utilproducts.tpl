{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

 {if isset($products) && !empty($products)}
    <div class="widget widget-products {$addition_cls} {if isset($stylecls)&&$stylecls}block-{$stylecls}{/if} block">

    {if isset($widget_heading)&&!empty($widget_heading)}
    <h4 class="title_block">
        {$widget_heading}
    </h4>
    {/if}
    
    <div class="widget-inner block_content">
        {$tabname = "utilproduct_list"}

        {if !empty($products)}
                 <div class="util-carousel top-nav-box {if Configuration::get('PTS_CP_PRODUCT_STYLE') == 'style1'}style1{elseif Configuration::get('PTS_CP_PRODUCT_STYLE') == 'style2'}style2 {elseif Configuration::get('PTS_CP_PRODUCT_STYLE') == 'style3'}style3 {elseif Configuration::get('PTS_CP_PRODUCT_STYLE') == 'style4'}style4 {elseif Configuration::get('PTS_CP_PRODUCT_STYLE') == 'style5'}style5{/if}" id="{$tabname}" >
        {foreach from=$products item=product name=products}
            <div class="pts-product_list ajax_block_product {if $smarty.foreach.products.iteration%$column == 0} last-in-line{elseif $smarty.foreach.products.iteration%$column == 1} first-in-line{/if}">
                {if Configuration::get('PTS_CP_PRODUCT_STYLE') == 'style1'}
                        {include file="$tpl_dir./sub/product/style1.tpl" product=$product class=''}
                    {elseif Configuration::get('PTS_CP_PRODUCT_STYLE') == 'style2'}
                        {include file="$tpl_dir./sub/product/style2.tpl" product=$product class=''}
                    {elseif Configuration::get('PTS_CP_PRODUCT_STYLE') == 'style3'}
                        {include file="$tpl_dir./sub/product/style3.tpl" product=$product class=''}
                    {elseif Configuration::get('PTS_CP_PRODUCT_STYLE') == 'style4'}
                        {include file="$tpl_dir./sub/product/style4.tpl" product=$product class=''}
                    {elseif Configuration::get('PTS_CP_PRODUCT_STYLE') == 'style5'}
                        {include file="$tpl_dir./sub/product/style5.tpl" product=$product class=''}
                    {/if}
            </div>
        {/foreach}
                
            </div>
        {/if}


    </div>
</div>
{/if}

<!-- /MODULE Block ptsutilfeaturedproduct -->
{literal}
<script type="text/javascript">
    $(document).ready(function() {
        $('#{/literal}{$tabname}'{literal}).utilCarousel({
            pagination : false,
            pause: 'hover',
            interval: {/literal}{$utilinterval}{literal},
            autoPlay : {/literal}{if $utilinterval==0}false{else}true{/if}{literal},
            responsiveMode : 'itemWidthRange',
            itemWidthRange : [{/literal}{$utilmin_width}{literal}, {/literal}{$utilmax_width}{literal}],
            navigation : true,
            navigationText : ['‹', '›']
        });
    });
</script>
{/literal}
