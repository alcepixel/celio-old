{if isset($video_link)}
<div class="widget block widget-video {if isset($stylecls)&&$stylecls}block-{$stylecls}{/if}">
 {if isset($widget_heading)&&!empty($widget_heading)}
 <h4 class="title_block">
  {$widget_heading}
 </h4>
 {/if}
 <div class="widget-inner block_content">
  <iframe src="{$video_link}" style="width:{$width};height:{$height};"></iframe>
  {if $subinfo}
  <div class="subinfo">{$subinfo}</div>
  {/if}
 </div>
</div>
{/if}