<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_24453fb9d8d0309ee63f4222b1fd0ce1'] = 'نقطة الشريط الجانبي الاجتماعي';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_0675372b875f0246825d054b94c4c47f'] = 'عرض الشريط الجانبي الاجتماعي.';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_3d971943089a3388c01fb297a32d9ba7'] = 'شفاف';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_9914a0ce04a7b7b6a8e39bec55064b82'] = 'ضوء';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_994ae1d9731cebe455aff211bcb25b93'] = 'اللون الرمادي';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_a862bce6269b42e31e60994cb0dc295d'] = 'Drark';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_f4f70727dc34561dfde1a3c529b6205c'] = 'الإعدادات';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_d721757161f7f70c5b0949fdb6ec2c30'] = 'موضوع';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_6fd3c76d0413e364d2ee4a6d605c977a'] = 'استجابة ماكس العرض';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_915607560a045c6313283246b947852d'] = 'استجابة مين العرض';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_eae639a70006feff484a39363c977e24'] = 'نطاق';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_e80c71a31d91b46e7d22963a49c73634'] = 'أضف هذا ID';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_621b8e807a05ee4389bef2c46c238517'] = 'إذا كنت لا حساب في';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_855d24595e8b1adaac4c387b74a8d849'] = 'يمكنك';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_76ea0bebb3c22822b4f0dd9c9fd021c5'] = 'خلق';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_fc0866d94b8aad1493e7bc2e8927da81'] = 'حساب و';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_ed3c125da2b94107ea0ba340459837b7'] = 'الحصول على معرف البيانات';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_aa99c38247152ca9c466cf4669b2fd1e'] = 'للمساهمة في هذه المعلمة.';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_c9cc8cce247e49bae79f15173ce97354'] = 'حفظ';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_a4ffdcf0dc1f31b9acaf295d75b51d00'] = 'أعلى';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_2ad9d63b69c4a10a5cc9cad923133bc4'] = 'أسفل';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_7d544e9cb5619c519aaed0c6bd76471c'] = 'موبايل التكوين';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_2d53839e403957e2427232c676dc3151'] = 'زر شريط الوظيفة';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_d78fd18d6e1781e1dc52099527c4ef03'] = 'الموضوع المحمول';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_ec53a8c4f07baed5d8825072c89799be'] = 'وضع';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_93cba07454f06a4a960172bbd6e2a435'] = 'نعم';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_bafd7322c6e97d25b6299b5d6fe8920b'] = 'لا';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_17263426fbda45430b5227729cc9d19f'] = 'أووتسيتي الحق';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_929717c9862190cfcb2a3196914975d6'] = 'أووتسيتي اليسار';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_d85544fce402c7a2a96a48078edaf203'] = 'الفيسبوك';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_2491bc9c7d8731e1ae33124093bc7026'] = 'لغو';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_c309e69f7a616a117782bdf96fcb981e'] = 'Y! بريد';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_a6ab200b051faf31aaaa04c8bcffd3c9'] = 'ZingMe';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_b0286125d3b86d4630070f9e3abd79c5'] = 'بينتيريست يعلقون عليه';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_d3da97e2d9aee5c8fbe03156ad051c99'] = 'أكثر';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_13dba24862cf9128167a59100e154c8d'] = 'طباعة';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_ce8ae9da5b7cd6c3df2929543a9af92d'] = 'البريد الإلكتروني';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_0dfec3de88c4e6eead14e739f55bfb26'] = 'نعرفكم';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_e884c507c5198a4578a84498f7a323e2'] = 'ينكدين';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_d78986947356ddd37b43d57df289dee9'] = 'المفضلة';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_7d53cbd3f0c0e2af31c3ba1eab940f22'] = 'جوجل';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_6b4d351a9d2e2c5aa735e93220d0c33d'] = 'حصة على + Google';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_6a13a3fa88eacf63c653d5b95491cff2'] = 'هوتميل';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_d6a545e5e15ced5a0e02e179b8c108c7'] = 'LinkShares';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_e760f0b08771bce2da01c196c58e8162'] = 'ماي سبيس';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_98507888a6a94973dac70d1281aa3cdf'] = 'PrintFriendly';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_da8db33f835160e71f8659e8a7b1dd4b'] = 'Virb';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_c87b64790f97245f6a764fb5d031f95a'] = 'Webnews';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_f51474f3ebf9dfa487b5704b72880493'] = 'نوافذ الأدوات';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_fde31686817fe5029025586d51b35861'] = 'وورد';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_43e9fc73ea3d79d23e52fda9d380bc65'] = 'Yigg';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_7bc11c185d7cf987d6663c6eb46b9f0e'] = 'ZicZac';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_5a95a425f74314a96f13a2f136992178'] = 'حصة';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_b405aa4451afc09c32917dc75d901937'] = 'موقف حصة';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_00817efa63b9bf81eb6fe826847e5938'] = 'الخدمات المختارة';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_55d28d8e00859c3fcbf34c7c0387cbf9'] = 'شكرا لك عنوان';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_82c23100a5932bf76bce2a7a1d44a292'] = 'اتبع رسالة';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_f3432946876a7d11e558cca9f40b0c03'] = 'حصة الموضوع';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_5648946511d4153b5192d68a96f98c8f'] = 'عرض سطح المكتب';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_3c6caae64841f86a36a4995c1d7274c8'] = 'المعرض الجوال';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_3903aab323863bd2e9b68218a7a65ebd'] = 'اتبع';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_09c979f62c1a490b83d4e286fb208283'] = 'عنوان المتابعة';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_beefce2a91641d1a128952ccb8464ac1'] = 'متابعة جوجل';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_970cfba66b8380fb97b742e4571356c6'] = 'يوتيوب';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_3b0f3a25d07e5d9dbdf98db15ee70410'] = 'فليكر';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_15db599e0119be476d71bfc1fda72217'] = 'فيميو';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_86709a608bd914b28221164e6680ebf7'] = 'بينتيريست';
$_MODULE['<{ptssocialsidebar}pf_mixstore>ptssocialsidebar_55f015a0c5605702f913536afe70cfb0'] = 'إينستاجرام';
