{if isset($htmlitems) && $htmlitems}

{if (int)$hookcols>0}
{assign var="hcol" value=12/$hookcols}
{else}
{assign var="hcol" value=4}
{/if}

<div id="ptsstaticontent_{$hook}" class="block {if $hook==footertop} col-xs-12 col-sm-12 col-md-9 col-lg-9{/if}">
    <ul class="staticontent clearfix {if $hook==bottom} row{/if}">
        {foreach name=items from=$htmlitems item=hItem}
            {if $hItem.col_lg <= 0}
            {$hItem.col_lg=floor(12/count($htmlitems))}
            {/if}

            {if $hItem.col_sm <= 0}
            {$hItem.col_sm=12}
            {/if}


            <li class="staticontent-item-{$smarty.foreach.items.iteration} staticontent-item {if $hItem.col_lg > 0} col-lg-{$hItem.col_lg} col-md-{$hItem.col_lg} col-sm-{$hItem.col_lg}{/if} hidden-xs {$hItem.class}">
                {if $hItem.image}
                        {if $hItem.url}
                            <a href="{$hItem.url|escape:'html':'UTF-8'}" class="item-link"{if $hItem.target == 1} target="_blank"{/if}>
                        {/if}
                             <span class="item-img"><img src="{$module_dir}images/{$hItem.image}" alt="" /></span>
                        {if $hItem.url}
                            </a>
                        {/if}
                    {/if}

                    {if $hItem.title && $hItem.title_use == 1}
                        <h3 class="item-title">{$hItem.title}</h3>
                    {/if}
                    {if $hItem.html}
                        <div class="item-html">
                            {$hItem.html}                         
                        </div>
                    {/if}
            </li>
        {/foreach}
    </ul>
</div>
{/if}

