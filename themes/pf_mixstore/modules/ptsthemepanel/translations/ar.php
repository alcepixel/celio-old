<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_c3098a94ebed2d2c33c86cb27fb2274f'] = 'لوحة تحكم PTS الموضوع';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_cf8fdaf6e745133c90516eb9b74e31c3'] = 'تكوين العناصر الرئيسية للموضوع الخاص بك.';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_b0301033a440844d134fcfa7b3ffb792'] = 'لوحة تحكم موضوع';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_764484b5c85efb6dd7e311f31369639c'] = 'الملامح منشئ الصفحة';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_b6d4de9b3e9a5098ee25f20d7db4d144'] = 'الملامح منشئ تذييل';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_1b2d0c4288ff0995063189bf65309a18'] = 'Megamenu';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_6c99351f73892627e7a23820e0461331'] = 'يعيش Megamenu محرر';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_739abe09ca70c329d652a629fe1b3c9b'] = 'محرر الموضوع الحية';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_3dce9c779d648827975df6d2d2ea217c'] = 'ضبط موضوع';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_3dc9e9cb412c8d23f560c84e19f302c8'] = 'صفحة تذييل منشئ';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_da22c93ccb398c72070f4000cc7b59a1'] = 'التخصيص';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_1fcc8de693babd527675c60c1905d090'] = 'إعداد وحدات';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'تمكين';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_b9f5c797ebbf55adccdd8539a65a0241'] = 'معاق';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_1cd8e92cc2319376054a73f23096cb77'] = 'لوحة تحديد مواصفات المحور الحية';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_6bc3a7c674ef76da35c61983d020a569'] = 'عند تغيير الجلد إلى الجلد الآخرين، جميع وحدات سيتم إعادة مدمن مخدرات على المناصب المتوقع أن الجلد. هذا لا تفعل. حفظ القيمة في التكوين';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_f1206f9fadc5ce41694f69129aecac26'] = 'تكوين';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_e873d89b2c012e5f3d22f8540180bd0e'] = 'إعدادات موضوع';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_c9cc8cce247e49bae79f15173ce97354'] = 'حفظ';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_356b8475422372cdb3532be61c2da619'] = 'وتذييل الصفحة تعيين منشئ Profies';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_5bd909d2aabb76836e8fdad730968abb'] = 'التخصيصات';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_d541d486af7b8e11d5d256d8979c1de6'] = ' الإطار يدعم لك اثنين طريقة لوضع التخصيص الخاص بك على الموضوع.';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_f3998425eceacbf5ff44cf992758622e'] = '1. إنشاء ملف CSS الخاص بك (ق) ووضعها في PTS_YOURTHEME / CSS / autoload. ووضع شبيبة الملفات في PTS_YOURTHEME / شبيبة / autoload';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_40363ee5ac1959fe9ccad9f7487ccdbe'] = '2. سيتم تحميل جميع الملفات التلقائي أو استخدام أدوات في رفع الصوت عاليا';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_ff7ada0be2c2b5094d565b2d9e5cfec2'] = 'انقر لايف تخصيص موضوع لخلق العرف موضوع، لمحات، وأنها سوف تكون مدرجة في مربع القائمة المنسدلة أعلاه. اخترت موضوع ملف تعريف واحد لتطبيق لموقعك';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_fde440d3006f332822eb958966103765'] = '! المهم: يتم تخزين جميع التشكيلات في موضوع PTS_YOURTHEME مجلد / CSS / تخصيص، وأنها تحتاج إذن إلى 0755 وضع الملفات داخل';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_fd8378449c9ca3cd5daff5b04ad1ff56'] = 'يعيش موضوع تحديد مواصفات';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_825c358fe7f1fa3e7bb0048bffd37dd2'] = 'عليك فقط أن ترى هذا على جهاز مكتب استقبال - زوارك لا يرى هذه الأداة. ';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_b95d3d403307efa40962df363734e941'] = 'ضبط وحدة';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_10acdd1571a76b796bed5d119b127a35'] = 'إضافة مخصص المغلق';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_6252c0f2c2ed83b7b06dfca86d4650bb'] = 'أحرف غير صالحة:';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_3c039bdb05442cf9ba4a43186f74c8b6'] = 'إضافة مخصص JS';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_af2bd2483562a756a482a08840a46579'] = 'عليك فقط أن ترى هذا [1] على ك مكتب استقبال [/ 1] - زوارك لن ترى هذه الأداة.';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_058e1168dd26085fe8d317effdf70dc3'] = 'عليك فقط أن ترى هذا على جهاز مكتب استقبال - زوارك لا يرى هذه الأداة.';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_6e3670bb5e3826106c5243b242cc52d9'] = 'عرض روابط لحسابات الاجتماعية المتجر الخاص بك (تويتر، الفيسبوك، الخ)';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_4bae4cdd2d56a5a8b8c320288c5d3426'] = 'عرض معلومات الاتصال';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_17a84b9793933f127b961f44b9c8bcbc'] = 'عرض أزرار المشاركة الاجتماعية على صفحة المنتجات';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_6080ab31226b39af728c2d40fd57f0d0'] = 'عرض كتلة الفيسبوك على الصفحة الرئيسية';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_988659f6c5d3210a3f085ecfecccf5d3'] = 'العرف CMS كتلة المعلومات';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_87965e506665f83903c1de5d3ad0c98e'] = 'تمكين نظرة سريعة';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_158fc4b90f7acab3770c57827fb0e424'] = 'تمكين كبار راية';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_cf5ca43ed94712fe0160fff0a523b8ef'] = 'عرض الشعارات دفع المنتج الخاص بك';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_2e1328793a14abff890f7bae4234328c'] = 'تمكين تحديد مواصفات لايف';
$_MODULE['<{ptsthemepanel}pf_mixstore>ptsthemepanel_f5a35b8e88be7f51ad3a3714a83df3a1'] = 'الأداة التخصيص يسمح لك لجعل اللون والخط التغيرات في الموضوع الخاص بك.';
$_MODULE['<{ptsthemepanel}pf_mixstore>form_c54f9f209ed8fb4683e723daa4955377'] = 'الإعداد العام';
$_MODULE['<{ptsthemepanel}pf_mixstore>form_8625e1de7be14c39b1d14dc03d822497'] = 'أدوات';
$_MODULE['<{ptsthemepanel}pf_mixstore>form_17827247cf516e808f0f56b2a27c0a76'] = 'نموذج بيانات وحدات';
$_MODULE['<{ptsthemepanel}pf_mixstore>form_097eb22572daf951788a4b8fdceb33c5'] = 'هذه أداة تسمح بتثبيت datamples لجعل الشكل والمظهر مثل ما عرض على مخزن موجودة.';
$_MODULE['<{ptsthemepanel}pf_mixstore>form_39b6a88f07ac14970ef03affe25fb7cb'] = 'انقر لاستيراد تكوين نموذج لتثبيت DataSample التكوين من وحدات تجعل لActived الموضوع.';
$_MODULE['<{ptsthemepanel}pf_mixstore>form_984971c38e03be87c4abff4378b3f00f'] = 'إذا حدات مستعدون مع التشكيلات القديمة، يرجى الضغط على زر النسخ الاحتياطي الذي سوف يعيد لك عند النقر على زر استعادة';
$_MODULE['<{ptsthemepanel}pf_mixstore>form_c68f9e0c2548f885af5c13912b6b5bb9'] = 'تثبيت نموذج استعلامات الميزة تستخدم فقط لوحدة (ق) التي لم تقم حتى تثبيت أو جداول وسجلات وحدة (ق) هو فارغ';
$_MODULE['<{ptsthemepanel}pf_mixstore>form_e55f75a29310d7b60f7ac1d390c8ae42'] = 'وحدة';
$_MODULE['<{ptsthemepanel}pf_mixstore>form_9a0580ec77d3ad9a6d25099a0ad612f4'] = 'لديه تكوين';
$_MODULE['<{ptsthemepanel}pf_mixstore>form_0403476e6f509dccfc11e2383f601b83'] = 'لديه الاستفسارات';
$_MODULE['<{ptsthemepanel}pf_mixstore>form_7fdb798fde2d86bcba9472754191fc20'] = 'دعم';
$_MODULE['<{ptsthemepanel}pf_mixstore>form_004bf6c9a40003140292e97330236c53'] = 'عمل';
$_MODULE['<{ptsthemepanel}pf_mixstore>form_93cba07454f06a4a960172bbd6e2a435'] = 'نعم';
$_MODULE['<{ptsthemepanel}pf_mixstore>form_bafd7322c6e97d25b6299b5d6fe8920b'] = 'لا';
$_MODULE['<{ptsthemepanel}pf_mixstore>form_9ffa4765d2da990741800bbe1ad4e7f8'] = 'الإجراءات السائبة';
$_MODULE['<{ptsthemepanel}pf_mixstore>form_4c41e0bd957698b58100a5c687d757d9'] = 'تحديد كافة';
$_MODULE['<{ptsthemepanel}pf_mixstore>form_237c7b6874386141a095e321c9fdfd38'] = 'إلغاء تحديد الكل';
$_MODULE['<{ptsthemepanel}pf_mixstore>form_1113cf7dd8840bc5a6a0b368450c5bfe'] = 'أدوات الموضوع';
$_MODULE['<{ptsthemepanel}pf_mixstore>injecthead_49842154b7f93a5a3780a4517fc08fe4'] = 'كنت قد تحولت إلى موضوع جديد لمتجرك.';
$_MODULE['<{ptsthemepanel}pf_mixstore>injecthead_450e1ba486b665675d8c74ab43513028'] = 'قد تحتاج إلى تثبيت عينات البيانات الافتراضية وتكوينات البيانات عن وحدات لإلقاء نظرة نفس التجريبي لدينا';
$_MODULE['<{ptsthemepanel}pf_mixstore>injecthead_d0d652a947de29daf11dee724d706f34'] = 'تثبيت عينة الآن';
$_MODULE['<{ptsthemepanel}pf_mixstore>injecthead_e38c0c4d1928ccdc3c3c5df4a6bccfca'] = 'التلقائي تركيب نموذج في لوحة التحكم الموضوع';
$_MODULE['<{ptsthemepanel}pf_mixstore>injecthead_4828ccb6a79f9feb413ecd2312fa50a6'] = 'UserGuide';
$_MODULE['<{ptsthemepanel}pf_mixstore>injecthead_a338a28ab21f9b037fca27187c55782c'] = 'تثبيت عينة يدويا عن طريق أدلة القراءة';
$_MODULE['<{ptsthemepanel}pf_mixstore>injecthead_a1ffa089a7d27dd3ade917a90c4d179a'] = 'إغلاق هذه النوافذ المنبثقة فإنه ليس أظهر في المرة القادمة';
$_MODULE['<{ptsthemepanel}pf_mixstore>live_configurator_f4040952f31ad8c3bce98edbf24094eb'] = 'وضع RTL';
$_MODULE['<{ptsthemepanel}pf_mixstore>live_configurator_8ba6365546b45d2efcfecf33be02e88d'] = 'موضوع جلود';
$_MODULE['<{ptsthemepanel}pf_mixstore>live_configurator_163e783130644a3d7694489298a58788'] = 'إعادة هوك، التكوين';
$_MODULE['<{ptsthemepanel}pf_mixstore>live_configurator_93cba07454f06a4a960172bbd6e2a435'] = 'نعم';
$_MODULE['<{ptsthemepanel}pf_mixstore>live_configurator_bafd7322c6e97d25b6299b5d6fe8920b'] = 'لا';
$_MODULE['<{ptsthemepanel}pf_mixstore>live_configurator_526d688f37a86d3c3f27d0c5016eb71d'] = 'إعادة تعيين';
$_MODULE['<{ptsthemepanel}pf_mixstore>live_configurator_c9cc8cce247e49bae79f15173ce97354'] = 'حفظ';
$_MODULE['<{ptsthemepanel}pf_mixstore>live_configurator_be41862dd02d0f035b7796dec212fa90'] = 'أبل تغير الآن';
$_MODULE['<{ptsthemepanel}pf_mixstore>themeeditor_2e69012849ab082a4959a3b5ee4654b7'] = 'موضوع التشكيلات الجانبية المخصصة الإدارة';
$_MODULE['<{ptsthemepanel}pf_mixstore>themeeditor_c320cdeffafbb2d60d6d759619c6b5e2'] = 'حفظ الملف';
$_MODULE['<{ptsthemepanel}pf_mixstore>themeeditor_4d3d769b812b6faa6b76e1a8abaece2d'] = 'نشط';
