{if !empty($products)}
    <div class="util-carousel top-nav-box {if Configuration::get('PTS_CP_PRODUCT_STYLE') == 'style1'}style1{elseif Configuration::get('PTS_CP_PRODUCT_STYLE') == 'style2'}style2 {elseif Configuration::get('PTS_CP_PRODUCT_STYLE') == 'style3'}style3 {elseif Configuration::get('PTS_CP_PRODUCT_STYLE') == 'style4'}style4 {elseif Configuration::get('PTS_CP_PRODUCT_STYLE') == 'style5'}style5{/if}" id="{$tabname}" >
        {foreach from=$products item=product name=products}
            <div class="pts-product_list ajax_block_product">
                {if Configuration::get('PTS_CP_PRODUCT_STYLE') == 'style1'}
                        {include file="$tpl_dir./sub/product/style1.tpl" product=$product class=''}
                    {elseif Configuration::get('PTS_CP_PRODUCT_STYLE') == 'style2'}
                        {include file="$tpl_dir./sub/product/style2.tpl" product=$product class=''}
                    {elseif Configuration::get('PTS_CP_PRODUCT_STYLE') == 'style3'}
                        {include file="$tpl_dir./sub/product/style3.tpl" product=$product class=''}
                    {elseif Configuration::get('PTS_CP_PRODUCT_STYLE') == 'style4'}
                        {include file="$tpl_dir./sub/product/style4.tpl" product=$product class=''}
                    {elseif Configuration::get('PTS_CP_PRODUCT_STYLE') == 'style5'}
                        {include file="$tpl_dir./sub/product/style5.tpl" product=$product class=''}
                    {/if}
            </div>
        {/foreach}
    </div>
{/if}