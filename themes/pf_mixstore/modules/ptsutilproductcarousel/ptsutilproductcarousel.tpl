{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<!-- MODULE Block ptsutilproductcarousel -->
<div class=" block products_block ptsutilproductcarousel">
	<h4 class="title_block"><span>{l s='Latest Products' mod='ptsutilproductcarousel'}</span></h4>
	<div class="block_content">
		{if !empty($products)}
			{$tabname="ptsutilproductcarousel"}
			{include file="{$product_tpl}"} 
		{/if}
	</div>
</div>
<!-- /MODULE Block ptsutilproductcarousel -->
{literal}
<script type="text/javascript">
    $(document).ready(function() {
        $('#{/literal}{$tabname}'{literal}).utilCarousel({
            pagination : false,
            pause: 'hover',
            interval: {/literal}{$interval}{literal},
            autoPlay : {/literal}{if $interval==0}false{else}true{/if}{literal},
            responsiveMode : 'itemWidthRange',
            itemWidthRange : [{/literal}{$min_width}{literal}, {/literal}{$max_width}{literal}],
            navigation : true,
            navigationText : ['‹', '›']
        });
    });
</script>
{/literal}
 