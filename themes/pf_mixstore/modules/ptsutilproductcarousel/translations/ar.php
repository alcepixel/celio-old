<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_4ed914e82bcd41f3e51500d6c0506360'] = 'نقطة المنتجات UTIL كاروسيل بلوك';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_68a7e286862f2fe15498f6b0c78f55b2'] = 'عرض المنتجات في فئات UTIL كاروسيل.';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_8dd2f915acf4ec98006d11c9a4b0945b'] = 'تحديث الإعدادات بنجاح.';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_3c7679577cd1d0be2b98faf898cfc56d'] = 'تاريخ الإضافة';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_235e70e4e7c0a3d61ccb6f60518ebd24'] = 'تاريخ الإضافة تنازلي';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_49ee3087348e8d44e1feda1917443987'] = 'اسم';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_1b7bb88b3317fe166424fa9e7535e1a9'] = 'اسم تنازلي';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_694e8d1f2ee056f98ee488bdc4982d73'] = 'كمية';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_003085dd2a352e197d8efea06dfa75b8'] = 'كمية تنازلي';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_3601146c4e948c32b6424d2c0a7f0118'] = 'السعر';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_32da8a9347c34947b76a0a33d59edf4c'] = 'السعر تنازلي';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_f4f70727dc34561dfde1a3c529b6205c'] = 'الإعدادات';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_8c38776925f7cf41c090646a43157024'] = 'فئات:';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_801322095c363689f7620e0147e3fd14'] = 'الترتيب حسب';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_2b1ea5960e397a45dc376823f106cb13'] = 'البند نطاق عرض الحد الأدنى:';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_1d25b4c27d7f457fe81e396f37fc2c53'] = 'مجموعة العرض البند عندما responsiveMode في \"itemWidthRange\". استخدامه مثل [200، 400]، يعني UtilCarousel سيبقي عرض هذا البند بين 200px 400px لو caculate السيارات كيف العديد من البنود التي تظهر.';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_04923eebded8dc0ffca836e89af1a14a'] = 'البند نطاق العرض الأقصى:';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_2a8e84536f313169f9ff0a287c442eca'] = 'الحد الأصناف';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_4dfe84d30e86a689bee6dad89d5aba29'] = 'الحد الأقصى لعدد المنتجات في كل كاروسيل (الافتراضي: 8).';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_d16dd01adf735ed9b87eebff5fc39ce5'] = 'الفاصلة';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_b24a524f3acce7d7d0ef2d3bd954d153'] = 'دخول الوقت (miniseconds) للعب دائري. قيمة 0 لإيقاف.';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_c9cc8cce247e49bae79f15173ce97354'] = 'حفظ';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_db5bbee0cb649caceaa88f704351ac80'] = 'أحدث المنتجات';
