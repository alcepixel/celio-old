<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_4ed914e82bcd41f3e51500d6c0506360'] = 'Pt Prodotti Util Carousel Block';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_68a7e286862f2fe15498f6b0c78f55b2'] = 'Visualizzare i prodotti di categorie in Util Carousel.';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_8dd2f915acf4ec98006d11c9a4b0945b'] = 'Impostazioni aggiornati correttamente.';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_3c7679577cd1d0be2b98faf898cfc56d'] = 'Data di registrazione';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_235e70e4e7c0a3d61ccb6f60518ebd24'] = 'Data di registrazione DESC';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_49ee3087348e8d44e1feda1917443987'] = 'Nome';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_1b7bb88b3317fe166424fa9e7535e1a9'] = 'Nome DESC';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_694e8d1f2ee056f98ee488bdc4982d73'] = 'Quantità';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_003085dd2a352e197d8efea06dfa75b8'] = 'Quantità DESC';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_3601146c4e948c32b6424d2c0a7f0118'] = 'Prezzo';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_32da8a9347c34947b76a0a33d59edf4c'] = 'Costo DECR';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_f4f70727dc34561dfde1a3c529b6205c'] = 'Impostazioni';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_8c38776925f7cf41c090646a43157024'] = 'Categorie:';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_801322095c363689f7620e0147e3fd14'] = 'Ordini';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_2b1ea5960e397a45dc376823f106cb13'] = 'Articolo gamma Larghezza min:';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_1d25b4c27d7f457fe81e396f37fc2c53'] = 'La gamma di larghezza voce quando responsiveMode in \'itemWidthRange\'. Usalo come [200, 400], significa UtilCarousel manterrà la larghezza dell\'elemento tra 200px per 400px e auto caculate il numero di elementi da mostrare.';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_04923eebded8dc0ffca836e89af1a14a'] = 'Articolo gamma Larghezza max:';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_2a8e84536f313169f9ff0a287c442eca'] = 'Limite Articoli';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_4dfe84d30e86a689bee6dad89d5aba29'] = 'Il numero massimo di prodotti in ogni Carousel (default: 8).';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_d16dd01adf735ed9b87eebff5fc39ce5'] = 'Intervallo';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_b24a524f3acce7d7d0ef2d3bd954d153'] = 'Inserire l\'ora (miniseconds) per riprodurre carosello. Valore 0 per interrompere.';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_c9cc8cce247e49bae79f15173ce97354'] = 'Salva';
$_MODULE['<{ptsutilproductcarousel}pf_mixstore>ptsutilproductcarousel_db5bbee0cb649caceaa88f704351ac80'] = 'Ultimi Prodotti';
