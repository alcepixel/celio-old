<div class="block_verticalmenu">
    <div id="pts-verticalmenu" class="verticalmenu">
        <h4 class="{if isset($hook_name) && ($hook_name == 'left' || $hook_name == 'right')} title_block{else}vertical_title{/if}">
            {l s='Vertical Megamenu' mod='ptsverticalmenu'} 
            <i class="icon icon-angle-down pull-right"></i> 
        </h4>
        <div id="pts-vertical-menu" class="pts-verticalmenu collapse navbar-collapse navbar-ex1-collapse {if isset($hook_name)}vertical-{$hook_name}{/if}">
            {$ptsverticalmenu}
        </div>
    </div>
</div>
<script type="text/javascript">
    if($(window).width() >= 992){
        $('#pts-vertical-menu a.dropdown-toggle').click(function(){
            var redirect_url = $(this).attr('href');
            window.location = redirect_url;
        });
    }
</script>