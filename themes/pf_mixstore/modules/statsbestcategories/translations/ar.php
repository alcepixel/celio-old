<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statsbestcategories}pf_mixstore>statsbestcategories_4f29d8c727dcf2022ac241cb96c31083'] = 'عاد سجلات فارغة';
$_MODULE['<{statsbestcategories}pf_mixstore>statsbestcategories_f5c493141bb4b2508c5938fd9353291a'] = 'عرض٪ 1 $ s من٪ 2 $ S';
$_MODULE['<{statsbestcategories}pf_mixstore>statsbestcategories_49ee3087348e8d44e1feda1917443987'] = 'الاسم';
$_MODULE['<{statsbestcategories}pf_mixstore>statsbestcategories_eebfd2d9a7ea25b9e61e8260bcd4849c'] = 'إجمالي الكمية المباعة';
$_MODULE['<{statsbestcategories}pf_mixstore>statsbestcategories_f3547ae5e06426d87312eff7dda775aa'] = 'السعر الإجمالي';
$_MODULE['<{statsbestcategories}pf_mixstore>statsbestcategories_c13329e42ec01a10f84c0f950274b404'] = 'إجمالي مشاهدة';
$_MODULE['<{statsbestcategories}pf_mixstore>statsbestcategories_6e3b3150807da868ebd33ad2c991b8d7'] = 'أفضل الفئات';
$_MODULE['<{statsbestcategories}pf_mixstore>statsbestcategories_e5510869e31bbf721ca15dff21cf1169'] = 'يضيف قائمة أفضل الفئات لوحة القيادة الإحصاءات.';
$_MODULE['<{statsbestcategories}pf_mixstore>statsbestcategories_998e4c5c80f27dec552e99dfed34889a'] = 'إستيراد ملف إعدادات';
