<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statsorigin}pf_mixstore>statsorigin_f0b1507c6bdcdefb60a0e6f9b89d4ae8'] = 'Origine visitatori';
$_MODULE['<{statsorigin}pf_mixstore>statsorigin_a69c2a3091fe48c7f4f391595aa3ac19'] = 'Aggiunge nel pannello Statistiche un grafico che mostra il sito web di provenienza del tuo visitatore.';
$_MODULE['<{statsorigin}pf_mixstore>statsorigin_14542f5997c4a02d4276da364657f501'] = 'Link diretto';
$_MODULE['<{statsorigin}pf_mixstore>statsorigin_3edf8ca26a1ec14dd6e91dd277ae1de6'] = 'Origine';
$_MODULE['<{statsorigin}pf_mixstore>statsorigin_4b69c1f7f555aa19fd90ee01e4aa63cd'] = 'In questa scheda riassumiamo i 10 siti web più popolari che portano clienti al tuo negozio online.';
$_MODULE['<{statsorigin}pf_mixstore>statsorigin_6602bbeb2956c035fb4cb5e844a4861b'] = 'Guida';
$_MODULE['<{statsorigin}pf_mixstore>statsorigin_cec998cc46cd200fa97490137de2cc7f'] = 'Che cosa è un sito web di riferimento?';
$_MODULE['<{statsorigin}pf_mixstore>statsorigin_54f00c2c9a36e2b300b5bacc1bb7912c'] = 'Il riferimento è l\'URL della pagina web contenente il collegamento al tuo negozio e visualizzata dal visitatore prima di entrare nel tuo negozio online.';
$_MODULE['<{statsorigin}pf_mixstore>statsorigin_7231fe46fc79eb2b3a269f790b79e01f'] = 'Un riferimento consente di sapere quali parole chiave sono state inserite dai visitatori nei motori di ricerca quando hanno cercato di trovare il tuo negozio, ed è utile anche per ottimizzare la promozione sul web.';
$_MODULE['<{statsorigin}pf_mixstore>statsorigin_af19c8da1c414055c960a73d86471119'] = 'Un riferimento può essere:';
$_MODULE['<{statsorigin}pf_mixstore>statsorigin_c227be237c874ba6b2f8771d7b66b90e'] = 'Qualcuno che mettere un link sul suo sito web verso il tuo negozio.';
$_MODULE['<{statsorigin}pf_mixstore>statsorigin_ea87a2280d5cdb638a2727147a3dd85c'] = 'Un partner con cui hai fatto uno scambio di link al fine di aumentare le vendite o attirare nuovi clienti.';
$_MODULE['<{statsorigin}pf_mixstore>statsorigin_998e4c5c80f27dec552e99dfed34889a'] = 'Esporta CSV';
$_MODULE['<{statsorigin}pf_mixstore>statsorigin_96b0141273eabab320119c467cdcaf17'] = 'Totale';
$_MODULE['<{statsorigin}pf_mixstore>statsorigin_0bebf95ee829c33f34fde535ed4ed100'] = 'Solo link diretti';
$_MODULE['<{statsorigin}pf_mixstore>statsorigin_450a7e38e636dd49f5dfb356f96d3996'] = 'I primi 10 siti web';
$_MODULE['<{statsorigin}pf_mixstore>statsorigin_52ef9633d88a7480b3a938ff9eaa2a25'] = 'Altri';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_c3098a94ebed2d2c33c86cb27fb2274f'] = 'Pannello di controllo Theme PTS';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_cf8fdaf6e745133c90516eb9b74e31c3'] = 'Configurare gli elementi principali del vostro tema.';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_b0301033a440844d134fcfa7b3ffb792'] = 'Pannello di controllo Theme';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_77c060aaa1bf237843034902852c3f99'] = 'Page Builder';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_1b2d0c4288ff0995063189bf65309a18'] = 'Megamenu';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_6c99351f73892627e7a23820e0461331'] = 'Vivere Megamenu Editor';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_739abe09ca70c329d652a629fe1b3c9b'] = 'Live Theme Editor';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Attivato';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_b9f5c797ebbf55adccdd8539a65a0241'] = 'Disabile';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_1cd8e92cc2319376054a73f23096cb77'] = 'Live Theme Pannello Configurator';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_6bc3a7c674ef76da35c61983d020a569'] = 'Quando si cambia pelle a pelle dell\'altro, tutti i moduli saranno nuovamente agganciati su posizioni che ci si attende di quella pelle. Questo doest non salvare il valore in configurazione';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_f1206f9fadc5ce41694f69129aecac26'] = 'Configurazione';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_e873d89b2c012e5f3d22f8540180bd0e'] = 'Impostazioni del tema';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_c9cc8cce247e49bae79f15173ce97354'] = 'Salva';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_5bd909d2aabb76836e8fdad730968abb'] = 'Personalizzazioni';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_d541d486af7b8e11d5d256d8979c1de6'] = ' La struttura vi supporta due modi per mettere la vostra personalizzazione sul tema.';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_f3998425eceacbf5ff44cf992758622e'] = '1. Si crea il file css (s) e mettere in PTS_YOURTHEME / css / caricamento automatico. E mettere i file js in PTS_YOURTHEME / js / autoload';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_40363ee5ac1959fe9ccad9f7487ccdbe'] = '2. Tutti i file verranno caricati strumenti automatici o l\'uso a soffietto';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_ff7ada0be2c2b5094d565b2d9e5cfec2'] = 'Fare clic su Live Personalizzazione tema per creare personalizzati-theme-profili e saranno elencati nella casella a discesa sopra. Si seleziona un profilo tema da applicare per il tuo sito';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_fde440d3006f332822eb958966103765'] = 'Importante: tutti i profili tematici sono memorizzati nella cartella PTS_YOURTHEME / css / personalizzare, si necessita di permessi 0755 per mettere i file all\'interno';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_fd8378449c9ca3cd5daff5b04ad1ff56'] = 'Live Theme Configurator';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_825c358fe7f1fa3e7bb0048bffd37dd2'] = 'Solo tu puoi vedere questo sul tuo Front-Office - i visitatori non vedranno questo strumento. ';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_b95d3d403307efa40962df363734e941'] = 'Impostazioni modulo';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_10acdd1571a76b796bed5d119b127a35'] = 'Aggiungere personalizzato Css';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_6252c0f2c2ed83b7b06dfca86d4650bb'] = 'Caratteri non validi:';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_3c039bdb05442cf9ba4a43186f74c8b6'] = 'Aggiungere personalizzato JS';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_af2bd2483562a756a482a08840a46579'] = 'Solo tu puoi vedere questo [1] sul Front-Office [/ 1] - i visitatori non vedere questo strumento.';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_058e1168dd26085fe8d317effdf70dc3'] = 'Solo tu puoi vedere questo sul tuo Front-Office - i visitatori non vedranno questo strumento.';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_6e3670bb5e3826106c5243b242cc52d9'] = 'Mostra link ai conti sociali del negozio (Twitter, Facebook, ecc)';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_4bae4cdd2d56a5a8b8c320288c5d3426'] = 'Mostra contatto';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_17a84b9793933f127b961f44b9c8bcbc'] = 'Visualizza i pulsanti di condivisione sociale nella pagina Prodotti';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_6080ab31226b39af728c2d40fd57f0d0'] = 'Visualizzare il blocco di Facebook in home page';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_988659f6c5d3210a3f085ecfecccf5d3'] = 'CMS personalizzato blocco di informazioni';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_87965e506665f83903c1de5d3ad0c98e'] = 'Attiva la descrizione breve';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_158fc4b90f7acab3770c57827fb0e424'] = 'Abilita top banner';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_cf5ca43ed94712fe0160fff0a523b8ef'] = 'Visualizzare i loghi di pagamento del prodotto';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_2e1328793a14abff890f7bae4234328c'] = 'Abilita Live Configurator';
$_MODULE['<{statsorigin}pf_mixstore>ptsthemepanel_f5a35b8e88be7f51ad3a3714a83df3a1'] = 'Lo strumento di personalizzazione consente di effettuare dei colori e dei font cambiamenti nel vostro tema.';
