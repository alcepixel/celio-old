<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statssales}pf_mixstore>statssales_45c4b3e103155326596d6ccd2fea0f25'] = 'المبيعات وطلبات الشراء';
$_MODULE['<{statssales}pf_mixstore>statssales_d2fb07753354576172a2b144c373a610'] = 'ويضيف الرسومات عرض تطور المبيعات وأوامر إلى الإحصائيات لوحة القيادة.';
$_MODULE['<{statssales}pf_mixstore>statssales_6602bbeb2956c035fb4cb5e844a4861b'] = 'المعالج';
$_MODULE['<{statssales}pf_mixstore>statssales_bdaa0cab56c2880f8f60e6a2cef40e63'] = 'حول الأوضاع أجل';
$_MODULE['<{statssales}pf_mixstore>statssales_0bbd4e137201977de3d977ddfb06a364'] = 'في المكتب الخاص بك العودة، يمكنك تعديل الأوضاع الترتيب التالي: بإنتظار تحقق الدفع، الدفع المقبولة، إعداد في التقدم والشحن وتسليمها، إلغاء، رد ودفع الرسوم خطأ، من المخزون، وانتظار بنك سلك الدفع.';
$_MODULE['<{statssales}pf_mixstore>statssales_82b0aefc59fe709e96204c6cbaaeaec7'] = 'لا يمكن إزالة هذه الأوضاع أمر من شباك الظهر. ولكن لديك خيار لإضافة المزيد.';
$_MODULE['<{statssales}pf_mixstore>statssales_ddb21e1caa84c463bc744c412a7b05f5'] = 'وتمثل الرسوم البيانية التالية تطور أوامر والمبيعات دوران المحل الخاص بك لفترة محددة.';
$_MODULE['<{statssales}pf_mixstore>statssales_ef9c3c65723819a9c183d857a39ff403'] = 'يجب استشارة كثير من الأحيان هذه الشاشة، كما أنه يسمح لك لمراقبة سرعة الاستدامة المحل الخاص بك. كما يسمح لك لمراقبة فترات زمنية متعددة.';
$_MODULE['<{statssales}pf_mixstore>statssales_5cc6f5194e3ef633bcab4869d79eeefa'] = 'وتتمثل أوامر صالحة فقط بالرسوم والصور.';
$_MODULE['<{statssales}pf_mixstore>statssales_c3987e4cac14a8456515f0d200da04ee'] = 'كافة الدول';
$_MODULE['<{statssales}pf_mixstore>statssales_d7778d0c64b6ba21494c97f77a66885a'] = 'فلتر';
$_MODULE['<{statssales}pf_mixstore>statssales_9ccb8353e945f1389a9585e7f21b5a0d'] = 'طلبات الشراء:';
$_MODULE['<{statssales}pf_mixstore>statssales_156e5c5872c9af24a5c982da07a883c2'] = 'اشترى المنتجات :';
$_MODULE['<{statssales}pf_mixstore>statssales_998e4c5c80f27dec552e99dfed34889a'] = 'إستيراد ملف إعدادات';
$_MODULE['<{statssales}pf_mixstore>statssales_ec3e48bb9aa902ba2ad608547fdcbfdc'] = 'المبيعات:';
$_MODULE['<{statssales}pf_mixstore>statssales_f6825178a5fef0a97dacf963409829f0'] = 'يمكنك عرض توزيع الحالات أمر أدناه.';
$_MODULE['<{statssales}pf_mixstore>statssales_da80af4de99df74dd59e665adf1fac8f'] = 'لا يوجد طلبات شراء لهذه المدة.';
$_MODULE['<{statssales}pf_mixstore>statssales_c58114720bcd52bfe96fd801cee77e93'] = 'الطلبات';
$_MODULE['<{statssales}pf_mixstore>statssales_c8be451a5698956a0e78b5c2caab4821'] = 'المنتجات التي اشترتها';
$_MODULE['<{statssales}pf_mixstore>statssales_b52b44c9d23e141b067d7e83b44bb556'] = 'المنتجات';
$_MODULE['<{statssales}pf_mixstore>statssales_497a2a4cf0a780ff5b60a7a6e43ea533'] = 'مبيعات العملة:٪ S';
$_MODULE['<{statssales}pf_mixstore>statssales_17833fb3783b26e0a9bc8b21ee85302a'] = 'النسبة المئوية للأوامر في الوضع.';
