<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statssales}pf_mixstore>statssales_45c4b3e103155326596d6ccd2fea0f25'] = 'Ventas y pedidos';
$_MODULE['<{statssales}pf_mixstore>statssales_d2fb07753354576172a2b144c373a610'] = 'Añade gráficas mostrando la evolución de ventas y pedidos en el panel de Estadísticas.';
$_MODULE['<{statssales}pf_mixstore>statssales_6602bbeb2956c035fb4cb5e844a4861b'] = 'Guía';
$_MODULE['<{statssales}pf_mixstore>statssales_bdaa0cab56c2880f8f60e6a2cef40e63'] = 'Acerca del estado de las compras';
$_MODULE['<{statssales}pf_mixstore>statssales_0bbd4e137201977de3d977ddfb06a364'] = 'En su Panel de Administrador puede modificar el estado de los pedidos: Esperando confirmar el pago, Pago aceptado, Preparación en progreso, Enviado, Entregado, Cancelado, Reembolsado, Error en Pago, Sin Existencias y Esperando transferencia bancaria.';
$_MODULE['<{statssales}pf_mixstore>statssales_82b0aefc59fe709e96204c6cbaaeaec7'] = 'Estos estados de los pedidos no pueden ser eliminados desde el Panel de administrador, pero tiene la opción de añadir más.';
$_MODULE['<{statssales}pf_mixstore>statssales_ddb21e1caa84c463bc744c412a7b05f5'] = 'Les graphiques suivants représentent l\'évolution des commandes et des ventes chiffre d\'affaires de votre boutique pour une période sélectionnée.';
$_MODULE['<{statssales}pf_mixstore>statssales_ef9c3c65723819a9c183d857a39ff403'] = 'Vous devez souvent consulter cet écran, car il vous permet de contrôler rapidement la viabilité de votre boutique. Il vous permet également de surveiller plusieurs périodes de temps.';
$_MODULE['<{statssales}pf_mixstore>statssales_5cc6f5194e3ef633bcab4869d79eeefa'] = 'Solo se representan gráficamente las compras válidas.';
$_MODULE['<{statssales}pf_mixstore>statssales_c3987e4cac14a8456515f0d200da04ee'] = 'Todos los países';
$_MODULE['<{statssales}pf_mixstore>statssales_d7778d0c64b6ba21494c97f77a66885a'] = 'Filtrar';
$_MODULE['<{statssales}pf_mixstore>statssales_9ccb8353e945f1389a9585e7f21b5a0d'] = 'Pedidos realizados:';
$_MODULE['<{statssales}pf_mixstore>statssales_156e5c5872c9af24a5c982da07a883c2'] = 'Productos pedidos:';
$_MODULE['<{statssales}pf_mixstore>statssales_998e4c5c80f27dec552e99dfed34889a'] = 'Exportar CSV';
$_MODULE['<{statssales}pf_mixstore>statssales_ec3e48bb9aa902ba2ad608547fdcbfdc'] = 'Ventas:';
$_MODULE['<{statssales}pf_mixstore>statssales_f6825178a5fef0a97dacf963409829f0'] = 'Puedes ver la distribución de estados de compra a continuación.';
$_MODULE['<{statssales}pf_mixstore>statssales_da80af4de99df74dd59e665adf1fac8f'] = 'No hay pedidos para este periodo';
$_MODULE['<{statssales}pf_mixstore>statssales_c58114720bcd52bfe96fd801cee77e93'] = 'Les commandes passées';
$_MODULE['<{statssales}pf_mixstore>statssales_c8be451a5698956a0e78b5c2caab4821'] = 'Les produits achetés';
$_MODULE['<{statssales}pf_mixstore>statssales_b52b44c9d23e141b067d7e83b44bb556'] = 'Productos';
$_MODULE['<{statssales}pf_mixstore>statssales_497a2a4cf0a780ff5b60a7a6e43ea533'] = 'Divisa de venta: %s';
$_MODULE['<{statssales}pf_mixstore>statssales_17833fb3783b26e0a9bc8b21ee85302a'] = 'Porcentaje de pedidos por estado.';
