<header id="header">
	<section id="topbar" class="topbar">
		<div class="container">
			<div class="container-inner clearfix">
				{hook h="displayNav"}
					{if class_exists('PtsthemePanel')}
						{plugin module='blockcart' hook='top'}
					{/if}
				<a href="/pedido" class="carrito"><span></span></a>
				<div id="social_block" class="top-social">
					<ul class="social">
					<li class="facebook"><a class="btn-tooltip" href="http://www.facebook.com/celiochile?fref=ts" target="_blank" > Facebook </a></li>
					<li class="twitter"><a class="btn-tooltip" href="http://twitter.com/celiochile" target="_blank">Twitter </a></li>
					<li class="instagram"><a class="btn-tooltip" href="http://instagram.com/celiochile/" target="_blank"> Instagram</a></li>
					<li class="pinterest"><a class="btn-tooltip" href="http://es.pinterest.com/celiochile/" target="_blank"> Pinterest</a></li>
					</ul>
				</div>
			</div>
		</div>
	</section>	
	<section  id="header-main" class="header">
		<div class="container">
				<div id="header_logo" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 inner">
					<div id="logo-theme" class="logo-store col-xs-2 col-sm-2 col-md-2 col-lg-2">
				 
						<a href="{$base_dir}" title="{$shop_name|escape:'html':'UTF-8'}">
								<img class="logo img-responsive" src="{$logo_url}" alt="{$shop_name|escape:'html':'UTF-8'}"{if isset($logo_image_width) && $logo_image_width} width="{$logo_image_width}"{/if}{if isset($logo_image_height) && $logo_image_height} height="{$logo_image_height}"{/if}/>
						</a>
					 
					</div>
					<section  id="pts-mainnav" class="main-menu col-lg-9 col-md-9 col-sm-9 col-sm-offset-1 col-xs-9">
				    
					    <div>
						    {hook h="displayMainmenu"}
						
						{if class_exists('PtsthemePanel')}
							{plugin module='blocksearch' hook='top'}
						{/if}
						</div>              
				</section>
				</div>
				
		</div>
	</section >
</header>