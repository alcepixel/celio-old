<div class="row">
	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
		{if $page_name == 'category'}
			{if isset($category)}
				{if $category->id AND $category->active}
					<h1 class="page-heading-title">{$category->name|escape:'html':'UTF-8'}   {include file="$tpl_dir./category-count.tpl"}</h1>
				{/if}
			{/if}

		{elseif $page_name == 'address'}
			<h1 class="page-heading-title">{l s='Your addresse'}</h1>
		{elseif $page_name == 'addresses'}
			<h1 class="page-heading-title">{l s='My addresses'}</h1>
		{elseif $page_name == 'attachment'}
			<h1 class="page-heading-title">{l s='Attachment'}</h1>
		{elseif $page_name == 'auth'}
			<h1 class="page-heading-title">{l s='Authentication'}</h1>
		{elseif $page_name == 'authentication'}
			<h1 class="page-heading-title">{l s='Authentication'}</h1>
		{elseif $page_name == 'best-sales'}
			<h1 class="page-heading-title">{l s='Top sellers'}</h1>
		{elseif $page_name == 'cms'}
			<h1 class="page-heading-title">
				{if isset($cms) && !isset($cms_category)}
					{if $cms->active}
						{$cms->meta_title|escape:'html':'UTF-8'}
					{/if}
				{elseif isset($cms_category)}
					{$cms_category->name|escape:'html':'UTF-8'}
				{/if}
			</h1>
		{elseif $page_name == 'compare'}
			<h1 class="page-heading-title">{l s='Product Comparison'}</h1>
		{elseif $page_name == 'contact'}
			<h1 class="page-heading-title">
				{l s='Customer service'} - {if isset($customerThread) && $customerThread}{l s='Your reply'}{else}{l s='Contact us'}{/if}
			</h1>
		{elseif $page_name == 'discount'}
			<h1 class="page-heading-title">
				{l s='My vouchers'}
			</h1>
		{elseif $page_name == 'guest-tracking'}
			<h1 class="page-heading-title">{l s='Guest Tracking'}</h1>
		{elseif $page_name == 'history'}
			<h1 class="page-heading-title">{l s='Order history'}</h1>

		{elseif $page_name == 'identity'}
			<h1 class="page-heading-title">{l s='Your personal information'}</h1>
		{elseif $page_name == 'manufacturer'}
			<h1 class="page-heading-title">
				{if isset($manufacturer)}
					
					{l s='List of products by manufacturer'}&nbsp;{$manufacturer->name|escape:'html':'UTF-8'}
				{elseif isset($manufacturers)}
					{l s='Brands'}
					{strip}
						<span class="heading-counter">
							{if $nbManufacturers == 0}{l s='There are no manufacturers.'}
							{else}
								{if $nbManufacturers == 1}
									{l s='There is 1 brand'}
								{else}
									{l s='There are %d brands' sprintf=$nbManufacturers}
								{/if}
							{/if}
						</span>
				    {/strip}
				{/if}
			</h1>
		{elseif $page_name == 'my-account'}
			<h1 class="page-heading-title">{l s='My account'}</h1>
		{elseif $page_name == 'new-products'}
			<h1 class="page-heading-title">{l s='New products'}</h1>
		{elseif $page_name == 'orderconfirmation'}
			<h1 class="page-heading-title">{l s='Order confirmation'}</h1>
		{elseif $page_name == 'orderfollow'}
			<h1 class="page-heading-title">{l s='Return Merchandise Authorization (RMA)'}</h1>
		{elseif $page_name == 'order'}
			<h1 class="page-heading-title">
				{l s='Shopping-cart summary'}
				{if !isset($empty) && !$PS_CATALOG_MODE && isset($productNumber)}
					<span class="heading-counter">{l s='Your shopping cart contains:'}
						<span id="summary_products_quantity">{$productNumber} {if $productNumber == 1}{l s='product'}{else}{l s='products'}{/if}</span>
					</span>
				{/if}
			</h1>
		{elseif $page_name == 'orderopc'}
			<h1 class="page-heading-title">{l s='Your shopping cart'}</h1>
		{elseif $page_name == 'orderreturn'}
			<h1 class="page-heading-title">{l s='Order Return'}</h1>
		{elseif $page_name == 'orderslip'}
			<h1 class="page-heading-title">{l s='Credit slips'}</h1>
		{elseif $page_name == 'password'}
			<h1 class="page-heading-title">{l s='Forgot your password?'}</h1>

		{elseif $page_name == 'prices-drop'}
			<h1 class="page-heading-title">{l s='Price drop'}</h1>
		{elseif $page_name == 'search'}
			<h1 class="page-heading-title">
				{l s='Search'}&nbsp;
			    {if $nbProducts > 0}
			        <span class="lighter">
			            "{if isset($search_query) && $search_query}{$search_query|escape:'html':'UTF-8'}{elseif $search_tag}{$search_tag|escape:'html':'UTF-8'}{elseif $ref}{$ref|escape:'html':'UTF-8'}{/if}"
			        </span>
			    {/if}
			</h1>
		{elseif $page_name == 'sitemap'}
			<h1 class="page-heading-title">{l s='Sitemap'}</h1>

		{elseif $page_name == 'stores'}
			<h1 class="page-heading-title">{l s='Our stores'}</h1>
		{elseif $page_name == 'supplier'}
			<h1 class="page-heading-title">
				{if isset($supplier)}
					{l s='List of products by supplier:'}&nbsp;{$supplier->name|escape:'html':'UTF-8'}
				{elseif isset($suppliers_list)}
					
					{l s='Suppliers:'}
					{strip}
						<span class="heading-counter">
							{if $nbSuppliers == 0}{l s='There are no suppliers.'}
							{else}
								{if $nbSuppliers == 1}
									{l s='There is %d supplier.' sprintf=$nbSuppliers}
								{else}
									{l s='There are %d suppliers.' sprintf=$nbSuppliers}
								{/if}
							{/if}
						</span>
					{/strip}
				{/if}
			</h1>
		
		{elseif $page_name == 'module-leoblog-category'}
			<h1 class="page-heading-title">
				{if $category->show_title}
					{$category->title}
		        {/if}
			</h1>
		{elseif $page_name == 'module-leoblog-list'}
			<h1 class="page-heading-title">
				{l s='Lastest Blogs'}
			</h1>
		{elseif $page_name == 'module-blockwishlist-mywishlist'}
			<h1 class="page-heading-title">
				{l s='My wishlists'}
			</h1>
		{elseif $page_name == 'module-blockwishlist-view'}
			<h1 class="page-heading-title">
				{l s='Wishlist'}
			</h1>
		{elseif $page_name == 'module-bankwire-payment'}
			<h1 class="page-heading-title">
				{l s='Bank-wire payment.'}
			</h1>
		{/if}
	</div>
	<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
	{if $page_name !='index' && $page_name !='pagenotfound'}
			{include file="$tpl_dir./breadcrumb.tpl"}
	{/if}
	</div>
</div>