<header id="header">
	<section id="topbar" class="topbar">
		<div class="container">
			<div class="container-inner clearfix">
				{hook h="displayNav"}
				{if class_exists('PtsthemePanel')}
					{plugin module='ptsstaticcontent' hook='home'}
				{/if}

			</div>
		</div>
	</section>	
	<section  id="header-main" class="header">
		<div class="container">
				<div id="header_logo">
					<div id="logo-theme" class="logo-store">
					{if Configuration::get('PTS_CP_LOGOTYPE') == 'logo-theme'}
						<div class="logo-theme">
							<a href="{$base_dir}" title="{$shop_name|escape:'html':'UTF-8'}">
								
							</a>
						</div>
					{else}
						<a href="{$base_dir}" title="{$shop_name|escape:'html':'UTF-8'}">
								<img class="logo img-responsive" src="{$logo_url}" alt="{$shop_name|escape:'html':'UTF-8'}"{if isset($logo_image_width) && $logo_image_width} width="{$logo_image_width}"{/if}{if isset($logo_image_height) && $logo_image_height} height="{$logo_image_height}"{/if}/>
						</a>
					{/if}
					</div>
				</div>
				<div class="header-right">

						{if class_exists('PtsthemePanel')}
							{plugin module='blockcart' hook='top'}
						{/if}
						{if class_exists('PtsthemePanel')}
							{plugin module='blocksearch' hook='top'}
						{/if}
				    <div  id="pts-mainnav" class="main-menu">
				        {hook h="displayMainmenu"}
				    </div>
				</div>
		</div>
	</section >

</header>