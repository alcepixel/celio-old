<header id="header">
	<section id="topbar" class="topbar">
		<div class="container">
			<div class="container-inner clearfix">
				{hook h="displayNav"}	
				{if class_exists('PtsthemePanel')}
					{plugin module='blocksearch' hook='top'}
				{/if}

			</div>
		</div>
	</section>	
	<section  id="header-main" class="header">
		<div class="container">
			<div class="row">
				<div id="header_logo" class="col-xs-12 col-sm-12 col-md-3 col-lg-3 inner">
					<div id="logo-theme" class="logo-store">
					{if Configuration::get('PTS_CP_LOGOTYPE') == 'logo-theme'}
						<div class="logo-theme">
							<a href="{$base_dir}" title="{$shop_name|escape:'html':'UTF-8'}">
								
							</a>
						</div>
					{else}
						<a href="{$base_dir}" title="{$shop_name|escape:'html':'UTF-8'}">
								<img class="logo img-responsive" src="{$logo_url}" alt="{$shop_name|escape:'html':'UTF-8'}"{if isset($logo_image_width) && $logo_image_width} width="{$logo_image_width}"{/if}{if isset($logo_image_height) && $logo_image_height} height="{$logo_image_height}"{/if}/>
						</a>
					{/if}
					</div>
				</div>

				<div class="inner hidden-xs col-sm-12 col-md-9 col-lg-9 ">
					{if class_exists('PtsthemePanel')}
						{plugin module='ptsblockreinsurance' hook='footer'}
					{/if}
				</div>
			</div>
		</div>
	
	</section >

    <section  id="pts-mainnav" class="main-menu">
        <div class="container">
			<div class="container-inner clearfix">
                    {hook h="displayMainmenu"}
					{if class_exists('PtsthemePanel')}
						{plugin module='blockcart' hook='top'}
					{/if}

			</div>
        </div>
    </section>

</header>